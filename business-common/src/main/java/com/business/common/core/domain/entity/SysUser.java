package com.business.common.core.domain.entity;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.business.common.annotation.Excel;
import com.business.common.annotation.Excel.ColumnType;
import com.business.common.annotation.Excel.Type;
import com.business.common.annotation.Excels;
import com.business.common.core.domain.BaseEntity;
import com.business.common.xss.Xss;

/**
 * 用户对象 sys_user
 *
 * @author ruoyi
 */
public class SysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户序号", cellType = ColumnType.NUMERIC, prompt = "用户编号")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门编号", type = Type.IMPORT)
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String deptName;

    /** 用户账号 */
    @Excel(name = "登录名称")
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户名称")
    private String nickName;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱", needMerge = true, args = { "aaa", "bbb" })
    private String email;

    /** 用户工号 */
    @Excel(name = "用户工号")
    private String jobNo;

    /** openID */
    @Excel(name = "openID")
    private String openId;

    /** 临时密码 **/
    @Excel(name = "临时密码")
    private String cypher;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phonenumber;

    /** 用户性别 */
    @Excel(name = "用户性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 帐号状态（0正常 1停用） */
    @Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    @Excel(name = "最后登录IP", type = Type.EXPORT)
    private String loginIp;

    /** 最后登录时间 */
    @Excel(name = "最后登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
    private Date loginDate;

    /** 部门对象 */
    @Excels({
        @Excel(name = "部门名称", targetAttr = "deptName", type = Type.EXPORT),
        @Excel(name = "部门负责人", targetAttr = "leader", type = Type.EXPORT)
    })
    private SysDept dept;

    /** 角色对象 */
    @Excel(name = "角色")
    private List<SysRole> roles;

    /** 角色组 */
    private Long[] roleIds;

    /** 岗位组 */
    private Long[] postIds;

    /** 岗位 */
    private String postName;

    /** 角色ID */
    private Long roleId;

    private List<String> sysDeptStr;

    /** 兼职部门ID */
    private String partDeptId;

    /** 兼职部门名称 */
    private String partDeptName;

    /** 部门列表 */
    private String ancestors;

    /** 类人（1：一类人  2：二类人） */
    private String humanoid;

    /** 打卡 */
    private String clockIn;

    /** 日报标记 */
    private String dailyMark;

    /** 岗位编码 */
    private String jobCode;

    /** 副岗编码 */
    private String deputyPostCode;

    /** 兼职部门对应公司 */
    private String partDeptCompany;

    /** 直接上级领导 */
    private String superiorLeaders;

    /** 负责人 */
    private String leader;

    /** 总经理 */
    private String generalManager;

    /** 董事长 */
    private String chairman;

    /** 财务总监 */
    private String financeDirector;

    /** 会计 */
    private String accounting;

    /** 出纳 */
    private String cashier;

    /** 人力资源 */
    private String humanResources;

    /** 行政管理 */
    private String administration;

    /** 文秘 */
    private String secretary;

    /** 分管领导 */
    private String responsibilities;

    /** 无需打卡 */
    private String needNot;

    /** 开户行 */
    private String bankName;

    /** 收款人全称 */
    private String collectionUser;

    /** 银行账号 */
    private String account;

    /**
     * 是否管理员账号，0否1是，默认否
     */
    private Integer adminFlag;

    private String comId;

    private String comName;

    /**
     * 是否超级管理员
     */
    private Integer superAdminFlag;

    /**
     * 租户对象
     */
    private String annex;

    /**
     * 平台标记
     */
    private String plateMark;

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid;

    /**
     * 部门简称
     */
    private String deptAbbreviation;

    /**
     * 钉钉
     */
    private String dingDing;

    /**
     * 钉钉邀请方式
     */
    private Integer dingSource;

    /**
     * 手机标识
     */
    private String phoneCode;

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Integer getDingSource() {
        return dingSource;
    }

    public void setDingSource(Integer dingSource) {
        this.dingSource = dingSource;
    }

    public String getDingDing() {
        return dingDing;
    }

    public void setDingDing(String dingDing) {
        this.dingDing = dingDing;
    }

    public String getDeptAbbreviation() {
        return deptAbbreviation;
    }

    public void setDeptAbbreviation(String deptAbbreviation) {
        this.deptAbbreviation = deptAbbreviation;
    }

    public String getPlateMark() {
        return plateMark;
    }

    public void setPlateMark(String plateMark) {
        this.plateMark = plateMark;
    }

    public String getAnnex() {
        return annex;
    }

    public void setAnnex(String annex) {
        this.annex = annex;
    }

    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getComName() {
        return comName;
    }

    public void setComName(String comName) {
        this.comName = comName;
    }

    public Integer getAdminFlag() {
        return adminFlag;
    }

    public void setAdminFlag(Integer adminFlag) {
        this.adminFlag = adminFlag;
    }

    public Integer getSuperAdminFlag() {
        return superAdminFlag;
    }

    public void setSuperAdminFlag(Integer superAdminFlag) {
        this.superAdminFlag = superAdminFlag;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCollectionUser() {
        return collectionUser;
    }

    public void setCollectionUser(String collectionUser) {
        this.collectionUser = collectionUser;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 车辆信息
     * @return
     */
    private List<BaCars> baCars;

    public List<BaCars> getBaCars() {
        return baCars;
    }

    public void setBaCars(List<BaCars> baCars) {
        this.baCars = baCars;
    }

    public String getNeedNot() {
        return needNot;
    }

    public void setNeedNot(String needNot) {
        this.needNot = needNot;
    }

    public String getGeneralManager() {
        return generalManager;
    }

    public void setGeneralManager(String generalManager) {
        this.generalManager = generalManager;
    }

    public String getChairman() {
        return chairman;
    }

    public void setChairman(String chairman) {
        this.chairman = chairman;
    }

    public String getFinanceDirector() {
        return financeDirector;
    }

    public void setFinanceDirector(String financeDirector) {
        this.financeDirector = financeDirector;
    }

    public String getAccounting() {
        return accounting;
    }

    public void setAccounting(String accounting) {
        this.accounting = accounting;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    public String getHumanResources() {
        return humanResources;
    }

    public void setHumanResources(String humanResources) {
        this.humanResources = humanResources;
    }

    public String getAdministration() {
        return administration;
    }

    public void setAdministration(String administration) {
        this.administration = administration;
    }

    public String getSecretary() {
        return secretary;
    }

    public void setSecretary(String secretary) {
        this.secretary = secretary;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

        public String getLeader()
    {
        return leader;
    }

    public void setLeader(String leader)
    {
        this.leader = leader;
    }

    public String getSuperiorLeaders() {
        return superiorLeaders;
    }

    public void setSuperiorLeaders(String superiorLeaders) {
        this.superiorLeaders = superiorLeaders;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getDeputyPostCode() {
        return deputyPostCode;
    }

    public void setDeputyPostCode(String deputyPostCode) {
        this.deputyPostCode = deputyPostCode;
    }

    public String getHumanoid() {
        return humanoid;
    }

    public void setHumanoid(String humanoid) {
        this.humanoid = humanoid;
    }

    public String getClockIn() {
        return clockIn;
    }

    public void setClockIn(String clockIn) {
        this.clockIn = clockIn;
    }

    public String getDailyMark() {
        return dailyMark;
    }

    public void setDailyMark(String dailyMark) {
        this.dailyMark = dailyMark;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public String getPartDeptId() {
        return partDeptId;
    }

    public void setPartDeptId(String partDeptId) {
        this.partDeptId = partDeptId;
    }

    public String getPartDeptName() {
        return partDeptName;
    }

    public void setPartDeptName(String partDeptName) {
        this.partDeptName = partDeptName;
    }

    public String getPartDeptCompany() {
        return partDeptCompany;
    }

    public void setPartDeptCompany(String partDeptCompany) {
        this.partDeptCompany = partDeptCompany;
    }

    public List<String> getSysDeptStr() {
        return sysDeptStr;
    }

    public void setSysDeptStr(List<String> sysDeptStr) {
        this.sysDeptStr = sysDeptStr;
    }

    public SysUser()
    {

    }

    public SysUser(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }

    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

    public Long getDeptId()
    {
        return deptId;
    }

    public void setDeptId(Long deptId)
    {
        this.deptId = deptId;
    }

    @Xss(message = "用户昵称不能包含脚本字符")
    @Size(min = 0, max = 30, message = "用户昵称长度不能超过30个字符")
    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    @Xss(message = "用户账号不能包含脚本字符")
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getJobNo()
    {
        return jobNo;
    }

    public void setJobNo(String jobNo)
    {
        this.jobNo = jobNo;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getCypher() {
        return cypher;
    }

    public void setCypher(String cypher) {
        this.cypher = cypher;
    }

    @Size(min = 0, max = 11, message = "手机号码长度不能超过11个字符")
    public String getPhonenumber()
    {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber)
    {
        this.phonenumber = phonenumber;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getAvatar()
    {
        return avatar;
    }

    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getLoginIp()
    {
        return loginIp;
    }

    public void setLoginIp(String loginIp)
    {
        this.loginIp = loginIp;
    }

    public Date getLoginDate()
    {
        return loginDate;
    }

    public void setLoginDate(Date loginDate)
    {
        this.loginDate = loginDate;
    }

    public SysDept getDept()
    {
        return dept;
    }

    public void setDept(SysDept dept)
    {
        this.dept = dept;
    }

    public List<SysRole> getRoles()
    {
        return roles;
    }

    public void setRoles(List<SysRole> roles)
    {
        this.roles = roles;
    }

    public Long[] getRoleIds()
    {
        return roleIds;
    }

    public void setRoleIds(Long[] roleIds)
    {
        this.roleIds = roleIds;
    }

    public Long[] getPostIds()
    {
        return postIds;
    }

    public void setPostIds(Long[] postIds)
    {
        this.postIds = postIds;
    }

    public Long getRoleId()
    {
        return roleId;
    }

    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("deptId", getDeptId())
            .append("userName", getUserName())
            .append("nickName", getNickName())
            .append("email", getEmail())
                .append("jobNo", getJobNo())
                .append("openId",getOpenId())
                .append("cypher",getCypher())
            .append("phonenumber", getPhonenumber())
            .append("sex", getSex())
            .append("avatar", getAvatar())
            .append("password", getPassword())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("loginIp", getLoginIp())
            .append("loginDate", getLoginDate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("dept", getDept())
                .append("humanoid", getHumanoid())
                .append("clockIn", getClockIn())
                .append("dailyMark", getDailyMark())
                .append("jobCode",getJobCode())
                .append("deputyPostCode",getDeputyPostCode())
                .append("partDeptCompany",getPartDeptCompany())
                .append("superiorLeaders",getSuperiorLeaders())
                .append("needNot",getNeedNot())
                .append("plateMark",getPlateMark())
                .append("deptAbbreviation",getDeptAbbreviation())
                .append("dingDing",getDingDing())
                .append("dingSource",getDingSource())
                .append("phoneCode",getPhoneCode())
            .toString();
    }
}
