package com.business.common.core.domain;

/**
 * 统一结果返回接口
 *
 */
public interface ResultCode {

    Integer SUCCESS = 0; //成功
    Integer ERROR = -1; //失败
    Integer WRONGPASSWORD = -1; //密码错误
    Integer CODEERROR = -4; //验证码不正确
    Integer NULLCODE = -2; //验证码不存在
    Integer NULLPASSWORD = -4; //请输入密码
    Integer ALREADYUSED = -3; //该手机号或者邮箱已被注册
    Integer percentage = 1; //比例
}
