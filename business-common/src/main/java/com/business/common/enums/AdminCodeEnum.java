package com.business.common.enums;

/**
 * 公共业务枚举类
 */
public enum AdminCodeEnum {
    /**
     * 任务类型
     */
    TASK_TYPE_PRODUCT_PROJECT("taskType:project", "立项申请"),
    TASK_TYPE_PRODUCT_PROJEC_BUSINESS("taskType:projectBusiness", "立项申请"),
    TASK_TYPE_PRODUCT_BID("taskType:bid", "投标申请"),
    TASK_TYPE_PRODUCT_CONTRACT("taskType:contract", "合同申请"),
    TASK_TYPE_PRODUCT_ORDER("taskType:order", "订单申请"),
    TASK_TYPE_PRODUCT_TRANSPORT("taskType:transport", "运输申请"),
    TASK_TYPE_PRODUCT_SETTLEMENT("taskType:settlement", "结算申请"),
    TASK_TYPE_PRODUCT_PAYMENT("taskType:payment", "付款申请"),
    TASK_TYPE_PRODUCT_COLLECTION("taskType:collection", "收款申请"),
    TASK_TYPE_PRODUCT_INVOICE("taskType:invoice", "销项发票申请"),
    TASK_TYPE_PRODUCT_INTODEPOT("taskType:intodepot", "入库申请"),
    TASK_TYPE_PRODUCT_OUTPUTDEPOT("taskType:outputdepot", "出库申请"),
    TASK_TYPE_PRODUCT_CHECKED("taskType:checked", "盘点申请"),
    TASK_TYPE_PRODUCT_CLAIM("taskType:collectionClaim", "认领申请"),
    TASK_TYPE_PRODUCT_AUTOMOBILE("taskType:automobile", "汽运申请"),
    TASK_TYPE_PRODUCT_COUNTERPART("taskType:counterpart", "对接人申请"),
    TASK_TYPE_PRODUCT_TRADER("taskType:trader", "客商信息申请"),
    TASK_TYPE_PRODUCT_CONTRACTSTART("taskType:contractStart", "合同启动申请"),
    TASK_TYPE_PRODUCT_CONTRACTFORWARD("taskType:contractForward", "结转申请"),
    TASK_TYPE_PRODUCT_INPUTINVOICE("taskType:inputInvoice", "进项发票申请"),
    TASK_TYPE_PRODUCT_SEAL("taskType:seal", "用印申请"),
    TASK_TYPE_PRODUCT_DECLARE("taskType:declare", "计划申报申请"),
    TASK_TYPE_PRODUCT_DEPOT("taskType:depot", "仓库申请"),
    TASK_TYPE_PRODUCT_FINANCINGAPPROVAL("taskType:financingApproval", "融资审批"),
    TASK_TYPE_PRODUCT_FINANCINGHANDLE("taskType:financingHandle", "融资办理"),
    TASK_TYPE_PRODUCT_PROCUREMENTPLAN("taskType:procurementPlan", "采购申请"),
    TASK_TYPE_PRODUCT_PROJECTBEFOREHAND("taskType:projectBeforehand", "预立项申请"),
    TASK_TYPE_PRODUCT_GOODS("taskType:goods", "商品信息申请"),
    TASK_TYPE_PRODUCT_GENERALSEAL("taskType:generalSeal", "普通用印申请"),
    TASK_TYPE_PRODUCT_FINANCIALSEAL("taskType:financialSeal", "用印申请"),
    TASK_TYPE_OA_LEAVE("taskType:oaLeave", "请假申请"),
    TASK_TYPE_OA_OVERTIME("taskType:oaOvertime", "加班申请"),
    TASK_TYPE_OA_EVECTION("taskType:oaEvection", "出差申请"),
    TASK_TYPE_OA_EGRESS("taskType:oaEgress", "外出申请"),
    TASK_TYPE_OA_MAINTENANCEEXPANSION("taskType:oaMaintenanceExpansion", "业务维护/拓展申请"),
    TASK_TYPE_OA_FEE("taskType:oaFee", "费用申请"),
    TASK_TYPE_OA_ENTERTAIN("taskType:oaEntertain", "招待申请"),
    TASK_TYPE_OA_OTHER("taskType:oaOther", "其他申请"),
    TASK_TYPE_OA_SUPPLYCLOCK("taskType:supplyClock", "补卡申请"),
    TASK_TYPE_OA_CLAIM("taskType:oaClaim", "报销申请"),
    TASK_TYPE_PRODUCT_CARGOOWNER("taskType:cargoOwner", "货主申请"),
    TASK_TYPE_PRODUCT_WASHINTODEPOT("taskType:washIntodepot", "洗煤入库申请"),
    TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT("taskType:washOutputdepot", "洗煤出库申请"),
    TASK_TYPE_PRODUCT_BLENDINTODEPOT("taskType:blendIntodepot", "配煤入库申请"),
    TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT("taskType:blendOutputdepot", "配煤出库申请"),
    TASK_TYPE_PRODUCT_SAMPLE("taskType:sample", "取样申请"),
    TASK_TYPE_PRODUCT_ASSAY("taskType:assay", "化验申请"),
    TASK_TYPE_PRODUCT_ENTERREGISTER("taskType:enterRegister", "企业注册申请"),
    TASK_TYPE_OA_CONTRACTSEAL("taskType:contractSeal", "合同用印"),

    /**
     * 立项管理审批状态
     */
    PROJECT_STATUS_DRAFT("projectStatus:draft", "草稿"),
    PROJECT_STATUS_VERIFYING("projectStatus:verifying", "审批中"),
    PROJECT_STATUS_PASS("projectStatus:pass", "已通过"),
    PROJECT_STATUS_REJECT("projectStatus:reject", "已拒绝"),
    PROJECT_STATUS_WITHDRAW("projectStatus:withdraw", "已撤回"),
    /**
     * 常规立项业务助理申请
     */
    PROJEC_BUSINESS_STATUS_VERIFYING("projectBusinessStatus:verifying", "审批中"),
    PROJEC_BUSINESS_STATUS_PASS("projectBusinessStatus:pass", "已通过"),
    PROJEC_BUSINESS_STATUS_REJECT("projectBusinessStatus:reject", "已拒绝"),
    PROJEC_BUSINESS_STATUS_WITHDRAW("projectBusinessStatus:withdraw", "已撤回"),

    /**
     * 投标管理审批状态
     */
    BID_STATUS_VERIFYING("bid:verifying", "审批中"),
    BID_STATUS_PASS("bid:pass", "已通过"),
    BID_STATUS_REJECT("bid:reject", "已拒绝"),
    BID_STATUS_WITHDRAW("bid:withdraw", "已撤回"),

    /**
     * 合同管理审批状态
     */
    CONTRACT_STATUS_VERIFYING("contract:verifying", "审批中"),
    CONTRACT_STATUS_PASS("contract:pass", "已通过"),
    CONTRACT_STATUS_REJECT("contract:reject", "已拒绝"),
    CONTRACT_STATUS_WITHDRAW("contract:withdraw", "已撤回"),

    /**
     * 订单管理审批状态
     */
    ORDER_STATUS_VERIFYING("order:verifying", "审批中"),
    ORDER_STATUS_PASS("order:pass", "已通过"),
    ORDER_STATUS_REJECT("order:reject", "已拒绝"),
    ORDER_STATUS_WITHDRAW("order:withdraw", "已撤回"),

    /**
     * 运输管理审批状态
     */
    TRANSPORT_STATUS_VERIFYING("transport:verifying", "审批中"),
    TRANSPORT_STATUS_PASS("transport:pass", "已通过"),
    TRANSPORT_STATUS_REJECT("transport:reject", "已拒绝"),
    TRANSPORT_STATUS_WITHDRAW("transport:withdraw", "已撤回"),

    /**
     * 付款管理审批状态
     */
    PAYMENT_STATUS_VERIFYING("payment:verifying", "审批中"),
    PAYMENT_STATUS_PASS("payment:pass", "已通过"),
    PAYMENT_STATUS_REJECT("payment:reject", "已拒绝"),
    PAYMENT_STATUS_WITHDRAW("payment:withdraw", "已撤回"),

    /**
     * 收款管理审批状态
     */
    COLLECTION_STATUS_VERIFYING("collection:verifying", "审批中"),
    COLLECTION_STATUS_PASS("collection:pass", "已通过"),
    COLLECTION_STATUS_REJECT("collection:reject", "已拒绝"),
    COLLECTION_STATUS_WITHDRAW("collection:withdraw", "已撤回"),

    /**
     * 销项发票管理审批状态
     */
    INVOICE_STATUS_VERIFYING("invoice:verifying", "审批中"),
    INVOICE_STATUS_PASS("invoice:pass", "已通过"),
    INVOICE_STATUS_REJECT("invoice:reject", "已拒绝"),
    INVOICE_STATUS_WITHDRAW("invoice:withdraw", "已撤回"),

    /**
     * 入库审批状态
     */
    INTODEPOT_STATUS_VERIFYING("intodepot:verifying", "审批中"),
    INTODEPOT_STATUS_PASS("intodepot:pass", "已通过"),
    INTODEPOT_STATUS_REJECT("intodepot:reject", "已拒绝"),
    INTODEPOT_STATUS_WITHDRAW("intodepot:withdraw", "已撤回"),

    /**
     * 出库审批状态
     */
    OUTPUTDEPOT_STATUS_VERIFYING("outputdepot:verifying", "审批中"),
    OUTPUTDEPOT_STATUS_PASS("outputdepot:pass", "已通过"),
    OUTPUTDEPOT_STATUS_REJECT("outputdepot:reject", "已拒绝"),
    OUTPUTDEPOT_STATUS_WITHDRAW("outputdepot:withdraw", "已撤回"),

    /**
     * 盘点审批状态
     */
    CHECKED_STATUS_VERIFYING("checked:verifying", "审批中"),
    CHECKED_STATUS_PASS("checked:pass", "已通过"),
    CHECKED_STATUS_REJECT("checked:reject", "已拒绝"),
    CHECKED_STATUS_WITHDRAW("checked:withdraw", "已撤回"),

    /**
     * 认领审批状态
     */
    CLAIM_STATUS_VERIFYING("claim:verifying", "审批中"),
    CLAIM_STATUS_PASS("claim:pass", "已通过"),
    CLAIM_STATUS_REJECT("claim:reject", "已拒绝"),
    CLAIM_STATUS_WITHDRAW("claim:withdraw", "已撤回"),

    /**
     * 汽运管理审批状态
     */
    AUTOMOBILE_STATUS_VERIFYING("automobile:verifying", "审批中"),
    AUTOMOBILE_STATUS_PASS("automobile:pass", "已通过"),
    AUTOMOBILE_STATUS_REJECT("automobile:reject", "已拒绝"),
    AUTOMOBILE_STATUS_WITHDRAW("automobile:withdraw", "已撤回"),

    /**
     * 对接人审批状态
     */
    COUNTERPART_STATUS_VERIFYING("counterpart:verifying", "审批中"),
    COUNTERPART_STATUS_PASS("counterpart:pass", "已通过"),
    COUNTERPART_STATUS_REJECT("counterpart:reject", "已拒绝"),
    COUNTERPART_STATUS_WITHDRAW("counterpart:withdraw", "已撤回"),

    /**
     * 供应商审批状态
     */
    SUPPLIER_STATUS_VERIFYING("supplier:verifying", "审批中"),
    SUPPLIER_STATUS_PASS("supplier:pass", "已通过"),
    SUPPLIER_STATUS_REJECT("supplier:reject", "已拒绝"),
    SUPPLIER_STATUS_WITHDRAW("supplier:withdraw", "已撤回"),

    /**
     * 终端企业审批状态
     */
    ENTERPRISE_STATUS_VERIFYING("enterprise:verifying", "审批中"),
    ENTERPRISE_STATUS_PASS("enterprise:pass", "已通过"),
    ENTERPRISE_STATUS_REJECT("enterprise:reject", "已拒绝"),
    ENTERPRISE_STATUS_WITHDRAW("enterprise:withdraw", "已撤回"),

    /**
     * 公司审批状态
     */
    COMPANY_STATUS_VERIFYING("company:verifying", "审批中"),
    COMPANY_STATUS_PASS("company:pass", "已通过"),
    COMPANY_STATUS_REJECT("company:reject", "已拒绝"),
    COMPANY_STATUS_WITHDRAW("company:withdraw", "已撤回"),

    /**
     * 合同启动审批状态
     */
    CONTRACTSTART_STATUS_VERIFYING("contractStart:verifying", "审批中"),
    CONTRACTSTART_STATUS_PASS("contractStart:pass", "已通过"),
    CONTRACTSTART_STATUS_REJECT("contractStart:reject", "已拒绝"),
    CONTRACTSTART_STATUS_WITHDRAW("contractStart:withdraw", "已撤回"),

    /**
     * 合同结转审批状态
     */
    CONTRACTFORWARD_STATUS_VERIFYING("contractForward:verifying", "审批中"),
    CONTRACTFORWARD_STATUS_PASS("contractForward:pass", "已通过"),
    CONTRACTFORWARD_STATUS_REJECT("contractForward:reject", "已拒绝"),
    CONTRACTFORWARD_STATUS_WITHDRAW("contractForward:withdraw", "已撤回"),

    /**
     * 结算审批状态
     */
    SETTLEMENT_STATUS_VERIFYING("settlement:verifying", "审批中"),
    SETTLEMENT_STATUS_PASS("settlement:pass", "已通过"),
    SETTLEMENT_STATUS_REJECT("settlement:reject", "已拒绝"),
    SETTLEMENT_STATUS_WITHDRAW("settlement:withdraw", "已撤回"),

    /**
     * 进项发票管理审批状态
     */
    INPUTINVOICE_STATUS_VERIFYING("inputInvoice:verifying", "审批中"),
    INPUTINVOICE_STATUS_PASS("inputInvoice:pass", "已通过"),
    INPUTINVOICE_STATUS_REJECT("inputInvoice:reject", "已拒绝"),
    INPUTINVOICE_STATUS_WITHDRAW("inputInvoice:withdraw", "已撤回"),

    /**
     * 用印申请审批状态
     */
    SEAL_STATUS_VERIFYING("seal:verifying", "审批中"),
    SEAL_STATUS_PASS("seal:pass", "已通过"),
    SEAL_STATUS_REJECT("seal:reject", "已拒绝"),
    SEAL_STATUS_WITHDRAW("seal:withdraw", "已撤回"),
    /**
     * 仓库申请审批状态
     */
    DEPOT_STATUS_VERIFYING("depot:verifying", "审批中"),
    DEPOT_STATUS_PASS("depot:pass", "已通过"),
    DEPOT_STATUS_REJECT("depot:reject", "已拒绝"),
    DEPOT_STATUS_WITHDRAW("depot:withdraw", "已撤回"),

    /**
     * 计划申报申请审批状态
     */
    DECLARE_STATUS_VERIFYING("declare:verifying", "审批中"),
    DECLARE_STATUS_PASS("declare:pass", "已通过"),
    DECLARE_STATUS_REJECT("declare:reject", "已拒绝"),
    DECLARE_STATUS_WITHDRAW("declare:withdraw", "已撤回"),

    /**
     * 融资审批申请审批状态
     */
    FINANCINGAPPROVAL_STATUS_VERIFYING("financingApproval:verifying", "审批中"),
    FINANCINGAPPROVAL_STATUS_PASS("financingApproval:pass", "已通过"),
    FINANCINGAPPROVAL_STATUS_REJECT("financingApproval:reject", "已拒绝"),
    FINANCINGAPPROVAL_STATUS_WITHDRAW("financingApproval:withdraw", "已撤回"),

    /**
     * 融资办理申请审批状态
     */
    FINANCINGHANDLE_STATUS_VERIFYING("financingHandle:verifying", "审批中"),
    FINANCINGHANDLE_STATUS_PASS("financingHandle:pass", "已通过"),
    FINANCINGHANDLE_STATUS_REJECT("financingHandle:reject", "已拒绝"),
    FINANCINGHANDLE_STATUS_WITHDRAW("financingHandle:withdraw", "已撤回"),

    /**
     * 采购计划申请审批状态
     */
    PROCUREMENTPLAN_STATUS_VERIFYING("procurementPlan:verifying", "审批中"),
    PROCUREMENTPLAN_STATUS_PASS("procurementPlan:pass", "已通过"),
    PROCUREMENTPLAN_STATUS_REJECT("procurementPlan:reject", "已拒绝"),
    PROCUREMENTPLAN_STATUS_WITHDRAW("procurementPlan:withdraw", "已撤回"),

    /**
     * 预立项管理审批状态
     */
    PROJECTBEFOREHAND_STATUS_VERIFYING("projectBeforehand:verifying", "审批中"),
    PROJECTBEFOREHAND_STATUS_PASS("projectBeforehand:pass", "已通过"),
    PROJECTBEFOREHAND_STATUS_REJECT("projectBeforehand:reject", "已拒绝"),
    PROJECTBEFOREHAND_STATUS_WITHDRAW("projectBeforehand:withdraw", "已撤回"),

    /**
     * 商品信息审批状态
     */
    GOODS_STATUS_VERIFYING("goods:verifying", "审批中"),
    GOODS_STATUS_PASS("goods:pass", "已通过"),
    GOODS_STATUS_REJECT("goods:reject", "已拒绝"),
    GOODS_STATUS_WITHDRAW("goods:withdraw", "已撤回"),

    /**
     * 财务章用印申请审批状态
     */
    FINANCIALSEAL_STATUS_VERIFYING("financialSeal:verifying", "审批中"),
    FINANCIALSEAL_STATUS_PASS("financialSeal:pass", "已通过"),
    FINANCIALSEAL_STATUS_REJECT("financialSeal:reject", "已拒绝"),
    FINANCIALSEAL_STATUS_WITHDRAW("financialSeal:withdraw", "已撤回"),

    /**
     * 普通用印申请审批状态
     */
    GENERALSEAL_STATUS_VERIFYING("generalSeal:verifying", "审批中"),
    GENERALSEAL_STATUS_PASS("generalSeal:pass", "已通过"),
    GENERALSEAL_STATUS_REJECT("generalSeal:reject", "已拒绝"),
    GENERALSEAL_STATUS_WITHDRAW("generalSeal:withdraw", "已撤回"),

    /**
     * 请假申请审批状态
     */
    OALEAVE_STATUS_VERIFYING("oaLeave:verifying", "审批中"),
    OALEAVE_STATUS_PASS("oaLeave:pass", "已通过"),
    OALEAVE_STATUS_REJECT("oaLeave:reject", "已拒绝"),
    OALEAVE_STATUS_WITHDRAW("oaLeave:withdraw", "已撤回"),

    /**
     * 加班申请审批状态
     */
    OAOVERTIME_STATUS_VERIFYING("oaOvertime:verifying", "审批中"),
    OAOVERTIME_STATUS_PASS("oaOvertime:pass", "已通过"),
    OAOVERTIME_STATUS_REJECT("oaOvertime:reject", "已拒绝"),
    OAOVERTIME_STATUS_WITHDRAW("oaOvertime:withdraw", "已撤回"),

    /**
     * 出差申请审批状态
     */
    OAEVECTION_STATUS_VERIFYING("oaEvection:verifying", "审批中"),
    OAEVECTION_STATUS_PASS("oaEvection:pass", "已通过"),
    OAEVECTION_STATUS_REJECT("oaEvection:reject", "已拒绝"),
    OAEVECTION_STATUS_WITHDRAW("oaEvection:withdraw", "已撤回"),

    /**
     * 外出申请审批状态
     */
    OAEGRESS_STATUS_VERIFYING("oaEgress:verifying", "审批中"),
    OAEGRESS_STATUS_PASS("oaEgress:pass", "已通过"),
    OAEGRESS_STATUS_REJECT("oaEgress:reject", "已拒绝"),
    OAEGRESS_STATUS_WITHDRAW("oaEgress:withdraw", "已撤回"),

    /**
     * 业务维护/拓展申请审批状态
     */
    OAMAINTENANCEEXPANSION_STATUS_VERIFYING("oaMaintenanceExpansion:verifying", "审批中"),
    OAMAINTENANCEEXPANSION_STATUS_PASS("oaMaintenanceExpansion:pass", "已通过"),
    OAMAINTENANCEEXPANSION_STATUS_REJECT("oaMaintenanceExpansion:reject", "已拒绝"),
    OAMAINTENANCEEXPANSION_STATUS_WITHDRAW("oaMaintenanceExpansion:withdraw", "已撤回"),

    /**
     * 费用申请审批状态
     */
    OAFEE_STATUS_VERIFYING("oaFee:verifying", "审批中"),
    OAFEE_STATUS_PASS("oaFee:pass", "已通过"),
    OAFEE_STATUS_REJECT("oaFee:reject", "已拒绝"),
    OAFEE_STATUS_WITHDRAW("oaFee:withdraw", "已撤回"),

    /**
     * 招待申请审批状态
     */
    OAENTERTAIN_STATUS_VERIFYING("oaEntertain:verifying", "审批中"),
    OAENTERTAIN_STATUS_PASS("oaEntertain:pass", "已通过"),
    OAENTERTAIN_STATUS_REJECT("oaEntertain:reject", "已拒绝"),
    OAENTERTAIN_STATUS_WITHDRAW("oaEntertain:withdraw", "已撤回"),

    /**
     * 其他申请审批状态
     */
    OAOTHER_STATUS_VERIFYING("oaOther:verifying", "审批中"),
    OAOTHER_STATUS_PASS("oaOther:pass", "已通过"),
    OAOTHER_STATUS_REJECT("oaOther:reject", "已拒绝"),
    OAOTHER_STATUS_WITHDRAW("oaOther:withdraw", "已撤回"),

    /**
     * 补卡申请审批状态
     */
    OASUPPLYCLOCK_STATUS_VERIFYING("oaSupplyClock:verifying", "审批中"),
    OASUPPLYCLOCK_STATUS_PASS("oaSupplyClock:pass", "已通过"),
    OASUPPLYCLOCK_STATUS_REJECT("oaSupplyClock:reject", "已拒绝"),
    OASUPPLYCLOCK_STATUS_WITHDRAW("oaSupplyClock:withdraw", "已撤回"),

    /**
     * 报销申请审批状态
     */
    OACLAIM_STATUS_VERIFYING("oaClaim:verifying", "审批中"),
    OACLAIM_STATUS_PASS("oaClaim:pass", "已通过"),
    OACLAIM_STATUS_REJECT("oaClaim:reject", "已拒绝"),
    OACLAIM_STATUS_WITHDRAW("oaClaim:withdraw", "已撤回"),

    /**
     * 货主档案审批状态
     */
    CARGOOWNER_STATUS_VERIFYING("cargoOwner:verifying", "审批中"),
    CARGOOWNER_STATUS_PASS("cargoOwner:pass", "已通过"),
    CARGOOWNER_STATUS_REJECT("cargoOwner:reject", "已拒绝"),
    CARGOOWNER_STATUS_WITHDRAW("cargoOwner:withdraw", "已撤回"),

    /**
     * 洗煤出库审批状态
     */
    WASHIOUTPUTDEPOT_STATUS_VERIFYING("washOutputdepot:verifying", "审批中"),
    WASHIOUTPUTDEPOT_STATUS_PASS("washOutputdepot:pass", "已通过"),
    WASHIOUTPUTDEPOT_STATUS_REJECT("washOutputdepot:reject", "已拒绝"),
    WASHIOUTPUTDEPOT_STATUS_WITHDRAW("washOutputdepot:withdraw", "已撤回"),

    /**
     * 洗煤入库审批状态
     */
    WASHINTODEPOT_STATUS_VERIFYING("washIntodepot:verifying", "审批中"),
    WASHINTODEPOT_STATUS_PASS("washIntodepot:pass", "已通过"),
    WASHINTODEPOT_STATUS_REJECT("washIntodepot:reject", "已拒绝"),
    WASHINTODEPOT_STATUS_WITHDRAW("washIntodepot:withdraw", "已撤回"),

    /**
     * 配煤出库审批状态
     */
    BLENDOUTPUTDEPOT_STATUS_VERIFYING("blendOutputdepot:verifying", "审批中"),
    BLENDOUTPUTDEPOT_STATUS_PASS("blendOutputdepot:pass", "已通过"),
    BLENDOUTPUTDEPOT_STATUS_REJECT("blendOutputdepot:reject", "已拒绝"),
    BLENDOUTPUTDEPOT_STATUS_WITHDRAW("blendOutputdepot:withdraw", "已撤回"),

    /**
     * 配煤入库审批状态
     */
    BLENDINTODEPOT_STATUS_VERIFYING("blendIntodepot:verifying", "审批中"),
    BLENDINTODEPOT_STATUS_PASS("blendIntodepot:pass", "已通过"),
    BLENDINTODEPOT_STATUS_REJECT("blendIntodepot:reject", "已拒绝"),
    BLENDINTODEPOT_STATUS_WITHDRAW("blendIntodepot:withdraw", "已撤回"),

    /**
     * 取样审批状态
     */
    SAMPLE_STATUS_VERIFYING("sample:verifying", "审批中"),
    SAMPLE_STATUS_PASS("sample:pass", "已通过"),
    SAMPLE_STATUS_REJECT("sample:reject", "已拒绝"),
    SAMPLE_STATUS_WITHDRAW("sample:withdraw", "已撤回"),

    /**
     * 化验审批状态
     */
    ASSAY_STATUS_VERIFYING("assay:verifying", "审批中"),
    ASSAY_STATUS_PASS("assay:pass", "已通过"),
    ASSAY_STATUS_REJECT("assay:reject", "已拒绝"),
    ASSAY_STATUS_WITHDRAW("assay:withdraw", "已撤回"),

    /**
     * 企业注册审批状态
     */
    ENTERREGISTER_STATUS_VERIFYING("enterRegister:verifying", "审批中"),
    ENTERREGISTER_STATUS_PASS("enterRegister:pass", "已通过"),
    ENTERREGISTER_STATUS_REJECT("enterRegister:reject", "已拒绝"),
    ENTERREGISTER_STATUS_WITHDRAW("enterRegister:withdraw", "已撤回"),

    /**
     * 合同用印审批状态
     */
    CONTRACTSEAL_STATUS_VERIFYING("contractSeal:verifying", "审批中"),
    CONTRACTSEAL_STATUS_PASS("contractSeal:pass", "已通过"),
    CONTRACTSEAL_STATUS_REJECT("contractSeal:reject", "已拒绝"),
    CONTRACTSEAL_STATUS_WITHDRAW("contractSeal:withdraw", "已撤回"),

    /**
     * 立项管理：审批内容
     */
    PROJECT_APPROVAL_CONTENT_INSERT("projectApprovalContent:insert", "新增立项"),
    PROJECT_APPROVAL_CONTENT_UPDATE("projectApprovalContent:update", "修改立项"),
    PROJECT_APPROVAL_CONTENT_DELETE("projectApprovalContent:delete", "删除立项"),
    /**
     * 常规立项业务助理管理：审批内容
     */
    /*PROJEC_BUSINESS_APPROVAL_CONTENT_INSERT("projectBusinessApprovalContent:insert", "新增常规立项"),
    PROJEC_BUSINESS_APPROVAL_CONTENT_UPDATE("projectBusinessApprovalContent:update", "修改常规立项"),
    PROJEC_BUSINESS_APPROVAL_CONTENT_DELETE("projectBusinessApprovalContent:delete", "删除常规立项"),*/
    PROJEC_BUSINESS_APPROVAL_CONTENT_INSERT("projectusinessApprovalContent:insert", "新增立项"),
    PROJEC_BUSINESS_APPROVAL_CONTENT_UPDATE("projectbusinessApprovalContent:update", "修改立项"),
    PROJEC_BUSINESS_APPROVAL_CONTENT_DELETE("projectbusinessApprovalContent:delete", "删除立项"),
    /**
     * 投标管理：审批内容
     */
    BID_APPROVAL_CONTENT_INSERT("bidApprovalContent:insert", "新增投标"),
    BID_APPROVAL_CONTENT_UPDATE("bidApprovalContent:update", "修改投标"),
    BID_APPROVAL_CONTENT_DELETE("bidApprovalContent:delete", "删除投标"),

    /**
     * 合同管理：审批内容
     */
    CONTRACT_APPROVAL_CONTENT_INSERT("contractApprovalContent:insert", "新增合同"),
    CONTRACT_APPROVAL_CONTENT_UPDATE("contractApprovalContent:update", "修改合同"),
    CONTRACT_APPROVAL_CONTENT_DELETE("contractApprovalContent:delete", "删除合同"),

    /**
     * 订单管理：审批内容
     */
    ORDER_APPROVAL_CONTENT_INSERT("orderApprovalContent:insert", "新增订单"),
    ORDER_APPROVAL_CONTENT_UPDATE("orderApprovalContent:update", "修改订单"),
    ORDER_APPROVAL_CONTENT_DELETE("orderApprovalContent:delete", "删除订单"),

    /**
     * 运输管理：审批内容
     */
    TRANSPORT_APPROVAL_CONTENT_INSERT("transportApprovalContent:insert", "新增运输"),
    TRANSPORT_APPROVAL_CONTENT_UPDATE("transportApprovalContent:update", "修改运输"),
    TRANSPORT_APPROVAL_CONTENT_DELETE("transportApprovalContent:delete", "删除运输"),

    /**
     * 付款管理：审批内容
     */
    PAYMENT_APPROVAL_CONTENT_INSERT("paymentApprovalContent:insert", "新增付款"),
    PAYMENT_APPROVAL_CONTENT_UPDATE("paymentApprovalContent:update", "修改付款"),
    PAYMENT_APPROVAL_CONTENT_DELETE("paymentApprovalContent:delete", "删除付款"),

    /**
     * 收款管理：审批内容
     */
    COLLECTION_APPROVAL_CONTENT_INSERT("collectionApprovalContent:insert", "新增收款"),
    COLLECTION_APPROVAL_CONTENT_UPDATE("collectionApprovalContent:update", "修改收款"),
    COLLECTION_APPROVAL_CONTENT_DELETE("collectionApprovalContent:delete", "删除收款"),

    /**
     * 销项发票管理：审批内容
     */
    INVOICE_APPROVAL_CONTENT_INSERT("invoiceApprovalContent:insert", "新增销项发票"),
    INVOICE_APPROVAL_CONTENT_UPDATE("invoiceApprovalContent:update", "修改销项发票"),
    INVOICE_APPROVAL_CONTENT_DELETE("invoiceApprovalContent:delete", "删除销项发票"),

    /**
     * 入库管理：审批内容
     */
    INTODEPOT_APPROVAL_CONTENT_INSERT("intodepotApprovalContent:insert", "新增入库"),
    INTODEPOT_APPROVAL_CONTENT_UPDATE("intodepotApprovalContent:update", "修改入库"),
    INTODEPOT_APPROVAL_CONTENT_DELETE("intodepotApprovalContent:delete", "删除入库"),

    /**
     * 出库管理：审批内容
     */
    OUTPUTDEPOT_APPROVAL_CONTENT_INSERT("outputdepotApprovalContent:insert", "新增出库"),
    OUTPUTDEPOT_APPROVAL_CONTENT_UPDATE("outputdepotApprovalContent:update", "修改出库"),
    OUTPUTDEPOT_APPROVAL_CONTENT_DELETE("outputdepotApprovalContent:delete", "删除出库"),

    /**
     * 盘点管理：审批内容
     */
    CHECKED_APPROVAL_CONTENT_INSERT("checkedApprovalContent:insert", "新增盘点"),
    CHECKED_APPROVAL_CONTENT_UPDATE("checkedApprovalContent:update", "修改盘点"),
    CHECKED_APPROVAL_CONTENT_DELETE("checkedApprovalContent:delete", "删除盘点"),

    /**
     * 认领收款：审批内容
     */
    CLAIM_APPROVAL_CONTENT_INSERT("claimApprovalContent:insert", "新增认领"),
    CLAIM_APPROVAL_CONTENT_UPDATE("claimApprovalContent:update", "修改收款"),

    /**
     * 汽运管理：审批内容
     */
    AUTOMOBILE_APPROVAL_CONTENT_INSERT("automobileApprovalContent:insert", "新增汽运"),
    AUTOMOBILE_APPROVAL_CONTENT_UPDATE("automobileApprovalContent:update", "修改汽运"),
    AUTOMOBILE_APPROVAL_CONTENT_DELETE("automobileApprovalContent:delete", "查看汽运"),

    /**
     * 汽运管理：审批内容
     */
    COUNTERPART_APPROVAL_CONTENT_INSERT("counterpartApprovalContent:insert", "新增对接人"),
    COUNTERPART_APPROVAL_CONTENT_UPDATE("counterpartApprovalContent:update", "修改对接人"),
    COUNTERPART_APPROVAL_CONTENT_DELETE("counterpartApprovalContent:delete", "查看对接人"),

    /**
     * 供应商管理：审批内容
     */
    SUPPLIER_APPROVAL_CONTENT_INSERT("supplierApprovalContent:insert", "新增供应商"),
    SUPPLIER_APPROVAL_CONTENT_UPDATE("supplierApprovalContent:update", "修改供应商"),
    SUPPLIER_APPROVAL_CONTENT_DELETE("supplierApprovalContent:delete", "查看供应商"),

    /**
     * 终端企业管理：审批内容
     */
    ENTERPRISE_APPROVAL_CONTENT_INSERT("enterpriseApprovalContent:insert", "新增终端企业"),
    ENTERPRISE_APPROVAL_CONTENT_UPDATE("enterpriseApprovalContent:update", "修改终端企业"),
    ENTERPRISE_APPROVAL_CONTENT_DELETE("enterpriseApprovalContent:delete", "查看终端企业"),

    /**
     * 公司管理：审批内容
     */
    COMPANY_APPROVAL_CONTENT_INSERT("companyApprovalContent:insert", "新增公司"),
    COMPANY_APPROVAL_CONTENT_UPDATE("companyApprovalContent:update", "修改公司"),
    COMPANY_APPROVAL_CONTENT_DELETE("companyApprovalContent:delete", "查看公司"),

    /**
     * 合同启动：审批内容
     */
    CONTRACTSTART_APPROVAL_CONTENT_INSERT("contractStartApprovalContent:insert", "新增合同启动"),
    CONTRACTSTART_APPROVAL_CONTENT_UPDATE("contractStartApprovalContent:update", "修改合同启动"),
    CONTRACTSTART_APPROVAL_CONTENT_DELETE("contractStartApprovalContent:delete", "查看合同启动"),

    /**
     * 合同结转：审批内容
     */
    CONTRACTFORWARD_APPROVAL_CONTENT_INSERT("contractForwardApprovalContent:insert", "新增合同结转"),
    CONTRACTFORWARD_APPROVAL_CONTENT_UPDATE("contractForwardApprovalContent:update", "修改合同结转"),
    CONTRACTFORWARD_APPROVAL_CONTENT_DELETE("contractForwardApprovalContent:delete", "查看合同结转"),

    /**
     * 结算：审批内容
     */
    SETTLEMENT_APPROVAL_CONTENT_INSERT("settlementApprovalContent:insert", "新增结算"),
    SETTLEMENT_APPROVAL_CONTENT_UPDATE("settlementApprovalContent:update", "修改结算"),
    SETTLEMENT_APPROVAL_CONTENT_DELETE("settlementApprovalContent:delete", "查看结算"),

    /**
     * 进项发票管理：审批内容
     */
    INPUTINVOICE_APPROVAL_CONTENT_INSERT("inputInvoiceApprovalContent:insert", "新增进项发票"),
    INPUTINVOICE_APPROVAL_CONTENT_UPDATE("inputInvoiceApprovalContent:update", "修改进项发票"),
    INPUTINVOICE_APPROVAL_CONTENT_DELETE("inputInvoiceApprovalContent:delete", "删除进项发票"),

    /**
     * 用印申请：审批内容
     */
    SEAL_APPROVAL_CONTENT_INSERT("sealApprovalContent:insert", "新增用印"),
    SEAL_APPROVAL_CONTENT_UPDATE("sealApprovalContent:update", "修改用印"),
    SEAL_APPROVAL_CONTENT_DELETE("sealApprovalContent:delete", "删除用印"),

    /**
     * (财务章)用印申请：审批内容
     */
    FINANCIALSEAL_APPROVAL_CONTENT_INSERT("financialSealApprovalContent:insert", "新增用印"),
    FINANCIALSEAL_APPROVAL_CONTENT_UPDATE("financialSealApprovalContent:update", "修改用印"),
    FINANCIALSEAL_APPROVAL_CONTENT_DELETE("financialSealApprovalContent:delete", "删除用印"),

    /**
     * 用印申请：审批内容
     */
    GENERALSEAL_APPROVAL_CONTENT_INSERT("generalSealApprovalContent:insert", "新增用印"),
    GENERALSEAL_APPROVAL_CONTENT_UPDATE("generalSealApprovalContent:update", "修改用印"),
    GENERALSEAL_APPROVAL_CONTENT_DELETE("generalSealApprovalContent:delete", "删除用印"),

    /**
     * 计划申报申请：审批内容
     */
    DECLARE_APPROVAL_CONTENT_INSERT("declareApprovalContent:insert", "新增计划申报"),
    DECLARE_APPROVAL_CONTENT_UPDATE("declareApprovalContent:update", "修改计划申报"),
    DECLARE_APPROVAL_CONTENT_DELETE("declareApprovalContent:delete", "删除计划申报"),
    /**
     * 仓库申请：审批内容
     */
    DEPOT_APPROVAL_CONTENT_INSERT("depotApprovalContent:insert", "新增仓库"),
    DEPOT_APPROVAL_CONTENT_UPDATE("depotApprovalContent:update", "修改仓库"),
    DEPOT_APPROVAL_CONTENT_DELETE("depotApprovalContent:delete", "删除仓库"),

    /**
     * 融资申请：审批内容
     */
    FINANCINGAPPROVAL_APPROVAL_CONTENT_INSERT("financingApproval:insert", "新增融资审批"),
    FINANCINGAPPROVAL_APPROVAL_CONTENT_UPDATE("financingApproval:update", "修改融资审批"),
    FINANCINGAPPROVAL_APPROVAL_CONTENT_DELETE("financingApproval:delete", "删除融资审批"),

    /**
     * 融资办理：审批内容
     */
    FINANCINGHANDLE_APPROVAL_CONTENT_INSERT("financingHandle:insert", "新增融资办理"),
    FINANCINGHANDLE_APPROVAL_CONTENT_UPDATE("financingHandle:update", "修改融资办理"),
    FINANCINGHANDLE_APPROVAL_CONTENT_DELETE("financingHandle:delete", "删除融资办理"),

    /**
     * ：采购计划：审批内容
     */
    PROCUREMENTPLAN_APPROVAL_CONTENT_INSERT("procurementPlanApprovalContent:insert", "新增采购计划"),
    PROCUREMENTPLAN_APPROVAL_CONTENT_UPDATE("procurementPlanApprovalContent:update", "修改采购计划"),
    PROCUREMENTPLAN_APPROVAL_CONTENT_DELETE("procurementPlanApprovalContent:delete", "删除采购计划"),

    /**
     * 预立项管理：审批内容
     */
    PROJECTBEFOREHAND_APPROVAL_CONTENT_INSERT("projectBeforehandApprovalContent:insert", "新增预立项"),
    PROJECTBEFOREHAND_APPROVAL_CONTENT_UPDATE("projectBeforehandApprovalContent:update", "修改预立项"),
    PROJECTBEFOREHAND_APPROVAL_CONTENT_DELETE("projectBeforehandApprovalContent:delete", "删除预立项"),

    /**
     * 商品信息：审批内容
     */
    GOODS_APPROVAL_CONTENT_INSERT("goodsApprovalContent:insert", "新增商品"),
    GOODS_APPROVAL_CONTENT_UPDATE("goodsApprovalContent:update", "修改商品"),
    GOODS_APPROVAL_CONTENT_DELETE("goodsApprovalContent:delete", "删除商品"),

    /**
     * 请假申请：审批内容
     */
    OALEAVE_APPROVAL_CONTENT_INSERT("oaLeaveApprovalContent:insert", "新增请假申请"),
    OALEAVE_APPROVAL_CONTENT_UPDATE("oaLeaveApprovalContent:update", "修改请假申请"),
    OALEAVE_APPROVAL_CONTENT_DELETE("oaLeaveApprovalContent:delete", "删除请假申请"),

    /**
     * 加班申请：审批内容
     */
    OAOVERTIME_APPROVAL_CONTENT_INSERT("oaOvertimeApprovalContent:insert", "新增加班申请"),
    OAOVERTIME_APPROVAL_CONTENT_UPDATE("oaOvertimeApprovalContent:update", "修改加班申请"),
    OAOVERTIME_APPROVAL_CONTENT_DELETE("oaOvertimeApprovalContent:delete", "删除加班申请"),

    /**
     * 出差申请：审批内容
     */
    OAEVECTION_APPROVAL_CONTENT_INSERT("oaEvectionApprovalContent:insert", "新增出差申请"),
    OAEVECTION_APPROVAL_CONTENT_UPDATE("oaEvectionApprovalContent:update", "修改出差申请"),
    OAEVECTION_APPROVAL_CONTENT_DELETE("oaEvectionApprovalContent:delete", "删除出差申请"),

    /**
     * 外出申请：审批内容
     */
    OAEGRESS_APPROVAL_CONTENT_INSERT("oaEgressApprovalContent:insert", "新增外出申请"),
    OAEGRESS_APPROVAL_CONTENT_UPDATE("oaEgressApprovalContent:update", "修改外出申请"),
    OAEGRESS_APPROVAL_CONTENT_DELETE("oaEgressApprovalContent:delete", "删除外出申请"),

    /**
     * 业务维护/拓展申请：审批内容
     */
    OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_INSERT("oaMaintenanceExpansionApprovalContent:insert", "新增业务维护/拓展申请"),
    OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_UPDATE("oaMaintenanceExpansionApprovalContent:update", "修改业务维护/拓展申请"),
    OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_DELETE("oaMaintenanceExpansionApprovalContent:delete", "删除业务维护/拓展申请"),

    /**
     * 费用申请：审批内容
     */
    OAFEE_APPROVAL_CONTENT_INSERT("oaFeeApprovalContent:insert", "新增费用申请"),
    OAFEE_APPROVAL_CONTENT_UPDATE("oaFeeApprovalContent:update", "修改费用申请"),
    OAFEE_APPROVAL_CONTENT_DELETE("oaFeeApprovalContent:delete", "删除费用申请"),

    /**
     * 招待申请：审批内容
     */
    OAENTERTAIN_APPROVAL_CONTENT_INSERT("oaEntertainApprovalContent:insert", "新增招待申请"),
    OAENTERTAIN_APPROVAL_CONTENT_UPDATE("oaEntertainApprovalContent:update", "修改招待申请"),
    OAENTERTAIN_APPROVAL_CONTENT_DELETE("oaEntertainApprovalContent:delete", "删除招待申请"),

    /**
     * 其他申请：审批内容
     */
    OAOTHER_APPROVAL_CONTENT_INSERT("oaOtherApprovalContent:insert", "新增其他申请"),
    OAOTHER_APPROVAL_CONTENT_UPDATE("oaOtherApprovalContent:update", "修改其他申请"),
    OAOTHER_APPROVAL_CONTENT_DELETE("oaOtherApprovalContent:delete", "删除其他申请"),

    /**
     * 补卡申请：审批内容
     */
    OASUPPLYCLOCK_APPROVAL_CONTENT_INSERT("oaSupplyClockApprovalContent:insert", "新增补卡申请"),
    OASUPPLYCLOCK_APPROVAL_CONTENT_UPDATE("oaSupplyClockApprovalContent:update", "修改补卡申请"),
    OASUPPLYCLOCK_APPROVAL_CONTENT_DELETE("oaSupplyClockApprovalContent:delete", "删除补卡申请"),

    /**
     * 报销申请：审批内容
     */
    OACLAIM_APPROVAL_CONTENT_INSERT("oaClaimApprovalContent:insert", "新增报销申请"),
    OACLAIM_APPROVAL_CONTENT_UPDATE("oaClaimApprovalContent:update", "修改报销申请"),
    OACLAIM_APPROVAL_CONTENT_DELETE("oaClaimApprovalContent:delete", "删除报销申请"),

    /**
     * 货主申请：审批内容
     */
    CARGOOWNER_APPROVAL_CONTENT_INSERT("cargoOwnerApprovalContent:insert", "新增货主申请"),
    CARGOOWNER_APPROVAL_CONTENT_UPDATE("cargoOwnerApprovalContent:update", "修改货主申请"),
    CARGOOWNER_APPROVAL_CONTENT_DELETE("cargoOwnerApprovalContent:delete", "删除货主申请"),

    /**
     * 洗煤入库管理：审批内容
     */
    WASHINTODEPOT_APPROVAL_CONTENT_INSERT("washIntodepotApprovalContent:insert", "新增洗煤入库"),
    WASHINTODEPOT_APPROVAL_CONTENT_UPDATE("washIntodepotApprovalContent:update", "修改洗煤入库"),
    WASHINTODEPOT_APPROVAL_CONTENT_DELETE("washIntodepotApprovalContent:delete", "删除洗煤入库"),

    /**
     * 洗煤出库管理：审批内容
     */
    WASHIOUTPUTDEPOT_APPROVAL_CONTENT_INSERT("washOutputdepotApprovalContent:insert", "新增洗煤出库"),
    WASHIOUTPUTDEPOT_APPROVAL_CONTENT_UPDATE("washOutputdepotApprovalContent:update", "修改洗煤出库"),
    WASHIOUTPUTDEPOT_APPROVAL_CONTENT_DELETE("washOutputdepotApprovalContent:delete", "删除洗煤出库"),

    /**
     * 配煤入库管理：审批内容
     */
    BLENDINTODEPOT_APPROVAL_CONTENT_INSERT("blendIntodepotApprovalContent:insert", "新增配煤入库"),
    BLENDINTODEPOT_APPROVAL_CONTENT_UPDATE("blendIntodepotApprovalContent:update", "修改配煤入库"),
    BLENDINTODEPOT_APPROVAL_CONTENT_DELETE("blendIntodepotApprovalContent:delete", "删除配煤入库"),

    /**
     * 配煤出库管理：审批内容
     */
    BLENDIOUTPUTDEPOT_APPROVAL_CONTENT_INSERT("blendOutputdepotApprovalContent:insert", "新增配煤出库"),
    BLENDIOUTPUTDEPOT_APPROVAL_CONTENT_UPDATE("blendOutputdepotApprovalContent:update", "修改配煤出库"),
    BLENDIOUTPUTDEPOT_APPROVAL_CONTENT_DELETE("blendOutputdepotApprovalContent:delete", "删除配煤出库"),

    /**
     * 取样：审批内容
     */
    SAMPLE_APPROVAL_CONTENT_INSERT("sampleApprovalContent:insert", "新增取样"),
    SAMPLE_APPROVAL_CONTENT_UPDATE("sampleApprovalContent:update", "修改取样"),
    SAMPLE_APPROVAL_CONTENT_DELETE("sampleApprovalContent:delete", "删除取样"),

    /**
     * 化验：审批内容
     */
    ASSAY_APPROVAL_CONTENT_INSERT("assayApprovalContent:insert", "新增化验"),
    ASSAY_APPROVAL_CONTENT_UPDATE("assayApprovalContent:update", "修改化验"),
    ASSAY_APPROVAL_CONTENT_DELETE("assayApprovalContent:delete", "删除化验"),

    /**
     * 企业注册：审批内容
     */
    ENTERREGISTER_APPROVAL_CONTENT_INSERT("enterRegisterApprovalContent:insert", "新增企业注册"),
    ENTERREGISTER_APPROVAL_CONTENT_UPDATE("enterRegisterApprovalContent:update", "修改企业注册"),
    ENTERREGISTER_APPROVAL_CONTENT_DELETE("enterRegisterApprovalContent:delete", "删除企业注册"),

    /**
     * 合同用印：审批内容
     */
    CONTRACTSEAL_APPROVAL_CONTENT_INSERT("contractSealApprovalContent:insert", "新增合同用印"),
    CONTRACTSEAL_APPROVAL_CONTENT_UPDATE("contractSealApprovalContent:update", "修改合同用印"),
    CONTRACTSEAL_APPROVAL_CONTENT_DELETE("contractSealApprovalContent:delete", "删除合同用印"),

    /**
     * 立项管理：业务状态
     */
    PROJECT_DRAFT("draft","草稿"),
    PROJECT_NOT_APPROVED("not","未立项"),
    //已立项等于进行中
    PROJECT_UNDER_WAY("way","已立项"),
    PROJECT_COMPLETED("completed","已完成"),

    /**
     * 付款管理：业务状态
     */
    PAYMENT_TO_BE("payment_to_be","待付款"),
    PAYMENT_PAID("payment_paid","已付款"),

    /**
     * 收款管理：业务状态
     */
    COLLECTION_TO_BE("collection_to_be","待收款"),
    COLLECTION_RECEIVED("collection_received","已收款"),

    /**
     * 发票类型
     */
    SALE_INVOICE("1","销项发票"),
    PURCHASE_INVOICE("2","进项发票"),

    /**
     * 中储智运运单结算状态
     */
    CMST_ORDER_NOT_APPLY("1","未申请"),
    CMST_ORDER_APPLY("2","已申请"),
    CMST_ORDER_SETTLEMENT("3","已结算"),
    ;

    private String code;
    private String description;

    private AdminCodeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public AdminCodeEnum prase2Code(String code){
        for (AdminCodeEnum argumentCodeEnum : AdminCodeEnum.values()) {
            if (code.equals(argumentCodeEnum.getCode())) {
                return argumentCodeEnum;
            }
        }
        return null;
    }

    public String toString(){
        return this.getCode();
    }
}
