package com.business.common.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.gson.Gson;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.compress.utils.Lists;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class ListUtil {
    /**
     * 把一个json的字符串转换成为一个包含POJO对象的List
     *
     * @param string
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> jsonStringConvertToList(String string, Class<T[]> cls) {
        Gson gson = new Gson();
        T[] array = gson.fromJson(string, cls);
        return Arrays.asList(array);
    }

    /**
     * targetList 需要处理的list
     * targetClass 目标class
     * @return List 处理好的list
     */
    public List listMapParseListObj(List targetList, Class targetClass){
        // 先获取该类的代理对象
        Object obj = null;
        try {
            obj = targetClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        // 将list转为list对象
        for(int i = 0; i< targetList.size(); i++){
            Map map = (Map)targetList.get(i);                  // 获取每个list里面每个map
            Iterator it = map.keySet().iterator();
            while(it.hasNext()){
                String name = it.next().toString();      // 名称 key
                String value = map.get(name).toString(); // 值  value
                try{
                    //取得值的类形
                    Class type = PropertyUtils.getPropertyType(obj, name);
                    if(!StringUtils.isEmpty(type)){
                        PropertyUtils.setProperty(obj, name, ConvertUtils.convert(value, type));
                    }
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }


        // 将list<Map> 转为对象进行返回
        List resListObj = Lists.newArrayList();
        for (Object object : targetList) {
            JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(object));
            resListObj.add(jsonObject.toJavaObject((Type)targetClass));
        }

        //返回封装好的集合
        return resListObj;
    }
}
