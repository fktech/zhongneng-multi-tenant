package com.business.common.utils;

import com.github.dozermapper.core.Mapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * dozer 转换器
 */
@Component
public class BeanUtil {

    @Resource
    Mapper mapper;

    public <T, S> T beanConvert(S source, Class<T> clz) {
        if (source == null) return null;
        return mapper.map(source, clz);
    }

    public void copyBeanConvertObject(Object source, Object object) {
        mapper.map(source, object);
    }

    public <T> void copyBeanConvert(T source, Object object) {
        mapper.map(source, object);
    }

    public <T, S> List<T> listConvert(List<S> source, Class<T> clz) {
        if (source == null) return null;
        List<T> list = new ArrayList<>();
        for (S s : source) {
            list.add(mapper.map(s, clz));
        }
        return list;
    }

    public <T, S> Set<T> setConvert(Set<S> source, Class<T> clz) {
        if (source == null) return null;
        Set<T> set = new TreeSet<>();
        for (S s : source) {
            set.add(mapper.map(s, clz));
        }
        return set;
    }


}
