package com.business.common.utils;

import com.business.common.core.domain.entity.SysUser;
import com.business.common.exception.CustomException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.business.common.constant.HttpStatus;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.exception.ServiceException;

/**
 * 安全服务工具类
 *
 * @author ruoyi
 */
public class SecurityUtils
{
    /**
     * 用户ID
     **/
    public static Long getUserId()
    {
        try
        {
            return getLoginUser().getUserId();
        }
        catch (Exception e)
        {
            throw new ServiceException("获取用户ID异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取部门ID
     **/
    public static Long getDeptId()
    {
        try
        {
            return getLoginUser().getDeptId();
        }
        catch (Exception e)
        {
            throw new ServiceException("获取部门ID异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取用户账户
     **/
    public static String getUsername()
    {
        try
        {
            return getLoginUser().getUsername();
        }
        catch (Exception e)
        {
            throw new ServiceException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取用户
     **/
    public static LoginUser getLoginUser()
    {
        try
        {
            return (LoginUser) getAuthentication().getPrincipal();
        }
        catch (Exception e)
        {
            throw new ServiceException("获取用户信息异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication()
    {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 生成BCryptPasswordEncoder密码
     *
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /**
     * 判断密码是否相同
     *
     * @param rawPassword 真实密码
     * @param encodedPassword 加密后字符
     * @return 结果
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 是否为管理员
     *
     * @param userId 用户ID
     * @return 结果
     */
    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }


    private static final Integer ADMIN_FLAG_YES = 1;

    /**
     * 是否为管理员
     *
     * @param user 用户
     * @return 结果
     */
    public static boolean isComAdmin(SysUser user) {
        return ADMIN_FLAG_YES.equals(user.getAdminFlag());
    }

    /**
     * 判断当前登录者是否为所在公司的管理员
     * @return
     */
    public static boolean isComAdmin() {
        return ADMIN_FLAG_YES.equals(getSysUser().getAdminFlag());
    }

    /**
     * 是否为平台超级管理员
     * @return
     */
    public static boolean isSuperAdmin(SysUser user) {
        return ADMIN_FLAG_YES.equals(user.getSuperAdminFlag());
    }

    /**
     * 登陆者是否为平台超级管理员
     * @return
     */
    public static boolean isSuperAdmin() {
        return ADMIN_FLAG_YES.equals(getSysUser().getSuperAdminFlag());
    }

    /**
     * 获取用户
     **/
    public static SysUser getSysUser() {
        SysUser user = getLoginUser().getUser();
        return getLoginUser().getUser();
    }

    /**
     * 获取用户对应公司ID
     **/
    public static String getCurrComId() {
        try {
            return getLoginUser().getUser().getComId();
        } catch (Exception e) {
            throw new CustomException("获取用户公司信息异常", HttpStatus.UNAUTHORIZED);
        }
    }


    /**
     * 获取用户ID
     **/
    public static Long getCurrUserId() {
        try {
            return getLoginUser().getUser().getUserId();
        } catch (Exception e) {
            throw new CustomException("获取用户ID信息异常", HttpStatus.UNAUTHORIZED);
        }
    }
}
