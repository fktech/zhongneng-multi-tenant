package com.business.common.utils.dingding;

import com.aliyun.dingtalkoauth2_1_0.models.GetAccessTokenResponse;
import com.aliyun.tea.TeaException;

/**
 * 钉钉人脸识别接口对接工具类
 */
public class DingTalkUtil {

    //小世手机注册应用
    public static final String appKey = "dingsywa8xgu2xn8pxgf";
    public static final String appSecret = "-GOWu3JvE9_jo9b2IPHVpIzXSq-lXyu7iHVB-I4OCURr-TBVcf6yOsfOdWloLve1";

    /**
     * 使用 Token 初始化账号Client
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dingtalkoauth2_1_0.Client createClient() throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkoauth2_1_0.Client(config);
    }

    public static String getAccessToken() {
        String accessToken = "";
        try {
            com.aliyun.dingtalkoauth2_1_0.Client client = DingTalkUtil.createClient();
            com.aliyun.dingtalkoauth2_1_0.models.GetAccessTokenRequest getAccessTokenRequest = new com.aliyun.dingtalkoauth2_1_0.models.GetAccessTokenRequest()
                    .setAppKey(appKey)
                    .setAppSecret(appSecret);
            GetAccessTokenResponse accessTokenResponse = client.getAccessToken(getAccessTokenRequest);
            accessToken = accessTokenResponse.getBody().getAccessToken();
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }
        }
        return accessToken;
    }
}
