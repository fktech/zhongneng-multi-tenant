package com.business.common.exception;
/**
 * @author js
 * @version 1.0
 * @date 2022/12/8
 * @since
 */
public interface ErrorCode {
    String getCode();

    Object getDescription();

    Object getMessage();

    Object getMessageOrElseDescription();
}
