package com.business.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @author js
 * @Description: 审批异常实现类
 * @version 1.0
 * @date 2022/12/8
 * @since
 */
@Getter
@Setter
public class VerificationException extends RuntimeException {

    private static final long serialVersionUID = 8013089097491789902L;

    private ExceptionMessage exceptionMessage;

    public VerificationException(ExceptionMessage exceptionMessage) {
        super(exceptionMessage.getMessage()!=null?exceptionMessage.getMessage().toString():"");
        this.exceptionMessage=exceptionMessage;
    }

    public VerificationException(String message) {
        super(message);
        this.exceptionMessage=CoreExceptionMessage.build(message);
    }

    public VerificationException(String message, ExceptionMessage exceptionMessage) {
        super(message);
        this.exceptionMessage=exceptionMessage;
    }

    public VerificationException(String code, Object message) {
        super(message != null ? message.toString() : "");
        this.exceptionMessage=CoreExceptionMessage.build(code,message);
    }

    public VerificationException(ErrorCode errorCode) {
        super(errorCode.getMessageOrElseDescription().toString());
        this.exceptionMessage=CoreExceptionMessage.build(errorCode.getCode(),errorCode.getMessageOrElseDescription());
    }

    public VerificationException(Throwable exception) {
        super(exception);
        this.exceptionMessage=CoreExceptionMessage.build(exception);
    }

    public VerificationException(ExceptionMessage exceptionMessage, Throwable exception) {
        super(exceptionMessage.getMessage()!=null?exceptionMessage.getMessage().toString():exception.getMessage(),exception);
        this.exceptionMessage=exceptionMessage;
    }

    public VerificationException(String message, Throwable exception) {
        super(message,exception);
        this.exceptionMessage=CoreExceptionMessage.build(message);
    }

    public VerificationException(String code, String message, Throwable exception) {
        super(message,exception);
        this.exceptionMessage=CoreExceptionMessage.build(code,message);
    }

}
