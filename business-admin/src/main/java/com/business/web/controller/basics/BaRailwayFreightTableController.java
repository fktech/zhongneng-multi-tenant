package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaRailwayFreightTable;
import com.business.system.service.IBaRailwayFreightTableService;

/**
 * 铁路运费Controller
 *
 * @author ljb
 * @date 2022-11-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/freightTable")
@Api(tags ="铁路运费接口")
public class BaRailwayFreightTableController extends BaseController
{
    @Autowired
    private IBaRailwayFreightTableService baRailwayFreightTableService;

    /**
     * 查询铁路运费列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:freightTable:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询铁路运费列表接口")
    public TableDataInfo list(BaRailwayFreightTable baRailwayFreightTable)
    {
        startPage();
        List<BaRailwayFreightTable> list = baRailwayFreightTableService.selectBaRailwayFreightTableList(baRailwayFreightTable);
        return getDataTable(list);
    }

    /**
     * 导出铁路运费列表
     */
    @PreAuthorize("@ss.hasPermi('admin:freightTable:export')")
    @Log(title = "铁路运费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出铁路运费列表接口")
    public void export(HttpServletResponse response, BaRailwayFreightTable baRailwayFreightTable) throws IOException
    {
        List<BaRailwayFreightTable> list = baRailwayFreightTableService.selectBaRailwayFreightTableList(baRailwayFreightTable);
        ExcelUtil<BaRailwayFreightTable> util = new ExcelUtil<BaRailwayFreightTable>(BaRailwayFreightTable.class);
        util.exportExcel(response, list, "freightTable");
    }

    /**
     * 获取铁路运费详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:freightTable:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取铁路运费详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baRailwayFreightTableService.selectBaRailwayFreightTableById(id));
    }

    /**
     * 新增铁路运费
     */
    @PreAuthorize("@ss.hasPermi('admin:freightTable:add')")
    @Log(title = "铁路运费", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增铁路运费接口")
    public AjaxResult add(@RequestBody BaRailwayFreightTable baRailwayFreightTable)
    {
        Integer result = baRailwayFreightTableService.insertBaRailwayFreightTable(baRailwayFreightTable);
        if(result == 0){
            return AjaxResult.error(0,"线路名称已存在");
        }else if(result == -1){
            return AjaxResult.error(0,"起始点已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改铁路运费
     */
    @PreAuthorize("@ss.hasPermi('admin:freightTable:edit')")
    @Log(title = "铁路运费", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改铁路运费接口")
    public AjaxResult edit(@RequestBody BaRailwayFreightTable baRailwayFreightTable)
    {
        return toAjax(baRailwayFreightTableService.updateBaRailwayFreightTable(baRailwayFreightTable));
    }

    /**
     * 删除铁路运费
     */
    @PreAuthorize("@ss.hasPermi('admin:freightTable:remove')")
    @Log(title = "铁路运费", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除铁路运费接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baRailwayFreightTableService.deleteBaRailwayFreightTableByIds(ids));
    }
}
