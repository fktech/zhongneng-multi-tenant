package com.business.web.controller.oa;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaClaimRelation;
import com.business.system.domain.vo.OaClaimStatVO;
import com.business.system.service.IOaClaimRelationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 报销关联流程Controller
 *
 * @author single
 * @date 2023-12-13
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaRelation")
@Api(tags ="报销关联流程接口")
public class OaClaimRelationController extends BaseController
{
    @Autowired
    private IOaClaimRelationService oaClaimRelationService;

    /**
     * 查询报销关联流程列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询报销关联流程列表接口")
    public TableDataInfo list(OaClaimRelation oaClaimRelation)
    {
        startPage();
        List<OaClaimRelation> list = oaClaimRelationService.selectOaClaimRelationList(oaClaimRelation);
        return getDataTable(list);
    }

    /**
     * 导出报销关联流程列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:export')")
    @Log(title = "报销关联流程", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出报销关联流程列表接口")
    public void export(HttpServletResponse response, OaClaimRelation oaClaimRelation) throws IOException
    {
        List<OaClaimRelation> list = oaClaimRelationService.selectOaClaimRelationList(oaClaimRelation);
        ExcelUtil<OaClaimRelation> util = new ExcelUtil<OaClaimRelation>(OaClaimRelation.class);
        util.exportExcel(response, list, "oaRelation");
    }

    /**
     * 获取报销关联流程详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取报销关联流程详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClaimRelationService.selectOaClaimRelationById(id));
    }

    /**
     * 新增报销关联流程
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:add')")
    @Log(title = "报销关联流程", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增报销关联流程接口")
    public AjaxResult add(@RequestBody OaClaimRelation oaClaimRelation)
    {
        return toAjax(oaClaimRelationService.insertOaClaimRelation(oaClaimRelation));
    }

    /**
     * 修改报销关联流程
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:edit')")
    @Log(title = "报销关联流程", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改报销关联流程接口")
    public AjaxResult edit(@RequestBody OaClaimRelation oaClaimRelation)
    {
        return toAjax(oaClaimRelationService.updateOaClaimRelation(oaClaimRelation));
    }

    /**
     * 删除报销关联流程
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:remove')")
    @Log(title = "报销关联流程", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除报销关联流程接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaClaimRelationService.deleteOaClaimRelationByIds(ids));
    }

    /**
     * 查询日常报销统计
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaRelation:query')")
    @PostMapping(value = "/sumOaClaimRelation")
    @ApiOperation(value = "查询日常报销统计")
    public AjaxResult sumOaClaimRelation(@RequestBody OaClaimRelation oaClaimRelation)
    {
        OaClaimStatVO oaClaimStatVO = oaClaimRelationService.sumOaClaimRelation(oaClaimRelation);
        if(ObjectUtils.isEmpty(oaClaimStatVO)){
            oaClaimStatVO = new OaClaimStatVO();
            oaClaimStatVO.setToBeClaim("");
            oaClaimStatVO.setDoingClaim("");
            oaClaimStatVO.setEndClaim("");
        }
        return AjaxResult.success(oaClaimStatVO);
    }
}
