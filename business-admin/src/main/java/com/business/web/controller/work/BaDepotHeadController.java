package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaCheckedHead;
import com.business.system.domain.BaDepotInventory;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.dto.DepotHeadDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaDepotHead;
import com.business.system.service.IBaDepotHeadService;

/**
 * 出入库记录Controller
 *
 * @author single
 * @date 2023-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/depotHead")
@Api(tags ="出入库记录接口")
public class BaDepotHeadController extends BaseController
{
    @Autowired
    private IBaDepotHeadService baDepotHeadService;

    /**
     * 查询出入库记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:depotHead:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询出入库记录列表接口")
    public TableDataInfo list(BaDepotHead baDepotHead)
    {
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDepotHead> list = baDepotHeadService.selectBaDepotHead(baDepotHead);
        return getDataTable(list);
    }

    /**
     * 导出出入库记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:export')")
    @Log(title = "出入库记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出出入库记录列表接口")
    public void export(HttpServletResponse response, BaDepotHead baDepotHead) throws IOException
    {
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        List<BaDepotHead> list = baDepotHeadService.selectBaDepotHeadList(baDepotHead);
        ExcelUtil<BaDepotHead> util = new ExcelUtil<BaDepotHead>(BaDepotHead.class);
        util.exportExcel(response, list, "depotHead");
    }

    /**
     * 获取出入库记录详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:depotHead:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取出入库记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDepotHeadService.selectBaDepotHeadById(id));
    }

    /**
     * 新增入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:add')")
    @Log(title = "入库记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增入库记录接口")
    public AjaxResult add(@RequestBody BaDepotHead baDepotHead)
    {
        return toAjax(baDepotHeadService.insertBaDepotHead(baDepotHead));
    }

    /**
     * 新增出库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:insertRetrieval')")
    @Log(title = "出库记录", businessType = BusinessType.INSERT)
    @PostMapping(value = "/insertRetrieval")
    @ApiOperation(value = "新增出库记录接口")
    public AjaxResult insertRetrieval(@RequestBody BaDepotHead baDepotHead)
    {
        return toAjax(baDepotHeadService.insertRetrieval(baDepotHead));
    }

    /**
     * 修改出入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:edit')")
    @Log(title = "出入库记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改出入库记录接口")
    public AjaxResult edit(@RequestBody BaDepotHead baDepotHead)
    {
        Integer result = baDepotHeadService.updateBaDepotHead(baDepotHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 修改入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:updateDepotHead')")
    @Log(title = "入库记录", businessType = BusinessType.UPDATE)
    @PostMapping("/updateDepotHead")
    @ApiOperation(value = "修改入库记录接口")
    public AjaxResult updateDepotHead(@RequestBody BaDepotHead baDepotHead)
    {
        Integer result = baDepotHeadService.updateDepotHead(baDepotHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 删除出入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:remove')")
    @Log(title = "出入库记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除出入库记录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDepotHeadService.deleteBaDepotHeadByIds(ids));
    }

    /**
     * 质检
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:inspection')")
    @PostMapping("/inspection")
    @ApiOperation(value = "质检接口")
    public AjaxResult inspection(@RequestBody BaDepotHead baDepotHead)
    {
        Integer result = baDepotHeadService.inspection(baDepotHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }else if(result == -2){
            return AjaxResult.error("入库申请失败");
        }
        return toAjax(result);
    }

    /**
     * 入库撤销
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:into')")
    @GetMapping(value = "/into/revoke/{id}")
    @ApiOperation(value = "入库撤销按钮")
    public AjaxResult intoRevoke(@PathVariable("id") String id)
    {
        Integer result = baDepotHeadService.intoRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 出库撤销
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:revoke')")
    @GetMapping(value = "/output/revoke/{id}")
    @ApiOperation(value = "出库撤销按钮")
    public AjaxResult outputRevoke(@PathVariable("id") String id)
    {
        Integer result = baDepotHeadService.outputRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 出库按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:output')")
    @PostMapping(value = "/output")
    @ApiOperation(value = "出库按钮")
    public AjaxResult output(@RequestBody BaDepotHead baDepotHead){
       Integer result = baDepotHeadService.output(baDepotHead);
       if(result == -1){
           return AjaxResult.error("id不能为空");
       }else if(result == 0){
           return AjaxResult.error("出库失败");
       }
       return toAjax(result);
    }

    @GetMapping("/outputList")
    @ApiOperation(value = "查询加工出库记录列表接口")
    public TableDataInfo outputList(DepotHeadDTO depotHeadDTO)
    {
        //租户ID
        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<DepotHeadDTO> list = baDepotHeadService.outputBaDepotHead(depotHeadDTO);
        return getDataTable(list);
    }

    @GetMapping("/intoList")
    @ApiOperation(value = "查询加工入库记录列表接口")
    public TableDataInfo intoList(DepotHeadDTO depotHeadDTO)
    {
        //租户ID
        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<DepotHeadDTO> list = baDepotHeadService.intoBaDepotHead(depotHeadDTO);
        return getDataTable(list);
    }

    /**
     * 入库清单
     */
    @PostMapping("/inventory")
    @ApiOperation(value = "入库清单")
    public AjaxResult inventory(@RequestBody BaDepotHead baDepotHead){

        return toAjax(baDepotHeadService.inventory(baDepotHead));
    }

    /**
     * 统计出入库
     */
    @GetMapping(value = "/countBaDepotHeadList")
    @ApiOperation(value = "统计出入库")
    public AjaxResult countBaDepotHeadList(BaDepotHead baDepotHead)
    {
        return AjaxResult.success(baDepotHeadService.countBaDepotHeadList(baDepotHead));
    }

    /**
     * 统计出入库明细
     */
    @GetMapping(value = "/depotHeadDetail")
    @ApiOperation(value = "统计出入库明细")
    public TableDataInfo depotHeadDetail(BaDepotHead baDepotHead)
    {
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDepotHead> list = baDepotHeadService.depotHeadDetail(baDepotHead);
        return getDataTable(list);
    }

    /**
     * 智慧驾驶舱统计出入库记录
     */
    @GetMapping(value = "/depotHeadRecord")
    @ApiOperation(value = "统计出入库明细")
    public TableDataInfo depotHeadRecord(BaDepotInventory baDepotInventory)
    {
        //租户ID
        baDepotInventory.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDepotInventory> list = baDepotHeadService.selectBaDepotInventory(baDepotInventory);
        return getDataTable(list);
    }

    /**
     * 查询多租户授权信息出入库列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:depotHead:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息出入库列表")
    public TableDataInfo authorizedList(BaDepotHead baDepotHead)
    {
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDepotHead> list = baDepotHeadService.selectBaDepotHeadAuthorizedList(baDepotHead);
        return getDataTable(list);
    }

    /**
     * 授权出入库
     */
    @PreAuthorize("@ss.hasPermi('admin:depotHead:authorized')")
    @Log(title = "出入库", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权出入库")
    public AjaxResult authorized(@RequestBody BaDepotHead baDepotHead)
    {
        return AjaxResult.success(baDepotHeadService.authorizedBaDepotHead(baDepotHead));
    }
}
