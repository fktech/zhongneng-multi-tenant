package com.business.web.controller.work;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.*;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.CountBaProjectDTO;
import com.business.system.domain.dto.CountContractStartDTO;
import com.business.system.domain.dto.DropDownBox;
import com.business.system.domain.dto.json.StartComboBox;
import com.business.system.service.IBaHiContractStartService;
import com.business.system.service.IBaShippingPlanService;
import com.business.system.service.ISysDictDataService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.service.IBaContractStartService;

/**
 * 合同启动Controller
 *
 * @author ljb
 * @date 2023-03-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/contractStart")
@Api(tags ="合同启动接口")
public class BaContractStartController extends BaseController
{
    @Autowired
    private IBaContractStartService baContractStartService;

    @Autowired
    private IBaShippingPlanService baShippingPlanService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private IBaHiContractStartService baHiContractStartService;

    /**
     * 查询合同启动列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询合同启动列表接口")
    public TableDataInfo list(BaContractStart baContractStart)
    {
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContractStart> list = baContractStartService.selectBaContractStartList(baContractStart);
        return getDataTable(list);
    }

    /**
     * 导出合同启动列表
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:export')")
    @Log(title = "合同启动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出合同启动列表接口")
    public void export(HttpServletResponse response, BaContractStart baContractStart) throws IOException
    {
        List<BaContractStart> list = baContractStartService.selectBaContractStartList(baContractStart);
        ExcelUtil<BaContractStart> util = new ExcelUtil<BaContractStart>(BaContractStart.class);
        util.exportExcel(response, list, "contractStart");
    }

    /**
     * 获取合同启动详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取合同启动详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baContractStartService.selectBaContractStartById(id));
    }

    /**
     * 新增合同启动
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:add')")
    @Log(title = "合同启动", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增合同启动接口")
    public AjaxResult add(@RequestBody BaContractStart baContractStart)
    {
        int result = baContractStartService.insertBaContractStart(baContractStart);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1) {
            return AjaxResult.error(-1,"此时间段内本合同启动已添加计划申报，请勿重复操作");
        }
        return toAjax(result);
    }

    /**
     * 修改合同启动
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:edit')")
    @Log(title = "合同启动", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改合同启动接口")
    public AjaxResult edit(@RequestBody BaContractStart baContractStart)
    {
        int result = baContractStartService.updateBaContractStart(baContractStart);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1) {
            return AjaxResult.error(-1,"此时间段内本合同启动已添加计划申报，请勿重复操作");
        }
        return toAjax(result);
    }

    /**
     * 删除合同启动
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:remove')")
    @Log(title = "合同启动", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除合同启动接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baContractStartService.deleteBaContractStartByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baContractStartService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 新增差额
     * difference
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:difference')")
    @PostMapping("/difference")
    @ApiOperation(value = "新增差额")
    public AjaxResult difference(@RequestBody BaContractStart baContractStart){
        int result = baContractStartService.difference(baContractStart);
        if(result == 0){
            AjaxResult.error(0,"操作失败");
        }
        return toAjax(result);
    }

    /**
     * 合同名称
     */

    @GetMapping("/contractStartName")
    @ApiOperation(value = "合同名称")
    public UR contractStartName(String projectId,String type){
        UR ur = baContractStartService.contractStartName(projectId,type);
        return ur;
    }

    /** 合同序号 */
    @GetMapping("/contractStartSerial")
    @ApiOperation(value = "合同序号")
    public String contractStartSerial(String project){
        String serial = baContractStartService.contractStartSerial(project);
        return serial;
    }

    /**
     * 首页合同启动面板
     */
    @GetMapping("/homeStart")
    @ApiOperation(value = "首页合同启动面板")
    @Anonymous
    public TableDataInfo contractStartName(BaContractStartDTO baContractStartDTO){
        startPage();
        List<BaContractStart> list = baContractStartService.contractStartList(baContractStartDTO);
        return getDataTable(list);
    }

    /**
     * PC端首页合同启动面板统计
     */
    @GetMapping("/homeStartStat")
    @ApiOperation(value = "首页合同启动面板统计")
    @Anonymous
    public UR homeStartStat(CountContractStartDTO countContractStartDTO){
        UR ur = baContractStartService.homeStartStat(countContractStartDTO);
        return ur;
    }

    /**
     * APP首页合同启动面板统计
     */
    @GetMapping("/homeContractStartStat")
    @ApiOperation(value = "首页合同启动面板统计")
    @Anonymous
    public UR homeContractStartStat(BaContractStartDTO baContractStartDTO){
        UR ur = baContractStartService.contractStartListStat(baContractStartDTO);
        return ur;
    }

    /**
     * 首页合同启动面板列表
     */
    @GetMapping("/homeContractStartList")
    @ApiOperation(value = "首页合同启动面板列表")
    @Anonymous
    public TableDataInfo homeContractStartList(BaContractStartDTO baContractStartDTO){
        startPage();
        List<BaContractStart> list = baContractStartService.homeContractStart(baContractStartDTO);
        return getDataTable(list);
    }

    /**
     * 移动端合同启动面板
     */
    @GetMapping("/contractLaunchPanel")
    @ApiOperation(value = "移动端合同启动面板列表")
    public TableDataInfo contractLaunchPanel(BaContractStartDTO baContractStartDTO){
        startPage();
        List<BaContractStart> list = baContractStartService.contractLaunchPanel(baContractStartDTO);
        return getDataTable(list);
    }

    /**
     *  移动端合同启动面板详细信息接口
     */
    @GetMapping(value = "/contractLaunchPanelDetails")
    @ApiOperation(value = "移动端合同启动面板详细信息接口")
    public AjaxResult contractLaunchPanelDetails(String id)
    {
        return AjaxResult.success(baContractStartService.contractLaunchPanelDetails(id));
    }

    /**
     * 移动端判断左侧是否点亮
     */
    @GetMapping(value = "/illuminate")
    @ApiOperation(value = "移动端合同启动面板详细信息接口")
    public UR illuminate(String id)
    {
        return baContractStartService.illuminate(id);
    }

    /**
     * 批量删除发运计划
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:delete')")
    @Log(title = "批量删除发运计划", businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    @ApiOperation(value = "批量删除发运计划")
    public AjaxResult delete(@PathVariable String[] ids)
    {
        return toAjax(baShippingPlanService.deleteBaShippingPlanByIds(ids));
    }

    /**
     * 查询订单列表
     */
    @GetMapping("/orderList")
    @ApiOperation(value = "查询订单列表")
    public TableDataInfo orderList(BaContractStart baContractStart)
    {
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContractStart> list = baContractStartService.selectOrderList(baContractStart);
        return getDataTable(list);
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:addOrder')")
    @Log(title = "合同启动", businessType = BusinessType.INSERT)
    @PostMapping("/addOrder")
    @ApiOperation(value = "新增订单")
    public AjaxResult addOrder(@RequestBody BaContractStart baContractStart)
    {
        int result = baContractStartService.insertOrder(baContractStart);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }
        return toAjax(result);
    }
    /**
     * 编辑订单
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:orderEdit')")
    @Log(title = "编辑订单", businessType = BusinessType.UPDATE)
    @PostMapping("/orderEdit")
    @ApiOperation(value = "编辑订单")
    public AjaxResult orderEdit(@RequestBody BaContractStart baContractStart)
    {
        int result = baContractStartService.updateOrder(baContractStart);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 关联业务
     */
    @PostMapping("/associatedBusiness")
    @ApiOperation(value = "关联业务")
    public AjaxResult associatedBusiness(@RequestBody BaContractStart baContractStart){

        return toAjax(baContractStartService.associatedBusiness(baContractStart));
    }

    /**
     * 合同启动下拉
     */
    @GetMapping("/comboBox")
    @ApiOperation(value = "合同启动下拉")
    public TableDataInfo comboBox()
    {
        //查询合同启动类型
        List<SysDictData> dictData = sysDictDataService.selectDictDataByType("contract_start_type");
        List<StartComboBox> startComboBoxList = new ArrayList<>();
        for (SysDictData sysDictData:dictData) {
            if(sysDictData.getDictValue().equals("5")){
                StartComboBox startComboBox = new StartComboBox();
                BaContractStart baContractStart = new BaContractStart();
                baContractStart.setType(sysDictData.getDictValue());
                //合同启动标准类型
                if(sysDictData.getDictValue().equals("1") || sysDictData.getDictValue().equals("4")||sysDictData.getDictValue().equals("5")){
                    baContractStart.setState("contractStart:pass");
                    baContractStart.setCompletion((long)0);
                }else {
                    baContractStart.setState("order:pass");
                    baContractStart.setContractStatus("2");
                }
                baContractStart.setTenantId(SecurityUtils.getCurrComId());
                //查询合同信息
                List<BaContractStart> contractStarts = baContractStartService.baContractStartList(baContractStart);
                startComboBox.setValue(sysDictData.getDictValue());
                startComboBox.setLabel(sysDictData.getDictLabel());
                startComboBox.setChildren(contractStarts);
                startComboBoxList.add(startComboBox);
            }
        }
        return getDataTable(startComboBoxList);
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:revokeOrder')")
    @GetMapping(value = "/revokeOrder/{id}")
    @ApiOperation(value = "订单撤销按钮")
    public AjaxResult revokeOrder(@PathVariable("id") String id) {
        Integer result = baContractStartService.revokeOrder(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /** 订单完结 **/
    @PreAuthorize("@ss.hasPermi('admin:contractStart:complete')")
    @GetMapping(value = "/complete/{id}")
    @ApiOperation(value = "订单完结按钮")
    public AjaxResult complete(@PathVariable("id") String id) {
        Integer result = baContractStartService.complete(id);
        if (result == 0) {
            return AjaxResult.error("完结操作失败");
        }
        return toAjax(result);
    }

    /**
     * 人员定责下拉
     */
    @GetMapping("/deptUser")
    @ApiOperation(value = "人员定责下拉")
    public List<SysUser> deptUser(){

        return baContractStartService.deptUser();
    }

    /**
     * 合同启动业务数据统计
     */
    @GetMapping("/businessCount")
    @ApiOperation(value = "业务统计")
    public Map<String, BigDecimal> businessCount(String startId){

        return baContractStartService.businessCount(startId);
    }

    /**
     * PC端首页合同启动统计
     */
    @GetMapping("/countContractStartList")
    @ApiOperation(value = "PC端首页合同启动统计")
    public AjaxResult countContractStartList(CountContractStartDTO countContractStartDTO){
        return AjaxResult.success(baContractStartService.countContractStartList(countContractStartDTO));
    }

    /**
     * 弹框下拉
     */
    @GetMapping("/bulletBoxList")
    @ApiOperation(value = "弹框下拉")
    public TableDataInfo bulletBoxList(BaContractStart baContractStart)
    {
        startPage();
        List<BaContractStart> list = baContractStartService.selectContractStartList(baContractStart);
        return getDataTable(list);
    }

    /**
     * 关联已签订合同
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:bindingContract')")
    @PostMapping("/bindingContract")
    @ApiOperation(value = "关联已签订合同")
    public AjaxResult bindingContract(@RequestBody BaContractStart baContractStart) {
        return toAjax(baContractStartService.bindingContract(baContractStart));
    }

    /**
     * 变更合同启动
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:change')")
    @Log(title = "变更合同启动", businessType = BusinessType.UPDATE)
    @PostMapping("/change")
    @ApiOperation(value = "变更合同启动接口")
    public AjaxResult change(@RequestBody BaContractStart baContractStart) throws Exception {
        int result = baContractStartService.changeBaContractStart(baContractStart);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        } else if(result == -1){
            return AjaxResult.error(-3,"未进行任何变更");
        }
        return toAjax(result);
    }

    /**
     * 查询合同启动变更历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:list')")
    @GetMapping("/changeList")
    @ApiOperation(value = "查询合同启动变更历史列表")
    public TableDataInfo changeList(BaHiContractStart baHiContractStart)
    {
        startPage();
        List<BaHiContractStart> list = baHiContractStartService.selectChangeDataBaHiContractStartList(baHiContractStart);
        return getDataTable(list);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    @GetMapping(value = "/change/{id}")
    @ApiOperation(value = "获取当前数据和上一次变更记录对比数据")
    public List<String> selectChangeDataList(@PathVariable String id)
    {
        return baContractStartService.selectChangeDataList(id);
    }

    /**
     * 查询合同启动管理变更记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeDataList")
    @ApiOperation(value = "查询合同启动管理变更记录列表")
    public TableDataInfo changeDataList(BaHiContractStart baHiContractStart)
    {
        startPage();
        List<BaHiContractStart> list = baHiContractStartService.selectChangeDataBaHiContractStartList(baHiContractStart);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 获取合同启动详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:query')")
    @GetMapping(value = "/hi/{id}")
    @ApiOperation(value = "获取合同启动详细信息接口")
    public AjaxResult getHiInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baHiContractStartService.selectBaHiContractStartById(id));
    }

    /**
     * 查询多租户授权信息合同启动列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息合同启动列表")
    public TableDataInfo authorizedList(BaContractStart baContractStart)
    {
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContractStart> list = baContractStartService.selectBaContractStartAuthorizedList(baContractStart);
        return getDataTable(list);
    }

    /**
     * 查询多租户授权信息销售申请列表
     */
    @GetMapping("/authorizedOrderList")
    @ApiOperation(value = "查询多租户授权信息销售申请列表")
    public TableDataInfo authorizedOrderList(BaContractStart baContractStart)
    {
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContractStart> list = baContractStartService.selectOrderAuthorizedList(baContractStart);
        return getDataTable(list);
    }

    /**
     * 授权合同启动
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:authorized')")
    @Log(title = "合同启动", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权合同启动")
    public AjaxResult authorized(@RequestBody BaContractStart baContractStart)
    {
        return AjaxResult.success(baContractStartService.authorizedBaContractStart(baContractStart));
    }

    /**
     *
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractStart:query')")
    @GetMapping(value = "/integration")
    @ApiOperation(value = "获取合同启动详细信息接口")
    public AjaxResult integration(String id)
    {
        return AjaxResult.success(baContractStartService.integration(id));
    }

    /**
     * 上游公司
     */
    @GetMapping(value = "/upstreamCompanies")
    @ApiOperation(value = "上游公司")
    public TableDataInfo upstreamCompanies(String name){
        List<DropDownBox> list = baContractStartService.upstreamCompanies(name);
        return getDataTable(list);
    }
    /**
     * 下游公司
     */
    @GetMapping(value = "/downstreamCompanies")
    @ApiOperation(value = "下游公司")
    public TableDataInfo downstreamCompanies(String type,String name){
        List<DropDownBox> list = baContractStartService.downstreamCompanies(type,name);
        return getDataTable(list);
    }
}
