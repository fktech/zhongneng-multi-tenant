package com.business.web.controller.oa;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.DingTalkClockDTO;
import com.business.common.utils.DateUtils;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaClock;
import com.business.system.domain.OaWorkTimes;
import com.business.system.mapper.OaClockMapper;
import com.business.system.service.DingDingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@CrossOrigin //跨域
@RequestMapping("/dingDingApi")
@Api(tags ="钉钉API")
public class DingDingApi {

    @Autowired
    private DingDingService dingDingService;

    @Autowired
    private OaClockMapper oaClockMapper;

    @GetMapping("/getUpDate")
    @ApiOperation(value = "钉钉获取用户考勤数据")
    public String getUpDate() throws Exception{
        return dingDingService.getUpDate();
    }

    @PostMapping("/getDingTalk")
    @ApiOperation(value = "获取钉钉人脸识别打卡数据")
    public String getDingTalk(@RequestBody DingTalkClockDTO dingTalkClockDTO) throws Exception{
        return dingDingService.getDingTalk(dingTalkClockDTO);
    }

    @GetMapping("/getColumnVal")
    @ApiOperation(value = "获取钉钉考勤报表")
    public String getColumnVal() throws Exception{
        return dingDingService.getColumnVal();
    }

    @GetMapping("/getAttColumns")
    @ApiOperation(value = "获取考勤报表列定义")
    public String getAttColumns() throws Exception{
        return dingDingService.getAttColumns();
    }

    @GetMapping("/timestamp")
    @ApiOperation(value = "时间转换")
    public String timestamp(Long timestamp){
        // 创建一个SimpleDateFormat对象，定义目标格式：HH:mm, MM/dd
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        OaClock clock = new OaClock();
        try {
            Date parse = sdf.parse(DateUtils.timestampToDate(timestamp));
            clock.setClockDate(sdf1.parse(sdf1.format(new Date())));
            clock.setId("123456");
            clock.setClockTime(parse);
            oaClockMapper.insertOaClock(clock);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return DateUtils.timestampToDate(timestamp);
    }

    @GetMapping("/listRecord")
    @ApiOperation(value = "获取打卡详情")
    public String listRecord(List<String> userIds) throws Exception{
        return dingDingService.getListRecord(userIds);
    }


    @GetMapping("/groupAdd")
    @ApiOperation(value = "创建考勤组")
    public String groupAdd(OaAttendanceGroup attendanceGroup) throws Exception{
        return dingDingService.groupAdd(attendanceGroup);
    }



    @GetMapping("/groupDelete")
    @ApiOperation(value = "删除考勤组")
    public String groupDelete(String groupId) throws Exception{
        return dingDingService.groupDelete(groupId);
    }

    @GetMapping("/shiftAdd")
    @ApiOperation(value = "创建班次")
    public String shiftAdd(OaWorkTimes oaWorkTimes) throws Exception{
        return dingDingService.shiftAdd(oaWorkTimes);
    }

    @PostMapping("/shiftDelete")
    @ApiOperation(value = "删除班次")
    public String shiftDelete(Long shiftId) throws Exception{
        return dingDingService.shiftDelete(shiftId);
    }

    @PostMapping("/groupUsersAdd")
    @ApiOperation(value = "批量新增参与考勤人员")
    public String groupUsersAdd() throws Exception{
        return dingDingService.groupUsersAdd();
    }

    @PostMapping("/groupMemberUpdate")
    @ApiOperation(value = "更新参与考勤人员")
    public String groupMemberUpdate(OaAttendanceGroup attendanceGroup) throws Exception{
        return dingDingService.groupMemberUpdate(attendanceGroup);
    }

    @PostMapping("/userCreate")
    @ApiOperation(value = "创建用户")
    public String userCreate(SysUser user) throws Exception{
        return dingDingService.userCreate(user);
    }

    @PostMapping("/userUpdate")
    @ApiOperation(value = "修改用户")
    public String userUpdate(SysUser user) throws Exception{
        return dingDingService.userUpdate(user);
    }

    @GetMapping("/userDelete")
    @ApiOperation(value = "删除用户")
    public String userDelete(String userId) throws Exception{
        return dingDingService.userDelete(userId);
    }

    @PostMapping("/departmentCreate")
    @ApiOperation(value = "创建部门")
    public String departmentCreate(SysDept dept) throws Exception{
        return dingDingService.departmentCreate(dept);
    }

    @PostMapping("/departmentUpdate")
    @ApiOperation(value = "编辑部门")
    public String departmentUpdate(SysDept dept) throws Exception{
        return dingDingService.departmentUpdate(dept);
    }

    @GetMapping("/departmentDelete")
    @ApiOperation(value = "删除部门")
    public String departmentDelete(Long deptId) throws Exception{
        return dingDingService.departmentDelete(deptId);
    }

    @GetMapping("/departmentListSub")
    @ApiOperation(value = "获取部门列表")
    public String departmentDelete() throws Exception{
        return dingDingService.departmentListSub();
    }


}
