package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaCheckedHead;
import com.business.system.service.IBaCheckedHeadService;

/**
 * 盘点Controller
 *
 * @author single
 * @date 2023-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/checkedHead")
@Api(tags ="盘点详情接口")
public class BaCheckedHeadController extends BaseController
{
    @Autowired
    private IBaCheckedHeadService baCheckedHeadService;

    /**
     * 查询盘点列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:checkedHead:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询盘点列表接口")
    public TableDataInfo list(BaCheckedHead baCheckedHead)
    {
        startPage();
        List<BaCheckedHead> list = baCheckedHeadService.selectBaCheckedHeadList(baCheckedHead);
        return getDataTable(list);
    }

    /**
     * 查询盘点列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:checkedHead:list')")
    @GetMapping("/checkedHeadlist")
    @ApiOperation(value = "查询盘点列表接口")
    public TableDataInfo checkedHeadlist(BaCheckedHead baCheckedHead)
    {
        startPage();
        List<BaCheckedHead> list = baCheckedHeadService.selectBaCheckedHeadListByCheckId(baCheckedHead);
        return getDataTable(list);
    }

    /**
     * 导出盘点列表
     */
    @PreAuthorize("@ss.hasPermi('admin:checkedHead:export')")
    @Log(title = "盘点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出盘点列表接口")
    public void export(HttpServletResponse response, BaCheckedHead baCheckedHead) throws IOException
    {
        List<BaCheckedHead> list = baCheckedHeadService.selectBaCheckedHeadList(baCheckedHead);
        ExcelUtil<BaCheckedHead> util = new ExcelUtil<BaCheckedHead>(BaCheckedHead.class);
        util.exportExcel(response, list, "checkedHead");
    }

    /**
     * 获取盘点详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:checkedHead:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取盘点详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCheckedHeadService.selectBaCheckedHeadById(id));
    }

    /**
     * 获取盘点信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:checkedHead:query')")
    @PostMapping(value = "/getInfoDetail")
    @ApiOperation(value = "获取盘点信息")
    public AjaxResult getInfoDetail(@RequestBody BaCheckedHead baCheckedHead)
    {
        return AjaxResult.success(baCheckedHeadService.selectBaCheckedHead(baCheckedHead));
    }

    /**
     * 新增盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checkedHead:add')")
    @Log(title = "盘点", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增盘点接口")
    public AjaxResult add(@RequestBody BaCheckedHead baCheckedHead)
    {
        return toAjax(baCheckedHeadService.insertBaCheckedHead(baCheckedHead));
    }

    /**
     * 修改盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checkedHead:edit')")
    @Log(title = "盘点", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改盘点接口")
    public AjaxResult edit(@RequestBody BaCheckedHead baCheckedHead)
    {
        return toAjax(baCheckedHeadService.updateBaCheckedHead(baCheckedHead));
    }

    /**
     * 删除盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checkedHead:remove')")
    @Log(title = "盘点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除盘点接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCheckedHeadService.deleteBaCheckedHeadByIds(ids));
    }

    /**
     * 删除盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checkedHead:remove')")
    @Log(title = "盘点", businessType = BusinessType.DELETE)
    @DeleteMapping("/removeByCheckedId/{checkedId}")
    @ApiOperation(value = "删除盘点接口")
    public AjaxResult removeByCheckedId(@PathVariable String checkedId)
    {
        return toAjax(baCheckedHeadService.deleteBaCheckedHeadByCheckedId(checkedId));
    }

    /**
     * 统计库存盘点
     */
    @GetMapping(value = "/countBaCheckedHead")
    @ApiOperation(value = "统计库存盘点")
    public AjaxResult countBaCheckedHead(BaCheckedHead baCheckedHead)
    {
        return AjaxResult.success(baCheckedHeadService.countBaCheckedHead(baCheckedHead));
    }
}
