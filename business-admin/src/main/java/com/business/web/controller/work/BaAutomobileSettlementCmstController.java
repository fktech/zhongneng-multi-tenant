package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaAutomobileSettlementCmst;
import com.business.system.service.IBaAutomobileSettlementCmstService;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.core.page.TableDataInfo;

/**
 * 中储智运无车承运汽运结算Controller
 *
 * @author js
 * @date 2023-08-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/settlementCmst")
@Api(tags ="汽运结算接口")
public class BaAutomobileSettlementCmstController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementCmstService baAutomobileSettlementCmstService;

    /**
     * 查询汽运结算列表
     */
  /*  @PreAuthorize("@ss.hasPermi('admin:cmst:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询汽运结算列表接口")
    public TableDataInfo list(BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        startPage();
        List<BaAutomobileSettlementCmst> list = baAutomobileSettlementCmstService.selectBaAutomobileSettlementCmstList(baAutomobileSettlementCmst);
        return getDataTable(list);
    }

    /**
     * 导出汽运结算列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:export')")
    @Log(title = "汽运结算", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出汽运结算列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlementCmst baAutomobileSettlementCmst) throws IOException
    {
        List<BaAutomobileSettlementCmst> list = baAutomobileSettlementCmstService.selectBaAutomobileSettlementCmstList(baAutomobileSettlementCmst);
        ExcelUtil<BaAutomobileSettlementCmst> util = new ExcelUtil<BaAutomobileSettlementCmst>(BaAutomobileSettlementCmst.class);
        util.exportExcel(response, list, "cmst");
    }

    /**
     * 获取汽运结算详细信息
     */
   /* @PreAuthorize("@ss.hasPermi('admin:cmst:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取汽运结算详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementCmstService.selectBaAutomobileSettlementCmstById(id));
    }

    /**
     * 新增汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:add')")
    @Log(title = "汽运结算", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增汽运结算接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        return toAjax(baAutomobileSettlementCmstService.insertBaAutomobileSettlementCmst(baAutomobileSettlementCmst));
    }

    /**
     * 修改汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:edit')")
    @Log(title = "汽运结算", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改汽运结算接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        return toAjax(baAutomobileSettlementCmstService.updateBaAutomobileSettlementCmst(baAutomobileSettlementCmst));
    }

    /**
     * 删除汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:remove')")
    @Log(title = "汽运结算", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除汽运结算接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementCmstService.deleteBaAutomobileSettlementCmstByIds(ids));
    }
}
