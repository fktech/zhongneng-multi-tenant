package com.business.web.controller.work;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.BusinessType;
import com.business.common.exception.VerificationErrorCode;
import com.business.common.exception.VerificationException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.*;
import com.business.system.domain.dto.sourceGoodsDTO;
import com.business.system.mapper.BaAutomobuleStandardMapper;
import com.business.system.mapper.BaShippingOrderMapper;
import com.business.system.mapper.BaTransportAutomobileMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.service.IBaTransportAutomobileService;

/**
 * 汽车运输/货源Controller
 *
 * @author ljb
 * @date 2023-02-17
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/automobile")
@Api(tags ="汽车运输/货源接口")
public class BaTransportAutomobileController extends BaseController
{
    @Autowired
    private IBaTransportAutomobileService baTransportAutomobileService;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    protected BaAutomobuleStandardMapper baAutomobuleStandardMapper;

    @Autowired
    protected BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    /**
     * 查询汽车运输/货源列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:list')")
    @GetMapping("/list")
    @ApiOperation(value = "原货源列表接口")
    public TableDataInfo list(BaTransportAutomobile baTransportAutomobile)
    {
        startPage();
        List<BaTransportAutomobile> list = baTransportAutomobileService.selectBaTransportAutomobileList(baTransportAutomobile);
        return getDataTable(list);
    }

    /**
     * 查询汽车运输/货源列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:list')")
    @GetMapping("/sourceGoodsDTOList")
    @ApiOperation(value = "新货源列表接口")
    public TableDataInfo sourceGoodsDTOList(BaTransportAutomobile baTransportAutomobile)
    {
        startPage();
        List<sourceGoodsDTO> list = baTransportAutomobileService.sourceGoodsDTOList(baTransportAutomobile);
        return getDataTable(list);
    }

    /**
     * 导出汽车运输/货源列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:export')")
    @Log(title = "汽车运输/货源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出汽车运输/货源列表接口")
    public void export(HttpServletResponse response, BaTransportAutomobile baTransportAutomobile) throws IOException
    {
        List<BaTransportAutomobile> list = baTransportAutomobileService.selectBaTransportAutomobileList(baTransportAutomobile);
        ExcelUtil<BaTransportAutomobile> util = new ExcelUtil<BaTransportAutomobile>(BaTransportAutomobile.class);
        util.exportExcel(response, list, "automobile");
    }

    /**
     * 获取汽车运输/货源详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:automobile:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取汽车运输/货源详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baTransportAutomobileService.selectBaTransportAutomobileById(id));
    }

    /**
     * 新增汽车运输/货源
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:add')")
    @Log(title = "汽车运输/货源", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增汽车运输/货源接口")
    public AjaxResult add(@RequestBody BaTransportAutomobile baTransportAutomobile)
    {
        Integer result = baTransportAutomobileService.insertBaTransportAutomobile(baTransportAutomobile);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 昕科推送货源单信息
     * @param jsonObject
     * @return
     */
    @PostMapping("addAutomobile")
    @ApiOperation(value = "新增货源接口")
    @Anonymous
    public AjaxResult addAutomobile(@RequestBody JSONObject jsonObject)
    {

        Integer result = baTransportAutomobileService.insertAutomobile(jsonObject);

        return toAjax(result);
    }

    /**
     * 中储推送货源单信息
     * @param jsonObject
     * @return
     */
    @PostMapping("added")
    @ApiOperation(value = "中储新增货源接口")
    @Anonymous
    public AjaxResult added(@RequestBody JSONObject jsonObject) throws Exception
    {

        Integer result = baTransportAutomobileService.insertCmst(jsonObject);

        return toAjax(result);
    }

    /**
     * 中储推送货源单信息
     * @param jsonObject
     * @return
     */
    @PostMapping("edited")
    @ApiOperation(value = "中储更新货源接口")
    @Anonymous
    public AjaxResult edited(@RequestBody JSONObject jsonObject) throws Exception
    {

        Integer result = baTransportAutomobileService.updateCmst(jsonObject);

        return toAjax(result);
    }

    /**
     * 修改汽车运输/货源
     */
    @PreAuthorize("@ss.hasPermi('admin:automobile:edit')")
    @Log(title = "汽车运输/货源", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改汽车运输/货源接口")
    public AjaxResult edit(@RequestBody BaTransportAutomobile baTransportAutomobile)
    {
        Integer result = baTransportAutomobileService.updateBaTransportAutomobile(baTransportAutomobile);
        if(result == 0){
            return AjaxResult.error(0,"运单编号已存在");
        }
        return toAjax(result);
    }

    /**
     * 昕科推送货源单信息
     * @param jsonObject
     * @return
     */
    @PostMapping("editAutomobile")
    @ApiOperation(value = "修改货源接口")
    @Anonymous
    public AjaxResult editAutomobile(@RequestBody JSONObject jsonObject)
    {

        Integer result = baTransportAutomobileService.updateAutomobile(jsonObject);

        return toAjax(result);
    }

    /**
     * 删除汽车运输/货源
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:remove')")
    @Log(title = "汽车运输/货源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除汽车运输/货源接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baTransportAutomobileService.deleteBaTransportAutomobileByIds(ids));
    }

    /**
     * 昕科删除货源信息
     * @param supplierNo
     * @return
     */
    @PostMapping("deleteAutomobile")
    @ApiOperation(value = "删除汽车运输/货源接口")
    @Anonymous
    public AjaxResult delete(@RequestBody List<String> supplierNo)
    {

        String[] strings = supplierNo.toArray(new String[supplierNo.size()]);

        return toAjax(baTransportAutomobileService.deleteBaTransportAutomobileByIds(strings));
    }

    /**
     * 撤销按钮
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baTransportAutomobileService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 发货按钮
     */
    @PostMapping(value = "/delivery")
    @ApiOperation(value = "发货按钮")
    @Anonymous
    public AjaxResult delivery(@RequestBody JSONObject object){
        System.out.println("****发货**** : " + JSONObject.toJSONString(object));
        String id =  object.getString("vidpic8");
        BaShippingOrder baShippingOrder = new BaShippingOrder();
        baShippingOrder.setSupplierNo(id);
        baShippingOrder.setRcjweight(object.getBigDecimal("rcjweight"));
        baShippingOrder.setId(object.getString("thdno"));
        baShippingOrder.setVidpic2(object.getString("vidpic2"));
        baShippingOrder.setYfratesmoney(object.getBigDecimal("yfratesmoney"));
        baShippingOrder.setDhdate(object.getDate("dhdate"));
        baShippingOrder.setBangcha(object.getBigDecimal("bangcha"));
        //货主运费单价
        baShippingOrder.setRatesprice(object.getBigDecimal("ratesprice"));
        //货物名称
        baShippingOrder.setCoaltypename(object.getString("coaltypename"));
        //是否结算运费
        baShippingOrder.setIsjsratesflag(object.getInteger("isjsratesflag"));
        //实际结算金额
        baShippingOrder.setSfratesmoney(object.getBigDecimal("sfratesmoney"));
        //信息费
        baShippingOrder.setInfocost(object.getBigDecimal("infocost"));
        //其他扣款
        baShippingOrder.setRatesdikoumoney(object.getBigDecimal("ratesdikoumoney"));
        //收货单位
        baShippingOrder.setFhunitname(object.getString("fhunitname"));
        //发货单位
        baShippingOrder.setShunitname(object.getString("shunitname"));
        //驾驶号
        baShippingOrder.setDriverno(object.getString("driverno"));
        //涨吨
        baShippingOrder.setZhangdun(object.getBigDecimal("zhangdun"));
        //磅差（存在）
        baShippingOrder.setBangcha(object.getBigDecimal("bangcha"));
        return toAjax(baTransportAutomobileService.delivery(baShippingOrder));
    }

    /**
     * 收货按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:automobile:receipt')")
    @PostMapping(value = "/receipt")
    @ApiOperation(value = "收货按钮")
    public AjaxResult receipt(@RequestBody BaTransportAutomobile baTransportAutomobile){

        return toAjax(baTransportAutomobileService.receipt(baTransportAutomobile));
    }

    /**
     * 货主签收
     */
    @GetMapping(value = "/sign")
    @ApiOperation(value = "货主签收")
    public TableDataInfo sign(BaShippingOrder baShippingOrder) {
        startPage();
        List<BaShippingOrder> baShippingOrders = baTransportAutomobileService.shippingList(baShippingOrder);
        return getDataTable(baShippingOrders);
    }
    /**
     * 新增货源测试
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:add')")
    //@Log(title = "新增货源测试", businessType = BusinessType.INSERT)
    @PostMapping(value = "/addTransport")
    @ApiOperation(value = "新增货源测试")
    @Anonymous
    public void addTransport(@RequestBody BaTransportAutomobile baTransportAutomobile)
    {
        //货源新增接口
        String url = "http://xays.xkgo.net/prod-api/business/supplier/addSource";

        //汽车运输/货源对象
        baTransportAutomobile.setId("XJ1676794987252");
        baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());
        baTransportAutomobile.setState("1");
        String userId = String.valueOf(baTransportAutomobile.getUserId());

//        查询货源运价
        QueryWrapper<BaAutomobuleStandard> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id", baTransportAutomobile.getId());
        List<BaAutomobuleStandard> baAutomobuleStandards = baAutomobuleStandardMapper.selectList(queryWrapper);
//        设置货源中运价对象集合
        baTransportAutomobile.setRatesItemList(baAutomobuleStandards);
        baTransportAutomobile.setDeliveryTime(baTransportAutomobile.getYfStartDate());
        baTransportAutomobile.setValidityTime(baTransportAutomobile.getYfEndDate());

        List<String> time = new ArrayList<>();
        time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfStartDate()));
        time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfEndDate()));
        baTransportAutomobile.setTime(time);
        baTransportAutomobile.setSupplierId(new Long(10101010));
        baTransportAutomobile.setSupplierName("张非");
//        调用推送信息接口
        System.out.println(JSON.toJSONString(baTransportAutomobile));
        try {
//            String json = restTemplateUtil.postJsonData2(url, JSONObject.toJSONString(baTransportAutomobile));
            restTemplateUtil.postMethod(url,JSONObject.toJSONString(baTransportAutomobile));
        } catch (Exception e) {
            e.printStackTrace();
            throw new VerificationException(VerificationErrorCode.VERIFICATION_PROCESS_ERROR);
        }
    }

    /**
     * 新增发运
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobile:add')")
    @Log(title = "新增发运", businessType = BusinessType.INSERT)
    @PostMapping(value = "/addShippingOrder")
    @ApiOperation(value = "新增发运")
    public void addShippingOrder()
    {
        //货源新增接口
        String url = "http://shiheda.natapp1.cc/automobile/delivery";

        //汽车运输/货源对象
        BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
        baTransportAutomobile.setId("XJ1676704217792");
        baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());

        List<String> time = new ArrayList<>();
        time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfStartDate()));
        time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfEndDate()));
        baTransportAutomobile.setTime(time);
        baTransportAutomobile.setSupplierId(new Long(10101010));
        baTransportAutomobile.setSupplierName("张非");
//        调用推送信息接口
        System.out.println(JSON.toJSONString(baTransportAutomobile));
        try {
//            String json = restTemplateUtil.postJsonData2(url, JSONObject.toJSONString(baTransportAutomobile));
            restTemplateUtil.postMethod(url,JSONObject.toJSONString(baTransportAutomobile));
        } catch (Exception e) {
            e.printStackTrace();
            throw new VerificationException(VerificationErrorCode.VERIFICATION_PROCESS_ERROR);
        }
    }

    /**
     * 获取二维码
     */
    @PostMapping("/addQrCode")
    @ApiOperation(value = "获取二维码")
    @Anonymous
    public AjaxResult addQrCode(@RequestBody JSONObject object)
    {
        BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
        baTransportAutomobile.setId(object.getString("supplierNo"));
        baTransportAutomobile.setQrcode(object.getString("qrcode"));
        return toAjax(baTransportAutomobileService.qrCode(baTransportAutomobile));
    }

    /**
     * 货转按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:automobile:transfer')")
    @PostMapping(value = "/transfer")
    @ApiOperation(value = "货转按钮")
    public AjaxResult transfer(@RequestBody BaTransportAutomobile baTransportAutomobile){

        return toAjax(baTransportAutomobileService.transfer(baTransportAutomobile));
    }

    /**
     * 获取货主信息
     */
    @GetMapping(value = "/gain")
    @ApiOperation(value = "获取货主信息")
    @Anonymous
    public UR gain(){
        UR gain = baTransportAutomobileService.gain();
        return gain;
}

    /**
     * 货源下拉框
     */
    @GetMapping("/listAutomobile")
    @ApiOperation(value = "货源下拉框")
    public TableDataInfo listAutomobile()
    {
        startPage();
        List<BaTransportAutomobile> list = baTransportAutomobileService.listAutomobile();
        return getDataTable(list);
    }

    /**
     * 出入库下拉
     */
    @GetMapping("/listSourceGoods")
    @ApiOperation(value = "出入库下拉")
    public TableDataInfo listSourceGoods(BaTransportAutomobile baTransportAutomobile)
    {
        startPage();
        List<sourceGoodsDTO> list = baTransportAutomobileService.listSourceGoods(baTransportAutomobile);
        return getDataTable(list);
    }

    /**
     * 取样下拉
     */
    @GetMapping("/sampleList")
    @ApiOperation(value = "取样下拉")
    public TableDataInfo sampleList(BaTransportAutomobile baTransportAutomobile)
    {
        startPage();
        List<sourceGoodsDTO> list = baTransportAutomobileService.sampleList(baTransportAutomobile);
        return getDataTable(list);
    }

    /**
     * 线下新增货源信息
     */
    @PostMapping("/sourceOfGoods")
    @ApiOperation(value = "新增汽车运输/货源接口")
    public AjaxResult sourceOfGoods(@RequestBody BaTransportAutomobile baTransportAutomobile)
    {
        Integer result = baTransportAutomobileService.insertSourceOfGoods(baTransportAutomobile);

        return toAjax(result);
    }
}
