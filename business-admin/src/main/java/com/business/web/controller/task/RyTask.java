package com.business.web.controller.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.SysUserByRoleIdDTO;
import com.business.system.domain.dto.SysUserDepDTO;
import com.business.system.domain.vo.DingClockResultVO;
import com.business.system.domain.vo.DingDingVO;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaDailyMapper baDailyMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaAttendanceMapper baAttendanceMapper;

    @Autowired
    private OaHolidayMapper oaHolidayMapper;

    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaWeekdayMapper oaWeekdayMapper;

    @Autowired
    private OaWorkTimesMapper oaWorkTimesMapper;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private OaClockRecordMapper oaClockRecordMapper;

    @Autowired
    private OaEvectionMapper oaEvectionMapper;

    @Autowired
    private OaEvectionTripMapper oaEvectionTripMapper;

    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private OaEgressMapper oaEgressMapper;

    @Autowired
    private OaClockSummaryMapper oaClockSummaryMapper;

    @Autowired
    private OaCalendarMapper oaCalendarMapper;

    @Autowired
    private IOaClockSummaryService  oaClockSummaryService;

    @Autowired
    private IOaAssignedService oaAssignedService;

    @Autowired
    private IOaSupervisingService oaSupervisingService;

    @Autowired
    private OaSupervisingMapper oaSupervisingMapper;

    @Autowired
    private IBaEventsSearchService baEventsSearchService;

    @Autowired
    private IBaOnlineMentService baOnlineMentService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private BaProjectBeforehandMapper baProjectBeforehandMapper;

    @Autowired
    private BaEarlyWarningMapper baEarlyWarningMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private IBaContractStartService baContractStartService;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaMonitorMapper baMonitorMapper;

    @Autowired
    private DingDingService dingDingService;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;


    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(new Date());

        // 获取当前日期
        Calendar calendar1 = Calendar.getInstance();
        // 将日期减1
        calendar1.add(Calendar.DAY_OF_YEAR, -1);
        String previousDay = df.format(calendar1.getTime());

        System.out.println(params+"-------------");
        //分钟定时任务
        if(params.equals("min")){
            try {
                //获取当前是周几
                String week = DateUtils.dateToWeek(queryTime);
                //获取所有的交办数据
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setState("0");
                oaAssigned.setType("1");
                List<OaAssigned> oaAssigneds = oaAssignedService.selectOaAssignedList(oaAssigned);
                for (OaAssigned assigned:oaAssigneds) {
                    //查看督办数据
                    OaSupervising supervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());
                    //判断是否存在回复周期
                    if(StringUtils.isNotEmpty(supervising.getReturningCycle())){
                        String[] split = supervising.getReturningCycle().split(",");
                        for (String s:split) {
                            int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                            int minute = calendar1.get(Calendar.MINUTE);
                            String time = "";
                            if(10 > minute){
                                time = hour+":"+"0"+minute;
                            }else {
                                //当前时间
                                time = hour+":"+minute;
                            }
                            SimpleDateFormat recoveryTime = new SimpleDateFormat("HH:mm");
                            String format = recoveryTime.format(supervising.getRecoveryTime());
                            //当前时间等于回复时间执行
                            if(s.equals(week) && time.equals(format)){
                                //更新逾期时间
                                supervising.setReminderTime(queryTime+" "+time);
                                oaSupervisingMapper.updateOaSupervising(supervising);
                                //推送消息通知交办逾期
                                this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办到期提醒");
                            }
                        }
                    }
                }

                //预立项跟新进度信息
                BaProjectBeforehand baProjectBeforehand = new BaProjectBeforehand();
                baProjectBeforehand.setBeforehandState("1");
                List<BaProjectBeforehand> baProjectBeforehands = baProjectBeforehandMapper.selectBaProjectBeforehand(baProjectBeforehand);
                for (BaProjectBeforehand projectBeforehand:baProjectBeforehands) {
                    //判断是否存在回复周期
                    if(StringUtils.isNotEmpty(projectBeforehand.getReturningCycle())){
                        String[] split = projectBeforehand.getReturningCycle().split(",");
                        for (String s:split) {
                            int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                            int minute = calendar1.get(Calendar.MINUTE);
                            String time = "";
                            if(10 > minute){
                                time = hour+":"+"0"+minute;
                            }else {
                                //当前时间
                                time = hour+":"+minute;
                            }
                            SimpleDateFormat recoveryTime = new SimpleDateFormat("HH:mm");
                            String format = recoveryTime.format(projectBeforehand.getRecoveryTime());
                            //当前时间等于回复时间执行
                            if(s.equals(week) && time.equals(format)){
                                //更新逾期时间
                                projectBeforehand.setReminderTime(queryTime+" "+time);
                                baProjectBeforehandMapper.updateBaProjectBeforehand(projectBeforehand);
                                //推送消息通知交办逾期
                                this.supervisingMessage(projectBeforehand.getUserId().toString(),"请及时更新"+"("+projectBeforehand.getName()+")"+"的项目进度","11",projectBeforehand.getId(),"预立项进度更新提醒");
                            }
                        }
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(params+"每分钟执行一次");
        }
        //小时定时任务
        if(params.equals("hour")){
            //获取当前是周几
            try {
                //String week = DateUtils.dateToWeek(queryTime);
                //获取所有的交办数据
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setState("0");
                oaAssigned.setType("1");
                List<OaAssigned> oaAssigneds = oaAssignedService.selectOaAssignedList(oaAssigned);
                for (OaAssigned assigned:oaAssigneds) {
                    //查看督办数据
                    OaSupervising supervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());
                   /* String[] split = supervising.getReturningCycle().split(",");
                    for (String s:split) {
                        //当前时间时
                        int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                        //回复时间时
                        SimpleDateFormat recoveryTime = new SimpleDateFormat("HH");
                        String format = recoveryTime.format(supervising.getRecoveryTime());
                        //当前时间等于回复时间执行
                        if(s.equals(week) && hour > Integer.valueOf(format)){
                            //推送消息通知交办逾期
                            this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办逾期提醒");
                        }
                    }*/
                    if(StringUtils.isNotEmpty(supervising.getReminderTime())){
                        SimpleDateFormat recoveryTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date parse = recoveryTime.parse(supervising.getReminderTime());
                        int i = parse.compareTo(new Date());
                        //提醒时间早于当前时间提醒
                        if(i < 0){
                            //推送消息通知交办逾期
                            this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办逾期提醒");
                        }

                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(params+"每小时执行一次");
        }
        //合同管理状态修改
        if(params.equals("ry")){
            BaContract baContract = new BaContract();
            List<BaContract> baContracts = baContractMapper.selectBaContractList(baContract);
            for (BaContract baContract1:baContracts) {
                if(baContract1.getEndTime() != null && baContract1.getSigningStatus() != null){
                    if(baContract1.getEndTime().getTime() < DateUtils.getNowDate().getTime()  && baContract1.getSigningStatus().equals("已签")){
                        baContract1.setContractState("已结束");
                        baContractMapper.updateBaContract(baContract1);
                    }
                }
            }
        }
       //每天指定时间8点50执行
        if(params.equals("clock")){
           //查询需要打卡用户
            List<String> list = sysUserMapper.selectUserIdByAssort("2", SecurityUtils.getCurrComId());
            list.stream().forEach(itm ->{
                //查询当天是否有上班打卡记录
                BaAttendance baAttendance = new BaAttendance();
                baAttendance.setEmployeeId(Long.valueOf(itm));
                baAttendance.setQueryTime(queryTime);
                baAttendance.setCheckType("1");
                List<BaAttendance> baAttendances = baAttendanceMapper.selectBaAttendanceList(baAttendance);
                if(baAttendances.size() == 0){
                    //推送消息通知今日未打卡
                    this.insertMessage(itm,"今日还未打卡","4");
                }
            });
        }
        //每天指定时间17点50执行
        if(params.equals("daily")){
            //查询需要提交日报用户
            List<String> list = sysUserMapper.selectUserIdByAssort("2", SecurityUtils.getCurrComId());
            list.stream().forEach(itm ->{
                //查询是否有日报信息
                BaDaily baDaily = new BaDaily();
                baDaily.setQueryTime(queryTime);
                baDaily.setUserId(Long.valueOf(itm));
                List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
                if(baDailies.size() == 0){
                    //推送消息通知今日未提交日报
                    this.insertMessage(itm,"今日还未提交日报","3");
                    System.out.println("++++++++++++"+params);
                }
            });
        }
        //每天凌晨判断是否存在当天日报如果没有,存一条未提交数据
        if(params.equals("dailyDay")){
            //查询需要提交日报用户
            List<String> list = sysUserMapper.selectUserIdByAssort("2", null);
            list.stream().forEach(itm ->{
                //查询是否有日报信息
                BaDaily baDaily = new BaDaily();
                baDaily.setQueryTime(queryTime);
                baDaily.setUserId(Long.valueOf(itm));
                baDaily.setDailyState("1");
                List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
                if(baDailies.size() == 0){
                    baDaily.setTime(DateUtils.getNowDate());
                    baDaily.setId(getRedisIncreID.getId());
                    baDaily.setDailyState("2");
                    baDaily.setType("1");
                    SysUser user = sysUserMapper.selectUserById(baDaily.getUserId());
                    baDaily.setDeptId(user.getDeptId());
                    //租户ID
                    baDaily.setTenantId(user.getComId());
                    try {
                        baDaily.setWeek(DateUtils.dateToWeek(queryTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    baDailyMapper.insertBaDaily(baDaily);
                }
            });
        }
        //每年12月1号执行
        if(params.equals("year")){
            System.out.println("999999999999999");
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);

            String s=DateUtils.sendGet("http://timor.tech/api/holiday/year/"+year+1, null);
            JSONObject jsonObject = JSONObject.parseObject(s);
            JSONObject holiday = jsonObject.getJSONObject("holiday");
            Set<Map.Entry<String,Object>> entrySet = holiday.entrySet();
            for (Map.Entry<String,Object> entry:entrySet) {
                String key = entry.getKey();
                Object value = entry.getValue();
                System.out.println("++++++"+value);
                JSONObject o = (JSONObject) JSON.toJSON(value);
                OaHoliday oaHoliday = JSONObject.toJavaObject((JSONObject) JSON.toJSON(value), OaHoliday.class);
                oaHoliday.setId(getRedisIncreID.getId());
                oaHoliday.setHolidaysDate(o.getString("date"));
                try {
                    String week = DateUtils.dateToWeek(oaHoliday.getHolidaysDate());
                    oaHoliday.setWeek(week);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                oaHoliday.setAffiliationYear(String.valueOf(year+1));
                oaHolidayMapper.insertOaHoliday(oaHoliday);
            }
        }
        //每晚11点执行更改考勤组状态
        if(params.equals("attendance")){
            //立即执行考勤组
            OaAttendanceGroup oaAttendanceGroup = new OaAttendanceGroup();
            oaAttendanceGroup.setState("1");
            List<OaAttendanceGroup> oaAttendanceGroups = oaAttendanceGroupMapper.selectOaAttendanceGroupList(oaAttendanceGroup);
            for (OaAttendanceGroup attendanceGroup:oaAttendanceGroups) {
                //查询原考勤组
                if(StringUtils.isNotEmpty(attendanceGroup.getParentId())){
                    OaAttendanceGroup attendanceGroup1 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(attendanceGroup.getParentId());
                    //现有考勤组部门
                    if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){
                        String[] split1 = attendanceGroup.getAttendanceMembers().split(",");
                    //删除释放部门相关打卡初始化数据
                    if(StringUtils.isNotEmpty(attendanceGroup1.getAttendanceMembers())){
                        //释放对应部门
                        String[] split = attendanceGroup1.getAttendanceMembers().split(",");
                        List<String> compare = compare(split, split1);
                        String[] split2 = compare.toArray(new String[compare.size()]);
                        List<Long> l = Arrays.stream(split2).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                        Long[] deptIds = l.toArray(new Long[l.size()]);
                        //判断是否有需要释放的部门
                        if(deptIds.length > 0){
                            List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, null);
                            for (SysUser user:sysUsers) {
                                OaClock clock = new OaClock();
                                clock.setQueryTime(queryTime);
                                clock.setEmployeeId(user.getUserId());
                                List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                                oaClockList.stream().forEach(itm ->{
                                    oaClockMapper.deleteOaClockById(itm.getId());
                                });
                            }
                        }
                        for (String deptId:split) {
                            //释放原有数据考勤人员
                            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                            dept.setAttendanceLock("0");
                            sysDeptMapper.updateDept(dept);
                        }
                      }
                    }
                    //原人员释放
                    if(StringUtils.isNotEmpty(attendanceGroup1.getNonAttendanceMembers())){
                        String[] split = attendanceGroup1.getNonAttendanceMembers().split(",");
                        for (String userId:split) {
                            //删除对应人员当天打卡数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(Long.valueOf(userId));
                            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                            for (OaClock oaClock:oaClockList) {
                                oaClockMapper.deleteOaClockById(oaClock.getId());
                            }
                            //释放不用打卡用户
                            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                            user.setNeedNot("0");
                            sysUserMapper.updateUser(user);
                        }
                    }
                    //修改原有考勤组状态
                    attendanceGroup1.setState("3");
                    attendanceGroup1.setFlag(1L);
                    oaAttendanceGroupMapper.updateOaAttendanceGroup(attendanceGroup1);
                }
                 //需生效考勤组锁定对应部门
                if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){

                    //锁定对应部门
                    String[] split = attendanceGroup.getAttendanceMembers().split(",");
                    for (String deptId:split) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(attendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }
                //更新无需打卡人员
                if(StringUtils.isNotEmpty(attendanceGroup.getNonAttendanceMembers())){
                    //标记不用打卡用户
                    String[] split = attendanceGroup.getNonAttendanceMembers().split(",");
                    for (String userId:split) {
                        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                        user.setNeedNot("1");
                        sysUserMapper.updateUser(user);
                    }
                }
                //立即生效
                attendanceGroup.setState("0");

                //新考勤组成员生成打卡数据
                //新增打卡缓存数据
                //获取所有考勤组成员
                String[] split = attendanceGroup.getAttendanceMembers().split(",");
                List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                Long[] deptIds = l.toArray(new Long[l.size()]);
                List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, null);
                //无需打卡人员
            /*String nonAttendanceMembers = oaAttendanceGroup.getNonAttendanceMembers();
            String[] split1 = nonAttendanceMembers.split(",");*/
                for (SysUser user:sysUsers) {
                    {
                        //查询打卡记录表中存在今天数据
                        OaClock clock = new OaClock();
                        clock.setQueryTime(queryTime);
                        clock.setEmployeeId(user.getUserId());
                        //clock.setFlag(1L);
                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                        //工作日查询
                        OaWeekday weekday = new OaWeekday();
                        weekday.setRelationId(attendanceGroup.getId());
                        //查询是否必须打卡
                        weekday.setType("2");
                        weekday.setAttendanceStart(queryTime);
                        List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                        if(mustWeekdays.size() > 0){
                            if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                //必须打卡日期
                                //缓存打卡数据
                                if(oaClocks.size() == 0){
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    //如有上班打卡时间或者下班打卡时间不更新
                                    oaClocks.stream().forEach(itm ->{
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                            itm.setEndClock(oaWorkTimes.getGoEndClock());
                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            itm.setClockState("0");
                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                            itm.setEndClock(oaWorkTimes.getOffEndClock());
                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            itm.setClockState("0");
                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                        }
                        //查询是否无需打卡
                        weekday.setType("3");
                        List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                        if(oaWeekdays1.size() > 0){
                            if(oaClocks.size() == 0){
                                //缓存打卡数据
                                OaClock oaClock = new OaClock();
                                oaClock.setId(getRedisIncreID.getId());
                                oaClock.setFlag(1L);
                                oaClock.setClockType("1");
                                oaClock.setType("1");
                                oaClock.setEmployeeId(user.getUserId());
                                //租户ID
                                oaClock.setTenantId(user.getComId());
                                oaClock.setClockState("5");
                                oaClock.setAttendanceType("0");
                                oaClock.setCreateTime(DateUtils.getNowDate());
                                oaClockMapper.insertOaClock(oaClock);
                                //下班打卡数据
                                OaClock oaClock1 = new OaClock();
                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                oaClock1.setId(getRedisIncreID.getId());
                                oaClock1.setClockType("2");
                                oaClockMapper.insertOaClock(oaClock1);
                            }else {
                                oaClocks.stream().forEach(itm ->{
                                    if("1".equals(itm.getClockType())&& itm.getClockTime() == null){
                                        itm.setAttendanceType("0");
                                        itm.setClockState("5");
                                        oaClockMapper.updateOaClock(itm);
                                    }else if("2".equals(itm.getClockType())&& itm.getClockTime() == null){
                                        itm.setAttendanceType("0");
                                        itm.setClockState("5");
                                        oaClockMapper.updateOaClock(itm);
                                    }
                                });
                            }
                        }
                        //必须打卡与无需打卡不存在
                        if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0) {
                            OaHoliday oaHoliday = new OaHoliday();
                            oaHoliday.setHolidaysDate(queryTime);
                            oaHoliday.setHoliday("true");
                            List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                            if(oaHolidays.size() > 0){
                                if(oaClocks.size() == 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType("0");
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    oaClocks.stream().forEach(itm ->{
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                            weekday.setType("1");
                            weekday.setAttendanceStart("");
                            try {
                                //前一天时间转周几
                                weekday.setName(DateUtils.dateToWeek(queryTime));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(oaHolidays.size() == 0){
                                if("1".equals(attendanceGroup.getAttendanceType())){
                                    //固定班制
                                    List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                    OaWeekday oaWeekday = oaWeekdays.get(0);
                                    //获取班次信息
                                    if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                        if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                            OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setType("1");
                                                oaClock.setClockType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                        itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                        itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                        itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                        itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                        itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                        itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                        itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                        itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }else {
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }else if("2".equals(oaAttendanceGroup.getAttendanceType())){
                                    //弹性班制
                                    List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                    //弹性工作日不包含本天设置为休假
                                    if(oaWeekdays.size() == 0){
                                        if(oaClocks.size() == 0){
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            oaClock.setEmployeeId(user.getUserId());
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setClockState("5");
                                            oaClock.setAttendanceType("0");
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            oaClocks.stream().forEach(itm ->{
                                                if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceType("0");
                                                    itm.setClockState("5");
                                                    oaClockMapper.updateOaClock(itm);
                                                }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceType("0");
                                                    itm.setClockState("5");
                                                    oaClockMapper.updateOaClock(itm);
                                                }
                                            });
                                        }
                                    }else {
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        if(oaClocks.size() == 0){
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setEmployeeId(user.getUserId());
                                            oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                            oaClock.setWorkHours(oaWeekday.getWorkHours());
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            oaClocks.stream().forEach(itm ->{
                                                if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                    itm.setWorkHours(oaWeekday.getWorkHours());
                                                    itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    itm.setClockState("0");
                                                    oaClockMapper.updateOaClock(itm);
                                                }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                    itm.setWorkHours(oaWeekday.getWorkHours());
                                                    itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    itm.setClockState("0");
                                                    oaClockMapper.updateOaClock(itm);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        //判断是否有已通过的请假
                        OaLeave leave = new OaLeave();
                        leave.setUserId(user.getUserId());
                        leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                        leave.setStart(queryTime);
                        leave.setEnd(queryTime);
                        List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                        //判断当天是否有外出
                        OaEgress egress = new OaEgress();
                        egress.setUserId(user.getUserId());
                        egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                        egress.setStart(queryTime);
                        egress.setEnd(queryTime);
                        //外出信息
                        List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                        //判断是否有已通过的出差信息
                        OaEvection evection = new OaEvection();
                        evection.setUserId(user.getUserId());
                        evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                        evection.setStartTime(queryTime);
                        evection.setEndTime(queryTime);
                        List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                        //执行类型
                        String executeType = "";
                        //请假为空其余不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                            //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                            if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "egress";
                            }else {
                                executeType = "evection";
                            }
                        }
                        //请假、出差不为空外出为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                            //请假开始与结束都是当天上午或者下午并且外出不能重复
                            if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                    || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                for (OaClock clock1:oaClocks) {
                                    if("1".equals(oaLeaves.get(0).getStartCycle())){
                                        if("1".equals(clock1.getClockType())){
                                            clock1.setClockState("4");
                                        }
                                    }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                        if("2".equals(clock1.getClockType())){
                                            clock1.setClockState("4");
                                        }
                                    }
                                    oaClockMapper.updateOaClock(clock1);
                                }
                            }
                            //外出开始与结束都是当天上午或者下午
                            if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                    || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                for (OaClock clock1:oaClocks) {
                                    if("1".equals(oaEgresses.get(0).getStartCycle())){
                                        if("1".equals(clock1.getClockType())){
                                            clock1.setClockState("10");
                                        }
                                    }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                        if("2".equals(clock1.getClockType())){
                                            clock1.setClockState("10");
                                        }
                                    }
                                    oaClockMapper.updateOaClock(clock1);
                                }
                            }
                            //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }else {
                                executeType = "egress";
                            }
                        }
                        //请假、外出不为空出差为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                            //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }else {
                                executeType = "evection";
                            }
                        }
                        //请假、出差、外出都不为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                            //请假时间在外出时间之后又在出差之后：以请假为准
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }
                            //外出时间在请假时间之后又在出差时间之后
                            if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "egress";
                            }
                            //出差时间在请假时间之后又在出差时间之后
                            if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                executeType = "evection";
                            }
                        }
                        //请假不为空其余为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                            executeType = "leaves";
                        }
                        //请假、外出为空出差不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                            executeType = "evection";
                        }
                        //请假、出差为空外出不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                            executeType = "egress";
                        }

                        //请假信息
                        if("leave".equals(executeType)){
                            if(oaLeaves.size() > 0){
                                OaLeave oaLeave = oaLeaves.get(0);
                                //查看每次行程每一天
                                String[] leaveSplit = oaLeave.getEveryDay().split(",");
                                //数组转list
                                List<String> list1 = Arrays.asList(leaveSplit);
                                if(list1.size() == 1){
                                    //判断时间为今天
                                    //出差时间为一天
                                    if(list1.get(0).equals(queryTime)){
                                        for (OaClock oaClock:oaClocks) {
                                            if("1".equals(oaLeave.getStartCycle())){
                                                //修改上班打卡数据
                                                if("1".equals(oaClock.getClockType())){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaLeave.getStartCycle())){
                                                //修改下班打卡数据
                                                if("2".equals(oaClock.getClockType())){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                            if("1".equals(oaLeave.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("1")){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaLeave.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("2")){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }

                                    }
                                }else {
                                    for (String day:list1) {
                                        if(day.equals(queryTime)){
                                            //拿到第一个
                                            if(day.equals(list1.get(0))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaLeave.getStartCycle())){
                                                        //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }else if("2".equals(oaLeave.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }else if(day.equals(list1.get(list1.size()-1))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaLeave.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaLeave.getEndCycle())){
                                                        //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }else {
                                                //上班下班都要更改
                                                for (OaClock oaClock:oaClocks) {
                                                    //打卡状态为出差
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }


                        //外出信息
                        if("egress".equals(executeType)){
                            if(oaEgresses.size() > 0){
                                OaEgress oaEgress = oaEgresses.get(0);
                                //查看每一天外出
                                String[] egressSplit = oaEgress.getEveryDay().split(",");
                                //数组转list
                                List<String> list1 = Arrays.asList(egressSplit);
                                if(list1.size() == 1){
                                    //判断时间为今天
                                    //出差时间为一天
                                    if(list1.get(0).equals(queryTime)){
                                        for (OaClock oaClock:oaClocks) {
                                            if("1".equals(oaEgress.getStartCycle())){
                                                //修改上班打卡数据
                                                if("1".equals(oaClock.getClockType())){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaEgress.getStartCycle())){
                                                //修改下班打卡数据
                                                if("2".equals(oaClock.getClockType())){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                            if("1".equals(oaEgress.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("1")){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaEgress.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("2")){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }

                                    }
                                }else {
                                    for (String day:list1) {
                                        if(day.equals(queryTime)){
                                            //拿到第一个
                                            if(day.equals(list1.get(0))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEgress.getStartCycle())){
                                                        //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }else if("2".equals(oaEgress.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }else if(day.equals(list1.get(list1.size()-1))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEgress.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEgress.getEndCycle())){
                                                        //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }else {
                                                //上班下班都要更改
                                                for (OaClock oaClock:oaClocks) {
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //出差申请
                        if("evection".equals(executeType)){
                            //拿到最后审批通过数据
                            if(oaEvections.size() > 0 ){
                                OaEvection evection1 = oaEvections.get(0);
                                //查询所有行程
                                QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                queryWrapper.eq("evection_id",evection1.getId());
                                List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                    //查看每次行程每一天
                                    String[] split1 = oaEvectionTrip.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split1);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //出差时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEvectionTrip.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEvectionTrip.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEvectionTrip.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEvectionTrip.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                oaAttendanceGroupMapper.updateOaAttendanceGroup(attendanceGroup);
            }
        }
        //当天凌晨十二点执行插入当天打卡数据
        if(params.equals("oaClock")){
            //查询所有人
            List<String> list = sysUserMapper.selectUserByHumanoid(null);
            for (String userId:list) {
                //获取今天打卡班次
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                if("0".equals(user.getNeedNot())){
                    SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                    if(StringUtils.isNotNull(dept)) {
                        if (!"0".equals(dept.getAttendanceLock())) {
                            //查询考勤组
                            OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                            OaWeekday weekday = new OaWeekday();
                            weekday.setRelationId(oaAttendanceGroup.getId());
                            //查询是否必须打卡
                            weekday.setType("2");
                            //昨天
                            //weekday.setAttendanceStart(previousDay);
                            weekday.setAttendanceStart(queryTime);
                            List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(mustWeekdays.size() > 0){
                                if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                    //必须打卡日期
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                    //昨天数据
                                    //oaClock.setCreateTime(calendar1.getTime());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                    oaClock.setClockState("0");
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                    oaClockMapper.insertOaClock(oaClock1);
                                }
                            }
                            //无需打卡
                            //查询是否无需打卡
                            weekday.setType("3");
                            List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(oaWeekdays1.size() > 0){
                                //缓存打卡数据
                                OaClock oaClock = new OaClock();
                                oaClock.setId(getRedisIncreID.getId());
                                oaClock.setFlag(1L);
                                oaClock.setClockType("1");
                                oaClock.setType("1");
                                oaClock.setEmployeeId(user.getUserId());
                                //租户ID
                                oaClock.setTenantId(user.getComId());
                                oaClock.setClockState("5");
                                oaClock.setAttendanceType("0");
                                //昨天数据
                                //oaClock.setCreateTime(calendar1.getTime());
                                oaClock.setCreateTime(DateUtils.getNowDate());
                                oaClockMapper.insertOaClock(oaClock);
                                //下班打卡数据
                                OaClock oaClock1 = new OaClock();
                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                oaClock1.setId(getRedisIncreID.getId());
                                oaClock1.setClockType("2");
                                oaClockMapper.insertOaClock(oaClock1);
                            }
                            //必须打卡与无需打卡不存在
                            if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0){
                                OaHoliday oaHoliday = new OaHoliday();
                                //昨天数据
                                //oaHoliday.setHolidaysDate(previousDay);
                                oaHoliday.setHolidaysDate(queryTime);
                                oaHoliday.setHoliday("true");
                                List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                                if(oaHolidays.size() > 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType("0");
                                    //昨天数据
                                    //oaClock.setCreateTime(calendar1.getTime());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }
                                //固定工作制打卡
                                weekday.setType("1");
                                weekday.setAttendanceStart("");
                                try {
                                    //前一天时间转周几
                                    //weekday.setName(DateUtils.dateToWeek(previousDay));
                                    weekday.setName(DateUtils.dateToWeek(queryTime));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(oaHolidays.size() == 0){
                                    if ("1".equals(oaAttendanceGroup.getAttendanceType())) {
                                        //固定班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        //获取班次信息
                                        if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                            if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClock.setClockState("0");
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }
                                        }
                                    } else if ("2".equals(oaAttendanceGroup.getAttendanceType())) {
                                        //弹性班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        //弹性工作日不包含本天设置为休假
                                        if(oaWeekdays.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            OaWeekday oaWeekday = oaWeekdays.get(0);
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            oaClock.setEmployeeId(user.getUserId());
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                            oaClock.setWorkHours(oaWeekday.getWorkHours());
                                            //昨天数据
                                            //oaClock.setCreateTime(calendar1.getTime());
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            oaClock.setClockState("0");
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }
                                    }
                                }
                            }
                            OaClock clock = new OaClock();
                            clock.setEmployeeId(user.getUserId());
                            //判断是否有已通过的请假
                            OaLeave leave = new OaLeave();
                            leave.setUserId(Long.valueOf(userId));
                            leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                            leave.setStart(queryTime);
                            leave.setEnd(queryTime);
                            List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                            //判断当天是否有外出
                            OaEgress egress = new OaEgress();
                            egress.setUserId(Long.valueOf(userId));
                            egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                            egress.setStart(queryTime);
                            egress.setEnd(queryTime);
                            //外出信息
                            List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                            //判断是否有已通过的出差信息
                            OaEvection evection = new OaEvection();
                            evection.setUserId(Long.valueOf(userId));
                            evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                            evection.setStartTime(queryTime);
                            evection.setEndTime(queryTime);
                            List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                            //执行类型
                            String executeType = "";
                            //请假为空其余不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }else {
                                    executeType = "evection";
                                }
                            }
                            //请假、外出不为空出差为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                //查询打卡信息
                                clock.setQueryTime(queryTime);
                                //clock.setFlag(1L);
                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                //请假开始与结束都是当天上午或者下午并且外出不能重复
                                if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                        || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaLeaves.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //外出开始与结束都是当天上午或者下午
                                if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                        || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaEgresses.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }else {
                                    executeType = "egresses";
                                }
                            }
                            //请假、出差不为空外出为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }else {
                                    executeType = "evections";
                                }
                            }
                            //请假、出差、外出都不为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //请假时间在外出时间之后又在出差之后：以请假为准
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }
                                //外出时间在请假时间之后又在出差时间之后
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egresses";
                                }
                                //出差时间在请假时间之后又在出差时间之后
                                if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "evections";
                                }
                            }
                            //请假不为空其余为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                                executeType = "leaves";
                            }
                            //请假、外出为空出差不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                executeType = "evections";
                            }
                            //请假、出差为空外出不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                executeType = "evections";
                            }

                            //请假信息
                            if("leave".equals(executeType)){
                                if(oaLeaves.size() > 0){
                                    OaLeave oaLeave = oaLeaves.get(0);
                                    //查看每次行程每一天
                                    String[] split = oaLeave.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //请假时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            //查询打卡信息
                                            clock.setQueryTime(queryTime);
                                            //clock.setFlag(1L);
                                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaLeave.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    //查询打卡信息
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为请假
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaLeave.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    //最后一天做对比
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaLeave.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }


                            //外出信息
                            if("egress".equals(executeType)){
                                if(oaEgresses.size() > 0){
                                    OaEgress oaEgress = oaEgresses.get(0);
                                    //查看每一天外出
                                    String[] split = oaEgress.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //外出时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            //查询打卡信息
                                            clock.setQueryTime(queryTime);
                                            //clock.setFlag(1L);
                                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEgress.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    //查询打卡信息
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaEgress.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    //最后一天做对比
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEgress.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);

                                                        }
                                                    }
                                                }else {
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //出差申请
                            if("evection".equals(executeType)){
                                //拿到最后审批通过数据
                                if(oaEvections.size() > 0 ){
                                    OaEvection evection1 = oaEvections.get(0);
                                    //查询所有行程
                                    QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                    queryWrapper.eq("evection_id",evection1.getId());
                                    List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                    for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                        //查看每次行程每一天
                                        String[] split = oaEvectionTrip.getEveryDay().split(",");
                                        //数组转list
                                        List<String> list1 = Arrays.asList(split);
                                        if(list1.size() == 1){
                                            //判断时间为今天
                                            //出差时间为一天
                                            if(list1.get(0).equals(queryTime)){
                                                //查询打卡信息
                                                clock.setQueryTime(queryTime);
                                                //clock.setFlag(1L);
                                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                    if("1".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("1")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("2")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }

                                            }
                                        }else {
                                            for (String day:list1) {
                                                if(day.equals(queryTime)){
                                                    //拿到第一个
                                                    if(day.equals(list1.get(0))){
                                                        //查询打卡信息
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getStartCycle())){
                                                                //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                            }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                                //修改下班打卡数据
                                                                if("2".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }
                                                        }
                                                    }else if(day.equals(list1.get(list1.size()-1))){
                                                        //最后一天做对比
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getEndCycle())){
                                                                //修改上班打卡数据
                                                                if("1".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                                //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }else {
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        //上班下班都要更改
                                                        for (OaClock oaClock:oaClocks) {
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //每月1号1点半初始化汇总数据
        if(params.equals("month")){
            //日期格式化
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            String month = format.format(new Date());
            List<String> userId = sysUserMapper.selectUserByHumanoid(null);
            for (String id:userId){
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(id));
                //月度汇总
                OaClockSummary oaClockSummary = new OaClockSummary();
                oaClockSummary.setUserId(String.valueOf(user.getUserId()));
                //查看部门信息
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                if(StringUtils.isNotNull(dept)){
                    oaClockSummary.setDeptId(String.valueOf(dept.getDeptId()));
                    if(!"0".equals(dept.getAttendanceLock())){
                        OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                        oaClockSummary.setAttendanceId(attendanceGroup.getId());
                        oaClockSummary.setAttendanceName(attendanceGroup.getAttendanceName());
                    }
                }
                oaClockSummary.setId(getRedisIncreID.getId());
                oaClockSummary.setSummaryDate(month);
                //租户ID
                oaClockSummary.setTenantId(user.getComId());
                oaClockSummaryMapper.insertOaClockSummary(oaClockSummary);
            }
            //OA日历
            String date = month+"-"+"01";
            List<String> everyday = oaClockSummaryMapper.everyday(date);
            for (String day:everyday) {
                OaCalendar oaCalendar = new OaCalendar();
                oaCalendar.setId(getRedisIncreID.getId());
                oaCalendar.setDate(day);
                oaCalendar.setCreateTime(DateUtils.getNowDate());
                oaCalendarMapper.insertOaCalendar(oaCalendar);
            }
        }
        //每天更新打卡汇总数据(1点半执行)
        if(params.equals("clockSummary")){
            oaClockSummaryService.sameMonth();
        }

        //每天凌晨23点59上下班均无打卡数据状态更新为旷工
        if(params.equals("neglectWork")){
            //查询所有人
            List<String> list = sysUserMapper.selectUserByHumanoid(null);
            for (String userId:list) {
                //获取今天打卡班次
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                if("0".equals(user.getNeedNot())){
                    SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                    if(StringUtils.isNotNull(dept)) {
                        if (!"0".equals(dept.getAttendanceLock())) {
                            //查询打卡记录表中存在今天数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(user.getUserId());
                            clock.setFlag(1L);
                            clock.setClockState("0");
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            //判断上下班都未打卡改为旷工状态
                            if(oaClocks.size() == 2){
                                for (OaClock oaClock:oaClocks) {
                                    oaClock.setFlag(0L);
                                    oaClock.setClockState("11");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void jkParams(String params)
    {
        if(params.equals("min")){
            System.out.println("海康定时任务");
            //获取离线监控点
            String camera = baEventsSearchService.cameraList("0", null);
            Object parse1 = JSONObject.parse(camera);
            JSONObject data1 = ((JSONObject) parse1).getJSONObject("data");
            JSONArray list1 = data1.getJSONArray("list");
            System.out.println("+++++++++++++++++"+list1);
            for (Object object:list1) {
                BaOnlineMent indexCode = baOnlineMentService.selectBaOnlineMentById(((JSONObject) object).getString("indexCode"));
                System.out.println("111111111111"+((JSONObject) object).getString("indexCode"));
                if(StringUtils.isNull(indexCode)){
                    //如果掉线数据不存在新增或者是已回复
                    BaOnlineMent baOnlineMent = JSONObject.toJavaObject((JSONObject) JSON.toJSON(object), BaOnlineMent.class);
                    baOnlineMent.setId(((JSONObject) object).getString("indexCode"));
                    baOnlineMent.setCreateTime(DateUtils.getNowDate());
                    baOnlineMentService.insertBaOnlineMent(baOnlineMent);
                }
                //再次离线改变状态
                if(StringUtils.isNotNull(indexCode) && indexCode.getState().equals("1")){
                    indexCode.setState("0");
                    indexCode.setUpdateTime(DateUtils.getNowDate());
                    baOnlineMentService.updateBaOnlineMent(indexCode);
                }
                //更新海康监控信息
                BaMonitor monitor = new BaMonitor();
                monitor.setIndexCode(((JSONObject) object).getString("indexCode"));
                List<BaMonitor> baMonitors = baMonitorMapper.selectBaMonitorList(monitor);
                if(baMonitors.size() > 0){
                    for (BaMonitor baMonitor:baMonitors) {
                        baMonitor.setOnline("0");
                        baMonitorMapper.updateBaMonitor(baMonitor);
                    }
                }
            }
            //获取在线控制点编辑对应离线数据
            BaOnlineMent baOnlineMent1 = new BaOnlineMent();
            baOnlineMent1.setState("0");
            List<BaOnlineMent> baOnlineMents = baOnlineMentService.selectBaOnlineMentList(baOnlineMent1);
            for (BaOnlineMent baOnlineMent:baOnlineMents) {
                String list2 = baEventsSearchService.cameraList("1", new String[]{baOnlineMent.getIndexCode()});
                JSONObject data = ((JSONObject) JSONObject.parse(list2)).getJSONObject("data");
                if(data.getString("total").equals("0") == false){
                    System.out.println("9999999999999"+list2);
                    baOnlineMent.setState("1");
                    baOnlineMent.setUpdateTime(DateUtils.getNowDate());
                    baOnlineMentService.updateBaOnlineMent(baOnlineMent);
                }
            }
        }
    }

    public void hkParams(){
        System.out.println("监控定时任务提醒");
        BaOnlineMent baOnlineMent = new BaOnlineMent();
        baOnlineMent.setState("0");
        List<BaOnlineMent> baOnlineMents = baOnlineMentService.selectBaOnlineMentList(baOnlineMent);
        String content = "";
        for (BaOnlineMent baOnlineMent1:baOnlineMents) {
            SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            content = baOnlineMent1.getRegionName() + "-"+baOnlineMent1.getCn()+"-"+dateFormat.format(baOnlineMent1.getCollectTime())+","+content;
        }
        if(baOnlineMents.size() > 0){
            //查询融资经理用户
            SysUserByRoleIdDTO sysUserByRoleIdDTO = new SysUserByRoleIdDTO();
            sysUserByRoleIdDTO.setRoleId("10");
            List<SysUser> sysUsers = userService.selectUserByRole(sysUserByRoleIdDTO);
            /*sysUserByRoleIdDTO.setRoleId("11");
            List<SysUser> sysUserList = userService.selectUserByRole(sysUserByRoleIdDTO);
            sysUsers.addAll(sysUserList);*/
            String mid = "";
            for (SysUser user:sysUsers) {
                mid = user.getUserId() + "," + mid;
            }
            //生成弹框内容
            BaMessage baMessage = new BaMessage();
            baMessage.setMsId("TZ" + getRedisIncreID.getId());
            baMessage.setMId(mid.substring(0,mid.length() - 1));
            baMessage.setMessContent(content.substring(0,content.length()-1));
            baMessage.setSendTime(DateUtils.getNowDate());
            baMessage.setType("7"); //监控提醒
            baMessageMapper.insertBaMessage(baMessage);
        }
        System.out.println("123132132456465456");
    }

    public void earlyWarning(String params){
        //预警3分钟定时器
        if(params.equals("min")){
            //每分钟计算一次预警信息
            //合同启动维度
            BaEarlyWarning baEarlyWarning = new BaEarlyWarning();
            baEarlyWarning.setBusinessType("2");
            List<BaEarlyWarning> baEarlyWarnings = baEarlyWarningMapper.selectBaEarlyWarningList(baEarlyWarning);
            for (BaEarlyWarning earlyWarning:baEarlyWarnings) {
                //下游最后回款日期(设置为最终结算的数据)
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(earlyWarning.getRelevanceId());
                //查询合同启动对应的结算数据
                QueryWrapper<BaSettlement> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("contract_id", contractStart.getId());
                queryWrapper.eq("state", "settlement:pass");
                queryWrapper.eq("flag", 0);
                queryWrapper.eq("final_mark",1);
                BaSettlement baSettlement = baSettlementMapper.selectOne(queryWrapper);
                if(StringUtils.isNotNull(baSettlement)){
                    //最终回款日期
                    earlyWarning.setFinalPaymentDate(baSettlement.getInitiationTime());
                }else {
                    earlyWarning.setFinalPaymentDate(null);
                }
                //开尾票日期
                QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
                invoiceQueryWrapper.eq("start_id", contractStart.getId());
                invoiceQueryWrapper.eq("invoice_type", 1);
                invoiceQueryWrapper.eq("state", "invoice:pass");
                invoiceQueryWrapper.eq("flag", 0);
                invoiceQueryWrapper.eq("collection_time",1);
                invoiceQueryWrapper.orderByDesc("initiation_time");
                List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
                if(baInvoices.size() > 0){
                    //开尾票时间
                    earlyWarning.setTailTime(baInvoices.get(0).getInitiationTime());
                    //预估回款日期
                    earlyWarning.setEstimatedPaymentDate(baInvoices.get(0).getExpectReturnTime());
                }
                //回款周期上限
                //查询合同启动对应立项信息
                if(StringUtils.isNotEmpty(contractStart.getProjectId())){
                    BaProject baProject = baProjectMapper.selectBaProjectById(contractStart.getProjectId());
                    //回款周期上限
                    earlyWarning.setCycleUpperLimit(Long.valueOf(baProject.getPaymentCycle()));
                }
                //当前回款账期=当天日期-开尾票日期
                Date date = new Date();
                if(null != earlyWarning.getTailTime()){
                    long time = date.getTime() - earlyWarning.getTailTime().getTime();
                    long diffInDays = TimeUnit.MILLISECONDS.toDays(time);
                    earlyWarning.setCurrentAccountPeriod(diffInDays);
                }
                //最终回款账期=下游最终回款日期-中能给下游开尾票日期
                if(null != earlyWarning.getFinalPaymentDate() && null != earlyWarning.getTailTime()){
                    long finalAccountPeriod = earlyWarning.getFinalPaymentDate().getTime() - earlyWarning.getTailTime().getTime();
                    long diffInDays = TimeUnit.MILLISECONDS.toDays(finalAccountPeriod);
                    earlyWarning.setFinalAccountPeriod(diffInDays);
                }else {
                    earlyWarning.setFinalAccountPeriod(0L);
                }
                //认领收款
                BigDecimal applyCollectionAmount = new BigDecimal(0);
                //查询认领金额
                QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                historyQueryWrapper.eq("order_id", contractStart.getId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                for (BaClaimHistory baClaimHistory : baClaimHistories) {
                    //查询认领信息
                    BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                    if(!ObjectUtils.isEmpty(baClaim)) {
                        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                        //判断认领收款审批通过
                        if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState()) && baCollection.getCollectionCategory().equals("1")) {
                            if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                                applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                            }
                        }
                    }
                }
                //付款为退下游货款
                BigDecimal paymentForGoods = new BigDecimal(0);
                BaPayment payment = new BaPayment();
                payment.setStartId(contractStart.getId());
                //payment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
                payment.setOnlyPass("1");
                payment.setType("6");//付款类型
                List<BaPayment> list1 = baPaymentService.selectBaPaymentList(payment);
                for (BaPayment payment1:list1) {
                    if(payment1.getPaymentAmount() != null){
                        paymentForGoods = paymentForGoods.add(payment1.getPaymentAmount());
                    }
                }
                //下游回款总金额：合同启动项下收款为下游货款类型之和+付款为退下游货款类型之和
                BigDecimal decimal = applyCollectionAmount.add(paymentForGoods);
                //系统自动终止条件：下游回款金额=下游最终结算单总金额时
                if(StringUtils.isNotNull(baSettlement)){
                    int i = baSettlement.getSettlementAmount().compareTo(decimal);
                    if(i == 0){
                        earlyWarning.setRepaymentCompleted("1");
                        earlyWarning.setReceivableState("2");
                    }else {
                        earlyWarning.setRepaymentCompleted("0");
                        earlyWarning.setReceivableState("0");
                    }
                }else {
                    earlyWarning.setRepaymentCompleted("0");
                    earlyWarning.setReceivableState("0");
                }
                //应收账款预警状态
                if(null != earlyWarning.getEarlyWarning() && null != earlyWarning.getEstimatedPaymentDate()){
                    if(!earlyWarning.getRepaymentCompleted().equals("1") && !earlyWarning.getReceivableState().equals("2")){
                        //当【预估回款时间-当前时间】大于预警天数时为进行中
                        long l = earlyWarning.getEstimatedPaymentDate().getTime() - date.getTime();
                        long diffInDays = TimeUnit.MILLISECONDS.toDays(l)+1;
                        if(diffInDays > earlyWarning.getEarlyWarning()){
                            earlyWarning.setReceivableState("1");
                        }else {
                            earlyWarning.setReceivableState("0");
                        }
                    }
                }
                //占资账期预警
                //首付款日期（最先审核通过的那一笔付款）
                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                paymentQueryWrapper.eq("start_id", contractStart.getId());
                paymentQueryWrapper.eq("state",AdminCodeEnum.PAYMENT_STATUS_PASS.getCode());
                paymentQueryWrapper.eq("flag",0);
                paymentQueryWrapper.orderByAsc("pass_time");
                List<BaPayment> payments = baPaymentMapper.selectList(paymentQueryWrapper);
                if(payments.size() > 0){
                    earlyWarning.setDownPaymentTime(payments.get(0).getInitiationTime());
                }
                //占资账期上限
                earlyWarning.setAccountUpperLimit(contractStart.getAccountUpperLimit());
                //当前占资账期=当日日期-首付款日期
                if(null != earlyWarning.getDownPaymentTime()){
                    long current = date.getTime() - earlyWarning.getDownPaymentTime().getTime();
                    long diffInDays = TimeUnit.MILLISECONDS.toDays(current);
                    earlyWarning.setCurrentUpperLimit(diffInDays);
                }
                //最终占资账期=下游最终回款日期-中能首付款日期
                if(null != earlyWarning.getDownPaymentTime() && null != earlyWarning.getFinalPaymentDate()){
                    long ultimate = earlyWarning.getFinalPaymentDate().getTime() - earlyWarning.getDownPaymentTime().getTime();
                    long diffInDays = TimeUnit.MILLISECONDS.toDays(ultimate);
                    earlyWarning.setFinalUpperLimit(diffInDays);
                }else {
                    earlyWarning.setFinalUpperLimit(0L);
                }
                //系统自动终止
                if(earlyWarning.getRepaymentCompleted().equals("1")){
                    earlyWarning.setAccountPeriodState("2");
                }
                //占资账期预警状态
                if(null != earlyWarning.getCurrentUpperLimit() && null != earlyWarning.getAccountUpperLimit() && !earlyWarning.getAccountPeriodState().equals("2")){
                     if(earlyWarning.getCurrentUpperLimit() > earlyWarning.getAccountUpperLimit()){
                         earlyWarning.setAccountPeriodState("1");
                     }else {
                         earlyWarning.setAccountPeriodState("0");
                     }
                }
                //发货预警状态
                //查询合同启动信息
                if(!earlyWarning.getDeliveryAlertState().equals("2")){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(earlyWarning.getRelevanceId());
                    //查询相关合同信息
                    //合同信息
                    BaContract contract = baContractMapper.selectBaContractById(baContractStart.getDownstream());
                    if(StringUtils.isNotNull(contract)){
                        //下游合同履约期
                        Date startTime = contract.getStartTime();
                        Date endTime = contract.getEndTime();
                        //查找运输信息
                        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                        transportQueryWrapper.eq("start_id", contractStart.getId());
                        transportQueryWrapper.eq("flag", 0);
                        //transportQueryWrapper.orderByDesc("create_time");
                        List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                        if(baTransports.size() > 0){
                            List<BaCheck> checks = new ArrayList<>();
                            for (BaTransport transport:baTransports) {
                                //发货信息
                                QueryWrapper<BaCheck> queryWrapper0 = new QueryWrapper<>();
                                queryWrapper0.eq("relation_id", transport.getId());
                                queryWrapper0.eq("type", "1");
                                BaCheck baCheck = baCheckMapper.selectOne(queryWrapper0);
                                if(StringUtils.isNotNull(baCheck)){
                                    checks.add(baCheck);
                                }
                            }
                            if(StringUtils.isNotNull(checks)){
                                List<BaCheck> collect = checks.stream().sorted(Comparator.comparing(BaCheck::getId).reversed()).collect(Collectors.toList());
                                BaCheck baCheck = collect.get(0);
                                //发货结束时间
                                Date checkEndTime = baCheck.getCheckEndTime();
                                int i = checkEndTime.compareTo(endTime);
                                if(i > 0){
                                    earlyWarning.setDeliveryAlertState("1");
                                }
                            }
                        }/*else {
                            //履约结束时间小于当前时间
                            int i = endTime.compareTo(DateUtils.getNowDate());
                            if(i < 0){
                                earlyWarning.setDeliveryAlertState("1");
                            }
                        }*/
                    }
                }
                baEarlyWarningMapper.updateBaEarlyWarning(earlyWarning);
            }
            //常规立项维度
            //占资额度预警
            baEarlyWarning = new BaEarlyWarning();
            baEarlyWarning.setBusinessType("1");
            List<BaEarlyWarning> earlyWarnings = baEarlyWarningMapper.selectBaEarlyWarningList(baEarlyWarning);
            for (BaEarlyWarning earlyWarning:earlyWarnings) {
                //立项信息
                BaProject baProject = baProjectMapper.selectBaProjectById(earlyWarning.getRelevanceId());
                //查询立项下所有合同启动
                BaContractStart contractStart = new BaContractStart();
                contractStart.setProjectId(baProject.getId());
                contractStart.setState("contractStart:pass");
                List<BaContractStart> contractStarts = baContractStartMapper.selectBaContractStartList(contractStart);
                //所有付款
                BigDecimal paymentTotal = new BigDecimal(0);
                //所有收款
                BigDecimal collectionTotal = new BigDecimal(0);
                for (BaContractStart baContractStart:contractStarts) {
                    //认领收款
                    BigDecimal applyCollectionAmount = new BigDecimal(0);
                    //查询认领金额
                    QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                    historyQueryWrapper.eq("order_id", baContractStart.getId());
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                    for (BaClaimHistory baClaimHistory : baClaimHistories) {
                        //查询认领信息
                        BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                        if(!ObjectUtils.isEmpty(baClaim)) {
                            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                            //判断认领收款审批通过
                            if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                                if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                                    applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                                }
                            }
                        }
                    }
                    //直接收款金额
                    BigDecimal directCollectionAmount = new BigDecimal(0);
                    QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("start_id",baContractStart.getId());
                    queryWrapper.eq("state",AdminCodeEnum.CLAIM_STATUS_PASS.getCode());
                    queryWrapper.eq("flag",0);
                    List<BaCollection> collections = baCollectionMapper.selectList(queryWrapper);
                    for (BaCollection baCollection:collections) {
                        directCollectionAmount = directCollectionAmount.add(baCollection.getApplyCollectionAmount());
                    }
                    collectionTotal = collectionTotal.add(applyCollectionAmount).add(directCollectionAmount);
                    //所有付款
                    BigDecimal paymentForGoods = new BigDecimal(0);
                    BaPayment payment = new BaPayment();
                    payment.setStartId(baContractStart.getId());
                    //payment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
                    payment.setOnlyPass("1");
                    List<BaPayment> list1 = baPaymentService.selectBaPaymentList(payment);
                    for (BaPayment payment1:list1) {
                        if(payment1.getPaymentAmount() != null){
                            paymentForGoods = paymentForGoods.add(payment1.getPaymentAmount());
                        }
                    }
                    paymentTotal = paymentTotal.add(paymentForGoods);
                }
                //总付款
                earlyWarning.setPaymentTotal(paymentTotal.divide(new BigDecimal(10000),2, RoundingMode.HALF_UP));
                //总收款
                earlyWarning.setCollectionTotal(collectionTotal.divide(new BigDecimal(10000),2, RoundingMode.HALF_UP));
                //实际占资=项目下所有合同启动的所有付款-所有收款
                earlyWarning.setActualCapitalOccupation(earlyWarning.getPaymentTotal().subtract(earlyWarning.getCollectionTotal()));
                //额度上限
                if(null != baProject.getUpperLimit()){
                    earlyWarning.setLimitLimit(baProject.getUpperLimit());
                }
                //占资额度预警状态
                if(null != earlyWarning.getActualCapitalOccupation() && null != earlyWarning.getLimitLimit() && !earlyWarning.getLimitState().equals("2")){
                    int compareTo = earlyWarning.getActualCapitalOccupation().compareTo(earlyWarning.getLimitLimit());
                    if(compareTo > 0){
                        earlyWarning.setLimitState("1");
                    }else {
                        earlyWarning.setLimitState("0");
                    }
                }
                baEarlyWarningMapper.updateBaEarlyWarning(earlyWarning);
            }
            System.out.println("预警定时任务测试");
        }
        if(params.equals("msg")){
            //日期格式化
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            BaEarlyWarning earlyWarning = new BaEarlyWarning();
            List<BaEarlyWarning> earlyWarnings = baEarlyWarningMapper.selectBaEarlyWarningList(earlyWarning);
            for (BaEarlyWarning baEarlyWarning:earlyWarnings) {
                if(baEarlyWarning.getReceivableState().equals("1")){
                   //应收预警消息发送
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baEarlyWarning.getRelevanceId());
                    //业务经理
                    String serviceManager = baContractStart.getServiceManager();
                    Date date = new Date();
                    if(null != baEarlyWarning.getEarlyWarning() && null != baEarlyWarning.getEstimatedPaymentDate()){
                        long l = baEarlyWarning.getEstimatedPaymentDate().getTime() - date.getTime();
                        long l1 = TimeUnit.MILLISECONDS.toDays(l)+1;
                        if(0 > l1){
                            //long l1 = baEarlyWarning.getEarlyWarning() - diffInDays;
                            this.insertMessage(serviceManager,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"已超期"+"，请及时沟通处理！","7");
                            //王煜总userId:17
                            this.insertMessage("17","您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"已超期"+"，请及时沟通处理！","7");
                            //对应运营人员
                            String[] split = baContractStart.getPersonCharge().split(",");
                            for (String userId:split) {
                                this.insertMessage(userId,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"已超期"+"天，请及时沟通处理！","7");
                            }
                        }else {
                            //long l1 = baEarlyWarning.getEarlyWarning() - diffInDays;
                            this.insertMessage(serviceManager,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"距离预计回款截止日期还有"+l1+"天，请及时沟通处理！","7");
                            //王煜总userId:17
                            this.insertMessage("17","您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"距离预计回款截止日期还有"+l1+"天，请及时沟通处理！","7");
                            //对应运营人员
                            String[] split = baContractStart.getPersonCharge().split(",");
                            for (String userId:split) {
                                this.insertMessage(userId,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"距离预计回款截止日期还有"+l1+"天，请及时沟通处理！","7");
                            }
                        }
                    }
                }
                if(baEarlyWarning.getLimitState().equals("1")){
                    //额度预警消息推送
                    BaProject baProject = baProjectMapper.selectBaProjectById(baEarlyWarning.getRelevanceId());
                    //业务经理
                    if(StringUtils.isNotEmpty(baProject.getServiceManager())){
                        if(baProject.getServiceManager().indexOf(",") != -1){
                            String[] serviceManagerArray = baProject.getServiceManager().split(",");
                            String serviceManagerStr = serviceManagerArray[serviceManagerArray.length - 1];
                            if(StringUtils.isNotEmpty(serviceManagerStr)){
                                this.insertMessage(serviceManagerStr,"您有一个项目"+"("+baProject.getName()+")"+"资金占用额度已超出资金额度上限"+"("+baProject.getUpperLimit()+")"+"万元，请及时沟通处理！","9");
                            }
                        }}
                    //王煜总userId:17
                    this.insertMessage("17","您有一个项目"+"("+baProject.getName()+")"+"资金占用额度已超出资金额度上限"+"("+baProject.getUpperLimit()+")"+"万元，请及时沟通处理！","9");
                    //对应运营人员
                    if(StringUtils.isNotEmpty(baProject.getPersonCharge())){
                        String[] split = baProject.getPersonCharge().split(",");
                        for (String userId:split) {
                            this.insertMessage(userId,"您有一个项目"+"("+baProject.getName()+")"+"资金占用额度已超出资金额度上限"+"("+baProject.getUpperLimit()+")"+"万元，请及时沟通处理！","9");
                        }
                    }
                }
                if(baEarlyWarning.getAccountPeriodState().equals("1")){
                    //账期预警消息推送
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baEarlyWarning.getRelevanceId());
                    //业务经理
                    String serviceManager = baContractStart.getServiceManager();
                    this.insertMessage(serviceManager,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"占资账期已超占资账期上限"+"("+baContractStart.getAccountUpperLimit()+")"+"天，请及时沟通处理！","8");
                    //王煜总userId:17
                    this.insertMessage("17","您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"占资账期已超占资账期上限"+"("+baContractStart.getAccountUpperLimit()+")"+"天，请及时沟通处理！","8");
                    //对应运营人员
                    String[] split = baContractStart.getPersonCharge().split(",");
                    for (String userId:split) {
                        this.insertMessage(userId,"您有一笔业务"+"("+baContractStart.getStartShorter()+")"+"占资账期已超占资账期上限"+"("+baContractStart.getAccountUpperLimit()+")"+"天，请及时沟通处理！","8");
                    }
                }
                if(baEarlyWarning.getDeliveryAlertState().equals("1")){
                    //发货预警消息推送
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baEarlyWarning.getRelevanceId());
                    //合同信息
                    BaContract contract = baContractMapper.selectBaContractById(baContractStart.getDownstream());
                    //业务经理
                    String serviceManager = baContractStart.getServiceManager();
                    this.insertMessage(serviceManager,"您有一个合同启动"+"("+baContractStart.getStartShorter()+")"+"已逾期发货"+"("+df.format(contract.getEndTime())+")"+"，请及时沟通处理！","10");
                    //王煜总userId:17
                    this.insertMessage("17","您有一个合同启动"+"("+baContractStart.getStartShorter()+")"+"已逾期发货"+"("+df.format(contract.getEndTime())+")"+"，请及时沟通处理！","10");
                    //对应运营人员
                    String[] split = baContractStart.getPersonCharge().split(",");
                    for (String userId:split) {
                        this.insertMessage(userId,"您有一个合同启动"+"("+baContractStart.getStartShorter()+")"+"已逾期发货"+"("+df.format(contract.getEndTime())+")"+"，请及时沟通处理！","10");
                    }
                }
            }
            System.out.println("预警消息定时任务测试");
        }
    }

    public void dingDing(String params){
        if(params.equals("min")){
            //日期格式化
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String queryTime = df.format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

            List<SysUser> sysUsers = sysUserMapper.dingDingUser(null);
            int index = 50;//初始50 的大小，调用获取打卡详情接口每次最多传50个用户
            DingClockResultVO dingClockResultVO = null;
            List<DingDingVO> dingDingVOS = new ArrayList<>();
            Map<String, List<DingDingVO>> dingDingMap = new HashMap<>();
            if(!CollectionUtils.isEmpty(sysUsers)) {
                //dataSize 代表数据的大小，用于分段截取数据集，每次最大50条
                int dataSize = sysUsers.size();
                for (int i = 0; i < sysUsers.size(); i += 50) {
                    if (i + 50 > sysUsers.size()) {
                        index = sysUsers.size() - i;
                    }
                    List<SysUser> newUserList = sysUsers.subList(i, i + index);
                    List<String> newDingdingUserList = newUserList.stream().map(SysUser::getDingDing) // 获取每个Dingding
                            .collect(Collectors.toList());
                    //获取钉钉打卡数据
                    try {
                        dingClockResultVO = JSON.parseObject(dingDingService.getListRecord(newDingdingUserList), DingClockResultVO.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if ("ok".equals(dingClockResultVO.getErrmsg()) && !ObjectUtils.isEmpty(dingClockResultVO)) {
                        dingDingVOS.addAll(dingClockResultVO.getRecordresult());
                    }
                }
                List<String> newEmployeeList = sysUsers.stream().map(SysUser::getUserId) // 获取每个Dingding
                        .map(String::valueOf)
                        .collect(Collectors.toList());
                Map<Long, String> dingdingUserMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, SysUser::getDingDing));
                //钉钉数据转Map
                if (!CollectionUtils.isEmpty(dingDingVOS)) {
                    for (DingDingVO dingDingVO : dingDingVOS) {
                        if (dingDingMap.containsKey(dingDingVO.getUserId())) {
                            List<DingDingVO> dingDingVOS1 = dingDingMap.get(dingDingVO.getUserId());
                            dingDingVOS1.add(dingDingVO);
                            dingDingMap.put(dingDingVO.getUserId(), dingDingVOS1);
                        } else {
                            List<DingDingVO> dingDingVOS1 = new ArrayList<>();
                            dingDingVOS1.add(dingDingVO);
                            dingDingMap.put(dingDingVO.getUserId(), dingDingVOS1);
                        }
                    }
                }
                if(!CollectionUtils.isEmpty(newEmployeeList)){
                    String[] newEmployeeArray = newEmployeeList.stream().toArray(value -> new String[value]);
                    List<OaClock> list = oaClockMapper.selectOaClockListByCondition(queryTime, newEmployeeArray);
                    if (!CollectionUtils.isEmpty(list)){
                        for (OaClock oaClock : list) {
                            //打卡用户
                            SysUser sysUser = sysUserMapper.selectUserById(oaClock.getEmployeeId());
                            if (oaClock.getClockType().equals("1") && oaClock.getFlag() == 1) {
                                if (!ObjectUtils.isEmpty(dingDingMap)) {
                                    String dingDing = dingdingUserMap.get(oaClock.getEmployeeId());
                                    List<DingDingVO> dingDingVOS1 = dingDingMap.get(dingDing);
                                    if(null != dingDingVOS1){
                                        for (DingDingVO dingDingVO : dingDingVOS1) {
                                            if ("OnDuty".equals(dingDingVO.getCheckType())) {
                                                try {
                                                    //覆盖上班打卡
                                                    oaClock.setClockDate(df.parse(df.format(new Date())));
                                                    //上班打卡时间
                                                    Date parse1 = sdf.parse(DateUtils.timestampToDate(Long.valueOf(dingDingVO.getUserCheckTime())));
                                                    oaClock.setClockTime(parse1);
                                                    //经度
                                                    oaClock.setLongitude(dingDingVO.getUserLongitude());
                                                    //纬度
                                                    oaClock.setLatitude(dingDingVO.getUserLatitude());
                                                    //地址
                                                    oaClock.setAddress(dingDingVO.getUserAddress());
                                                    //判断打卡状态
                                                    if(dingDingVO.getTimeResult().equals("Normal")){
                                                        oaClock.setClockState("1");
                                                    }else if(dingDingVO.getTimeResult().equals("Late")){
                                                        oaClock.setClockState("2");
                                                        //计算上班时间差
                                                        LocalTime startTime = LocalTime.of(oaClock.getInitialTime().getHours(),oaClock.getInitialTime().getMinutes());
                                                        LocalTime endTime = LocalTime.of(oaClock.getClockTime().getHours(),oaClock.getClockTime().getMinutes());
                                                        Duration between = Duration.between(startTime, endTime);
                                                        //迟到时间
                                                        oaClock.setDuration(between.toMinutes());
                                                    }
                                                    //状态
                                                    oaClock.setFlag(0L);
                                                    oaClockMapper.updateOaClock(oaClock);
                                                    //打卡记录表新增数据
                                                    OaClockRecord oaClockRecord = new OaClockRecord();
                                                    BeanUtils.copyBeanProp(oaClockRecord, oaClock);
                                                    oaClockRecord.setId(getRedisIncreID.getId());
                                                    oaClockRecord.setRelevanceId(oaClock.getId());
                                                    oaClockRecord.setClockTime(DateUtils.getNowDate());
                                                    //租户ID
                                                    oaClockRecord.setTenantId(sysUser.getComId());
                                                    oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (oaClock.getClockType().equals("2") && (oaClock.getClockState().equals("0") || oaClock.getClockState().equals("1"))) {
                                //获取钉钉打卡数据
                                if (!ObjectUtils.isEmpty(dingDingMap)) {
                                    try {
                                        if (!ObjectUtils.isEmpty(dingDingMap)) {
                                            String dingDing = dingdingUserMap.get(oaClock.getEmployeeId());
                                            List<DingDingVO> dingDingVOS1 = dingDingMap.get(dingDing);
                                            if(null != dingDingVOS1){
                                                for (DingDingVO dingDingVO : dingDingVOS1) {
                                                    if ("OffDuty".equals(dingDingVO.getCheckType())) {
                                                        //钉钉下班打卡时间
                                                        Date parse1 = sdf.parse(DateUtils.timestampToDate(Long.valueOf(dingDingVO.getUserCheckTime())));
                                                        //打卡时间不相等
                                                        if(null != oaClock.getClockTime()){
                                                            if(parse1.compareTo(oaClock.getClockTime()) != 0){
                                                                //覆盖下班打卡
                                                                oaClock.setClockDate(df.parse(df.format(new Date())));
                                                                //下班打卡时间
                                                                oaClock.setClockTime(parse1);
                                                                //经度
                                                                oaClock.setLongitude(dingDingVO.getUserLongitude());
                                                                //纬度
                                                                oaClock.setLatitude(dingDingVO.getUserLatitude());
                                                                //地址
                                                                oaClock.setAddress(dingDingVO.getUserAddress());
                                                                //判断打卡状态
                                                                if(dingDingVO.getTimeResult().equals("Normal")){
                                                                    oaClock.setClockState("1");
                                                                }else if(dingDingVO.getTimeResult().equals("Early")){
                                                                    oaClock.setClockState("3");
                                                                    //计算上班时间差
                                                                    LocalTime startTime = LocalTime.of(oaClock.getInitialTime().getHours(),oaClock.getInitialTime().getMinutes());
                                                                    LocalTime endTime = LocalTime.of(oaClock.getClockTime().getHours(),oaClock.getClockTime().getMinutes());
                                                                    Duration between = Duration.between(endTime, startTime);
                                                                    //早退时间
                                                                    oaClock.setDuration(between.toMinutes());
                                                                }
                                                                //状态
                                                                oaClock.setFlag(0L);
                                                                oaClockMapper.updateOaClock(oaClock);
                                                                //打卡记录表新增数据
                                                                OaClockRecord oaClockRecord = new OaClockRecord();
                                                                BeanUtils.copyBeanProp(oaClockRecord, oaClock);
                                                                oaClockRecord.setId(getRedisIncreID.getId());
                                                                oaClockRecord.setRelevanceId(oaClock.getId());
                                                                oaClockRecord.setClockTime(DateUtils.getNowDate());
                                                                //租户ID
                                                                oaClockRecord.setTenantId(sysUser.getComId());
                                                                oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
                                                            }
                                                        }else {
                                                            //覆盖下班打卡
                                                            oaClock.setClockDate(df.parse(df.format(new Date())));
                                                            //下班打卡时间
                                                            oaClock.setClockTime(parse1);
                                                            //经度
                                                            oaClock.setLongitude(dingDingVO.getUserLongitude());
                                                            //纬度
                                                            oaClock.setLatitude(dingDingVO.getUserLatitude());
                                                            //地址
                                                            oaClock.setAddress(dingDingVO.getUserAddress());
                                                            //判断打卡状态
                                                            if(dingDingVO.getTimeResult().equals("Normal")){
                                                                oaClock.setClockState("1");
                                                            }else if(dingDingVO.getTimeResult().equals("Early")){
                                                                oaClock.setClockState("3");
                                                                //计算上班时间差
                                                                LocalTime startTime = LocalTime.of(oaClock.getInitialTime().getHours(),oaClock.getInitialTime().getMinutes());
                                                                LocalTime endTime = LocalTime.of(oaClock.getClockTime().getHours(),oaClock.getClockTime().getMinutes());
                                                                Duration between = Duration.between(endTime, startTime);
                                                                //早退时间
                                                                oaClock.setDuration(between.toMinutes());
                                                            }
                                                            //状态
                                                            oaClock.setFlag(0L);
                                                            oaClockMapper.updateOaClock(oaClock);
                                                            //打卡记录表新增数据
                                                            OaClockRecord oaClockRecord = new OaClockRecord();
                                                            BeanUtils.copyBeanProp(oaClockRecord, oaClock);
                                                            oaClockRecord.setId(getRedisIncreID.getId());
                                                            oaClockRecord.setRelevanceId(oaClock.getId());
                                                            oaClockRecord.setClockTime(DateUtils.getNowDate());
                                                            //租户ID
                                                            oaClockRecord.setTenantId(sysUser.getComId());
                                                            oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            System.out.println("钉钉获取数据");
        }
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(String mId, String msgContent,String type) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType(type);
        baMessage.setCreateBy("定时任务");
        baMessage.setCreateTime(DateUtils.getNowDate());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            if(type.equals("3")){
                baMessageService.messagePush(sysUser.getPhoneCode(),"日报提交提醒",msgContent,baMessage.getType(),"","");
            }else if(type.equals("7")){
                baMessageService.messagePush(sysUser.getPhoneCode(),"应收预警提醒",msgContent,baMessage.getType(),"","");
            }else if(type.equals("8")){
                baMessageService.messagePush(sysUser.getPhoneCode(),"账期预警提醒",msgContent,baMessage.getType(),"","");
            }else if(type.equals("9")){
                baMessageService.messagePush(sysUser.getPhoneCode(),"额度预警提醒",msgContent,baMessage.getType(),"","");
            }else if(type.equals("10")){
                baMessageService.messagePush(sysUser.getPhoneCode(),"发货预警提醒",msgContent,baMessage.getType(),"","");
            }
        }
        baMessageMapper.insertBaMessage(baMessage);
    }


    /**
     * 督办消息提醒
     *
     * @return
     */
    private void supervisingMessage(String mId, String msgContent,String type,String businessId,String messTitle) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType(type);
        baMessage.setMessTitle(messTitle);
        baMessage.setCreateBy("定时任务");
        baMessage.setCreateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(businessId)){
            baMessage.setBusinessId(businessId);
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    //两个数组对比拿出不同数据
    public static <T> List<T> compare(T[] t1, T[] t2) {
        List<T> list1 = Arrays.asList(t1);
        List<T> list2 = new ArrayList<T>();
        for (T t : t2) {
            if (!list1.contains(t)) {
                list2.add(t);
            }
        }
        return list2;
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }
}
