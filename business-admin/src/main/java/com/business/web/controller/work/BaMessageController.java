package com.business.web.controller.work;

import java.util.HashMap;
import java.util.List;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaMessage;
import com.business.system.service.IBaMessageService;

/**
 * 系统消息记录Controller
 *
 * @author single
 * @date 2023-01-10
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/message")
@Api(tags ="系统消息记录接口")
public class BaMessageController extends BaseController
{
    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    /**
     * 查询系统消息记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询系统消息记录列表接口")
    public TableDataInfo list(BaMessage baMessage)
    {
        //baMessage.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaMessage> list = baMessageService.selectBaMessageList(baMessage);
        return getDataTable(list);
    }

    /**
     * 导出系统消息记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:message:export')")
    @Log(title = "系统消息记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出系统消息记录列表接口")
    public void export(HttpServletResponse response, BaMessage baMessage) throws IOException
    {
        List<BaMessage> list = baMessageService.selectBaMessageList(baMessage);
        ExcelUtil<BaMessage> util = new ExcelUtil<BaMessage>(BaMessage.class);
        util.exportExcel(response, list, "message");
    }

    /**
     * 获取系统消息记录详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:query')")
    @GetMapping(value = "/{msId}")
    @ApiOperation(value = "获取系统消息记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("msId") String msId)
    {
        return AjaxResult.success(baMessageService.selectBaMessageById(msId));
    }

    /**
     * 新增系统消息记录
     */
    @PreAuthorize("@ss.hasPermi('admin:message:add')")
    @Log(title = "系统消息记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增系统消息记录接口")
    public AjaxResult add(@RequestBody BaMessage baMessage)
    {
        return toAjax(baMessageService.insertBaMessage(baMessage));
    }

    /**
     * 修改系统消息记录
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:edit')")
    @Log(title = "系统消息记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改系统消息记录接口")
    public AjaxResult edit(@RequestBody BaMessage baMessage)
    {
        return toAjax(baMessageService.updateBaMessage(baMessage));
    }

    /**
     * 删除系统消息记录
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:remove')")
    @Log(title = "系统消息记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{msIds}")
    @ApiOperation(value = "删除系统消息记录接口")
    public AjaxResult remove(@PathVariable String[] msIds)
    {
        return toAjax(baMessageService.deleteBaMessageByIds(msIds));
    }

    /**
     * 清空
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:empty')")
    @Log(title = "清空", businessType = BusinessType.DELETE)
    @DeleteMapping("/empty")
    @ApiOperation(value = "清空")
    public AjaxResult empty(){
        Integer result = baMessageService.empty();
        if(result == 0){
            return AjaxResult.error("用户id不能为空");
        }
        return toAjax(result);
    }

    /**
     * 已读
     */
    //@PreAuthorize("@ss.hasPermi('admin:message:read')")
    @Log(title = "已读", businessType = BusinessType.UPDATE)
    @GetMapping("/read")
    @ApiOperation(value = "已读")
    public AjaxResult read(){
        Integer result = baMessageService.read();
        return toAjax(result);
    }

    /**
     * 批量已读
     */
    @GetMapping("/batchRead/{msIds}")
    @ApiOperation(value = "批量已读")
    public AjaxResult batchRead(@PathVariable String[] msIds){
        Integer result = baMessageService.batchRead(msIds);
        return toAjax(result);
    }

    /**
     * 消息弹框
     */
    @GetMapping(value = "/select")
    @ApiOperation(value = "弹框获取消息")
    public AjaxResult getMessage()
    {
        BaMessage baMessage = baMessageService.selectBaMessage();
        if(StringUtils.isNull(baMessage)){
            return AjaxResult.error(0,"数据不存在");
        }
        return AjaxResult.success(baMessageService.selectBaMessage());
    }

    /**
     * 更新手机标识
     */
    @PostMapping("/phoneCode")
    @ApiOperation(value = "登录成功后更新手机标识")
    @Anonymous
    public AjaxResult phoneCode(@RequestBody BaMessage baMessage)
    {
        return toAjax(baMessageService.phoneCode(baMessage));
    }

    /**
     * 手机标识查询消息
     */
    @GetMapping(value = "/selectPhoneCode")
    @ApiOperation(value = "手机标识查询消息")
    @Anonymous
    public TableDataInfo selectPhoneCode(String phoneCode)
    {
        startPage();
        List<BaMessage> list = baMessageService.selectPhoneCode(phoneCode);
        return getDataTable(list);
    }

    /**
     * 消息推送
     */
    /*@PostMapping(value = "messagePush")
    @ApiOperation(value = "消息推送")
    @Anonymous
    public String messagePush(){
        String url = "https://fc-mp-dcaa57da-2be2-468c-8dce-c85f485a2235.next.bspapp.com/requsetPush/pushApi";
        //Map<String, String> map = new HashMap<>();
        JSONObject paramMap = new JSONObject();
        Map<String, String> map = new HashMap<>();
        map.put("type","通知公告");
        map.put("id","id");
        paramMap.put("cid", "c4cc001d28af48c4ab82de232000de14");
        paramMap.put("title", "消息提醒推送");
        paramMap.put("content", "您有一条消息正在发送");
        paramMap.put("payload",map);
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(paramMap));
        return "1234536";
    }*/

}
