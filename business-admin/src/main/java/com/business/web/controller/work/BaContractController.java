package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaMaterialStock;
import com.business.system.domain.BaProject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaContract;
import com.business.system.service.IBaContractService;
/**
 * 合同Controller
 *
 * @author ljb
 * @date 2022-12-07
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/contract")
@Api(tags ="合同接口")
public class BaContractController extends BaseController {
    @Autowired
    private IBaContractService baContractService;

    /**
     * 查询合同列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contract:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询合同列表接口")
    public TableDataInfo list(BaContract baContract) {
        //租户ID
        baContract.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContract> list = baContractService.selectBaContractList(baContract);
        return getDataTable(list);
    }

    /**
     * 查询所有合同列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contract:list')")
    @GetMapping("/listAll")
    @ApiOperation(value = "查询所有合同列表")
    public AjaxResult listAll(BaContract baContract) {
        return AjaxResult.success(baContractService.selectBaContractListAll(baContract));
    }

    /**
     * 导出合同列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contract:export')")
    @Log(title = "合同", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出合同列表接口")
    public void export(HttpServletResponse response, BaContract baContract) throws IOException {
        //租户ID
        baContract.setTenantId(SecurityUtils.getCurrComId());
        List<BaContract> list = baContractService.selectBaContractList(baContract);
        ExcelUtil<BaContract> util = new ExcelUtil<BaContract>(BaContract.class);
        util.exportExcel(response, list, "contract");
    }

    /**
     * 获取合同详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:contract:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取合同详细信息接口")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(baContractService.selectBaContractById(id));
    }

    /**
     * 新增合同
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:add')")
    @Log(title = "合同", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增合同接口")
    public AjaxResult add(@RequestBody BaContract baContract) {
       Integer result = baContractService.insertBaContract(baContract);
       if(result == 0){
           return AjaxResult.error(0,"启动流程失败");
       }else if(result == -1){
           return AjaxResult.error(-1,"合同编码不能重复");
       }else if(result == -2){
           return AjaxResult.error(-2,"合同名称不能重复");
       }
        return toAjax(result);
    }

    /**
     * 修改合同
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:edit')")
    @Log(title = "合同", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改合同接口")
    public AjaxResult edit(@RequestBody BaContract baContract) {
        Integer result = baContractService.updateBaContract(baContract);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -2){
            return AjaxResult.error(-2,"合同名称不能重复");
        }else if(result == -1){
            return AjaxResult.error(-1,"合同编号不能重复");
        }
        return toAjax(result);
    }

    /**
     * 签订按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:signing')")
    @PostMapping("/signing")
    @ApiOperation(value = "签订按钮")
    public AjaxResult signing(@RequestBody BaContract baContract) {
        return toAjax(baContractService.signing(baContract));
    }

    /**
     * 删除合同
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:remove')")
    @Log(title = "合同", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除合同接口")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(baContractService.deleteBaContractByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baContractService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
       return toAjax(result);
    }

    /**
     * 归档附件
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:query')")
    @GetMapping(value = "/annex/{id}")
    @ApiOperation(value = "归档附件接口")
    public AjaxResult getAnnexList(@PathVariable("id") String id) {
        return AjaxResult.success(baContractService.getAnnexList(id));
    }

    /**
     * 合同邮寄
     */
    @PostMapping("/postOff")
    @ApiOperation(value = "合同邮寄")
    @Anonymous
    public AjaxResult postOff(@RequestBody BaContract baContract) {

        return toAjax(baContractService.postOff(baContract));
    }

    /**
     * 合同统计
     */
    @GetMapping("/contractCount")
    @ApiOperation(value = "合同统计")
    public UR counts(){
        UR ur = baContractService.contractCount();
        return ur;
    }

    /**
     * 绑定合同启动
     * binding
     */
     @PreAuthorize("@ss.hasPermi('admin:contract:bindingStart')")
     @PostMapping("/bindingStart")
     @ApiOperation(value = "绑定合同启动")
     public AjaxResult bindingStart(@RequestBody BaContract baContract) {
         return toAjax(baContractService.bindingStart(baContract));
     }

    /**
     * 关联多个模块
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:association')")
    @PostMapping("/association")
    @ApiOperation(value = "关联多个模块")
    public AjaxResult association(@RequestBody BaContract baContract) {
        return toAjax(baContractService.association(baContract));
    }

    /**
     * 已签合同解绑
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:unbinding')")
    @PostMapping("/unbinding")
    @ApiOperation(value = "已签合同解绑")
    public AjaxResult unbinding(@RequestBody BaContract baContract) {
        return toAjax(baContractService.unbinding(baContract));
    }

    /**
     * 统计合同数量
     */
    @GetMapping("/countBaContractList")
    @ApiOperation(value = "统计合同数量")
    public AjaxResult countBaContractList(BaContract baContract)
    {
        //租户ID
        baContract.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(baContractService.countBaContractList(baContract));
    }

    /**
     * 查询多租户授权信息合同列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contract:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息合同列表")
    public TableDataInfo authorizedList(BaContract baContract) {
        //租户ID
        baContract.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContract> list = baContractService.selectBaContractAuthorizedList(baContract);
        return getDataTable(list);
    }

    /**
     * 授权合同管理
     */
    @PreAuthorize("@ss.hasPermi('admin:contract:authorized')")
    @Log(title = "合同管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权合同管理")
    public AjaxResult authorized(@RequestBody BaContract baContract)
    {
        return AjaxResult.success(baContractService.authorizedBaContract(baContract));
    }

}
