package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDepotHead;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaPoundRoom;
import com.business.system.service.IBaPoundRoomService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 磅房数据Controller
 *
 * @author ljb
 * @date 2023-09-04
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/poundRoom")
@Api(tags ="磅房数据接口")
public class BaPoundRoomController extends BaseController
{
    @Autowired
    private IBaPoundRoomService baPoundRoomService;

    /**
     * 查询磅房数据列表
     */
   /* @PreAuthorize("@ss.hasPermi('admin:poundRoom:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询磅房数据列表接口")
    public TableDataInfo list(BaPoundRoom baPoundRoom)
    {
        //租户ID
        baPoundRoom.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaPoundRoom> list = baPoundRoomService.selectBaPoundRoomList(baPoundRoom);
        return getDataTable(list);
    }

    /**
     * 导出磅房数据列表
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:export')")
    @Log(title = "磅房数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出磅房数据列表接口")
    public void export(HttpServletResponse response, BaPoundRoom baPoundRoom) throws IOException
    {
        //租户ID
        baPoundRoom.setTenantId(SecurityUtils.getCurrComId());
        List<BaPoundRoom> list = baPoundRoomService.selectBaPoundRoomList(baPoundRoom);
        ExcelUtil<BaPoundRoom> util = new ExcelUtil<BaPoundRoom>(BaPoundRoom.class);
        util.exportExcel(response, list, "poundRoom");
    }

    @PreAuthorize("@ss.hasPermi('admin:poundRoom:importTemplate')")
    @PostMapping("/importTemplate")
    @ApiOperation(value = "模板")
    @Anonymous
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<BaPoundRoom> util = new ExcelUtil<BaPoundRoom>(BaPoundRoom.class);
        util.importTemplateExcel(response, "磅房数据");
    }

    /**
     * 导磅房位信息
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:importData')")
    @PostMapping("/importData")
    @ResponseBody
    @ApiOperation(value = "导入")
    public AjaxResult importData(@RequestPart("file") MultipartFile file, boolean updateSupport) throws Exception{
        ExcelUtil<BaPoundRoom> util = new ExcelUtil<BaPoundRoom>(BaPoundRoom.class);
        List<BaPoundRoom> poundRoomList = util.importExcel(file.getInputStream());
        String message = baPoundRoomService.importDepotHead(poundRoomList);
        return AjaxResult.success(message);
    }

    /**
     * 获取磅房数据详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:poundRoom:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取磅房数据详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baPoundRoomService.selectBaPoundRoomById(id));
    }

    /**
     * 新增磅房数据
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:add')")
    @Log(title = "磅房数据", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增磅房数据接口")
    public AjaxResult add(@RequestBody BaPoundRoom baPoundRoom)
    {
        int result = baPoundRoomService.insertBaPoundRoom(baPoundRoom);
        if(result == 0){
            return AjaxResult.error(0,"运单已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改磅房数据
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:edit')")
    @Log(title = "磅房数据", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改磅房数据接口")
    public AjaxResult edit(@RequestBody BaPoundRoom baPoundRoom)
    {
        return toAjax(baPoundRoomService.updateBaPoundRoom(baPoundRoom));
    }

    /**
     * 删除磅房数据
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:remove')")
    @Log(title = "磅房数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除磅房数据接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baPoundRoomService.deleteBaPoundRoomByIds(ids));
    }

    /**
     * 关联计划
     */
    @PreAuthorize("@ss.hasPermi('admin:poundRoom:association')")
    @PostMapping("/association")
    @ApiOperation(value = "关联计划")
    public AjaxResult association(@RequestBody BaPoundRoom baPoundRoom){

        return toAjax(baPoundRoomService.association(baPoundRoom));
    }
}
