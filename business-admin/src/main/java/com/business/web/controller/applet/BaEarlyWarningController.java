package com.business.web.controller.applet;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaEarlyWarning;
import com.business.system.service.IBaEarlyWarningService;

/**
 * 预警Controller
 *
 * @author single
 * @date 2024-05-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/earlyWarning")
@Api(tags ="预警接口")
public class BaEarlyWarningController extends BaseController
{
    @Autowired
    private IBaEarlyWarningService baEarlyWarningService;

    /**
     * 查询预警列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:earlyWarning:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询预警列表接口")
    public TableDataInfo list(BaEarlyWarning baEarlyWarning)
    {
        startPage();
        List<BaEarlyWarning> list = baEarlyWarningService.selectBaEarlyWarningList(baEarlyWarning);
        return getDataTable(list);
    }

    @GetMapping("/limitList")
    @ApiOperation(value = "查询额度预警列表接口")
    public TableDataInfo limitList(BaEarlyWarning baEarlyWarning)
    {
        startPage();
        List<BaEarlyWarning> list = baEarlyWarningService.selectEarlyWarning(baEarlyWarning);
        return getDataTable(list);
    }

    /**
     * 获取预警详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:earlyWarning:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取预警详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baEarlyWarningService.selectBaEarlyWarningById(id));
    }

    /**
     * 新增预警
     */
    @PreAuthorize("@ss.hasPermi('admin:earlyWarning:add')")
    @Log(title = "预警", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增预警接口")
    public AjaxResult add(@RequestBody BaEarlyWarning baEarlyWarning)
    {
        return toAjax(baEarlyWarningService.insertBaEarlyWarning(baEarlyWarning));
    }

    /**
     * 修改预警
     */
    @PreAuthorize("@ss.hasPermi('admin:earlyWarning:edit')")
    @Log(title = "预警", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改预警接口")
    public AjaxResult edit(@RequestBody BaEarlyWarning baEarlyWarning)
    {
        return toAjax(baEarlyWarningService.updateBaEarlyWarning(baEarlyWarning));
    }

    /**
     * 删除预警
     */
    @PreAuthorize("@ss.hasPermi('admin:earlyWarning:remove')")
    @Log(title = "预警", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除预警接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baEarlyWarningService.deleteBaEarlyWarningByIds(ids));
    }

    /**
     * 终止预警
     */
    //@PreAuthorize("@ss.hasPermi('admin:earlyWarning:cancel')")
    @PutMapping("/cancel")
    @ApiOperation(value = "终止预警接口")
    public AjaxResult cancel(@RequestBody BaEarlyWarning baEarlyWarning)
    {
        return toAjax(baEarlyWarningService.cancelBaEarlyWarning(baEarlyWarning));
    }
}
