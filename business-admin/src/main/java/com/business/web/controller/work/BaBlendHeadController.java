package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaWashHead;
import com.business.system.service.IBaBlendHeadService;
import com.business.system.service.IBaWashHeadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 配煤出入库记录Controller
 *
 * @author js
 * @date 2024-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/blend")
@Api(tags ="配煤配煤出入库记录接口")
public class BaBlendHeadController extends BaseController
{
    @Autowired
    private IBaBlendHeadService baBlendHeadService;

    /**
     * 查询配煤配煤出入库记录列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:blend:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询配煤配煤出入库记录列表接口")
    public TableDataInfo list(BaWashHead baWashHead)
    {
        //租户
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaWashHead> list = baBlendHeadService.selectBaBlendHeadList(baWashHead);
        return getDataTable(list);
    }

    /**
     * 导出配煤配煤出入库记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:export')")
    @Log(title = "配煤配煤出入库记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出配煤配煤出入库记录列表接口")
    public void export(HttpServletResponse response, BaWashHead baWashHead) throws IOException
    {
        //租户
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        List<BaWashHead> list = baBlendHeadService.selectBaBlendHeadList(baWashHead);
        ExcelUtil<BaWashHead> util = new ExcelUtil<BaWashHead>(BaWashHead.class);
        util.exportExcel(response, list, "wash");
    }

    /**
     * 获取配煤配煤出入库记录详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:blend:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取配煤配煤出入库记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBlendHeadService.selectBaBlendHeadById(id));
    }

    /**
     * 新增配煤入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:add')")
    @Log(title = "配煤配煤出入库记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增配煤配煤出入库记录接口")
    public AjaxResult add(@RequestBody BaWashHead baWashHead)
    {
        return toAjax(baBlendHeadService.insertBaBlendHead(baWashHead));
    }

    /**
     * 新增配煤出库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:add')")
    @Log(title = "新增配煤出库记录", businessType = BusinessType.INSERT)
    @PostMapping(value = "/insertBlendOutputdepot")
    @ApiOperation(value = "新增配煤出库记录")
    public AjaxResult insertBlendOutputdepot(@RequestBody BaWashHead baWashHead)
    {
        return toAjax(baBlendHeadService.insertBlendOutputdepot(baWashHead));
    }

    /**
     * 修改配煤入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:updateDepotHead')")
    @Log(title = "修改配煤入库记录", businessType = BusinessType.UPDATE)
    @PostMapping("/updateBaBlendHead")
    @ApiOperation(value = "修改配煤入库记录")
    public AjaxResult updateBaBlendHead(@RequestBody BaWashHead baWashHead)
    {
        Integer result = baBlendHeadService.updateBaBlendHead(baWashHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 修改配煤出库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:edit')")
    @Log(title = "修改配煤出库记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改配煤出库记录")
    public AjaxResult updateBlendOutputdepot(@RequestBody BaWashHead baWashHead)
    {
        Integer result = baBlendHeadService.updateBlendOutputdepot(baWashHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 删除配煤配煤出入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:remove')")
    @Log(title = "配煤配煤出入库记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除配煤配煤出入库记录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBlendHeadService.deleteBaBlendHeadByIds(ids));
    }

    /** 配煤入库撤销 */
    @PreAuthorize("@ss.hasPermi('admin:blend:into')")
    @GetMapping(value = "/into/revoke/{id}")
    @ApiOperation(value = "入库撤销按钮")
    public AjaxResult intoRevoke(@PathVariable("id") String id)
    {
        Integer result = baBlendHeadService.intoRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 配煤出库撤销
     */
    @PreAuthorize("@ss.hasPermi('admin:blend:revoke')")
    @GetMapping(value = "/output/revoke/{id}")
    @ApiOperation(value = "出库撤销按钮")
    public AjaxResult outputRevoke(@PathVariable("id") String id)
    {
        Integer result = baBlendHeadService.outputRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }
}
