package com.business.web.controller.oa;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.OaClock;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.dto.UserClockDTO;
import com.business.system.service.IOaClockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * 打卡统计Controller
 *
 * @author js
 * @date 2023-11-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/clockStat")
@Api(tags ="打卡统计接口")
public class OaClockStatController extends BaseController
{
    @Autowired
    private IOaClockService oaClockService;

    /**
     * APP打卡统计
     */
//    @PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @PostMapping(value = "/statClock")
    @ApiOperation(value = "APP打卡统计接口")
    public AjaxResult statClock(@RequestBody OaClockDTO oaClockDTO)
    {
        return AjaxResult.success(oaClockService.statClock(oaClockDTO));
    }

    /**
     * APP打卡查询一天打卡数据
     */
//    @PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @PostMapping(value = "/statOaClockList")
    @ApiOperation(value = "APP打卡查询一天打卡数据")
    public AjaxResult statOaClockList(@RequestBody OaClockDTO oaClockDTO)
    {
        return AjaxResult.success(oaClockService.statOaClockList(oaClockDTO));
    }

    /**
     * 初始化打卡数据
     */
//    @PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @PostMapping(value = "/initClockData")
    @ApiOperation(value = "APP打卡查询一天打卡数据")
    public AjaxResult initClockData()
    {
        return AjaxResult.success(oaClockService.initClockData());
    }

    /**
     * APP考勤报表天数据统计
     */
    @GetMapping("/dayData")
    @ApiOperation(value = "APP考勤报表天数据")
    public Map<String,Integer> dayData(OaClock oaClock){
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        return oaClockService.dayData(oaClock);
    }

    /**
     * APP考勤报表天数据统计
     */
    @GetMapping("/dayDataClock")
    @ApiOperation(value = "APP考勤报表天数据列表")
    public TableDataInfo dayDataClock(OaClock oaClock){
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        List<UserClockDTO> list = oaClockService.dayDataClock(oaClock);

        return getDataTable(list);
    }

    /**
     * APP考勤报表周数据统计
     */
    @GetMapping("/weeklyData")
    @ApiOperation(value = "APP考勤报表周数据")
    public AjaxResult weeklyData(OaClock oaClock){
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        UserClockDTO userClockDTO = oaClockService.weeklyData(oaClock);
        return AjaxResult.success(userClockDTO);
    }

    /**
     * APP考勤报表周打卡信息列表
     */
    @GetMapping("/weeklyDataClock")
    @ApiOperation(value = "APP考勤报表周打卡信息列表")
    public TableDataInfo weeklyDataClock(OaClock oaClock){
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        List<UserClockDTO> list = oaClockService.weeklyDataClock(oaClock);
        return getDataTable(list);
    }


}
