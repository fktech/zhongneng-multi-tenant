package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaTopic;
import com.business.system.service.IBaTopicService;

/**
 * 问答Controller
 *
 * @author single
 * @date 2024-04-07
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baTopic")
@Api(tags ="问答接口")
public class BaTopicController extends BaseController
{
    @Autowired
    private IBaTopicService baTopicService;

    /**
     * 查询问答列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:baTopic:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询问答列表接口")
    public TableDataInfo list(BaTopic baTopic)
    {
        startPage();
        List<BaTopic> list = baTopicService.selectBaTopicList(baTopic);
        return getDataTable(list);
    }

    /**
     * 导出问答列表
     */
    @PreAuthorize("@ss.hasPermi('admin:baTopic:export')")
    @Log(title = "问答", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出问答列表接口")
    public void export(HttpServletResponse response, BaTopic baTopic) throws IOException
    {
        List<BaTopic> list = baTopicService.selectBaTopicList(baTopic);
        ExcelUtil<BaTopic> util = new ExcelUtil<BaTopic>(BaTopic.class);
        util.exportExcel(response, list, "baTopic");
    }

    /**
     * 获取问答详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:baTopic:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取问答详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baTopicService.selectBaTopicById(id));
    }

    /**
     * 新增问答
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baTopic:add')")
    @Log(title = "问答", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增问答接口")
    public AjaxResult add(@RequestBody BaTopic baTopic)
    {
        return toAjax(baTopicService.insertBaTopic(baTopic));
    }

    /**
     * 修改问答
     */
    @PreAuthorize("@ss.hasPermi('admin:baTopic:edit')")
    @Log(title = "问答", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改问答接口")
    public AjaxResult edit(@RequestBody BaTopic baTopic)
    {
        return toAjax(baTopicService.updateBaTopic(baTopic));
    }

    /**
     * 删除问答
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baTopic:remove')")
    @Log(title = "问答", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除问答接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baTopicService.deleteBaTopicByIds(ids));
    }
}
