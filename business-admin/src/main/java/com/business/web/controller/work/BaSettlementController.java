package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaMaterialStock;
import com.business.system.domain.BaProject;
import com.business.system.domain.dto.BaOrderDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaSettlement;
import com.business.system.service.IBaSettlementService;
/**
 * 结算信息Controller
 *
 * @author ljb
 * @date 2022-12-26
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/settlement")
@Api(tags ="结算信息接口")
public class BaSettlementController extends BaseController
{
    @Autowired
    private IBaSettlementService baSettlementService;

    /**
     * 查询结算信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:settlement:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询结算信息列表接口")
    public TableDataInfo list(BaSettlement baSettlement)
    {
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSettlement> list = baSettlementService.selectBaSettlementList(baSettlement);
        return getDataTable(list);
    }

    /**
     * 导出结算信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:settlement:export')")
    @Log(title = "结算信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出结算信息列表接口")
    public void export(HttpServletResponse response, BaSettlement baSettlement) throws IOException
    {
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        List<BaSettlement> list = baSettlementService.selectBaSettlementList(baSettlement);
        ExcelUtil<BaSettlement> util = new ExcelUtil<BaSettlement>(BaSettlement.class);
        util.exportExcel(response, list, "settlement");
    }

    /**
     * 获取结算信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:settlement:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取结算信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baSettlementService.selectSettlementById(id));
    }

    /**
     * 新增结算信息
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:add')")
    @Log(title = "结算信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增结算信息接口")
    public AjaxResult add(@RequestBody BaSettlement baSettlement)
    {
        int result = baSettlementService.insertSettlement(baSettlement);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"此合同启动下已有最终结算单");
        }
        return toAjax(result);
    }

    /**
     * 修改结算信息
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:edit')")
    @Log(title = "结算信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改结算信息接口")
    public AjaxResult edit(@RequestBody BaSettlement baSettlement)
    {
        int result = baSettlementService.updateBaSettlement(baSettlement);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"此合同启动下已有最终结算单");
        }
        return toAjax(result);
    }

    /**
     * 删除结算信息
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:remove')")
    @Log(title = "结算信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除结算信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baSettlementService.deleteBaSettlementByIds(ids));
    }

    /**
     * 查询相关订单信息
     */
    @GetMapping("/orderDto")
    @ApiOperation(value = "查询相关订单信息")
    public TableDataInfo orderDto(String contractId,String type)
    {
       List<BaOrderDTO> list = baSettlementService.orderDto(contractId,type);
        return getDataTable(list);
    }

    /**
     * 查询结算申请列表
     */
    @GetMapping("/selectBaSettlement")
    @ApiOperation(value = "查询结算申请列表接口")
    public TableDataInfo selectBaSettlement(BaSettlement baSettlement)
    {
        startPage();
        List<BaSettlement> list = baSettlementService.selectBaSettlement(baSettlement);
        return getDataTable(list);
    }

    /**
     * 结算完结
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:settlementEnd')")
    @GetMapping(value = "/settlementEnd")
    @ApiOperation(value = "结算完结")
    public AjaxResult settlementEnd(String id)
    {
        return AjaxResult.success(baSettlementService.settlementEnd(id));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baSettlementService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 统计结算
     */
    @GetMapping("/countBaSettlementList")
    @ApiOperation(value = "统计结算")
    public AjaxResult countBaSettlementList(BaSettlement baSettlement)
    {
        return AjaxResult.success(baSettlementService.countBaSettlementList(baSettlement));
    }

    /**
     * 查询多租户授权信息结算信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:settlement:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息结算信息列表接口")
    public TableDataInfo authorizedList(BaSettlement baSettlement)
    {
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSettlement> list = baSettlementService.selectBaSettlementAuthorizedList(baSettlement);
        return getDataTable(list);
    }

    /**
     * 授权结算管理
     */
    @PreAuthorize("@ss.hasPermi('admin:settlement:authorized')")
    @Log(title = "结算管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权结算管理")
    public AjaxResult authorized(@RequestBody BaSettlement baSettlement)
    {
        return AjaxResult.success(baSettlementService.authorizedBaSettlement(baSettlement));
    }
}
