package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaRailwayPlatform;
import com.business.system.service.IBaRailwayPlatformService;

/**
 * 铁路站台Controller
 *
 * @author ljb
 * @date 2022-11-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/platform")
@Api(tags ="铁路站台接口")
public class BaRailwayPlatformController extends BaseController
{
    @Autowired
    private IBaRailwayPlatformService baRailwayPlatformService;

    /**
     * 查询铁路站台列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:platform:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询铁路站台列表接口")
    public TableDataInfo list(BaRailwayPlatform baRailwayPlatform)
    {
        startPage();
        List<BaRailwayPlatform> list = baRailwayPlatformService.selectBaRailwayPlatformList(baRailwayPlatform);
        return getDataTable(list);
    }

    /**
     * 查询运费列表信息
     */
    @GetMapping(value = "/freight")
    @ApiOperation(value = "运费列表接口")
    public AjaxResult freight()
    {
        return AjaxResult.success(baRailwayPlatformService.freightTable());
    }

    /**
     * 导出铁路站台列表
     */
    @PreAuthorize("@ss.hasPermi('admin:platform:export')")
    @Log(title = "铁路站台", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出铁路站台列表接口")
    public void export(HttpServletResponse response, BaRailwayPlatform baRailwayPlatform) throws IOException
    {
        List<BaRailwayPlatform> list = baRailwayPlatformService.selectBaRailwayPlatformList(baRailwayPlatform);
        ExcelUtil<BaRailwayPlatform> util = new ExcelUtil<BaRailwayPlatform>(BaRailwayPlatform.class);
        util.exportExcel(response, list, "platform");
    }

    /**
     * 获取铁路站台详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:platform:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取铁路站台详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baRailwayPlatformService.selectBaRailwayPlatformById(id));
    }

    /**
     * 新增铁路站台
     */
    @PreAuthorize("@ss.hasPermi('admin:platform:add')")
    @Log(title = "铁路站台", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增铁路站台接口")
    public UR add(@RequestBody BaRailwayPlatform baRailwayPlatform)
    {
        int count = baRailwayPlatformService.insertBaRailwayPlatform(baRailwayPlatform);
        if(count > 0){
            return UR.ok().code(200).message("添加成功");
        }else if(count < 0){
            return UR.error().code(500).message("添加失败");
        }
        return UR.error().code(0).message("站台已存在");
    }

    /**
     * 修改铁路站台
     */
    @PreAuthorize("@ss.hasPermi('admin:platform:edit')")
    @Log(title = "铁路站台", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改铁路站台接口")
    public AjaxResult edit(@RequestBody BaRailwayPlatform baRailwayPlatform)
    {
        return toAjax(baRailwayPlatformService.updateBaRailwayPlatform(baRailwayPlatform));
    }

    /**
     * 删除铁路站台
     */
    @PreAuthorize("@ss.hasPermi('admin:platform:remove')")
    @Log(title = "铁路站台", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除铁路站台接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baRailwayPlatformService.deleteBaRailwayPlatformByIds(ids));
    }
}
