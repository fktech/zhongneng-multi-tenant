package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaBidTransport;
import com.business.system.domain.dto.CountBaProjectDTO;
import com.business.system.service.IBaBidTransportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 合同启动投标信息运输Controller
 *
 * @author single
 * @date 2023-06-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/bidTransport")
@Api(tags ="合同启动投标信息运输接口")
public class BaBidTransportController extends BaseController
{
    @Autowired
    private IBaBidTransportService baBidTransportService;

    /**
     * 查询合同启动投标信息运输列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:transport:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询合同启动投标信息运输列表接口")
    public TableDataInfo list(BaBidTransport baBidTransport)
    {
        //租户ID
        //baBidTransport.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaBidTransport> list = baBidTransportService.selectBaBidTransportList(baBidTransport);
        return getDataTable(list);
    }

    /**
     * 导出合同启动投标信息运输列表
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:export')")
    @Log(title = "合同启动投标信息运输", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出合同启动投标信息运输列表接口")
    public void export(HttpServletResponse response, BaBidTransport baBidTransport) throws IOException
    {
        List<BaBidTransport> list = baBidTransportService.selectBaBidTransportList(baBidTransport);
        ExcelUtil<BaBidTransport> util = new ExcelUtil<BaBidTransport>(BaBidTransport.class);
        util.exportExcel(response, list, "transport");
    }

    /**
     * 获取合同启动投标信息运输详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取合同启动投标信息运输详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBidTransportService.selectBaBidTransportById(id));
    }

    /**
     * 新增合同启动投标信息运输
     */
    //@PreAuthorize("@ss.hasPermi('admin:transport:add')")
    @Log(title = "合同启动投标信息运输", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增合同启动投标信息运输接口")
    public AjaxResult add(@RequestBody BaBidTransport baBidTransport)
    {
        return toAjax(baBidTransportService.insertBaBidTransport(baBidTransport));
    }

    /**
     * 修改合同启动投标信息运输
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:edit')")
    @Log(title = "合同启动投标信息运输", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改合同启动投标信息运输接口")
    public AjaxResult edit(@RequestBody BaBidTransport baBidTransport)
    {
        return toAjax(baBidTransportService.updateBaBidTransport(baBidTransport));
    }

    /**
     * 删除合同启动投标信息运输
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:remove')")
    @Log(title = "合同启动投标信息运输", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除合同启动投标信息运输接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBidTransportService.deleteBaBidTransportByIds(ids));
    }
}
