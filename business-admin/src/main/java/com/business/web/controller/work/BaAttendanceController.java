package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.ClockCountDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaAttendance;
import com.business.system.service.IBaAttendanceService;

/**
 * 打卡Controller
 *
 * @author single
 * @date 2023-10-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/attendance")
@Api(tags ="历史打卡接口")
public class BaAttendanceController extends BaseController
{
    @Autowired
    private IBaAttendanceService baAttendanceService;

    /**
     * 查询打卡列表
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询打卡列表接口")
    public TableDataInfo list(BaAttendance baAttendance)
    {
        startPage();
        List<BaAttendance> list = baAttendanceService.selectBaAttendanceList(baAttendance);
        return getDataTable(list);
    }

    /**
     * 导出打卡列表
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:export')")
    @Log(title = "打卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出打卡列表接口")
    public void export(HttpServletResponse response, BaAttendance baAttendance) throws IOException
    {
        List<BaAttendance> list = baAttendanceService.selectBaAttendanceList(baAttendance);
        ExcelUtil<BaAttendance> util = new ExcelUtil<BaAttendance>(BaAttendance.class);
        util.exportExcel(response, list, "attendance");
    }

    /**
     * 获取打卡详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取打卡详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAttendanceService.selectBaAttendanceById(id));
    }

    /**
     * 新增打卡
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:add')")
    @Log(title = "打卡", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增打卡接口")
    public AjaxResult add(@RequestBody BaAttendance baAttendance)
    {
        return toAjax(baAttendanceService.insertBaAttendance(baAttendance));
    }

    /**
     * 修改打卡
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:edit')")
    @Log(title = "打卡", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改打卡接口")
    public AjaxResult edit(@RequestBody BaAttendance baAttendance)
    {
        return toAjax(baAttendanceService.updateBaAttendance(baAttendance));
    }

    /**
     * 删除打卡
     */
    @PreAuthorize("@ss.hasPermi('admin:attendance:remove')")
    @Log(title = "打卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除打卡接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAttendanceService.deleteBaAttendanceByIds(ids));
    }

    /** 打卡统计 */
    @GetMapping("/clockCount")
    @ApiOperation(value = "打卡统计")
    public AjaxResult clockCount(BaAttendance baAttendance){
        ClockCountDTO clockCountDTO = baAttendanceService.clockCount(baAttendance);
        return AjaxResult.success(clockCountDTO);
    }

}
