package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaGlobalNumber;
import com.business.system.service.IBaGlobalNumberService;

/**
 * 全局编号Controller
 *
 * @author ljb
 * @date 2024-06-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baGlobalNumber")
@Api(tags ="全局编号接口")
public class BaGlobalNumberController extends BaseController
{
    @Autowired
    private IBaGlobalNumberService baGlobalNumberService;

    /**
     * 查询全局编号列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:baGlobalNumber:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询全局编号列表接口")
    public TableDataInfo list(BaGlobalNumber baGlobalNumber)
    {
        startPage();
        List<BaGlobalNumber> list = baGlobalNumberService.selectBaGlobalNumberList(baGlobalNumber);
        return getDataTable(list);
    }
}
