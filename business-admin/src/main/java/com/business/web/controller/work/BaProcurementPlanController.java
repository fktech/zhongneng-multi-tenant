package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaContract;
import com.business.system.domain.BaProject;
import com.business.system.domain.BaPurchaseOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.service.IBaProcurementPlanService;

/**
 * 采购计划Controller
 *
 * @author ljb
 * @date 2023-08-31
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/procurementPlan")
@Api(tags ="采购计划接口")
public class BaProcurementPlanController extends BaseController
{
    @Autowired
    private IBaProcurementPlanService baProcurementPlanService;

    /**
     * 查询采购计划列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:procurementPlan:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询采购计划列表接口")
    public TableDataInfo list(BaProcurementPlan baProcurementPlan)
    {
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProcurementPlan> list = baProcurementPlanService.selectBaProcurementPlanList(baProcurementPlan);
        return getDataTable(list);
    }

    /**
     * 导出采购计划列表
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:export')")
    @Log(title = "采购计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出采购计划列表接口")
    public void export(HttpServletResponse response, BaProcurementPlan baProcurementPlan) throws IOException
    {
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        List<BaProcurementPlan> list = baProcurementPlanService.selectBaProcurementPlanList(baProcurementPlan);
        ExcelUtil<BaProcurementPlan> util = new ExcelUtil<BaProcurementPlan>(BaProcurementPlan.class);
        util.exportExcel(response, list, "procurementPlan");
    }

    /**
     * 获取采购计划详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:procurementPlan:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取采购计划详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baProcurementPlanService.selectBaProcurementPlanById(id));
    }

    /**
     * 新增采购计划
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:add')")
    @Log(title = "采购计划", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增采购计划接口")
    public AjaxResult add(@RequestBody BaProcurementPlan baProcurementPlan)
    {
        int result = baProcurementPlanService.insertBaProcurementPlan(baProcurementPlan);
        if(result == -1){
            return AjaxResult.error(-1,"启动流程失败");
        }else if(result == 0){
            return AjaxResult.error(0,"计划名称重复");
        }
        return toAjax(result);
    }

    /**
     * 修改采购计划
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:edit')")
    @Log(title = "采购计划", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改采购计划接口")
    public AjaxResult edit(@RequestBody BaProcurementPlan baProcurementPlan)
    {
        int result = baProcurementPlanService.updateBaProcurementPlan(baProcurementPlan);
        if(result == -1){
            return AjaxResult.error(-1,"启动流程失败");
        }else if(result == 0){
            return AjaxResult.error(0,"计划名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 删除采购计划
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:remove')")
    @Log(title = "采购计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除采购计划接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baProcurementPlanService.deleteBaProcurementPlanByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baProcurementPlanService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 关联项目
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:association')")
    @PostMapping(value = "/association")
    @ApiOperation(value = "关联项目")
    public AjaxResult association(@RequestBody BaProcurementPlan baProcurementPlan){
        return toAjax(baProcurementPlanService.association(baProcurementPlan));
    }

    /**
     * 采购计划下拉
     */
    @GetMapping(value = "/dropDown")
    @ApiOperation(value = "采购计划下拉")
    public TableDataInfo dropDown(String projectId,String goodsId){
        List<BaPurchaseOrder> purchaseOrders = baProcurementPlanService.dropDown(projectId, goodsId);
        return getDataTable(purchaseOrders);
    }

    /**
     * 采购清单数据
     */
    @GetMapping(value = "/baPurchaseOrderList")
    @ApiOperation(value = "采购清单列表")
    public TableDataInfo baPurchaseOrderList(String projectId,String planId){
        List<BaPurchaseOrder> list = baProcurementPlanService.selectBaPurchaseOrderList(projectId,planId);
        return getDataTable(list);
    }

    /**
     * 合同名称
     */

    @GetMapping("/purchaseRequestName")
    @ApiOperation(value = "采购申请名称")
    public UR purchaseRequestName(String projectId, String type){
        UR ur = baProcurementPlanService.purchaseRequestName(projectId,type);
        return ur;
    }

    /**
     * 统计采购计划
     */
    @GetMapping("/countBaProcurementPlanList")
    @ApiOperation(value = "统计采购计划")
    public AjaxResult countBaProcurementPlanList(BaProcurementPlan baProcurementPlan)
    {
        return AjaxResult.success(baProcurementPlanService.countBaProcurementPlanList(baProcurementPlan));
    }

    /**
     * 查询多租户授权信息采购计划列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:procurementPlan:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息采购计划列表")
    public TableDataInfo authorizedList(BaProcurementPlan baProcurementPlan)
    {
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProcurementPlan> list = baProcurementPlanService.selectBaProcurementPlanAuthorizedList(baProcurementPlan);
        return getDataTable(list);
    }

    /**
     * 授权采购计划
     */
    @PreAuthorize("@ss.hasPermi('admin:procurementPlan:authorized')")
    @Log(title = "采购计划", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权采购计划")
    public AjaxResult authorized(@RequestBody BaProcurementPlan baProcurementPlan)
    {
        return AjaxResult.success(baProcurementPlanService.authorizedBaProcurementPlan(baProcurementPlan));
    }
}
