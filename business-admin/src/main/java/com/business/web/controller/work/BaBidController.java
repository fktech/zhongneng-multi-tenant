package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaBid;
import com.business.system.service.IBaBidService;

/**
 * 投标管理Controller
 *
 * @author ljb
 * @date 2022-12-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/babid")
@Api(tags ="投标管理接口")
public class BaBidController extends BaseController
{
    @Autowired
    private IBaBidService baBidService;

    /**
     * 查询投标管理列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:babid:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询投标管理列表接口")
    public TableDataInfo list(BaBid baBid)
    {
        startPage();
        List<BaBid> list = baBidService.selectBaBidList(baBid);
        return getDataTable(list);
    }

    /**
     * 导出投标管理列表
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:export')")
    @Log(title = "投标管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出投标管理列表接口")
    public void export(HttpServletResponse response, BaBid baBid) throws IOException
    {
        List<BaBid> list = baBidService.selectBaBidList(baBid);
        ExcelUtil<BaBid> util = new ExcelUtil<BaBid>(BaBid.class);
        util.exportExcel(response, list, "babid");
    }

    /**
     * 获取投标管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取投标管理详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBidService.selectBaBidById(id));
    }

    /**
     * 新增投标管理
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:add')")
    @Log(title = "投标管理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增投标管理接口")
    public AjaxResult add(@RequestBody BaBid baBid)
    {
        Integer result = baBidService.insertBaBid(baBid);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改投标管理
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:edit')")
    @Log(title = "投标管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改投标管理接口")
    public AjaxResult edit(@RequestBody BaBid baBid)
    {
        Integer result = baBidService.updateBaBid(baBid);
        if(result == 0){
         return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 投标按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:bidSubmit')")
    @PutMapping("/bidSubmit")
    @ApiOperation(value = "投标按钮")
    public AjaxResult bidSubmit(@RequestBody BaBid baBid)
    {
        return toAjax(baBidService.bidSubmit(baBid));
    }

    /**
     * 删除投标管理
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:remove')")
    @Log(title = "投标管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除投标管理接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBidService.deleteBaBidByIds(ids));
    }

    /**
     * 获取投标管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:babid:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baBidService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }
}
