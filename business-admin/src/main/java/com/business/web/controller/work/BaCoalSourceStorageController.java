package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaCoalSourceStorage;
import com.business.system.service.IBaCoalSourceStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 煤源库Controller
 *
 * @author single
 * @date 2024-01-25
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/storage")
@Api(tags ="煤源库接口")
public class BaCoalSourceStorageController extends BaseController
{
    @Autowired
    private IBaCoalSourceStorageService baCoalSourceStorageService;

    /**
     * 查询煤源库列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:storage:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询煤源库列表接口")
    public TableDataInfo list(BaCoalSourceStorage baCoalSourceStorage)
    {
        //租户ID
        baCoalSourceStorage.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCoalSourceStorage> list = baCoalSourceStorageService.selectBaCoalSourceStorageList(baCoalSourceStorage);
        return getDataTable(list);
    }

    /**
     * 导出煤源库列表
     */
    @PreAuthorize("@ss.hasPermi('admin:storage:export')")
    @Log(title = "煤源库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出煤源库列表接口")
    public void export(HttpServletResponse response, BaCoalSourceStorage baCoalSourceStorage) throws IOException
    {
        //租户ID
        baCoalSourceStorage.setTenantId(SecurityUtils.getCurrComId());
        List<BaCoalSourceStorage> list = baCoalSourceStorageService.countBaCoalSourceStorageList(baCoalSourceStorage);
        ExcelUtil<BaCoalSourceStorage> util = new ExcelUtil<BaCoalSourceStorage>(BaCoalSourceStorage.class);
        util.exportExcel(response, list, "storage");
    }

    /**
     * 获取煤源库详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:storage:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取煤源库详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCoalSourceStorageService.selectBaCoalSourceStorageById(id));
    }

    /**
     * 新增煤源库
     */
    @PreAuthorize("@ss.hasPermi('admin:storage:add')")
    @Log(title = "煤源库", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增煤源库接口")
    public AjaxResult add(@RequestBody BaCoalSourceStorage baCoalSourceStorage)
    {
        return toAjax(baCoalSourceStorageService.insertBaCoalSourceStorage(baCoalSourceStorage));
    }

    /**
     * 修改煤源库
     */
    @PreAuthorize("@ss.hasPermi('admin:storage:edit')")
    @Log(title = "煤源库", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改煤源库接口")
    public AjaxResult edit(@RequestBody BaCoalSourceStorage baCoalSourceStorage)
    {
        return toAjax(baCoalSourceStorageService.updateBaCoalSourceStorage(baCoalSourceStorage));
    }

    /**
     * 删除煤源库
     */
    @PreAuthorize("@ss.hasPermi('admin:storage:remove')")
    @Log(title = "煤源库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除煤源库接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCoalSourceStorageService.deleteBaCoalSourceStorageByIds(ids));
    }

    /**
     * 查询煤源库列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:storage:list')")
    @GetMapping("/countList")
    @ApiOperation(value = "统计煤源库列表接口")
    public TableDataInfo countList(BaCoalSourceStorage baCoalSourceStorage)
    {
        //租户ID
        baCoalSourceStorage.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCoalSourceStorage> list = baCoalSourceStorageService.countBaCoalSourceStorageList(baCoalSourceStorage);
        return getDataTable(list);
    }
}
