package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaTransport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaTransportCmst;
import com.business.system.service.IBaTransportCmstService;

/**
 * 中储智运Controller
 *
 * @author single
 * @date 2023-08-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/transportCmst")
@Api(tags ="中储智运接口")
public class BaTransportCmstController extends BaseController
{
    @Autowired
    private IBaTransportCmstService baTransportCmstService;

    /**
     * 查询中储智运列表
     */
   /* @PreAuthorize("@ss.hasPermi('admin:transportCmst:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询中储智运列表接口")
    public TableDataInfo list(BaTransportCmst baTransportCmst)
    {
        startPage();
        List<BaTransportCmst> list = baTransportCmstService.baTransportCmstList(baTransportCmst);
        return getDataTable(list);
    }

    /**
     * 导出中储智运列表
     */
    @PreAuthorize("@ss.hasPermi('admin:transportCmst:export')")
    @Log(title = "中储智运", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出中储智运列表接口")
    public void export(HttpServletResponse response, BaTransportCmst baTransportCmst) throws IOException
    {
        List<BaTransportCmst> list = baTransportCmstService.selectBaTransportCmstList(baTransportCmst);
        ExcelUtil<BaTransportCmst> util = new ExcelUtil<BaTransportCmst>(BaTransportCmst.class);
        util.exportExcel(response, list, "transportCmst");
    }

    /**
     * 获取中储智运详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:transportCmst:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取中储智运详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baTransportCmstService.selectBaTransportCmstById(id));
    }

    /**
     * 新增中储智运
     */
    @PreAuthorize("@ss.hasPermi('admin:transportCmst:add')")
    @Log(title = "中储智运", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增中储智运接口")
    public AjaxResult add(@RequestBody BaTransportCmst baTransportCmst)
    {
        return toAjax(baTransportCmstService.insertBaTransportCmst(baTransportCmst));
    }

    /**
     * 修改中储智运
     */
    @PreAuthorize("@ss.hasPermi('admin:transportCmst:edit')")
    @Log(title = "中储智运", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改中储智运接口")
    public AjaxResult edit(@RequestBody BaTransportCmst baTransportCmst)
    {
        return toAjax(baTransportCmstService.updateBaTransportCmst(baTransportCmst));
    }

    /**
     * 删除中储智运
     */
    @PreAuthorize("@ss.hasPermi('admin:transportCmst:remove')")
    @Log(title = "中储智运", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除中储智运接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baTransportCmstService.deleteBaTransportCmstByIds(ids));
    }

    /** 关联发运 */
    @PostMapping("/transport")
    @ApiOperation(value = "关联发运")
    public AjaxResult transport(@RequestBody BaTransport baTransport){

        return toAjax(baTransportCmstService.transport(baTransport));
    }
}
