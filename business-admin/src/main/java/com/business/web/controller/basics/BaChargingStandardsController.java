package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaChargingStandards;
import com.business.system.service.IBaChargingStandardsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 收费标准Controller
 *
 * @author js
 * @date 2023-06-02
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/standards")
@Api(tags ="收费标准接口")
public class BaChargingStandardsController extends BaseController
{
    @Autowired
    private IBaChargingStandardsService baChargingStandardsService;

    /**
     * 查询收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询收费标准列表接口")
    public TableDataInfo list(BaChargingStandards baChargingStandards)
    {
        startPage();
        List<BaChargingStandards> list = baChargingStandardsService.selectBaChargingStandardsList(baChargingStandards);
        return getDataTable(list);
    }

    /**
     * 导出收费标准列表
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:export')")
    @Log(title = "收费标准", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出收费标准列表接口")
    public void export(HttpServletResponse response, BaChargingStandards baChargingStandards) throws IOException
    {
        List<BaChargingStandards> list = baChargingStandardsService.selectBaChargingStandardsList(baChargingStandards);
        ExcelUtil<BaChargingStandards> util = new ExcelUtil<BaChargingStandards>(BaChargingStandards.class);
        util.exportExcel(response, list, "standards");
    }

    /**
     * 获取收费标准详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取收费标准详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baChargingStandardsService.selectBaChargingStandardsById(id));
    }

    /**
     * 新增收费标准
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:add')")
    @Log(title = "收费标准", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增收费标准接口")
    public AjaxResult add(@RequestBody BaChargingStandards baChargingStandards)
    {
        return toAjax(baChargingStandardsService.insertBaChargingStandards(baChargingStandards));
    }

    /**
     * 修改收费标准
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:edit')")
    @Log(title = "收费标准", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改收费标准接口")
    public AjaxResult edit(@RequestBody BaChargingStandards baChargingStandards)
    {
        return toAjax(baChargingStandardsService.updateBaChargingStandards(baChargingStandards));
    }

    /**
     * 删除收费标准
     */
    @PreAuthorize("@ss.hasPermi('admin:standards:remove')")
    @Log(title = "收费标准", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除收费标准接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baChargingStandardsService.deleteBaChargingStandardsByIds(ids));
    }
}
