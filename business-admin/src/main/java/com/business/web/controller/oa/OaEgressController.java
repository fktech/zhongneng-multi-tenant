package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaEgress;
import com.business.system.service.IOaEgressService;
/**
 * 外出申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaEgress")
@Api(tags ="外出申请接口")
public class OaEgressController extends BaseController
{
    @Autowired
    private IOaEgressService oaEgressService;

    /**
     * 查询外出申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEgress:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询外出申请列表接口")
    public TableDataInfo list(OaEgress oaEgress)
    {
        //租户ID
        oaEgress.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaEgress> list = oaEgressService.selectOaEgressList(oaEgress);
        return getDataTable(list);
    }

    /**
     * 导出外出申请列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEgress:export')")
    @Log(title = "外出申请", businessType = BusinessType.EXPORT)*/
    @PostMapping("/export")
    @ApiOperation(value = "导出外出申请列表接口")
    public void export(HttpServletResponse response, OaEgress oaEgress) throws IOException
    {
        //租户ID
        oaEgress.setTenantId(SecurityUtils.getCurrComId());
        List<OaEgress> list = oaEgressService.selectOaEgressList(oaEgress);
        ExcelUtil<OaEgress> util = new ExcelUtil<OaEgress>(OaEgress.class);
        util.exportExcel(response, list, "oaEgress");
    }

    /**
     * 获取外出申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEgress:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取外出申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaEgressService.selectOaEgressById(id));
    }

    /**
     * 新增外出申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEgress:add')")
    @Log(title = "外出申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增外出申请接口")
    public AjaxResult add(@RequestBody OaEgress oaEgress)
    {
        int result = oaEgressService.insertOaEgress(oaEgress);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改外出申请
     */
   /* @PreAuthorize("@ss.hasPermi('admin:oaEgress:edit')")
    @Log(title = "外出申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改外出申请接口")
    public AjaxResult edit(@RequestBody OaEgress oaEgress)
    {
        int result = oaEgressService.updateOaEgress(oaEgress);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(oaEgressService.updateOaEgress(oaEgress));
    }

    /**
     * 删除外出申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEgress:remove')")
    @Log(title = "外出申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除外出申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaEgressService.deleteOaEgressByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEgress:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaEgressService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
