package com.business.web.controller.workflow;

import com.business.common.constant.BusinessConstant;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.BaProcessInstanceRelatedQueryDTO;
import com.business.system.domain.dto.ProcessTaskDTO;
import com.business.system.domain.dto.ProcessTaskQueryDTO;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;
import com.business.system.service.IBaProcessTaskService;
import com.business.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0
 * @Author: js
 * @Description: 审批任务前端控制器
 * @Date: 2022/12/6 13:32
 */
@RestController
@RequestMapping("/workflow/task")
@Api(tags = "审批任务接口")
public class BaProcessTaskController extends BaseController {

    @Autowired
    private IBaProcessTaskService iBaProcessTaskService;

    @Autowired
    private ISysUserService iSysUserService;

    @ApiOperation("分页查询我的待办任务")
    @GetMapping("/runtime")
    public TableDataInfo listRuntimeProcessTask(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listRuntimeProcessTask(queryDTO);
    }

    @ApiOperation("分页查询待办任务(首页)")
    @GetMapping("/home/runtime")
    public TableDataInfo listHomeRuntimeProcessTask(ProcessTaskQueryDTO queryDTO) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listHomeRuntimeProcessTask(queryDTO);
    }

    @ApiOperation("统计我的待办任务")
    @GetMapping("/countRuntimeTask")
    public AjaxResult countRuntimeTask(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
        if(StringUtils.hasText(queryDTO.getStartUserName())){
            iBaProcessTaskService.setStartUserId(queryDTO);
        }
        return success(iBaProcessTaskService.countRuntimeTask(queryDTO));
    }

    @ApiOperation("办理待办任务")
    @PostMapping("/completeTask")
    public AjaxResult completeTask(@RequestBody ProcessTaskDTO processTaskDTO){
        iBaProcessTaskService.completeTask(processTaskDTO);
        return success();
    }

    @ApiOperation("查询我的已办任务")
    @GetMapping("/historyTask")
    public TableDataInfo listHistoryProcessTask(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listHistoryProcessTask(queryDTO);
    }

    @ApiOperation("统计我的已办任务")
    @GetMapping("/countHistoryProcessTask")
    public AjaxResult countHistoryProcessTask(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
        if(StringUtils.hasText(queryDTO.getStartUserName())){
            iBaProcessTaskService.setStartUserId(queryDTO);
        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return success(iBaProcessTaskService.countHistoryProcess(queryDTO));
    }

    @ApiOperation("查询我的已完结任务")
    @GetMapping("/historyTask/end")
    public TableDataInfo listHistoryProcessTaskEnd(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
        iBaProcessTaskService.setStartUserId(queryDTO);
        //设置已完结状态
        queryDTO.setProcessIsEnd(BusinessConstant.PROCESS_HISTORY_END);
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listHistoryProcessTask(queryDTO);
    }

    @ApiOperation("统计各个业务我的待办总数")
    @PostMapping("/countRuntimeTask")
    public AjaxResult countRuntimeTask(){
        return success(iBaProcessTaskService.countRuntimeTask());
    }

    @ApiOperation("统计各个业务我的已办总数")
    @PostMapping("/countHistoryProcessTask")
    public AjaxResult countHistoryProcessTask(){
        return success(iBaProcessTaskService.countHistoryProcessTask());
    }

    @ApiOperation("统计各个业务我的已完结总数")
    @PostMapping("/countHistoryProcessIsEndTask")
    public AjaxResult countHistoryProcessIsEndTask(){
        return success(iBaProcessTaskService.countHistoryProcessIsEndTask());
    }

    @ApiOperation("统计查询我的申请")
    @GetMapping("/myApply")
    public TableDataInfo myApply(BaProcessInstanceRelatedQueryDTO queryDTO) {
        startPage();
        //设置发起人ID
        queryDTO.setCreateBy(String.valueOf(SecurityUtils.getUsername()));
        return getDataTable(iBaProcessTaskService.listMyApply(queryDTO));
    }

    @ApiOperation("查询我的申请列表")
    @GetMapping("/myApplyList")
    public TableDataInfo myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO) {
        //设置发起人ID
        queryDTO.setCreateBy(String.valueOf(SecurityUtils.getUsername()));
        startPage();
        return iBaProcessTaskService.myApplyList(queryDTO);
    }

    @ApiOperation("分页查询抄送给我的")
    @GetMapping("/runtimeSend")
    public TableDataInfo listRuntimeProcessTaskSend(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listRuntimeProcessTaskSend(queryDTO);
    }

    @ApiOperation("驳回按钮")
    @PostMapping("/refuse")
    public AjaxResult refuse(@RequestBody ProcessTaskDTO processTaskDTO){
        iBaProcessTaskService.refuse(processTaskDTO);
        return success();
    }

    @ApiOperation("督办任务查询关联流程")
    @GetMapping("/supervisingHistoryProcess")
    public TableDataInfo supervisingHistoryProcess(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        //设置已完结状态
        queryDTO.setProcessIsEnd(BusinessConstant.PROCESS_HISTORY_END);
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.supervisingHistoryProcess(queryDTO);
    }

    @ApiOperation("APP分页查询我的待办任务")
    @GetMapping("/runtimeApp")
    public TableDataInfo listRuntimeProcessTaskApp(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listRuntimeProcessTaskApp(queryDTO);
    }

    @ApiOperation("APP查询我的已办任务")
    @GetMapping("/historyTaskApp")
    public TableDataInfo listHistoryProcessTaskApp(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listHistoryProcessTaskApp(queryDTO);
    }

    @ApiOperation("APP分页查询抄送给我的")
    @GetMapping("/runtimeSendApp")
    public TableDataInfo listRuntimeProcessTaskSendApp(ProcessTaskQueryDTO queryDTO) {
        //设置发起人ID
//        if(StringUtils.hasText(queryDTO.getStartUserName())){
//            iBaProcessTaskService.setStartUserId(queryDTO);
//        }
        PageDomain pageDomain = TableSupport.buildPageRequest();
        queryDTO.setCurrent(Long.valueOf(pageDomain.getPageNum()));
        queryDTO.setSize(Long.valueOf(pageDomain.getPageSize()));
        return iBaProcessTaskService.listRuntimeProcessTaskSendApp(queryDTO);
    }
}
