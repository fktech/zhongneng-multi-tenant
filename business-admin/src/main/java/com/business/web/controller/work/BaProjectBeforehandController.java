package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaProgressInformation;
import com.business.system.domain.BaProject;
import com.business.system.domain.dto.BaProjectInstanceRelatedEditDTO;
import com.business.system.domain.dto.CountBaProjectDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaProjectBeforehand;
import com.business.system.service.IBaProjectBeforehandService;

/**
 * 预立项Controller
 *
 * @author ljb
 * @date 2023-09-20
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/beforehand")
@Api(tags ="预立项接口")
public class BaProjectBeforehandController extends BaseController
{
    @Autowired
    private IBaProjectBeforehandService baProjectBeforehandService;

    /**
     * 查询预立项列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:beforehand:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询预立项列表接口")
    public TableDataInfo list(BaProjectBeforehand baProjectBeforehand)
    {
        baProjectBeforehand.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProjectBeforehand> list = baProjectBeforehandService.selectBaProjectBeforehandList(baProjectBeforehand);
        return getDataTable(list);
    }

    /**
     * 导出预立项列表
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:export')")
    @Log(title = "预立项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出预立项列表接口")
    public void export(HttpServletResponse response, BaProjectBeforehand baProjectBeforehand) throws IOException
    {
        baProjectBeforehand.setTenantId(SecurityUtils.getCurrComId());
        List<BaProjectBeforehand> list = baProjectBeforehandService.selectBaProjectBeforehandList(baProjectBeforehand);
        ExcelUtil<BaProjectBeforehand> util = new ExcelUtil<BaProjectBeforehand>(BaProjectBeforehand.class);
        util.exportExcel(response, list, "beforehand");
    }

    /**
     * 获取预立项详细信息
     */
   //@PreAuthorize("@ss.hasPermi('admin:beforehand:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取预立项详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baProjectBeforehandService.selectBaProjectBeforehandById(id));
    }

    /**
     * 新增预立项
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:add')")
    @Log(title = "预立项", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增预立项接口")
    public AjaxResult add(@RequestBody BaProjectBeforehand baProjectBeforehand)
    {
        return toAjax(baProjectBeforehandService.insertBaProjectBeforehand(baProjectBeforehand));
    }

    /**
     * 修改预立项
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:edit')")
    @Log(title = "预立项", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改预立项接口")
    public AjaxResult edit(@RequestBody BaProjectBeforehand baProjectBeforehand)
    {
        return toAjax(baProjectBeforehandService.updateBaProjectBeforehand(baProjectBeforehand));
    }

    /**
     * 回复周期
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:replyCycle')")
    @Log(title = "预立项", businessType = BusinessType.UPDATE)
    @PutMapping("/replyCycle")
    @ApiOperation(value = "回复周期")
    public AjaxResult replyCycle(@RequestBody BaProjectBeforehand baProjectBeforehand)
    {
        return toAjax(baProjectBeforehandService.updateBaProjectBeforehand(baProjectBeforehand));
    }

    /**
     * 批复
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:approval')")
    @Log(title = "预立项", businessType = BusinessType.UPDATE)
    @PutMapping("/approval")
    @ApiOperation(value = "批复")
    public AjaxResult approval(@RequestBody BaProjectBeforehand baProjectBeforehand)
    {
        return toAjax(baProjectBeforehandService.updateBaProjectBeforehand(baProjectBeforehand));
    }

    /**
     * 删除预立项
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:remove')")
    @Log(title = "预立项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除预立项接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baProjectBeforehandService.deleteBaProjectBeforehandByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baProjectBeforehandService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 编辑预立项待定状态
     */
    @PostMapping("/updateProjectBeforehand")
    @ApiOperation(value = "编辑预立项待定状态")
    public AjaxResult updateProjectBeforehand(@RequestBody BaProjectBeforehand baProjectBeforehand)
    {
        return toAjax(baProjectBeforehandService.updateProjectBeforehand(baProjectBeforehand));
    }

    /**
     * 预立项序号
     */
    @ApiOperation("预立项序号")
    @GetMapping("/projectSerial")
    public String projectSerial() {

        return baProjectBeforehandService.projectSerial();

    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO) {
        int result = baProjectBeforehandService.updateProcessBusinessData(baProjectInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * APP业务首页预立项
     */
    @GetMapping("/appHomeProjectBeforehandStat")
    @ApiOperation(value = "APP业务首页预立项")
    @Anonymous
    public UR appHomeProjectBeforehandStat(BaProjectBeforehand baProjectBeforehand){
        //租户ID
        baProjectBeforehand.setTenantId(SecurityUtils.getCurrComId());
        UR ur = baProjectBeforehandService.appHomeProjectBeforehandStat(baProjectBeforehand);
        return ur;
    }

    /**
     * 新增进度信息
     */
    @PreAuthorize("@ss.hasPermi('admin:beforehand:progressInformationAdd')")
    @Log(title = "进度信息", businessType = BusinessType.INSERT)
    @PostMapping("/progressInformationAdd")
    @ApiOperation(value = "新增进度信息接口")
    public AjaxResult add(@RequestBody BaProgressInformation baProgressInformation)
    {
        return toAjax(baProjectBeforehandService.insertBaProgressInformation(baProgressInformation));
    }

}
