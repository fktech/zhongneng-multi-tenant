package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaOvertime;
import com.business.system.service.IOaOvertimeService;

/**
 * 加班申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaOvertime")
@Api(tags ="加班申请接口")
public class OaOvertimeController extends BaseController
{
    @Autowired
    private IOaOvertimeService oaOvertimeService;

    /**
     * 查询加班申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOvertime:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询加班申请列表接口")
    public TableDataInfo list(OaOvertime oaOvertime)
    {
        //租户
        oaOvertime.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaOvertime> list = oaOvertimeService.selectOaOvertimeList(oaOvertime);
        return getDataTable(list);
    }

    /**
     * 导出加班申请列表
     */
  /*  @PreAuthorize("@ss.hasPermi('admin:oaOvertime:export')")
    @Log(title = "加班申请", businessType = BusinessType.EXPORT)*/
    @PostMapping("/export")
    @ApiOperation(value = "导出加班申请列表接口")
    public void export(HttpServletResponse response, OaOvertime oaOvertime) throws IOException
    {
        //租户
        oaOvertime.setTenantId(SecurityUtils.getCurrComId());
        List<OaOvertime> list = oaOvertimeService.selectOaOvertimeList(oaOvertime);
        ExcelUtil<OaOvertime> util = new ExcelUtil<OaOvertime>(OaOvertime.class);
        util.exportExcel(response, list, "oaOvertime");
    }

    /**
     * 获取加班申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOvertime:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取加班申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaOvertimeService.selectOaOvertimeById(id));
    }

    /**
     * 新增加班申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaOvertime:add')")
    @Log(title = "加班申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增加班申请接口")
    public AjaxResult add(@RequestBody OaOvertime oaOvertime)
    {
        int result = oaOvertimeService.insertOaOvertime(oaOvertime);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改加班申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaOvertime:edit')")
    @Log(title = "加班申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改加班申请接口")
    public AjaxResult edit(@RequestBody OaOvertime oaOvertime)
    {
        int result = oaOvertimeService.updateOaOvertime(oaOvertime);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除加班申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaOvertime:remove')")
    @Log(title = "加班申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除加班申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaOvertimeService.deleteOaOvertimeByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOvertime:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaOvertimeService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
