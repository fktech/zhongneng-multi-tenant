package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.core.domain.UR;
import com.business.common.utils.StringUtils;
import com.business.system.domain.vo.OrderCordinate;
import com.im.open.client.sdk.ZczyApiClient;
import com.im.open.client.sdk.ZczyClient;
import com.im.open.client.sdk.request.OrderCordinateRequest;
import com.im.open.client.sdk.response.OrderCordinateResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaShippingOrderCmst;
import com.business.system.service.IBaShippingOrderCmstService;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.core.page.TableDataInfo;

/**
 * 中储智运发运单Controller
 *
 * @author single
 * @date 2023-08-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/cmst")
@Api(tags ="中储智运发运单接口")
public class BaShippingOrderCmstController extends BaseController
{
    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    @Value("${url}")
    private String url;

    @Value("${appKey}")
    private String appKey;

    @Value("${appSecret}")
    private String appSecret;

    @Value("${publicKey}")
    private String publicKey;

    @Value("${consignorId}")
    private String consignorId;

    /**
     * 查询中储智运发运单列表
     */
   /* @PreAuthorize("@ss.hasPermi('admin:cmst:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询中储智运发运单列表接口")
    public TableDataInfo list(BaShippingOrderCmst baShippingOrderCmst)
    {
        startPage();
        List<BaShippingOrderCmst> list = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst);
        return getDataTable(list);
    }

    /**
     * 导出中储智运发运单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:export')")
    @Log(title = "中储智运发运单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出中储智运发运单列表接口")
    public void export(HttpServletResponse response, BaShippingOrderCmst baShippingOrderCmst) throws IOException
    {
        List<BaShippingOrderCmst> list = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst);
        ExcelUtil<BaShippingOrderCmst> util = new ExcelUtil<BaShippingOrderCmst>(BaShippingOrderCmst.class);
        util.exportExcel(response, list, "cmst");
    }

    /**
     * 获取中储智运发运单详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:cmst:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取中储智运发运单详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baShippingOrderCmstService.selectBaShippingOrderCmstById(id));
    }

    /**
     * 新增中储智运发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:add')")
    @Log(title = "中储智运发运单", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增中储智运发运单接口")
    public AjaxResult add(@RequestBody BaShippingOrderCmst baShippingOrderCmst)
    {
        return toAjax(baShippingOrderCmstService.insertBaShippingOrderCmst(baShippingOrderCmst));
    }

    /**
     * 修改中储智运发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:edit')")
    @Log(title = "中储智运发运单", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改中储智运发运单接口")
    public AjaxResult edit(@RequestBody BaShippingOrderCmst baShippingOrderCmst)
    {
        return toAjax(baShippingOrderCmstService.updateBaShippingOrderCmst(baShippingOrderCmst));
    }

    /**
     * 删除中储智运发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:remove')")
    @Log(title = "中储智运发运单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除中储智运发运单接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baShippingOrderCmstService.deleteBaShippingOrderCmstByIds(ids));
    }

    /**
     * 在途轨迹
     */
    @GetMapping("/trajectory")
    @ApiOperation(value = "在途轨迹")
    public UR trajectory(OrderCordinate orderCordinate) throws Exception{

        String format = "json";


        OrderCordinateRequest orderCordinateRequest = new OrderCordinateRequest();
        //订单号
        if(StringUtils.isNotEmpty(orderCordinate.getOrderId())){
            orderCordinateRequest.setOrderId(orderCordinate.getOrderId());
        }
        //开始时间
        if(orderCordinate.getCreatedStartTime() != null){
            orderCordinateRequest.setCreatedStartTime(orderCordinate.getCreatedStartTime());
        }
        //结束时间
        if(orderCordinate.getCreatedEndTime() != null){
            orderCordinateRequest.setCreatedEndTime(orderCordinate.getCreatedEndTime());
        }

        //构建客户端
        ZczyClient client = new ZczyApiClient(url, appKey, appSecret, publicKey, format);

        OrderCordinateResponse execute = client.execute(orderCordinateRequest,consignorId);
        String code = execute.getCode();
        //响应提示信息
        String message = execute.getMessage();
        //返回数据
        OrderCordinateResponse.Result result = execute.getResult();
        return UR.ok().code(Integer.valueOf(code)).message(message).data("result",result);
    }
}
