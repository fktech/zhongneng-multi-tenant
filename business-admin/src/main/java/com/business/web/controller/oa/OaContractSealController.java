package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.OaContractSealInstanceRelatedEditDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaContractSeal;
import com.business.system.service.IOaContractSealService;

/**
 * oa合同用印Controller
 *
 * @author single
 * @date 2024-06-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/contractSeal")
@Api(tags ="oa合同用印接口")
public class OaContractSealController extends BaseController
{
    @Autowired
    private IOaContractSealService oaContractSealService;

    /**
     * 查询oa合同用印列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractSeal:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询oa合同用印列表接口")
    public TableDataInfo list(OaContractSeal oaContractSeal)
    {
        startPage();
        List<OaContractSeal> list = oaContractSealService.selectOaContractSealList(oaContractSeal);
        return getDataTable(list);
    }

    /**
     * 导出oa合同用印列表
     */
    @PreAuthorize("@ss.hasPermi('admin:contractSeal:export')")
    @Log(title = "oa合同用印", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出oa合同用印列表接口")
    public void export(HttpServletResponse response, OaContractSeal oaContractSeal) throws IOException
    {
        List<OaContractSeal> list = oaContractSealService.selectOaContractSealList(oaContractSeal);
        ExcelUtil<OaContractSeal> util = new ExcelUtil<OaContractSeal>(OaContractSeal.class);
        util.exportExcel(response, list, "contractSeal");
    }

    /**
     * 获取oa合同用印详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractSeal:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取oa合同用印详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaContractSealService.selectOaContractSealById(id));
    }

    /**
     * 新增oa合同用印
     */
    @PreAuthorize("@ss.hasPermi('admin:contractSeal:add')")
    @Log(title = "oa合同用印", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增oa合同用印接口")
    public AjaxResult add(@RequestBody OaContractSeal oaContractSeal)
    {
        int result = oaContractSealService.insertOaContractSeal(oaContractSeal);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改oa合同用印
     */
    @PreAuthorize("@ss.hasPermi('admin:contractSeal:edit')")
    @Log(title = "oa合同用印", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改oa合同用印接口")
    public AjaxResult edit(@RequestBody OaContractSeal oaContractSeal)
    {
        int result = oaContractSealService.updateOaContractSeal(oaContractSeal);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除oa合同用印
     */
    @PreAuthorize("@ss.hasPermi('admin:contractSeal:remove')")
    @Log(title = "oa合同用印", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除oa合同用印接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaContractSealService.deleteOaContractSealByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEgress:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaContractSealService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody OaContractSealInstanceRelatedEditDTO oaContractSealInstanceRelatedEditDTO) {
        int result = oaContractSealService.updateProcessBusinessData(oaContractSealInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }
        return toAjax(result);
    }
}
