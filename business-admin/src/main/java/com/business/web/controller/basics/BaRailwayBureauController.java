package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaRailwayBureau;
import com.business.system.service.IBaRailwayBureauService;

/**
 * 铁路局Controller
 *
 * @author ljb
 * @date 2023-01-16
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/bureau")
@Api(tags ="铁路局接口")
public class BaRailwayBureauController extends BaseController
{
    @Autowired
    private IBaRailwayBureauService baRailwayBureauService;

    /**
     * 查询铁路局列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:bureau:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询铁路局列表接口")
    public TableDataInfo list(BaRailwayBureau baRailwayBureau)
    {
        startPage();
        List<BaRailwayBureau> list = baRailwayBureauService.selectBaRailwayBureauList(baRailwayBureau);
        return getDataTable(list);
    }

    /**
     * 导出铁路局列表
     */
    @PreAuthorize("@ss.hasPermi('admin:bureau:export')")
    @Log(title = "铁路局", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出铁路局列表接口")
    public void export(HttpServletResponse response, BaRailwayBureau baRailwayBureau) throws IOException
    {
        List<BaRailwayBureau> list = baRailwayBureauService.selectBaRailwayBureauList(baRailwayBureau);
        ExcelUtil<BaRailwayBureau> util = new ExcelUtil<BaRailwayBureau>(BaRailwayBureau.class);
        util.exportExcel(response, list, "bureau");
    }

    /**
     * 获取铁路局详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:bureau:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取铁路局详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baRailwayBureauService.selectBaRailwayBureauById(id));
    }

    /**
     * 新增铁路局
     */
    @PreAuthorize("@ss.hasPermi('admin:bureau:add')")
    @Log(title = "铁路局", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增铁路局接口")
    public AjaxResult add(@RequestBody BaRailwayBureau baRailwayBureau)
    {
        return toAjax(baRailwayBureauService.insertBaRailwayBureau(baRailwayBureau));
    }

    /**
     * 修改铁路局
     */
    @PreAuthorize("@ss.hasPermi('admin:bureau:edit')")
    @Log(title = "铁路局", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改铁路局接口")
    public AjaxResult edit(@RequestBody BaRailwayBureau baRailwayBureau)
    {
        return toAjax(baRailwayBureauService.updateBaRailwayBureau(baRailwayBureau));
    }

    /**
     * 删除铁路局
     */
    @PreAuthorize("@ss.hasPermi('admin:bureau:remove')")
    @Log(title = "铁路局", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除铁路局接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baRailwayBureauService.deleteBaRailwayBureauByIds(ids));
    }
}
