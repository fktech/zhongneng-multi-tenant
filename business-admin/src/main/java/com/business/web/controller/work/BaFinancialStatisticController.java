package com.business.web.controller.work;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.annotation.Anonymous;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.*;
import com.business.system.service.*;
import com.business.system.service.workflow.IBaDriveAiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *智慧驾驶舱财务统计Controller
 *
 * @author songkai
 * @date 2023-4-20
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/FinancialStatistic")
@Api(tags ="智慧驾驶舱财务统计接口")
public class BaFinancialStatisticController extends BaseController {
    @Autowired
    private IBaClaimService iBaClaimService;
    @Autowired
    private IBaCollectionService iBaCollectionService;
    @Autowired
    private IBaPaymentService iBaPaymentService;
    @Autowired
    private  IBaProjectService iBaProjectService;

    @Autowired
    private  IBaInvoiceService iBaInvoiceService;
    @Autowired
    private  IBaInvoiceDetailService iBaInvoiceDetailService;
    @Autowired
    private IBaSettlementService iBaSettlementService;
    @Autowired
    private ISysDictDataService iSysDictDataService;
    @Autowired
    private IBaContractStartService iBaContractStartService;
    @Autowired
    private IBaClaimHistoryService iBaClaimHistoryService;

    @Autowired
    private IBaInvoiceService baInvoiceService;

    /**
     * 查询财务统计中销售额
     */
    @GetMapping("/SelectSalesVolume")
    @ApiOperation(value = "查询财务统计中销售额")
    public AjaxResult SelectSalesVolume() {
        //查询收款累计金额
        BaInvoice baInvoice = new BaInvoice();
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        //类型
        baInvoice.setInvoiceType("1");
        NumsAndMoneyDTO numsAndMoneyDTO4 = iBaInvoiceService.selectBaInvoiceForUp(baInvoice);
        //本月度
        NumsAndMoneyDTO numsAndMoneyDTO7 = iBaInvoiceService.selectBaInvoiceForMonth(baInvoice);
        //本年度
        NumsAndMoneyDTO numsAndMoneyDTO = iBaInvoiceService.selectBaInvoiceForYear(baInvoice);
        Map map=new HashMap();
        map.put("salesVolumeNums",numsAndMoneyDTO4.getAmounts());
        if(null != numsAndMoneyDTO7){
            map.put("salesVolumeMonth",numsAndMoneyDTO7.getAmounts());
        }else {
            map.put("salesVolumeMonth",0);
        }
        map.put("salesVolumeYear",numsAndMoneyDTO.getAmounts());
        return success(map);
    }

    /**
     * 查询财务统计中销售额
     */
    @GetMapping("/SelectTurnover")
    @ApiOperation(value = "查询财务统计中年度营业额")
    public AjaxResult SelectTurnover () {
        //按本月度之前12月查询营业额
        BaClaim baClaim = new BaClaim();
        baClaim.setClaimState("2");
        List<MonthSumDTO> monthSumDTOS = iBaClaimService.SelectSalesVolumeByEveryoneMonth(baClaim);
        //按本月度之前12月付款金额
        BaPayment baPayment=new BaPayment();
        baPayment.setPaymentState("payment_paid");
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        List<MonthSumDTO> monthSumDTOS1 = iBaPaymentService.SelectSalesVolumeByEveryoneMonth(baPayment);
        List<MonthSumDTO>  monthSumDTOS2=new ArrayList<>();
        //求毛利润
        for (MonthSumDTO monthsum:monthSumDTOS) {
            MonthSumDTO monthSumDTO=new MonthSumDTO();
            for (MonthSumDTO monthsum1:monthSumDTOS1) {
                //月份相同的相减求毛利润
                if((monthsum.getMonth()).equals(monthsum1.getMonth())){
                        BigDecimal subtract = monthsum.getCount().subtract(monthsum1.getCount());
                        monthSumDTO.setMonth(monthsum1.getMonth());
                        monthSumDTO.setCount(subtract);
                        if(subtract.compareTo(new BigDecimal(0.00))==-1){
                            monthSumDTO.setCount(new BigDecimal(0.00));
                        }
                }
            }
            monthSumDTOS2.add(monthSumDTO);
        }
        Map map=new HashMap();
        map.put("turnovers",monthSumDTOS);
        map.put("maoNums",monthSumDTOS2);
        map.put("expenditureNums",monthSumDTOS1);

        return success(map);
    }
    /**
     * 查询财务统计中项目收益
     */
    @GetMapping("/SelectProjectIncome")
    @ApiOperation(value = "查询财务统计项目收益")
    public AjaxResult SelectProjectIncome () {
        List<BaProjectDTO> baProjectDTOS = iBaProjectService.baProjectDTOList1();
        return  success(baProjectDTOS);
    }
    /**
     * 查询财务统计中项目收益
     */
    @GetMapping("/SelectInvoiceAndMoney")
    @ApiOperation(value = "账款及发票统计接口")
    public AjaxResult SelectInvoiceAndMoney () {
        //String s="invoice:pass";
        //销项开票的1列表
        List<BaInvoice> baInvoices = iBaInvoiceService.selectBaInvoiceNums(AdminCodeEnum.INVOICE_STATUS_PASS.getCode());
        int InvoiceNUms=0;
        for (BaInvoice balnvoice:baInvoices) {
            //依靠开票id查询开票的
            int i = iBaInvoiceDetailService.selectBaInvoiceDetailByinviceId(balnvoice.getId());
            InvoiceNUms+=i;
        }
        //进项的票列表
       // String s1="inputInvoice:pass";
        List<BaInvoice> baInvoices1 = iBaInvoiceService.selectBaInvoiceNums(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
        for (BaInvoice balnvoice1:baInvoices1) {
            //依靠开票id查询开票的
            int j = iBaInvoiceDetailService.selectBaInvoiceDetailByinviceId(balnvoice1.getId());
            InvoiceNUms+=j;
        }
        //查询本月开票数及开票金额
        BaInvoice baInvoice=new BaInvoice();
        baInvoice.setFlag(0L);
        baInvoice.setState(AdminCodeEnum.INVOICE_STATUS_PASS.getCode());
        baInvoice.setInvoiceState("2");
        baInvoice.setInvoiceType("1");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        List<BaInvoice> baInvoices2 = iBaInvoiceService.selectNumsAmountByMonth(baInvoice);
        NumsAndMoneyDTO numsAndMoneyDTO1=new NumsAndMoneyDTO();
        BigDecimal add = new BigDecimal("0.00");
        for (BaInvoice baInvoice1:baInvoices2) {
            NumsAndMoneyDTO numsAndMoneyDTO = iBaInvoiceDetailService.selectDetailByinviceIdMonth(baInvoice1.getId());
           // num+=numsAndMoneyDTO.getNums();
            numsAndMoneyDTO1.setNums(numsAndMoneyDTO1.getNums()+numsAndMoneyDTO.getNums());
            if(numsAndMoneyDTO.getAmounts()==null){
                numsAndMoneyDTO.setAmounts(new BigDecimal("0.00"));
            }
            add = add.add(numsAndMoneyDTO.getAmounts());
            numsAndMoneyDTO1.setAmounts(add);
        }
        //查询本月shou票数及开票金额
        baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
        baInvoice.setInvoiceState("1");
        baInvoice.setInvoiceType("2");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        List<BaInvoice> baInvoices3 = iBaInvoiceService.selectNumsAmountByMonth(baInvoice);
        NumsAndMoneyDTO numsAndMoneyDTO2=new NumsAndMoneyDTO();
        BigDecimal addshou = new BigDecimal("0.00");
        for (BaInvoice baInvoice1:baInvoices3) {
            NumsAndMoneyDTO numsAndMoneyDTO3 = iBaInvoiceDetailService.selectDetailByinviceIdMonth(baInvoice1.getId());
            // num+=numsAndMoneyDTO.getNums();
            numsAndMoneyDTO2.setNums(numsAndMoneyDTO2.getNums()+numsAndMoneyDTO3.getNums());
            if(numsAndMoneyDTO3.getAmounts()==null){
                numsAndMoneyDTO3.setAmounts(new BigDecimal("0.00"));
            }
            addshou = addshou.add(numsAndMoneyDTO3.getAmounts());
            numsAndMoneyDTO2.setAmounts(addshou);
        }
        //查询应付金额的总数及金额（结算单上游）
        BaSettlement baSettlement=new BaSettlement();
        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
        baSettlement.setFlag(0);
        baSettlement.setSettlementType(1L);
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        NumsAndMoneyDTO numsAndMoneyDTO4 = iBaSettlementService.selectBaSettlementforup(baSettlement);
        //  //查询应付金额的总数及金额（结算单上游）本月度
        NumsAndMoneyDTO numsAndMoneyDTO5 = iBaSettlementService.selectBaSettlementforMonth(baSettlement);
        baSettlement.setSettlementType(2L);
        NumsAndMoneyDTO numsAndMoneyDTO6 = iBaSettlementService.selectBaSettlementforup(baSettlement);
        NumsAndMoneyDTO numsAndMoneyDTO7 = iBaSettlementService.selectBaSettlementforMonth(baSettlement);
        Map map=new HashMap();
        map.put("drawbills",numsAndMoneyDTO1);
        map.put("receipt",numsAndMoneyDTO2);
        map.put("settlementforup",numsAndMoneyDTO4);
        map.put("settlementforupMonth",numsAndMoneyDTO5);
        map.put("settlementforout",numsAndMoneyDTO6);
        map.put("settlementforoutMOnth",numsAndMoneyDTO7);
        map.put("total",InvoiceNUms);
        return success(map);
    }
    /**
     * 查询收入与支付的总金额
     */
    @GetMapping("/SelectIncomeAndExpenditure")
    @ApiOperation(value = "收入与支出的接口")
    public AjaxResult SelectIncomeAndExpenditure () {
        Map map=new HashMap();
        //查询收入总金额
        BigDecimal decimal = iBaCollectionService.selectBaCollectionByyear();
        map.put("IncomeMoneys",decimal);
        //查询支付总金额
        BigDecimal decimal1 = iBaPaymentService.SelectSalesVolumeByyear();
        map.put("ExpenditureMoneys",decimal1);
        //支出类型占比
           //查询支付时的类型
        String type="payment_type";
        List<SysDictData> sysDictData = iSysDictDataService.selectDictDataByType(type);
        List<TypeProportionDTO> proportionDTOList=new ArrayList<>();
        for (SysDictData sysDictData1:sysDictData) {
            TypeProportionDTO typeProportionDTO=new TypeProportionDTO();
            typeProportionDTO.setTypeName(sysDictData1.getDictLabel());
            typeProportionDTO.setTypeValue(sysDictData1.getDictValue());
            //根据付款类型查询付款数据
            BigDecimal decimal2 = iBaPaymentService.SelectSalesVolumeByyearAndType(sysDictData1.getDictValue());
            BigDecimal divide = new BigDecimal("0.00");
            if(decimal2!=null){
                divide = (decimal2.divide(decimal1, 4, BigDecimal.ROUND_UP)).multiply(new BigDecimal(100));
            }
            typeProportionDTO.setProportion(divide);
            proportionDTOList.add(typeProportionDTO);
        }
        //求所占类型占比的总和
        BigDecimal divide1 = new BigDecimal("0.00");
        for(TypeProportionDTO typeProportionDTO:proportionDTOList){
            divide1=divide1.add(typeProportionDTO.getProportion());
        }
        //判断占比是不是等于100
        List<TypeProportionDTO> proportionDTOList1=new ArrayList<>();
        if(divide1.compareTo(new BigDecimal("100"))!=0){
            for(int i=0;i<proportionDTOList.size();i++){
                if(proportionDTOList.get(i).getProportion().compareTo(new BigDecimal("0.00"))!=0){
                    proportionDTOList1.add(proportionDTOList.get(i));
                }
            }
        }
        BigDecimal divide2 = new BigDecimal("0.00");
        for(int i=0;i<proportionDTOList1.size();i++){
            if(i<proportionDTOList1.size()-1){
                divide2=divide2.add(proportionDTOList1.get(i).getProportion());
            }
            if(i==proportionDTOList1.size()-1){
                proportionDTOList1.get(i).setProportion(new BigDecimal(100).subtract(divide2));
            }
        }
        for (TypeProportionDTO typeProportionDTO:proportionDTOList) {
            for (TypeProportionDTO typeProportionDTO1:proportionDTOList1)
                if(typeProportionDTO.getTypeValue().equals(typeProportionDTO1.getTypeValue())){
                    typeProportionDTO.setProportion(typeProportionDTO1.getProportion());
                }
        }
        map.put("typeProportion",proportionDTOList);
        //每个项目业务类型占收入的比例
        //查询支付时的类型
        String projrctype="business_type";
        List<SysDictData> sysDictData1= iSysDictDataService.selectDictDataByType(projrctype);
        List<TypeProportionDTO> proportionDTOList2=new ArrayList<>();
        //根据业务类型查询审核通过的立项项目
        for(SysDictData sysDictData2:sysDictData1){
            TypeProportionDTO typeProportionDTO4=new TypeProportionDTO();
            typeProportionDTO4.setTypeName(sysDictData2.getDictLabel());
            typeProportionDTO4.setTypeValue(sysDictData2.getDictValue());
            BigDecimal applyCollectionAmount =new BigDecimal(0);
            BaProject baProject=new BaProject();
            baProject.setState(AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
            baProject.setFlag(0);
            baProject.setBusinessType(sysDictData2.getDictValue());
            List<BaProject> baProjects = iBaProjectService.selectBaProjectList(baProject);
            for (BaProject baProject1:baProjects) {
                //根据立项项目查询合同
                BaContractStart baContractStart=new BaContractStart();
                baContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
                baContractStart.setFlag(0L);
                baContractStart.setProjectId(baProject1.getId());
                List<BaContractStart> baContractStarts = iBaContractStartService.selectBaContractStartList(baContractStart);
                //根据合同id查询订单列表
                for (BaContractStart baContractStart1:baContractStarts) {
                    BaClaimHistory baClaimHistory=new BaClaimHistory();
                    baClaimHistory.setOrderId(baContractStart1.getId());
                    List<BaClaimHistory> baClaimHistories = iBaClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
                    BigDecimal decimal2 = iBaClaimService.SelectSalesVolumeBybinusstype(baClaimHistories);
                    applyCollectionAmount=applyCollectionAmount.add(decimal2);
                }

            }
            BigDecimal divide5 = new BigDecimal("0.00");
            if(applyCollectionAmount!=null){
                divide5 = (applyCollectionAmount.divide(decimal, 4, BigDecimal.ROUND_UP)).multiply(new BigDecimal(100));
            }
            typeProportionDTO4.setProportion(divide5);
            proportionDTOList2.add(typeProportionDTO4);
        }
        //求所占类型占比的总和
        BigDecimal divide3 = new BigDecimal("0.00");
        for(TypeProportionDTO typeProportionDTO5:proportionDTOList2){
            divide3=divide1.add(typeProportionDTO5.getProportion());
        }
        //判断占比是不是等于100
        List<TypeProportionDTO> proportionDTOList3=new ArrayList<>();
        if(divide1.compareTo(new BigDecimal("100"))!=0){
            for(int i=0;i<proportionDTOList2.size();i++){
                if(proportionDTOList2.get(i).getProportion().compareTo(new BigDecimal("0.00"))!=0){
                    proportionDTOList3.add(proportionDTOList2.get(i));
                }
            }
        }
        BigDecimal divide4 = new BigDecimal("0.00");
        for(int i=0;i<proportionDTOList3.size();i++){
            if(i<proportionDTOList3.size()-1){
                divide4=divide4.add(proportionDTOList3.get(i).getProportion());
            }
            if(i==proportionDTOList3.size()-1){
                proportionDTOList3.get(i).setProportion(new BigDecimal(100).subtract(divide4));
            }
        }
        for (TypeProportionDTO typeProportionDTO2:proportionDTOList2) {
            for (TypeProportionDTO typeProportionDTO3:proportionDTOList3)
                if(typeProportionDTO2.getTypeValue().equals(typeProportionDTO3.getTypeValue())){
                    typeProportionDTO2.setProportion(typeProportionDTO3.getProportion());
                }
        }
        map.put("businesstype",proportionDTOList2);
        return success(map);
    }

    /**
     * 发票统计
     */
    @PostMapping("/sumInvoiceAmount")
    @ApiOperation("发票统计")
    @Anonymous
    public AjaxResult sumInvoiceAmount(@RequestBody BaInvoice baInvoice){
        BaInvoiceStatVO baInvoiceStatVO = baInvoiceService.sumInvoiceAmount(baInvoice);
        return AjaxResult.success(baInvoiceStatVO);
    }

    /**
     * 纳税金额统计
     */
    @GetMapping("/sumTaxAmount")
    @ApiOperation("纳税金额统计")
    @Anonymous
    public AjaxResult sumTaxAmount(){
        BaInvoiceVO baInvoiceVO = baInvoiceService.sumTaxAmount();
        return AjaxResult.success(baInvoiceVO);
    }

    /**
     * 可视化大屏统计应收账款
     */
    @PostMapping("/sumBaCollection")
    @ApiOperation("可视化大屏统计应收账款")
    @Anonymous
    public AjaxResult sumBaCollection(){
        BaCollectionStatVO baCollectionStatVO = iBaCollectionService.sumBaCollection();
        return AjaxResult.success(baCollectionStatVO);
    }

    /**
     * 可视化大屏统计应付账款
     */
    @PostMapping("/sumBaPayment")
    @ApiOperation("可视化大屏统计应付账款")
    @Anonymous
    public AjaxResult sumBaPayment(){
        BaPaymentStatVO baPaymentStatVO = iBaPaymentService.sumBaPayment();
        return AjaxResult.success(baPaymentStatVO);
    }

    /**
     * 可视化大屏收入占比
     */
    @GetMapping("/incomeProportion")
    @ApiOperation(value = "可视化大屏收入占比")
    public AjaxResult incomeProportion () {
        Map<String, BigDecimal> map = new HashMap<>();
        //查询收入总金额
        BigDecimal decimal = iBaCollectionService.selectBaCollectionByyear();
        map.put("IncomeMoneys",decimal);
        //查询各类型合同启动
        BaContractStartTonVO baContractStartTonVO = new BaContractStartTonVO();
        baContractStartTonVO.setTenantId(SecurityUtils.getCurrComId());
        //到厂
        baContractStartTonVO.setType("arriveTon");
        List<BaContractStartTonVO> baContractStartTonVOS = iBaContractStartService.contractStartListByType(baContractStartTonVO);
        //到厂收入总额
        BigDecimal arriveTonIncome = new BigDecimal(0);
        if(baContractStartTonVOS.size() > 0){
            for (BaContractStartTonVO contractStartTonVO:baContractStartTonVOS) {
                BaClaimHistory baClaimHistory=new BaClaimHistory();
                baClaimHistory.setOrderId(contractStartTonVO.getStartId());
                List<BaClaimHistory> baClaimHistories = iBaClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
                BigDecimal decimal2 = iBaClaimService.SelectSalesVolumeBybinusstype(baClaimHistories);
                arriveTonIncome=arriveTonIncome.add(decimal2);
            }
        }
        map.put("arriveTonIncome",arriveTonIncome);
        //车板
        baContractStartTonVO.setType("vehicleTon");
        List<BaContractStartTonVO> vehicleTonVOS = iBaContractStartService.contractStartListByType(baContractStartTonVO);
        //车板收入总额
        BigDecimal vehicleTonIncome = new BigDecimal(0);
        if(vehicleTonVOS.size() > 0){
            for (BaContractStartTonVO contractStartTonVO:vehicleTonVOS) {
                BaClaimHistory baClaimHistory=new BaClaimHistory();
                baClaimHistory.setOrderId(contractStartTonVO.getStartId());
                List<BaClaimHistory> baClaimHistories = iBaClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
                BigDecimal decimal2 = iBaClaimService.SelectSalesVolumeBybinusstype(baClaimHistories);
                vehicleTonIncome=vehicleTonIncome.add(decimal2);
            }
        }
        map.put("vehicleTonIncome",vehicleTonIncome);
        //运费
        baContractStartTonVO.setType("arriveFreightTon");
        List<BaContractStartTonVO> arriveFreightTonVOS = iBaContractStartService.contractStartListByType(baContractStartTonVO);
        //进场运费收入总额
        BigDecimal arriveFreightTonIncome = new BigDecimal(0);
        if(arriveFreightTonVOS.size() > 0){
            for (BaContractStartTonVO contractStartTonVO:arriveFreightTonVOS) {
                BaClaimHistory baClaimHistory=new BaClaimHistory();
                baClaimHistory.setOrderId(contractStartTonVO.getStartId());
                List<BaClaimHistory> baClaimHistories = iBaClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
                BigDecimal decimal2 = iBaClaimService.SelectSalesVolumeBybinusstype(baClaimHistories);
                arriveFreightTonIncome=arriveFreightTonIncome.add(decimal2);
            }
        }
        map.put("arriveFreightTonIncome",arriveFreightTonIncome);
        //代采
        baContractStartTonVO.setType("replacePurchaseTon");
        List<BaContractStartTonVO> replacePurchaseTonVOS = iBaContractStartService.contractStartListByType(baContractStartTonVO);
        //车板收入总额
        BigDecimal replacePurchaseTonIncome = new BigDecimal(0);
        if(replacePurchaseTonVOS.size() > 0){
            for (BaContractStartTonVO contractStartTonVO:replacePurchaseTonVOS) {
                BaClaimHistory baClaimHistory=new BaClaimHistory();
                baClaimHistory.setOrderId(contractStartTonVO.getStartId());
                List<BaClaimHistory> baClaimHistories = iBaClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
                BigDecimal decimal2 = iBaClaimService.SelectSalesVolumeBybinusstype(baClaimHistories);
                replacePurchaseTonIncome=replacePurchaseTonIncome.add(decimal2);
            }
        }
        map.put("replacePurchaseTonIncome",replacePurchaseTonIncome);
        return success(map);
    }
}
