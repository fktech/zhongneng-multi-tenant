package com.business.web.controller.file;

import com.aliyun.oss.model.OSSObjectSummary;
import com.business.common.annotation.Anonymous;
import com.business.system.domain.FileUploadResult;
import com.business.system.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 文件上传前端控制器
 *
 * @author jiahe
 * @date 2020.7
 */
@RestController
@RequestMapping("/ossService")
@CrossOrigin //跨域
@Api(tags = "阿里云oss上传/下载文件接口")
public class OssController {

    @Autowired //注入Service
    private OssService ossService;

    //上传图片
    /**
     *  实名认证，收绒，发布，企业
     */
    @ApiOperation("上传图片/文件接口")
    @PostMapping("/Certification")
    @Anonymous
    public FileUploadResult Certification(@RequestPart("file") MultipartFile file, String type, HttpServletRequest request) throws Exception {
        //获取上传文件 MultipartFile
        //返回上传到oss的路径

        return this.ossService.Certification(file,type,request);
    }

    //根据文件名下载文件
    @ApiOperation("下载图片/文件接口")
    @GetMapping("/filedownload")
    @ResponseBody
    public void download(@RequestParam("fileName") String objectName, HttpServletResponse response) throws IOException {
        //通知浏览器以附件形式下载
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(objectName.getBytes(),
                        StandardCharsets.ISO_8859_1));
        this.ossService.downloadOssFile(response.getOutputStream(),objectName);
    }


    //上传文件
    @ApiOperation("上传文件接口")
    //@PostMapping("/fileupload")
    public FileUploadResult uploadOssFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        //获取上传文件 MultipartFile
        //返回上传到oss的路径
       return this.ossService.uploadFile(file,request);
    }

    //根据文件名删除文件
    @ApiOperation("删除文件接口")
    @RequestMapping("/filedelete")
    @ResponseBody
    public FileUploadResult delete(@RequestParam("fileName") String objectName) throws Exception {
        return this.ossService.delete(objectName);
    }

    //查询oss上所有文件
    @ApiOperation("查询所有文件接口")
    @RequestMapping("/filelist")
    @ResponseBody
    public List<OSSObjectSummary> list() throws Exception {
        return this.ossService.list();
    }

    //上传app
    @ApiOperation("上传app接口")
    @PostMapping("/uploadApp")
    public FileUploadResult uploadApp(@RequestPart("file") MultipartFile file, HttpServletRequest request) throws Exception {
        //获取上传文件 MultipartFile
        //返回上传到oss的路径
        return this.ossService.uploadApp(file,request);
    }




}
