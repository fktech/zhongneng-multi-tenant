package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaContractForward;
import com.business.system.service.IBaContractForwardService;


/**
 * 合同结转Controller
 *
 * @author ljb
 * @date 2023-03-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/contractForward")
@Api(tags ="合同结转接口")
public class BaContractForwardController extends BaseController
{
    @Autowired
    private IBaContractForwardService baContractForwardService;

    /**
     * 查询合同结转列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractForward:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询合同结转列表接口")
    public TableDataInfo list(BaContractForward baContractForward)
    {
        //租户ID
        baContractForward.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaContractForward> list = baContractForwardService.selectBaContractForwardList(baContractForward);
        return getDataTable(list);
    }

    /**
     * 导出合同结转列表
     */
    @PreAuthorize("@ss.hasPermi('admin:contractForward:export')")
    @Log(title = "合同结转", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出合同结转列表接口")
    public void export(HttpServletResponse response, BaContractForward baContractForward) throws IOException
    {
        List<BaContractForward> list = baContractForwardService.selectBaContractForwardList(baContractForward);
        ExcelUtil<BaContractForward> util = new ExcelUtil<BaContractForward>(BaContractForward.class);
        util.exportExcel(response, list, "contractForward");
    }

    /**
     * 获取合同结转详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:contractForward:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取合同结转详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baContractForwardService.selectBaContractForwardById(id));
    }

    /**
     * 新增合同结转
     */
    @PreAuthorize("@ss.hasPermi('admin:contractForward:add')")
    @Log(title = "合同结转", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增合同结转接口")
    public AjaxResult add(@RequestBody BaContractForward baContractForward)
    {
        int result = baContractForwardService.insertBaContractForward(baContractForward);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改合同结转
     */
    @PreAuthorize("@ss.hasPermi('admin:contractForward:edit')")
    @Log(title = "合同结转", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改合同结转接口")
    public AjaxResult edit(@RequestBody BaContractForward baContractForward)
    {
        int result = baContractForwardService.updateBaContractForward(baContractForward);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除合同结转
     */
    @PreAuthorize("@ss.hasPermi('admin:contractForward:remove')")
    @Log(title = "合同结转", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除合同结转接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baContractForwardService.deleteBaContractForwardByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:contractStart:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baContractForwardService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
