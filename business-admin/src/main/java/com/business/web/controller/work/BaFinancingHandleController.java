package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaFinancingHandle;
import com.business.system.service.IBaFinancingHandleService;
/**
 * 融资办理Controller
 *
 * @author single
 * @date 2023-05-03
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/financingHandle")
@Api(tags ="融资办理接口")
public class BaFinancingHandleController extends BaseController
{
    @Autowired
    private IBaFinancingHandleService baFinancingHandleService;

    /**
     * 查询融资办理列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:financingHandle:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询融资办理列表接口")
    public TableDataInfo list(BaFinancingHandle baFinancingHandle)
    {
        startPage();
        List<BaFinancingHandle> list = baFinancingHandleService.selectBaFinancingHandleList(baFinancingHandle);
        return getDataTable(list);
    }

    /**
     * 导出融资办理列表
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:export')")
    @Log(title = "融资办理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出融资办理列表接口")
    public void export(HttpServletResponse response, BaFinancingHandle baFinancingHandle) throws IOException
    {
        List<BaFinancingHandle> list = baFinancingHandleService.selectBaFinancingHandleList(baFinancingHandle);
        ExcelUtil<BaFinancingHandle> util = new ExcelUtil<BaFinancingHandle>(BaFinancingHandle.class);
        util.exportExcel(response, list, "financingHandle");
    }

    /**
     * 获取融资办理详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取融资办理详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baFinancingHandleService.selectBaFinancingHandleById(id));
    }

    /**
     * 新增融资办理
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:add')")
    @Log(title = "融资办理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增融资办理接口")
    public AjaxResult add(@RequestBody BaFinancingHandle baFinancingHandle)
    {
        return toAjax(baFinancingHandleService.insertBaFinancingHandle(baFinancingHandle));
    }

    /**
     * 修改融资办理
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:edit')")
    @Log(title = "融资办理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改融资办理接口")
    public AjaxResult edit(@RequestBody BaFinancingHandle baFinancingHandle)
    {
        return toAjax(baFinancingHandleService.updateBaFinancingHandle(baFinancingHandle));
    }

    /**
     * 删除融资办理
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:remove')")
    @Log(title = "融资办理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除融资办理接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baFinancingHandleService.deleteBaFinancingHandleByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:financingHandle:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baFinancingHandleService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

}
