package com.business.web.controller.oa;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.system.domain.OaClaimDetail;
import com.business.system.service.IOaClaimDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 报销明细Controller
 *
 * @author single
 * @date 2023-12-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaClaimDetail")
@Api(tags ="报销明细接口")
public class OaClaimDetailController extends BaseController
{
    @Autowired
    private IOaClaimDetailService oaClaimDetailService;

    /**
     * 查询报销明细列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaimDetail:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询报销明细列表接口")
    public TableDataInfo list(OaClaimDetail oaClaimDetail)
    {
        startPage();
        List<OaClaimDetail> list = oaClaimDetailService.selectOaClaimDetailList(oaClaimDetail);
        return getDataTable(list);
    }

    /**
     * 获取报销明细详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaimDetail:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取报销明细详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClaimDetailService.selectOaClaimDetailById(id));
    }
}
