package com.business.web.controller.work;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDailyFollow;
import com.business.system.domain.OaClock;
import com.business.system.domain.dto.DailyDTO;
import com.business.system.domain.dto.UserDailyDTO;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaDaily;
import com.business.system.service.IBaDailyService;

/**
 * 日报Controller
 *
 * @author single
 * @date 2023-10-09
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/daily")
@Api(tags ="日报接口")
public class BaDailyController extends BaseController
{
    @Autowired
    private IBaDailyService baDailyService;

    /**
     * 查询日报列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询日报列表接口")
    public TableDataInfo list(BaDaily baDaily)
    {
        //startPage();
        List<BaDaily> list = baDailyService.selectBaDailyList(baDaily);
        return getDataTable(list);
    }

    /**
     * 导出日报列表
     */
    @PreAuthorize("@ss.hasPermi('admin:daily:export')")
    @Log(title = "日报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出日报列表接口")
    public void export(HttpServletResponse response, BaDaily baDaily) throws IOException
    {
        List<BaDaily> list = baDailyService.selectBaDailyList(baDaily);
        ExcelUtil<BaDaily> util = new ExcelUtil<BaDaily>(BaDaily.class);
        util.exportExcel(response, list, "daily");
    }

    /**
     * 获取日报详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取日报详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDailyService.selectBaDailyById(id));
    }

    /**
     * 新增日报
     */
    @Log(title = "日报", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增日报接口")
    public AjaxResult add(@RequestBody BaDaily baDaily)
    {
        int result = baDailyService.insertBaDaily(baDaily);
        if(result == 0){
            return AjaxResult.error(0,"保存数据已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改日报
     */
    @Log(title = "日报", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改日报接口")
    public AjaxResult edit(@RequestBody BaDaily baDaily)
    {
        return toAjax(baDailyService.updateBaDaily(baDaily));
    }

    /**
     * 删除日报
     */
    @Log(title = "日报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除日报接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDailyService.deleteBaDailyByIds(ids));
    }

    /**
     * 未提交数据统计
     */
    @GetMapping(value = "/notSubmitted")
    @ApiOperation(value = "获取未完成")
    public TableDataInfo notSubmitted(BaDaily baDaily)
    {
        List<UserDailyDTO> userDailyDTOList = baDailyService.notSubmitted(baDaily);
        return getDataTable(userDailyDTOList);
    }
    /**
     * 已完成日报整合
     */
    @GetMapping(value = "/completedDaily")
    @ApiOperation(value = "已完成日报")
    public TableDataInfo completedDaily(BaDaily baDaily)
    {
        //startPage();
        List<UserDailyDTO> userDailyDTOList = baDailyService.completedDaily(baDaily);
        return getDataTable(userDailyDTOList);
    }

    /**
     * 日报关注
     */
    @GetMapping("/dailyFollow")
    @ApiOperation(value = "日报关注")
    public AjaxResult dailyFollow(BaDailyFollow baDailyFollow)
    {
        return toAjax(baDailyService.dailyFollow(baDailyFollow));
    }

    /**
     * 关注点亮
     */
    @GetMapping("/dailyFollowList")
    @ApiOperation(value = "关注点亮")
    public TableDataInfo dailyFollowList(BaDailyFollow baDailyFollow)
    {
        return getDataTable(baDailyService.selectBaDailyFollowList(baDailyFollow));
    }

    /**
     * 取消关注
     */
    @GetMapping("/unFollow")
    @ApiOperation(value = "取消关注")
    public AjaxResult unFollow(BaDailyFollow baDailyFollow)
    {
        return toAjax(baDailyService.unFollow(baDailyFollow));
    }

    /**
     * 关注列表
     */
    @GetMapping(value = "/followList")
    @ApiOperation(value = "关注列表")
    public TableDataInfo followList(BaDaily baDaily)
    {
        List<UserDailyDTO> userDailyDTOList = baDailyService.followList(baDaily);
        return getDataTable(userDailyDTOList);
    }

    /**
     * 单个关注
     */
    @GetMapping("/singleFollow")
    @ApiOperation(value = "单个关注")
    public AjaxResult singleFollow(BaDailyFollow baDailyFollow)
    {
        return toAjax(baDailyService.singleFollow(baDailyFollow));
    }

    /**
     * 日报统计
     */
    @GetMapping("/dailyCount")
    @ApiOperation(value = "日报统计")
    public Map<String,Integer> dailyCount(BaDaily baDaily){
        //租户ID
        baDaily.setTenantId(SecurityUtils.getCurrComId());
        return baDailyService.dailyCount(baDaily);
    }

    /**
     * 报表统计
     */
    @GetMapping("/dailyStatistics")
    @ApiOperation(value = "报表统计")
    public TableDataInfo dailyStatistics(BaDaily baDaily)
    {
        //租户ID
        baDaily.setTenantId(SecurityUtils.getCurrComId());
        List<DailyDTO> list = baDailyService.dailyStatistics(baDaily);
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        TableDataInfo rspData =new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(myStartPage(list, pageNum, pageSize));
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    //列表分页
    public static List myStartPage(List list, Integer pageNum, Integer pageSize){
        List<Object> objects = new ArrayList<>();
        if(list ==null){
            return objects;
        }
        if(list.size()==0){
            return objects;
        }
        Integer count = list.size();//
        Integer pageCount =0;//
        if(count % pageSize ==0){
            pageCount = count / pageSize;
        }else{
            pageCount = count / pageSize +1;
        }
        int fromIndex =0;//
        int toIndex =0;//
        if(pageNum != pageCount){
            fromIndex =(pageNum -1)* pageSize;
            toIndex = fromIndex + pageSize;
        }else{
            fromIndex =(pageNum -1)* pageSize;
            toIndex = count;
        }
        List pageList = list.subList(fromIndex,toIndex);
        return pageList;
    }
}
