package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaEvectionTrip;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaEvection;
import com.business.system.service.IOaEvectionService;

/**
 * 出差申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaEvection")
@Api(tags ="出差申请接口")
public class OaEvectionController extends BaseController
{
    @Autowired
    private IOaEvectionService oaEvectionService;

    /**
     * 查询出差申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEvection:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询出差申请列表接口")
    public TableDataInfo list(OaEvection oaEvection)
    {
        //租户ID
        oaEvection.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaEvection> list = oaEvectionService.selectOaEvectionList(oaEvection);
        return getDataTable(list);
    }

    /**
     * 导出出差申请列表
     */
    @PreAuthorize("@ss.hasPermi('admin:oaEvection:export')")
    @Log(title = "出差申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出出差申请列表接口")
    public void export(HttpServletResponse response, OaEvection oaEvection) throws IOException
    {
        //租户ID
        oaEvection.setTenantId(SecurityUtils.getCurrComId());
        List<OaEvection> list = oaEvectionService.selectOaEvectionList(oaEvection);
        ExcelUtil<OaEvection> util = new ExcelUtil<OaEvection>(OaEvection.class);
        util.exportExcel(response, list, "oaEvection");
    }

    /**
     * 获取出差申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEvection:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取出差申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaEvectionService.selectOaEvectionById(id));
    }

    /**
     * 新增出差申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEvection:add')")
    @Log(title = "出差申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增出差申请接口")
    public AjaxResult add(@RequestBody OaEvection oaEvection)
    {
        int result = oaEvectionService.insertOaEvection(oaEvection);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改出差申请
     */
   /* @PreAuthorize("@ss.hasPermi('admin:oaEvection:edit')")
    @Log(title = "出差申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改出差申请接口")
    public AjaxResult edit(@RequestBody OaEvection oaEvection)
    {
        int result = oaEvectionService.updateOaEvection(oaEvection);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除出差申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEvection:remove')")
    @Log(title = "出差申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除出差申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaEvectionService.deleteOaEvectionByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEvection:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaEvectionService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 查询出差申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEvection:list')")
    @GetMapping("/listOaEvectionTrip")
    @ApiOperation(value = "出差申请报表接口")
    public TableDataInfo listOaEvectionTrip(OaEvectionTrip oaEvectionTrip)
    {
        //租户ID
        oaEvectionTrip.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaEvectionTrip> list = oaEvectionService.selectOaEvectionTrip(oaEvectionTrip);
        return getDataTable(list);
    }
}
