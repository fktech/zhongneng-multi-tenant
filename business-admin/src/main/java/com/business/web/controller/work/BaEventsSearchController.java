package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaEventsSearch;
import com.business.system.service.IBaEventsSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 联动事件Controller
 *
 * @author single
 * @date 2023-07-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/eventsSearch")
@Api(tags ="联动事件接口")
public class BaEventsSearchController extends BaseController
{
    @Autowired
    private IBaEventsSearchService baEventsSearchService;

    /**
     * 查询联动事件列表
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询联动事件列表接口")
    public TableDataInfo list(BaEventsSearch baEventsSearch)
    {
        startPage();
        List<BaEventsSearch> list = baEventsSearchService.selectBaEventsSearchList(baEventsSearch);
        return getDataTable(list);
    }

    /**
     * 导出联动事件列表
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:export')")
    @Log(title = "联动事件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出联动事件列表接口")
    public void export(HttpServletResponse response, BaEventsSearch baEventsSearch) throws IOException
    {
        List<BaEventsSearch> list = baEventsSearchService.selectBaEventsSearchList(baEventsSearch);
        ExcelUtil<BaEventsSearch> util = new ExcelUtil<BaEventsSearch>(BaEventsSearch.class);
        util.exportExcel(response, list, "eventsSearch");
    }

    /**
     * 获取联动事件详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取联动事件详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baEventsSearchService.selectBaEventsSearchById(id));
    }

    /**
     * 新增联动事件
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:add')")
    @Log(title = "联动事件", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增联动事件接口")
    public AjaxResult add(@RequestBody BaEventsSearch baEventsSearch)
    {
        return toAjax(baEventsSearchService.insertBaEventsSearch(baEventsSearch));
    }

    /**
     * 修改联动事件
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:edit')")
    @Log(title = "联动事件", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改联动事件接口")
    public AjaxResult edit(@RequestBody BaEventsSearch baEventsSearch)
    {
        return toAjax(baEventsSearchService.updateBaEventsSearch(baEventsSearch));
    }

    /**
     * 删除联动事件
     */
    @PreAuthorize("@ss.hasPermi('admin:eventsSearch:remove')")
    @Log(title = "联动事件", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除联动事件接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baEventsSearchService.deleteBaEventsSearchByIds(ids));
    }
}
