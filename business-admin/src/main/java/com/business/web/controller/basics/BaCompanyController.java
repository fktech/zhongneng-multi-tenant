package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaCompany;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.BaCompanyInstanceRelatedEditDTO;
import com.business.system.service.IBaCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 公司Controller
 *
 * @author ljb
 * @date 2022-11-30
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/company")
@Api(tags ="公司接口")
public class BaCompanyController extends BaseController
{
    @Autowired
    private IBaCompanyService baCompanyService;

    /**
     * 查询公司列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:company:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询公司列表接口")
    public TableDataInfo list(BaCompany baCompany)
    {
        //租户ID
        baCompany.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCompany> list = baCompanyService.selectBaCompanyList(baCompany);
        return getDataTable(list);
    }

    /**
     * 导出公司列表
     */
    @PreAuthorize("@ss.hasPermi('admin:company:export')")
    @Log(title = "公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出公司列表接口")
    public void export(HttpServletResponse response, BaCompany baCompany) throws IOException
    {
        //租户ID
        baCompany.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> list = baCompanyService.selectBaCompanyList(baCompany);
        ExcelUtil<BaCompany> util = new ExcelUtil<BaCompany>(BaCompany.class);
        util.exportExcel(response, list, "company");
    }

    /**
     * 获取公司详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:company:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取公司详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCompanyService.selectBaCompanyById(id));
    }

    /**
     * 新增公司
     */
    @PreAuthorize("@ss.hasPermi('admin:company:add')")
    @Log(title = "公司", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增公司接口")
    public AjaxResult add(@RequestBody BaCompany baCompany)
    {
        int result = baCompanyService.insertBaCompany(baCompany);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改公司
     */
    @PreAuthorize("@ss.hasPermi('admin:company:edit')")
    @Log(title = "公司", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改公司接口")
    public AjaxResult edit(@RequestBody BaCompany baCompany)
    {
        int result = baCompanyService.updateBaCompany(baCompany);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改账户信息
     */
    @Log(title = "公司", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation(value = "修改账户信息")
    public AjaxResult update(@RequestBody BaCompany baCompany){

        return toAjax(baCompanyService.updateCompany(baCompany));
    }

    /**
     * 删除公司
     */
    @PreAuthorize("@ss.hasPermi('admin:company:remove')")
    @Log(title = "公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除公司接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCompanyService.deleteBaCompanyByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:company:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baCompanyService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody BaCompanyInstanceRelatedEditDTO baCompanyInstanceRelatedEditDTO) {
        int result = baCompanyService.updateProcessBusinessData(baCompanyInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 公司下拉
     */
    @GetMapping("/select")
    @ApiOperation(value = "公司下拉")
    public TableDataInfo select(BaCompany baCompany)
    {
        List<BaCompany> list = baCompanyService.companyList(baCompany);

        return getDataTable(list);
    }

    /**
     * 客商统计
     * @return
     */
    @GetMapping("/counts")
    @ApiOperation(value = "客商统计")
    public UR counts(){
        UR ur = baCompanyService.counts();
        return ur;
    }

}
