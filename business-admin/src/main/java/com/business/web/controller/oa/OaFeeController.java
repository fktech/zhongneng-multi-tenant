package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaFee;
import com.business.system.service.IOaFeeService;
/**
 * 费用申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaFee")
@Api(tags ="费用申请接口")
public class OaFeeController extends BaseController
{
    @Autowired
    private IOaFeeService oaFeeService;

    /**
     * 查询费用申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaFee:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询费用申请列表接口")
    public TableDataInfo list(OaFee oaFee)
    {
        //租户ID
        oaFee.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaFee> list = oaFeeService.selectOaFeeList(oaFee);
        return getDataTable(list);
    }

    /**
     * 导出费用申请列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaFee:export')")
    @Log(title = "费用申请", businessType = BusinessType.EXPORT)*/
    @PostMapping("/export")
    @ApiOperation(value = "导出费用申请列表接口")
    public void export(HttpServletResponse response, OaFee oaFee) throws IOException
    {
        //租户ID
        oaFee.setTenantId(SecurityUtils.getCurrComId());
        List<OaFee> list = oaFeeService.selectOaFeeList(oaFee);
        ExcelUtil<OaFee> util = new ExcelUtil<OaFee>(OaFee.class);
        util.exportExcel(response, list, "oaFee");
    }

    /**
     * 获取费用申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaFee:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取费用申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaFeeService.selectOaFeeById(id));
    }

    /**
     * 新增费用申请
     */
   /* @PreAuthorize("@ss.hasPermi('admin:oaFee:add')")
    @Log(title = "费用申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增费用申请接口")
    public AjaxResult add(@RequestBody OaFee oaFee)
    {
        int result = oaFeeService.insertOaFee(oaFee);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改费用申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaFee:edit')")
    @Log(title = "费用申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改费用申请接口")
    public AjaxResult edit(@RequestBody OaFee oaFee)
    {
        int result = oaFeeService.updateOaFee(oaFee);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除费用申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaFee:remove')")
    @Log(title = "费用申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除费用申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaFeeService.deleteOaFeeByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaFee:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaFeeService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
