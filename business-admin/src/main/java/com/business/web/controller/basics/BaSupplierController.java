package com.business.web.controller.basics;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.BaSupplierInstanceRelatedEditDTO;
import com.business.system.domain.dto.DropDownBox;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 供应商Controller
 *
 * @author ljb
 * @date 2022-11-28
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/supplier")
@Api(tags ="供应商接口")
public class BaSupplierController extends BaseController
{
    @Autowired
    private IBaSupplierService baSupplierService;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    /**
     * 查询供应商列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:supplier:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询供应商列表接口")
    public TableDataInfo list(BaSupplier baSupplier)
    {
        //租户ID
        baSupplier.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSupplier> list = baSupplierService.selectBaSupplierList(baSupplier);
        return getDataTable(list);
    }

    /**
     * 导出供应商列表
     */
    @PreAuthorize("@ss.hasPermi('admin:supplier:export')")
    @Log(title = "供应商", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出供应商列表接口")
    public void export(HttpServletResponse response, BaSupplier baSupplier) throws IOException
    {
        //租户ID
        baSupplier.setTenantId(SecurityUtils.getCurrComId());
        List<BaSupplier> list = baSupplierService.selectBaSupplierList(baSupplier);
        ExcelUtil<BaSupplier> util = new ExcelUtil<BaSupplier>(BaSupplier.class);
        util.exportExcel(response, list, "supplier");
    }

    /**
     * 获取供应商详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:supplier:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取供应商详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baSupplierService.selectBaSupplierById(id));
    }

    /**
     * 新增供应商
     */
    @PreAuthorize("@ss.hasPermi('admin:supplier:add')")
    @Log(title = "供应商", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增供应商接口")
    public AjaxResult add(@RequestBody BaSupplier baSupplier)
    {
        int result = baSupplierService.insertBaSupplier(baSupplier);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改供应商
     */
    @PreAuthorize("@ss.hasPermi('admin:supplier:edit')")
    @Log(title = "供应商", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改供应商接口")
    public AjaxResult edit(@RequestBody BaSupplier baSupplier)
    {
        int result = baSupplierService.updateBaSupplier(baSupplier);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改账户信息
     */
    @Log(title = "供应商", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation(value = "修改账户信息")
    public AjaxResult update(@RequestBody BaSupplier baSupplier){

        return toAjax(baSupplierService.updateSupplier(baSupplier));
    }


    /**
     * 删除供应商
     */
    @PreAuthorize("@ss.hasPermi('admin:supplier:remove')")
    @Log(title = "供应商", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除供应商接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baSupplierService.deleteBaSupplierById(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:supplier:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baSupplierService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody BaSupplierInstanceRelatedEditDTO baSupplierInstanceRelatedEditDTO) {
        int result = baSupplierService.updateProcessBusinessData(baSupplierInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 供应商下拉
     */
    @GetMapping("/select")
    @ApiOperation(value = "供应商下拉")
    public TableDataInfo select()
    {
//        startPage();
        List<BaSupplier> list = baSupplierService.baSupplierList();
        return getDataTable(list);
    }

    /**
     * 供应商下拉组合（供应商、装卸服务公司）
     */
    @GetMapping("/dropDownBox")
    @ApiOperation(value = "供应商下拉")
    public TableDataInfo dropDownBox(String name){
        List<DropDownBox> list = baSupplierService.dropDownBox(name);
        return getDataTable(list);
    }
    /**
     * 分类统计客商管理信息
     */
    @GetMapping(value = "/statTraderInfo")
    @ApiOperation(value = "分类统计客商管理信息")
    @Anonymous
    public UR statTraderInfo(){
        UR ur = baSupplierService.statTraderInfo();
        return ur;
    }
}
