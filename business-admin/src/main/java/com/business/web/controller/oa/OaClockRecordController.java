package com.business.web.controller.oa;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaClockRecord;
import com.business.system.service.IOaClockRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 打卡记录Controller
 *
 * @author single
 * @date 2023-11-27
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/record")
@Api(tags ="打卡记录接口")
public class OaClockRecordController extends BaseController
{
    @Autowired
    private IOaClockRecordService oaClockRecordService;

    /**
     * 查询打卡记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:record:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询打卡记录列表接口")
    public TableDataInfo list(OaClockRecord oaClockRecord)
    {
        //租户
        oaClockRecord.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaClockRecord> list = oaClockRecordService.selectOaClockRecordList(oaClockRecord);
        return getDataTable(list);
    }

    /**
     * 导出打卡记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:record:export')")
    @Log(title = "打卡记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出打卡记录列表接口")
    public void export(HttpServletResponse response, OaClockRecord oaClockRecord) throws IOException
    {
        //租户
        oaClockRecord.setTenantId(SecurityUtils.getCurrComId());
        List<OaClockRecord> list = oaClockRecordService.selectOaClockRecordList(oaClockRecord);
        ExcelUtil<OaClockRecord> util = new ExcelUtil<OaClockRecord>(OaClockRecord.class);
        util.exportExcel(response, list, "record");
    }

    /**
     * 获取打卡记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:record:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取打卡记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClockRecordService.selectOaClockRecordById(id));
    }

    /**
     * 新增打卡记录
     */
    @PreAuthorize("@ss.hasPermi('admin:record:add')")
    @Log(title = "打卡记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增打卡记录接口")
    public AjaxResult add(@RequestBody OaClockRecord oaClockRecord)
    {
        return toAjax(oaClockRecordService.insertOaClockRecord(oaClockRecord));
    }

    /**
     * 修改打卡记录
     */
    @PreAuthorize("@ss.hasPermi('admin:record:edit')")
    @Log(title = "打卡记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改打卡记录接口")
    public AjaxResult edit(@RequestBody OaClockRecord oaClockRecord)
    {
        return toAjax(oaClockRecordService.updateOaClockRecord(oaClockRecord));
    }

    /**
     * 删除打卡记录
     */
    @PreAuthorize("@ss.hasPermi('admin:record:remove')")
    @Log(title = "打卡记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除打卡记录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaClockRecordService.deleteOaClockRecordByIds(ids));
    }
}
