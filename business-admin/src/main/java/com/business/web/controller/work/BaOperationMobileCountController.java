package com.business.web.controller.work;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import com.business.system.domain.BaSeal;

/**
 * 移动端业务信息统计接口Controller
 *
 * @author songkai
 * @date 2023-4-25
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/OperationMobileCount")
@Api(tags = "移动端业务信息统计接口")
public class BaOperationMobileCountController extends BaseController {
    @Autowired
    private  IBaProjectService iBaProjectService;
    @Autowired
    private  IBaDeclareService iBaDeclareService;
    @Autowired
    private IBaSettlementService iBaSettlementService;
    @Autowired
    private IBaContractStartService iBaContractStartService;
    @Autowired
    private IBaSupplierService iBaSupplierService;
    @Autowired
    private IBaEnterpriseService iBaEnterpriseService;
    @Autowired
    private  IBaCompanyService iBaCompanyService;
    @Autowired
    private  IBaContractService iBaContractService;
    @Autowired
    private  IBaSealService iBaSealService;
    @Autowired
    private IBaMaterialStockService iBaMaterialStockService;
    @Autowired
    private IHomePageService iHomePageService;
    /**
     * 移动端业务信息
     */
    @GetMapping("/SelectCounts")
    @ApiOperation(value = "移动端业务信息")
    public AjaxResult SelectSalesVolume() {
       /* Map map=new HashMap();
        BaProject baProject=new BaProject();
        baProject.setState(AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
        baProject.setFlag(0);
        baProject.setPepsi("type");
        //查询审核通过的立项数目
        List<BaProject> list = iBaProjectService.selectBaProjectList(baProject);
        map.put("projrctNums",list.size());
        //进行中项目
        baProject.setPepsi("123");
        baProject.setProjectType("4");
        List<BaProject> list1 = iBaProjectService.selectBaProjectList(baProject);
        map.put("ongoingNums",list.size());
        BaContractStart baContractStart=new BaContractStart();
        baContractStart.setFlag(0L);
        //查询业务类型时标准业务
        //baContractStart.setType("5");
        baContractStart.setCompletion(0L);
        baContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
        //查询审核通过的合同数目
        List<BaContractStart> baContractStartList = iBaContractStartService.selectBaContractStartList(baContractStart);
        //审批通过的订单数
        baContractStart.setState(AdminCodeEnum.ORDER_STATUS_PASS.getCode());
        //baContractStart.setContractStatus("5");
        List<BaContractStart> baContractStartList1 = iBaContractStartService.selectBaContractStartList(baContractStart);
        map.put("contractStartNUms",baContractStartList.size());
        //进行中的订单数
        map.put("orderNUms",baContractStartList1.size());
        //查询本月计划申报
        BaDeclare baDeclare=new BaDeclare();
        baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode());
        UR ur = iBaDeclareService.thisMonth1(baDeclare);
        map.put("declareNums",ur.getData().get("本月数据"));
        //查询客商的统计数量
           //查询供应商
        List<BaSupplier> baSuppliers = iBaSupplierService.baSupplierList();
           //查询终端企业
        List<BaEnterprise> baEnterprises = iBaEnterpriseService.enterpriseList();
        //查询公路运输类有限公司,铁路运输,下游贸易商,采购公司,托盘公司
        BaCompany baCompany=new BaCompany();
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany.setFlag(0);
        List<BaCompany> baCompanies = iBaCompanyService.selectBaCompanyList(baCompany);
        map.put("investorNums",baSuppliers.size()+baEnterprises.size()+baCompanies.size());
        //查询结算
        BaSettlement baSettlement=new BaSettlement();
        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
        List<BaSettlement> baSettlements = iBaSettlementService.selectBaSettlementList(baSettlement);
        map.put("baSettlements",baSettlements.size());
        //合同数量
        BaContract baContract =new BaContract();
        baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        baContract.setFlag(0);
        baContract.setSigningStatus("已签");
        List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
        map.put("baContracts",baContracts.size());
        //用印数
        BaSeal baSeal=new BaSeal();
        baSeal.setState(AdminCodeEnum.SEAL_STATUS_PASS.getCode());
        List<BaSeal> baSeals = iBaSealService.selectBaSealList(baSeal);
        map.put("baSeals",baSeals.size());
        baContractStartList1.addAll(baContractStartList);

      *//*  //金额查询
        Map map1 = iBaProjectService.selectNumsAndName(baContractStartList1);
        map.putAll(map1);
        BigDecimal totals = iBaMaterialStockService.totals();
        map.put("stock",totals);*//*
        List<BaContractStart> contractStarts = iBaContractStartService.baContractStartList(baContractStart);
        Map map1 = iBaProjectService.selectNumsAndName(contractStarts);
        BigDecimal bigDecimal=BigDecimal.valueOf(1000);
        Iterator<Map.Entry<String,BigDecimal >> it = map1.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, BigDecimal> entry = it.next();
            map1.put(entry.getKey(), entry.getValue().divide(bigDecimal).setScale(2, RoundingMode.DOWN));
        }
         map.putAll(map1);
        //仓储总量
        BigDecimal totals = iBaMaterialStockService.totals();
        BigDecimal totals1 =new BigDecimal(0);
        if(StringUtils.isNotNull(totals)){
            totals1 = totals.divide(bigDecimal).setScale(2, RoundingMode.DOWN);
        }
        map.put("stock",totals1);*/
        Map map = iHomePageService.selectSalesVolume();

        return success(map);

    }
}
