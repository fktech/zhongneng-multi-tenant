package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaUnit;
import com.business.system.service.IBaUnitService;

/**
 * 单位Controller
 *
 * @author ljb
 * @date 2022-11-30
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/unit")
@Api(tags ="单位接口")
public class BaUnitController extends BaseController
{
    @Autowired
    private IBaUnitService baUnitService;

    /**
     * 查询单位列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:unit:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询单位列表接口")
    public TableDataInfo list(BaUnit baUnit)
    {
        //租户ID
        baUnit.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaUnit> list = baUnitService.selectBaUnitList(baUnit);
        return getDataTable(list);
    }

    /**
     * 导出单位列表
     */
    @PreAuthorize("@ss.hasPermi('admin:unit:export')")
    @Log(title = "单位", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出单位列表接口")
    public void export(HttpServletResponse response, BaUnit baUnit) throws IOException
    {
        List<BaUnit> list = baUnitService.selectBaUnitList(baUnit);
        ExcelUtil<BaUnit> util = new ExcelUtil<BaUnit>(BaUnit.class);
        util.exportExcel(response, list, "unit");
    }

    /**
     * 获取单位详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:unit:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取单位详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baUnitService.selectBaUnitById(id));
    }

    /**
     * 新增单位
     */
    @PreAuthorize("@ss.hasPermi('admin:unit:add')")
    @Log(title = "单位", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增单位接口")
    public AjaxResult add(@RequestBody BaUnit baUnit)
    {
        Integer result = baUnitService.insertBaUnit(baUnit);
        if (result == -1){
            return AjaxResult.error("单位名称已存在！");
        }
        return toAjax(result);
    }

    /**
     * 修改单位
     */
    @PreAuthorize("@ss.hasPermi('admin:unit:edit')")
    @Log(title = "单位", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改单位接口")
    public AjaxResult edit(@RequestBody BaUnit baUnit)
    {
        return toAjax(baUnitService.updateBaUnit(baUnit));
    }

    /**
     * 删除单位
     */
    @PreAuthorize("@ss.hasPermi('admin:unit:remove')")
    @Log(title = "单位", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除单位接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baUnitService.deleteBaUnitByIds(ids));
    }

}
