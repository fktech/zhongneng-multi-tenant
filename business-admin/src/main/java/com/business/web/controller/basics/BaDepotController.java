package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.DepotDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaDepot;
import com.business.system.service.IBaDepotService;

/**
 * 仓库信息Controller
 *
 * @author ljb
 * @date 2022-11-30
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/depot")
@Api(tags ="仓库信息接口")
public class BaDepotController extends BaseController
{
    @Autowired
    private IBaDepotService baDepotService;

    /**
     * 查询仓库信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:depot:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询仓库信息列表接口")
    public TableDataInfo list(BaDepot baDepot)
    {
        //租户ID
        baDepot.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDepot> list = baDepotService.selectBaDepotList(baDepot);
        return getDataTable(list);
    }

    /**
     * 导出仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:export')")
    @Log(title = "仓库信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出仓库信息列表接口")
    public void export(HttpServletResponse response, BaDepot baDepot) throws IOException
    {
        //租户ID
        baDepot.setTenantId(SecurityUtils.getCurrComId());
        List<BaDepot> list = baDepotService.selectBaDepotList(baDepot);
        ExcelUtil<BaDepot> util = new ExcelUtil<BaDepot>(BaDepot.class);
        util.exportExcel(response, list, "depot");
    }

    /**
     * 获取仓库信息详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:depot:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取仓库信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDepotService.selectBaDepotById(id));
    }

    /**
     * 新增仓库信息
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:add')")
    @Log(title = "仓库信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增仓库信息接口")
    public AjaxResult add(@RequestBody BaDepot baDepot)
    {
        int result = baDepotService.insertBaDepot(baDepot);
        if(result == -1){
            return AjaxResult.error(-1,"仓库名称已存在");
        }else if(result == -2){
            return AjaxResult.error(-2, "库位编号已存在");
        }else if(result == -3){
            return AjaxResult.error(-3, "库位名称已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改仓库信息
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:edit')")
    @Log(title = "仓库信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改仓库信息接口")
    public AjaxResult edit(@RequestBody BaDepot baDepot)
    {
        return toAjax(baDepotService.updateBaDepot(baDepot));
    }

    /**
     * 修改仓库信息不启动流程
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:edit')")
    @Log(title = "修改仓库信息不启动流程", businessType = BusinessType.UPDATE)
    @PutMapping("/editNoProcess")
    @ApiOperation(value = "修改仓库信息不启动流程")
    public AjaxResult editNoProcess(@RequestBody BaDepot baDepot)
    {
        return toAjax(baDepotService.editNoProcess(baDepot));
    }

    /**
     * 删除仓库信息
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:remove')")
    @Log(title = "仓库信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除仓库信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDepotService.deleteBaDepotByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:depot:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baDepotService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }
    /**
     * 查询仓库信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:depot:list')")
    @GetMapping("/appList")
    @ApiOperation(value = "app查询仓库统计接口")
    public AjaxResult appList(BaDepot baDepot)
    {
        DepotDTO depotDTO = baDepotService.selectBaDepotAppList(baDepot);
        return AjaxResult.success(depotDTO);
    }

    /**
     * 判断仓位是否可删除
     */
    @GetMapping("/deleteJudge")
    @ApiOperation(value = "修改仓库信息不启动流程")
    public AjaxResult deleteJudge(String locationId)
    {
        int result = baDepotService.deleteJudge(locationId);
        if(result == 0){
            return AjaxResult.error(0,"已存在出入库信息");
        }
        return toAjax(result);
    }
}
