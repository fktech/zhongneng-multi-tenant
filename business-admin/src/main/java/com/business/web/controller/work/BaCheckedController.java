package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaChecked;
import com.business.system.service.IBaCheckedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 盘点Controller
 *
 * @author single
 * @date 2023-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/checked")
@Api(tags ="盘点接口")
public class BaCheckedController extends BaseController
{
    @Autowired
    private IBaCheckedService baCheckedService;

    /**
     * 查询盘点列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:checked:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询盘点列表接口")
    public TableDataInfo list(BaChecked baChecked)
    {
        //租户ID
        baChecked.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaChecked> list = baCheckedService.selectBaCheckedList(baChecked);
        return getDataTable(list);
    }

    /**
     * 导出盘点列表
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:export')")
    @Log(title = "盘点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出盘点列表接口")
    public void export(HttpServletResponse response, BaChecked baChecked) throws IOException
    {
        //租户ID
        baChecked.setTenantId(SecurityUtils.getCurrComId());
        List<BaChecked> list = baCheckedService.selectBaCheckedList(baChecked);
        ExcelUtil<BaChecked> util = new ExcelUtil<BaChecked>(BaChecked.class);
        util.exportExcel(response, list, "checked");
    }

    /**
     * 获取盘点详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:checked:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取盘点详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCheckedService.selectBaCheckedById(id));
    }

    /**
     * 新增盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:add')")
    @Log(title = "盘点", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增盘点接口")
    public AjaxResult add(@RequestBody BaChecked baChecked)
    {
        return toAjax(baCheckedService.insertBaChecked(baChecked));
    }

    /**
     * 修改盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:edit')")
    @Log(title = "盘点", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改盘点接口")
    public AjaxResult edit(@RequestBody BaChecked baChecked)
    {
        Integer result = baCheckedService.updateBaChecked(baChecked);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:remove')")
    @Log(title = "盘点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除盘点接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCheckedService.deleteBaCheckedByIds(ids));
    }

    /**
     * 获取盘点ID
     *
     * @return 盘点
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:query')")
    @GetMapping(value = "/getBaCheckedID")
    @ApiOperation(value = "获取盘点ID")
    public AjaxResult getBaCheckedID()
    {
        return AjaxResult.success(baCheckedService.getBaCheckedID());
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:checked:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baCheckedService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }
}
