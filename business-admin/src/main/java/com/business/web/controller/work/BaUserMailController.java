package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaUserMail;
import com.business.system.service.IBaUserMailService;

/**
 * 通讯录Controller
 *
 * @author ljb
 * @date 2023-06-02
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/userMail")
@Api(tags ="通讯录接口")
public class BaUserMailController extends BaseController
{
    @Autowired
    private IBaUserMailService baUserMailService;

    /**
     * 查询通讯录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:userMail:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询通讯录列表接口")
    public TableDataInfo list(BaUserMail baUserMail)
    {
        //租户ID
        baUserMail.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaUserMail> list = baUserMailService.selectBaUserMailList(baUserMail);
        return getDataTable(list);
    }

    /**
     * 下拉接口
     */
    @GetMapping("/userMailList")
    @ApiOperation(value = "查询通讯录下拉接口")
    public TableDataInfo userMailList(BaUserMail baUserMail)
    {
        List<BaUserMail> list = baUserMailService.userMailList(baUserMail);

        return getDataTable(list);
    }

    /**
     * 导出通讯录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:userMail:export')")
    @Log(title = "通讯录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出通讯录列表接口")
    public void export(HttpServletResponse response, BaUserMail baUserMail) throws IOException
    {
        List<BaUserMail> list = baUserMailService.selectBaUserMailList(baUserMail);
        ExcelUtil<BaUserMail> util = new ExcelUtil<BaUserMail>(BaUserMail.class);
        util.exportExcel(response, list, "userMail");
    }

    /**
     * 获取通讯录详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:userMail:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取通讯录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baUserMailService.selectBaUserMailById(id));
    }

    /**
     * 新增通讯录
     */
    @PreAuthorize("@ss.hasPermi('admin:userMail:add')")
    @Log(title = "通讯录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增通讯录接口")
    public AjaxResult add(@RequestBody BaUserMail baUserMail)
    {
        return toAjax(baUserMailService.insertBaUserMail(baUserMail));
    }

    /**
     * 修改通讯录
     */
    @PreAuthorize("@ss.hasPermi('admin:userMail:edit')")
    @Log(title = "通讯录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改通讯录接口")
    public AjaxResult edit(@RequestBody BaUserMail baUserMail)
    {
        return toAjax(baUserMailService.updateBaUserMail(baUserMail));
    }

    /**
     * 删除通讯录
     */
    @PreAuthorize("@ss.hasPermi('admin:userMail:remove')")
    @Log(title = "通讯录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除通讯录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baUserMailService.deleteBaUserMailByIds(ids));
    }

    /**
     * 通讯录下拉框
     */
    @GetMapping("/userMail")
    @ApiOperation(value = "通讯录下拉框")
    public TableDataInfo userMail()
    {
        List<BaUserMail> list = baUserMailService.userMail();
        return getDataTable(list);
    }
}
