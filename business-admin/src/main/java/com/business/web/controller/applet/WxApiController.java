package com.business.web.controller.applet;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.constant.Constants;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginBody;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.framework.web.service.SysLoginService;
import com.business.system.service.ISysUserService;
import com.business.system.util.AESUtil;
import com.business.system.util.HttpClientUtils;
import com.business.web.entity.WxInfo;
import com.google.gson.Gson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin //跨域
@RestController
@RequestMapping("/wxService")
@Api(tags ="微信接口")
public class WxApiController {

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysLoginService loginService;


    //小程序微信登录
    //获取sessionKey和openID
    @PostMapping("/WXLoginGetOpenId")
    @ApiOperation("微信小程序获取sessionKey和openID接口")
    @Anonymous
    public UR pcWXLoginGetOpenId(String code, @RequestBody WxInfo wxInfo) throws Exception {

        //中能主题
        String appid = "wx7ae5321779bfa028";
        String secret = "615b81678d714ffd0120588b1417975f";

        String accessTokenUrl ="https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+secret+"&js_code="+code+"&grant_type=authorization_code";
        String accessTokenInfo = HttpClientUtils.get(accessTokenUrl);

        Gson gson = new Gson();
        HashMap mapAccessToken = gson.fromJson(accessTokenInfo, HashMap.class);
        String openid = (String)mapAccessToken.get("openid");
        //查看是否存在此openId用户
        if(StringUtils.isNotEmpty(openid)){
            SysUser sysUser = userService.selectOpenIdByUser(openid);
            if(StringUtils.isNotNull(sysUser)){
                //openId存在自动登录
                return UR.ok().data("user",sysUser).mid(sysUser.getUserId().toString());
            }else {
                if(StringUtils.isNotEmpty(wxInfo.getUserName())){
                    //校验用户是否正常
                    loginService.noCodeLogin(wxInfo.getUserName(), wxInfo.getPassword());

                    SysUser sysUser1 = userService.selectUserByUserName(wxInfo.getUserName());
                    if(StringUtils.isNotEmpty(sysUser1.getOpenId()) && sysUser1.getOpenId().equals("退出") == false){
                        return UR.ok().data("user",1).message("该账号已绑定微信");
                    }
                    //绑定微信openId
                    sysUser1.setOpenId(openid);
                    String encrypt = AESUtil.encrypt(wxInfo.getPassword(), "key_value_length");
                    sysUser1.setCypher(encrypt);
                    userService.updateUserProfile(sysUser1);
                    return UR.ok().data("user",2);
                }
                return UR.ok().data("user",null);
            }
        }
        return UR.ok();
    }

    //账号密码登录登录
    @GetMapping("/getToken")
    @ApiOperation("登录获取token")
    @Anonymous
    public AjaxResult getToken (String mid) throws Exception
    {
        SysUser user = userService.selectUserById(Long.parseLong(mid));
        LoginBody loginBody = new LoginBody();
        //临时密码解密
        String decrypt = AESUtil.decrypt(user.getCypher(), "key_value_length");
        loginBody.setUsername(user.getUserName());
        loginBody.setPassword(decrypt);
        //System.out.println("*********** name is :" + name);
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.noCodeLogin(loginBody.getUsername(), loginBody.getPassword());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    //退出登录
    @GetMapping("/getOut")
    @ApiOperation("退出登录")
    @Anonymous
    public UR getOut (String mid)
    {
        SysUser user = userService.selectUserById(Long.parseLong(mid));
        user.setCypher("退出");
        user.setOpenId("退出");
        int result = userService.updateUserStatus(user);
        if(result > 0){
            return UR.ok().code(200);
        }
        return UR.error().code(500);
    }


    //消息发送
    @GetMapping("/message")
    @ApiOperation("服务消息推送")
    @Anonymous
    public String message() throws Exception{
        //拿到access_token
        String appid = "wx7ae5321779bfa028";
        String secret = "615b81678d714ffd0120588b1417975f";

        String accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?appid="+appid+"&secret="+secret+"&grant_type=client_credential";
        String accessTokenInfo = null;
        accessTokenInfo = HttpClientUtils.get(accessTokenUrl);
        Gson gson = new Gson();
        HashMap mapAccessToken = gson.fromJson(accessTokenInfo, HashMap.class);
        String access_token = (String)mapAccessToken.get("access_token");

        String path = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + access_token;

        Map param = new HashMap();
        param.put("touser","ogx3V5WikNxxbuloiH00qMzdscd0");
        param.put("template_id","n2zB2Qt6hrlHMLBKFPDjzCbCLUbqJSQoIKEURisNQjY");
        Map dataMap = new HashMap();
        dataMap.put("name3",new HashMap<String,String>(){{put("value","测试");}});
        dataMap.put("name4",new HashMap<String,String>(){{put("value","测试");}});
        dataMap.put("time19",new HashMap<String,String>(){{put("value","15:01");}});
        dataMap.put("thing5",new HashMap<String,String>(){{put("value","无");}});
        param.put("data", dataMap);

        System.out.println("++++++++++++++"+param);

        JSONObject posts = HttpClientUtils.Posts(path, new JSONObject(param));

        return posts.toJSONString();
    }
}
