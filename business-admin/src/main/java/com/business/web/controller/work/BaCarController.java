package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaCar;
import com.business.system.service.IBaCarService;

/**
 * 车辆信息Controller
 *
 * @author single
 * @date 2023-12-01
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baCar")
@Api(tags ="车辆信息接口")
public class BaCarController extends BaseController
{
    @Autowired
    private IBaCarService baCarService;

    /**
     * 查询车辆信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:baCar:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询车辆信息列表接口")
    public TableDataInfo list(BaCar baCar)
    {
        startPage();
        List<BaCar> list = baCarService.selectBaCarList(baCar);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baCar:export')")
    @Log(title = "车辆信息", businessType = BusinessType.EXPORT)*/
    @PostMapping("/export")
    @ApiOperation(value = "导出车辆信息列表接口")
    public void export(HttpServletResponse response, BaCar baCar) throws IOException
    {
        List<BaCar> list = baCarService.selectBaCarList(baCar);
        ExcelUtil<BaCar> util = new ExcelUtil<BaCar>(BaCar.class);
        util.exportExcel(response, list, "baCar");
    }

    /**
     * 获取车辆信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:baCar:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取车辆信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCarService.selectBaCarById(id));
    }

    /**
     * 新增车辆信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baCar:add')")
    @Log(title = "车辆信息", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增车辆信息接口")
    public AjaxResult add(@RequestBody BaCar baCar)
    {
        return toAjax(baCarService.insertBaCar(baCar));
    }

    /**
     * 修改车辆信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baCar:edit')")
    @Log(title = "车辆信息", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改车辆信息接口")
    public AjaxResult edit(@RequestBody BaCar baCar)
    {
        return toAjax(baCarService.updateBaCar(baCar));
    }

    /**
     * 删除车辆信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:baCar:remove')")
    @Log(title = "车辆信息", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除车辆信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCarService.deleteBaCarByIds(ids));
    }
}
