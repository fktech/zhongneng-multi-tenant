package com.business.web.controller.applet;

import com.business.common.annotation.Anonymous;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaProject;
import com.business.system.domain.SysNotice;
import com.business.system.domain.dto.StatisticsDTO;
import com.business.system.service.IBaContractStartService;
import com.business.system.service.IBaProjectService;
import com.business.system.service.IHomePageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin //跨域
@RestController
@RequestMapping("/homePage")
@Api(tags ="首页数据统计")
public class HomePageController {
    @Autowired
    private IHomePageService homePageService;
    @Autowired
    private IBaProjectService baProjectService;
    @Autowired
    private IBaContractStartService baContractStartService;

    @GetMapping("/screen")
    @ApiOperation("查询业务经理首页去申报与立项草稿显示")
    @Anonymous
    public UR screen(String id){
        Long userId = Long.parseLong(id);
        UR screen = homePageService.screen(userId);
        return screen;
    }

    @GetMapping("/lineChart")
    @ApiOperation("查询业务经理首页折线数据")
    @Anonymous
    public UR lineChart(String type){
        UR screen = homePageService.lineChart(type);
        return screen;
    }

    /**
     * 获取通知公告列表
     */
    @GetMapping("/noticeList")
    @ApiOperation("首页通知")
    @Anonymous
    public List<SysNotice> list(SysNotice notice)
    {
        List<SysNotice> list = homePageService.selectNoticeList(notice);
        return list;
    }

    /**
     * 查询立项管理列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/projectList")
    @ApiOperation(value = "查询立项管理列表接口")
    public List<BaProject> list(BaProject baProject)
    {
        baProject.setState("projectStatus:pass");
        baProject.setPepsi("project");
        List<BaProject> list = baProjectService.selectBaProjectList(baProject);
        return list;
    }

    /**
     * 查询合同启动列表
     */
    @GetMapping("/contractStartList")
    @ApiOperation(value = "查询合同启动列表接口")
    public List<BaContractStart> list(BaContractStart baContractStart)
    {
        baContractStart.setState("contractStart:pass");
        List<BaContractStart> contractStarts = new ArrayList<>();
        List<BaContractStart> list = baContractStartService.selectBaContractStartList(baContractStart);
        /**
         *  if(list.size() > 1){
         *             contractStarts.add(list.get(0));
         *             contractStarts.add(list.get(1));
         *         }else {
         *             return list;
         *         }
         */
        //拿到前两条数据

        return list;
    }

    /**
     * 数量统计
     */
    @GetMapping("/dataStatistics")
    @ApiOperation("数量统计")
    @Anonymous
    public UR dataStatistics(String depId){
        Long id = null;
        if(depId != null){
            id = Long.parseLong(depId);
        }
        UR ur = homePageService.dataStatistics(id);
        return ur;
    }

    /**
     * 本周计划申报人
     */
    @GetMapping("/thisWeekDeclared")
    @ApiOperation("本周计划申报人")
    @Anonymous
    public List<BaDeclare> thisWeekDeclared(){
        List<BaDeclare> baDeclares = homePageService.thisWeekDeclared();
        return baDeclares;
    }

    /**
     * 数据呈现首页统计（累计合同额）
     */
    @GetMapping("/statContractAmount")
    @ApiOperation("数据呈现首页统计（累计合同额）")
    @Anonymous
    public UR statContractAmount(String deptId){
        UR ur = homePageService.statContractAmount(deptId);
        return ur;
    }

    /**
     * 数据呈现首页统计(累计贸易额)
     */
    @GetMapping("/statTradeAmount")
    @ApiOperation("数据呈现首页统计(累计贸易额)")
    @Anonymous
    public UR statTradeAmount(String deptId){
        UR ur = homePageService.statTradeAmount(deptId);
        return ur;
    }

    /**
     * 数据呈现首页统计(累计运输量)
     */
    @GetMapping("/statCheckCoalNum")
    @ApiOperation("数据呈现首页统计(累计运输量)")
    @Anonymous
    public UR statCheckCoalNum(String deptId){
        UR ur = homePageService.statCheckCoalNum(deptId);
        return ur;
    }

    /**
     * 数据呈现首页统计(累计实现利润)
     */
    @GetMapping("/profitAccumulate")
    @ApiOperation("数据呈现首页统计(累计实现利润)")
    @Anonymous
    public UR profitAccumulate(String deptId){
        UR ur = homePageService.profitAccumulate(deptId);
        return ur;
    }

    /**
     * 数据呈现子公司首页统计
     */
    @GetMapping("/dataLayoutSubCompany")
    @ApiOperation("数据呈现子公司首页统计")
    @Anonymous
    public AjaxResult dataLayoutSubCompany(String type){
        return AjaxResult.success( homePageService.dataLayoutSubCompany(type));
    }

    /**
     * 数据呈现事业部首页统计
     */
    @GetMapping("/dataLayoutDepartment")
    @ApiOperation("数据呈现事业部首页统计")
    @Anonymous
    public AjaxResult dataLayoutDepartment(String deptId, String type){

        return AjaxResult.success(homePageService.dataLayoutDepartment(Long.valueOf(deptId), type));
    }

    /**
     * 业务部数据
     */
    @GetMapping("/statistics")
    @ApiOperation(value = "业务部数据")
    @Anonymous
    public List<StatisticsDTO> contractStartHome(){

        List<StatisticsDTO> statistics = homePageService.statistics();

        return statistics;
    }
}
