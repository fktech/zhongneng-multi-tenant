package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaServiceVoucher;
import com.business.system.service.IBaServiceVoucherService;

/**
 * 进厂确认单Controller
 *
 * @author single
 * @date 2023-09-19
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/serviceVoucher")
@Api(tags ="进厂确认单接口")
public class BaServiceVoucherController extends BaseController
{
    @Autowired
    private IBaServiceVoucherService baServiceVoucherService;

    /**
     * 查询进厂确认单列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:serviceVoucher:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询进厂确认单列表接口")
    public TableDataInfo list(BaServiceVoucher baServiceVoucher)
    {
        startPage();
        List<BaServiceVoucher> list = baServiceVoucherService.selectBaServiceVoucherList(baServiceVoucher);
        return getDataTable(list);
    }

    /**
     * 导出进厂确认单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:serviceVoucher:export')")
    @Log(title = "进厂确认单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出进厂确认单列表接口")
    public void export(HttpServletResponse response, BaServiceVoucher baServiceVoucher) throws IOException
    {
        List<BaServiceVoucher> list = baServiceVoucherService.selectBaServiceVoucherList(baServiceVoucher);
        ExcelUtil<BaServiceVoucher> util = new ExcelUtil<BaServiceVoucher>(BaServiceVoucher.class);
        util.exportExcel(response, list, "serviceVoucher");
    }

    /**
     * 获取进厂确认单详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:serviceVoucher:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取进厂确认单详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baServiceVoucherService.selectBaServiceVoucherById(id));
    }

    /**
     * 新增进厂确认单
     */
    @PreAuthorize("@ss.hasPermi('admin:serviceVoucher:add')")
    @Log(title = "进厂确认单", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增进厂确认单接口")
    public AjaxResult add(@RequestBody BaServiceVoucher baServiceVoucher)
    {
        return toAjax(baServiceVoucherService.insertBaServiceVoucher(baServiceVoucher));
    }

    /**
     * 修改进厂确认单
     */
    @PreAuthorize("@ss.hasPermi('admin:serviceVoucher:edit')")
    @Log(title = "进厂确认单", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改进厂确认单接口")
    public AjaxResult edit(@RequestBody BaServiceVoucher baServiceVoucher)
    {
        return toAjax(baServiceVoucherService.updateBaServiceVoucher(baServiceVoucher));
    }

    /**
     * 删除进厂确认单
     */
    @PreAuthorize("@ss.hasPermi('admin:serviceVoucher:remove')")
    @Log(title = "进厂确认单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除进厂确认单接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baServiceVoucherService.deleteBaServiceVoucherByIds(ids));
    }


}
