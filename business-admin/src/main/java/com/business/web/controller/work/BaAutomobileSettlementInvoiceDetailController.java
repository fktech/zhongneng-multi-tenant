package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaAutomobileSettlementInvoiceDetail;
import com.business.system.service.IBaAutomobileSettlementInvoiceDetailService;

/**
 * 无车承运开票详情Controller
 *
 * @author single
 * @date 2023-04-13
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/automobileInvoiceDetail")
@Api(tags ="无车承运开票详情接口")
public class BaAutomobileSettlementInvoiceDetailController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementInvoiceDetailService baAutomobileSettlementInvoiceDetailService;

    /**
     * 查询无车承运开票详情列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询无车承运开票详情列表接口")
    public TableDataInfo list(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        startPage();
        List<BaAutomobileSettlementInvoiceDetail> list = baAutomobileSettlementInvoiceDetailService.selectBaAutomobileSettlementInvoiceDetailList(baAutomobileSettlementInvoiceDetail);
        return getDataTable(list);
    }

    /**
     * 导出无车承运开票详情列表
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:export')")
    @Log(title = "无车承运开票详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出无车承运开票详情列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail) throws IOException
    {
        List<BaAutomobileSettlementInvoiceDetail> list = baAutomobileSettlementInvoiceDetailService.selectBaAutomobileSettlementInvoiceDetailList(baAutomobileSettlementInvoiceDetail);
        ExcelUtil<BaAutomobileSettlementInvoiceDetail> util = new ExcelUtil<BaAutomobileSettlementInvoiceDetail>(BaAutomobileSettlementInvoiceDetail.class);
        util.exportExcel(response, list, "automobileInvoiceDetail");
    }

    /**
     * 获取无车承运开票详情详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取无车承运开票详情详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementInvoiceDetailService.selectBaAutomobileSettlementInvoiceDetailById(id));
    }

    /**
     * 新增无车承运开票详情
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:add')")
    @Log(title = "无车承运开票详情", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增无车承运开票详情接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        return toAjax(baAutomobileSettlementInvoiceDetailService.insertBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail));
    }

    /**
     * 修改无车承运开票详情
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:edit')")
    @Log(title = "无车承运开票详情", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改无车承运开票详情接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        return toAjax(baAutomobileSettlementInvoiceDetailService.updateBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail));
    }

    /**
     * 删除无车承运开票详情
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoiceDetail:remove')")
    @Log(title = "无车承运开票详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除无车承运开票详情接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementInvoiceDetailService.deleteBaAutomobileSettlementInvoiceDetailByIds(ids));
    }
}
