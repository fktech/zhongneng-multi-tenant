package com.business.web.controller.work;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.*;
import com.business.system.domain.dto.CountTransportDTO;
import com.business.system.service.IBaHiTransportService;
import com.business.system.service.IBaQualityReportService;
import com.business.system.service.OssService;
import com.business.system.util.ExportMyWord;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.service.IBaTransportService;


/**
 * 运输Controller
 *
 * @author ljb
 * @date 2022-12-13
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/transport")
@Api(tags ="运输接口")
public class BaTransportController extends BaseController
{
    @Autowired
    private IBaTransportService baTransportService;

    @Autowired
    private OssService ossService;

    @Autowired
    private IBaQualityReportService qualityReportService;

    @Autowired
    private IBaHiTransportService iBaHiTransportService;

    /**
     * 查询运输列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:transport:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询运输列表接口")
    public TableDataInfo list(BaTransport baTransport)
    {
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaTransport> list = baTransportService.selectBaTransportList(baTransport);
        return getDataTable(list);
    }

    /**
     * 导出运输列表
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:export')")
    @Log(title = "运输", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出运输列表接口")
    public void export(HttpServletResponse response, BaTransport baTransport) throws IOException
    {
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        List<BaTransport> list = baTransportService.selectBaTransportList(baTransport);
        ExcelUtil<BaTransport> util = new ExcelUtil<BaTransport>(BaTransport.class);
        util.exportExcel(response, list, "transport");
    }

    /**
     * 获取运输详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取运输详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baTransportService.selectBaTransportById(id));
    }

    /**
     * 新增运输
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:add')")
    @Log(title = "运输", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增运输接口")
    public AjaxResult add(@RequestBody BaTransport baTransport)
    {
        Integer result = baTransportService.insertBaTransport(baTransport);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改运输
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:edit')")
    @Log(title = "运输", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改运输接口")
    public AjaxResult edit(@RequestBody BaTransport baTransport)
    {
        Integer result = baTransportService.updateBaTransport(baTransport);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除运输
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:remove')")
    @Log(title = "运输", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除运输接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baTransportService.deleteBaTransportByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baTransportService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 发货按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:delivery')")
    @PostMapping(value = "/delivery")
    @ApiOperation(value = "发货按钮")
    public AjaxResult delivery(@RequestBody BaTransport baTransport){

        return toAjax(baTransportService.delivery(baTransport));
    }

    /**
     * 收货按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:receipt')")
    @PostMapping(value = "/receipt")
    @ApiOperation(value = "收货按钮")
    public AjaxResult receipt(@RequestBody BaTransport baTransport){

        return toAjax(baTransportService.receipt(baTransport));
    }

    /**
     * 货转按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:transfer')")
    @PostMapping(value = "/transfer")
    @ApiOperation(value = "货转按钮")
    public AjaxResult transfer(@RequestBody BaTransport baTransport){

        return toAjax(baTransportService.transfer(baTransport));
    }

    /**
     * 运输下拉
     */
    @GetMapping("/select")
    @ApiOperation(value = "运输下拉")
    public TableDataInfo list(String startId)
    {
        List<BaTransport> list = baTransportService.select(startId);
        return getDataTable(list);
    }

    /**
     * 保存按钮
     */
    @PostMapping(value = "/saveButton")
    @ApiOperation(value = "保存按钮")
    public AjaxResult saveButton(@RequestBody BaTransport baTransport){

        return toAjax(baTransportService.saveButton(baTransport));
    }

    @GetMapping(value = "/download2zip")
    @ApiOperation(value = "文件下载")
    @Anonymous
    public void export(HttpServletRequest request, HttpServletResponse response,String ids) {
        List<String> imagePaths = new ArrayList<>();

        String[] split = ids.split(",");
        for (String id:split) {
            BaQualityReport baQualityReport = qualityReportService.selectBaQualityReportById(id);
            String[] paths = baQualityReport.getAnnex().split(",");
            for (String path:paths) {
                String substring = path.substring(path.lastIndexOf("/", path.lastIndexOf("/") - 1) + 1);
                imagePaths.add(substring);
            }
        }

        //imagePaths.add("mt/货转协议（火车）.docx");
        String zipName = "test" + ".zip";
        try {
            // 创建临时文件
            File zipFile = File.createTempFile("test", ".zip");
            // 设置响应类型，以附件形式下载文件
            response.reset();
            response.setContentType("text/plain");
            response.setContentType("application/octet-stream; charset=utf-8");
            response.setHeader("Location", zipName);
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
            // 下载文件为zip压缩包
            ossService.getZipFromOSSByPaths(zipName, zipFile, request, response, imagePaths);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改货权转移证明数据
     */
    @Log(title = "修改货权转移证明数据", businessType = BusinessType.UPDATE)
    @PostMapping(value ="/updateTansferRights")
    @ApiOperation(value = "修改货权转移证明数据")
    public AjaxResult updateTansferRights(@RequestBody BaCheck baCheck)
    {
        Integer result = baTransportService.updateBaCheckTransferRights(baCheck);
        if(result == 0){
            return AjaxResult.error("修改货权转移证明数据失败");
        }
        return toAjax(result);
    }


    /**
     * PC端首页累计发运量统计
     */
    @GetMapping("/countBidTransportList")
    @ApiOperation(value = "PC端首页累计发运量统计")
    public AjaxResult countTransportList(CountTransportDTO countTransportDTO){
        return AjaxResult.success(baTransportService.countTransportList(countTransportDTO));
    }

    /**
     * PC端首页仓储累计发运量统计
     */
    @GetMapping("/storageCountTransportList")
    @ApiOperation(value = "PC端首页仓储累计发运量统计")
    public AjaxResult storageCountTransportList(BaTransportAutomobile baTransportAutomobile){
        return AjaxResult.success(baTransportService.storageCountTransportList(baTransportAutomobile));
    }

    /**
     * 变更火车运输
     */
//    @PreAuthorize("@ss.hasPermi('admin:transport:change')")
    @Log(title = "变更火运", businessType = BusinessType.UPDATE)
    @PostMapping("/change")
    @ApiOperation(value = "变更火运接口")
    public AjaxResult change(@RequestBody BaTransport baTransport) throws Exception {
        int result = baTransportService.changeBaTransport(baTransport);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        } else if(result == -1){
            return AjaxResult.error(-1,"未进行任何变更");
        }
        return toAjax(result);
    }

    /**
     * 查询火车运输变更历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeList")
    @ApiOperation(value = "查询火车运输变更历史列表")
    public TableDataInfo changeList(BaHiTransport baHiTransport)
    {
        startPage();
        List<BaHiTransport> list = iBaHiTransportService.selectChangeDataBaHiTransportList(baHiTransport);
        return getDataTable(list);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    @GetMapping(value = "/change/{id}")
    @ApiOperation(value = "获取当前数据和上一次变更记录对比数据")
    public List<String> selectChangeDataList(@PathVariable String id)
    {
        return baTransportService.selectChangeDataList(id);
    }

    /**
     * 查询火运管理变更记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeDataList")
    @ApiOperation(value = "查询火运管理变更记录列表")
    public TableDataInfo changeDataList(BaHiTransport baHiTransport)
    {
        startPage();
        List<BaHiTransport> list = iBaHiTransportService.selectChangeDataBaHiTransportList(baHiTransport);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 获取运输历史详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:transport:query')")
    @GetMapping(value = "/hi/{id}")
    @ApiOperation(value = "获取运输历史详细信息")
    public AjaxResult getHiInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(iBaHiTransportService.selectBaHiTransportById(id));
    }

    /**
     * 查询多租户授权信息运输列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:transport:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息运输列表")
    public TableDataInfo authorizedList(BaTransport baTransport)
    {
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaTransport> list = baTransportService.selectBaTransportAuthorizedList(baTransport);
        return getDataTable(list);
    }

    /**
     * 授权运输管理
     */
    @PreAuthorize("@ss.hasPermi('admin:transport:authorized')")
    @Log(title = "运输管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权运输管理")
    public AjaxResult authorized(@RequestBody BaTransport baTransport)
    {
        return AjaxResult.success(baTransportService.authorizedBaTransport(baTransport));
    }
}
