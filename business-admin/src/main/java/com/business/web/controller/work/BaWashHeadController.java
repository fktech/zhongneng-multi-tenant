package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.BaWashInventory;
import com.business.system.service.IBaWashHeadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 洗煤出入库记录Controller
 *
 * @author js
 * @date 2024-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/wash")
@Api(tags ="洗煤配煤出入库记录接口")
public class BaWashHeadController extends BaseController
{
    @Autowired
    private IBaWashHeadService baWashHeadService;

    /**
     * 查询洗煤配煤出入库记录列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:wash:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询洗煤配煤出入库记录列表接口")
    public TableDataInfo list(BaWashHead baWashHead)
    {
        //租户ID
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaWashHead> list = baWashHeadService.selectBaWashHeadList(baWashHead);
        return getDataTable(list);
    }

    /**
     * 导出洗煤配煤出入库记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:export')")
    @Log(title = "洗煤配煤出入库记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出洗煤配煤出入库记录列表接口")
    public void export(HttpServletResponse response, BaWashHead baWashHead) throws IOException
    {
        //租户ID
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        List<BaWashHead> list = baWashHeadService.selectBaWashHeadList(baWashHead);
        ExcelUtil<BaWashHead> util = new ExcelUtil<BaWashHead>(BaWashHead.class);
        util.exportExcel(response, list, "wash");
    }

    /**
     * 获取洗煤配煤出入库记录详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:wash:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取洗煤配煤出入库记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baWashHeadService.selectBaWashHeadById(id));
    }

    /**
     * 新增洗煤入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:add')")
    @Log(title = "新增洗煤入库记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增洗煤入库记录")
    public AjaxResult add(@RequestBody BaWashHead baWashHead)
    {
        return toAjax(baWashHeadService.insertBaWashHead(baWashHead));
    }

    /**
     * 新增洗煤出库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:add')")
    @Log(title = "新增洗煤出库记录", businessType = BusinessType.INSERT)
    @PostMapping(value = "/insertWashOutputdepot")
    @ApiOperation(value = "新增洗煤出库记录")
    public AjaxResult insertWashOutputdepot(@RequestBody BaWashHead baWashHead)
    {
        return toAjax(baWashHeadService.insertWashOutputdepot(baWashHead));
    }

    /**
     * 修改洗煤入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:updateDepotHead')")
    @Log(title = "修改洗煤入库记录", businessType = BusinessType.UPDATE)
    @PostMapping("/updateBaWashHead")
    @ApiOperation(value = "修改洗煤入库记录")
    public AjaxResult updateBaWashHead(@RequestBody BaWashHead baWashHead)
    {
        Integer result = baWashHeadService.updateBaWashHead(baWashHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 修改洗煤出库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:edit')")
    @Log(title = "修改洗煤出库记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改洗煤出库记录")
    public AjaxResult updateWashOutputdepot(@RequestBody BaWashHead baWashHead)
    {
        Integer result = baWashHeadService.updateWashOutputdepot(baWashHead);
        if(result == 0){
            return AjaxResult.error("启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 删除洗煤配煤出入库记录
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:remove')")
    @Log(title = "洗煤配煤出入库记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除洗煤配煤出入库记录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baWashHeadService.deleteBaWashHeadByIds(ids));
    }

    /** 洗煤入库撤销 */
    @PreAuthorize("@ss.hasPermi('admin:wash:into')")
    @GetMapping(value = "/into/revoke/{id}")
    @ApiOperation(value = "入库撤销按钮")
    public AjaxResult intoRevoke(@PathVariable("id") String id)
    {
        Integer result = baWashHeadService.intoRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 洗煤出库撤销
     */
    @PreAuthorize("@ss.hasPermi('admin:wash:revoke')")
    @GetMapping(value = "/output/revoke/{id}")
    @ApiOperation(value = "出库撤销按钮")
    public AjaxResult outputRevoke(@PathVariable("id") String id)
    {
        Integer result = baWashHeadService.outputRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 查询库存判断是否允许审批通过
     */
//    @PreAuthorize("@ss.hasPermi('admin:wash:query')")
    @GetMapping(value = "/getMaterialStockResult/{id}")
    @ApiOperation(value = "查询库存判断是否允许审批通过")
    public AjaxResult getMaterialStockResult(@PathVariable("id") String id)
    {
        return AjaxResult.success(baWashHeadService.getMaterialStockResult(id));
    }

    /**
     * 洗配煤出入库统计
     */
    @GetMapping("/washHeadDetail")
    @ApiOperation(value = "洗配煤出入库统计")
    public TableDataInfo washHeadDetail(BaWashInventory baWashInventory)
    {
        startPage();
        List<BaWashInventory> list = baWashHeadService.washHeadDetail(baWashInventory);
        return getDataTable(list);
    }

}
