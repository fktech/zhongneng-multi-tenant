package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.BaEnterpriseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaCompanyGroup;
import com.business.system.service.IBaCompanyGroupService;

/**
 * 集团公司Controller
 *
 * @author ljb
 * @date 2023-03-01
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/companyGroup")
@Api(tags ="集团公司接口")
public class BaCompanyGroupController extends BaseController
{
    @Autowired
    private IBaCompanyGroupService baCompanyGroupService;

    /**
     * 查询集团公司列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:companyGroup:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询集团公司列表接口")
    public TableDataInfo list(BaCompanyGroup baCompanyGroup)
    {
        //租户ID
        baCompanyGroup.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCompanyGroup> list = baCompanyGroupService.selectBaCompanyGroupList(baCompanyGroup);
        return getDataTable(list);
    }

    /**
     * 导出集团公司列表
     */
    @PreAuthorize("@ss.hasPermi('admin:companyGroup:export')")
    @Log(title = "集团公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出集团公司列表接口")
    public void export(HttpServletResponse response, BaCompanyGroup baCompanyGroup) throws IOException
    {
        //租户ID
        baCompanyGroup.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompanyGroup> list = baCompanyGroupService.selectBaCompanyGroupList(baCompanyGroup);
        ExcelUtil<BaCompanyGroup> util = new ExcelUtil<BaCompanyGroup>(BaCompanyGroup.class);
        util.exportExcel(response, list, "companyGroup");
    }

    /**
     * 获取集团公司详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:companyGroup:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取集团公司详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCompanyGroupService.selectBaCompanyGroupById(id));
    }

    /**
     * 新增集团公司
     */
    @PreAuthorize("@ss.hasPermi('admin:companyGroup:add')")
    @Log(title = "集团公司", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增集团公司接口")
    public AjaxResult add(@RequestBody BaCompanyGroup baCompanyGroup)
    {
        int result = baCompanyGroupService.insertBaCompanyGroup(baCompanyGroup);
        if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改集团公司
     */
    @PreAuthorize("@ss.hasPermi('admin:companyGroup:edit')")
    @Log(title = "集团公司", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改集团公司接口")
    public AjaxResult edit(@RequestBody BaCompanyGroup baCompanyGroup)
    {
        int result = baCompanyGroupService.updateBaCompanyGroup(baCompanyGroup);
        if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 删除集团公司
     */
    @PreAuthorize("@ss.hasPermi('admin:companyGroup:remove')")
    @Log(title = "集团公司", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除集团公司接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        int result = baCompanyGroupService.deleteBaCompanyGroupByIds(ids);
        if(result == -1){
            return AjaxResult.error(-1,"有终端企业已绑定");
        }
        return toAjax(baCompanyGroupService.deleteBaCompanyGroupByIds(ids));
    }

    /**
     * 首页查询终端面板统计
     */
    @GetMapping("/homeCompanyGroupStat")
    @ApiOperation(value = "首页查询终端面板统计")
    @Anonymous
    public AjaxResult homeCompanyGroupStat(BaCompanyGroup baCompanyGroup){
        return AjaxResult.success(baCompanyGroupService.companyGroupListStat(baCompanyGroup));
    }

    /**
     * 子公司数据
     */
    @GetMapping("/subsidiaryList")
    @ApiOperation(value = "子公司数据")
    public TableDataInfo subsidiaryList(String depId)
    {
        List<BaCompanyGroup> list = baCompanyGroupService.subsidiaryList(depId);
        return getDataTable(list);
    }
}
