package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaBatch;
import com.business.system.service.IBaBatchService;

/**
 * 批次Controller
 *
 * @author single
 * @date 2023-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/batch")
@Api(tags ="批次接口")
public class BaBatchController extends BaseController
{
    @Autowired
    private IBaBatchService baBatchService;

    /**
     * 查询批次列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:batch:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询批次列表接口")
    public TableDataInfo list(BaBatch baBatch)
    {
        startPage();
        List<BaBatch> list = baBatchService.selectBaBatchList(baBatch);
        return getDataTable(list);
    }

    /**
     * 导出批次列表
     */
    @PreAuthorize("@ss.hasPermi('admin:batch:export')")
    @Log(title = "批次", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出批次列表接口")
    public void export(HttpServletResponse response, BaBatch baBatch) throws IOException
    {
        List<BaBatch> list = baBatchService.selectBaBatchList(baBatch);
        ExcelUtil<BaBatch> util = new ExcelUtil<BaBatch>(BaBatch.class);
        util.exportExcel(response, list, "batch");
    }

    /**
     * 获取批次详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:batch:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取批次详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBatchService.selectBaBatchById(id));
    }

    /**
     * 新增批次
     */
    @PreAuthorize("@ss.hasPermi('admin:batch:add')")
    @Log(title = "批次", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增批次接口")
    public AjaxResult add(@RequestBody BaBatch baBatch)
    {
        Integer result = baBatchService.insertBaBatch(baBatch);
        if(result == 0){
           return AjaxResult.error("批次号不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改批次
     */
    @PreAuthorize("@ss.hasPermi('admin:batch:edit')")
    @Log(title = "批次", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改批次接口")
    public AjaxResult edit(@RequestBody BaBatch baBatch)
    {
        return toAjax(baBatchService.updateBaBatch(baBatch));
    }

    /**
     * 删除批次
     */
    @PreAuthorize("@ss.hasPermi('admin:batch:remove')")
    @Log(title = "批次", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除批次接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBatchService.deleteBaBatchByIds(ids));
    }

    /**
     * 批次校验
     */
    @GetMapping("/selectBaBatch")
    @ApiOperation(value = "批次校验")
    public AjaxResult selectBaBatch(String batch)
    {
        Integer result = baBatchService.selectBaBatch(batch);
        if(result == 0){
            return AjaxResult.error(result,"批次号已被停用");
        }else if(result == -1){
            return AjaxResult.error(result,"传入空值");
        }
        return toAjax(result);
    }
}
