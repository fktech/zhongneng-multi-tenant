package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaAutomobileSettlementDetail;
import com.business.system.service.IBaAutomobileSettlementDetailService;

/**
 * 付款明细Controller
 *
 * @author ljb
 * @date 2023-04-12
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/automobileDetail")
@Api(tags ="付款明细接口")
public class BaAutomobileSettlementDetailController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementDetailService baAutomobileSettlementDetailService;

    /**
     * 查询付款明细列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileDetail:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询付款明细列表接口")
    public TableDataInfo list(BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        startPage();
        List<BaAutomobileSettlementDetail> list = baAutomobileSettlementDetailService.selectBaAutomobileSettlementDetailList(baAutomobileSettlementDetail);
        return getDataTable(list);
    }

    /**
     * 导出付款明细列表
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileDetail:export')")
    @Log(title = "付款明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出付款明细列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlementDetail baAutomobileSettlementDetail) throws IOException
    {
        List<BaAutomobileSettlementDetail> list = baAutomobileSettlementDetailService.selectBaAutomobileSettlementDetailList(baAutomobileSettlementDetail);
        ExcelUtil<BaAutomobileSettlementDetail> util = new ExcelUtil<BaAutomobileSettlementDetail>(BaAutomobileSettlementDetail.class);
        util.exportExcel(response, list, "automobileDetail");
    }

    /**
     * 获取付款明细详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobileDetail:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取付款明细详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementDetailService.selectBaAutomobileSettlementDetailById(id));
    }

    /**
     * 新增付款明细
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileDetail:add')")
    @Log(title = "付款明细", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增付款明细接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        return toAjax(baAutomobileSettlementDetailService.insertBaAutomobileSettlementDetail(baAutomobileSettlementDetail));
    }

    /**
     * 修改付款明细
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileDetail:edit')")
    @Log(title = "付款明细", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改付款明细接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        return toAjax(baAutomobileSettlementDetailService.updateBaAutomobileSettlementDetail(baAutomobileSettlementDetail));
    }

    /**
     * 删除付款明细
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileDetail:remove')")
    @Log(title = "付款明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除付款明细接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementDetailService.deleteBaAutomobileSettlementDetailByIds(ids));
    }

    @PostMapping("/updateDetail")
    @ApiOperation(value = "昕科推送付款状态")
    @Anonymous
    public AjaxResult updateDetail(@RequestBody JSONObject automobileDetail)
    {
        int result = baAutomobileSettlementDetailService.updateDetail(automobileDetail);


        return toAjax(result);
    }
}
