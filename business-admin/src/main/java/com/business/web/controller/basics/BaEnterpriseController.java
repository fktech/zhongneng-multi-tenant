package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaHiEnterprise;
import com.business.system.domain.dto.BaCompanyInstanceRelatedEditDTO;
import com.business.system.domain.dto.BaEnterpriseInstanceRelatedEditDTO;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaEnterprise;
import com.business.system.service.IBaEnterpriseService;


/**
 * 用煤企业Controller
 *
 * @author ljb
 * @date 2022-11-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/enterprise")
@Api(tags ="用煤企业接口")
public class BaEnterpriseController extends BaseController
{
    @Autowired
    private IBaEnterpriseService baEnterpriseService;

    /**
     * 查询用煤企业列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询用煤企业列表接口")
    public TableDataInfo list(BaEnterprise baEnterprise)
    {
        //租户ID
        baEnterprise.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaEnterprise> list = baEnterpriseService.selectBaEnterpriseList(baEnterprise);
        return getDataTable(list);
    }


    /**
     * 导出用煤企业列表
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:export')")
    @Log(title = "用煤企业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出用煤企业列表接口")
    public void export(HttpServletResponse response, BaEnterprise baEnterprise) throws IOException
    {
        //租户ID
        baEnterprise.setTenantId(SecurityUtils.getCurrComId());
        List<BaEnterprise> list = baEnterpriseService.selectBaEnterpriseList(baEnterprise);
        ExcelUtil<BaEnterprise> util = new ExcelUtil<BaEnterprise>(BaEnterprise.class);
        util.exportExcel(response, list, "enterprise");
    }

    /**
     * 获取用煤企业详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取用煤企业详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baEnterpriseService.selectBaEnterpriseById(id));
    }

    @GetMapping(value = "/detEnterprise")
    @ApiOperation(value = "获取终端详细信息接口")
    public AjaxResult detEnterprise(String id)
    {
        return AjaxResult.success(baEnterpriseService.selectBaEnterpriseById(id));
    }

    /**
     * 新增用煤企业
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:add')")
    @Log(title = "用煤企业", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增用煤企业接口")
    public AjaxResult add(@RequestBody BaEnterprise baEnterprise)
    {
        Integer result = baEnterpriseService.insertBaEnterprise(baEnterprise);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改用煤企业
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:edit')")
    @Log(title = "用煤企业", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改用煤企业接口")
    public AjaxResult edit(@RequestBody BaEnterprise baEnterprise)
    {
        int result = baEnterpriseService.updateBaEnterprise(baEnterprise);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改账户信息
     */
    @Log(title = "用煤企业", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation(value = "修改用煤企业接口")
    public AjaxResult update(@RequestBody BaEnterprise baEnterprise){

        return toAjax(baEnterpriseService.updateEnterprise(baEnterprise));
    }

    /**
     * 删除用煤企业
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:remove')")
    @Log(title = "用煤企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除用煤企业接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baEnterpriseService.deleteBaEnterpriseByIds(ids));
    }

    /**
     * 封装终端企业下拉列表
     */
    @GetMapping("/enterpriseList")
    @ApiOperation(value = "封装终端企业下拉列表接口")
    public List<BaEnterprise> selectEnterpriseList()
    {
        return baEnterpriseService.selectEnterpriseList();
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baEnterpriseService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody BaEnterpriseInstanceRelatedEditDTO baEnterpriseInstanceRelatedEditDTO) {
        int result = baEnterpriseService.updateProcessBusinessData(baEnterpriseInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    @ApiOperation("终端企业删除所需煤种")
    @GetMapping("/delete/{id}")
    public AjaxResult delete(@PathVariable String id){
        return toAjax(baEnterpriseService.delete(id));
    }


    /**
     * 终端企业下拉
     */
    @GetMapping("/select")
    @ApiOperation(value = "终端企业下拉")
    public TableDataInfo list()
    {

        List<BaEnterprise> list = baEnterpriseService.enterpriseList();
        return getDataTable(list);
    }

    /**
     * 变更终端企业
     */
    @PreAuthorize("@ss.hasPermi('admin:enterprise:change')")
    @Log(title = "终端企业", businessType = BusinessType.UPDATE)
    @PostMapping("/change")
    @ApiOperation(value = "变更终端企业接口")
    public AjaxResult change(@RequestBody BaEnterprise baEnterprise) throws Exception {
        int result = baEnterpriseService.changeBaEnterprise(baEnterprise);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }else if(result == -3){
            return AjaxResult.error(-3,"未进行任何变更");
        }
        return toAjax(result);
    }

    /**
     * 查询用煤企业变更历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeList")
    @ApiOperation(value = "查询用煤企业变更历史列表")
    public TableDataInfo changeList(BaHiEnterprise baHiEnterprise)
    {
        startPage();
        List<BaHiEnterprise> list = baEnterpriseService.selectChangeBaHiEnterpriseList(baHiEnterprise);
        return getDataTable(list);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    @GetMapping(value = "/change/{id}")
    @ApiOperation(value = "获取当前数据和上一次变更记录对比数据")
    public List<String> selectChangeDataList(@PathVariable String id)
    {
        return baEnterpriseService.selectChangeDataList(id);
    }

    /**
     * 查询用煤企业变更记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeDataList")
    @ApiOperation(value = "查询用煤企业变更记录列表")
    public TableDataInfo changeDataList(BaHiEnterprise baHiEnterprise)
    {
        startPage();
        List<BaHiEnterprise> list = baEnterpriseService.selectChangeDataBaHiEnterpriseList(baHiEnterprise);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 获取客商类型
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:query')")
    @GetMapping(value = "/companyType/{id}")
    @ApiOperation(value = "获取客商类型")
    public AjaxResult getCompanyType(@PathVariable("id") String id)
    {
        return AjaxResult.success(baEnterpriseService.getCompanyType(id));
    }
}
