package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaFinancingApproval;
import com.business.system.service.IBaFinancingApprovalService;

/**
 * 融资审批Controller
 *
 * @author single
 * @date 2023-05-03
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/financingApproval")
@Api(tags ="融资审批接口")
public class BaFinancingApprovalController extends BaseController
{
    @Autowired
    private IBaFinancingApprovalService baFinancingApprovalService;

    /**
     * 查询融资审批列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:financingApproval:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询融资审批列表接口")
    public TableDataInfo list(BaFinancingApproval baFinancingApproval)
    {
        startPage();
        List<BaFinancingApproval> list = baFinancingApprovalService.selectBaFinancingApprovalList(baFinancingApproval);
        return getDataTable(list);
    }

    /**
     * 导出融资审批列表
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:export')")
    @Log(title = "融资审批", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出融资审批列表接口")
    public void export(HttpServletResponse response, BaFinancingApproval baFinancingApproval) throws IOException
    {
        List<BaFinancingApproval> list = baFinancingApprovalService.selectBaFinancingApprovalList(baFinancingApproval);
        ExcelUtil<BaFinancingApproval> util = new ExcelUtil<BaFinancingApproval>(BaFinancingApproval.class);
        util.exportExcel(response, list, "financingApproval");
    }

    /**
     * 获取融资审批详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取融资审批详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baFinancingApprovalService.selectBaFinancingApprovalById(id));
    }

    /**
     * 新增融资审批
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:add')")
    @Log(title = "融资审批", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增融资审批接口")
    public AjaxResult add(@RequestBody BaFinancingApproval baFinancingApproval)
    {
        return toAjax(baFinancingApprovalService.insertBaFinancingApproval(baFinancingApproval));
    }

    /**
     * 修改融资审批
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:edit')")
    @Log(title = "融资审批", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改融资审批接口")
    public AjaxResult edit(@RequestBody BaFinancingApproval baFinancingApproval)
    {
        return toAjax(baFinancingApprovalService.updateBaFinancingApproval(baFinancingApproval));
    }

    /**
     * 删除融资审批
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:remove')")
    @Log(title = "融资审批", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除融资审批接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baFinancingApprovalService.deleteBaFinancingApprovalByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:financingApproval:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baFinancingApprovalService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
