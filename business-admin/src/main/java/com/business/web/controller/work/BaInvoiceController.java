package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.core.domain.UR;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaContract;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaInvoice;
import com.business.system.service.IBaInvoiceService;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.core.page.TableDataInfo;

/**
 * 发票信息列Controller
 *
 * @author js
 * @date 2022-12-28
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/invoice")
@Api(tags ="发票信息列接口")
public class BaInvoiceController extends BaseController
{
    @Autowired
    private IBaInvoiceService baInvoiceService;

    /**
     * 查询发票信息列列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:invoice:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询发票信息列列表接口")
    public TableDataInfo list(BaInvoice baInvoice)
    {
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaInvoice> list = baInvoiceService.selectBaInvoiceList(baInvoice);
        return getDataTable(list);
    }

    /**
     * 导出发票信息列列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:invoice:export')")
    @Log(title = "发票信息列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出发票信息列列表接口")
    public void export(HttpServletResponse response, BaInvoice baInvoice) throws IOException
    {
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        List<BaInvoice> list = baInvoiceService.selectBaInvoiceList(baInvoice);
        for (BaInvoice invoice:list) {
            //开票信息为空
            if(invoice.getBaInvoiceDetailList().size() == 0){
                invoice.setBaInvoiceDetailList(null);
            }
        }
        ExcelUtil<BaInvoice> util = new ExcelUtil<BaInvoice>(BaInvoice.class);
        util.exportExcel(response, list, "invoice");
    }

    /**
     * 获取发票信息列详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取发票信息列详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baInvoiceService.selectBaInvoiceById(id));
    }

    /**
     * 根据ID获取发票信息
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:query')")
    @GetMapping(value = "/info/{id}")
    @ApiOperation(value = "根据ID获取发票信息")
    public AjaxResult getInfoById(@PathVariable("id") String id)
    {
        return AjaxResult.success(baInvoiceService.selectBaInvoiceInfoById(id));
    }

    /**
     * 新增发票信息列
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:add')")
    @Log(title = "发票信息列", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增发票信息列接口")
    public AjaxResult add(@RequestBody BaInvoice baInvoice)
    {
        return toAjax(baInvoiceService.insertBaInvoice(baInvoice));
    }

    /**
     * 修改发票信息列
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:edit')")
    @Log(title = "发票信息列", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改发票信息列接口")
    public AjaxResult edit(@RequestBody BaInvoice baInvoice)
    {
        Integer result = baInvoiceService.updateBaInvoice(baInvoice);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除发票信息列
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:remove')")
    @Log(title = "发票信息列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除发票信息列接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baInvoiceService.deleteBaInvoiceByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baInvoiceService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 立项发票统计
     */
    @GetMapping("/baInvoiceTotal")
    @ApiOperation(value = "立项发票统计")
    public UR baInvoiceTotal(BaInvoice baInvoice){
        UR ur = baInvoiceService.baInvoiceTotal(baInvoice);
        return ur;
    }

    /**
     * 查询多租户授权信息发票列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:invoice:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息发票列表")
    public TableDataInfo authorizedList(BaInvoice baInvoice) {
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaInvoice> list = baInvoiceService.selectBaInvoiceAuthorizedList(baInvoice);
        return getDataTable(list);
    }

    /**
     * 授权发票管理
     */
    @PreAuthorize("@ss.hasPermi('admin:invoice:authorized')")
    @Log(title = "发票管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权发票管理")
    public AjaxResult authorized(@RequestBody BaInvoice baInvoice)
    {
        return AjaxResult.success(baInvoiceService.authorizedBaInvoice(baInvoice));
    }
}
