package com.business.web.controller.work;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.*;
import com.business.system.domain.dto.DictTypeDTO;
import com.business.system.domain.dto.MonthWaybilDTO;
import com.business.system.domain.dto.OngoingcontractsDto;
import com.business.system.domain.dto.RolenumDTO;
import com.business.system.domain.vo.BaDriveAi;
import com.business.system.domain.vo.BaEnter;
import com.business.system.domain.vo.BaShippingOrderVO;
import com.business.system.service.*;
import com.business.system.service.workflow.IBaDriveAiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 智慧驾驶舱网络货运Controller
 *
 * @author js
 * @date 2023-8-30
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/networkFreight")
@Api(tags ="智慧驾驶舱网络货运接口")
public class BaNetworkFreightController extends BaseController
{

    @Autowired
    private IBaShippingOrderService baShippingOrderService;

    @Autowired
    private IBaTransportAutomobileService baTransportAutomobileService;

    @Autowired
    private IBaCompanyService baCompanyService;

    /**
     * 统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）对比
     */
    @GetMapping("/recentCars")
    @ApiOperation(value = "统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）对比")
    public AjaxResult recentCars() {
        //查询供货中合同
        return success(baShippingOrderService.recentCars());
    }

    /**
     * 拉运货物占比
     */
    @GetMapping("/selectGoodsProportion")
    @ApiOperation(value = "拉运货物占比")
    public AjaxResult selectGoodsProportion() {
        return success(baShippingOrderService.selectGoodsProportion());
    }

    /**
     * 地图数据组装
     */
    @GetMapping("/atlasData")
    @ApiOperation(value = "地图数据组装")
    public AjaxResult atlasData() {
        return success(baTransportAutomobileService.atlasData());
    }

    /**
     * 运单本月数据统计
     */
    @GetMapping("/monthWaybill")
    @ApiOperation(value = "本月运单数据")
    public AjaxResult monthWaybill(MonthWaybilDTO monthWaybilDTO) {
        return success(baShippingOrderService.monthWaybill(monthWaybilDTO));
    }

    /**
     * 运单明细
     */
    @GetMapping("/waybillDetails")
    @ApiOperation(value = "运单明细")
    public AjaxResult waybillDetails(MonthWaybilDTO monthWaybilDTO) {
        return success(baShippingOrderService.waybillDetails(monthWaybilDTO));
    }

    /**
     * 承运单位发运
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/selectCompanyRanking")
    @ApiOperation(value = "承运单位发运")
    public AjaxResult selectCompanyRanking() {
        return AjaxResult.success(baCompanyService.countRanking());
    }

}

