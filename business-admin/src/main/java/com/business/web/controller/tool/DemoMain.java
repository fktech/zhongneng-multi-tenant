package com.business.web.controller.tool;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.UR;
import com.openapi.sdk.service.DataExchangeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Api("中交接口测试")
@RestController
@RequestMapping("/test/transport")
public class DemoMain extends BaseController {

    @ApiOperation("中交接口测试插件下载")
    @GetMapping("/select")
    @Anonymous
    public UR plugin() throws Exception {

        Map<String, String> map = new HashMap<String, String>(3);
        //正式账号
        map.put("cid", "d99c292b-ae63-4be8-8834-9267727ddd2f");
        map.put("srt", "53ad8997-4c7d-4573-a55a-b9bfd2e87dd0");
        map.put("type", "pathTrack");
        String url = " https://zhiyunopenapi.95155.com/save/apis/pluginUrl";
        DataExchangeService des = new DataExchangeService(5000, 8000);
        System.out.println("请求地址:" + url);
        // 通过 https 方式调用，此方法内部会使用私钥生成签名参数 sign,私钥不会发送
        String res = des.postHttps(url, map);
        return UR.ok().data("res", res);
    }

    @ApiOperation("中交接口测试运输节点")
    @GetMapping("/point")
    @Anonymous
    public UR transTimeManage() throws Exception {
        Map<String, String> map = new HashMap<String, String>(3);
        //测试账号
        map.put("cid", "dcf22c92-21a2-418c-9ef3-dd7ba1055534");
        map.put("srt", "a37d4fb5-3f0e-420e-937c-d72cc8ef285f");
        map.put("vnos", "陕YH0009_2");
        map.put("timeNearby", "30");
        String url = " https://openapi-test.sinoiov.cn/save/apis/transTimeManageV3";
        DataExchangeService des = new DataExchangeService(5000, 8000);
        System.out.println("请求地址:" + url);
        // 通过 https 方式调用，此方法内部会使用私钥生成签名参数 sign,私钥不会发送
        String res = des.postHttps(url, map);
        return UR.ok().data("res", res);
    }

    @ApiOperation("中交接口测试运输行程")
    @GetMapping("/itinerary")
    @Anonymous
    public UR itinerary() throws Exception {
        Map<String, String> map = new HashMap<String, String>(3);
        //测试账号
        map.put("cid", "dcf22c92-21a2-418c-9ef3-dd7ba1055534");
        map.put("srt", "a37d4fb5-3f0e-420e-937c-d72cc8ef285f");
        map.put("vclN", "陕YH0009");
        map.put("vco", "2");
        map.put("qryBtm", "2021-09-05 15:56:46");
        map.put("qryEtm", "2021-09-06 15:56:46");
        map.put("startLonlat", "116.31795,30.4252");
        map.put("endLonlat", "110.9946817,25.9089816");
        String url = " https://openapi-test.sinoiov.cn/save/apis/routerPath";
        DataExchangeService des = new DataExchangeService(5000, 8000);
        System.out.println("请求地址:" + url);
        // 通过 https 方式调用，此方法内部会使用私钥生成签名参数 sign,私钥不会发送
        String res = des.postHttps(url, map);
        return UR.ok().data("res", JSONObject.parse(res));
    }
}
