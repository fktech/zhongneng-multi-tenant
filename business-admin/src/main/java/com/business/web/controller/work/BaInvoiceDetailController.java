package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaInvoiceDetail;
import com.business.system.service.IBaInvoiceDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 发票信息详情Controller
 *
 * @author single
 * @date 2023-03-14
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/detail")
@Api(tags ="发票信息详情接口")
public class BaInvoiceDetailController extends BaseController
{
    @Autowired
    private IBaInvoiceDetailService baInvoiceDetailService;

    /**
     * 查询发票信息详情列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询发票信息详情列表接口")
    public TableDataInfo list(BaInvoiceDetail baInvoiceDetail)
    {
        startPage();
        List<BaInvoiceDetail> list = baInvoiceDetailService.selectBaInvoiceDetailList(baInvoiceDetail);
        return getDataTable(list);
    }

    /**
     * 导出发票信息详情列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:export')")
    @Log(title = "发票信息详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出发票信息详情列表接口")
    public void export(HttpServletResponse response, BaInvoiceDetail baInvoiceDetail) throws IOException
    {
        List<BaInvoiceDetail> list = baInvoiceDetailService.selectBaInvoiceDetailList(baInvoiceDetail);
        ExcelUtil<BaInvoiceDetail> util = new ExcelUtil<BaInvoiceDetail>(BaInvoiceDetail.class);
        util.exportExcel(response, list, "detail");
    }

    /**
     * 获取发票信息详情详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取发票信息详情详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baInvoiceDetailService.selectBaInvoiceDetailById(id));
    }

    /**
     * 新增发票信息详情
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:add')")
    @Log(title = "发票信息详情", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增发票信息详情接口")
    public AjaxResult add(@RequestBody BaInvoiceDetail baInvoiceDetail)
    {
        return toAjax(baInvoiceDetailService.insertBaInvoiceDetail(baInvoiceDetail));
    }

    /**
     * 修改发票信息详情
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:edit')")
    @Log(title = "发票信息详情", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改发票信息详情接口")
    public AjaxResult edit(@RequestBody BaInvoiceDetail baInvoiceDetail)
    {
        return toAjax(baInvoiceDetailService.updateBaInvoiceDetail(baInvoiceDetail));
    }

    /**
     * 删除发票信息详情
     */
//    @PreAuthorize("@ss.hasPermi('admin:detail:remove')")
    @Log(title = "发票信息详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除发票信息详情接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baInvoiceDetailService.deleteBaInvoiceDetailByIds(ids));
    }
}
