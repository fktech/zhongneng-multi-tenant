package com.business.web.controller.work;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaContract;
import com.business.system.domain.BaSettlement;
import com.business.system.domain.dto.CountPaymentDTO;
import com.business.system.domain.dto.BaPaymentVoucherDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaPayment;
import com.business.system.service.IBaPaymentService;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.core.page.TableDataInfo;

/**
 * 付款信息Controller
 *
 * @author js
 * @date 2022-12-26
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/payment")
@Api(tags ="付款信息接口")
public class BaPaymentController extends BaseController
{
    @Autowired
    private IBaPaymentService baPaymentService;

    /**
     * 查询付款信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:payment:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询付款信息列表接口")
    public TableDataInfo list(BaPayment baPayment)
    {
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaPayment> list = baPaymentService.selectBaPaymentList(baPayment);
        return getDataTable(list);
    }

    /**
     * 导出付款信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:payment:export')")
    @Log(title = "付款信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出付款信息列表接口")
    public void export(HttpServletResponse response, BaPayment baPayment) throws IOException
    {
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        List<BaPayment> list = baPaymentService.selectBaPaymentList(baPayment);
        for (BaPayment payment:list) {
            if(StringUtils.isEmpty(payment.getStartName())){
                if(StringUtils.isNotEmpty(payment.getPlanName())){
                    payment.setStartName(payment.getPlanName());
                }
            }
            //付款凭证转换
            if(payment.getBaPaymentVouchers().size() == 0){
                payment.setBaPaymentVouchers(null);
            }
        }
        ExcelUtil<BaPayment> util = new ExcelUtil<BaPayment>(BaPayment.class);
        util.exportExcel(response, list, "payment");
    }

    /**
     * 获取付款信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:payment:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取付款信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baPaymentService.selectBaPaymentById(id));
    }

    /**
     * 新增付款信息
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:add')")
    @Log(title = "付款信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增付款信息接口")
    public AjaxResult add(@RequestBody BaPayment baPayment)
    {
        return toAjax(baPaymentService.insertBaPayment(baPayment));
    }

    /**
     * 新增付款凭证
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:addBaPaymentVoucher')")
    @Log(title = "付款凭证", businessType = BusinessType.INSERT)
    @PostMapping(value = "/addBaPaymentVoucher")
    @ApiOperation(value = "新增付款凭证接口")
    public AjaxResult addBaPaymentVoucher(@RequestBody BaPaymentVoucherDTO baPaymentVoucherDTO)
    {
        return toAjax(baPaymentService.insertBaPaymentVoucher(baPaymentVoucherDTO));
    }

    /**
     * 修改付款信息
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:edit')")
    @Log(title = "付款信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改付款信息接口")
    public AjaxResult edit(@RequestBody BaPayment baPayment)
    {
        Integer result = baPaymentService.updateBaPayment(baPayment);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 结束付款功能
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:endPayment')")
    @GetMapping(value = "/endPayment/{id}")
    @ApiOperation(value = "结束付款功能")
    public AjaxResult endPayment(@PathVariable("id") String id)
    {
        return AjaxResult.success(baPaymentService.endPayment(id));
    }
    /**
     * 删除付款信息
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:remove')")
    @Log(title = "付款信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除付款信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baPaymentService.deleteBaPaymentByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baPaymentService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * PC端首页累计付款
     */
    @GetMapping("/countPaymentList")
    @ApiOperation(value = "PC端首页累计付款")
    public AjaxResult countPaymentList(CountPaymentDTO countPaymentDTO){
        return AjaxResult.success(baPaymentService.countPaymentList(countPaymentDTO));
    }

    /**
     * 统计付款
     */
    @GetMapping("/countDepotPaymentList")
    @ApiOperation(value = "统计付款")
    public AjaxResult countDepotPaymentList(CountPaymentDTO countPaymentDTO){
        return AjaxResult.success(baPaymentService.countDepotPaymentList(countPaymentDTO));
    }

    /**
     * 关联
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:association')")
    @PostMapping("/association")
    @ApiOperation(value = "关联")
    public AjaxResult association(@RequestBody BaPayment baPayment) {
        return toAjax(baPaymentService.association(baPayment));
    }

    /**
     * 查询多租户授权信息付款信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:payment:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息付款信息列表")
    public TableDataInfo authorizedList(BaPayment baPayment)
    {
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaPayment> list = baPaymentService.selectBaPaymentAuthorizedList(baPayment);
        return getDataTable(list);
    }

    /**
     * 授权付款管理
     */
    @PreAuthorize("@ss.hasPermi('admin:payment:authorized')")
    @Log(title = "付款管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权付款管理")
    public AjaxResult authorized(@RequestBody BaPayment baPayment)
    {
        return AjaxResult.success(baPaymentService.authorizedBaPayment(baPayment));
    }
}
