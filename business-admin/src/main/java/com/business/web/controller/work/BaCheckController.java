package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaCheck;
import com.business.system.service.IBaCheckService;

/**
 * 验收Controller
 *
 * @author ljb
 * @date 2022-12-14
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/check")
@Api(tags ="验收接口")
public class BaCheckController extends BaseController
{
    @Autowired
    private IBaCheckService baCheckService;

    /**
     * 查询验收列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:check:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询验收列表接口")
    public TableDataInfo list(BaCheck baCheck)
    {
        startPage();
        List<BaCheck> list = baCheckService.selectBaCheckList(baCheck);
        return getDataTable(list);
    }

    /**
     * 导出验收列表
     */
    @PreAuthorize("@ss.hasPermi('admin:check:export')")
    @Log(title = "验收", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出验收列表接口")
    public void export(HttpServletResponse response, BaCheck baCheck) throws IOException
    {
        List<BaCheck> list = baCheckService.selectBaCheckList(baCheck);
        ExcelUtil<BaCheck> util = new ExcelUtil<BaCheck>(BaCheck.class);
        util.exportExcel(response, list, "check");
    }

    /**
     * 获取验收详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:check:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取验收详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCheckService.selectBaCheckById(id));
    }

    /**
     * 新增验收
     */
    @PreAuthorize("@ss.hasPermi('admin:check:add')")
    @Log(title = "验收", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增验收接口")
    public AjaxResult add(@RequestBody BaCheck baCheck)
    {
        return toAjax(baCheckService.insertBaCheck(baCheck));
    }

    /**
     * 修改验收
     */
    @PreAuthorize("@ss.hasPermi('admin:check:edit')")
    @Log(title = "验收", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改验收接口")
    public AjaxResult edit(@RequestBody BaCheck baCheck)
    {
        return toAjax(baCheckService.updateBaCheck(baCheck));
    }

    /**
     * 删除验收
     */
    @PreAuthorize("@ss.hasPermi('admin:check:remove')")
    @Log(title = "验收", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除验收接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCheckService.deleteBaCheckByIds(ids));
    }
}
