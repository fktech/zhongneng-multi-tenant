package com.business.web.controller.basics;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaGoods;
import com.business.system.service.IBaGoodsService;

/**
 * 商品Controller
 *
 * @author ljb
 * @date 2022-12-01
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/goods")
@Api(tags ="商品接口")
public class BaGoodsController extends BaseController
{
    @Autowired
    private IBaGoodsService baGoodsService;

    /**
     * 查询商品列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:goods:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询商品列表接口")
    public TableDataInfo list(BaGoods baGoods)
    {
        //租户ID
        baGoods.setTenantId(SecurityUtils.getCurrComId());
        //startPage();
        List<BaGoods> list = baGoodsService.selectBaGoodsList(baGoods);
        //分页
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        TableDataInfo rspData =new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(myStartPage(list, pageNum, pageSize));
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    //列表分页
    public static List myStartPage(List list, Integer pageNum, Integer pageSize){
        List<Object> objects = new ArrayList<>();
        if(list ==null){
            return objects;
        }
        if(list.size()==0){
            return objects;
        }
        Integer count = list.size();//
        Integer pageCount =0;//
        if(count % pageSize ==0){
            pageCount = count / pageSize;
        }else{
            pageCount = count / pageSize +1;
        }
        int fromIndex =0;//
        int toIndex =0;//
        if(pageNum != pageCount){
            fromIndex =(pageNum -1)* pageSize;
            toIndex = fromIndex + pageSize;
        }else{
            fromIndex =(pageNum -1)* pageSize;
            toIndex = count;
        }
        List pageList = list.subList(fromIndex,toIndex);
        return pageList;
    }

    /**
     * 导出商品列表
     */
    @PreAuthorize("@ss.hasPermi('admin:goods:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出商品列表接口")
    public void export(HttpServletResponse response, BaGoods baGoods) throws IOException
    {
        //租户ID
        baGoods.setTenantId(SecurityUtils.getCurrComId());
        List<BaGoods> list = baGoodsService.selectBaGoodsList(baGoods);
        ExcelUtil<BaGoods> util = new ExcelUtil<BaGoods>(BaGoods.class);
        util.exportExcel(response, list, "goods");
    }

    /**
     * 获取商品详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:goods:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取商品详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baGoodsService.selectBaGoodsById(id));
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('admin:goods:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增商品接口")
    public AjaxResult add(@RequestBody BaGoods baGoods)
    {
       Integer result = baGoodsService.insertBaGoods(baGoods);

       if(result == -2){
        return AjaxResult.error("商品编码已存在");
       }else if(result == 0){
           return AjaxResult.error("添加商品失败");
       }else if(result == -1){
           return AjaxResult.error("商品名称已存在");
       }
        return toAjax(result);
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('admin:goods:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改商品接口")
    public AjaxResult edit(@RequestBody BaGoods baGoods)
    {
        int result = baGoodsService.updateBaGoods(baGoods);
        if(result == -2){
            return AjaxResult.error("商品编码已存在");
        }else if(result == -1){
            return AjaxResult.error("商品名称已存在");
        }
        return toAjax(result);
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('admin:goods:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除商品接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baGoodsService.deleteBaGoodsByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:goods:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baGoodsService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
