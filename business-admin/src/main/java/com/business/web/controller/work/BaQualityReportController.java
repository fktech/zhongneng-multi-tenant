package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaQualityReport;
import com.business.system.service.IBaQualityReportService;
/**
 * 数质量报告Controller
 *
 * @author single
 * @date 2023-07-19
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/qualityReport")
@Api(tags ="数质量报告接口")
public class BaQualityReportController extends BaseController
{
    @Autowired
    private IBaQualityReportService baQualityReportService;

    /**
     * 查询数质量报告列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:qualityReport:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询数质量报告列表接口")
    public TableDataInfo list(BaQualityReport baQualityReport)
    {
        startPage();
        List<BaQualityReport> list = baQualityReportService.selectBaQualityReportList(baQualityReport);
        return getDataTable(list);
    }

    /**
     * 导出数质量报告列表
     */
    @PreAuthorize("@ss.hasPermi('admin:qualityReport:export')")
    @Log(title = "数质量报告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出数质量报告列表接口")
    public void export(HttpServletResponse response, BaQualityReport baQualityReport) throws IOException
    {
        List<BaQualityReport> list = baQualityReportService.selectBaQualityReportList(baQualityReport);
        ExcelUtil<BaQualityReport> util = new ExcelUtil<BaQualityReport>(BaQualityReport.class);
        util.exportExcel(response, list, "qualityReport");
    }

    /**
     * 获取数质量报告详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:qualityReport:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取数质量报告详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baQualityReportService.selectBaQualityReportById(id));
    }

    /**
     * 新增数质量报告
     */
    @PreAuthorize("@ss.hasPermi('admin:qualityReport:add')")
    @Log(title = "数质量报告", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增数质量报告接口")
    public AjaxResult add(@RequestBody BaQualityReport baQualityReport)
    {
        return toAjax(baQualityReportService.insertBaQualityReport(baQualityReport));
    }

    /**
     * 修改数质量报告
     */
    @PreAuthorize("@ss.hasPermi('admin:qualityReport:edit')")
    @Log(title = "数质量报告", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改数质量报告接口")
    public AjaxResult edit(@RequestBody BaQualityReport baQualityReport)
    {
        return toAjax(baQualityReportService.updateBaQualityReport(baQualityReport));
    }

    /**
     * 删除数质量报告
     */
    @PreAuthorize("@ss.hasPermi('admin:qualityReport:remove')")
    @Log(title = "数质量报告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除数质量报告接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baQualityReportService.deleteBaQualityReportByIds(ids));
    }
}
