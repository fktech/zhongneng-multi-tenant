package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.OaEntertainInstanceRelatedEditDTO;
import com.business.system.domain.dto.OaMaintenanceExpansionInstanceRelatedEditDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaEntertain;
import com.business.system.service.IOaEntertainService;

/**
 * 招待申请Controller
 *
 * @author ljb
 * @date 2023-11-13
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaEntertain")
@Api(tags ="招待申请接口")
public class OaEntertainController extends BaseController
{
    @Autowired
    private IOaEntertainService oaEntertainService;

    /**
     * 查询招待申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEntertain:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询招待申请列表接口")
    public TableDataInfo list(OaEntertain oaEntertain)
    {
        //租户ID
        oaEntertain.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaEntertain> list = oaEntertainService.selectOaEntertainList(oaEntertain);
        return getDataTable(list);
    }

    /**
     * 导出招待申请列表
     */
    @PreAuthorize("@ss.hasPermi('admin:oaEntertain:export')")
    //@Log(title = "招待申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出招待申请列表接口")
    public void export(HttpServletResponse response, OaEntertain oaEntertain) throws IOException
    {
        //租户ID
        oaEntertain.setTenantId(SecurityUtils.getCurrComId());
        List<OaEntertain> list = oaEntertainService.selectOaEntertainList(oaEntertain);
        ExcelUtil<OaEntertain> util = new ExcelUtil<OaEntertain>(OaEntertain.class);
        util.exportExcel(response, list, "oaEntertain");
    }

    /**
     * 获取招待申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEntertain:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取招待申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaEntertainService.selectOaEntertainById(id));
    }

    /**
     * 新增招待申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEntertain:add')")
    @Log(title = "招待申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增招待申请接口")
    public AjaxResult add(@RequestBody OaEntertain oaEntertain)
    {
        int result = oaEntertainService.insertOaEntertain(oaEntertain);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改招待申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEntertain:edit')")
    @Log(title = "招待申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改招待申请接口")
    public AjaxResult edit(@RequestBody OaEntertain oaEntertain)
    {
        int result = oaEntertainService.updateOaEntertain(oaEntertain);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除招待申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaEntertain:remove')")
    @Log(title = "招待申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除招待申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaEntertainService.deleteOaEntertainByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEntertain:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaEntertainService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody OaEntertainInstanceRelatedEditDTO oaEntertainInstanceRelatedEditDTO) {
        int result = oaEntertainService.updateProcessBusinessData(oaEntertainInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }
        return toAjax(result);
    }
}
