package com.business.web.controller.system;

import java.util.List;

import com.business.common.core.domain.entity.SysUser;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.mapper.SysPostMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.ISysPostService;
import com.business.system.util.PostName;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.constant.UserConstants;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.enums.BusinessType;
import com.business.common.utils.StringUtils;
import com.business.system.service.ISysDeptService;

/**
 * 部门信息
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController extends BaseController
{
    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PostName postName;

    @Autowired
    private SysPostMapper postMapper;

    /**
     * 获取部门列表
     */
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list")
    public AjaxResult list(SysDept dept)
    {
        dept.setTenantId(SecurityUtils.getCurrComId());
        List<SysDept> depts = deptService.selectDeptList(dept);
        if(!CollectionUtils.isEmpty(depts)){
            for(SysDept sysDept : depts){
                if(!ObjectUtils.isEmpty(sysDept)){
                    List<Long> longs = postService.selectPostListByDeptId(sysDept.getDeptId());
                    if(!CollectionUtils.isEmpty(longs)){
                        /*创建数组*/
                        Long[] deptIds = new Long[longs.size()];
                        /*数组赋值*/
                        deptIds = longs.toArray(deptIds);
                        sysDept.setPostIds(deptIds);
                    }
                    SysUser sysUser = new SysUser();
                    sysUser.setDeptId(sysDept.getDeptId());
                    sysUser.setStatus("0");
                    List<SysUser> sysUsers = sysUserMapper.selectUserList(sysUser);
                    if(!CollectionUtils.isEmpty(sysUsers)){
                        for(SysUser sysUser1 : sysUsers){
                            if(!ObjectUtils.isEmpty(sysUser1)){
                                String name = postName.getName(sysUser1.getUserId());
                                if(name.equals("") == false){
                                    sysUser1.setPostName(name.substring(0, name.length() - 1));
                                }
                            }
                        }
                    }
                    sysDept.setSysUsers(sysUsers);
                }
            }
        }
        return success(depts);
    }

    /**
     * 查询部门列表（排除节点）
     */
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list/exclude/{deptId}")
    public AjaxResult excludeChild(@PathVariable(value = "deptId", required = false) Long deptId)
    {
        SysDept dept = new SysDept();
        dept.setTenantId(SecurityUtils.getCurrComId());
        List<SysDept> depts = deptService.selectDeptList(dept);
        depts.removeIf(d -> d.getDeptId().intValue() == deptId || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""));
        return success(depts);
    }

    /**
     * 根据部门编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dept:query')")
    @GetMapping(value = "/{deptId}")
    public AjaxResult getInfo(@PathVariable Long deptId)
    {
        deptService.checkDeptDataScope(deptId);
        SysDept sysDept = deptService.selectDeptById(deptId);
        if(!ObjectUtils.isEmpty(sysDept)){
            List<Long> longs = postService.selectPostListByDeptId(deptId);
            if(!CollectionUtils.isEmpty(longs)){
                /*创建数组*/
                Long[] deptIds = new Long[longs.size()];
                /*数组赋值*/
                deptIds = longs.toArray(deptIds);
                sysDept.setPostIds(deptIds);
            }
        }
        return success(sysDept);
    }

    /**
     * 新增部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:add')")
    @Log(title = "部门管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysDept dept)
    {
        if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept)))
        {
            return error("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        dept.setCreateBy(getUsername());
        int result = deptService.insertDept(dept);
        if(result == 0){
            return AjaxResult.error(0,"新增部门数据同步钉钉失败，请联系管理员");
        }
        return toAjax(result);
    }

    /**
     * 修改部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:edit')")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysDept dept)
    {
        Long deptId = dept.getDeptId();
        deptService.checkDeptDataScope(deptId);
        if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept)))
        {
            return error("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        else if (dept.getParentId().equals(deptId))
        {
            return error("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
        }
        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus()) && deptService.selectNormalChildrenDeptById(deptId) > 0)
        {
            return error("该部门包含未停用的子部门！");
        }
        dept.setUpdateBy(getUsername());
        int result = deptService.updateDept(dept);
        if(result == 0){
            return AjaxResult.error(0,"修改部门数据同步钉钉失败，请联系管理员");
        }
        return toAjax(result);
    }

    /**
     * 删除部门
     */
    @PreAuthorize("@ss.hasPermi('system:dept:remove')")
    @Log(title = "部门管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{deptId}")
    public AjaxResult remove(@PathVariable Long deptId)
    {
        if (deptService.hasChildByDeptId(deptId))
        {
            return warn("存在下级部门,不允许删除");
        }
        if (deptService.checkDeptExistUser(deptId))
        {
            return warn("部门存在用户,不允许删除");
        }
        deptService.checkDeptDataScope(deptId);
        int result = deptService.deleteDeptById(deptId);
        if(result == 0){
            return AjaxResult.error(0,"删除部门数据同步钉钉失败，请联系管理员");
        }
        return toAjax(result);
    }

    /**
     * 查询所有子公司
     */
    //@PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/selectSubCompanyList")
    public AjaxResult selectSubCompanyList()
    {
        //租户ID
        String tenantId = SecurityUtils.getCurrComId();
        return success(deptService.selectSubCompanyList(tenantId));
    }

    /**
     * 查询所有集团及子公司
     */
    //@PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/selectGroupCompanyList")
    public AjaxResult selectGroupCompanyList()
    {
        return success(deptService.selectGroupCompanyList());
    }

    /**
     * 查询所有关联部门
     * @param roleId
     * @return
     */
    @GetMapping("/selectDeptIdByRoleId")
    public AjaxResult selectGroupCompanyList(String roleId)
    {
        return success(deptService.selectDeptIdByRoleId(Long.valueOf(roleId)));
    }


    /**
     * 更新历史钉钉部门
     */
//    @PreAuthorize("@ss.hasPermi('system:dept:add')")
    @Log(title = "更新历史钉钉部门", businessType = BusinessType.INSERT)
    @PostMapping("/initDingdingDepot")
    public AjaxResult initDingdingDepot(@RequestBody SysDept dept)
    {
        String result = deptService.insertDingdingDept(dept);
        if("0".equals(result)){
            return AjaxResult.error(0,"新增用户失败，");
        }
        return AjaxResult.success(result);
    }
}
