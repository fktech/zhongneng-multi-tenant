package com.business.web.controller.oa;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaClaim;
import com.business.system.service.IOaClaimService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 报销申请Controller
 *
 * @author single
 * @date 2023-12-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaClaim")
@Api(tags ="报销申请接口")
public class OaClaimController extends BaseController
{
    @Autowired
    private IOaClaimService oaClaimService;

    /**
     * 查询报销申请列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaim:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询报销申请列表接口")
    public TableDataInfo list(OaClaim oaClaim)
    {
        //租户ID
        oaClaim.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaClaim> list = oaClaimService.selectOaClaimList(oaClaim);
        return getDataTable(list);
    }

    /**
     * 导出报销申请列表
     */
    @PreAuthorize("@ss.hasPermi('admin:oaClaim:export')")
    @Log(title = "报销申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出报销申请列表接口")
    public void export(HttpServletResponse response, OaClaim oaClaim) throws IOException
    {
        //租户ID
        oaClaim.setTenantId(SecurityUtils.getCurrComId());
        List<OaClaim> list = oaClaimService.selectOaClaimList(oaClaim);
        ExcelUtil<OaClaim> util = new ExcelUtil<OaClaim>(OaClaim.class);
        util.exportExcel(response, list, "oa_claim");
    }

    /**
     * 获取报销申请详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaim:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取报销申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClaimService.selectOaClaimById(id));
    }

    /**
     * 新增报销申请
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaim:add')")
    @Log(title = "报销申请", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增报销申请接口")
    public AjaxResult add(@RequestBody OaClaim oaClaim)
    {
        int result = oaClaimService.insertOaClaim(oaClaim);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改报销申请
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaim:edit')")
    @Log(title = "报销申请", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改报销申请接口")
    public AjaxResult edit(@RequestBody OaClaim oaClaim)
    {
        int result = oaClaimService.updateOaClaim(oaClaim);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除报销申请
     */
//    @PreAuthorize("@ss.hasPermi('admin:oaClaim:remove')")
    @Log(title = "报销申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除报销申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaClaimService.deleteOaClaimByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaEvection:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaClaimService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
