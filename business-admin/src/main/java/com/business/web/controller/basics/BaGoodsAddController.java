package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaGoodsAdd;
import com.business.system.service.IBaGoodsAddService;

/**
 * 商品附加Controller
 *
 * @author ljb
 * @date 2022-12-01
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/goodsAdd")
@Api(tags ="商品附加接口")
public class BaGoodsAddController extends BaseController
{
    @Autowired
    private IBaGoodsAddService baGoodsAddService;

    /**
     * 查询商品附加列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:goodsAdd:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询商品附加列表接口")
    public TableDataInfo list(BaGoodsAdd baGoodsAdd)
    {
        startPage();
        List<BaGoodsAdd> list = baGoodsAddService.selectBaGoodsAddList(baGoodsAdd);
        return getDataTable(list);
    }

    /**
     * 导出商品附加列表
     */
    @PreAuthorize("@ss.hasPermi('admin:goodsAdd:export')")
    @Log(title = "商品附加", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出商品附加列表接口")
    public void export(HttpServletResponse response, BaGoodsAdd baGoodsAdd) throws IOException
    {
        List<BaGoodsAdd> list = baGoodsAddService.selectBaGoodsAddList(baGoodsAdd);
        ExcelUtil<BaGoodsAdd> util = new ExcelUtil<BaGoodsAdd>(BaGoodsAdd.class);
        util.exportExcel(response, list, "goodsAdd");
    }

    /**
     * 获取商品附加详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:goodsAdd:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取商品附加详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baGoodsAddService.selectBaGoodsAddById(id));
    }

    /**
     * 新增商品附加
     */
    @PreAuthorize("@ss.hasPermi('admin:goodsAdd:add')")
    @Log(title = "商品附加", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增商品附加接口")
    public AjaxResult add(@RequestBody BaGoodsAdd baGoodsAdd)
    {
        return toAjax(baGoodsAddService.insertBaGoodsAdd(baGoodsAdd));
    }

    /**
     * 修改商品附加
     */
    @PreAuthorize("@ss.hasPermi('admin:goodsAdd:edit')")
    @Log(title = "商品附加", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改商品附加接口")
    public AjaxResult edit(@RequestBody BaGoodsAdd baGoodsAdd)
    {
        return toAjax(baGoodsAddService.updateBaGoodsAdd(baGoodsAdd));
    }

    /**
     * 删除商品附加
     */
    @PreAuthorize("@ss.hasPermi('admin:goodsAdd:remove')")
    @Log(title = "商品附加", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除商品附加接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baGoodsAddService.deleteBaGoodsAddByIds(ids));
    }
}
