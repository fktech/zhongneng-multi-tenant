package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaSettlementFreight;
import com.business.system.service.IBaSettlementFreightService;

/**
 * 汽运结算Controller
 *
 * @author ljb
 * @date 2023-02-24
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/settlementFreight")
@Api(tags ="汽运结算")
public class BaSettlementFreightController extends BaseController
{
    @Autowired
    private IBaSettlementFreightService baSettlementFreightService;

    /**
     * 查询汽运结算列表
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询汽运结算列表接口")
    public TableDataInfo list(BaSettlementFreight baSettlementFreight)
    {
        startPage();
        List<BaSettlementFreight> list = baSettlementFreightService.selectBaSettlementFreightList(baSettlementFreight);
        return getDataTable(list);
    }

    /**
     * 导出汽运结算列表
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:export')")
    @Log(title = "汽运结算", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出汽运结算列表接口")
    public void export(HttpServletResponse response, BaSettlementFreight baSettlementFreight) throws IOException
    {
        List<BaSettlementFreight> list = baSettlementFreightService.selectBaSettlementFreightList(baSettlementFreight);
        ExcelUtil<BaSettlementFreight> util = new ExcelUtil<BaSettlementFreight>(BaSettlementFreight.class);
        util.exportExcel(response, list, "settlementFreight");
    }

    /**
     * 获取汽运结算详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取汽运结算详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baSettlementFreightService.selectBaSettlementFreightById(id));
    }

    /**
     * 新增汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:add')")
    @Log(title = "汽运结算", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增汽运结算接口")
    public AjaxResult add(@RequestBody BaSettlementFreight baSettlementFreight)
    {
        return toAjax(baSettlementFreightService.insertBaSettlementFreight(baSettlementFreight));
    }

    /**
     * 修改汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:edit')")
    @Log(title = "汽运结算", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改汽运结算接口")
    public AjaxResult edit(@RequestBody BaSettlementFreight baSettlementFreight)
    {
        return toAjax(baSettlementFreightService.updateBaSettlementFreight(baSettlementFreight));
    }

    /**
     * 删除汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:settlementFreight:remove')")
    @Log(title = "汽运结算", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除汽运结算接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baSettlementFreightService.deleteBaSettlementFreightByIds(ids));
    }
}
