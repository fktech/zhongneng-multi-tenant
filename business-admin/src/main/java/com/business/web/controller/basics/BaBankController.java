package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaBank;
import com.business.system.service.IBaBankService;
import com.business.common.annotation.Log;

/**
 * 开户行Controller
 *
 * @author ljb
 * @date 2022-12-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/bank")
@Api(tags ="开户行接口")
public class BaBankController extends BaseController
{
    @Autowired
    private IBaBankService baBankService;

    /**
     * 查询开户行列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:bank:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询开户行列表接口")
    public TableDataInfo list(BaBank baBank)
    {
        startPage();
        List<BaBank> list = baBankService.selectBaBankList(baBank);
        return getDataTable(list);
    }

    /**
     * 导出开户行列表
     */
    @PreAuthorize("@ss.hasPermi('admin:bank:export')")
    @Log(title = "开户行", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出开户行列表接口")
    public void export(HttpServletResponse response, BaBank baBank) throws IOException
    {
        List<BaBank> list = baBankService.selectBaBankList(baBank);
        ExcelUtil<BaBank> util = new ExcelUtil<BaBank>(BaBank.class);
        util.exportExcel(response, list, "bank");
    }

    /**
     * 获取开户行详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:bank:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取开户行详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBankService.selectBaBankById(id));
    }

    /**
     * 新增开户行
     */
    @PreAuthorize("@ss.hasPermi('admin:bank:add')")
    @Log(title = "开户行", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增开户行接口")
    public AjaxResult add(@RequestBody BaBank baBank)
    {
        return toAjax(baBankService.insertBaBank(baBank));
    }

    /**
     * 修改开户行
     */
    @PreAuthorize("@ss.hasPermi('admin:bank:edit')")
    @Log(title = "开户行", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改开户行接口")
    public AjaxResult edit(@RequestBody BaBank baBank)
    {
        return toAjax(baBankService.updateBaBank(baBank));
    }

    /**
     * 删除开户行
     */
    //@PreAuthorize("@ss.hasPermi('admin:bank:remove')")
    @Log(title = "开户行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除开户行接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBankService.deleteBaBankByIds(ids));
    }
}
