package com.business.web.controller.system;

import com.business.common.core.domain.entity.BaCars;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.BaCar;
import com.business.system.domain.FileUploadResult;
import com.business.system.domain.SysCompany;
import com.business.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.business.common.annotation.Log;
import com.business.common.constant.UserConstants;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.framework.web.service.TokenService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 个人信息 业务处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/user/profile")
@Api(tags = "个人信息")
public class SysProfileController extends BaseController
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private OssService ossService;

    @Autowired
    private IBaCarService baCarService;

    @Autowired
    private ISysCompanyService sysCompanyService;


    /**
     * 个人信息
     */
    @GetMapping
    public AjaxResult profile()
    {
        LoginUser loginUser = getLoginUser();
        SysUser user = loginUser.getUser();
        List<SysDept> sysDeptList = new ArrayList<>();
        String sysDeptStr = "";
        List<String> sysDeptStrList = new ArrayList<>();
        String currentDept = "";
        String ancestors = "";
        String[] split = null;
        SysDept sysDept = null;
        if(!ObjectUtils.isEmpty(user)) {
            if(null != user.getDeptId()) {
                long deptId = user.getDeptId();
                sysDept = deptService.selectDeptById(deptId);
                sysDeptList.add(sysDept);
                if (!ObjectUtils.isEmpty(sysDept)) {
                    currentDept = sysDept.getDeptName();
                    ancestors = sysDept.getAncestors();
                    split = ancestors.split(",");
                    if (!ObjectUtils.isEmpty(split)) {
                        for (String s : split) {
                            if (!"0".equals(s)) {
                                sysDept = deptService.selectDeptById(Long.valueOf(s));
                                if (!ObjectUtils.isEmpty(sysDept)) {
                                    if (!"".equals(sysDeptStr)) {
                                        sysDeptStr = sysDeptStr + " - " + sysDept.getDeptName();
                                    } else {
                                        sysDeptStr = sysDept.getDeptName();
                                    }
                                }
                            }
                        }
                    }
                    if ("".equals(sysDeptStr)) {
                        sysDeptStr = currentDept;
                    } else {
                        sysDeptStr = sysDeptStr + " - " + currentDept;
                    }
                    sysDeptStrList.add(sysDeptStr);
                }
            }
            //拼接兼职部门
            if (user.getPartDeptId() != null) {
                String partDeptId = user.getPartDeptId();
                if(StringUtils.isNotEmpty(partDeptId)){
                    String[] splitPartDept = partDeptId.split(",");
                    for(String partDept : splitPartDept){
                        sysDeptStr = "";
                        if(StringUtils.isNotEmpty(partDept)){
                            sysDept = deptService.selectDeptById(Long.parseLong(partDept));
                            if (!ObjectUtils.isEmpty(sysDept)) {
                                currentDept = sysDept.getDeptName();
                                ancestors = sysDept.getAncestors();
                                split = ancestors.split(",");
                                if (!ObjectUtils.isEmpty(split)) {
                                    for (String s : split) {
                                        if (!"0".equals(s) && StringUtils.isNotEmpty(s)) {
                                            sysDept = deptService.selectDeptById(Long.valueOf(s));
                                            if (!ObjectUtils.isEmpty(sysDept)) {
                                                if (!"".equals(sysDeptStr)) {
                                                    sysDeptStr = sysDeptStr + " - " + sysDept.getDeptName();
                                                } else {
                                                    sysDeptStr = sysDept.getDeptName();
                                                }
                                            }
                                        }
                                    }
                                }
                                if ("".equals(sysDeptStr)) {
                                    sysDeptStr = currentDept;
                                } else {
                                    sysDeptStr = sysDeptStr + " - " + currentDept;
                                }
                            }
                        }
                        sysDeptStrList.add(sysDeptStr);
                    }
                }
            }
        }
        //拼接部门
        user.setSysDeptStr(sysDeptStrList);
        //车辆信息
        List<BaCars> cars = new ArrayList<>();
        BaCar baCar = new BaCar();
        baCar.setUserId(user.getUserId());
        List<BaCar> baCars = baCarService.selectBaCarList(baCar);
        for (BaCar car:baCars) {
            BaCars baCars1 = new BaCars();
            BeanUtils.copyBeanProp(baCars1, car);
            cars.add(baCars1);
        }
        user.setBaCars(cars);
        AjaxResult ajax = AjaxResult.success(user);
        ajax.put("roleGroup", userService.selectUserRoleGroup(loginUser.getUsername()));
        ajax.put("postGroup", userService.selectUserPostGroup(loginUser.getUsername()));
        return ajax;
    }

    /**
     * 修改用户
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult updateProfile(@RequestBody SysUser user)
    {
        LoginUser loginUser = getLoginUser();
        SysUser sysUser = loginUser.getUser();
        user.setUserName(sysUser.getUserName());
        if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            return error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            return error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUserId(sysUser.getUserId());
        user.setPassword(null);
        user.setAvatar(null);
        user.setDeptId(null);
        if (userService.updateUserProfile(user) > 0)
        {
           //删除所有车辆信息
            BaCar car = new BaCar();
            car.setUserId(user.getUserId());
            List<BaCar> baCars = baCarService.selectBaCarList(car);
            for (BaCar car1:baCars) {
                baCarService.deleteBaCarById(car1.getId());
            }
            //新增车辆信息
            if(user.getBaCars() != null){
                if(user.getBaCars().size() > 0){
                    for (BaCars baCar:user.getBaCars()) {
                        BaCar baCar1 = new BaCar();
                        baCar1.setUserId(baCar.getUserId());
                        baCar1.setName(baCar.getName());
                        baCar1.setInfo(baCar.getInfo());
                        baCarService.insertBaCar(baCar1);
                    }
                }
            }
            // 更新缓存用户信息
            sysUser.setNickName(user.getNickName());
            sysUser.setPhonenumber(user.getPhonenumber());
            sysUser.setEmail(user.getEmail());
            sysUser.setSex(user.getSex());
            sysUser.setBankName(user.getBankName());
            sysUser.setCollectionUser(user.getCollectionUser());
            sysUser.setAccount(user.getAccount());
            tokenService.setLoginUser(loginUser);
            //修改相关租户ID
            SysUser user1 = userService.selectUserById(user.getUserId());
            if(user1.getAdminFlag() != null){
                if(StringUtils.isNotEmpty(user1.getComId())){
                    //查看租户信息
                    SysCompany sysCompany = sysCompanyService.selectSysCompanyById(user1.getComId());
                    sysCompany.setPhone(user1.getPhonenumber());
                    sysCompany.setLegalPerson(user1.getNickName());
                    sysCompany.setEmail(user1.getEmail());
                    sysCompanyService.updateSysCompany(sysCompany);
                }
            }
            return success();
        }
        return error("修改个人信息异常，请联系管理员");
    }

    /**
     * 重置密码
     */
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping("/updatePwd")
    public AjaxResult updatePwd(String oldPassword, String newPassword)
    {
        LoginUser loginUser = getLoginUser();
        String userName = loginUser.getUsername();
        String password = loginUser.getPassword();
        if (!SecurityUtils.matchesPassword(oldPassword, password))
        {
            return error("修改密码失败，旧密码错误");
        }
        if (SecurityUtils.matchesPassword(newPassword, password))
        {
            return error("新密码不能与旧密码相同");
        }
        if (userService.resetUserPwd(userName, SecurityUtils.encryptPassword(newPassword)) > 0)
        {
            // 更新缓存用户密码
            loginUser.getUser().setPassword(SecurityUtils.encryptPassword(newPassword));
            tokenService.setLoginUser(loginUser);
            return success();
        }
        return error("修改密码异常，请联系管理员");
    }

    /**
     * 头像上传
     */
    @Log(title = "用户头像", businessType = BusinessType.UPDATE)
    @ApiOperation("修改头像")
    @PostMapping("/avatar")
    public AjaxResult avatar(@RequestPart("avatarfile") MultipartFile avatarfile, HttpServletRequest request) throws Exception
    {
        if (!avatarfile.isEmpty())
        {
            LoginUser loginUser = getLoginUser();
            //String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file, MimeTypeUtils.IMAGE_EXTENSION);
            FileUploadResult avatar = this.ossService.Certification(avatarfile, "avatar", request);
            if (userService.updateUserAvatar(loginUser.getUser().getUserName(), avatar.getName()))
            {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("imgUrl", avatar.getName());
                // 更新缓存用户头像
                loginUser.getUser().setAvatar(avatar.getName());
                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return error("上传图片异常，请联系管理员");
    }
}
