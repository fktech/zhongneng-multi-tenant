package com.business.web.controller.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.TreeSelect;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.redis.RedisCache;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 工作流部门和人员树
 *
 * @author js
 */
@RestController
@RequestMapping("/workflow/org")
@Api(tags = "工作流部门和人员树接口")
public class OrgUserAndDeptController extends BaseController
{
    @Autowired
    private IOrgUserAndDeptService orgService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private IBaProjectService baProjectService;

    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    @ApiOperation("查询组织架构树接口")
    @GetMapping("/tree")
    public AjaxResult getOrgTreeData(SysDeptDTO sysDeptDTO){
        if(ObjectUtils.isEmpty(sysDeptDTO.getParentId())){
            sysDeptDTO.setParentId(new Long(0));
        }
        return success(orgService.getOrgTreeData(sysDeptDTO));
    }

    /**
     * 查询角色列表
     * @param sysRole 角色
     * @return 角色列表数据
     */
    @ApiOperation("查询角色列表")
    @GetMapping("/role/tree")
    public AjaxResult getRoleTreeData(SysRole sysRole){
        return success(orgService.getRoleTreeData(sysRole));
    }

    /**
     * 模糊搜索用户
     * @param sysUserDTO 用户
     * @return 匹配到的用户
     */
    @ApiOperation("模糊搜索用户接口")
    @GetMapping("/user/search")
    public AjaxResult getOrgTreeUser(SysUserDTO sysUserDTO){
        return success(orgService.getOrgTreeUser(sysUserDTO));
    }
    @PostMapping("/start")
    public AjaxResult start(){
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey("Flowable1629048213817524224");
        startProcessInstanceDTO.setFormData(new JSONObject());
        UserInfo userInfo = new UserInfo();
        userInfo.setId("1");
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        AjaxResult start = workFlowFeignClient.start(startProcessInstanceDTO);
        return success(start);
    }


    /**
     * 根据部门ID及角色ID查询用户列表
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByRoleId")
    @ApiOperation("根据部门ID及角色ID查询用户列表")
    public AjaxResult selectUserByRoleId(@RequestBody SysUserRoleDTO sysUserRoleDTO)
    {
        return success(userService.selectUserByRoleId(sysUserRoleDTO));
    }

    /**
     * 根据角色ID查询用户列表
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByRole")
    public AjaxResult selectUserByRole(@RequestBody SysUserByRoleIdDTO sysUserByRoleIdDTO)
    {
        return success(userService.selectUserByRole(sysUserByRoleIdDTO));
    }

    /**
     * 获取当前登录人信息
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/getCurrentUserInfo")
    @Anonymous
    public AjaxResult getCurrentUserInfo()
    {
        return success(userService.getCurrentUserInfo());
    }

    @GetMapping("/getCurrentUser")
    @Anonymous
    public AjaxResult getCurrentUser()
    {
//        SysUser sysUser = (SysUser)redisCache.getCacheObject(Constants.LOGIN_USER);
        return success(userService.getCurrentUserInfo());
    }

    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    @ApiOperation("查询组织架构所有数据")
    @GetMapping("/treeAll")
    public AjaxResult getOrgTree(SysDeptDTO sysDeptDTO){
        if(ObjectUtils.isEmpty(sysDeptDTO.getParentId())){
            sysDeptDTO.setParentId(new Long(0));
        }
        return success(orgService.getOrgTree(sysDeptDTO));
    }
    /**
     * 查询客户信息
     * @param
     * @return 组织架构树数据
     */
    @ApiOperation("查询客户信息")
    @GetMapping("/addressBook")
    public AjaxResult getAddressBook(){

        return success(orgService.getAddressBook());
    }
    /**
     * APP通讯录集团查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    @ApiOperation("APP通讯录集团查询")
    @GetMapping("/appTreeAll")
    public AjaxResult getAppOrgTree(SysDeptDTO sysDeptDTO){
        if(ObjectUtils.isEmpty(sysDeptDTO.getParentId())){
            sysDeptDTO.setParentId(new Long(0));
        }
        return success(orgService.getAppOrgTree(sysDeptDTO));
    }
    /**
     * 根据角色ID查询自定义数据权限部pp门列表
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectDeptsByRoleId")
    public AjaxResult selectDeptsByRoleId(@RequestBody SysDeptRoleDTO sysDeptRoleDTO)
    {
        return success(userService.selectDeptsByRoleId(sysDeptRoleDTO));
    }

    /**
     * 根据部门查询用户
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByDepId")
    public AjaxResult selectUserByDepId(@RequestBody SysUserDepDTO sysUserDepDTO)
    {
        return success(userService.selectUserByDepId(sysUserDepDTO));
    }

    /**
     * 根据部门及角色查询用户
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByDepIdAndRole")
    public AjaxResult selectUserByDepIdAndRole(@RequestBody SysUserDepDTO sysUserDepDTO)
    {
        return success(userService.selectUserByDepIdAndRole(sysUserDepDTO));
    }

    /**
     * 根据部门及角色查询用户
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByDepIdAndRoleName")
    public AjaxResult selectUserByDepIdAndRoleName(@RequestBody SysUserDepDTO sysUserDepDTO)
    {
        return success(userService.selectUserByDepIdAndRoleName(sysUserDepDTO));
    }


    @GetMapping("/deptTree")
    @ApiOperation("部门树")
    @Anonymous
    public AjaxResult deptTree(SysDept dept)
    {
        //租户ID
        dept.setTenantId(SecurityUtils.getCurrComId());
        List<TreeSelect> treeSelects = deptService.deptTreeList(dept);
        return success(treeSelects);
    }

    @GetMapping("/deptId")
    @ApiOperation("部门查询")
    @Anonymous
    public AjaxResult deptId(Long deptId)
    {
        SysDept dept = deptService.selectDeptById(deptId);
        return AjaxResult.success(dept);
    }

    @GetMapping("/topDeptId")
    @ApiOperation("部门查询")
    @Anonymous
    public AjaxResult topDeptId()
    {
        SysDept dept = deptService.selectTopDeptBytenantId();
        return AjaxResult.success(dept);
    }

    /**
     * 根据运营查询部门经理列表
     */
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUserByRoleIdAndDepRole")
    public AjaxResult selectUserByRoleIdAndDepRole(@RequestBody SysUserByDepRoleDTO sysUserByDepRoleDTO)
    {
        return success(userService.selectUserByRoleIdAndDepRole(sysUserByDepRoleDTO));
    }

    /**
     * 根据用户ID查询角色列表
     */
    @PostMapping("/selectRolesByUser")
    public AjaxResult selectRolesByUser(@RequestBody SysRolesByUserDTO sysRolesByUserDTO)
    {
        return success(userService.selectRolesByUser(sysRolesByUserDTO));
    }

    /**
     * 根据用户ID查询运营角色列表
     */
    @PostMapping("/selectOperationRolesByUser")
    public AjaxResult selectOperationRolesByUser(@RequestBody SysRolesByUserDTO sysRolesByUserDTO)
    {
        return success(userService.selectOperationRolesByUser(sysRolesByUserDTO));
    }

    /**
     * 根据角色ID查询用户ID列表
     */
    @PostMapping("/selectUserIdsByRole")
    public AjaxResult selectUserIdsByRole(@RequestBody SysUserByRoleIdDTO sysUserByRoleIdDTO)
    {
        return success(userService.selectUserIdsByRole(sysUserByRoleIdDTO));
    }

    /**
     * 根据角色ID查询角色
     */
    @PostMapping("/selectRoleByRoleId")
    public AjaxResult selectRoleByRoleId(@RequestBody SysRolesByRoleIdDTO sysRolesByRoleIdDTO)
    {
        return success(sysRoleService.selectRoleByRoleId(sysRolesByRoleIdDTO));
    }


    /**
     * 根据userId获取用户信息
     */
    @ApiOperation("根据Id获取用户信息")
    @GetMapping(value = { "/selectUserByUserId/{userId}" })
    public AjaxResult selectUserByUserId(@PathVariable(value = "userId") Long userId)
    {
        return success(userService.selectUserByUserId(Long.valueOf(userId)));
    }

    /**
     * 根据userId获取对应归属公司信息
     */
    @ApiOperation("根据userId获取对应归属公司信息")
    @GetMapping(value = { "/selectUserDeptByUserId/{userId}" })
    public AjaxResult selectUserDeptByUserId(@PathVariable(value = "userId") Long userId)
    {
        return success(userService.selectUserDeptByUserId(Long.valueOf(userId)));
    }

    /**
     * 查询该组顶级部门
     */
    @GetMapping("/tenantIdDeptName")
    public AjaxResult tenantIdDeptName(String tenantId)
    {
        return success(deptService.tenantIdDeptName(tenantId));
    }

    /**
     * 根据付款流程实例ID查询对应项目的业务经理用户ID
     */
    @PostMapping("/getUserIdByProcessInstanceId")
    @ApiOperation(value = "根据付款流程实例ID查询对应项目的业务经理用户ID")
    public AjaxResult getUserIdByProcessInstanceId(@RequestBody BusinessUserDTO businessUserDTO)
    {
        return success(baProjectService.selectBaProjectByBusinessId(businessUserDTO));
    }
}
