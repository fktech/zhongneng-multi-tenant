package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaMarketInformation;
import com.business.system.service.IBaMarketInformationService;

/**
 * 市场信息管理Controller
 *
 * @author ljb
 * @date 2023-03-22
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/information")
@Api(tags ="市场信息管理接口")
public class BaMarketInformationController extends BaseController
{
    @Autowired
    private IBaMarketInformationService baMarketInformationService;

    /**
     * 查询市场信息管理列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:information:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询市场信息管理列表接口")
    public TableDataInfo list(BaMarketInformation baMarketInformation)
    {
        baMarketInformation.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaMarketInformation> list = baMarketInformationService.selectBaMarketInformationList(baMarketInformation);
        return getDataTable(list);
    }

    /**
     * 导出市场信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('admin:information:export')")
    @Log(title = "市场信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出市场信息管理列表接口")
    public void export(HttpServletResponse response, BaMarketInformation baMarketInformation) throws IOException
    {
        List<BaMarketInformation> list = baMarketInformationService.selectBaMarketInformationList(baMarketInformation);
        ExcelUtil<BaMarketInformation> util = new ExcelUtil<BaMarketInformation>(BaMarketInformation.class);
        util.exportExcel(response, list, "information");
    }

    /**
     * 获取市场信息管理详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:information:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取市场信息管理详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baMarketInformationService.selectBaMarketInformationById(id));
    }

    /**
     * 新增市场信息管理
     */
    @PreAuthorize("@ss.hasPermi('admin:information:add')")
    @Log(title = "市场信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增市场信息管理接口")
    public AjaxResult add(@RequestBody BaMarketInformation baMarketInformation)
    {
        return toAjax(baMarketInformationService.insertBaMarketInformation(baMarketInformation));
    }

    /**
     * 修改市场信息管理
     */
    @PreAuthorize("@ss.hasPermi('admin:information:edit')")
    @Log(title = "市场信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改市场信息管理接口")
    public AjaxResult edit(@RequestBody BaMarketInformation baMarketInformation)
    {
        return toAjax(baMarketInformationService.updateBaMarketInformation(baMarketInformation));
    }

    /**
     * 删除市场信息管理
     */
    @PreAuthorize("@ss.hasPermi('admin:information:remove')")
    @Log(title = "市场信息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除市场信息管理接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baMarketInformationService.deleteBaMarketInformationByIds(ids));
    }
}
