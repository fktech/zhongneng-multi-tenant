package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaOnlineMent;
import com.business.system.service.IBaOnlineMentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 监控点在线状态Controller
 *
 * @author single
 * @date 2023-07-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/onlineMen")
@Api(tags ="监控点在线状态接口")
public class BaOnlineMentController extends BaseController
{
    @Autowired
    private IBaOnlineMentService baOnlineMentService;

    /**
     * 查询监控点在线状态列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:onlineMen:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询监控点在线状态列表接口")
    public TableDataInfo list(BaOnlineMent baOnlineMent)
    {
        startPage();
        List<BaOnlineMent> list = baOnlineMentService.selectBaOnlineMentList(baOnlineMent);
        return getDataTable(list);
    }

    /**
     * 导出监控点在线状态列表
     */
    @PreAuthorize("@ss.hasPermi('admin:onlineMen:export')")
    @Log(title = "监控点在线状态", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出监控点在线状态列表接口")
    public void export(HttpServletResponse response, BaOnlineMent baOnlineMent) throws IOException
    {
        List<BaOnlineMent> list = baOnlineMentService.selectBaOnlineMentList(baOnlineMent);
        ExcelUtil<BaOnlineMent> util = new ExcelUtil<BaOnlineMent>(BaOnlineMent.class);
        util.exportExcel(response, list, "onlineMen");
    }

    /**
     * 获取监控点在线状态详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:onlineMen:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取监控点在线状态详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baOnlineMentService.selectBaOnlineMentById(id));
    }

    /**
     * 新增监控点在线状态
     */
    @PreAuthorize("@ss.hasPermi('admin:onlineMen:add')")
    @Log(title = "监控点在线状态", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增监控点在线状态接口")
    public AjaxResult add(@RequestBody BaOnlineMent baOnlineMent)
    {
        return toAjax(baOnlineMentService.insertBaOnlineMent(baOnlineMent));
    }

    /**
     * 修改监控点在线状态
     */
    @PreAuthorize("@ss.hasPermi('admin:onlineMen:edit')")
    @Log(title = "监控点在线状态", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改监控点在线状态接口")
    public AjaxResult edit(@RequestBody BaOnlineMent baOnlineMent)
    {
        return toAjax(baOnlineMentService.updateBaOnlineMent(baOnlineMent));
    }

    /**
     * 删除监控点在线状态
     */
    @PreAuthorize("@ss.hasPermi('admin:onlineMen:remove')")
    @Log(title = "监控点在线状态", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除监控点在线状态接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baOnlineMentService.deleteBaOnlineMentByIds(ids));
    }

    /**
     *
     */
}
