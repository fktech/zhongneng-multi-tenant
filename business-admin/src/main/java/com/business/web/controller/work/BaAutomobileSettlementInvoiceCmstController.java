package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.annotation.Log;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaAutomobileSettlementInvoiceCmst;
import com.business.system.service.IBaAutomobileSettlementInvoiceCmstService;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.core.page.TableDataInfo;

/**
 * 中储智运无车承运开票Controller
 *
 * @author single
 * @date 2023-08-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/invoiceCmst")
@Api(tags ="中储智运无车承运开票接口")
public class BaAutomobileSettlementInvoiceCmstController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementInvoiceCmstService baAutomobileSettlementInvoiceCmstService;

    /**
     * 查询中储智运无车承运开票列表
     */
   /* @PreAuthorize("@ss.hasPermi('admin:cmst:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询中储智运无车承运开票列表接口")
    public TableDataInfo list(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        startPage();
        List<BaAutomobileSettlementInvoiceCmst> list = baAutomobileSettlementInvoiceCmstService.selectBaAutomobileSettlementInvoiceCmstList(baAutomobileSettlementInvoiceCmst);
        return getDataTable(list);
    }

    /**
     * 导出中储智运无车承运开票列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:export')")
    @Log(title = "中储智运无车承运开票", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出中储智运无车承运开票列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst) throws IOException
    {
        List<BaAutomobileSettlementInvoiceCmst> list = baAutomobileSettlementInvoiceCmstService.selectBaAutomobileSettlementInvoiceCmstList(baAutomobileSettlementInvoiceCmst);
        ExcelUtil<BaAutomobileSettlementInvoiceCmst> util = new ExcelUtil<BaAutomobileSettlementInvoiceCmst>(BaAutomobileSettlementInvoiceCmst.class);
        util.exportExcel(response, list, "cmst");
    }

    /**
     * 获取中储智运无车承运开票详细信息
     */
   /* @PreAuthorize("@ss.hasPermi('admin:cmst:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取中储智运无车承运开票详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementInvoiceCmstService.selectBaAutomobileSettlementInvoiceCmstById(id));
    }

    /**
     * 新增中储智运无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:add')")
    @Log(title = "中储智运无车承运开票", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增中储智运无车承运开票接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        return toAjax(baAutomobileSettlementInvoiceCmstService.insertBaAutomobileSettlementInvoiceCmst(baAutomobileSettlementInvoiceCmst));
    }

    /**
     * 修改中储智运无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:edit')")
    @Log(title = "中储智运无车承运开票", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改中储智运无车承运开票接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        return toAjax(baAutomobileSettlementInvoiceCmstService.updateBaAutomobileSettlementInvoiceCmst(baAutomobileSettlementInvoiceCmst));
    }

    /**
     * 删除中储智运无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:cmst:remove')")
    @Log(title = "中储智运无车承运开票", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除中储智运无车承运开票接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementInvoiceCmstService.deleteBaAutomobileSettlementInvoiceCmstByIds(ids));
    }
}
