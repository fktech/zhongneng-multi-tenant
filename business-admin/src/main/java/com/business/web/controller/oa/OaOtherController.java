package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.OaEntertainInstanceRelatedEditDTO;
import com.business.system.domain.dto.OaOtherInstanceRelatedEditDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaOther;
import com.business.system.service.IOaOtherService;

/**
 * 其他申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaOther")
@Api(tags ="其他申请接口")
public class OaOtherController extends BaseController
{
    @Autowired
    private IOaOtherService oaOtherService;

    /**
     * 查询其他申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOther:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询其他申请列表接口")
    public TableDataInfo list(OaOther oaOther)
    {
        //租户ID
        oaOther.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaOther> list = oaOtherService.selectOaOtherList(oaOther);
        return getDataTable(list);
    }

    /**
     * 导出其他申请列表
     */
    @PreAuthorize("@ss.hasPermi('admin:oaOther:export')")
    //@Log(title = "其他申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出其他申请列表接口")
    public void export(HttpServletResponse response, OaOther oaOther) throws IOException
    {
        //租户ID
        oaOther.setTenantId(SecurityUtils.getCurrComId());
        List<OaOther> list = oaOtherService.selectOaOtherList(oaOther);
        ExcelUtil<OaOther> util = new ExcelUtil<OaOther>(OaOther.class);
        util.exportExcel(response, list, "oaOther");
    }

    /**
     * 获取其他申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOther:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取其他申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaOtherService.selectOaOtherById(id));
    }

    /**
     * 新增其他申请
     */
   /* @PreAuthorize("@ss.hasPermi('admin:oaOther:add')")
    @Log(title = "其他申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增其他申请接口")
    public AjaxResult add(@RequestBody OaOther oaOther)
    {
        int result = oaOtherService.insertOaOther(oaOther);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改其他申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaOther:edit')")
    @Log(title = "其他申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改其他申请接口")
    public AjaxResult edit(@RequestBody OaOther oaOther)
    {
        int result = oaOtherService.updateOaOther(oaOther);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除其他申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaOther:remove')")
    @Log(title = "其他申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除其他申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaOtherService.deleteOaOtherByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOther:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaOtherService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody OaOtherInstanceRelatedEditDTO oaOtherInstanceRelatedEditDTO) {
        int result = oaOtherService.updateProcessBusinessData(oaOtherInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }
        return toAjax(result);
    }
}
