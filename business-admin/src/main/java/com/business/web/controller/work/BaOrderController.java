package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaOrder;
import com.business.system.service.IBaOrderService;

/**
 * 订单Controller
 *
 * @author ljb
 * @date 2022-12-09
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/order")
@Api(tags ="订单接口")
public class BaOrderController extends BaseController
{
    @Autowired
    private IBaOrderService baOrderService;

    /**
     * 查询订单列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:order:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询订单列表接口")
    public TableDataInfo list(BaOrder baOrder)
    {
        startPage();
        List<BaOrder> list = baOrderService.selectBaOrderList(baOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:order:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出订单列表接口")
    public void export(HttpServletResponse response, BaOrder baOrder) throws IOException
    {
        List<BaOrder> list = baOrderService.selectBaOrderList(baOrder);
        ExcelUtil<BaOrder> util = new ExcelUtil<BaOrder>(BaOrder.class);
        util.exportExcel(response, list, "order");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:order:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取订单详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baOrderService.selectBaOrderById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增订单接口")
    public AjaxResult add(@RequestBody BaOrder baOrder)
    {
        Integer result = baOrderService.insertBaOrder(baOrder);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改订单接口")
    public AjaxResult edit(@RequestBody BaOrder baOrder)
    {
       Integer result = baOrderService.updateBaOrder(baOrder);
       if(result == 0){
           return AjaxResult.error("流程启动失败");
       }
        return toAjax(result);
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('admin:order:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除订单接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baOrderService.deleteBaOrderByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:order:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baOrderService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 完成按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:order:complete')")
    @GetMapping(value = "/complete/{id}")
    @ApiOperation(value = "完成按钮")
    public AjaxResult complete(@PathVariable("id") String id){
        Integer result = baOrderService.complete(id);
        if(result == 0){
            return AjaxResult.error(0,"ID不能为空");
        }
        return toAjax(result);
    }

    /**
     * 根据类型统计订单数量(订单进度报表总量统计)
     */
    @PreAuthorize("@ss.hasPermi('admin:order:query')")
    @GetMapping(value = "/statOrderInfo")
    @ApiOperation(value = "根据类型统计订单数量(订单进度报表总量统计)")
    public AjaxResult statOrderInfo(BaOrder baOrder)
    {
        return AjaxResult.success(baOrderService.statOrderInfo(baOrder));
    }

    /**
     * 订单进度表(采购/销售)
     */
    @PreAuthorize("@ss.hasPermi('admin:order:query')")
    @GetMapping(value = "/statPurchaseProgress")
    @ApiOperation(value = "订单进度表(采购/销售)")
    public TableDataInfo statPurchaseProgress(BaOrder baOrder)
    {
        startPage();
        List<BaOrder> list = baOrderService.statPurchaseProgress(baOrder);
        return getDataTable(list);
    }
}
