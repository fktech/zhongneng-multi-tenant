package com.business.web.controller.work;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaBillingInformation;
import com.business.system.domain.BaCollection;
import com.business.system.domain.BaPayment;
import com.business.system.domain.BaSettlement;
import com.business.system.domain.dto.BaCollectionVoucherDTO;
import com.business.system.domain.dto.BaPaymentVoucherDTO;
import com.business.system.service.IBaCollectionService;
import com.business.system.service.IBaPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 收款信息Controller
 *
 * @author js
 * @date 2022-12-26
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/collection")
@Api(tags ="收款信息接口")
public class BaCollectionController extends BaseController
{
    @Autowired
    private IBaCollectionService baCollectionService;

    /**
     * 查询收款信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:collection:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询收款信息列表接口")
    public TableDataInfo list(BaCollection baCollection)
    {
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCollectionVoucherDTO> list=baCollectionService.claimCollection(baCollection);
        return getDataTable(list);
    }

    /*@GetMapping("/claimListInWx")
    @ApiOperation(value = "微信认领收款信息列表接口")
    public TableDataInfo claimListInWx(BaCollection baCollection)
    {
        startPage();
        List<BaCollectionVoucherDTO> list=baCollectionService.claimCollectionInWx(baCollection);
        return getDataTable(list);
    }*/
    @GetMapping("/claimList")
    @ApiOperation(value = "认领收款信息列表接口")
    public TableDataInfo claimList(BaCollection baCollection)
    {
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCollectionVoucherDTO> list = baCollectionService.claimCollection(baCollection);
        return getDataTable(list);
    }

    @GetMapping("/statistics")
    @ApiOperation(value = "页面统计")
    public UR statistics(BaCollection baCollection){
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        UR ur = baCollectionService.statistics(baCollection);
        return ur;
    }

    /**
     * 导出收款信息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:export')")
    @Log(title = "收款信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出收款信息列表接口")
    public void export(HttpServletResponse response, BaCollection baCollection) throws IOException
    {
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        List<BaCollectionVoucherDTO> list = baCollectionService.claimCollection(baCollection);
        ExcelUtil<BaCollectionVoucherDTO> util = new ExcelUtil<BaCollectionVoucherDTO>(BaCollectionVoucherDTO.class);
        util.exportExcel(response, list, "payment");
    }

    /**
     * 获取收款信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:collection:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取收款信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCollectionService.selectBaCollectionById(id));
    }

    /**
     * 新增收款信息
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:add')")
    @Log(title = "收款信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增收款信息接口")
    public AjaxResult add(@RequestBody BaCollection baCollection)
    {
        return toAjax(baCollectionService.insertBaCollection(baCollection));
    }

    /**
     * 新增收款凭证
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:addBaCollectionVoucher')")
    @Log(title = "收款凭证", businessType = BusinessType.INSERT)
    @PostMapping(value = "/addBaCollectionVoucher")
    @ApiOperation(value = "新增收款凭证接口")
    public AjaxResult addBaCollectionVoucher(@RequestBody BaCollectionVoucherDTO baCollectionVoucherDTO)
    {
        return toAjax(baCollectionService.insertBaCollectionVoucher(baCollectionVoucherDTO));
    }

    /**
     * 新增新增认领收款
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:insertCollection')")
    @Log(title = "新增认领收款", businessType = BusinessType.INSERT)
    @PostMapping(value = "/insertCollection")
    @ApiOperation(value = "新增认领收款")
    public AjaxResult insertCollection(@RequestBody BaCollection baCollection)
    {
        return toAjax(baCollectionService.insertCollection(baCollection));
    }

    /**
     * 修改收款信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:collection:edit')")
    @Log(title = "收款信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改收款信息接口")
    public AjaxResult edit(@RequestBody BaCollection baCollection)
    {
        Integer result = baCollectionService.updateBaCollection(baCollection);
        if(result == 0){
            return AjaxResult.error("流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除收款信息
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:remove')")
    @Log(title = "收款信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除收款信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCollectionService.deleteBaCollectionByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baCollectionService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 认领收款发起流程
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:approvalButton')")
    @GetMapping(value = "/approvalButton")
    @ApiOperation(value = "认领收款流程发起")
    public AjaxResult approvalButton(String id,String workState)
    {
        Integer result = baCollectionService.approvalButton(id,workState);
        if(result == 0){
            return AjaxResult.error("流程发起失败");
        }
        return toAjax(result);
    }

    /**
     * 认领收款撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:approvalRevoke')")
    @GetMapping(value = "/approvalRevoke/{id}")
    @ApiOperation(value = "认领收款撤销按钮")
    public AjaxResult approvalRevoke(@PathVariable("id") String id)
    {
        Integer result = baCollectionService.approvalRevoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 查询单位名称
     */
    @GetMapping("/searchName")
    @ApiOperation(value = "查询单位名称")
    @Anonymous
    public UR searchName(String name){
        UR ur = baCollectionService.searchName(name);
        return ur;
    }

    /**
     * 查询对应的开票信息
     */
    @GetMapping("/invoicingInformation")
    @ApiOperation(value = "开票信息")
    public TableDataInfo list(BaBillingInformation baBillingInformation)
    {

        //startPage();
        //List<BaCollectionVoucherDTO> list=baCollectionService.claimCollection(baCollection);
        List<BaBillingInformation> baBillingInformations = baCollectionService.invoicingInformation(baBillingInformation);
        return getDataTable(baBillingInformations);
    }

    /**
     * 退回
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:goBack')")
    @GetMapping("/goBack/{id}")
    @ApiOperation(value = "退回")
    public AjaxResult goBack(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCollectionService.goBack(id));
    }

    /**
     * 关联
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:association')")
    @PostMapping("/association")
    @ApiOperation(value = "关联")
    public AjaxResult association(@RequestBody BaCollection baCollection) {
        return toAjax(baCollectionService.association(baCollection));
    }

    /**
     * 查询多租户授权信息收款信息列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:collection:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息收款信息列表")
    public TableDataInfo authorizedList(BaCollection baCollection)
    {
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCollectionVoucherDTO> list=baCollectionService.claimCollectionAuthorized(baCollection);
        return getDataTable(list);
    }

    /**
     * 授权收款管理
     */
    @PreAuthorize("@ss.hasPermi('admin:collection:authorized')")
    @Log(title = "收款管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权收款管理")
    public AjaxResult authorized(@RequestBody BaCollection baCollection)
    {
        return AjaxResult.success(baCollectionService.authorizedBaCollection(baCollection));
    }
}
