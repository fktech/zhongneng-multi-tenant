package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaGoodsType;
import com.business.system.service.IBaGoodsTypeService;
/**
 * 商品分类Controller
 *
 * @author ljb
 * @date 2022-11-30
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/goosType")
@Api(tags ="商品分类接口")
public class BaGoodsTypeController extends BaseController
{
    @Autowired
    private IBaGoodsTypeService baGoodsTypeService;

    /**
     * 查询商品分类列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:goosType:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询商品分类列表接口")
    public TableDataInfo list(BaGoodsType baGoodsType)
    {
        //租户ID
        baGoodsType.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaGoodsType> list = baGoodsTypeService.selectBaGoodsTypeList(baGoodsType);
        return getDataTable(list);
    }

    /**
     * 查询下拉框中的type数据
     */
    //@PreAuthorize("@ss.hasPermi('system:materialType:list')")
    @GetMapping("/searchList")
    @ApiOperation(value = "查询下拉框中的type数据")
    public AjaxResult searchList(){
        return AjaxResult.success(baGoodsTypeService.selectBaGoodsTypeTypeList());
    }

    /**
     * 查询下拉框中的type数据及商品数据
     */
    //@PreAuthorize("@ss.hasPermi('system:materialType:list')")
    @GetMapping("/searchGoodsList")
    @ApiOperation(value = "查询下拉框中的type数据及商品数据")
    public AjaxResult searchGoodsList(){
        return AjaxResult.success(baGoodsTypeService.selectTypeAndGoodsList());
    }

    /**
     * 导出商品分类列表
     */
    @PreAuthorize("@ss.hasPermi('admin:goosType:export')")
    @Log(title = "商品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出商品分类列表接口")
    public void export(HttpServletResponse response, BaGoodsType baGoodsType) throws IOException
    {
        List<BaGoodsType> list = baGoodsTypeService.selectBaGoodsTypeList(baGoodsType);
        ExcelUtil<BaGoodsType> util = new ExcelUtil<BaGoodsType>(BaGoodsType.class);
        util.exportExcel(response, list, "goosType");
    }

    /**
     * 获取商品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:goosType:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取商品分类详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baGoodsTypeService.selectBaGoodsTypeById(id));
    }

    /**
     * 新增商品分类
     */
    @PreAuthorize("@ss.hasPermi('admin:goosType:add')")
    @Log(title = "商品分类", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增商品分类接口")
    public AjaxResult add(@RequestBody BaGoodsType baGoodsType)
    {
       Integer result =  baGoodsTypeService.insertBaGoodsType(baGoodsType);
       if(result == -1){
           return AjaxResult.error("商品分类不能重复");
       }
        return toAjax(result);
    }

    /**
     * 修改商品分类
     */
    @PreAuthorize("@ss.hasPermi('admin:goosType:edit')")
    @Log(title = "商品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改商品分类接口")
    public AjaxResult edit(@RequestBody BaGoodsType baGoodsType)
    {
        return toAjax(baGoodsTypeService.updateBaGoodsType(baGoodsType));
    }

    /**
     * 删除商品分类
     */
    @PreAuthorize("@ss.hasPermi('admin:goosType:remove')")
    @Log(title = "商品分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除商品分类接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        int result = baGoodsTypeService.deleteBaGoodsTypeByIds(ids);
        if(result == -1){
            return AjaxResult.error(-1,"该分类下已存在商品");
        }
        return toAjax(baGoodsTypeService.deleteBaGoodsTypeByIds(ids));
    }
}
