package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaEventRecording;
import com.business.system.service.IBaEventRecordingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 事件记录Controller
 *
 * @author single
 * @date 2023-07-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/recording")
@Api(tags ="事件记录接口")
public class BaEventRecordingController extends BaseController
{
    @Autowired
    private IBaEventRecordingService baEventRecordingService;

    /**
     * 查询事件记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询事件记录列表接口")
    public TableDataInfo list(BaEventRecording baEventRecording)
    {
        startPage();
        List<BaEventRecording> list = baEventRecordingService.selectBaEventRecordingList(baEventRecording);
        return getDataTable(list);
    }

    /**
     * 导出事件记录列表
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:export')")
    @Log(title = "事件记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出事件记录列表接口")
    public void export(HttpServletResponse response, BaEventRecording baEventRecording) throws IOException
    {
        List<BaEventRecording> list = baEventRecordingService.selectBaEventRecordingList(baEventRecording);
        ExcelUtil<BaEventRecording> util = new ExcelUtil<BaEventRecording>(BaEventRecording.class);
        util.exportExcel(response, list, "recording");
    }

    /**
     * 获取事件记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取事件记录详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baEventRecordingService.selectBaEventRecordingById(id));
    }

    /**
     * 新增事件记录
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:add')")
    @Log(title = "事件记录", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增事件记录接口")
    public AjaxResult add(@RequestBody BaEventRecording baEventRecording)
    {
        return toAjax(baEventRecordingService.insertBaEventRecording(baEventRecording));
    }

    /**
     * 修改事件记录
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:edit')")
    @Log(title = "事件记录", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改事件记录接口")
    public AjaxResult edit(@RequestBody BaEventRecording baEventRecording)
    {
        return toAjax(baEventRecordingService.updateBaEventRecording(baEventRecording));
    }

    /**
     * 删除事件记录
     */
    @PreAuthorize("@ss.hasPermi('admin:recording:remove')")
    @Log(title = "事件记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除事件记录接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baEventRecordingService.deleteBaEventRecordingByIds(ids));
    }
}
