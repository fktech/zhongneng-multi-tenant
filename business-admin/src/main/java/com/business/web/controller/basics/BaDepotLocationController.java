package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDepotLocation;
import com.business.system.service.IBaDepotLocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 仓库库位信息Controller
 *
 * @author single
 * @date 2023-09-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/location")
@Api(tags ="仓库库位信息接口")
public class BaDepotLocationController extends BaseController
{
    @Autowired
    private IBaDepotLocationService baDepotLocationService;

    /**
     * 查询仓库库位信息列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:location:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询仓库库位信息列表接口")
    public TableDataInfo list(BaDepotLocation baDepotLocation)
    {
        startPage();
        List<BaDepotLocation> list = baDepotLocationService.selectBaDepotLocationList(baDepotLocation);
        return getDataTable(list);
    }

    /**
     * 导出仓库库位信息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:location:export')")
    @Log(title = "仓库库位信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出仓库库位信息列表接口")
    public void export(HttpServletResponse response, BaDepotLocation baDepotLocation) throws IOException
    {
        List<BaDepotLocation> list = baDepotLocationService.selectBaDepotLocationList(baDepotLocation);
        ExcelUtil<BaDepotLocation> util = new ExcelUtil<BaDepotLocation>(BaDepotLocation.class);
        util.exportExcel(response, list, "location");
    }

    /**
     * 获取仓库库位信息详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:location:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取仓库库位信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDepotLocationService.selectBaDepotLocationById(id));
    }

    /**
     * 新增仓库库位信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:location:add')")
    @Log(title = "仓库库位信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增仓库库位信息接口")
    public AjaxResult add(@RequestBody BaDepotLocation baDepotLocation)
    {
        int result = baDepotLocationService.insertBaDepotLocation(baDepotLocation);
        if(result == -2){
            return AjaxResult.error(-2, "库位编号已存在");
        }else if(result == -3){
            return AjaxResult.error(-3, "库位名称已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改仓库库位信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:location:edit')")
    @Log(title = "仓库库位信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改仓库库位信息接口")
    public AjaxResult edit(@RequestBody BaDepotLocation baDepotLocation)
    {
        int result = baDepotLocationService.updateBaDepotLocation(baDepotLocation);
        if(result == -2){
            return AjaxResult.error(-2, "库位编号已存在");
        }else if(result == -3){
            return AjaxResult.error(-3, "库位名称已存在");
        }
        return toAjax(result);
    }

    /**
     * 删除仓库库位信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:location:remove')")
    @Log(title = "仓库库位信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除仓库库位信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDepotLocationService.deleteBaDepotLocationByIds(ids));
    }
}
