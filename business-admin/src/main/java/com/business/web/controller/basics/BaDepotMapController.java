package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDepotMap;
import com.business.system.service.IBaDepotMapService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 仓库库位图信息Controller
 *
 * @author single
 * @date 2023-09-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/map")
@Api(tags ="仓库库位图信息接口")
public class BaDepotMapController extends BaseController
{
    @Autowired
    private IBaDepotMapService baDepotMapService;

    /**
     * 查询仓库库位图信息列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询仓库库位图信息列表接口")
    public TableDataInfo list(BaDepotMap baDepotMap)
    {
        startPage();
        List<BaDepotMap> list = baDepotMapService.selectBaDepotMapList(baDepotMap);
        return getDataTable(list);
    }

    /**
     * 导出仓库库位图信息列表
     */
    @PreAuthorize("@ss.hasPermi('admin:map:export')")
    @Log(title = "仓库库位图信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出仓库库位图信息列表接口")
    public void export(HttpServletResponse response, BaDepotMap baDepotMap) throws IOException
    {
        List<BaDepotMap> list = baDepotMapService.selectBaDepotMapList(baDepotMap);
        ExcelUtil<BaDepotMap> util = new ExcelUtil<BaDepotMap>(BaDepotMap.class);
        util.exportExcel(response, list, "map");
    }

    /**
     * 获取仓库库位图信息详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取仓库库位图信息详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDepotMapService.selectBaDepotMapById(id));
    }

    /**
     * 新增仓库库位图信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:add')")
    @Log(title = "仓库库位图信息", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增仓库库位图信息接口")
    public AjaxResult add(@RequestBody List<BaDepotMap> baDepotMaps)
    {
        return toAjax(baDepotMapService.insertBaDepotMap(baDepotMaps));
    }

    /**
     * 修改仓库库位图信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:edit')")
    @Log(title = "仓库库位图信息", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改仓库库位图信息接口")
    public AjaxResult edit(@RequestBody BaDepotMap baDepotMap)
    {
        return toAjax(baDepotMapService.updateBaDepotMap(baDepotMap));
    }

    /**
     * 删除仓库库位图信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:remove')")
    @Log(title = "仓库库位图信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除仓库库位图信息接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDepotMapService.deleteBaDepotMapByIds(ids));
    }

    /**
     * 批量修改仓库库位图信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:map:edit')")
    @Log(title = "仓库库位图信息", businessType = BusinessType.UPDATE)
    @PutMapping("/batchEdit")
    @ApiOperation(value = "修改仓库库位图信息接口")
    public AjaxResult editBaDepotMapList(@RequestBody List<BaDepotMap> baDepotMaps)
    {
        return toAjax(baDepotMapService.batchUpdateBaDepotMap(baDepotMaps));
    }
}
