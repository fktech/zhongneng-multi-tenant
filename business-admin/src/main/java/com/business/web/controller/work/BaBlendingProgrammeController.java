package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaBlendingProgramme;
import com.business.system.service.IBaBlendingProgrammeService;

/**
 * 配煤方案Controller
 *
 * @author ljb
 * @date 2024-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/programme")
@Api(tags ="配煤方案接口")
public class BaBlendingProgrammeController extends BaseController
{
    @Autowired
    private IBaBlendingProgrammeService baBlendingProgrammeService;

    /**
     * 查询配煤方案列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:programme:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询配煤方案列表接口")
    public TableDataInfo list(BaBlendingProgramme baBlendingProgramme)
    {
        //租户ID
        baBlendingProgramme.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaBlendingProgramme> list = baBlendingProgrammeService.selectBaBlendingProgrammeList(baBlendingProgramme);
        return getDataTable(list);
    }

    /**
     * 导出配煤方案列表
     */
    @PreAuthorize("@ss.hasPermi('admin:programme:export')")
    @Log(title = "配煤方案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出配煤方案列表接口")
    public void export(HttpServletResponse response, BaBlendingProgramme baBlendingProgramme) throws IOException
    {
        //租户ID
        baBlendingProgramme.setTenantId(SecurityUtils.getCurrComId());
        List<BaBlendingProgramme> list = baBlendingProgrammeService.selectBaBlendingProgrammeList(baBlendingProgramme);
        ExcelUtil<BaBlendingProgramme> util = new ExcelUtil<BaBlendingProgramme>(BaBlendingProgramme.class);
        util.exportExcel(response, list, "programme");
    }

    /**
     * 获取配煤方案详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:programme:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取配煤方案详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baBlendingProgrammeService.selectBaBlendingProgrammeById(id));
    }

    /**
     * 新增配煤方案
     */
    @PreAuthorize("@ss.hasPermi('admin:programme:add')")
    @Log(title = "配煤方案", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增配煤方案接口")
    public AjaxResult add(@RequestBody BaBlendingProgramme baBlendingProgramme)
    {
        int result = baBlendingProgrammeService.insertBaBlendingProgramme(baBlendingProgramme);
        if(result == 0){
            return AjaxResult.error(0,"配煤方案名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 修改配煤方案
     */
    @PreAuthorize("@ss.hasPermi('admin:programme:edit')")
    @Log(title = "配煤方案", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改配煤方案接口")
    public AjaxResult edit(@RequestBody BaBlendingProgramme baBlendingProgramme)
    {
        return toAjax(baBlendingProgrammeService.updateBaBlendingProgramme(baBlendingProgramme));
    }

    /**
     * 删除配煤方案
     */
    @PreAuthorize("@ss.hasPermi('admin:programme:remove')")
    @Log(title = "配煤方案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除配煤方案接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baBlendingProgrammeService.deleteBaBlendingProgrammeByIds(ids));
    }
}
