package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaFormwork;
import com.business.system.service.IBaFormworkService;

/**
 * 模板Controller
 *
 * @author ljb
 * @date 2023-03-14
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baFormwork")
@Api(tags ="模板接口")
public class BaFormworkController extends BaseController
{
    @Autowired
    private IBaFormworkService baFormworkService;

    /**
     * 查询模板列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:baFormwork:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询模板列表接口")
    public TableDataInfo list(BaFormwork baFormwork)
    {
        //租户ID
        baFormwork.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaFormwork> list = baFormworkService.selectBaFormworkList(baFormwork);
        return getDataTable(list);
    }

    /**
     * 导出模板列表
     */
    @PreAuthorize("@ss.hasPermi('admin:baFormwork:export')")
    @Log(title = "模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出模板列表接口")
    public void export(HttpServletResponse response, BaFormwork baFormwork) throws IOException
    {
        //租户ID
        baFormwork.setTenantId(SecurityUtils.getCurrComId());
        List<BaFormwork> list = baFormworkService.selectBaFormworkList(baFormwork);
        ExcelUtil<BaFormwork> util = new ExcelUtil<BaFormwork>(BaFormwork.class);
        util.exportExcel(response, list, "baFormwork");
    }

    /**
     * 获取模板详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:baFormwork:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取模板详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baFormworkService.selectBaFormworkById(id));
    }

    /**
     * 新增模板
     */
    @PreAuthorize("@ss.hasPermi('admin:baFormwork:add')")
    @Log(title = "模板", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增模板接口")
    public AjaxResult add(@RequestBody BaFormwork baFormwork)
    {
        return toAjax(baFormworkService.insertBaFormwork(baFormwork));
    }

    /**
     * 修改模板
     */
    @PreAuthorize("@ss.hasPermi('admin:baFormwork:edit')")
    @Log(title = "模板", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改模板接口")
    public AjaxResult edit(@RequestBody BaFormwork baFormwork)
    {
        return toAjax(baFormworkService.updateBaFormwork(baFormwork));
    }

    /**
     * 删除模板
     */
    @PreAuthorize("@ss.hasPermi('admin:baFormwork:remove')")
    @Log(title = "模板", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除模板接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baFormworkService.deleteBaFormworkByIds(ids));
    }
}
