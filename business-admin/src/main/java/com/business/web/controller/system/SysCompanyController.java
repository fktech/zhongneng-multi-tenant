package com.business.web.controller.system;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaFee;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.BaSupplierInstanceRelatedEditDTO;
import com.business.system.domain.dto.SysCompanyInstanceRelatedEditDTO;
import com.business.system.service.ISysCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 公司信息Controller
 *
 * @author leslie1015
 * @date 2020-07-11
 */
@RestController
@RequestMapping("/system/company")
@Api(tags = "企业信息")
public class SysCompanyController extends BaseController {

  @Autowired
  private ISysCompanyService sysCompanyService;

  /**
   * 查询公司信息列表
   */
//  @PreAuthorize("@ss.hasPermi('system:company:list')")
  @GetMapping("/list")
  public TableDataInfo list(SysCompany sysCompany) {
    sysCompany.setScreen("all");
    startPage();
    List<SysCompany> list = sysCompanyService.selectSysCompanyList(sysCompany);
    return getDataTable(list);
  }

  /**
   * 查询公司信息列表用户多租户业务数据授权
   */
//  @PreAuthorize("@ss.hasPermi('system:company:list')")
  @GetMapping("/companyList/{tenantId}")
  public AjaxResult getCompanyList(@PathVariable("tenantId") String tenantId) {
    List<SysCompany> list = sysCompanyService.getCompanyList(tenantId);
    return AjaxResult.success(list);
  }

  /**
   * 导出公司信息列表
   */
  @PreAuthorize("@ss.hasPermi('system:company:export')")
  @Log(title = "公司信息", businessType = BusinessType.EXPORT)
  @PostMapping("/export")
  public void export(HttpServletResponse response, SysCompany sysCompany) throws IOException
  {
    sysCompany.setScreen("all");
    List<SysCompany> list = sysCompanyService.selectSysCompanyList(sysCompany);
    ExcelUtil<SysCompany> util = new ExcelUtil<SysCompany>(SysCompany.class);
    util.exportExcel(response, list, "sysCompany");
  }

  /**
   * 获取公司信息详细信息
   */
//  @PreAuthorize("@ss.hasPermi('system:company:query')")
  @GetMapping(value = "/{id}")
  public AjaxResult getInfo(@PathVariable("id") String id) {
    return AjaxResult.success(sysCompanyService.selectSysCompanyById(id));
  }

  /**
   * 新增公司信息
   */
  @PreAuthorize("@ss.hasPermi('system:company:add')")
  @Log(title = "公司信息", businessType = BusinessType.INSERT)
  @PostMapping
  public AjaxResult add(@RequestBody SysCompany sysCompany) {
    checkSuperAdmin();
    int result = sysCompanyService.insertSysCompany(sysCompany);
    if(result == -1){
      return AjaxResult.error(-1,"公司简称已存在");
    }
    return toAjax(result);
  }

  /**
   * 新增公司信息
   */
  @PostMapping("/addSysCompany")
  @ApiOperation("企业认证")
  @Anonymous
  public AjaxResult addSysCompany(@RequestBody SysCompany sysCompany) {
    int result = sysCompanyService.addSysCompany(sysCompany);
    if(result == 0){
      return AjaxResult.error(0,"启动流程失败");
    }else if(result == -1){
      return AjaxResult.error(-1,"公司简称已存在");
    }
    return toAjax(result);
  }

  /**
   * 修改公司信息
   */
  @PreAuthorize("@ss.hasPermi('system:company:edit')")
  @Log(title = "公司信息", businessType = BusinessType.UPDATE)
  @PutMapping
  public AjaxResult edit(@RequestBody SysCompany sysCompany) {

    int result = sysCompanyService.updateSysCompany(sysCompany);
    if(result == -1){
      return AjaxResult.error(-1,"公司简称已存在");
    }
    return toAjax(result);
  }

  /**
   * 删除公司信息
   */
  @PreAuthorize("@ss.hasPermi('system:company:remove')")
  @Log(title = "公司信息", businessType = BusinessType.DELETE)
  @DeleteMapping("/{ids}")
  public AjaxResult remove(@PathVariable String[] ids) {
    checkSuperAdmin();
    int result = sysCompanyService.deleteSysCompanyByIds(ids);
    if(result == -1){
      return AjaxResult.error(-1,"租户下存在用户");
    }
    return toAjax(result);
  }

  @ApiOperation("审批办理保存编辑业务数据")
  @PostMapping("/saveApproveBusinessData")
  public AjaxResult saveApproveBusinessData(@RequestBody SysCompanyInstanceRelatedEditDTO sysCompanyInstanceRelatedEditDTO) {
    int result = sysCompanyService.updateProcessBusinessData(sysCompanyInstanceRelatedEditDTO);
    if(result == 0){
      return AjaxResult.error(0,"任务办理失败");
    }
    return toAjax(result);
  }
}
