package com.business.web.controller.basics;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaLoadingStation;
import com.business.system.service.IBaLoadingStationService;

/**
 * 装货站Controller
 *
 * @author ljb
 * @date 2023-01-16
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/station")
@Api(tags ="装货站接口")
public class BaLoadingStationController extends BaseController
{
    @Autowired
    private IBaLoadingStationService baLoadingStationService;

    /**
     * 查询装货站列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:station:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询装货站列表接口")
    public TableDataInfo list(BaLoadingStation baLoadingStation)
    {
        //租户ID
        baLoadingStation.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaLoadingStation> list = baLoadingStationService.selectBaLoadingStationList(baLoadingStation);
        return getDataTable(list);
    }

    /**
     * 导出装货站列表
     */
    @PreAuthorize("@ss.hasPermi('admin:station:export')")
    @Log(title = "装货站", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出装货站列表接口")
    public void export(HttpServletResponse response, BaLoadingStation baLoadingStation) throws IOException
    {
        //租户ID
        baLoadingStation.setTenantId(SecurityUtils.getCurrComId());
        List<BaLoadingStation> list = baLoadingStationService.selectBaLoadingStationList(baLoadingStation);
        ExcelUtil<BaLoadingStation> util = new ExcelUtil<BaLoadingStation>(BaLoadingStation.class);
        util.exportExcel(response, list, "station");
    }

    /**
     * 获取装货站详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:station:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取装货站详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baLoadingStationService.selectBaLoadingStationById(id));
    }

    /**
     * 新增装货站
     */
    @PreAuthorize("@ss.hasPermi('admin:station:add')")
    @Log(title = "装货站", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增装货站接口")
    public AjaxResult add(@RequestBody BaLoadingStation baLoadingStation)
    {
        return toAjax(baLoadingStationService.insertBaLoadingStation(baLoadingStation));
    }

    /**
     * 修改装货站
     */
    @PreAuthorize("@ss.hasPermi('admin:station:edit')")
    @Log(title = "装货站", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改装货站接口")
    public AjaxResult edit(@RequestBody BaLoadingStation baLoadingStation)
    {
        return toAjax(baLoadingStationService.updateBaLoadingStation(baLoadingStation));
    }

    /**
     * 删除装货站
     */
    @PreAuthorize("@ss.hasPermi('admin:station:remove')")
    @Log(title = "装货站", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除装货站接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baLoadingStationService.deleteBaLoadingStationByIds(ids));
    }
}
