package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaRenew;
import com.business.system.service.IBaRenewService;

/**
 * 市场动态更新Controller
 *
 * @author ljb
 * @date 2023-03-22
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/renew")
@Api(tags ="市场动态更新接口")
public class BaRenewController extends BaseController
{
    @Autowired
    private IBaRenewService baRenewService;

    /**
     * 查询市场动态更新列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:renew:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询市场动态更新列表接口")
    public TableDataInfo list(BaRenew baRenew)
    {
        //租户ID
        baRenew.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaRenew> list = baRenewService.selectBaRenewList(baRenew);
        return getDataTable(list);
    }

    /**
     * 导出市场动态更新列表
     */
    @PreAuthorize("@ss.hasPermi('admin:renew:export')")
    @Log(title = "市场动态更新", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出市场动态更新列表接口")
    public void export(HttpServletResponse response, BaRenew baRenew) throws IOException
    {
        List<BaRenew> list = baRenewService.selectBaRenewList(baRenew);
        ExcelUtil<BaRenew> util = new ExcelUtil<BaRenew>(BaRenew.class);
        util.exportExcel(response, list, "renew");
    }

    /**
     * 获取市场动态更新详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:renew:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取市场动态更新详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baRenewService.selectBaRenewById(id));
    }

    /**
     * 新增市场动态更新
     */
    @PreAuthorize("@ss.hasPermi('admin:renew:add')")
    @Log(title = "市场动态更新", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增市场动态更新接口")
    public AjaxResult add(@RequestBody BaRenew baRenew)
    {
        return toAjax(baRenewService.insertBaRenew(baRenew));
    }

    /**
     * 修改市场动态更新
     */
    @PreAuthorize("@ss.hasPermi('admin:renew:edit')")
    @Log(title = "市场动态更新", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改市场动态更新接口")
    public AjaxResult edit(@RequestBody BaRenew baRenew)
    {
        return toAjax(baRenewService.updateBaRenew(baRenew));
    }

    /**
     * 删除市场动态更新
     */
    @PreAuthorize("@ss.hasPermi('admin:renew:remove')")
    @Log(title = "市场动态更新", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除市场动态更新接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baRenewService.deleteBaRenewByIds(ids));
    }
}
