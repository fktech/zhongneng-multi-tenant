package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.service.IBaAutomobileSettlementInvoiceCmstService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaAutomobileSettlementInvoice;
import com.business.system.service.IBaAutomobileSettlementInvoiceService;

/**
 * 无车承运开票Controller
 *
 * @author single
 * @date 2023-04-12
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/automobileInvoice")
@Api(tags ="无车承运开票接口")
public class BaAutomobileSettlementInvoiceController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementInvoiceService baAutomobileSettlementInvoiceService;

    @Autowired
    private IBaAutomobileSettlementInvoiceCmstService baAutomobileSettlementInvoiceCmstService;

    /**
     * 查询无车承运开票列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileInvoice:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询无车承运开票列表接口")
    public TableDataInfo list(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        startPage();
        List<BaAutomobileSettlementInvoice> list = baAutomobileSettlementInvoiceService.selectBaAutomobileSettlementInvoiceList(baAutomobileSettlementInvoice);
        return getDataTable(list);
    }

    /**
     * 导出无车承运开票列表
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoice:export')")
    @Log(title = "无车承运开票", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出无车承运开票列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlementInvoice baAutomobileSettlementInvoice) throws IOException
    {
        List<BaAutomobileSettlementInvoice> list = baAutomobileSettlementInvoiceService.selectBaAutomobileSettlementInvoiceList(baAutomobileSettlementInvoice);
        ExcelUtil<BaAutomobileSettlementInvoice> util = new ExcelUtil<BaAutomobileSettlementInvoice>(BaAutomobileSettlementInvoice.class);
        util.exportExcel(response, list, "automobileInvoice");
    }

    /**
     * 获取无车承运开票详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobileInvoice:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取无车承运开票详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementInvoiceService.selectBaAutomobileSettlementInvoiceById(id));
    }

    /**
     * 新增无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoice:add')")
    @Log(title = "无车承运开票", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增无车承运开票接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        return toAjax(baAutomobileSettlementInvoiceService.insertBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice));
    }

    /**
     * 修改无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoice:edit')")
    @Log(title = "无车承运开票", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改无车承运开票接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        return toAjax(baAutomobileSettlementInvoiceService.updateBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice));
    }

    /**
     * 删除无车承运开票
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileInvoice:remove')")
    @Log(title = "无车承运开票", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除无车承运开票接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementInvoiceService.deleteBaAutomobileSettlementInvoiceByIds(ids));
    }

    /**
     * 昕科推送发票（发票信息）
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:edit')")
    //@Log(title = "昕科推送发票（发票信息）", businessType = BusinessType.UPDATE)
    @PostMapping("/addAutomobileSettlementInvoice")
    @ApiOperation(value = "昕科推送发票（发票信息）")
    @Anonymous
    public AjaxResult addAutomobileSettlementInvoice(@RequestBody JSONObject automobileSettlementInvoice)
    {
        int result = baAutomobileSettlementInvoiceService.addAutomobileSettlementInvoice(automobileSettlementInvoice);
        if(result == 0){
            return AjaxResult.error(0,"开票信息明细获取失败");
        }
        return toAjax(result);
    }

    @PostMapping("/updateInvoice")
    @ApiOperation(value = "昕科推送发票状态")
    @Anonymous
    public AjaxResult updateInvoice(@RequestBody JSONObject automobileInvoice)
    {
        int result = baAutomobileSettlementInvoiceService.updateInvoice(automobileInvoice);


        return toAjax(result);
    }

    @PostMapping("/deleteAutomobileSettlementInvoice")
    @ApiOperation(value = "删除无车承运开票接口")
    @Anonymous
    public AjaxResult deleteAutomobileSettlementInvoice(@RequestBody List<String> invoiceNo)
    {
        String[] strings = invoiceNo.toArray(new String[invoiceNo.size()]);

        return toAjax(baAutomobileSettlementInvoiceService.deleteBaAutomobileSettlementInvoiceByIds(strings));
    }

    @GetMapping("/selectInvoice")
    @ApiOperation(value = "查询网货开票明细")
    @Anonymous
    public AjaxResult selectInvoice(String invoiceNo)
    {


        return toAjax(baAutomobileSettlementInvoiceService.selectInvoice(invoiceNo));
    }

    /**
     * 中储开票信息
     * @param jsonObject
     * @return
     */
    @PostMapping("/added")
    @ApiOperation(value = "中储开票信息）")
    @Anonymous
    public AjaxResult added(@RequestBody JSONObject jsonObject)
    {
        int result = baAutomobileSettlementInvoiceCmstService.added(jsonObject);
        if(result == 0){
            return AjaxResult.error(0,"开票信息明细获取失败");
        }
        return toAjax(result);
    }
}
