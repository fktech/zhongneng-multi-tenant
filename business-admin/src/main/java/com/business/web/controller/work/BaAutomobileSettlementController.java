package com.business.web.controller.work;

import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaAutomobileSettlement;
import com.business.system.service.IBaAutomobileSettlementCmstService;
import com.business.system.service.IBaAutomobileSettlementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 汽运结算Controller
 *
 * @author single
 * @date 2023-02-17
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/automobileSettlement")
@Api(tags ="汽运结算接口")
public class BaAutomobileSettlementController extends BaseController
{
    @Autowired
    private IBaAutomobileSettlementService baAutomobileSettlementService;

    @Autowired
    private IBaAutomobileSettlementCmstService baAutomobileSettlementCmstService;

    /**
     * 查询汽运结算列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileSettlement:list')")*/
    @GetMapping("/list")
    @ApiOperation(value = "查询汽运结算列表接口")
    public TableDataInfo list(BaAutomobileSettlement baAutomobileSettlement)
    {
        startPage();
        List<BaAutomobileSettlement> list = baAutomobileSettlementService.selectBaAutomobileSettlementList(baAutomobileSettlement);
        return getDataTable(list);
    }

    /**
     * 导出汽运结算列表
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:export')")
    @Log(title = "汽运结算", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出汽运结算列表接口")
    public void export(HttpServletResponse response, BaAutomobileSettlement baAutomobileSettlement) throws IOException
    {
        List<BaAutomobileSettlement> list = baAutomobileSettlementService.selectBaAutomobileSettlementList(baAutomobileSettlement);
        ExcelUtil<BaAutomobileSettlement> util = new ExcelUtil<BaAutomobileSettlement>(BaAutomobileSettlement.class);
        util.exportExcel(response, list, "automobileSettlement");
    }

    /**
     * 获取汽运结算详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:automobileSettlement:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取汽运结算详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baAutomobileSettlementService.selectBaAutomobileSettlementById(id));
    }

    /**
     * 新增汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:add')")
    @Log(title = "汽运结算", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增汽运结算接口")
    public AjaxResult add(@RequestBody BaAutomobileSettlement baAutomobileSettlement)
    {
        return toAjax(baAutomobileSettlementService.insertBaAutomobileSettlement(baAutomobileSettlement));
    }

    /**
     * 修改汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:edit')")
    @Log(title = "汽运结算", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改汽运结算接口")
    public AjaxResult edit(@RequestBody BaAutomobileSettlement baAutomobileSettlement)
    {
        return toAjax(baAutomobileSettlementService.updateBaAutomobileSettlement(baAutomobileSettlement));
    }

    /**
     * 删除汽运结算
     */
    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:remove')")
    @Log(title = "汽运结算", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除汽运结算接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baAutomobileSettlementService.deleteBaAutomobileSettlementByIds(ids));
    }

    /**
     * 昕科推送汽运结算（付款单）
     */
//    @PreAuthorize("@ss.hasPermi('admin:automobileSettlement:edit')")
    //@Log(title = "昕科推送汽运结算（付款单）", businessType = BusinessType.UPDATE)
    @PostMapping("/addAutomobileSettlement")
    @ApiOperation(value = "昕科推送汽运结算（付款单）")
    @Anonymous
    public AjaxResult addAutomobileSettlement(@RequestBody JSONObject automobileSettlement)
    {
        int result = baAutomobileSettlementService.addAutomobileSettlement(automobileSettlement);
        if(result == 0){
            return AjaxResult.error(0,"付款明细回调失败");
        }
        return toAjax(result);
    }

    /**
     * 新科删除汽运结算
     */
    @PostMapping("/deleteAutomobileSettlement")
    @ApiOperation(value = "昕科删除汽运结算接口")
    @Anonymous
    public AjaxResult deleteAutomobileSettlement(@RequestBody List<String> paymentOrderNum)
    {
        String[] strings = paymentOrderNum.toArray(new String[paymentOrderNum.size()]);

        return toAjax(baAutomobileSettlementService.deleteBaAutomobileSettlementByIds(strings));
    }

    /**
     * 新科删除汽运结算
     */
    @GetMapping("/selectSettlement")
    @ApiOperation(value = "查询网货付款单明细")
    @Anonymous
    public AjaxResult selectSettlement(String paymentOrderNum)
    {


        return toAjax(baAutomobileSettlementService.selectSettlement(paymentOrderNum));
    }

    @PostMapping("/added")
    @ApiOperation(value = "货主结算通知")
    @Anonymous
    public AjaxResult added(@RequestBody JSONObject jsonObject){

      return toAjax(baAutomobileSettlementCmstService.addSettlement(jsonObject));
    }

    @PostMapping("/editSettlement")
    @ApiOperation(value = "承运方结算通知")
    @Anonymous
    public AjaxResult editSettlement(@RequestBody JSONObject jsonObject){

        return toAjax(baAutomobileSettlementCmstService.editSettlement(jsonObject));
    }
}
