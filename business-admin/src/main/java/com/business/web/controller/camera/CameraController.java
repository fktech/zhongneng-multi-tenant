package com.business.web.controller.camera;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.annotation.Anonymous;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.StringUtils;
import com.business.system.domain.FileUploadResult;
import com.business.system.domain.dto.SysEventsSearchDTO;
import com.business.system.domain.vo.MaterialTypeVo;
import com.business.system.service.OssService;
import com.business.system.util.ExportMyWord;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@RestController
@CrossOrigin //跨域
@RequestMapping("/camera")
@Api(tags ="海康接口")
public class CameraController {

   /* @Autowired
    private BaProjectOngoingMapper baProjectOngoingMapper;*/

    @Autowired
    private OssService ossService;

    @Autowired
    private HttpServletRequest request;

    // 令牌自定义标识
    /*@Value("${file.path}")
    private String filePath;*/

    private static final String ARTEMIS_PATH = "/artemis";

    String host = "111.53.49.229:50443";
    String appkey = "20947274";
    String appSecret = "0s4VfibB33BTCj5YmQiS";


    @GetMapping("/callPostApiGetOrgList")
    @ApiOperation(value = "海康组织架构列表")
    @Anonymous
    public String callPostApiGetOrgList() throws Exception {
        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/resource/v1/org/orgList";
        Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        paramMap.put("pageNo", "1");
        paramMap.put("pageSize", "50");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", getCamsApi);
            }
        };
        return ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
    }

    @GetMapping("/rootList")
    @ApiOperation(value = "获取根区域信息")
    @Anonymous
    public String rootList() throws Exception {

        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/resource/v1/regions/root";
        Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        paramMap.put("pageNo", "1");
        paramMap.put("pageSize", "50");
        paramMap.put("treeCode","0");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(3) {
            {
                put("https://", getCamsApi);
            }
        };
        String s ="";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        return s;
    }


    @GetMapping("/subRegionsList")
    @ApiOperation(value = "根据区域编号获取下一级区域列表v2")
    @Anonymous
    public String subRegionsList(String parentIndexCode) throws Exception {

        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/resource/v2/regions/subRegions";
        Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        paramMap.put("pageNo", "1");
        paramMap.put("pageSize", "50");
        paramMap.put("parentIndexCode",parentIndexCode);
        paramMap.put("resourceType","camera");
        paramMap.put("cascadeFlag","0");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s ="";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        return s;
    }

    @GetMapping("/searchList")
    @ApiOperation(value = "查询监控点列表v2")
    @Anonymous
    public String searchList(String[] regionIndexCodes) throws Exception {

        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/resource/v2/camera/search";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageNo","1");
        jsonObject.put("pageSize","50");
        jsonObject.put("regionIndexCodes", regionIndexCodes);
        String body = JSON.toJSON(jsonObject).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s ="";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        return s;
    }


    @GetMapping("/previewURLs")
    @ApiOperation(value = "获取监控点预览取流URLv2")
    @Anonymous
    public String previewURLs(String cameraIndexCode) throws Exception {

        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/video/v2/cameras/previewURLs";
        Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        paramMap.put("cameraIndexCode", cameraIndexCode);
        paramMap.put("protocol", "ws");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s = ArtemisHttpUtil.doPostStringArtemis(config, path, body, null, null, "application/json");
        Object parse = JSONObject.parse(s);
        String code = ((JSONObject) parse).getString("code");
        if(code.equals("0")){
            ((JSONObject) parse).put("code",200);
        }
        return parse.toString();
    }

    @GetMapping("/eventsSearchList")
    @ApiOperation(value = "获取联动事件列表")
    @Anonymous
    public String eventsSearchList(SysEventsSearchDTO sysEventsSearchDTO) throws Exception {
        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/els/v1/events/search";
        //Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        JSONObject paramMap = new JSONObject();
        paramMap.put("startTime", sysEventsSearchDTO.getStartTime());
        paramMap.put("endTime", sysEventsSearchDTO.getEndTime());
        paramMap.put("pageSize", sysEventsSearchDTO.getPageSize());
        paramMap.put("pageNo", sysEventsSearchDTO.getPageNo());
        //左侧树筛选
        paramMap.put("eventType",sysEventsSearchDTO.getEventTypes());
        paramMap.put("eventRuleId",sysEventsSearchDTO.getEventRuleId());
        paramMap.put("regionIndexCode",sysEventsSearchDTO.getRegionIndexCode());
        paramMap.put("locationIndexCodes",sysEventsSearchDTO.getLocationIndexCodes());
        paramMap.put("resName",sysEventsSearchDTO.getResName());
        List indexCode = new ArrayList();
        if(StringUtils.isNotEmpty(sysEventsSearchDTO.getIndexCode())){
            String[] split = sysEventsSearchDTO.getIndexCode().split(",");
            for (String code:split) {
                indexCode.add(code);
            }
        }
        sysEventsSearchDTO.setResIndexCodes(indexCode);
        paramMap.put("resIndexCodes",sysEventsSearchDTO.getResIndexCodes());
        paramMap.put("eventLevels",sysEventsSearchDTO.getEventLevels());
        paramMap.put("handleStatus",sysEventsSearchDTO.getHandleStatus());
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(11) {
            {
                put("https://", getCamsApi);
            }
        };
        String s = "";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config, path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        Object parse = JSONObject.parse(s);
        String code = ((JSONObject) parse).getString("code");
        if(code.equals("0")){
            ((JSONObject) parse).put("code",200);
        }
        return parse.toString();
    }

    @GetMapping("/cameraList")
    @ApiOperation(value = "获取监控点在线状态")
    @Anonymous
    public String cameraList(String status,String[] indexCodes) throws Exception {
        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/nms/v1/online/camera/get";
        //Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        JSONObject paramMap = new JSONObject();
        paramMap.put("status",status);
        paramMap.put("indexCodes",indexCodes);
        paramMap.put("pageSize", "50");
        paramMap.put("pageNo", "1");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s ="";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        return s;
    }

   /* @GetMapping("/manualCapture")
    @ApiOperation(value = "手动抓图")
    @Anonymous
    public List<String> manualCapture(String projectName) throws Exception {
        List<String> cameraResultList = new ArrayList<>();
        QueryWrapper<BaProjectOngoing> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", projectName);
        queryWrapper.eq("flag",0);
        queryWrapper.eq("mark",1).and(item ->item.eq("state", AdminCodeEnum.PROJECT_RUN_STATUS_PASS.getCode()).or().eq("state", AdminCodeEnum.PROJECT_STATUS_PASS.getCode()));
        List<BaProjectOngoing> baProjectOngoings = baProjectOngoingMapper.selectList(queryWrapper);
        if(!CollectionUtils.isEmpty(baProjectOngoings)){
            BaProjectOngoing baProjectOngoing = baProjectOngoings.get(0);
            String cameraIndexCode = baProjectOngoing.getCamera();
            if(StringUtils.isNotEmpty(cameraIndexCode)){
                List<String> cameraList = Arrays.asList(cameraIndexCode.split(","));
                ArtemisConfig config = new ArtemisConfig();
                config.setHost(host); // 代理API网关nginx服务器ip端口
                config.setAppKey(appkey);  // 秘钥appkey
                config.setAppSecret(appSecret);// 秘钥appSecret
                final String getCamsApi = ARTEMIS_PATH + "/api/video/v1/manualCapture";
                for(String camera : cameraList){
                    Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
                    paramMap.put("cameraIndexCode", camera);
                    String body = JSON.toJSON(paramMap).toString();
                    Map<String, String> path = new HashMap<String, String>(5) {
                        {
                            put("https://", getCamsApi);
                        }
                    };
                    String s = ArtemisHttpUtil.doPostStringArtemis(config, path, body, null, null, "application/json");
                    Map map = (Map) JSONObject.parse(s);
                    if(!ObjectUtils.isEmpty(map)){
                        Map dataMap = (Map) map.get("data");
                        if(!ObjectUtils.isEmpty(dataMap)){
                            ExportMyWord emw = new ExportMyWord();
                            String picUrl = String.valueOf(dataMap.get("picUrl"));
                            //上传oss
                            cameraResultList.add(emw.OssUrl(picUrl, ossService, request, filePath));
                        }
                    }
                }
            }
        }
        return cameraResultList;
    }*/

    public static void main(String[] args) throws Exception {
        //定义一个URL对象，就是你想下载的图片的URL地址
        URL url = new URL("https://1.183.46.34:6114/pic?1d83=7816i3a-=o4418p=eddf*db-8bedc12b7*487==sp***111==6t7956996080=0l6*4015-8o*ee36od110l16-a64790");
        //打开连接
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //设置请求方式为"GET"
        conn.setRequestMethod("GET");
        //超时响应时间为10秒
        conn.setConnectTimeout(10 * 1000);
        //通过输入流获取图片数据
        InputStream is = conn.getInputStream();
        //得到图片的二进制数据，以二进制封装得到数据，具有通用性
        byte[] data = readInputStream(is);
        //创建一个文件对象用来保存图片，默认保存当前工程根目录，起名叫Copy.jpg
        File imageFile = new File("G:/qrCode/Copy.jpg");
        //创建输出流
        FileOutputStream outStream = new FileOutputStream(imageFile);
        //写入数据
        outStream.write(data);
        //关闭输出流，释放资源
        outStream.close();
    }

    public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //创建一个Buffer字符串
        byte[] buffer = new byte[6024];
        //每次读取的字符串长度，如果为-1，代表全部读取完毕
        int len;
        //使用一个输入流从buffer里把数据读取出来
        while ((len = inStream.read(buffer)) != -1) {
            //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
            outStream.write(buffer, 0, len);
        }
        //关闭输入流
        inStream.close();
        //把outStream里的数据写入内存
        return outStream.toByteArray();
    }

    //视频下拉数据组装
    @GetMapping("/videoList")
    @ApiOperation(value = "视频下拉")
    @Anonymous
    public List<MaterialTypeVo> videoList() {
        List<MaterialTypeVo> result = new ArrayList<>();
        try {
            //数据组合放入一级分类
            MaterialTypeVo materialTypeVo = new MaterialTypeVo();
            String s = this.rootList();
            //获取根区域信息
            Object parse = JSONObject.parse(s);
            JSONObject obj = (JSONObject) JSON.toJSON(parse);
            JSONObject data = obj.getJSONObject("data");
            String indexCode = data.getString("indexCode");
            String name = data.getString("name");
            materialTypeVo.setLabel(indexCode);
            materialTypeVo.setValue(name);
            //根区域下级区域
            String subRegionsList = this.subRegionsList(indexCode);
            parse = JSONObject.parse(subRegionsList);
            obj = (JSONObject) JSON.toJSON(parse);
            data = obj.getJSONObject("data");
            JSONArray list = data.getJSONArray("list");
            List<MaterialTypeVo> typeVoList = new ArrayList<>();
            //二级分类
            String regionIndexCodes = "";
            for (Object object:list) {
                MaterialTypeVo childListVo = new MaterialTypeVo();
                 obj = (JSONObject) JSON.toJSON(object);
                 childListVo.setLabel( obj.getString("indexCode"));
                 childListVo.setValue(obj.getString("name"));
                typeVoList.add(childListVo);
                regionIndexCodes = childListVo.getLabel()+","+regionIndexCodes;
            }
            //三级菜单
            for (MaterialTypeVo materialTypeVo1:typeVoList) {
                String substring = regionIndexCodes.substring(0, regionIndexCodes.length() - 1);
                String[] split = substring.split(",");
                String searchList = this.searchList(split);
                parse = JSONObject.parse(searchList);
                obj = (JSONObject) JSON.toJSON(parse);
                data = obj.getJSONObject("data");
                JSONArray list1 = data.getJSONArray("list");
                List<MaterialTypeVo> voList = new ArrayList<>();
                for (Object aa:list1) {
                    MaterialTypeVo materialTypeVo2 = new MaterialTypeVo();
                    obj = (JSONObject) JSON.toJSON(aa);
                    materialTypeVo2.setLabel( obj.getString("indexCode"));
                    materialTypeVo2.setValue(obj.getString("name"));
                    voList.add(materialTypeVo2);
                }
                materialTypeVo1.setChildren(voList);
            }
            materialTypeVo.setChildren(typeVoList);
            result.add(materialTypeVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
