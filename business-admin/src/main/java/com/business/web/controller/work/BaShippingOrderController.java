package com.business.web.controller.work;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaShippingOrderCmst;
import com.business.system.domain.dto.waybillDTO;
import com.business.system.service.IBaShippingOrderCmstService;
import com.google.inject.internal.asm.$ClassWriter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaShippingOrder;
import com.business.system.service.IBaShippingOrderService;

/**
 * 发运单Controller
 *
 * @author single
 * @date 2023-02-18
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/shippingOrder")
@Api(tags ="发运单接口")
public class BaShippingOrderController extends BaseController
{
    @Autowired
    private IBaShippingOrderService baShippingOrderService;

    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    /**
     * 查询发运单列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询发运单列表接口")
    public TableDataInfo list(BaShippingOrder baShippingOrder)
    {
        startPage();
        List<BaShippingOrder> list = baShippingOrderService.selectBaShippingOrderList(baShippingOrder);
        return getDataTable(list);
    }

    @GetMapping("/waybillDTOList")
    @ApiOperation(value = "新查询发运单列表接口")
    public TableDataInfo waybillDTOList(BaShippingOrder baShippingOrder)
    {
        startPage();
        List<waybillDTO> list = baShippingOrderService.waybillDTOList(baShippingOrder);
        return getDataTable(list);
    }

    /**
     * 导出发运单列表
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:export')")
    @PostMapping("/export")
    @ApiOperation(value = "导出发运单列表接口")
    public void export(HttpServletResponse response, BaShippingOrder baShippingOrder) throws IOException
    {
        List<waybillDTO> list = baShippingOrderService.waybillDTOList(baShippingOrder);
        ExcelUtil<waybillDTO> util = new ExcelUtil<waybillDTO>(waybillDTO.class);
        util.exportExcel(response, list, "shippingOrder");
    }

    /**
     * 获取发运单详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取发运单详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baShippingOrderService.selectBaShippingOrderById(id));
    }

    /**
     * 新增发运单
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:add')")
    //@Log(title = "发运单", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增发运单接口")
    @Anonymous
    public AjaxResult add(@RequestBody JSONObject object)
    {
        System.out.println("****运单信息**** : " + JSONObject.toJSONString(object));
        /*Date date = new Date(object.getLong("yfdate"));
        System.out.println("****时间**** : " + date);*/
        BaShippingOrder baShippingOrder = new BaShippingOrder();
        //实收吨数（存在）
        baShippingOrder.setSsdun(object.getBigDecimal("ssdun"));
        //运单编号（存在）
        baShippingOrder.setThdno(object.getString("thdno"));
        //发货详细地址（存在）
        baShippingOrder.setDepDetailed(object.getString("depDetailed"));
        //运单id
        baShippingOrder.setId(object.getString("thdno"));
        //车号（存在）
        baShippingOrder.setCarno(object.getString("carno"));
        //车主电话（存在）
        baShippingOrder.setCartel(object.getString("cartel"));
        //原发时间（存在）
        baShippingOrder.setYfdate(object.getDate("yfdate"));
        //进场时间（存在）
        baShippingOrder.setDhdate(object.getDate("dhdate"));
        //装车磅单图片（存在）
        baShippingOrder.setVidpic1(object.getString("vidpic1"));
        //卸车磅单图片（存在）
        baShippingOrder.setVidpic2(object.getString("vidpic2"));
        //出场净重（存在）
        baShippingOrder.setCcjweight(object.getBigDecimal("ccjweight"));
        //入场净重（存在）
        baShippingOrder.setRcjweight(object.getBigDecimal("rcjweight"));
        //磅差（存在）
        baShippingOrder.setBangcha(object.getBigDecimal("bangcha"));
        //涨吨
        baShippingOrder.setZhangdun(object.getBigDecimal("zhangdun"));
        //实收吨数（存在）
        baShippingOrder.setSsdun(object.getBigDecimal("ssdun"));
        //运费结算重量（存在）
        baShippingOrder.setYfjsweight(object.getBigDecimal("yfjsweight"));
        //货主货源应付运费（存在）
        baShippingOrder.setYfratesmoney(object.getBigDecimal("yfratesmoney"));
        //货主运费扣款金额（存在）
        baShippingOrder.setRateskoumoney(object.getBigDecimal("rateskoumoney"));
        //涨吨金额（存在）
        baShippingOrder.setZdmoney(object.getBigDecimal("zdmoney"));
        //经济人货源运费扣款金额（存在）
        baShippingOrder.setRateskoumoneyEconomic(object.getBigDecimal("rateskoumoneyEconomic"));
        //货源单编号（存在）
        baShippingOrder.setSupplierNo(object.getString("vidpic8"));
        //货主运费单价（存在）
        baShippingOrder.setRatesprice(object.getBigDecimal("ratesprice"));
        //货物名称（存在）
        baShippingOrder.setCoaltypename(object.getString("coaltypename"));
        //是否结算运费（存在）
        baShippingOrder.setIsjsratesflag(object.getInteger("isjsratesflag"));
        //实际结算金额（存在）
        baShippingOrder.setSfratesmoney(object.getBigDecimal("sfratesmoney"));
        //信息费（存在）
        baShippingOrder.setInfocost(object.getBigDecimal("infocost"));
        //其他扣款（存在）
        baShippingOrder.setRatesdikoumoney(object.getBigDecimal("ratesdikoumoney"));
        //收货单位（存在）
        baShippingOrder.setFhunitname(object.getString("fhunitname"));
        //发货单位（存在）
        baShippingOrder.setShunitname(object.getString("shunitname"));
        //驾驶号（存在）
        baShippingOrder.setDriverno(object.getString("driverno"));
        //司机名称
        baShippingOrder.setCardriver(object.getString("cardriver"));
        return toAjax(baShippingOrderService.insertBaShippingOrder(baShippingOrder));
    }

    /**
     * 中储新增发运单
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:add')")
    //@Log(title = "发运单", businessType = BusinessType.INSERT)
    @PostMapping("/added")
    @ApiOperation(value = "中储新增发运单接口")
    @Anonymous
    public AjaxResult added(@RequestBody JSONObject object)
    {
        System.out.println(object);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        BaShippingOrderCmst baShippingOrderCmst = new BaShippingOrderCmst();
        //0 抢单,1 竞价
        baShippingOrderCmst.setOrderModel(object.getString("orderModel"));
        //订单号，如果是批量货，就是子单号
        baShippingOrderCmst.setOrderId(object.getString("orderId"));
        baShippingOrderCmst.setId(object.getString("orderId"));
        //如果为批量货，就为母单号，普通货为空
        baShippingOrderCmst.setYardId(object.getString("yardId"));
        //自定义单号
        baShippingOrderCmst.setSelfComment(object.getString("selfComment"));

        //货主名称
        baShippingOrderCmst.setConsignorUserName(object.getString("consignorUserName"));
        //货主名称手机号
        baShippingOrderCmst.setConsignorMobile(object.getString("consignorMobile"));
        //承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止
        baShippingOrderCmst.setConsignorState(object.getString("consignorState"));
        //货物名称
        baShippingOrderCmst.setCargoName(object.getString("cargoName"));
        //承运方姓名
        baShippingOrderCmst.setCarrierName(object.getString("carrierName"));
        //承运方手机号
        baShippingOrderCmst.setCarrierMobile(object.getString("carrierMobile"));
        //摘牌时间，日期格式 格式:yyyy-mm-dd hh:mm:ss
        baShippingOrderCmst.setDelistTime(object.getDate("delistTime"));
        //摘单吨位
        baShippingOrderCmst.setWeight(object.getBigDecimal("weight"));
        //车牌号
        baShippingOrderCmst.setPlateNumber(object.getString("plateNumber"));
        //司机姓名
        baShippingOrderCmst.setDriverUserName(object.getString("driverUserName"));
        //司机手机号
        baShippingOrderCmst.setDriverMobile(object.getString("driverMobile"));
        //司机身份证号码
        baShippingOrderCmst.setDriverIdNumber(object.getString("driverIDNumber"));
        //司机身份证号附件
        baShippingOrderCmst.setDriverIdPhoto(object.getString("driverIDPhoto"));
        //司机驾驶证证号
        baShippingOrderCmst.setDriverLicenseNumber(object.getString("driverLicenseNumber"));
        //司机驾驶证证附件
        JSONArray driverLicensePhotos = object.getJSONArray("driverLicensePhoto");
        if(driverLicensePhotos != null) {
            String driverLicensePhoto = "";
            if (driverLicensePhotos.size() > 0) {
                for (Object o : driverLicensePhotos) {
                    driverLicensePhoto = o.toString() + "," + driverLicensePhoto;
                }
                baShippingOrderCmst.setDriverLicensePhoto(driverLicensePhoto.substring(0, driverLicensePhoto.length() - 1));
            }
        }
        //司机从业资格信息证号
        baShippingOrderCmst.setDriverOccupationNumber(object.getString("driverOccupationNumber"));
        //司机从业资格信息证附件
        JSONArray driverOccupationPhotos = object.getJSONArray("driverOccupationPhoto");
        if(driverOccupationPhotos != null) {
            if (driverOccupationPhotos.size() > 0) {
                String driverOccupationPhoto = "";
                for (Object o : driverOccupationPhotos) {
                    driverOccupationPhoto = o.toString() + "," + driverOccupationPhoto;
                }
                baShippingOrderCmst.setDriverOccupationPhoto(driverOccupationPhoto.substring(0, driverOccupationPhoto.length() - 1));
            }
        }
        //车辆行驶证号码
        baShippingOrderCmst.setTruckLicenseNumber(object.getString("TruckLicenseNumber"));
        //车辆行驶证附件
        JSONArray truckLicensePhotos = object.getJSONArray("TruckLicensePhoto");
        if(truckLicensePhotos != null) {
            if (truckLicensePhotos.size() > 0) {
                String truckLicensePhoto = "";
                for (Object o : truckLicensePhotos) {
                    truckLicensePhoto = o.toString() + "," + truckLicensePhoto;
                }
                baShippingOrderCmst.setTruckLicensePhoto(truckLicensePhoto.substring(0, truckLicensePhoto.length() - 1));
            }
        }
        //道路运输证号
        baShippingOrderCmst.setTruckRoadNumber(object.getString("TruckRoadNumber"));
        //道路运输证附件
        JSONArray truckRoadPhotos = object.getJSONArray("TruckRoadPhoto");
        if(truckRoadPhotos != null) {
            if (truckRoadPhotos.size() > 0) {
                String truckRoadPhoto = "";
                for (Object o : truckRoadPhotos) {
                    truckRoadPhoto = o.toString() + "," + truckRoadPhoto;
                }
                baShippingOrderCmst.setTruckRoadPhoto(truckRoadPhoto.substring(0, truckRoadPhoto.length() - 1));
            }
        }
        //司机身份证号附件正面
        JSONArray driverIDPhotoFronts = object.getJSONArray("driverIDPhotoFront");
        if(driverIDPhotoFronts != null) {
            if (driverIDPhotoFronts.size() > 0) {
                String driverIDPhotoFront = "";
                for (Object o : driverIDPhotoFronts) {
                    driverIDPhotoFront = o.toString() + "," + driverIDPhotoFront;
                }
                baShippingOrderCmst.setDriverIDPhotoFront(driverIDPhotoFront.substring(0, driverIDPhotoFront.length() - 1));
            }
        }
        //司机身份证号附件反面
        JSONArray driverIDPhotoBacks = object.getJSONArray("driverIDPhotoBack");
        if(driverIDPhotoBacks != null) {
            if (driverIDPhotoBacks.size() > 0) {
                String driverIDPhotoBack = "";
                for (Object o : driverIDPhotoFronts) {
                    driverIDPhotoBack = o.toString() + "," + driverIDPhotoBack;
                }
                baShippingOrderCmst.setDriverIDPhotoFront(driverIDPhotoBack.substring(0, driverIDPhotoBack.length() - 1));
            }
        }
        //挂车行驶证附件
        JSONArray truckLicensePhototrailers = object.getJSONArray("TruckLicensePhototrailer");
        if(truckLicensePhototrailers != null){
        if(truckLicensePhototrailers.size() > 0){
            String truckLicensePhototrailer = "";
            for (Object o:truckLicensePhototrailers) {
                truckLicensePhototrailer = o.toString()+","+truckLicensePhototrailer;
            }
            baShippingOrderCmst.setTruckLicensePhototrailer(truckLicensePhototrailer.substring(0,truckLicensePhototrailer.length()-1));
        }
          }
        //挂车行驶证号
        String truckLicenseNumbertrailer = object.getString("TruckLicenseNumbertrailer");
        baShippingOrderCmst.setTruckLicenseNumbertrailer(truckLicenseNumbertrailer);
        return toAjax(baShippingOrderCmstService.insertBaShippingOrderCmst(baShippingOrderCmst));
    }

    /**
     * 修改发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:edit')")
    @Log(title = "发运单", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改发运单接口")
    public AjaxResult edit(@RequestBody BaShippingOrder baShippingOrder)
    {
        return toAjax(baShippingOrderService.updateBaShippingOrder(baShippingOrder));
    }

    /**
     * 签收同时修改运单
     * @param jsonObject
     * @return
     */
    @PostMapping("/editShippingOrder")
    @ApiOperation(value = "昕科修改发运单接口")
    @Anonymous
    public AjaxResult editShippingOrder(@RequestBody JSONObject jsonObject)
    {
        return toAjax(baShippingOrderService.updateShippingOrder(jsonObject));
    }

    /**
     * 中储：确认发货通知/确认收货通知/运单变更通知
     * @param jsonObject
     * @return
     */
    @PostMapping("/confirmShippingOrder")
    @ApiOperation(value = "中储：确认发货通知/确认收货通知/运单变更通知")
    @Anonymous
    public AjaxResult confirmShippingOrder(@RequestBody JSONObject jsonObject)
    {
        return toAjax(baShippingOrderCmstService.updateShippingOrderCmst(jsonObject));
    }

    /**
     * 删除发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:remove')")
    @Log(title = "发运单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除发运单接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baShippingOrderService.deleteBaShippingOrderByIds(ids));
    }

    /**
     * 运单签收
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:sign')")
    @GetMapping("/sign/{ids}")
    @ApiOperation(value = "签收")
    public AjaxResult sign(@PathVariable String[] ids){
        int result = baShippingOrderService.signFor(ids);
        if(result == 0){
            return AjaxResult.error("未入住网货平台",result);
        }
        return toAjax(result);
    }

    /**
     * 发运单签收回调
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:edit')")
    @Log(title = "发运单签收回调", businessType = BusinessType.UPDATE)
    @PostMapping("/editSignForCallback")
    @ApiOperation(value = "发运单签收回调")
    @Anonymous
    public AjaxResult editSignForCallback(@RequestBody List<String> transNos)
    {
        return toAjax(baShippingOrderService.editSignForCallback(transNos));
    }

    /**
     * 中储：违约结果通知
     * @param jsonObject
     * @return
     */
    @PostMapping("/resultShippingOrder")
    @ApiOperation(value = "中储：违约结果通知")
    @Anonymous
    public AjaxResult resultShippingOrder(@RequestBody JSONObject jsonObject)
    {
        return toAjax(baShippingOrderCmstService.updateShippingOrderCmst(jsonObject));
    }

    //中储结算
    @PostMapping("/receipt")
    @ApiOperation(value = "回单确认通知")
    @Anonymous
    public AjaxResult receipt(@RequestBody JSONObject jsonObject)
    {
        int result = baShippingOrderCmstService.receiptShippingOrder(jsonObject);
        if(result == 0){
            return AjaxResult.error(0,"回单确认通知");
        }
        return toAjax(result);
    }
}
