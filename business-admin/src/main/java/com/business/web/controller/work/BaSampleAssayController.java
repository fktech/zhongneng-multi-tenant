package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaSampleAssay;
import com.business.system.service.IBaSampleAssayService;

/**
 * 取样化验Controller
 *
 * @author ljb
 * @date 2024-01-06
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/sampleAssay")
@Api(tags ="取样化验接口")
public class BaSampleAssayController extends BaseController
{
    @Autowired
    private IBaSampleAssayService baSampleAssayService;

    /**
     * 查询取样化验列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:sampleAssay:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询取样化验列表接口")
    public TableDataInfo list(BaSampleAssay baSampleAssay)
    {
        //租户ID
        baSampleAssay.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSampleAssay> list = baSampleAssayService.selectBaSampleAssayList(baSampleAssay);
        return getDataTable(list);
    }

    /**
     * 导出取样化验列表
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:export')")
    @Log(title = "取样化验", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出取样化验列表接口")
    public void export(HttpServletResponse response, BaSampleAssay baSampleAssay) throws IOException
    {
        //租户ID
        baSampleAssay.setTenantId(SecurityUtils.getCurrComId());
        List<BaSampleAssay> list = baSampleAssayService.selectBaSampleAssayList(baSampleAssay);
        ExcelUtil<BaSampleAssay> util = new ExcelUtil<BaSampleAssay>(BaSampleAssay.class);
        util.exportExcel(response, list, "sampleAssay");
    }

    /**
     * 获取取样化验详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:sampleAssay:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取取样化验详细信息接口")
    @Anonymous
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baSampleAssayService.selectBaSampleAssayById(id));
    }

    /**
     * 新增取样
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:add')")
    @Log(title = "取样化验", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增取样接口")
    public AjaxResult add(@RequestBody BaSampleAssay baSampleAssay)
    {
        int result = baSampleAssayService.insertBaSampleAssay(baSampleAssay);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改取样
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:edit')")
    @Log(title = "取样化验", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改取样接口")
    public AjaxResult edit(@RequestBody BaSampleAssay baSampleAssay)
    {
        int result = baSampleAssayService.updateBaSampleAssay(baSampleAssay);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 新增化验
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:addAssay')")
    @Log(title = "取样化验", businessType = BusinessType.INSERT)
    @PostMapping("/addAssay")
    @ApiOperation(value = "新增取样接口")
    public AjaxResult addAssay(@RequestBody BaSampleAssay baSampleAssay)
    {
        int result = baSampleAssayService.insertAssay(baSampleAssay);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"已存在化验信息");
        }
        return toAjax(result);
    }

    /**
     * H5新增化验
     */
    @PostMapping("/moveAddAssay")
    @ApiOperation(value = "新增取样接口")
    @Anonymous
    public AjaxResult moveAddAssay(@RequestBody BaSampleAssay baSampleAssay)
    {
        int result = baSampleAssayService.insertAssay(baSampleAssay);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改化验
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:editAssay')")
    @Log(title = "取样化验", businessType = BusinessType.UPDATE)
    @PostMapping("/editAssay")
    @ApiOperation(value = "修改取样接口")
    public AjaxResult editAssay(@RequestBody BaSampleAssay baSampleAssay)
    {
        int result = baSampleAssayService.updateAssay(baSampleAssay);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除取样化验
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:remove')")
    @Log(title = "取样化验", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除取样化验接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baSampleAssayService.deleteBaSampleAssayByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baSampleAssayService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 化验撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:sampleAssay:revokeAssay')")
    @GetMapping(value = "/revokeAssay/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revokeAssay(@PathVariable("id") String id) {
        Integer result = baSampleAssayService.revokeAssay(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }


}
