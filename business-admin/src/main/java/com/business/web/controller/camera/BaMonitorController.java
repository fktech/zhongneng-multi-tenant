package com.business.web.controller.camera;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaMonitor;
import com.business.system.service.IBaMonitorService;

/**
 * 监控Controller
 *
 * @author single
 * @date 2024-06-04
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baMonitor")
@Api(tags ="监控接口")
public class BaMonitorController extends BaseController
{
    @Autowired
    private IBaMonitorService baMonitorService;

    /**
     * 查询监控列表
     */
    //PreAuthorize("@ss.hasPermi('admin:baMonitor:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询监控列表接口")
    public TableDataInfo list(BaMonitor baMonitor)
    {
        baMonitor.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaMonitor> list = baMonitorService.selectBaMonitorList(baMonitor);
        return getDataTable(list);
    }

    /**
     * 导出监控列表
     */
    @PreAuthorize("@ss.hasPermi('admin:baMonitor:export')")
    @Log(title = "监控", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出监控列表接口")
    public void export(HttpServletResponse response, BaMonitor baMonitor) throws IOException
    {
        List<BaMonitor> list = baMonitorService.selectBaMonitorList(baMonitor);
        ExcelUtil<BaMonitor> util = new ExcelUtil<BaMonitor>(BaMonitor.class);
        util.exportExcel(response, list, "baMonitor");
    }

    /**
     * 获取监控详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:baMonitor:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取监控详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baMonitorService.selectBaMonitorById(id));
    }

    /**
     * 新增监控
     */
    @PreAuthorize("@ss.hasPermi('admin:baMonitor:add')")
    @Log(title = "监控", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增监控接口")
    public AjaxResult add(@RequestBody BaMonitor baMonitor)
    {
        int result = baMonitorService.insertBaMonitor(baMonitor);
        if(result == -1){
            return AjaxResult.error(-1,"摄像头已存在");
        }
        return toAjax(result);
    }

    /**
     * 修改监控
     */
    @PreAuthorize("@ss.hasPermi('admin:baMonitor:edit')")
    @Log(title = "监控", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改监控接口")
    public AjaxResult edit(@RequestBody BaMonitor baMonitor)
    {
        return toAjax(baMonitorService.updateBaMonitor(baMonitor));
    }

    /**
     * 删除监控
     */
    @PreAuthorize("@ss.hasPermi('admin:baMonitor:remove')")
    @Log(title = "监控", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除监控接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baMonitorService.deleteBaMonitorByIds(ids));
    }
}
