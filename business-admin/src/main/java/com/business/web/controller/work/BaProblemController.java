package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaProblem;
import com.business.system.service.IBaProblemService;

/**
 * 问题Controller
 *
 * @author single
 * @date 2023-10-09
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/problem")
@Api(tags ="问题接口")
public class BaProblemController extends BaseController
{
    @Autowired
    private IBaProblemService baProblemService;

    /**
     * 查询问题列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询问题列表接口")
    public TableDataInfo list(BaProblem baProblem)
    {
        startPage();
        List<BaProblem> list = baProblemService.selectBaProblemList(baProblem);
        return getDataTable(list);
    }

    /**
     * 导出问题列表
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:export')")
    @Log(title = "问题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出问题列表接口")
    public void export(HttpServletResponse response, BaProblem baProblem) throws IOException
    {
        List<BaProblem> list = baProblemService.selectBaProblemList(baProblem);
        ExcelUtil<BaProblem> util = new ExcelUtil<BaProblem>(BaProblem.class);
        util.exportExcel(response, list, "problem");
    }

    /**
     * 获取问题详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取问题详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baProblemService.selectBaProblemById(id));
    }

    /**
     * 以合同启动获取问题详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:problem:add')")
    @GetMapping(value = "/start")
    @ApiOperation(value = "以合同启动获取问题详细信息")
    public AjaxResult start(String startId)
    {
        return AjaxResult.success(baProblemService.selectBaProblemByStartId(startId));
    }

    /**
     * 新增问题
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:add')")
    @Log(title = "问题", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增问题接口")
    public AjaxResult add(@RequestBody BaProblem baProblem)
    {
        return toAjax(baProblemService.insertBaProblem(baProblem));
    }

    /**
     * 修改问题
     */
    //@PreAuthorize("@ss.hasPermi('admin:problem:add')")
    @Log(title = "问题", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改问题接口")
    public AjaxResult edit(@RequestBody BaProblem baProblem)
    {
        return toAjax(baProblemService.updateBaProblem(baProblem));
    }

    /**
     * 删除问题
     */
    @PreAuthorize("@ss.hasPermi('admin:problem:remove')")
    @Log(title = "问题", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除问题接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baProblemService.deleteBaProblemByIds(ids));
    }
}
