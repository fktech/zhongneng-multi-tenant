package com.business.web.controller.work;

import com.business.common.annotation.Anonymous;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaMaterialStock;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.BaTransportAutomobile;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.DepotHeadDTO;
import com.business.system.domain.vo.BaMasterialStockVO;
import com.business.system.domain.vo.BaOngoingContractStartVO;
import com.business.system.service.*;
import com.business.system.service.workflow.IBaDriveAiService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 仓库智慧驾驶舱接口Controller
 *
 * @author js
 * @date 2024-6-18
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/depotDriveAi")
@Api(tags = "仓库智慧驾驶舱接口")
public class BaDepotDriveAiController extends BaseController {

    @Autowired
    private IBaDepotHeadService baDepotHeadService;

    @Autowired
    private IBaTransportAutomobileService baTransportAutomobileService;

    @Autowired
    private IBaShippingOrderService baShippingOrderService;

    @Autowired
    private IBaMaterialStockService baMaterialStockService;

    /**
     * 今日入库量统计
     */
    @GetMapping("/inDepotHeadStat")
    @ApiOperation(value = "今日入库量统计")
    public AjaxResult inDepotHeadStat(){
        DepotHeadDTO depotHeadDTO = new DepotHeadDTO();
        depotHeadDTO.setType(new Long(1));
        return AjaxResult.success(baDepotHeadService.depotHeadStat(depotHeadDTO));
    }

    /**
     * 今日出库量统计
     */
    @GetMapping("/outDepotHeadStat")
    @ApiOperation(value = "今日出库量统计")
    public AjaxResult outDepotHeadStat(){
        DepotHeadDTO depotHeadDTO = new DepotHeadDTO();
        depotHeadDTO.setType(new Long(2));
        return AjaxResult.success(baDepotHeadService.depotHeadStat(depotHeadDTO));
    }

    /**
     * 今日货源单统计
     */
    @PostMapping("/transportAutomobileStat")
    @ApiOperation(value = "今日货源单统计")
    public AjaxResult transportAutomobileStat(@RequestBody BaTransportAutomobile baTransportAutomobile){
        return AjaxResult.success(baTransportAutomobileService.transportAutomobileStat(baTransportAutomobile));
    }

    /**
     * 今日运单统计
     */
    @PostMapping("/shippingOrderStat")
    @ApiOperation(value = "今日运单统计")
    public AjaxResult shippingOrderStat(@RequestBody BaShippingOrder baShippingOrder){
        return AjaxResult.success(baShippingOrderService.shippingOrderStat(baShippingOrder));
    }

    /**
     * 商品库存统计
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @PostMapping("/sumGoodsLocationList")
    @ApiOperation(value = "商品库存统计")
    public AjaxResult sumGoodsLocationList(@RequestBody BaMaterialStock baMaterialStock)
    {
        return AjaxResult.success(baMaterialStockService.sumGoodsLocationList(baMaterialStock));
    }

    /**
     * 获取商品数量
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @PostMapping("/countGoods")
    @ApiOperation(value = "获取商品数量")
    public AjaxResult countGoods(@RequestBody BaMaterialStock baMaterialStock){
        //租户ID
        baMaterialStock.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(baMaterialStockService.countGoods(baMaterialStock));
    }

    /**
     * 各品类入库量累计占比
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @PostMapping("/sumGoodsInDepotList")
    @ApiOperation(value = "各品类入库量累计占比")
    public AjaxResult sumGoodsInDepotList(@RequestBody BaMaterialStock baMaterialStock)
    {
        return AjaxResult.success(baMaterialStockService.sumGoodsDepotList(baMaterialStock));
    }

    /**
     * 各品类出库量累计占比
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @PostMapping("/sumGoodsOutDepotList")
    @ApiOperation(value = "各品类出库量累计占比")
    public AjaxResult sumGoodsOutDepotList(@RequestBody BaMaterialStock baMaterialStock)
    {
        return AjaxResult.success(baMaterialStockService.sumGoodsDepotList(baMaterialStock));
    }

    /**
     * 获取有商品的仓库
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @PostMapping("/depotList")
    @ApiOperation(value = "获取有商品的仓库")
    public AjaxResult depotList(){
        //租户ID
        BaMaterialStock baMaterialStock = new BaMaterialStock();
        baMaterialStock.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(baMaterialStockService.depotList(baMaterialStock));
    }
}

