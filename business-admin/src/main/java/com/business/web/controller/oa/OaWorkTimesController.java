package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaWorkTimes;
import com.business.system.service.IOaWorkTimesService;

/**
 * 班次Controller
 *
 * @author single
 * @date 2023-11-24
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/workTimes")
@Api(tags ="班次接口")
public class OaWorkTimesController extends BaseController
{
    @Autowired
    private IOaWorkTimesService oaWorkTimesService;

    /**
     * 查询班次列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询班次列表接口")
    public TableDataInfo list(OaWorkTimes oaWorkTimes)
    {
        //租户ID
        oaWorkTimes.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaWorkTimes> list = oaWorkTimesService.selectOaWorkTimesList(oaWorkTimes);
        return getDataTable(list);
    }

    /**
     * 导出班次列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:export')")
    @Log(title = "班次", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出班次列表接口")
    public void export(HttpServletResponse response, OaWorkTimes oaWorkTimes) throws IOException
    {
        List<OaWorkTimes> list = oaWorkTimesService.selectOaWorkTimesList(oaWorkTimes);
        ExcelUtil<OaWorkTimes> util = new ExcelUtil<OaWorkTimes>(OaWorkTimes.class);
        util.exportExcel(response, list, "workTimes");
    }

    /**
     * 获取班次详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取班次详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaWorkTimesService.selectOaWorkTimesById(id));
    }

    /**
     * 新增班次
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:add')")
    @Log(title = "班次", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增班次接口")
    public AjaxResult add(@RequestBody OaWorkTimes oaWorkTimes)
    {
        int result = oaWorkTimesService.insertOaWorkTimes(oaWorkTimes);
        if(result == -1){
            return AjaxResult.error(-1,"班次名称不能重复");
        }else if(result == 0){
            return AjaxResult.error(0,"班次数据同步钉钉失败，请联系管理员");
        }
        return toAjax(result);
    }

    /**
     * 修改班次
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:edit')")
    @Log(title = "班次", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改班次接口")
    public AjaxResult edit(@RequestBody OaWorkTimes oaWorkTimes)
    {
        int result = oaWorkTimesService.updateOaWorkTimes(oaWorkTimes);
        if(result == -1){
            return AjaxResult.error(-1,"班次名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 删除班次
     */
    //@PreAuthorize("@ss.hasPermi('admin:workTimes:remove')")
    @Log(title = "班次", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除班次接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        int result = oaWorkTimesService.deleteOaWorkTimesByIds(ids);
        if(result == -1){
            return AjaxResult.error(-1,"已绑定考勤组禁止删除");
        }
        return toAjax(result);
    }
}
