package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.CountBaProjectDTO;
import com.business.system.domain.vo.BaMasterialStockVO;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaMaterialStock;
import com.business.system.service.IBaMaterialStockService;

/**
 * 库存Controller
 *
 * @author single
 * @date 2023-01-05
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/stock")
@Api(tags ="库存接口")
public class BaMaterialStockController extends BaseController
{
    @Autowired
    private IBaMaterialStockService baMaterialStockService;

    /**
     * 查询库存列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询库存列表接口")
    public TableDataInfo list(BaMaterialStock baMaterialStock)
    {
        startPage();
        List<BaMaterialStock> list = baMaterialStockService.selectBaMaterialStockList(baMaterialStock);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */
    @PreAuthorize("@ss.hasPermi('admin:stock:export')")
    @Log(title = "库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出库存列表接口")
    public void export(HttpServletResponse response, BaMaterialStock baMaterialStock) throws IOException
    {
        List<BaMaterialStock> list = baMaterialStockService.selectBaMaterialStockList(baMaterialStock);
        ExcelUtil<BaMaterialStock> util = new ExcelUtil<BaMaterialStock>(BaMaterialStock.class);
        util.exportExcel(response, list, "stock");
    }

    /**
     * 获取库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:stock:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取库存详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baMaterialStockService.selectBaMaterialStockById(id));
    }

    /**
     * 新增库存
     */
    @PreAuthorize("@ss.hasPermi('admin:stock:add')")
    @Log(title = "库存", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增库存接口")
    public AjaxResult add(@RequestBody BaMaterialStock baMaterialStock)
    {
        return toAjax(baMaterialStockService.insertBaMaterialStock(baMaterialStock));
    }

    /**
     * 修改库存
     */
    @PreAuthorize("@ss.hasPermi('admin:stock:edit')")
    @Log(title = "库存", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改库存接口")
    public AjaxResult edit(@RequestBody BaMaterialStock baMaterialStock)
    {
        return toAjax(baMaterialStockService.updateBaMaterialStock(baMaterialStock));
    }

    /**
     * 删除库存
     */
    @PreAuthorize("@ss.hasPermi('admin:stock:remove')")
    @Log(title = "库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除库存接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baMaterialStockService.deleteBaMaterialStockByIds(ids));
    }

    /**
     * 根据商品查询库存仓库及货位列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/depotList")
    @ApiOperation(value = "根据商品查询库存仓库及货位列表")
    public TableDataInfo depotList(BaMaterialStock baMaterialStock)
    {
        return getDataTable(baMaterialStockService.selectDepotList(baMaterialStock));
    }

    /**
     * 根据仓库、库位查询库存
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/getListByDepotAndLocation")
    @ApiOperation(value = "根据仓库、库位查询库存")
    public AjaxResult getListByDepotAndLocation(BaMaterialStock baMaterialStock)
    {
        return AjaxResult.success(baMaterialStockService.getListByDepotAndLocation(baMaterialStock));
    }

    /**
     * 统计库存
     */
    @GetMapping("/countBaMaterialStockList")
    @ApiOperation(value = "统计库存接口")
    public AjaxResult countBaMaterialStockList(BaMaterialStock baMaterialStock)
    {
        return AjaxResult.success(baMaterialStockService.selectBaMaterialStockGoodsList(baMaterialStock));
    }

    /**
     * 库存统计
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/sumList")
    @ApiOperation(value = "库存统计")
    public TableDataInfo sumList(BaMaterialStock baMaterialStock)
    {
        //租户ID
        baMaterialStock.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaMasterialStockVO> list = baMaterialStockService.sumList(baMaterialStock);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 项目库存统计
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/sumProjectList")
    @ApiOperation(value = "项目库存统计")
    public TableDataInfo sumProjectList(BaMaterialStock baMaterialStock)
    {
        startPage();
        List<BaMasterialStockVO> list = baMaterialStockService.sumProjectList(baMaterialStock);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 仓位库存统计
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/sumLocationList")
    @ApiOperation(value = "仓位库存统计")
    public TableDataInfo sumLocationList(BaMaterialStock baMaterialStock)
    {
        startPage();
        List<BaMasterialStockVO> list = baMaterialStockService.sumLocationList(baMaterialStock);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 商品库存统计
     */
    //@PreAuthorize("@ss.hasPermi('admin:stock:list')")
    @GetMapping("/sumGoodsList")
    @ApiOperation(value = "商品库存统计")
    public TableDataInfo sumGoodsList(BaMaterialStock baMaterialStock)
    {
        startPage();
        List<BaMasterialStockVO> list = baMaterialStockService.sumGoodsList(baMaterialStock);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }
}
