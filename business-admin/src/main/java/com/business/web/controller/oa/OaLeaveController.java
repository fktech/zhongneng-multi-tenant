package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaLeave;
import com.business.system.service.IOaLeaveService;

/**
 * 请假申请Controller
 *
 * @author ljb
 * @date 2023-11-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/oaLeave")
@Api(tags ="请假申请接口")
public class OaLeaveController extends BaseController
{
    @Autowired
    private IOaLeaveService oaLeaveService;

    /**
     * 查询请假申请列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaLeave:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询请假申请列表接口")
    public TableDataInfo list(OaLeave oaLeave)
    {
        //租户ID
        oaLeave.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaLeave> list = oaLeaveService.selectOaLeaveList(oaLeave);
        return getDataTable(list);
    }

    /**
     * 导出请假申请列表
     */
    @PreAuthorize("@ss.hasPermi('admin:oaLeave:export')")
    @Log(title = "请假申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出请假申请列表接口")
    public void export(HttpServletResponse response, OaLeave oaLeave) throws IOException
    {
        //租户ID
        oaLeave.setTenantId(SecurityUtils.getCurrComId());
        List<OaLeave> list = oaLeaveService.selectOaLeaveList(oaLeave);
        ExcelUtil<OaLeave> util = new ExcelUtil<OaLeave>(OaLeave.class);
        util.exportExcel(response, list, "oaLeave");
    }

    /**
     * 获取请假申请详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaLeave:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取请假申请详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaLeaveService.selectOaLeaveById(id));
    }

    /**
     * 新增请假申请
     */
   /* @PreAuthorize("@ss.hasPermi('admin:oaLeave:add')")
    @Log(title = "请假申请", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增请假申请接口")
    public AjaxResult add(@RequestBody OaLeave oaLeave)
    {
        int result = oaLeaveService.insertOaLeave(oaLeave);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改请假申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaLeave:edit')")
    @Log(title = "请假申请", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改请假申请接口")
    public AjaxResult edit(@RequestBody OaLeave oaLeave)
    {
        int result = oaLeaveService.updateOaLeave(oaLeave);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除请假申请
     */
    /*@PreAuthorize("@ss.hasPermi('admin:oaLeave:remove')")
    @Log(title = "请假申请", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除请假申请接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaLeaveService.deleteOaLeaveByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaLeave:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaLeaveService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
