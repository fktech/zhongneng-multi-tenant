package com.business.web.controller.workflow;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.BeanUtil;
import com.business.system.mapper.BaProcessInstanceRelatedMapper;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.util.ProcessMapUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: js
 * @Description: 业务与流程实例关联前端控制类
 * @date: 2023-2-28 16:36:18
 */
@RestController
@RequestMapping("/workflow/instance")
@Api(tags = "业务与流程实例关联接口")
public class BaProcessInstanceRelatedController extends BaseController {

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ProcessMapUtil mapUtil;

    @Autowired
    private BeanUtil beanUtil;

    @ApiOperation("根据流程实例ID查询业务数据")
    @GetMapping("/getProcessInstanceBusinessData/{processInstanceId}")
    public AjaxResult getProcessInstanceBusinessData(@PathVariable("processInstanceId") String processInstanceId) {
        return success(iBaProcessBusinessInstanceRelatedService.getApplyBusinessDataByProcessInstanceId(processInstanceId));
    }
}
