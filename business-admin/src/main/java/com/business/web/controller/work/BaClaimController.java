package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaOrder;
import com.business.system.domain.dto.BaOrderDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaClaim;
import com.business.system.service.IBaClaimService;

/**
 * 认领Controller
 *
 * @author ljb
 * @date 2023-01-31
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/claim")
@Api(tags ="认领接口")
public class BaClaimController extends BaseController
{
    @Autowired
    private IBaClaimService baClaimService;

    /**
     * 查询认领列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:claim:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询认领列表接口")
    public TableDataInfo list(BaClaim baClaim)
    {
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaClaim> list = baClaimService.selectBaClaimList(baClaim);
        return getDataTable(list);
    }

    /*@GetMapping("/orderList")
    @ApiOperation(value = "查询订单列表接口")
    public TableDataInfo orderList()
    {
        List<BaOrderDTO> list = baClaimService.baOrderList();
        return getDataTable(list);
    }*/

    /**
     * 导出认领列表
     */
    @PreAuthorize("@ss.hasPermi('admin:claim:export')")
    @Log(title = "认领", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出认领列表接口")
    public void export(HttpServletResponse response, BaClaim baClaim) throws IOException
    {
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        List<BaClaim> list = baClaimService.selectBaClaimList(baClaim);
        ExcelUtil<BaClaim> util = new ExcelUtil<BaClaim>(BaClaim.class);
        util.exportExcel(response, list, "claim");
    }

    /**
     * 获取认领详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:claim:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取认领详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baClaimService.selectBaClaimById(id));
    }

    /**
     * 新增认领
     */
    @PreAuthorize("@ss.hasPermi('admin:claim:add')")
    @Log(title = "认领", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增认领接口")
    public AjaxResult add(@RequestBody BaClaim baClaim)
    {
        return toAjax(baClaimService.insertBaClaim(baClaim));
    }

    /**
     * 修改认领
     */
    @PreAuthorize("@ss.hasPermi('admin:claim:edit')")
    @Log(title = "认领", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改认领接口")
    public AjaxResult edit(@RequestBody BaClaim baClaim)
    {
        return toAjax(baClaimService.updateBaClaim(baClaim));
    }

    /**
     * 删除认领
     */
    @PreAuthorize("@ss.hasPermi('admin:claim:remove')")
    @Log(title = "认领", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除认领接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baClaimService.deleteBaClaimByIds(ids));
    }
}
