package com.business.web.controller.work;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.ProjectBox;
import com.business.system.domain.vo.BaProjectVO;
import com.business.system.mapper.BaProjectMapper;
import com.business.system.service.IBaHiProjectService;
import com.business.system.service.IBaProjectLinkService;
import com.business.system.service.ISysDictDataService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.service.IBaProjectService;

/**
 * 立项管理Controller
 *
 * @author ljb
 * @date 2022-12-02
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/project")
@Api(tags ="立项管理接口")
public class BaProjectController extends BaseController
{
    @Autowired
    private IBaProjectService baProjectService;

    @Autowired
    private IBaProjectLinkService baProjectLinkService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private IBaHiProjectService baHiProjectService;

    /**
     * 查询立项管理列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询立项管理列表接口")
    public TableDataInfo list(BaProject baProject)
    {
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        //查询总条数
        //List<BaProject> list1 = baProjectService.selectBaProjectList(baProject);
        startPage();
        List<BaProject> list = baProjectService.selectBaProjectList(baProject);
        //List<BaProject> collect = list.stream().sorted(Comparator.comparing(BaProject::getCreateTime).reversed()).collect(Collectors.toList());
        //list对象排序
        /*List<BaProject> collect = list.stream().sorted(Comparator.comparing(BaProject::getCreateTime).reversed()).collect(Collectors.toList());
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        TableDataInfo rspData =new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(myStartPage(collect, pageNum, pageSize));
        rspData.setTotal(new PageInfo(list).getTotal());*/

        return getDataTable(list);
    }

    //列表分页
    public static List myStartPage(List list, Integer pageNum, Integer pageSize){
        List<Object> objects = new ArrayList<>();
        if(list ==null){
            return objects;
        }
        if(list.size()==0){
            return objects;
        }
        Integer count = list.size();//
        Integer pageCount =0;//
        if(count % pageSize ==0){
            pageCount = count / pageSize;
        }else{
            pageCount = count / pageSize +1;
        }
        int fromIndex =0;//
        int toIndex =0;//
        if(pageNum != pageCount){
            fromIndex =(pageNum -1)* pageSize;
            toIndex = fromIndex + pageSize;
        }else{
            fromIndex =(pageNum -1)* pageSize;
            toIndex = count;
        }
        List pageList = list.subList(fromIndex,toIndex);
        return pageList;
    }

    /**
     * 草稿箱
     * @param baProject
     * @return
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:draftList')")
    @GetMapping("/draftList")
    @ApiOperation(value = "查询草稿箱列表接口")
    public TableDataInfo draftList(BaProject baProject)
    {
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProject> list = baProjectService.selectDraft(baProject);
        return getDataTable(list);
    }

    /**
     * 导出立项管理列表
     */
    @PreAuthorize("@ss.hasPermi('admin:project:export')")
    @Log(title = "立项管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出立项管理列表接口")
    public void export(HttpServletResponse response, BaProject baProject) throws IOException
    {
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        List<BaProject> list = baProjectService.selectBaProjectList(baProject);
        ExcelUtil<BaProject> util = new ExcelUtil<BaProject>(BaProject.class);
        SimpleDateFormat dateFormat =new SimpleDateFormat("yyyy-MM-dd");
        String format = dateFormat.format(new Date());
        util.exportExcel(response, list, "进行中项目"+format);
    }

    /**
     * 获取立项管理详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:project:query')")*/
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取立项管理详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baProjectService.selectBaProjectById(id));
    }

    /**
     * 新增立项管理
     */
    @PreAuthorize("@ss.hasPermi('admin:project:add')")
    @Log(title = "立项管理", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增立项管理接口")
    public AjaxResult add(@RequestBody BaProject baProject)
    {
        Integer result = baProjectService.insertBaProject(baProject);
        if(result == -1){
            return AjaxResult.error(-1,"项目简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"项目名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 提交绑定审批流程
     */
    @PreAuthorize("@ss.hasPermi('admin:project:submit')")
    @PostMapping("/submit")
    @ApiOperation(value = "提交立项信息")
    public AjaxResult submit(@RequestBody BaProject baProject)
    {
       Integer result = baProjectService.submit(baProject);
       if(result == 0){
           return AjaxResult.error(0,"流程实例启动失败");
       }else if(result == -1){
           return AjaxResult.error(-1,"立项简称不能重复");
       }else if(result == -2){
           return AjaxResult.error(-2,"立项名称不能重复");
       }
        return toAjax(result);
    }


    /**
     * 修改立项管理
     */
    @PreAuthorize("@ss.hasPermi('admin:project:edit')")
    @Log(title = "立项管理", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改立项管理接口")
    public AjaxResult edit(@RequestBody BaProject baProject)
    {
        int result = baProjectService.updateBaProject(baProject);
        if(result == -1){
            return AjaxResult.error(-1,"立项简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"立项名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 删除立项管理
     */
    @PreAuthorize("@ss.hasPermi('admin:project:remove')")
    @Log(title = "立项管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除立项管理接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baProjectService.deleteBaProjectByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:project:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baProjectService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    /**
     * 完成按钮
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('admin:project:complete')")
    @GetMapping(value = "/complete/{id}")
    @ApiOperation(value = "完成按钮")
    public AjaxResult complete(@PathVariable("id") String id){
        Integer result = baProjectService.complete(id);
        if(result == 0){
            return AjaxResult.error(0,"ID不能为空");
        }
        return toAjax(result);
    }

    /**
     * 查询项目进度列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/listProgress")
    @ApiOperation(value = "查询项目进度列表")
    @Anonymous
    public TableDataInfo listProgress(BaProject baProject)
    {
        startPage();
        List<BaProject> list = baProjectService.listProgress(baProject);
        return getDataTable(list);
    }

    /**
     * 查询是否有权限编辑草稿箱
     */
    @PostMapping("/getUpdateAuth")
    @ApiOperation(value = "查询是否有权限编辑草稿箱")
    @Anonymous
    public AjaxResult getUpdateAuth(@RequestBody BaProjectDTO baProjectDTO)
    {
        return AjaxResult.success(baProjectService.getUpdateAuth(baProjectDTO));
    }

    /**
     * 运营首页数据
     */
    @GetMapping("/home")
    @ApiOperation(value = "运营首页立项统计")
    @Anonymous
    public TableDataInfo listProgress(String type){

        List<BaProjectDTO> baProjectDTOS = baProjectService.baProjectDTOList(type);

        return getDataTable(baProjectDTOS);
    }

    /**
     * 业务首页数据
     */
    @GetMapping("/businessHome")
    @ApiOperation(value = "业务首页立项统计")
    @Anonymous
    public TableDataInfo businessHome(String type){

        List<BaProjectDTO> baProjectDTOS = baProjectService.baProjectDTOS(type);

        return getDataTable(baProjectDTOS);
    }

    /**
     * 业务首页合同启动数据
     */
    @GetMapping("/contractStartHome")
    @ApiOperation(value = "业务首页合同启动数据")
    @Anonymous
    public TableDataInfo contractStartHome(BaContractStartDTO startDTO){

        List<ContractStartDTO> contractStartDTOs = baProjectService.contractStart(startDTO);

        return getDataTable(contractStartDTOs);
    }

    @GetMapping("/dataStatistics")
    @ApiOperation(value = "业务总监本月数据查询")
    @Anonymous
    public UR dataStatistics(String type){
        UR ur = baProjectService.dataStatistics(type);
        return ur;
    }

    /**
     * 业务部排名
     */
    @GetMapping("/statistics")
    @ApiOperation(value = "业务部排名")
    @Anonymous
    public TableDataInfo contractStartHome(){

        List<StatisticsDTO> statistics = baProjectService.statistics();

       return getDataTable(statistics);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO) {
        int result = baProjectService.updateProcessBusinessData(baProjectInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"公司简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"公司名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 立项序号
     */
    @ApiOperation("立项序号")
    @GetMapping("/projectSerial")
    public String projectSerial() {

        return baProjectService.projectSerial();
    }

    /**
     * 新增授权
     */
    @PreAuthorize("@ss.hasPermi('admin:project:AddProjectLink')")
    @PostMapping("AddProjectLink")
    @ApiOperation(value = "新增立项授权接口")
    public AjaxResult add(@RequestBody BaProjectLink baProjectLink){

        return toAjax(baProjectService.insertBaProjectLink(baProjectLink));
    }

    /**
     * 立项下拉框
     */
    @GetMapping("/pullDown")
    @ApiOperation(value = "立项下拉框")
    public TableDataInfo pullDown(String type)
    {
        BaProject baProject1 = new BaProject();
        baProject1.setProjectType(type);
        baProject1.setState("projectStatus:pass");
        //租户ID
        baProject1.setTenantId(SecurityUtils.getCurrComId());
        List<BaProject> baProjects = baProjectService.baProjectList(baProject1);
        //授权查询
        BaProjectLink baProjectLink = new BaProjectLink();
        baProjectLink.setDeptId(SecurityUtils.getDeptId().toString());
        List<BaProjectLink> baProjectLinks = baProjectLinkService.selectBaProjectLinkList(baProjectLink);
        for (BaProjectLink baProjectLink1:baProjectLinks) {
            //查询立项
            BaProject baProject = baProjectService.selectBaProjectById(baProjectLink1.getProjectId());
            baProjects.add(baProject);
        }
        //立项集合去重
        List<BaProject> collect = baProjects.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaProject::getId))), ArrayList::new));
        //以提交时间排序
        //List<BaProject> list = collect.stream().sorted(Comparator.comparing(BaProject::getSubmitTime).reversed()).collect(Collectors.toList());
        //以创建时间排序
        List<BaProject> list = collect.stream().sorted(Comparator.comparing(BaProject::getCreateTime).reversed()).collect(Collectors.toList());
        return getDataTable(list);
    }

    @GetMapping(value = "/selectBaProjectLink")
    @ApiOperation(value = "查询授权信息")
    public AjaxResult selectBaProjectLink(BaProjectLink baProjectLink)
    {
        return AjaxResult.success(baProjectService.selectBaProjectLink(baProjectLink));
    }

    /**
     * 运输信息(业务经理首页)
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/transportInformation")
    @ApiOperation(value = "查询运输信息接口(业务经理首页)")
    public List<BaProjectVO> transportInformation(BaProject baProject)
    {
        return baProjectService.transportInformation(baProject);
    }
    /**
     * 运输信息(业务经理首页)
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/ProjectBox")
    @ApiOperation(value = "查询立项级联列表")
    public TableDataInfo  ProjectBox()
    {   //查询立项类型
        List<SysDictData> dictData = sysDictDataService.selectDictDataByType("project_type");
        List<ProjectBox> startComboBoxList = new ArrayList<>();
        for (SysDictData sysDictData:dictData) {
            if(sysDictData.getDictValue().equals("2")||sysDictData.getDictValue().equals("4")){
                ProjectBox projectBox = new ProjectBox();
                /* BaProject baProject1=new BaProject();*/
                QueryWrapper<BaProject> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("state", AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
                transportQueryWrapper.eq("flag",0);
                transportQueryWrapper.eq("project_type",sysDictData.getDictValue());
                transportQueryWrapper.eq("dept_id",SecurityUtils.getDeptId().toString());
                //租户信息
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    transportQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                }
                List<BaProject> baProjects = baProjectMapper.selectList(transportQueryWrapper);
                //授权查询
                BaProjectLink baProjectLink = new BaProjectLink();
                baProjectLink.setDeptId(SecurityUtils.getDeptId().toString());
                //查询立项
                baProjectLink.setProjectType(sysDictData.getDictValue());
                List<BaProjectLink> baProjectLinks = baProjectLinkService.selectBaProjectLinkList(baProjectLink);
                for (BaProjectLink baProjectLink1:baProjectLinks) {

                    BaProject baProject = baProjectService.selectBaProjectById(baProjectLink1.getProjectId());
                    baProjects.add(baProject);
                }
                //立项集合去重
                List<BaProject> collect = baProjects.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaProject::getId))), ArrayList::new));

                if(StringUtils.isNotNull(baProjects)){
                    projectBox.setValue(sysDictData.getDictValue());
                    projectBox.setLabel(sysDictData.getDictLabel());
                    projectBox.setChildren(collect);
                    startComboBoxList.add(projectBox);
                }
            }

        }
        return getDataTable(startComboBoxList);

    }

    /**
     * APP业务首页项目立项
     */
    @GetMapping("/appHomeProjectStat")
    @ApiOperation(value = "APP业务首页项目立项")
    @Anonymous
    public UR appHomeProjectStat(BaProject baProject){
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        UR ur = baProjectService.appHomeProjectStat(baProject);
        return ur;
    }

    /**
     * PC端首页立项统计
     */
    @GetMapping("/countBaProjectList")
    @ApiOperation(value = "PC端首页立项统计")
    public AjaxResult countBaProjectList(CountBaProjectDTO countBaProjectDTO){
        countBaProjectDTO.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(baProjectService.countBaProjectList(countBaProjectDTO));
    }

    /**
     * 变更立项
     */
    @PreAuthorize("@ss.hasPermi('admin:project:change')")
    @Log(title = "变更立项", businessType = BusinessType.UPDATE)
    @PostMapping("/change")
    @ApiOperation(value = "变更立项接口")
    public AjaxResult change(@RequestBody BaProject baProject) throws Exception {
        int result = baProjectService.changeBaProject(baProject);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"立项简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"立项名称不能重复");
        }else if(result == -3){
            return AjaxResult.error(-3,"未进行任何变更");
        }
        return toAjax(result);
    }

    /**
     * 查询立项管理变更历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeList")
    @ApiOperation(value = "查询立项管理变更历史列表")
    public TableDataInfo changeList(BaHiProject baHiProject)
    {
        startPage();
        List<BaHiProject> list = baHiProjectService.selectChangeDataBaHiProjectList(baHiProject);
        return getDataTable(list);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    @GetMapping(value = "/change/{id}")
    @ApiOperation(value = "获取当前数据和上一次变更记录对比数据")
    public List<String> selectChangeDataList(@PathVariable String id)
    {
        return baProjectService.selectChangeDataList(id);
    }

    /**
     * 查询立项管理变更记录列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:enterprise:list')")
    @GetMapping("/changeDataList")
    @ApiOperation(value = "查询立项管理变更记录列表")
    public TableDataInfo changeDataList(BaHiProject baHiProject)
    {
        startPage();
        List<BaHiProject> list = baHiProjectService.selectChangeDataBaHiProjectList(baHiProject);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(new PageInfo(list).getTotal());
        return tableDataInfo;
    }

    /**
     * 获取历史立项管理详细信息
     */
    /*@PreAuthorize("@ss.hasPermi('admin:project:query')")*/
    @GetMapping(value = "/hi/{id}")
    @ApiOperation(value = "获取历史立项管理详细信息")
    public AjaxResult getHiInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baHiProjectService.selectBaHiProjectById(id));
    }

    /**
     * 查询多租户授权信息立项列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息立项列表")
    public TableDataInfo authorizedList(BaProject baProject)
    {
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProject> list = baProjectService.selectBaProjectAuthorizedList(baProject);

        return getDataTable(list);
    }

    /**
     * 授权立项管理
     */
    @PreAuthorize("@ss.hasPermi('admin:project:authorized')")
    @Log(title = "立项管理", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权立项管理")
    public AjaxResult authorized(@RequestBody BaProject baProject)
    {
        return AjaxResult.success(baProjectService.authorizedBaProject(baProject));
    }

}
