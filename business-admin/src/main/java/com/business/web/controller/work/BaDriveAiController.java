package com.business.web.controller.work;

import com.business.common.annotation.Anonymous;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.*;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.OngoingcontractsDto;
import com.business.system.domain.dto.RolenumDTO;
import com.business.system.domain.vo.BaEnter;
import com.business.system.domain.vo.BaOngoingContractStartVO;
import com.business.system.service.*;
import com.business.system.service.workflow.IBaDriveAiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 智慧驾驶舱接口Controller
 *
 * @author ljb
 * @date 2022-12-02
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/driveAi")
@Api(tags = "智慧驾驶舱接口")
public class BaDriveAiController extends BaseController {
    @Autowired
    private IBaProjectService baProjectService;
    @Autowired
    private ISysDictDataService sysDictDataService;
    @Autowired
    private IBaDriveAiService iBaDriveAiService;
    @Autowired
    private IBaEnterpriseService iBaEnterpriseService;
    @Autowired
    private IBaContractStartService iBaContractStartService;
    @Autowired
    private IBaTransportService iBaTransportService;
    @Autowired
    private IBaCheckService iBaCheckService;
    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private ISysRoleService iSysRoleService;
    @Autowired
    private ISysDeptService iSysDeptService;
    @Autowired
    private IBaClaimService iBaClaimService;
    @Autowired
    private IBaClaimHistoryService iBaClaimHistoryService;
    @Autowired
    private IBaCollectionService iBaCollectionService;

    /**
     * 客商统计
     *
     * @return
     */
    @GetMapping("/counts")
    @ApiOperation(value = "客商统计")
    public UR counts() {
        UR ur = iBaDriveAiService.counts();
        return ur;
    }

    /**
     * 查询业务类型占比接口
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/countContractStartListByType")
    @ApiOperation(value = "查询业务类型占比接口")
    public AjaxResult countContractStartListByType() {
        return AjaxResult.success(iBaContractStartService.countContractStartListByType());
    }

    /**
     * 终端发运排名
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/selectEnterpriseRanking")
    @ApiOperation(value = "终端发运排名")
    public AjaxResult selectEnterpriseRanking() {
        return AjaxResult.success(iBaEnterpriseService.countRanking());
    }

    /**
     * 组织架构信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:project:list')")
    @GetMapping("/countUser")
    @ApiOperation(value = "组织架构信息")
    public AjaxResult countUser() {
        return AjaxResult.success(iSysUserService.countUser());
    }

    /**
     * 事业部排名
     */
    @GetMapping("/departmentRanking")
    @ApiOperation("事业部排名")
    @Anonymous
    public AjaxResult departmentRanking(Long deptId, String type){
        return AjaxResult.success(iBaDriveAiService.departmentRanking(deptId, type));
    }

    /**
     * 进行中的业务
     */
    @GetMapping("/selectOngoingBusiness")
    @ApiOperation(value = "进行中的业务")
    public TableDataInfo selectOngoingBusiness(BaContractStartDTO baContractStartDTO){
        startPage();
        List<BaOngoingContractStartVO> list = iBaDriveAiService.contractStartList(baContractStartDTO);
        return getDataTable(list);
    }
}

