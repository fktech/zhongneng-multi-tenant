package com.business.web.controller.work;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaContract;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaDeclare;
import com.business.system.mapper.BaDeclareMapper;
import com.business.system.service.IBaDeclareService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 计划申报Controller
 *
 * @author js
 * @date 2023-03-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/declare")
@Api(tags ="计划申报接口")
public class BaDeclareController extends BaseController
{
    @Autowired
    private IBaDeclareService baDeclareService;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    /**
     * 查询计划申报列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:declare:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询计划申报列表接口")
    public TableDataInfo list(BaDeclare baDeclare)
    {
        //租户ID
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDeclare> list = baDeclareService.selectBaDeclareList(baDeclare);
        return getDataTable(list);
    }

    /**
     * 导出计划申报列表
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:export')")
    @Log(title = "计划申报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出计划申报列表接口")
    public void export(HttpServletResponse response, BaDeclare baDeclare) throws IOException
    {
        List<BaDeclare> list = baDeclareService.selectBaDeclareList(baDeclare);
        ExcelUtil<BaDeclare> util = new ExcelUtil<BaDeclare>(BaDeclare.class);
        util.exportExcel(response, list, "declare");
    }

    /**
     * 获取计划申报详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:declare:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取计划申报详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDeclareService.selectBaDeclareById(id));
    }

    /**
     * 新增计划申报
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:add')")
    @Log(title = "计划申报", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增计划申报接口")
    public AjaxResult add(@RequestBody BaDeclare baDeclare)
    {
        int result = baDeclareService.insertBaDeclare(baDeclare);
        if(result == -1){
            return AjaxResult.error(-1,"此时间段内本合同启动已添加计划申报，请勿重复操作");
        }
        return toAjax(result);
    }


    /**
     * 修改计划申报
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:edit')")
    @Log(title = "计划申报", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改计划申报接口")
    public AjaxResult edit(@RequestBody BaDeclare baDeclare)
    {
        int result = baDeclareService.updateBaDeclare(baDeclare);
        if(result == -1){
            return AjaxResult.error(-1,"此时间段内本合同启动已添加计划申报，请勿重复操作");
        }
        return toAjax(result);
    }

    /**
     * 删除计划申报
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:remove')")
    @Log(title = "计划申报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除计划申报接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baDeclareService.deleteBaDeclareByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baDeclareService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤回流程实例失败");
        }

        return toAjax(result);
    }

    /**
     * 查询本月数据
     */
    @GetMapping(value = "thisMonth")
    @ApiOperation(value = "查询本月数据")
    @Anonymous
    public UR thisMonth(){
        UR ur = baDeclareService.thisMonth();
        return ur;
    }

    /**
     * 查询本月数据信息
     */
    @GetMapping("/thisMonthList")
    @ApiOperation(value = "查询本月数据信息")
    @Anonymous
    public TableDataInfo thisMonthList(BaDeclare baDeclare)
    {
        baDeclare.setThisMonth("本月");
        baDeclare.setState("declare:pass");
        List<BaDeclare> list = baDeclareService.selectBaDeclareList(baDeclare);
        return getDataTable(list);
    }

    /**
     * 查询多租户授权信息计划申报列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:declare:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息计划申报列表")
    public TableDataInfo authorizedList(BaDeclare baDeclare)
    {
        //租户ID
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaDeclare> list = baDeclareService.selectBaDeclareAuthorizedList(baDeclare);
        return getDataTable(list);
    }

    /**
     * 授权计划申报
     */
    @PreAuthorize("@ss.hasPermi('admin:declare:authorized')")
    @Log(title = "计划申报", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权计划申报")
    public AjaxResult authorized(@RequestBody BaDeclare baDeclare)
    {
        return AjaxResult.success(baDeclareService.authorizedBaDeclare(baDeclare));
    }
}
