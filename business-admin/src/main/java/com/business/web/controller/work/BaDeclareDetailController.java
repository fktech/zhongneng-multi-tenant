package com.business.web.controller.work;

import com.business.system.domain.BaDeclareDetail;
import com.business.system.service.IBaDeclareDetailService;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 计划申报详情Controller
 *
 * @author js
 * @date 2023-03-21
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/declare/detail")
@Api(tags ="计划申报详情接口")
public class BaDeclareDetailController extends BaseController
{
    @Autowired
    private IBaDeclareDetailService baDeclareDetailService;

    /**
     * 查询计划申报详情列表
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询计划申报详情列表接口")
    public TableDataInfo list(BaDeclareDetail baDeclareDetail)
    {
        startPage();
        List<BaDeclareDetail> list = baDeclareDetailService.selectBaDeclareDetailList(baDeclareDetail);
        return getDataTable(list);
    }

    /**
     * 导出计划申报详情列表
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:export')")
    @Log(title = "计划申报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出计划申报列表详情接口")
    public void export(HttpServletResponse response, BaDeclareDetail baDeclareDetail) throws IOException
    {
        List<BaDeclareDetail> list = baDeclareDetailService.selectBaDeclareDetailList(baDeclareDetail);
        ExcelUtil<BaDeclareDetail> util = new ExcelUtil<BaDeclareDetail>(BaDeclareDetail.class);
        util.exportExcel(response, list, "detail");
    }

    /**
     * 获取计划申报详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取计划申报详情详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baDeclareDetailService.selectBaDeclareDetailById(id));
    }

    /**
     * 新增计划申报详情
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:add')")
    @Log(title = "计划申报", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增计划申报详情接口")
    public AjaxResult add(@RequestBody BaDeclareDetail baDeclareDetail)
    {
        return toAjax(baDeclareDetailService.insertBaDeclareDetail(baDeclareDetail));
    }

    /**
     * 修改计划申报详情
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:edit')")
    @Log(title = "计划申报", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改计划申报详情接口")
    public AjaxResult edit(@RequestBody BaDeclareDetail baDeclareDetail)
    {
        return toAjax(baDeclareDetailService.updateBaDeclareDetail(baDeclareDetail));
    }

    /**
     * 删除计划申报详情
     */
    @PreAuthorize("@ss.hasPermi('admin:detail:remove')")
    @Log(title = "计划申报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除计划申报详情接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        int i = baDeclareDetailService.deleteBaDeclareDetailByIds(ids);
        System.out.println("+++++++++++++++"+i);
        return toAjax(baDeclareDetailService.deleteBaDeclareDetailByIds(ids));
    }
}
