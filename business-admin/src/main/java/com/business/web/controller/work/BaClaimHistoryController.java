package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaClaim;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaClaimHistory;
import com.business.system.service.IBaClaimHistoryService;

/**
 * 认领历史Controller
 *
 * @author ljb
 * @date 2023-01-31
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/history")
@Api(tags ="认领历史接口")
public class BaClaimHistoryController extends BaseController
{
    @Autowired
    private IBaClaimHistoryService baClaimHistoryService;

    /**
     * 查询认领历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:history:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询认领历史列表接口")
    public TableDataInfo list(BaClaimHistory baClaimHistory)
    {
        startPage();
        List<BaClaimHistory> list = baClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
        return getDataTable(list);
    }

    @GetMapping("/claimList")
    @ApiOperation(value = "反查认领记录")
    public TableDataInfo claimList(BaClaimHistory baClaimHistory)
    {
        //租户ID
        baClaimHistory.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaClaim> list = baClaimHistoryService.selectBaClaimList(baClaimHistory);
        TableDataInfo tableDataInfo = getDataTable(list);
        tableDataInfo.setTotal(baClaimHistoryService.countBaClaimHistoryList(baClaimHistory));
        return tableDataInfo;
    }

    /**
     * 导出认领历史列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:history:export')")
    @Log(title = "认领历史", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出认领历史列表接口")
    public void export(HttpServletResponse response, BaClaimHistory baClaimHistory) throws IOException
    {
        //租户ID
        baClaimHistory.setTenantId(SecurityUtils.getCurrComId());
        List<BaClaimHistory> list = baClaimHistoryService.selectBaClaimHistoryList(baClaimHistory);
        ExcelUtil<BaClaimHistory> util = new ExcelUtil<BaClaimHistory>(BaClaimHistory.class);
        util.exportExcel(response, list, "history");
    }

    /**
     * 获取认领历史详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:history:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取认领历史详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baClaimHistoryService.selectBaClaimHistoryById(id));
    }

    /**
     * 新增认领历史
     */
    @PreAuthorize("@ss.hasPermi('admin:history:add')")
    @Log(title = "认领历史", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增认领历史接口")
    public AjaxResult add(@RequestBody BaClaimHistory baClaimHistory)
    {
        return toAjax(baClaimHistoryService.insertBaClaimHistory(baClaimHistory));
    }

    /**
     * 修改认领历史
     */
    @PreAuthorize("@ss.hasPermi('admin:history:edit')")
    @Log(title = "认领历史", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改认领历史接口")
    public AjaxResult edit(@RequestBody BaClaimHistory baClaimHistory)
    {
        return toAjax(baClaimHistoryService.updateBaClaimHistory(baClaimHistory));
    }

    /**
     * 删除认领历史
     */
    @PreAuthorize("@ss.hasPermi('admin:history:remove')")
    @Log(title = "认领历史", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除认领历史接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baClaimHistoryService.deleteBaClaimHistoryByIds(ids));
    }
}
