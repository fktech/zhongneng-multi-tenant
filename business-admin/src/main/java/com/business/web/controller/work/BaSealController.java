package com.business.web.controller.work;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.FileUploadResult;
import com.business.system.service.OssService;
import com.business.system.service.impl.OssServiceImpl;
import com.business.system.util.ExportMyWord;
import com.business.system.util.OssBootUtil;
import com.zhuozhengsoft.pageoffice.FileSaver;
import com.zhuozhengsoft.pageoffice.OpenModeType;
import com.zhuozhengsoft.pageoffice.PageOfficeCtrl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaSeal;
import com.business.system.service.IBaSealService;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * 用印Controller
 *
 * @author ljb
 * @date 2023-03-14
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/BaSeal")
@Api(tags ="用印接口")
public class BaSealController extends BaseController
{
    @Autowired
    private IBaSealService baSealService;

    @Autowired
    private OssService ossService;

    /**
     * 查询用印列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:BaSeal:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询用印列表接口")
    @Anonymous
    public TableDataInfo list(BaSeal baSeal)
    {
        //租户ID
        baSeal.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSeal> list = baSealService.selectBaSealList(baSeal);
        return getDataTable(list);
    }

    /**
     * 导出用印列表
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:export')")
    @Log(title = "用印", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出用印列表接口")
    public void export(HttpServletResponse response, BaSeal baSeal) throws IOException
    {
        //租户ID
        baSeal.setTenantId(SecurityUtils.getCurrComId());
        List<BaSeal> list = baSealService.selectBaSealList(baSeal);
        ExcelUtil<BaSeal> util = new ExcelUtil<BaSeal>(BaSeal.class);
        util.exportExcel(response, list, "BaSeal");
    }

    /**
     * 获取用印详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:BaSeal:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取用印详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id,HttpServletResponse response,HttpServletRequest request)
    {
        BaSeal seal = baSealService.selectBaSealById(id);

        return AjaxResult.success(baSealService.selectBaSealById(id));
    }

    /**
     * 新增用印
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:add')")
    @Log(title = "用印", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增用印接口")
    public AjaxResult add(@RequestBody BaSeal baSeal)
    {
        int result = baSealService.insertBaSeal(baSeal);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 修改用印
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:edit')")
    @Log(title = "用印", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改用印接口")
    public AjaxResult edit(@RequestBody BaSeal baSeal)
    {
        int result = baSealService.updateBaSeal(baSeal);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }
        return toAjax(result);
    }

    /**
     * 删除用印
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:remove')")
    @Log(title = "用印", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除用印接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baSealService.deleteBaSealByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = baSealService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    @RequestMapping(value="/Word")
    @ResponseBody
    @ApiOperation(value = "文件")
    @Anonymous
    public String showWord(HttpServletRequest request,String name) {
        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
        //设置服务页面，/api是前端vue项目的代理地址
        poCtrl.setServerPage("/dev-api/poserver.zz");
        poCtrl.addCustomToolButton("保存", "Save", 1);
        poCtrl.addCustomToolButton("另存为", "SaveAs", 12);
        poCtrl.addCustomToolButton("打印设置", "PrintSet", 0);
        poCtrl.addCustomToolButton("打印", "PrintFile", 6);
        poCtrl.addCustomToolButton("全屏/还原", "IsFullScreen", 4);
        poCtrl.addCustomToolButton("-", "", 0);
        poCtrl.addCustomToolButton("关闭", "Close", 21);
        //设置保存方法的url
        poCtrl.setSaveFilePage("/dev-api/BaSeal/save");
        //打开word
        poCtrl.webOpen("E:\\"+name, OpenModeType.docNormalEdit, "张三");
        return  poCtrl.getHtmlCode("PageOfficeCtrl1");
    }

    /**
     * 保存
     * @param request
     * @param response
     */
    @RequestMapping("/save")
    @ApiOperation(value = "文件添加")
    @Anonymous
    public void saveFile(HttpServletRequest request, HttpServletResponse response){
        FileSaver fs = new FileSaver(request, response);
        fs.saveToFile("E:\\" + fs.getFileName());
        FileInputStream fileStream = fs.getFileStream();
        File file = new File(fs.getLocalFileName());
        fs.close();
    }

    @GetMapping("/exportMyWord")
    @ApiOperation(value = "生成文档")
    @Anonymous
    public boolean exportMyWord(){
        ExportMyWord emw = new ExportMyWord();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("name", "测试");
        dataMap.put("phone", "183923245665");
        dataMap.put("number","文档内容填充");
        emw.createWord(dataMap, "123"+".ftl", "E:/upload/"+"中能信链"+".docx");
        return true;
    }

    @GetMapping(value = "/download2zip")
    @ApiOperation(value = "文件下载")
    @Anonymous
    public void export(HttpServletRequest request, HttpServletResponse response) {
        List<String> imagePaths = new ArrayList<>();
        imagePaths.add("mt/Vtb7qkoWMDdT73e2f67e2723e2791b89a11cbba12351.png");
        String zipName = "test" + ".zip";
        try {
            // 创建临时文件
            File zipFile = File.createTempFile("test", ".zip");
            // 设置响应类型，以附件形式下载文件
            response.reset();
            response.setContentType("text/plain");
            response.setContentType("application/octet-stream; charset=utf-8");
            response.setHeader("Location", zipName);
            response.setHeader("Cache-Control", "max-age=0");
            response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
            // 下载文件为zip压缩包
            ossService.getZipFromOSSByPaths(zipName, zipFile, request, response, imagePaths);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询多租户授权信息业务用印列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:BaSeal:list')")
    @GetMapping("/authorizedList")
    @ApiOperation(value = "查询多租户授权信息业务用印列表")
    public TableDataInfo authorizedList(BaSeal baSeal)
    {
        //租户ID
        baSeal.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaSeal> list = baSealService.selectBaSealAuthorizedList(baSeal);
        return getDataTable(list);
    }

    /**
     * 授权业务用印
     */
    @PreAuthorize("@ss.hasPermi('admin:BaSeal:authorized')")
    @Log(title = "业务用印", businessType = BusinessType.UPDATE)
    @PostMapping("/authorized")
    @ApiOperation(value = "授权业务用印")
    public AjaxResult authorized(@RequestBody BaSeal baSeal)
    {
        return AjaxResult.success(baSealService.authorizedBaSeal(baSeal));
    }

    /*@GetMapping("/download2zip")
    @ApiOperation(value = "文件下载")
    public void export(HttpServletRequest request, HttpServletResponse response) throws IOException {
        OssBootUtil ossBootUtil = new OssBootUtil();
        List<String> pathList = new ArrayList<>();
        // https://oss域名/upload/agree/张**-《同意函》(2022-08-23)_1661246208735.pdf
        pathList.add("mt/Vtb7qkoWMDdT73e2f67e2723e2791b89a11cbba12351.png");
        String zipName = System.currentTimeMillis() + ".zip";
        System.out.println("---------"+zipName);
        // 设置响应类型，以附件形式下载文件
        response.reset();
        response.setContentType("text/plain");
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Location", zipName);
        response.setHeader("Cache-Control", "max-age=0");
        response.setHeader("Content-Disposition", "attachment; filename=" + zipName);
        ossBootUtil.getZipFromOSSByPaths(zipName,pathList);
        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
        final File tempZipFile = new File(zipName);
        FileUtils.copyFile(tempZipFile, bos);
        bos.close();
        if (tempZipFile.exists()) {
            tempZipFile.delete();
        }

    }*/
}
