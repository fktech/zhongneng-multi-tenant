package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaClaimHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 认领历史Mapper接口
 *
 * @author ljb
 * @date 2023-01-31
 */
@Repository
public interface BaClaimHistoryMapper extends BaseMapper<BaClaimHistory>
{
    /**
     * 查询认领历史
     *
     * @param id 认领历史ID
     * @return 认领历史
     */
    public BaClaimHistory selectBaClaimHistoryById(String id);

    /**
     * 查询认领历史列表
     *
     * @param baClaimHistory 认领历史
     * @return 认领历史集合
     */
    public List<BaClaimHistory> selectBaClaimHistoryList(BaClaimHistory baClaimHistory);

    /**
     * 新增认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    public int insertBaClaimHistory(BaClaimHistory baClaimHistory);

    /**
     * 修改认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    public int updateBaClaimHistory(BaClaimHistory baClaimHistory);

    /**
     * 删除认领历史
     *
     * @param id 认领历史ID
     * @return 结果
     */
    public int deleteBaClaimHistoryById(String id);

    /**
     * 批量删除认领历史
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaClaimHistoryByIds(String[] ids);

    /**
     * 查询认领历史数据
     *
     * @param baClaimHistory 认领历史
     * @return 认领历史集合
     */
    public int countBaClaimHistoryList(BaClaimHistory baClaimHistory);

    /**
     * 收款总计
     */
    public BigDecimal baClaimHistoryCount(@Param("startId") String startId, @Param("tenantId") String tenantId);

    /**
     * 部门收款总计
     */
    public BigDecimal baClaimHistoryCountByDept(@Param("deptId") String deptId,@Param("tenantId") String tenantId);

    /**
     * 子公司收款总计
     */
    public BigDecimal baClaimHistoryCountBySubCompany(String deptId);

    /**
     * 兼职部门
     */
    public BigDecimal baClaimHistoryCountByPartDept(String deptId);

}
