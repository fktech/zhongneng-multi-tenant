package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaChargingStandards;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 收费标准Mapper接口
 *
 * @author single
 * @date 2023-06-02
 */
@Repository
public interface BaChargingStandardsMapper extends BaseMapper<BaChargingStandards>
{
    /**
     * 查询收费标准
     *
     * @param id 收费标准ID
     * @return 收费标准
     */
    public BaChargingStandards selectBaChargingStandardsById(String id);

    /**
     * 查询收费标准列表
     *
     * @param baChargingStandards 收费标准
     * @return 收费标准集合
     */
    public List<BaChargingStandards> selectBaChargingStandardsList(BaChargingStandards baChargingStandards);

    /**
     * 新增收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    public int insertBaChargingStandards(BaChargingStandards baChargingStandards);

    /**
     * 修改收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    public int updateBaChargingStandards(BaChargingStandards baChargingStandards);

    /**
     * 删除收费标准
     *
     * @param id 收费标准ID
     * @return 结果
     */
    public int deleteBaChargingStandardsById(String id);

    /**
     * 删除收费标准
     *
     * @param relationId 关联ID
     * @return 结果
     */
    public int deleteBaChargingStandardsByRelationId(String relationId);

    /**
     * 批量删除收费标准
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaChargingStandardsByIds(String[] ids);
}
