package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaContractStart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.TerminalDTO;
import com.business.system.domain.vo.BaContractStartTonVO;
import com.business.system.domain.dto.CountContractStartDTO;
import org.springframework.stereotype.Repository;

/**
 * 合同启动Mapper接口
 *
 * @author ljb
 * @date 2023-03-06
 */
@Repository
public interface BaContractStartMapper extends BaseMapper<BaContractStart>
{
    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    public BaContractStart selectBaContractStartById(String id);

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaContractStart> selectBaContractStartList(BaContractStart baContractStart);

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaContractStart> selectContractStartList(BaContractStart baContractStart);
    /**
     * 依照名称模糊查询查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaContractStart> selectBaContractStartListbyName(BaContractStart baContractStart);
    /**
     * 查询合同启动列表Name
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaContractStart> selectBaContractStart(BaContractStart baContractStart);

    /**
     * 新增合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int insertBaContractStart(BaContractStart baContractStart);

    /**
     * 修改合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int updateBaContractStart(BaContractStart baContractStart);

    /**
     * 修改合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int updateBaContractStartChange(BaContractStart baContractStart);

    /**
     * 删除合同启动
     *
     * @param id 合同启动ID
     * @return 结果
     */
    public int deleteBaContractStartById(String id);

    /**
     * 批量删除合同启动
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaContractStartByIds(String[] ids);

    /**
     * 查询订单信息
     * @param baContractStart
     * @return
     */
    public List<BaContractStart> selectOrderList(BaContractStart baContractStart);

    /**
     * 统计合同启动
     *
     * @param countContractStartDTO 合同启动
     * @return 结果
     */
    public int countContractStartList(CountContractStartDTO countContractStartDTO);

    /**
     * 数据呈现子公司首页统计
     *
     * @return 合同启动集合
     */
    public List<BaContractStart> selectSubCompanyStartList(String tenantId);

    /**
     * 数据呈现子公司首页统计
     *
     * @return 合同启动集合
     */
    public List<BaContractStart> selectContractStartListByDeptId(Long deptId);

    /**
     * 累计合同额
     *
     * @param countContractStartDTO 合同启动
     * @return 结果
     */
    public BigDecimal sumContractAmountByDeptId(CountContractStartDTO countContractStartDTO);

    /**
     * 首页合同启动面板统计
     * @param countContractStartDTO
     * @return
     */
    public List<CountContractStartDTO> contractStarCountByDeptId(CountContractStartDTO countContractStartDTO);

    /**
     * 业务类型占比
     * @return
     */
    public List<BaContractStartTonVO> countContractStartListByType(String tenantId);

    /**
     * 终端统计
     */
    public List<TerminalDTO> terminalStatistics(TerminalDTO terminalDTO);

    /**
     * 收入占比
     */
    public List<BaContractStartTonVO> contractStartListByType(BaContractStartTonVO baContractStartTonVO);

    /**
     * 查询多租户授权信息合同启动列表
     * @param baContractStart
     * @return
     */
    public List<BaContractStart> selectBaContractStartAuthorizedList(BaContractStart baContractStart);

    /**
     * 查询多租户授权信息销售申请列表
     */
    public List<BaContractStart> selectOrderAuthorizedList(BaContractStart baContractStart);
}
