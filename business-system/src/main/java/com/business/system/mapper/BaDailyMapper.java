package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.OaMaintenanceExpansion;
import org.springframework.stereotype.Repository;

/**
 * 日报Mapper接口
 *
 * @author single
 * @date 2023-10-09
 */
@Repository
public interface BaDailyMapper extends BaseMapper<BaDaily>
{
    /**
     * 查询日报
     *
     * @param id 日报ID
     * @return 日报
     */
    public BaDaily selectBaDailyById(String id);

    /**
     * 查询日报列表
     *
     * @param baDaily 日报
     * @return 日报集合
     */
    public List<BaDaily> selectBaDailyList(BaDaily baDaily);

    /**
     * 新增日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    public int insertBaDaily(BaDaily baDaily);

    /**
     * 修改日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    public int updateBaDaily(BaDaily baDaily);

    /**
     * 删除日报
     *
     * @param id 日报ID
     * @return 结果
     */
    public int deleteBaDailyById(String id);

    /**
     * 批量删除日报
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDailyByIds(String[] ids);

    /**
     * 获取所有提交人员
     */
    public List<String> submitUser(BaDaily baDaily);
}
