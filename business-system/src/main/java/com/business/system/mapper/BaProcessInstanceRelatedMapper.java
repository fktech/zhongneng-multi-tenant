package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.dto.BaProcessInstanceRelatedQueryDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系表 Mapper 接口
 * @date: 2022/12/2 10:21
 */
@Repository
public interface BaProcessInstanceRelatedMapper extends BaseMapper<BaProcessInstanceRelated> {

    BaProcessInstanceRelated getApplyBusinessDataByProcessInstanceId(String processInstanceId);

    List<BaProcessInstanceRelated> listApplyBusinessDataByBusinessKey(@Param("baProcessInstanceRelated") BaProcessInstanceRelated baProcessInstanceRelated);

    List<BaProcessInstanceRelated> listApplyBusinessDataByBusinessKeys(@Param("businessKeys") List<String> businessKeys);

    List<BaProcessInstanceRelated> listProcessInstanceGroupByCreatetime(@Param("baProcessInstanceRelated") BaProcessInstanceRelated baProcessInstanceRelated);

//    List<BaProcessInstanceRelated> listProcessInstanceOrderByCreatetime(@Param("baProcessInstanceRelated") BaProcessInstanceRelated baProcessInstanceRelated);
    List<BaProcessInstanceRelated> listProcessInstanceOrderByCreatetime(@Param("baProcessInstanceRelated") BaProcessInstanceRelatedQueryDTO baProcessInstanceRelated);

    void updateInstanceRelatedProcessInstanceId(BaProcessInstanceRelated baProcessInstanceRelated);

    void updateBaProcessInstanceRelated(BaProcessInstanceRelated baProcessInstanceRelated);

    void updateInstanceRelated(BaProcessInstanceRelated baProcessInstanceRelated);

    /**
     * 小程序查询接口
     */
    List<BaProcessInstanceRelated> listProcessInstanceOrderByCreatetimeWx(@Param("baProcessInstanceRelated") BaProcessInstanceRelated baProcessInstanceRelated);

    List<String> listProcessInstanceIds(BaProcessInstanceRelated baProcessInstanceRelated);
}
