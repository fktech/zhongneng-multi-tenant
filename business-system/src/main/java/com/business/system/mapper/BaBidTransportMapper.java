package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaBidTransport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 合同启动投标信息运输Mapper接口
 *
 * @author single
 * @date 2023-06-05
 */
@Repository
public interface BaBidTransportMapper extends BaseMapper<BaBidTransport>
{
    /**
     * 查询合同启动投标信息运输
     *
     * @param id 合同启动投标信息运输ID
     * @return 合同启动投标信息运输
     */
    public BaBidTransport selectBaBidTransportById(String id);

    /**
     * 查询合同启动投标信息运输列表
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 合同启动投标信息运输集合
     */
    public List<BaBidTransport> selectBaBidTransportList(BaBidTransport baBidTransport);

    public List<BaBidTransport> baBidTransportList(BaBidTransport baBidTransport);

    /**
     * 新增合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    public int insertBaBidTransport(BaBidTransport baBidTransport);

    /**
     * 修改合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    public int updateBaBidTransport(BaBidTransport baBidTransport);

    /**
     * 修改合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    public int updateBaBidTransportChange(BaBidTransport baBidTransport);

    /**
     * 删除合同启动投标信息运输
     *
     * @param id 合同启动投标信息运输ID
     * @return 结果
     */
    public int deleteBaBidTransportById(String id);

    /**
     * 根据合同启动ID删除合同启动投标信息运输数据
     *
     * @param startId 合同启动startIdID
     * @return 结果
     */
    public int deleteBaChargingStandardsByStartId(String startId);

    /**
     * 批量删除合同启动投标信息运输
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaBidTransportByIds(String[] ids);

    /**
     * 累计发运量
     * @param baBidTransport
     * @return
     */
    public BigDecimal baBidTransportSum(BaBidTransport baBidTransport);

    /**
     * 累计发运量
     * @return
     */
    public BigDecimal countBidTransportList();
}
