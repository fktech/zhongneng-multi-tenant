package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaPoundRoom;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 磅房数据Mapper接口
 *
 * @author ljb
 * @date 2023-09-04
 */
@Repository
public interface BaPoundRoomMapper extends BaseMapper<BaPoundRoom>
{
    /**
     * 查询磅房数据
     *
     * @param id 磅房数据ID
     * @return 磅房数据
     */
    public BaPoundRoom selectBaPoundRoomById(String id);

    /**
     * 查询磅房数据列表
     *
     * @param baPoundRoom 磅房数据
     * @return 磅房数据集合
     */
    public List<BaPoundRoom> selectBaPoundRoomList(BaPoundRoom baPoundRoom);

    /**
     * 新增磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    public int insertBaPoundRoom(BaPoundRoom baPoundRoom);

    /**
     * 修改磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    public int updateBaPoundRoom(BaPoundRoom baPoundRoom);

    /**
     * 删除磅房数据
     *
     * @param id 磅房数据ID
     * @return 结果
     */
    public int deleteBaPoundRoomById(String id);

    /**
     * 批量删除磅房数据
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaPoundRoomByIds(String[] ids);
}
