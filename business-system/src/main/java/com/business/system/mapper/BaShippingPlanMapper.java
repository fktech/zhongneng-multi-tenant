package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaShippingPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 发运计划Mapper接口
 *
 * @author ljb
 * @date 2023-03-06
 */
@Repository
public interface BaShippingPlanMapper extends BaseMapper<BaShippingPlan>
{
    /**
     * 查询发运计划
     *
     * @param id 发运计划ID
     * @return 发运计划
     */
    public BaShippingPlan selectBaShippingPlanById(String id);

    /**
     * 查询发运计划列表
     *
     * @param baShippingPlan 发运计划
     * @return 发运计划集合
     */
    public List<BaShippingPlan> selectBaShippingPlanList(BaShippingPlan baShippingPlan);

    /**
     * 新增发运计划
     *
     * @param baShippingPlan 发运计划
     * @return 结果
     */
    public int insertBaShippingPlan(BaShippingPlan baShippingPlan);

    /**
     * 修改发运计划
     *
     * @param baShippingPlan 发运计划
     * @return 结果
     */
    public int updateBaShippingPlan(BaShippingPlan baShippingPlan);

    /**
     * 删除发运计划
     *
     * @param id 发运计划ID
     * @return 结果
     */
    public int deleteBaShippingPlanById(String id);

    /**
     * 批量删除发运计划
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaShippingPlanByIds(String[] ids);
}
