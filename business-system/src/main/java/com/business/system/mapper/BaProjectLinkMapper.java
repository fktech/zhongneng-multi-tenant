package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaProjectLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 立项授权Mapper接口
 *
 * @author ljb
 * @date 2023-06-05
 */
@Repository
public interface BaProjectLinkMapper extends BaseMapper<BaProjectLink>
{
    /**
     * 查询立项授权
     *
     * @param id 立项授权ID
     * @return 立项授权
     */
    public BaProjectLink selectBaProjectLinkById(String id);

    /**
     * 查询立项授权列表
     *
     * @param baProjectLink 立项授权
     * @return 立项授权集合
     */
    public List<BaProjectLink> selectBaProjectLinkList(BaProjectLink baProjectLink);

    /**
     * 新增立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    public int insertBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 修改立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    public int updateBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 删除立项授权
     *
     * @param id 立项授权ID
     * @return 结果
     */
    public int deleteBaProjectLinkById(String id);

    /**
     * 批量删除立项授权
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaProjectLinkByIds(String[] ids);
}
