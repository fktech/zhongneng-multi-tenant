package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaInvoiceDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.SysUserRole;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import org.springframework.stereotype.Repository;

/**
 * 发票信息详情Mapper接口
 *
 * @author single
 * @date 2023-03-14
 */
@Repository
public interface BaInvoiceDetailMapper extends BaseMapper<BaInvoiceDetail>
{
    /**
     * 查询发票信息详情
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public BaInvoiceDetail selectBaInvoiceDetailById(String id);
    /**
     * 查询开票票id查询相应的发票数量
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public int selectBaInvoiceDetailByinviceId(String id);
    /**
     * 查询开票票id查询相应的发票数量本月
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public NumsAndMoneyDTO selectDetailByinviceIdMonth(String id);

    /**
     * 查询发票信息详情列表
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 发票信息详情集合
     */
    public List<BaInvoiceDetail> selectBaInvoiceDetailList(BaInvoiceDetail baInvoiceDetail);

    /**
     * 新增发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    public int insertBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail);

    /**
     * 批量新增发票信息列
     *
     * @param list 发票信息详情列表
     * @return 结果
     */
    public int batchInsertBaInvoiceDetail(List<BaInvoiceDetail> list);

    /**
     * 修改发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    public int updateBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail);

    /**
     * 删除发票信息详情
     *
     * @param id 发票信息详情ID
     * @return 结果
     */
    public int deleteBaInvoiceDetailById(String id);

    /**
     * 批量删除发票信息详情
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaInvoiceDetailByIds(String[] ids);

    /**
     * 根据发票ID删除发票信息详情
     *
     * @param invoiceId 发票信息详情ID
     * @return 结果
     */
    public int deleteBaInvoiceDetailByInvoiceId(String invoiceId);
}
