package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaEventRecording;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 事件记录Mapper接口
 *
 * @author single
 * @date 2023-07-05
 */
@Repository
public interface BaEventRecordingMapper extends BaseMapper<BaEventRecording>
{
    /**
     * 查询事件记录
     *
     * @param id 事件记录ID
     * @return 事件记录
     */
    public BaEventRecording selectBaEventRecordingById(String id);

    /**
     * 查询事件记录列表
     *
     * @param baEventRecording 事件记录
     * @return 事件记录集合
     */
    public List<BaEventRecording> selectBaEventRecordingList(BaEventRecording baEventRecording);

    /**
     * 新增事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    public int insertBaEventRecording(BaEventRecording baEventRecording);

    /**
     * 修改事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    public int updateBaEventRecording(BaEventRecording baEventRecording);

    /**
     * 删除事件记录
     *
     * @param id 事件记录ID
     * @return 结果
     */
    public int deleteBaEventRecordingById(String id);

    /**
     * 批量删除事件记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaEventRecordingByIds(String[] ids);
}
