package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaGoodsAdd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 商品附加Mapper接口
 *
 * @author ljb
 * @date 2022-12-01
 */
@Repository
public interface BaGoodsAddMapper extends BaseMapper<BaGoodsAdd>
{
    /**
     * 查询商品附加
     *
     * @param id 商品附加ID
     * @return 商品附加
     */
    public BaGoodsAdd selectBaGoodsAddById(String id);

    /**
     * 查询商品附加列表
     *
     * @param baGoodsAdd 商品附加
     * @return 商品附加集合
     */
    public List<BaGoodsAdd> selectBaGoodsAddList(BaGoodsAdd baGoodsAdd);

    /**
     * 新增商品附加
     *
     * @param baGoodsAdd 商品附加
     * @return 结果
     */
    public int insertBaGoodsAdd(BaGoodsAdd baGoodsAdd);

    /**
     * 修改商品附加
     *
     * @param baGoodsAdd 商品附加
     * @return 结果
     */
    public int updateBaGoodsAdd(BaGoodsAdd baGoodsAdd);

    /**
     * 删除商品附加
     *
     * @param id 商品附加ID
     * @return 结果
     */
    public int deleteBaGoodsAddById(String id);

    /**
     * 批量删除商品附加
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaGoodsAddByIds(String[] ids);
}
