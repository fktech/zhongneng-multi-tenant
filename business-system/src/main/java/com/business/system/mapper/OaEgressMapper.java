package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaEgress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 外出申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaEgressMapper extends BaseMapper<OaEgress>
{
    /**
     * 查询外出申请
     *
     * @param id 外出申请ID
     * @return 外出申请
     */
    public OaEgress selectOaEgressById(String id);

    /**
     * 查询外出申请列表
     *
     * @param oaEgress 外出申请
     * @return 外出申请集合
     */
    public List<OaEgress> selectOaEgressList(OaEgress oaEgress);

    /**
     * 统计外出申请
     *
     * @param oaEgress 外出申请
     * @return 外出申请集合
     */
    public Long countOaEgressList(OaEgress oaEgress);

    /**
     * 时间查询
     *
     * @param oaEgress 外出申请
     * @return 外出申请集合
     */
    public List<OaEgress> selectOaEgress(OaEgress oaEgress);

    /**
     * 新增外出申请
     *
     * @param oaEgress 外出申请
     * @return 结果
     */
    public int insertOaEgress(OaEgress oaEgress);

    /**
     * 修改外出申请
     *
     * @param oaEgress 外出申请
     * @return 结果
     */
    public int updateOaEgress(OaEgress oaEgress);

    /**
     * 删除外出申请
     *
     * @param id 外出申请ID
     * @return 结果
     */
    public int deleteOaEgressById(String id);

    /**
     * 批量删除外出申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaEgressByIds(String[] ids);
}
