package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaChecked;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 盘点Mapper接口
 *
 * @author single
 * @date 2023-01-05
 */
@Repository
public interface BaCheckedMapper extends BaseMapper<BaChecked>
{
    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    public BaChecked selectBaCheckedById(String id);

    /**
     * 查询盘点列表
     *
     * @param baChecked 盘点
     * @return 盘点集合
     */
    public List<BaChecked> selectBaCheckedList(BaChecked baChecked);

    /**
     * 新增盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    public int insertBaChecked(BaChecked baChecked);

    /**
     * 修改盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    public int updateBaChecked(BaChecked baChecked);

    /**
     * 删除盘点
     *
     * @param id 盘点ID
     * @return 结果
     */
    public int deleteBaCheckedById(String id);

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCheckedByIds(String[] ids);
}
