package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.vo.BaCompanyVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 公司Mapper接口
 *
 * @author ljb
 * @date 2022-11-30
 */
@Repository
public interface BaCompanyMapper extends BaseMapper<BaCompany>
{
    /**
     * 查询公司
     *
     * @param id 公司ID
     * @return 公司
     */
    public BaCompany selectBaCompanyById(String id);

    /**
     * 查询公司列表
     *
     * @param baCompany 公司
     * @return 公司集合
     */
    public List<BaCompany> selectBaCompanyList(BaCompany baCompany);

    /**
     * 新增公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    public int insertBaCompany(BaCompany baCompany);

    /**
     * 修改公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    public int updateBaCompany(BaCompany baCompany);

    /**
     * 删除公司
     *
     * @param id 公司ID
     * @return 结果
     */
    public int deleteBaCompanyById(String id);

    /**
     * 批量删除公司
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCompanyByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth(@Param("companyType") String companyType, @Param("tenantId") String tenantId);

    /**
     * 查询其他公司本月数据
     */
    public Integer otherCompanyThisMonth(@Param("companyType") String companyType,@Param("tenantId") String tenantId);

    /**
     * 承运单位发运量
     */
    public List<BaCompanyVO> countRanking();
}
