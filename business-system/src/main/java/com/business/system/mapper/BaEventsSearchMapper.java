package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaEventsSearch;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 联动事件Mapper接口
 *
 * @author single
 * @date 2023-07-05
 */
@Repository
public interface BaEventsSearchMapper extends BaseMapper<BaEventsSearch>
{
    /**
     * 查询联动事件
     *
     * @param id 联动事件ID
     * @return 联动事件
     */
    public BaEventsSearch selectBaEventsSearchById(String id);

    /**
     * 查询联动事件列表
     *
     * @param baEventsSearch 联动事件
     * @return 联动事件集合
     */
    public List<BaEventsSearch> selectBaEventsSearchList(BaEventsSearch baEventsSearch);

    /**
     * 新增联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    public int insertBaEventsSearch(BaEventsSearch baEventsSearch);

    /**
     * 修改联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    public int updateBaEventsSearch(BaEventsSearch baEventsSearch);

    /**
     * 删除联动事件
     *
     * @param id 联动事件ID
     * @return 结果
     */
    public int deleteBaEventsSearchById(String id);

    /**
     * 批量删除联动事件
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaEventsSearchByIds(String[] ids);
}
