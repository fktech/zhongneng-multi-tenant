package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaClaimRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.vo.OaClaimStatVO;
import org.springframework.stereotype.Repository;

/**
 * 报销关联流程Mapper接口
 *
 * @author single
 * @date 2023-12-13
 */
@Repository
public interface OaClaimRelationMapper extends BaseMapper<OaClaimRelation>
{
    /**
     * 查询报销关联流程
     *
     * @param id 报销关联流程ID
     * @return 报销关联流程
     */
    public OaClaimRelation selectOaClaimRelationById(String id);

    /**
     * 查询报销关联流程列表
     *
     * @param oaClaimRelation 报销关联流程
     * @return 报销关联流程集合
     */
    public List<OaClaimRelation> selectOaClaimRelationList(OaClaimRelation oaClaimRelation);

    /**
     * 新增报销关联流程
     *
     * @param oaClaimRelation 报销关联流程
     * @return 结果
     */
    public int insertOaClaimRelation(OaClaimRelation oaClaimRelation);

    /**
     * 修改报销关联流程
     *
     * @param oaClaimRelation 报销关联流程
     * @return 结果
     */
    public int updateOaClaimRelation(OaClaimRelation oaClaimRelation);

    /**
     * 删除报销关联流程
     *
     * @param id 报销关联流程ID
     * @return 结果
     */
    public int deleteOaClaimRelationById(String id);

    /**
     * 批量删除报销关联流程
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaClaimRelationByIds(String[] ids);

    /**
     * 查询日常报销统计
     *
     * @return 报销明细
     */
    public OaClaimStatVO sumOaClaimRelation(OaClaimRelation oaClaimRelation);
}
