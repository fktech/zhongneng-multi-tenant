package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaHiProject;
import com.business.system.domain.BaHiTransport;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 立项管理历史Mapper接口
 *
 * @author ljb
 * @date 2022-12-02
 */
@Repository
public interface BaHiProjectMapper extends BaseMapper<BaHiProject>
{
    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    public BaHiProject selectBaHiProjectById(String id);

    /**
     * 查询立项管理列表
     *
     * @param baHiProject 立项管理
     * @return 立项管理集合
     */
    public List<BaHiProject> selectBaHiProjectList(BaHiProject baHiProject);

    public List<BaHiProject> selectHiProject(BaHiProject baHiProject);
    /**
     * 查询立项通过列表
     *
     * @param baHiProject 立项管理
     * @return 立项管理集合
     */
    public List<BaHiProject> selectBaHiProjectList1(BaHiProject baHiProject);


    /**
     * 新增立项管理
     *
     * @param baHiProject 立项管理
     * @return 结果
     */
    public int insertBaHiProject(BaHiProject baHiProject);

    /**
     * 修改立项管理
     *
     * @param baHiProject 立项管理
     * @return 结果
     */
    public int updateBaHiProject(BaHiProject baHiProject);

    /**
     * 修改立项管理
     *
     * @param baHiProject 立项管理
     * @return 结果
     */
    public int updateBaHiProjectChange(BaHiProject baHiProject);

    /**
     * 删除立项管理
     *
     * @param id 立项管理ID
     * @return 结果
     */
    public int deleteBaHiProjectById(String id);

    /**
     * 批量删除立项管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaHiProjectByIds(String[] ids);

    /**
     * 查询立项管理变更历史列表
     *
     * @param baHiProject 立项管理
     * @return 用煤企业集合
     */
    public List<BaHiProject> selectChangeBaHiProjectList(BaHiProject baHiProject);
}
