package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaLeave;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 请假申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaLeaveMapper extends BaseMapper<OaLeave>
{
    /**
     * 查询请假申请
     *
     * @param id 请假申请ID
     * @return 请假申请
     */
    public OaLeave selectOaLeaveById(String id);

    /**
     * 查询请假申请列表
     *
     * @param oaLeave 请假申请
     * @return 请假申请集合
     */
    public List<OaLeave> selectOaLeaveList(OaLeave oaLeave);

    /**
     * 统计请假申请
     *
     * @param oaLeave 请假申请
     * @return 请假申请集合
     */
    public Long countOaLeaveList(OaLeave oaLeave);

    /**
     * 时间查询
     * @param oaLeave
     * @return
     */
    public List<OaLeave> selectOaLeave(OaLeave oaLeave);

    /**
     * 新增请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    public int insertOaLeave(OaLeave oaLeave);

    /**
     * 修改请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    public int updateOaLeave(OaLeave oaLeave);

    /**
     * 删除请假申请
     *
     * @param id 请假申请ID
     * @return 结果
     */
    public int deleteOaLeaveById(String id);

    /**
     * 批量删除请假申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaLeaveByIds(String[] ids);
}
