package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 付款明细Mapper接口
 *
 * @author ljb
 * @date 2023-04-12
 */
@Repository
public interface BaAutomobileSettlementDetailMapper extends BaseMapper<BaAutomobileSettlementDetail>
{
    /**
     * 查询付款明细
     *
     * @param id 付款明细ID
     * @return 付款明细
     */
    public BaAutomobileSettlementDetail selectBaAutomobileSettlementDetailById(String id);

    /**
     * 查询付款明细列表
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 付款明细集合
     */
    public List<BaAutomobileSettlementDetail> selectBaAutomobileSettlementDetailList(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 查询付款明细列表
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 付款明细集合
     */
    public List<BaAutomobileSettlementDetail> automobileSettlementDetailList(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 新增付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    public int insertBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 修改付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    public int updateBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 删除付款明细
     *
     * @param id 付款明细ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementDetailById(String id);

    /**
     * 批量删除付款明细
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementDetailByIds(String[] ids);
}
