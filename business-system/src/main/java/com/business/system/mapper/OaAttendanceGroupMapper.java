package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaAttendanceGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 考勤组Mapper接口
 *
 * @author single
 * @date 2023-11-24
 */
@Repository
public interface OaAttendanceGroupMapper extends BaseMapper<OaAttendanceGroup>
{
    /**
     * 查询考勤组
     *
     * @param id 考勤组ID
     * @return 考勤组
     */
    public OaAttendanceGroup selectOaAttendanceGroupById(String id);

    /**
     * 查询考勤组列表
     *
     * @param oaAttendanceGroup 考勤组
     * @return 考勤组集合
     */
    public List<OaAttendanceGroup> selectOaAttendanceGroupList(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 新增考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    public int insertOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 修改考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    public int updateOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 删除考勤组
     *
     * @param id 考勤组ID
     * @return 结果
     */
    public int deleteOaAttendanceGroupById(String id);

    /**
     * 批量删除考勤组
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaAttendanceGroupByIds(String[] ids);
}
