package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaClaim;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaSettlement;
import com.business.system.domain.dto.MonthSumDTO;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import org.springframework.stereotype.Repository;

/**
 * 认领Mapper接口
 *
 * @author ljb
 * @date 2023-01-31
 */
@Repository
public interface BaClaimMapper extends BaseMapper<BaClaim>
{
    /**
     * 查询认领
     *
     * @param id 认领ID
     * @return 认领
     */
    public BaClaim selectBaClaimById(String id);


    /**
     * 查询认领列表
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public List<BaClaim> selectBaClaimList(BaClaim baClaim);

    public List<BaClaim> baClaimList(BaClaim baClaim);

    public List<BaClaim> claimList(BaClaim baClaim);
    public String selectBaClaimByrelationid(String id) ;
    public BaClaim selectBaClaimById1(String id) ;
    /**
     * 查询财务统计中销售额
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolume(BaClaim baClaim);
    /**
     * 查询财务统计中销售额本年度
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByMonth(BaClaim baClaim);
    /**
     * 查询财务统计中销售额本月度
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByYear(BaClaim baClaim);
    /**
     * 查询财务统计中营业额从本月去之前12个月的
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaClaim baClaim);


    /**
     * 新增认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    public int insertBaClaim(BaClaim baClaim);

    /**
     * 修改认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    public int updateBaClaim(BaClaim baClaim);

    /**
     * 删除认领
     *
     * @param id 认领ID
     * @return 结果
     */
    public int deleteBaClaimById(String id);

    /**
     * 批量删除认领
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaClaimByIds(String[] ids);

    /**
     * 查询收款总数及金额
     */
    public NumsAndMoneyDTO selectBaClaimForUp(BaClaim baClaim);
    /**
     * 查询月度收款总数
     */
    public NumsAndMoneyDTO selectBaClaimForMonth(BaClaim baClaim);
    /**
     * * 查询年度收款总数
     */
    public NumsAndMoneyDTO selectBaClaimForYear(BaClaim baClaim);

}
