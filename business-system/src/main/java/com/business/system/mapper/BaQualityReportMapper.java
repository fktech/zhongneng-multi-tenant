package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaQualityReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 数质量报告Mapper接口
 *
 * @author single
 * @date 2023-07-19
 */
@Repository
public interface BaQualityReportMapper extends BaseMapper<BaQualityReport>
{
    /**
     * 查询数质量报告
     *
     * @param id 数质量报告ID
     * @return 数质量报告
     */
    public BaQualityReport selectBaQualityReportById(String id);

    /**
     * 查询数质量报告列表
     *
     * @param baQualityReport 数质量报告
     * @return 数质量报告集合
     */
    public List<BaQualityReport> selectBaQualityReportList(BaQualityReport baQualityReport);

    /**
     * 查询数质量报告列表
     *
     * @param baQualityReport 数质量报告
     * @return 数质量报告集合
     */
    public List<BaQualityReport> baQualityReportList(BaQualityReport baQualityReport);

    /**
     * 新增数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    public int insertBaQualityReport(BaQualityReport baQualityReport);

    /**
     * 修改数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    public int updateBaQualityReport(BaQualityReport baQualityReport);

    /**
     * 删除数质量报告
     *
     * @param id 数质量报告ID
     * @return 结果
     */
    public int deleteBaQualityReportById(String id);

    /**
     * 删除
     *
     * @param transportId 运输ID
     * @return 结果
     */
    public int deleteBaQualityReportByTransportId(String transportId);

    /**
     * 批量删除数质量报告
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaQualityReportByIds(String[] ids);
}
