package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;

import com.business.system.domain.BaCollection;
import com.business.system.domain.BaPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.CountPaymentDTO;
import com.business.system.domain.dto.MonthSumDTO;
import com.business.system.domain.dto.BusinessUserDTO;
import com.business.system.domain.vo.BaCollectionStatVO;
import com.business.system.domain.vo.BaPaymentStatVO;
import org.springframework.stereotype.Repository;

/**
 * 付款信息Mapper接口
 *
 * @author single
 * @date 2022-12-26
 */
@Repository
public interface BaPaymentMapper extends BaseMapper<BaPayment>
{
    /**
     * 查询付款信息
     *
     * @param id 付款信息ID
     * @return 付款信息
     */
    public BaPayment selectBaPaymentById(String id);

    /**
     * 查询付款信息列表
     *
     * @param baPayment 付款信息
     * @return 付款信息集合
     */
    public List<BaPayment> selectBaPaymentList(BaPayment baPayment);

    /**
     * 查询付款信息列表
     *
     * @param baPayment 付款信息
     * @return 付款信息集合
     */
    public List<BaPayment> selectBaPaymentListByCondition(BaPayment baPayment);

    /**
     * 新增付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    public int insertBaPayment(BaPayment baPayment);

    /**
     * 修改付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    public int updateBaPayment(BaPayment baPayment);

    /**
     * 删除付款信息
     *
     * @param id 付款信息ID
     * @return 结果
     */
    public int deleteBaPaymentById(String id);

    /**
     * 批量删除付款信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaPaymentByIds(String[] ids);
    /**
     * 查询财务统计中营业额从本月去之前12个月的收款金额
     *
     * @param baPayment 认领
     * @return 认领集合
     */
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaPayment baPayment);
    /**
     * 查询财本年度的收款金额
     *
     * @param
     * @return 认领集合
     */
    /**
     * 查询财本年度的收款金额
     *
     * @param
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByyear(BaPayment baPayment);
    /**
     * 查询财本年度的收款金额
     *
     * @param
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByyearAndType(BaPayment baPayment);

    /**
     * 统计收款
     */
    public BigDecimal sumBaSettlementAndPayment(BaPayment baPayment);

    /**
     * PC端首页累计付款
     *
     * @return 运输
     */
    public BigDecimal countPaymentList(CountPaymentDTO countPaymentDTO);

    /**
     * 统计付款
     *
     * @return 运输
     */
    public int countDepotPaymentList(CountPaymentDTO countPaymentDTO);

    /**
     * 根据付款流程实例ID查询对应项目的业务经理用户ID
     */
    public List<String> getUserIdByProcessInstanceId(BusinessUserDTO paymentUserDTO);

    /**
     * 可视化大屏统计应付账款数量
     */
    public BaPaymentStatVO countBaPayment(BaPayment baPayment);

    /**
     * 可视化大屏统计应付账款
     */
    public BaPaymentStatVO sumBaPayment(BaPayment baPayment);

    /**
     * 统计确认付款金额
     */
    public BigDecimal amountPaid(BaPayment baPayment);

    public List<BaPayment> selectBaPaymentAuthorizedListByCondition(BaPayment baPayment);
}
