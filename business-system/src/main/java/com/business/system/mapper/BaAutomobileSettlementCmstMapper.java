package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementCmst;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 汽运结算Mapper接口
 *
 * @author single
 * @date 2023-08-21
 */
@Repository
public interface BaAutomobileSettlementCmstMapper extends BaseMapper<BaAutomobileSettlementCmst>
{
    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    public BaAutomobileSettlementCmst selectBaAutomobileSettlementCmstById(String id);

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 汽运结算集合
     */
    public List<BaAutomobileSettlementCmst> selectBaAutomobileSettlementCmstList(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    public int insertBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    public int updateBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 删除汽运结算
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementCmstById(String id);

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementCmstByIds(String[] ids);
}
