package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaProjectBeforehand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 预立项Mapper接口
 *
 * @author ljb
 * @date 2023-09-20
 */
@Repository
public interface BaProjectBeforehandMapper extends BaseMapper<BaProjectBeforehand>
{
    /**
     * 查询预立项
     *
     * @param id 预立项ID
     * @return 预立项
     */
    public BaProjectBeforehand selectBaProjectBeforehandById(String id);

    /**
     * 查询预立项列表
     *
     * @param baProjectBeforehand 预立项
     * @return 预立项集合
     */
    public List<BaProjectBeforehand> selectBaProjectBeforehandList(BaProjectBeforehand baProjectBeforehand);

    public List<BaProjectBeforehand> selectBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    /**
     * 新增预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    public int insertBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    /**
     * 修改预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    public int updateBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    /**
     * 删除预立项
     *
     * @param id 预立项ID
     * @return 结果
     */
    public int deleteBaProjectBeforehandById(String id);

    /**
     * 批量删除预立项
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaProjectBeforehandByIds(String[] ids);
}
