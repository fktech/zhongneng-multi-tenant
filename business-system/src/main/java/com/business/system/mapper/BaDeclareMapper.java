package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDeclare;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaDeclareDetail;
import com.business.system.domain.BaInvoice;
import org.springframework.stereotype.Repository;

/**
 * 计划申报Mapper接口
 *
 * @author single
 * @date 2023-03-21
 */
@Repository
public interface BaDeclareMapper extends BaseMapper<BaDeclare>
{
    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    public BaDeclare selectBaDeclareById(String id);

    /**
     * 查询计划申报列表
     *
     * @param baDeclare 计划申报
     * @return 计划申报集合
     */
    public List<BaDeclare> selectBaDeclareList(BaDeclare baDeclare);

    /**
     * 根据条件查询计划申报列表
     *
     * @param baDeclare 计划申报信息列
     * @return 计划申报信息列集合
     */
    public List<BaDeclare> selectBaDeclareListByCondition(BaDeclare baDeclare);

    /**
     * 新增计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    public int insertBaDeclare(BaDeclare baDeclare);

    /**
     * 修改计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    public int updateBaDeclare(BaDeclare baDeclare);

    /**
     * 删除计划申报
     *
     * @param id 计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareById(String id);

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDeclareByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth(Long deptId);
    /**
     * 查询本月审核通过的数据
     */
    public Integer thisMonth1(BaDeclare baDeclare);

    /**
     * 本周本人计划申报
     */
    public Integer thisWeek(Long userId);

    /**
     * 本周计划申报人
     */
    public List<BaDeclare> thisWeekDeclared();
    /**
     * 根据合同id和计划时间查询计划申报
     *
     * @param  baDeclare 计划申报详情
     * @return 结果
     */
    public BaDeclare selectBaDeclareBystartId(BaDeclare baDeclare);
    /**
     * 查询多租户授权信息计划申报列表
     */
    public List<BaDeclare> selectBaDeclareAuthorizedListByCondition(BaDeclare baDeclare);
}
