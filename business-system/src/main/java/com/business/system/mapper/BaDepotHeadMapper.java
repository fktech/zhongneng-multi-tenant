package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaDepotHead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.DepotHeadDTO;
import com.business.system.domain.vo.BaMasterialStockVO;
import org.springframework.stereotype.Repository;

/**
 * 出入库记录Mapper接口
 *
 * @author single
 * @date 2023-01-05
 */
@Repository
public interface BaDepotHeadMapper extends BaseMapper<BaDepotHead>
{
    /**
     * 查询出入库记录
     *
     * @param id 出入库记录ID
     * @return 出入库记录
     */
    public BaDepotHead selectBaDepotHeadById(String id);

    /**
     * 查询出入库记录列表
     *
     * @param baDepotHead 出入库记录
     * @return 出入库记录集合
     */
    public List<BaDepotHead> selectBaDepotHeadList(BaDepotHead baDepotHead);

    public List<BaDepotHead> selectBaDepotHead(BaDepotHead baDepotHead);

    public List<BaDepotHead> selectDepotHead(BaDepotHead baDepotHead);

    /**
     * 新增出入库记录
     *
     * @param baDepotHead 出入库记录
     * @return 结果
     */
    public int insertBaDepotHead(BaDepotHead baDepotHead);

    /**
     * 修改出入库记录
     *
     * @param baDepotHead 出入库记录
     * @return 结果
     */
    public int updateBaDepotHead(BaDepotHead baDepotHead);

    /**
     * 删除出入库记录
     *
     * @param id 出入库记录ID
     * @return 结果
     */
    public int deleteBaDepotHeadById(String id);

    /**
     * 批量删除出入库记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotHeadByIds(String[] ids);

    /**
     * 加工统计
     * @param depotHeadDTO
     * @return
     */
    public List<DepotHeadDTO> outputBaDepotHead(DepotHeadDTO depotHeadDTO);

    /**
     * 统计出入库
     */
    public int countBaDepotHeadList(BaDepotHead baDepotHead);

    /**
     * 统计不重复项目
     */
    public int countProjectList(BaDepotHead baDepotHead);

    public List<BaMasterialStockVO> selectProjectList(BaDepotHead baDepotHead);

    /**
     * 库存量
     */
    public BigDecimal totals(DepotHeadDTO depotHeadDTO);

    /**
     * 出入库明细
     * @param baDepotHead
     * @return
     */
    public List<BaDepotHead> depotHeadDetail(BaDepotHead baDepotHead);

    /**
     * 查询多租户授权信息出入库列表
     */
    public List<BaDepotHead> selectDepotHeadAuthorized(BaDepotHead baDepotHead);
}
