package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaClock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.OaClockDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 打卡Mapper接口
 *
 * @author single
 * @date 2023-11-27
 */
@Repository
public interface OaClockMapper extends BaseMapper<OaClock>
{
    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    public OaClock selectOaClockById(String id);

    /**
     * 查询打卡列表
     *
     * @param oaClock 打卡
     * @return 打卡集合
     */
    public List<OaClock> selectOaClockList(OaClock oaClock);

    /**
     * 查询打卡列表
     *
     * @return 打卡集合
     */
    public List<OaClock> selectOaClockListByCondition(@Param("queryTime") String queryTime, @Param("ids") String[] ids);

    /**
     * 新增打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    public int insertOaClock(OaClock oaClock);

    /**
     * 修改打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    public int updateOaClock(OaClock oaClock);

    /**
     * 删除打卡
     *
     * @param id 打卡ID
     * @return 结果
     */
    public int deleteOaClockById(String id);

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaClockByIds(String[] ids);

    /**
     * 查询打卡列表
     *
     * @param oaClockDTO 打卡DTO
     * @return 打卡集合
     */
    public List<OaClock> statOaClockList(OaClockDTO oaClockDTO);

    /**
     * 以打卡在状态查询每一天数据
     */
    public List<OaClock> selectOaClockState(OaClock oaClock);
}
