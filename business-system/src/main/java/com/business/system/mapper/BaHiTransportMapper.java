package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaHiEnterprise;
import com.business.system.domain.BaHiTransport;
import com.business.system.domain.BaTransport;
import com.business.system.domain.dto.CountTransportDTO;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * 运输Mapper接口
 *
 * @author ljb
 * @date 2022-12-13
 */
@Repository
public interface BaHiTransportMapper extends BaseMapper<BaHiTransport>
{
    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    public BaHiTransport selectBaHiTransportById(String id);

    /**
     * 查询运输列表
     *
     * @param baHiTransport 运输
     * @return 运输集合
     */
    public List<BaHiTransport> selectBaHiTransportList(BaHiTransport baHiTransport);


    /**
     * 查询运输列表
     * @param baHiTransport
     * @return
     */
    public List<BaHiTransport> selectBaHiTransport(BaHiTransport baHiTransport);

    /**
     * 新增运输
     *
     * @param baHiTransport 运输
     * @return 结果
     */
    public int insertBaHiTransport(BaHiTransport baHiTransport);

    /**
     * 修改运输
     *
     * @param baHiTransport 运输
     * @return 结果
     */
    public int updateBaHiTransport(BaHiTransport baHiTransport);

    /**
     * 修改运输
     *
     * @param baHiTransport 运输
     * @return 结果
     */
    public int updateBaHiTransportChange(BaHiTransport baHiTransport);

    /**
     * 删除运输
     *
     * @param id 运输ID
     * @return 结果
     */
    public int deleteBaHiTransportById(String id);

    /**
     * 批量删除运输
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaHiTransportByIds(String[] ids);

    /**
     * 查询火运管理变更历史列表
     *
     * @param baHiTransport 火运管理
     * @return 用煤企业集合
     */
    public List<BaHiTransport> selectChangeBaHiTransportList(BaHiTransport baHiTransport);

}
