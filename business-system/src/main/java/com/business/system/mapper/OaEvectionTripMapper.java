package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaEvectionTrip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 出差行程Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaEvectionTripMapper extends BaseMapper<OaEvectionTrip>
{
    /**
     * 查询出差行程
     *
     * @param id 出差行程ID
     * @return 出差行程
     */
    public OaEvectionTrip selectOaEvectionTripById(String id);

    /**
     * 查询出差行程列表
     *
     * @param oaEvectionTrip 出差行程
     * @return 出差行程集合
     */
    public List<OaEvectionTrip> selectOaEvectionTripList(OaEvectionTrip oaEvectionTrip);

    /**
     * 新增出差行程
     *
     * @param oaEvectionTrip 出差行程
     * @return 结果
     */
    public int insertOaEvectionTrip(OaEvectionTrip oaEvectionTrip);

    /**
     * 修改出差行程
     *
     * @param oaEvectionTrip 出差行程
     * @return 结果
     */
    public int updateOaEvectionTrip(OaEvectionTrip oaEvectionTrip);

    /**
     * 删除出差行程
     *
     * @param id 出差行程ID
     * @return 结果
     */
    public int deleteOaEvectionTripById(String id);

    /**
     * 批量删除出差行程
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaEvectionTripByIds(String[] ids);

    /**
     * 出差报表统计
     * @param oaEvectionTrip
     * @return
     */
    public List<OaEvectionTrip> selectOaEvectionTrip(OaEvectionTrip oaEvectionTrip);
}
