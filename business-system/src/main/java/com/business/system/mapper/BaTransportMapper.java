package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaTransport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.CountTransportDTO;
import org.springframework.stereotype.Repository;

/**
 * 运输Mapper接口
 *
 * @author ljb
 * @date 2022-12-13
 */
@Repository
public interface BaTransportMapper extends BaseMapper<BaTransport>
{
    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    public BaTransport selectBaTransportById(String id);

    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输集合
     */
    public List<BaTransport> selectBaTransportList(BaTransport baTransport);
    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输集合
     */
    public List<BaTransport> selectBaTransport(BaTransport baTransport);
    public List<BaTransport> selectBaTransport1(BaTransport baTransport);


    /**
     * 查询运输列表
     * @param baTransport
     * @return
     */
    public List<BaTransport> selectTransport(BaTransport baTransport);

    /**
     * 新增运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    public int insertBaTransport(BaTransport baTransport);

    /**
     * 修改运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    public int updateBaTransport(BaTransport baTransport);

    /**
     * 修改运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    public int updateBaTransportChange(BaTransport baTransport);

    /**
     * 删除运输
     *
     * @param id 运输ID
     * @return 结果
     */
    public int deleteBaTransportById(String id);

    /**
     * 批量删除运输
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaTransportByIds(String[] ids);

    /**
     * 统计发运量
     *
     * @param id 运输ID
     * @return 运输
     */
    public BaTransport selectTransportSum(BaTransport baTransport);

    /**
     * PC端首页累计发运量统计
     *
     * @return 运输
     */
    public BigDecimal countTransportList(CountTransportDTO countTransportDTO);

    /**
     * 查询多租户授权信息运输列表
     * @param baTransport
     * @return
     */
    public List<BaTransport> selectTransportAuthorized(BaTransport baTransport);
}
