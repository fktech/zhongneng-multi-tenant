package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDeclareDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaInvoiceDetail;
import org.springframework.stereotype.Repository;

/**
 * 计划申报Mapper接口
 *
 * @author single
 * @date 2023-03-21
 */
@Repository
public interface BaDeclareDetailMapper extends BaseMapper<BaDeclareDetail>
{
    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    public BaDeclareDetail selectBaDeclareDetailById(String id);

    /**
     * 查询计划申报列表
     *
     * @param baDeclareDetail 计划申报
     * @return 计划申报集合
     */
    public List<BaDeclareDetail> selectBaDeclareDetailList(BaDeclareDetail baDeclareDetail);

    /**
     * 新增计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    public int insertBaDeclareDetail(BaDeclareDetail baDeclareDetail);

    /**
     * 批量新增发票信息列
     *
     * @param list 发票信息详情列表
     * @return 结果
     */
    public int batchInsertBaDeclareDetail(List<BaDeclareDetail> list);

    /**
     * 修改计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    public int updateBaDeclareDetail(BaDeclareDetail baDeclareDetail);

    /**
     * 删除计划申报
     *
     * @param id 计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareDetailById(String id);

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDeclareDetailByIds(String[] ids);

    /**
     * 根据ID删除计划申报详情
     *
     * @param declareId 计划申报详情ID
     * @return 结果
     */
    public int deleteBaDeclareDetailByDeclareId(String declareId);
}
