package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaUserMail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 通讯录Mapper接口
 *
 * @author ljb
 * @date 2023-06-02
 */
@Repository
public interface BaUserMailMapper extends BaseMapper<BaUserMail>
{
    /**
     * 查询通讯录
     *
     * @param id 通讯录ID
     * @return 通讯录
     */
    public BaUserMail selectBaUserMailById(String id);

    /**
     * 查询通讯录列表
     *
     * @param baUserMail 通讯录
     * @return 通讯录集合
     */
    public List<BaUserMail> selectBaUserMailList(BaUserMail baUserMail);

    public List<BaUserMail> selectBaUserMail(BaUserMail baUserMail);

    /**
     * 新增通讯录
     *
     * @param baUserMail 通讯录
     * @return 结果
     */
    public int insertBaUserMail(BaUserMail baUserMail);

    /**
     * 修改通讯录
     *
     * @param baUserMail 通讯录
     * @return 结果
     */
    public int updateBaUserMail(BaUserMail baUserMail);

    /**
     * 删除通讯录
     *
     * @param id 通讯录ID
     * @return 结果
     */
    public int deleteBaUserMailById(String id);

    /**
     * 批量删除通讯录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaUserMailByIds(String[] ids);
}
