package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;

import com.business.system.domain.BaContract;
import com.business.system.domain.BaSettlement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import org.springframework.stereotype.Repository;

/**
 * 结算信息Mapper接口
 *
 * @author ljb
 * @date 2022-12-26
 */
@Repository
public interface BaSettlementMapper extends BaseMapper<BaSettlement>
{
    /**
     * 查询结算信息
     *
     * @param id 结算信息ID
     * @return 结算信息
     */
    public BaSettlement selectBaSettlementById(String id);

    /**
     * 查询结算信息列表
     *
     * @param baSettlement 结算信息
     * @return 结算信息集合
     */
    public List<BaSettlement> selectBaSettlementList(BaSettlement baSettlement);

    public List<BaSettlement> selectBaSettlement(BaSettlement baSettlement);

    /**
     * 新增结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    public int insertBaSettlement(BaSettlement baSettlement);

    /**
     * 修改结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    public int updateBaSettlement(BaSettlement baSettlement);

    /**
     * 删除结算信息
     *
     * @param id 结算信息ID
     * @return 结果
     */
    public int deleteBaSettlementById(String id);

    /**
     * 批量删除结算信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaSettlementByIds(String[] ids);
    /**
     * 查询结算表中总数及金额
     */
    public NumsAndMoneyDTO selectBaSettlementforup(BaSettlement baSettlement);
    /**
     * 查询结算表中总数及金本月度
     */
    public NumsAndMoneyDTO selectBaSettlementforMonth(BaSettlement baSettlement);
    /**
     * * 查询结算表中总数及金本年度
     */
    public NumsAndMoneyDTO selectBaSettlementforYear(BaSettlement baSettlement);

    /**
     * 统计结算和收款
     */
    public BigDecimal sumBaSettlementAndCollection(BaSettlement baSettlement);

    /**
     * 统计结算
     *
     * @param baSettlement 合同对象
     * @return 合同
     */
    public int countBaSettlementList(BaSettlement baSettlement);

    /**
     * 查询多租户授权信息结算信息列表
     * @param baSettlement
     * @return
     */
    public List<BaSettlement> selectBaSettlementAuthorized(BaSettlement baSettlement);
}
