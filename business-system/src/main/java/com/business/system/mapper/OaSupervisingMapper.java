package com.business.system.mapper;

import java.util.List;

import com.business.system.domain.OaFee;
import com.business.system.domain.OaSupervising;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 督办Mapper接口
 *
 * @author ljb
 * @date 2023-11-20
 */
@Repository
public interface OaSupervisingMapper extends BaseMapper<OaSupervising>
{
    /**
     * 查询督办
     *
     * @param id 督办ID
     * @return 督办
     */
    public OaSupervising selectOaSupervisingById(String id);

    /**
     * 查询督办列表
     *
     * @param oaSupervising 督办
     * @return 督办集合
     */
    public List<OaSupervising> selectOaSupervisingList(OaSupervising oaSupervising);

    /**
     * 统计督办申请列表
     *
     * @param oaSupervising 督办申请
     * @return 督办申请集合
     */
    public Long countOaSupervisingList(OaSupervising oaSupervising);

    /**
     * 新增督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    public int insertOaSupervising(OaSupervising oaSupervising);

    /**
     * 修改督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    public int updateOaSupervising(OaSupervising oaSupervising);

    /**
     * 删除督办
     *
     * @param id 督办ID
     * @return 结果
     */
    public int deleteOaSupervisingById(String id);

    /**
     * 批量删除督办
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaSupervisingByIds(String[] ids);

}
