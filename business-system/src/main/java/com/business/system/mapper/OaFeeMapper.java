package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaFee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 费用申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaFeeMapper extends BaseMapper<OaFee>
{
    /**
     * 查询费用申请
     *
     * @param id 费用申请ID
     * @return 费用申请
     */
    public OaFee selectOaFeeById(String id);

    /**
     * 查询费用申请列表
     *
     * @param oaFee 费用申请
     * @return 费用申请集合
     */
    public List<OaFee> selectOaFeeList(OaFee oaFee);

    /**
     * 统计费用申请列表
     *
     * @param oaFee 费用申请
     * @return 费用申请集合
     */
    public Long countOaFeeList(OaFee oaFee);

    /**
     * 新增费用申请
     *
     * @param oaFee 费用申请
     * @return 结果
     */
    public int insertOaFee(OaFee oaFee);

    /**
     * 修改费用申请
     *
     * @param oaFee 费用申请
     * @return 结果
     */
    public int updateOaFee(OaFee oaFee);

    /**
     * 删除费用申请
     *
     * @param id 费用申请ID
     * @return 结果
     */
    public int deleteOaFeeById(String id);

    /**
     * 批量删除费用申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaFeeByIds(String[] ids);
}
