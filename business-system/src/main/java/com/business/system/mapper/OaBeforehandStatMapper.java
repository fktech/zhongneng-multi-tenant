package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.OaBeforehandStat;
import com.business.system.domain.OaClock;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.vo.OaBeforehandStatVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 事前申请Mapper接口
 *
 * @author single
 * @date 2023-11-27
 */
@Repository
public interface OaBeforehandStatMapper extends BaseMapper<OaBeforehandStat>
{
    /**
     * 申请部门分布
     *
     * @return 部门分布集合
     */
    public List<OaBeforehandStatVO> applyDeptStat(OaBeforehandStat oaBeforehandStat);
}
