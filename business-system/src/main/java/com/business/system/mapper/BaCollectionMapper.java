package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaCollection;
import com.business.system.domain.BaInvoice;
import com.business.system.domain.BaPayment;
import com.business.system.domain.dto.BaCollectionVoucherDTO;
import com.business.system.domain.vo.BaCollectionStatVO;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * 收款信息Mapper接口
 *
 * @author single
 * @date 2022-12-26
 */
@Repository
public interface BaCollectionMapper extends BaseMapper<BaCollection>
{
    /**
     * 查询收款信息
     *
     * @param id 收款信息ID
     * @return 收款信息
     */
    public BaCollection selectBaCollectionById(String id);
    /**
     * 查询本年度收款金额
     *
     * @param
     * @return 收款信息
     */
    public BigDecimal selectBaCollectionByyear(BaCollection baCollection);
    /**
     * 查询本年度收款金额
     *
     * @param
     * @return 收款信息
     */
    public List<BaCollection> selectByyearandbiusstype(BaCollection baCollection);


    /**
     * 查询收款信息列表
     *
     * @param baCollection 收款信息
     * @return 收款信息集合
     */
    public List<BaCollection> selectBaCollectionList(BaCollection baCollection);

    /**
     * 认领收款查询
     * @param baCollection
     * @return
     */
    public List<BaCollectionVoucherDTO> claimCollection(BaCollection baCollection);

    public List<BaCollectionVoucherDTO> selectCollection(BaCollection baCollection);

   /* public List<BaCollectionVoucherDTO> selectBaClaimHistoryListBycontractName(BaCollection baCollection);*/
    /**
     * 新增收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    public int insertBaCollection(BaCollection baCollection);

    /**
     * 修改收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    public int updateBaCollection(BaCollection baCollection);

    /**
     * 删除收款信息
     *
     * @param id 收款信息ID
     * @return 结果
     */
    public int deleteBaCollectionById(String id);

    /**
     * 批量删除收款信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCollectionByIds(String[] ids);

    /**
     * 可视化大屏统计应收账款
     */
    public BaCollectionStatVO sumBaCollection(BaCollection baCollection);

    /**
     * 查询多租户授权信息收款信息列表
     */
    public List<BaCollectionVoucherDTO> selectCollectionAuthorized(BaCollection baCollection);
}
