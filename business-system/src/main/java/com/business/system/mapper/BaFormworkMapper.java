package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaFormwork;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 模板Mapper接口
 *
 * @author ljb
 * @date 2023-03-14
 */
@Repository
public interface BaFormworkMapper extends BaseMapper<BaFormwork>
{
    /**
     * 查询模板
     *
     * @param id 模板ID
     * @return 模板
     */
    public BaFormwork selectBaFormworkById(String id);

    /**
     * 查询模板列表
     *
     * @param baFormwork 模板
     * @return 模板集合
     */
    public List<BaFormwork> selectBaFormworkList(BaFormwork baFormwork);

    /**
     * 新增模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    public int insertBaFormwork(BaFormwork baFormwork);

    /**
     * 修改模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    public int updateBaFormwork(BaFormwork baFormwork);

    /**
     * 删除模板
     *
     * @param id 模板ID
     * @return 结果
     */
    public int deleteBaFormworkById(String id);

    /**
     * 批量删除模板
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaFormworkByIds(String[] ids);
}
