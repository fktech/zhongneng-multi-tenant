package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAttendance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 打卡Mapper接口
 *
 * @author single
 * @date 2023-10-11
 */
@Repository
public interface BaAttendanceMapper extends BaseMapper<BaAttendance>
{
    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    public BaAttendance selectBaAttendanceById(String id);

    /**
     * 查询打卡列表
     *
     * @param baAttendance 打卡
     * @return 打卡集合
     */
    public List<BaAttendance> selectBaAttendanceList(BaAttendance baAttendance);

    /**
     * 新增打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    public int insertBaAttendance(BaAttendance baAttendance);

    /**
     * 修改打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    public int updateBaAttendance(BaAttendance baAttendance);

    /**
     * 删除打卡
     *
     * @param id 打卡ID
     * @return 结果
     */
    public int deleteBaAttendanceById(String id);

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAttendanceByIds(String[] ids);
}
