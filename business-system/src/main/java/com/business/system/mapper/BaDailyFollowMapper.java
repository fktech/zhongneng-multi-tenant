package com.business.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaDailyFollow;
import org.springframework.stereotype.Repository;

/**
 * 日报关注Mapper接口
 *
 * @author ljb
 * @date 2023-11-08
 */
@Repository
public interface BaDailyFollowMapper extends BaseMapper<BaDailyFollow>
{
    /**
     * 查询日报关注
     *
     * @param id 日报关注ID
     * @return 日报关注
     */
    public BaDailyFollow selectBaDailyFollowById(String id);

    /**
     * 查询日报关注列表
     *
     * @param baDailyFollow 日报关注
     * @return 日报关注集合
     */
    public List<BaDailyFollow> selectBaDailyFollowList(BaDailyFollow baDailyFollow);

    /**
     * 新增日报关注
     *
     * @param baDailyFollow 日报关注
     * @return 结果
     */
    public int insertBaDailyFollow(BaDailyFollow baDailyFollow);

    /**
     * 修改日报关注
     *
     * @param baDailyFollow 日报关注
     * @return 结果
     */
    public int updateBaDailyFollow(BaDailyFollow baDailyFollow);

    /**
     * 删除日报关注
     *
     * @param id 日报关注ID
     * @return 结果
     */
    public int deleteBaDailyFollowById(String id);

    /**
     * 批量删除日报关注
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDailyFollowByIds(String[] ids);
}
