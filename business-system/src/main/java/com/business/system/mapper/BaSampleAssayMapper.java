package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaSampleAssay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 取样化验Mapper接口
 *
 * @author ljb
 * @date 2024-01-06
 */
@Repository
public interface BaSampleAssayMapper extends BaseMapper<BaSampleAssay>
{
    /**
     * 查询取样化验
     *
     * @param id 取样化验ID
     * @return 取样化验
     */
    public BaSampleAssay selectBaSampleAssayById(String id);

    /**
     * 查询取样化验列表
     *
     * @param baSampleAssay 取样化验
     * @return 取样化验集合
     */
    public List<BaSampleAssay> selectBaSampleAssayList(BaSampleAssay baSampleAssay);

    /**
     * 新增取样化验
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int insertBaSampleAssay(BaSampleAssay baSampleAssay);

    /**
     * 修改取样化验
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int updateBaSampleAssay(BaSampleAssay baSampleAssay);

    /**
     * 删除取样化验
     *
     * @param id 取样化验ID
     * @return 结果
     */
    public int deleteBaSampleAssayById(String id);

    /**
     * 批量删除取样化验
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaSampleAssayByIds(String[] ids);
}
