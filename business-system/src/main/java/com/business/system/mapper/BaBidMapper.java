package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaBid;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 投标管理Mapper接口
 *
 * @author ljb
 * @date 2022-12-06
 */
@Repository
public interface BaBidMapper extends BaseMapper<BaBid>
{
    /**
     * 查询投标管理
     *
     * @param id 投标管理ID
     * @return 投标管理
     */
    public BaBid selectBaBidById(String id);

    /**
     * 查询投标管理列表
     *
     * @param baBid 投标管理
     * @return 投标管理集合
     */
    public List<BaBid> selectBaBidList(BaBid baBid);

    /**
     * 新增投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    public int insertBaBid(BaBid baBid);

    /**
     * 修改投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    public int updateBaBid(BaBid baBid);

    /**
     * 删除投标管理
     *
     * @param id 投标管理ID
     * @return 结果
     */
    public int deleteBaBidById(String id);

    /**
     * 批量删除投标管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaBidByIds(String[] ids);
}
