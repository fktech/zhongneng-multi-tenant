package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementInvoiceDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 无车承运开票详情Mapper接口
 *
 * @author single
 * @date 2023-04-13
 */
@Repository
public interface BaAutomobileSettlementInvoiceDetailMapper extends BaseMapper<BaAutomobileSettlementInvoiceDetail>
{
    /**
     * 查询无车承运开票详情
     *
     * @param id 无车承运开票详情ID
     * @return 无车承运开票详情
     */
    public BaAutomobileSettlementInvoiceDetail selectBaAutomobileSettlementInvoiceDetailById(String id);

    /**
     * 查询无车承运开票详情列表
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 无车承运开票详情集合
     */
    public List<BaAutomobileSettlementInvoiceDetail> selectBaAutomobileSettlementInvoiceDetailList(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 新增无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 修改无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 删除无车承运开票详情
     *
     * @param id 无车承运开票详情ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceDetailById(String id);

    /**
     * 批量删除无车承运开票详情
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceDetailByIds(String[] ids);
}
