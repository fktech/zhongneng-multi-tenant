package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaSeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 用印Mapper接口
 *
 * @author ljb
 * @date 2023-03-14
 */
@Repository
public interface BaSealMapper extends BaseMapper<BaSeal>
{
    /**
     * 查询用印
     *
     * @param id 用印ID
     * @return 用印
     */
    public BaSeal selectBaSealById(String id);

    /**
     * 查询用印列表
     *
     * @param baSeal 用印
     * @return 用印集合
     */
    public List<BaSeal> selectBaSealList(BaSeal baSeal);

    /**
     * 新增用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    public int insertBaSeal(BaSeal baSeal);

    /**
     * 修改用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    public int updateBaSeal(BaSeal baSeal);

    /**
     * 删除用印
     *
     * @param id 用印ID
     * @return 结果
     */
    public int deleteBaSealById(String id);

    /**
     * 批量删除用印
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaSealByIds(String[] ids);

    /**
     * 查询多租户授权信息业务用印列表
     */
    public List<BaSeal> selectBaSealAuthorizedList(BaSeal baSeal);
}
