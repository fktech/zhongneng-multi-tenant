package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaMaintenanceExpansion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 业务维护/拓展Mapper接口
 *
 * @author ljb
 * @date 2023-11-13
 */
@Repository
public interface OaMaintenanceExpansionMapper extends BaseMapper<OaMaintenanceExpansion>
{
    /**
     * 查询业务维护/拓展
     *
     * @param id 业务维护/拓展ID
     * @return 业务维护/拓展
     */
    public OaMaintenanceExpansion selectOaMaintenanceExpansionById(String id);

    /**
     * 查询业务维护/拓展列表
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 业务维护/拓展集合
     */
    public List<OaMaintenanceExpansion> selectOaMaintenanceExpansionList(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 统计业务维护/拓展列表
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 业务维护/拓展集合
     */
    public Long countOaMaintenanceExpansionList(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 新增业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    public int insertOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 修改业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    public int updateOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 删除业务维护/拓展
     *
     * @param id 业务维护/拓展ID
     * @return 结果
     */
    public int deleteOaMaintenanceExpansionById(String id);

    /**
     * 批量删除业务维护/拓展
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaMaintenanceExpansionByIds(String[] ids);

    /**
     * 获取所有提交人员
     */
    public List<String> submitUser(OaMaintenanceExpansion oaMaintenanceExpansion);
}
