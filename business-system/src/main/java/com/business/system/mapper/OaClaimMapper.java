package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaClaim;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 报销申请Mapper接口
 *
 * @author single
 * @date 2023-12-11
 */
@Repository
public interface OaClaimMapper extends BaseMapper<OaClaim>
{
    /**
     * 查询报销申请
     *
     * @param id 报销申请ID
     * @return 报销申请
     */
    public OaClaim selectOaClaimById(String id);

    /**
     * 查询报销申请列表
     *
     * @param oaClaim 报销申请
     * @return 报销申请集合
     */
    public List<OaClaim> selectOaClaimList(OaClaim oaClaim);

    /**
     * 新增报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    public int insertOaClaim(OaClaim oaClaim);

    /**
     * 修改报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    public int updateOaClaim(OaClaim oaClaim);

    /**
     * 删除报销申请
     *
     * @param id 报销申请ID
     * @return 结果
     */
    public int deleteOaClaimById(String id);

    /**
     * 批量删除报销申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaClaimByIds(String[] ids);
}
