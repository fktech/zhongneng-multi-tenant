package com.business.system.mapper;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaProcurementPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 采购计划Mapper接口
 *
 * @author ljb
 * @date 2023-08-31
 */
@Repository
public interface BaProcurementPlanMapper extends BaseMapper<BaProcurementPlan>
{
    /**
     * 查询采购计划
     *
     * @param id 采购计划ID
     * @return 采购计划
     */
    public BaProcurementPlan selectBaProcurementPlanById(String id);

    /**
     * 查询采购计划列表
     *
     * @param baProcurementPlan 采购计划
     * @return 采购计划集合
     */
    public List<BaProcurementPlan> selectBaProcurementPlanList(BaProcurementPlan baProcurementPlan);

    public List<BaProcurementPlan> baProcurementPlanList(BaProcurementPlan baProcurementPlan);

    /**
     * 新增采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    public int insertBaProcurementPlan(BaProcurementPlan baProcurementPlan);

    /**
     * 修改采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    public int updateBaProcurementPlan(BaProcurementPlan baProcurementPlan);

    /**
     * 删除采购计划
     *
     * @param id 采购计划ID
     * @return 结果
     */
    public int deleteBaProcurementPlanById(String id);

    /**
     * 批量删除采购计划
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaProcurementPlanByIds(String[] ids);

    /**
     * 采购申请名称
     */
    public List<BaProcurementPlan> purchaseRequestName(BaProcurementPlan baProcurementPlan);

    /**
     * 统计采购计划
     *
     * @param baProcurementPlan 立项管理
     * @return 结果
     */
    public int countBaProcurementPlanList(BaProcurementPlan baProcurementPlan);

    /**
     * 查询多租户授权信息采购计划列表
     */
    public List<BaProcurementPlan> baProcurementPlanAuthorizedList(BaProcurementPlan baProcurementPlan);
}
