package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaEarlyWarning;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 预警Mapper接口
 *
 * @author single
 * @date 2024-05-29
 */
@Repository
public interface BaEarlyWarningMapper extends BaseMapper<BaEarlyWarning>
{
    /**
     * 查询预警
     *
     * @param id 预警ID
     * @return 预警
     */
    public BaEarlyWarning selectBaEarlyWarningById(String id);

    /**
     * 查询预警列表
     *
     * @param baEarlyWarning 预警
     * @return 预警集合
     */
    public List<BaEarlyWarning> selectBaEarlyWarningList(BaEarlyWarning baEarlyWarning);

    //合同启动维度
    public List<BaEarlyWarning> selectBaEarlyWarning(BaEarlyWarning baEarlyWarning);

    //立项维度
    public List<BaEarlyWarning> selectEarlyWarning(BaEarlyWarning baEarlyWarning);
    /**
     * 新增预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    public int insertBaEarlyWarning(BaEarlyWarning baEarlyWarning);

    /**
     * 修改预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    public int updateBaEarlyWarning(BaEarlyWarning baEarlyWarning);

    /**
     * 删除预警
     *
     * @param id 预警ID
     * @return 结果
     */
    public int deleteBaEarlyWarningById(String id);

    /**
     * 批量删除预警
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaEarlyWarningByIds(String[] ids);
}
