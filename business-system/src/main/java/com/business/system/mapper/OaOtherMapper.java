package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaOther;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 其他申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaOtherMapper extends BaseMapper<OaOther>
{
    /**
     * 查询其他申请
     *
     * @param id 其他申请ID
     * @return 其他申请
     */
    public OaOther selectOaOtherById(String id);

    /**
     * 查询其他申请列表
     *
     * @param oaOther 其他申请
     * @return 其他申请集合
     */
    public List<OaOther> selectOaOtherList(OaOther oaOther);

    /**
     * 统计其他申请列表
     *
     * @param oaOther 其他申请
     * @return 其他申请集合
     */
    public Long countOaOtherList(OaOther oaOther);

    /**
     * 新增其他申请
     *
     * @param oaOther 其他申请
     * @return 结果
     */
    public int insertOaOther(OaOther oaOther);

    /**
     * 修改其他申请
     *
     * @param oaOther 其他申请
     * @return 结果
     */
    public int updateOaOther(OaOther oaOther);

    /**
     * 删除其他申请
     *
     * @param id 其他申请ID
     * @return 结果
     */
    public int deleteOaOtherById(String id);

    /**
     * 批量删除其他申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaOtherByIds(String[] ids);
}
