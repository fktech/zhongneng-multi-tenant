package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaRailwayPlatform;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 铁路站台Mapper接口
 *
 * @author ljb
 * @date 2022-11-29
 */
@Repository
public interface BaRailwayPlatformMapper extends BaseMapper<BaRailwayPlatform>
{
    /**
     * 查询铁路站台
     *
     * @param id 铁路站台ID
     * @return 铁路站台
     */
    public BaRailwayPlatform selectBaRailwayPlatformById(String id);

    /**
     * 查询铁路站台列表
     *
     * @param baRailwayPlatform 铁路站台
     * @return 铁路站台集合
     */
    public List<BaRailwayPlatform> selectBaRailwayPlatformList(BaRailwayPlatform baRailwayPlatform);

    /**
     * 新增铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    public int insertBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform);

    /**
     * 修改铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    public int updateBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform);

    /**
     * 删除铁路站台
     *
     * @param id 铁路站台ID
     * @return 结果
     */
    public int deleteBaRailwayPlatformById(String id);

    /**
     * 批量删除铁路站台
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaRailwayPlatformByIds(String[] ids);
}
