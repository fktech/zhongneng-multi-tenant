package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDepotItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 出入库副Mapper接口
 *
 * @author ljb
 * @date 2023-04-23
 */
@Repository
public interface BaDepotItemMapper extends BaseMapper<BaDepotItem>
{
    /**
     * 查询出入库副
     *
     * @param id 出入库副ID
     * @return 出入库副
     */
    public BaDepotItem selectBaDepotItemById(String id);

    /**
     * 查询出入库副列表
     *
     * @param baDepotItem 出入库副
     * @return 出入库副集合
     */
    public List<BaDepotItem> selectBaDepotItemList(BaDepotItem baDepotItem);

    /**
     * 新增出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    public int insertBaDepotItem(BaDepotItem baDepotItem);

    /**
     * 修改出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    public int updateBaDepotItem(BaDepotItem baDepotItem);

    /**
     * 删除出入库副
     *
     * @param id 出入库副ID
     * @return 结果
     */
    public int deleteBaDepotItemById(String id);

    /**
     * 批量删除出入库副
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotItemByIds(String[] ids);
}
