package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 单位Mapper接口
 *
 * @author ljb
 * @date 2022-11-30
 */
@Repository
public interface BaUnitMapper extends BaseMapper<BaUnit>
{
    /**
     * 查询单位
     *
     * @param id 单位ID
     * @return 单位
     */
    public BaUnit selectBaUnitById(String id);

    /**
     * 查询单位列表
     *
     * @param baUnit 单位
     * @return 单位集合
     */
    public List<BaUnit> selectBaUnitList(BaUnit baUnit);

    /**
     * 新增单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    public int insertBaUnit(BaUnit baUnit);

    /**
     * 修改单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    public int updateBaUnit(BaUnit baUnit);

    /**
     * 删除单位
     *
     * @param id 单位ID
     * @return 结果
     */
    public int deleteBaUnitById(String id);

    /**
     * 批量删除单位
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaUnitByIds(String[] ids);
}
