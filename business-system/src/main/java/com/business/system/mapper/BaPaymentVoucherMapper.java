package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaPaymentVoucher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 付款凭证Mapper接口
 *
 * @author single
 * @date 2023-07-15
 */
@Repository
public interface BaPaymentVoucherMapper extends BaseMapper<BaPaymentVoucher>
{
    /**
     * 查询付款凭证
     *
     * @param id 付款凭证ID
     * @return 付款凭证
     */
    public BaPaymentVoucher selectBaPaymentVoucherById(String id);

    /**
     * 查询付款凭证列表
     *
     * @param baPaymentVoucher 付款凭证
     * @return 付款凭证集合
     */
    public List<BaPaymentVoucher> selectBaPaymentVoucherList(BaPaymentVoucher baPaymentVoucher);

    /**
     * 新增付款凭证
     *
     * @param baPaymentVoucher 付款凭证
     * @return 结果
     */
    public int insertBaPaymentVoucher(BaPaymentVoucher baPaymentVoucher);

    /**
     * 修改付款凭证
     *
     * @param baPaymentVoucher 付款凭证
     * @return 结果
     */
    public int updateBaPaymentVoucher(BaPaymentVoucher baPaymentVoucher);

    /**
     * 删除付款凭证
     *
     * @param id 付款凭证ID
     * @return 结果
     */
    public int deleteBaPaymentVoucherById(String id);

    /**
     * 批量删除付款凭证
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaPaymentVoucherByIds(String[] ids);
}
