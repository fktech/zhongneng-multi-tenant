package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaProject;
import com.business.system.domain.vo.BaDriveAi;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 立项管理Mapper接口
 *
 * @author ljb
 * @date 2022-12-02
 */
@Repository
public interface BaDriveAiMapper extends BaseMapper<BaProject>
{


    /**
     * 立项项目依照业务类型分类查询
     *
     * @param
     * @return 结果
     */
    public List<BaDriveAi> businesstype();
}
