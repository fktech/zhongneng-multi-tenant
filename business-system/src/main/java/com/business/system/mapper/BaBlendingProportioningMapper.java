package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaBlendingProportioning;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 配煤配比Mapper接口
 *
 * @author ljb
 * @date 2024-01-05
 */
@Repository
public interface BaBlendingProportioningMapper extends BaseMapper<BaBlendingProportioning>
{
    /**
     * 查询配煤配比
     *
     * @param id 配煤配比ID
     * @return 配煤配比
     */
    public BaBlendingProportioning selectBaBlendingProportioningById(String id);

    /**
     * 查询配煤配比列表
     *
     * @param baBlendingProportioning 配煤配比
     * @return 配煤配比集合
     */
    public List<BaBlendingProportioning> selectBaBlendingProportioningList(BaBlendingProportioning baBlendingProportioning);

    /**
     * 新增配煤配比
     *
     * @param baBlendingProportioning 配煤配比
     * @return 结果
     */
    public int insertBaBlendingProportioning(BaBlendingProportioning baBlendingProportioning);

    /**
     * 修改配煤配比
     *
     * @param baBlendingProportioning 配煤配比
     * @return 结果
     */
    public int updateBaBlendingProportioning(BaBlendingProportioning baBlendingProportioning);

    /**
     * 删除配煤配比
     *
     * @param id 配煤配比ID
     * @return 结果
     */
    public int deleteBaBlendingProportioningById(String id);

    /**
     * 批量删除配煤配比
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaBlendingProportioningByIds(String[] ids);
}
