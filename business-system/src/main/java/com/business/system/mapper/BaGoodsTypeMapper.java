package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaGoodsType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 商品分类Mapper接口
 *
 * @author ljb
 * @date 2022-11-30
 */
@Repository
public interface BaGoodsTypeMapper extends BaseMapper<BaGoodsType>
{
    /**
     * 查询商品分类
     *
     * @param id 商品分类ID
     * @return 商品分类
     */
    public BaGoodsType selectBaGoodsTypeById(String id);

    /**
     * 查询商品分类列表
     *
     * @param baGoodsType 商品分类
     * @return 商品分类集合
     */
    public List<BaGoodsType> selectBaGoodsTypeList(BaGoodsType baGoodsType);

    /**
     * 新增商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    public int insertBaGoodsType(BaGoodsType baGoodsType);

    /**
     * 修改商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    public int updateBaGoodsType(BaGoodsType baGoodsType);

    /**
     * 删除商品分类
     *
     * @param id 商品分类ID
     * @return 结果
     */
    public int deleteBaGoodsTypeById(String id);

    /**
     * 批量删除商品分类
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaGoodsTypeByIds(String[] ids);
}
