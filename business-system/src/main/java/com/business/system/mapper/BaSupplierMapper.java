package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaSupplier;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 供应商Mapper接口
 *
 * @author ljb
 * @date 2022-11-28
 */
@Repository
public interface BaSupplierMapper extends BaseMapper<BaSupplier>
{
    /**
     * 查询供应商
     *
     * @param id 供应商ID
     * @return 供应商
     */
    public BaSupplier selectBaSupplierById(String id);

    /**
     * 查询供应商列表
     *
     * @param baSupplier 供应商
     * @return 供应商集合
     */
    public List<BaSupplier> selectBaSupplierList(BaSupplier baSupplier);

    /**
     * 新增供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    public int insertBaSupplier(BaSupplier baSupplier);

    /**
     * 修改供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    public int updateBaSupplier(BaSupplier baSupplier);

    /**
     * 删除供应商
     *
     * @param id 供应商ID
     * @return 结果
     */
    public int deleteBaSupplierById(String id);

    /**
     * 批量删除供应商
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaSupplierByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth(String tenantId);
}
