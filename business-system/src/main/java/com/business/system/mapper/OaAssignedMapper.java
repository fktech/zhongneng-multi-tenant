package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaAssigned;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 交办数据Mapper接口
 *
 * @author ljb
 * @date 2023-11-21
 */
@Repository
public interface OaAssignedMapper extends BaseMapper<OaAssigned>
{
    /**
     * 查询交办数据
     *
     * @param id 交办数据ID
     * @return 交办数据
     */
    public OaAssigned selectOaAssignedById(String id);

    /**
     * 查询交办数据列表
     *
     * @param oaAssigned 交办数据
     * @return 交办数据集合
     */
    public List<OaAssigned> selectOaAssignedList(OaAssigned oaAssigned);

    public List<OaAssigned> oaAssignedList(OaAssigned oaAssigned);

    /**
     * 新增交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    public int insertOaAssigned(OaAssigned oaAssigned);

    /**
     * 修改交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    public int updateOaAssigned(OaAssigned oaAssigned);

    /**
     * 删除交办数据
     *
     * @param id 交办数据ID
     * @return 结果
     */
    public int deleteOaAssignedById(String id);

    /**
     * 批量删除交办数据
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaAssignedByIds(String[] ids);
}
