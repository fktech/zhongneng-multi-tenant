package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.BaHiEnterprise;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用煤企业历史Mapper接口
 *
 * @author js
 * @date 2023-7-10
 */
@Repository
public interface BaHiEnterpriseMapper extends BaseMapper<BaHiEnterprise>
{
    /**
     * 查询用煤企业
     *
     * @param id 用煤企业ID
     * @return 用煤企业
     */
    public BaHiEnterprise selectBaHiEnterpriseById(String id);

    /**
     * 查询用煤企业列表
     *
     * @param baHiEnterprise 用煤企业
     * @return 用煤企业集合
     */
    public List<BaHiEnterprise> selectBaHiEnterpriseList(BaHiEnterprise baHiEnterprise);

    /**
     * 新增用煤企业
     *
     * @param baHiEnterprise 用煤企业
     * @return 结果
     */
    public int insertBaHiEnterprise(BaHiEnterprise baHiEnterprise);

    /**
     * 修改用煤企业
     *
     * @param baHiEnterprise 用煤企业
     * @return 结果
     */
    public int updateBaHiEnterprise(BaHiEnterprise baHiEnterprise);

    /**
     * 删除用煤企业
     *
     * @param id 用煤企业ID
     * @return 结果
     */
    public int deleteBaHiEnterpriseById(String id);

    /**
     * 批量删除用煤企业
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaHiEnterpriseByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth();

    /**
     * 查询用煤企业变更历史列表
     *
     * @param baHiEnterprise 用煤企业
     * @return 用煤企业集合
     */
    public List<BaHiEnterprise> selectChangeBaHiEnterpriseList(BaHiEnterprise baHiEnterprise);

    /**
     * 查询用煤企业变更记录列表
     *
     * @param baHiEnterprise 用煤企业
     * @return 用煤企业集合
     */
    public List<BaHiEnterprise> selectChangeDataBaHiEnterpriseList(BaHiEnterprise baHiEnterprise);
}
