package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaEnterpriseRelevance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 终端企业关联Mapper接口
 *
 * @author ljb
 * @date 2023-03-01
 */
@Repository
public interface BaEnterpriseRelevanceMapper extends BaseMapper<BaEnterpriseRelevance>
{
    /**
     * 查询终端企业关联
     *
     * @param id 终端企业关联ID
     * @return 终端企业关联
     */
    public BaEnterpriseRelevance selectBaEnterpriseRelevanceById(String id);

    /**
     * 查询终端企业关联列表
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 终端企业关联集合
     */
    public List<BaEnterpriseRelevance> selectBaEnterpriseRelevanceList(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 新增终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    public int insertBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 修改终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    public int updateBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 删除终端企业关联
     *
     * @param id 终端企业关联ID
     * @return 结果
     */
    public int deleteBaEnterpriseRelevanceById(String id);

    /**
     * 批量删除终端企业关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaEnterpriseRelevanceByIds(String[] ids);
}
