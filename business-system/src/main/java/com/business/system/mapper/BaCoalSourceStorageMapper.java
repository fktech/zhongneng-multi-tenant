package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCoalSourceStorage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 煤源库Mapper接口
 *
 * @author single
 * @date 2024-01-25
 */
@Repository
public interface BaCoalSourceStorageMapper extends BaseMapper<BaCoalSourceStorage>
{
    /**
     * 查询煤源库
     *
     * @param id 煤源库ID
     * @return 煤源库
     */
    public BaCoalSourceStorage selectBaCoalSourceStorageById(String id);

    /**
     * 查询煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库集合
     */
    public List<BaCoalSourceStorage> selectBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 新增煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    public int insertBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 修改煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    public int updateBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 删除煤源库
     *
     * @param id 煤源库ID
     * @return 结果
     */
    public int deleteBaCoalSourceStorageById(String id);

    /**
     * 批量删除煤源库
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCoalSourceStorageByIds(String[] ids);

    /**
     * 统计煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库集合
     */
    public List<BaCoalSourceStorage> countBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage);
}
