package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaWorkTimes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 班次Mapper接口
 *
 * @author single
 * @date 2023-11-24
 */
@Repository
public interface OaWorkTimesMapper extends BaseMapper<OaWorkTimes>
{
    /**
     * 查询班次
     *
     * @param id 班次ID
     * @return 班次
     */
    public OaWorkTimes selectOaWorkTimesById(String id);

    /**
     * 查询班次列表
     *
     * @param oaWorkTimes 班次
     * @return 班次集合
     */
    public List<OaWorkTimes> selectOaWorkTimesList(OaWorkTimes oaWorkTimes);

    /**
     * 新增班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    public int insertOaWorkTimes(OaWorkTimes oaWorkTimes);

    /**
     * 修改班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    public int updateOaWorkTimes(OaWorkTimes oaWorkTimes);

    /**
     * 删除班次
     *
     * @param id 班次ID
     * @return 结果
     */
    public int deleteOaWorkTimesById(String id);

    /**
     * 批量删除班次
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaWorkTimesByIds(String[] ids);
}
