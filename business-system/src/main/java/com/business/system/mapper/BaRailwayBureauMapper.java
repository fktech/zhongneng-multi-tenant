package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaRailwayBureau;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 铁路局Mapper接口
 *
 * @author ljb
 * @date 2023-01-16
 */
@Repository
public interface BaRailwayBureauMapper extends BaseMapper<BaRailwayBureau>
{
    /**
     * 查询铁路局
     *
     * @param id 铁路局ID
     * @return 铁路局
     */
    public BaRailwayBureau selectBaRailwayBureauById(String id);

    /**
     * 查询铁路局列表
     *
     * @param baRailwayBureau 铁路局
     * @return 铁路局集合
     */
    public List<BaRailwayBureau> selectBaRailwayBureauList(BaRailwayBureau baRailwayBureau);

    /**
     * 新增铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    public int insertBaRailwayBureau(BaRailwayBureau baRailwayBureau);

    /**
     * 修改铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    public int updateBaRailwayBureau(BaRailwayBureau baRailwayBureau);

    /**
     * 删除铁路局
     *
     * @param id 铁路局ID
     * @return 结果
     */
    public int deleteBaRailwayBureauById(String id);

    /**
     * 批量删除铁路局
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaRailwayBureauByIds(String[] ids);
}
