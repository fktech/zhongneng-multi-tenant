package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaContractForward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 合同结转Mapper接口
 *
 * @author ljb
 * @date 2023-03-06
 */
@Repository
public interface BaContractForwardMapper extends BaseMapper<BaContractForward>
{
    /**
     * 查询合同结转
     *
     * @param id 合同结转ID
     * @return 合同结转
     */
    public BaContractForward selectBaContractForwardById(String id);

    /**
     * 查询合同结转列表
     *
     * @param baContractForward 合同结转
     * @return 合同结转集合
     */
    public List<BaContractForward> selectBaContractForwardList(BaContractForward baContractForward);

    /**
     * 查询合同结转列表
     * @param baContractForward
     * @return
     */
    public List<BaContractForward> selectBaContractForward(BaContractForward baContractForward);

    /**
     * 新增合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    public int insertBaContractForward(BaContractForward baContractForward);

    /**
     * 修改合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    public int updateBaContractForward(BaContractForward baContractForward);

    /**
     * 删除合同结转
     *
     * @param id 合同结转ID
     * @return 结果
     */
    public int deleteBaContractForwardById(String id);

    /**
     * 批量删除合同结转
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaContractForwardByIds(String[] ids);
}
