package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaFinancingHandle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 融资办理Mapper接口
 *
 * @author single
 * @date 2023-05-03
 */
@Repository
public interface BaFinancingHandleMapper extends BaseMapper<BaFinancingHandle>
{
    /**
     * 查询融资办理
     *
     * @param id 融资办理ID
     * @return 融资办理
     */
    public BaFinancingHandle selectBaFinancingHandleById(String id);

    /**
     * 查询融资办理列表
     *
     * @param baFinancingHandle 融资办理
     * @return 融资办理集合
     */
    public List<BaFinancingHandle> selectBaFinancingHandleList(BaFinancingHandle baFinancingHandle);

    /**
     * 新增融资办理
     *
     * @param baFinancingHandle 融资办理
     * @return 结果
     */
    public int insertBaFinancingHandle(BaFinancingHandle baFinancingHandle);

    /**
     * 修改融资办理
     *
     * @param baFinancingHandle 融资办理
     * @return 结果
     */
    public int updateBaFinancingHandle(BaFinancingHandle baFinancingHandle);

    /**
     * 删除融资办理
     *
     * @param id 融资办理ID
     * @return 结果
     */
    public int deleteBaFinancingHandleById(String id);

    /**
     * 批量删除融资办理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaFinancingHandleByIds(String[] ids);
}
