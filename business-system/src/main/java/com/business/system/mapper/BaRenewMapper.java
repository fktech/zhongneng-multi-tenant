package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaRenew;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 市场动态更新Mapper接口
 *
 * @author ljb
 * @date 2023-03-22
 */
@Repository
public interface BaRenewMapper extends BaseMapper<BaRenew>
{
    /**
     * 查询市场动态更新
     *
     * @param id 市场动态更新ID
     * @return 市场动态更新
     */
    public BaRenew selectBaRenewById(String id);

    /**
     * 查询市场动态更新列表
     *
     * @param baRenew 市场动态更新
     * @return 市场动态更新集合
     */
    public List<BaRenew> selectBaRenewList(BaRenew baRenew);

    /**
     * 新增市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    public int insertBaRenew(BaRenew baRenew);

    /**
     * 修改市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    public int updateBaRenew(BaRenew baRenew);

    /**
     * 删除市场动态更新
     *
     * @param id 市场动态更新ID
     * @return 结果
     */
    public int deleteBaRenewById(String id);

    /**
     * 批量删除市场动态更新
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaRenewByIds(String[] ids);
}
