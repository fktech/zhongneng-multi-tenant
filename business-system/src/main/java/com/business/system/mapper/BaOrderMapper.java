package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.vo.OrderStatVO;
import org.springframework.stereotype.Repository;

/**
 * 订单Mapper接口
 *
 * @author ljb
 * @date 2022-12-09
 */
@Repository
public interface BaOrderMapper extends BaseMapper<BaOrder>
{
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public BaOrder selectBaOrderById(String id);

    /**
     * 查询订单列表
     *
     * @param baOrder 订单
     * @return 订单集合
     */
    public List<BaOrder> selectBaOrderList(BaOrder baOrder);

    /**
     * 新增订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    public int insertBaOrder(BaOrder baOrder);

    /**
     * 修改订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    public int updateBaOrder(BaOrder baOrder);

    /**
     * 删除订单
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteBaOrderById(String id);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaOrderByIds(String[] ids);

    /**
     * 采购订单进度报表总量统计
     */
    public OrderStatVO statOrderInfo(BaOrder baOrder);

    /**
     * 销售订单进度报表总量统计
     */
    public OrderStatVO statOrderInfoSale(BaOrder baOrder);

    /**
     * 查询订单进度表(采购)列表
     *
     * @param baOrder 订单
     * @return 订单集合
     */
    public List<BaOrder> selectStatOrderInfoList(BaOrder baOrder);
}
