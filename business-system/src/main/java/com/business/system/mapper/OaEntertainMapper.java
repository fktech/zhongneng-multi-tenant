package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaEntertain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 招待申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-13
 */
@Repository
public interface OaEntertainMapper extends BaseMapper<OaEntertain>
{
    /**
     * 查询招待申请
     *
     * @param id 招待申请ID
     * @return 招待申请
     */
    public OaEntertain selectOaEntertainById(String id);

    /**
     * 查询招待申请列表
     *
     * @param oaEntertain 招待申请
     * @return 招待申请集合
     */
    public List<OaEntertain> selectOaEntertainList(OaEntertain oaEntertain);

    /**
     * 统计招待申请
     *
     * @param oaEntertain 招待申请
     * @return 招待申请集合
     */
    public Long countOaEntertainList(OaEntertain oaEntertain);

    /**
     * 新增招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    public int insertOaEntertain(OaEntertain oaEntertain);

    /**
     * 修改招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    public int updateOaEntertain(OaEntertain oaEntertain);

    /**
     * 删除招待申请
     *
     * @param id 招待申请ID
     * @return 结果
     */
    public int deleteOaEntertainById(String id);

    /**
     * 批量删除招待申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaEntertainByIds(String[] ids);
}
