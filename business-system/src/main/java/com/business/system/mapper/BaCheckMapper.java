package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaCheck;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 验收Mapper接口
 *
 * @author ljb
 * @date 2022-12-14
 */
@Repository
public interface BaCheckMapper extends BaseMapper<BaCheck>
{
    /**
     * 查询验收
     *
     * @param id 验收ID
     * @return 验收
     */
    public BaCheck selectBaCheckById(String id);

    /**
     * 查询验收列表
     *
     * @param baCheck 验收
     * @return 验收集合
     */
    public List<BaCheck> selectBaCheckList(BaCheck baCheck);

    /**
     * 新增验收
     *
     * @param baCheck 验收
     * @return 结果
     */
    public int insertBaCheck(BaCheck baCheck);
    /**
     * 查询发运总量
     *
     * @param
     * @return 结果
     */
    public BigDecimal selectnums();
    /**
     * 查询发运总量
     *
     * @param
     * @return 结果
     */
    public String selectnums1(String relationId);

    /**
     * 修改验收
     *
     * @param baCheck 验收
     * @return 结果
     */
    public int updateBaCheck(BaCheck baCheck);

    /**
     * 删除验收
     *
     * @param id 验收ID
     * @return 结果
     */
    public int deleteBaCheckById(String id);

    /**
     * 批量删除验收
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCheckByIds(String[] ids);
}
