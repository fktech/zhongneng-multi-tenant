package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDepot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 仓库信息Mapper接口
 *
 * @author ljb
 * @date 2022-11-30
 */
@Repository
public interface BaDepotMapper extends BaseMapper<BaDepot>
{
    /**
     * 查询仓库信息
     *
     * @param id 仓库信息ID
     * @return 仓库信息
     */
    public BaDepot selectBaDepotById(String id);

    /**
     * 查询仓库信息列表
     *
     * @param baDepot 仓库信息
     * @return 仓库信息集合
     */
    public List<BaDepot> selectBaDepotList(BaDepot baDepot);

    /**
     * 新增仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    public int insertBaDepot(BaDepot baDepot);

    /**
     * 修改仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    public int updateBaDepot(BaDepot baDepot);

    /**
     * 删除仓库信息
     *
     * @param id 仓库信息ID
     * @return 结果
     */
    public int deleteBaDepotById(String id);

    /**
     * 批量删除仓库信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotByIds(String[] ids);
}
