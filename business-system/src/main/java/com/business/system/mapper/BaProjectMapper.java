package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.CountBaProjectDTO;
import org.springframework.stereotype.Repository;

/**
 * 立项管理Mapper接口
 *
 * @author ljb
 * @date 2022-12-02
 */
@Repository
public interface BaProjectMapper extends BaseMapper<BaProject>
{
    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    public BaProject selectBaProjectById(String id);

    /**
     * 查询立项管理列表
     *
     * @param baProject 立项管理
     * @return 立项管理集合
     */
    public List<BaProject> selectBaProjectList(BaProject baProject);

    public List<BaProject> selectProject(BaProject baProject);
    /**
     * 查询立项通过列表
     *
     * @param baProject 立项管理
     * @return 立项管理集合
     */
    public List<BaProject> selectBaProjectList1(BaProject baProject);


    /**
     * 新增立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    public int insertBaProject(BaProject baProject);

    /**
     * 修改立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    public int updateBaProject(BaProject baProject);

    /**
     * 修改立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    public int updateBaProjectChange(BaProject baProject);

    /**
     * 删除立项管理
     *
     * @param id 立项管理ID
     * @return 结果
     */
    public int deleteBaProjectById(String id);

    /**
     * 批量删除立项管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaProjectByIds(String[] ids);

    /**
     * 统计立项
     *
     * @param countBaProjectDTO 立项管理
     * @return 结果
     */
    public int countBaProjectList(CountBaProjectDTO countBaProjectDTO);

    /**
     * 查询多租户授权信息立项列表
     * @param baProject
     * @return
     */
    public List<BaProject> selectProjectAuthorized(BaProject baProject);
}
