package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCargoOwner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 货主档案Mapper接口
 *
 * @author ljb
 * @date 2023-12-22
 */
@Repository
public interface BaCargoOwnerMapper extends BaseMapper<BaCargoOwner>
{
    /**
     * 查询货主档案
     *
     * @param id 货主档案ID
     * @return 货主档案
     */
    public BaCargoOwner selectBaCargoOwnerById(String id);

    /**
     * 查询货主档案列表
     *
     * @param baCargoOwner 货主档案
     * @return 货主档案集合
     */
    public List<BaCargoOwner> selectBaCargoOwnerList(BaCargoOwner baCargoOwner);

    /**
     * 新增货主档案
     *
     * @param baCargoOwner 货主档案
     * @return 结果
     */
    public int insertBaCargoOwner(BaCargoOwner baCargoOwner);

    /**
     * 修改货主档案
     *
     * @param baCargoOwner 货主档案
     * @return 结果
     */
    public int updateBaCargoOwner(BaCargoOwner baCargoOwner);

    /**
     * 删除货主档案
     *
     * @param id 货主档案ID
     * @return 结果
     */
    public int deleteBaCargoOwnerById(String id);

    /**
     * 批量删除货主档案
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCargoOwnerByIds(String[] ids);
}
