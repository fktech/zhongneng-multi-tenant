package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaProgressInformation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 进度信息Mapper接口
 *
 * @author single
 * @date 2024-04-01
 */
@Repository
public interface BaProgressInformationMapper extends BaseMapper<BaProgressInformation>
{
    /**
     * 查询进度信息
     *
     * @param id 进度信息ID
     * @return 进度信息
     */
    public BaProgressInformation selectBaProgressInformationById(String id);

    /**
     * 查询进度信息列表
     *
     * @param baProgressInformation 进度信息
     * @return 进度信息集合
     */
    public List<BaProgressInformation> selectBaProgressInformationList(BaProgressInformation baProgressInformation);

    /**
     * 新增进度信息
     *
     * @param baProgressInformation 进度信息
     * @return 结果
     */
    public int insertBaProgressInformation(BaProgressInformation baProgressInformation);

    /**
     * 修改进度信息
     *
     * @param baProgressInformation 进度信息
     * @return 结果
     */
    public int updateBaProgressInformation(BaProgressInformation baProgressInformation);

    /**
     * 删除进度信息
     *
     * @param id 进度信息ID
     * @return 结果
     */
    public int deleteBaProgressInformationById(String id);

    /**
     * 批量删除进度信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaProgressInformationByIds(String[] ids);
}
