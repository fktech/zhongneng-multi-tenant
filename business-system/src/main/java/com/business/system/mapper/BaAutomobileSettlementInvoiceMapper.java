package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementInvoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 无车承运开票Mapper接口
 *
 * @author single
 * @date 2023-04-12
 */
@Repository
public interface BaAutomobileSettlementInvoiceMapper extends BaseMapper<BaAutomobileSettlementInvoice>
{
    /**
     * 查询无车承运开票
     *
     * @param id 无车承运开票ID
     * @return 无车承运开票
     */
    public BaAutomobileSettlementInvoice selectBaAutomobileSettlementInvoiceById(String id);

    /**
     * 查询无车承运开票
     *
     * @param invoiceNo 无车承运开票号
     * @return 无车承运开票
     */
    public BaAutomobileSettlementInvoice selectBaAutomobileSettlementInvoiceByInvoiceNo(String invoiceNo);
    /**
     * 查询无车承运开票状态
     *
     * @param thdno 运单号
     * @return 无车承运开票
     */
    public BaAutomobileSettlementInvoice selectBaAutomobileSettlementInvoiceByexternalorder(String thdno);

    /**
     * 查询无车承运开票列表
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 无车承运开票集合
     */
    public List<BaAutomobileSettlementInvoice> selectBaAutomobileSettlementInvoiceList(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 新增无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 修改无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 删除无车承运开票
     *
     * @param id 无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceById(String id);

    /**
     * 批量删除无车承运开票
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceByIds(String[] ids);
}
