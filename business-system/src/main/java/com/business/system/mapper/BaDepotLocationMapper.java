package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDepotLocation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 仓库库位信息Mapper接口
 *
 * @author single
 * @date 2023-09-06
 */
@Repository
public interface BaDepotLocationMapper extends BaseMapper<BaDepotLocation>
{
    /**
     * 查询仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 仓库库位信息
     */
    public BaDepotLocation selectBaDepotLocationById(String id);

    /**
     * 查询仓库库位信息列表
     *
     * @param baDepotLocation 仓库库位信息
     * @return 仓库库位信息集合
     */
    public List<BaDepotLocation> selectBaDepotLocationList(BaDepotLocation baDepotLocation);

    /**
     * 新增仓库库位信息
     *
     * @param baDepotLocation 仓库库位信息
     * @return 结果
     */
    public int insertBaDepotLocation(BaDepotLocation baDepotLocation);

    /**
     * 修改仓库库位信息
     *
     * @param baDepotLocation 仓库库位信息
     * @return 结果
     */
    public int updateBaDepotLocation(BaDepotLocation baDepotLocation);

    /**
     * 删除仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 结果
     */
    public int deleteBaDepotLocationById(String id);

    /**
     * 批量删除仓库库位信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotLocationByIds(String[] ids);

    /**
     * 删除仓库库位信息
     *
     * @param relevanceId 关联ID
     * @return 结果
     */
    public int deleteBaDepotLocationByRelevanceId(String relevanceId);
}
