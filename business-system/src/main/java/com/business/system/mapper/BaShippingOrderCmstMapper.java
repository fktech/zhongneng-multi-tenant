package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaShippingOrderCmst;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 中储智运发运单Mapper接口
 *
 * @author single
 * @date 2023-08-21
 */
@Repository
public interface BaShippingOrderCmstMapper extends BaseMapper<BaShippingOrderCmst>
{
    /**
     * 查询中储智运发运单
     *
     * @param id 中储智运发运单ID
     * @return 中储智运发运单
     */
    public BaShippingOrderCmst selectBaShippingOrderCmstById(String id);
    /**
     * 查询中储智运发运单
     *
     * @param orderId 中储智运发运单ID
     * @return 中储智运发运单
     */
    public BaShippingOrderCmst selectBaShippingOrderCmstByOrderId(String orderId);

    /**
     * 查询中储智运发运单列表
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 中储智运发运单集合
     */
    public List<BaShippingOrderCmst> selectBaShippingOrderCmstList(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 新增中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    public int insertBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 修改中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    public int updateBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 删除中储智运发运单
     *
     * @param id 中储智运发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderCmstById(String id);

    /**
     * 批量删除中储智运发运单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaShippingOrderCmstByIds(String[] ids);
}
