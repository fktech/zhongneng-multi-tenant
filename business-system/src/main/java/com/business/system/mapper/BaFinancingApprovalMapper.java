package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaFinancingApproval;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 融资审批Mapper接口
 *
 * @author single
 * @date 2023-05-03
 */
@Repository
public interface BaFinancingApprovalMapper extends BaseMapper<BaFinancingApproval>
{
    /**
     * 查询融资审批
     *
     * @param id 融资审批ID
     * @return 融资审批
     */
    public BaFinancingApproval selectBaFinancingApprovalById(String id);

    /**
     * 查询融资审批列表
     *
     * @param baFinancingApproval 融资审批
     * @return 融资审批集合
     */
    public List<BaFinancingApproval> selectBaFinancingApprovalList(BaFinancingApproval baFinancingApproval);

    /**
     * 新增融资审批
     *
     * @param baFinancingApproval 融资审批
     * @return 结果
     */
    public int insertBaFinancingApproval(BaFinancingApproval baFinancingApproval);

    /**
     * 修改融资审批
     *
     * @param baFinancingApproval 融资审批
     * @return 结果
     */
    public int updateBaFinancingApproval(BaFinancingApproval baFinancingApproval);

    /**
     * 删除融资审批
     *
     * @param id 融资审批ID
     * @return 结果
     */
    public int deleteBaFinancingApprovalById(String id);

    /**
     * 批量删除融资审批
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaFinancingApprovalByIds(String[] ids);
}
