package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaHiContractStart;
import com.business.system.domain.BaHiTransport;
import com.business.system.domain.dto.CountContractStartDTO;
import com.business.system.domain.dto.TerminalDTO;
import com.business.system.domain.vo.BaContractStartTonVO;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * 历史合同启动Mapper接口
 *
 * @author ljb
 * @date 2023-03-06
 */
@Repository
public interface BaHiContractStartMapper extends BaseMapper<BaHiContractStart>
{
    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    public BaHiContractStart selectBaHiContractStartById(String id);

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaHiContractStart> selectBaHiContractStartList(BaHiContractStart baHiContractStart);

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaHiContractStart> selectHiContractStartList(BaHiContractStart baHiContractStart);
    /**
     * 新增合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int insertBaHiContractStart(BaHiContractStart baHiContractStart);

    /**
     * 修改合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int updateBaHiContractStart(BaHiContractStart baHiContractStart);

    /**
     * 修改合同启动
     *
     * @param baHiContractStart 合同启动
     * @return 结果
     */
    public int updateBaHiContractStartChange(BaHiContractStart baHiContractStart);

    /**
     * 删除合同启动
     *
     * @param id 合同启动ID
     * @return 结果
     */
    public int deleteBaHiContractStartById(String id);

    /**
     * 批量删除合同启动
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaHiContractStartByIds(String[] ids);

    /**
     * 查询合同启动变更历史列表
     *
     * @param baHiContractStart 合同启动
     * @return 用煤企业集合
     */
    public List<BaHiContractStart> selectChangeBaHiContractStartList(BaHiContractStart baHiContractStart);

}
