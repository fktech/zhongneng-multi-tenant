package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaEvection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.OaLeave;
import org.springframework.stereotype.Repository;

/**
 * 出差申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaEvectionMapper extends BaseMapper<OaEvection>
{
    /**
     * 查询出差申请
     *
     * @param id 出差申请ID
     * @return 出差申请
     */
    public OaEvection selectOaEvectionById(String id);

    /**
     * 查询出差申请列表
     *
     * @param oaEvection 出差申请
     * @return 出差申请集合
     */
    public List<OaEvection> selectOaEvectionList(OaEvection oaEvection);

    /**
     * 统计出差申请
     *
     * @param oaEvection 出差申请
     * @return 出差申请集合
     */
    public Long countOaEvectionList(OaEvection oaEvection);

    /**
     * 时间查询
     * @param oaEvection
     * @return
     */
    public List<OaEvection> selectOaEvection(OaEvection oaEvection);

    /**
     * 新增出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    public int insertOaEvection(OaEvection oaEvection);

    /**
     * 修改出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    public int updateOaEvection(OaEvection oaEvection);

    /**
     * 删除出差申请
     *
     * @param id 出差申请ID
     * @return 结果
     */
    public int deleteOaEvectionById(String id);

    /**
     * 批量删除出差申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaEvectionByIds(String[] ids);
}
