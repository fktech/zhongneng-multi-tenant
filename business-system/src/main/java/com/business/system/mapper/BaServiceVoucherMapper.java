package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaServiceVoucher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 进厂确认单Mapper接口
 *
 * @author single
 * @date 2023-09-19
 */
@Repository
public interface BaServiceVoucherMapper extends BaseMapper<BaServiceVoucher>
{
    /**
     * 查询进厂确认单
     *
     * @param id 进厂确认单ID
     * @return 进厂确认单
     */
    public BaServiceVoucher selectBaServiceVoucherById(String id);

    /**
     * 查询进厂确认单列表
     *
     * @param baServiceVoucher 进厂确认单
     * @return 进厂确认单集合
     */
    public List<BaServiceVoucher> selectBaServiceVoucherList(BaServiceVoucher baServiceVoucher);

    public List<BaServiceVoucher> selectBaServiceVoucher(BaServiceVoucher baServiceVoucher);

    /**
     * 新增进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    public int insertBaServiceVoucher(BaServiceVoucher baServiceVoucher);

    /**
     * 修改进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    public int updateBaServiceVoucher(BaServiceVoucher baServiceVoucher);

    /**
     * 删除进厂确认单
     *
     * @param id 进厂确认单ID
     * @return 结果
     */
    public int deleteBaServiceVoucherById(String id);

    /**
     * 批量删除进厂确认单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaServiceVoucherByIds(String[] ids);
}
