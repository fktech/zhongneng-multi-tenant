package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCheckedHead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 盘点Mapper接口
 *
 * @author single
 * @date 2023-01-05
 */
@Repository
public interface BaCheckedHeadMapper extends BaseMapper<BaCheckedHead>
{
    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    public BaCheckedHead selectBaCheckedHeadById(String id);

    /**
     * 查询盘点列表
     *
     * @param baCheckedHead 盘点
     * @return 盘点集合
     */
    public List<BaCheckedHead> selectBaCheckedHeadList(BaCheckedHead baCheckedHead);

    /**
     * 查询盘点列表
     *
     * @param baCheckedHead 盘点
     * @return 盘点集合
     */
    public List<BaCheckedHead> selectBaCheckedHeadListByCheckId(BaCheckedHead baCheckedHead);

    /**
     * 新增盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    public int insertBaCheckedHead(BaCheckedHead baCheckedHead);

    /**
     * 修改盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    public int updateBaCheckedHead(BaCheckedHead baCheckedHead);

    /**
     * 删除盘点
     *
     * @param id 盘点ID
     * @return 结果
     */
    public int deleteBaCheckedHeadById(String id);

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCheckedHeadByIds(String[] ids);

    /**
     * 根据盘点ID批量删除盘点详情
     *
     * @param checkedId 盘点ID
     * @return 结果
     */
    public int deleteBaCheckedHeadByCheckId(String checkedId);

    /**
     * 统计库存盘点
     *
     * @return 结果
     */
    public int countBaCheckedHead(BaCheckedHead baCheckedHead);
}
