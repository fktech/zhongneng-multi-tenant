package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaContract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.BaContractDTO;
import org.springframework.stereotype.Repository;

/**
 * 合同Mapper接口
 *
 * @author ljb
 * @date 2022-12-07
 */
@Repository
public interface BaContractMapper extends BaseMapper<BaContract>
{
    /**
     * 查询合同
     *
     * @param id 合同ID
     * @return 合同
     */
    public BaContract selectBaContractById(String id);

    /**
     * 查询合同列表
     *
     * @param baContract 合同
     * @return 合同集合
     */
    public List<BaContract> selectBaContractList(BaContract baContract);

    /**
     * 新增合同
     *
     * @param baContract 合同
     * @return 结果
     */
    public int insertBaContract(BaContract baContract);

    /**
     * 修改合同
     *
     * @param baContract 合同
     * @return 结果
     */
    public int updateBaContract(BaContract baContract);

    /**
     * 删除合同
     *
     * @param id 合同ID
     * @return 结果
     */
    public int deleteBaContractById(String id);

    /**
     * 批量删除合同
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaContractByIds(String[] ids);

    /**
     * 统计合同累计发量
     *
     * @param baContractDTO 合同对象
     * @return 合同
     */
    public BigDecimal countContractCumulativeVolume(BaContractDTO baContractDTO);

    /**
     * 统计合同数量
     *
     * @param baContract 合同对象
     * @return 合同
     */
    public int countBaContractList(BaContract baContract);

    /**
     * 查询多租户授权信息合同列表
     * @param baContract
     * @return
     */
    public List<BaContract> selectBaContractAuthorizedList(BaContract baContract);
}
