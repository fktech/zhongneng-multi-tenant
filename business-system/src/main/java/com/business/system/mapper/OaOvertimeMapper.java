package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaOvertime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 加班申请Mapper接口
 *
 * @author ljb
 * @date 2023-11-11
 */
@Repository
public interface OaOvertimeMapper extends BaseMapper<OaOvertime>
{
    /**
     * 查询加班申请
     *
     * @param id 加班申请ID
     * @return 加班申请
     */
    public OaOvertime selectOaOvertimeById(String id);

    /**
     * 查询加班申请列表
     *
     * @param oaOvertime 加班申请
     * @return 加班申请集合
     */
    public List<OaOvertime> selectOaOvertimeList(OaOvertime oaOvertime);

    /**
     * 新增加班申请
     *
     * @param oaOvertime 加班申请
     * @return 结果
     */
    public int insertOaOvertime(OaOvertime oaOvertime);

    /**
     * 修改加班申请
     *
     * @param oaOvertime 加班申请
     * @return 结果
     */
    public int updateOaOvertime(OaOvertime oaOvertime);

    /**
     * 删除加班申请
     *
     * @param id 加班申请ID
     * @return 结果
     */
    public int deleteOaOvertimeById(String id);

    /**
     * 批量删除加班申请
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaOvertimeByIds(String[] ids);
}
