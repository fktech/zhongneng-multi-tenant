package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaContractSeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * oa合同用印Mapper接口
 *
 * @author single
 * @date 2024-06-06
 */
@Repository
public interface OaContractSealMapper extends BaseMapper<OaContractSeal>
{
    /**
     * 查询oa合同用印
     *
     * @param id oa合同用印ID
     * @return oa合同用印
     */
    public OaContractSeal selectOaContractSealById(String id);

    /**
     * 查询oa合同用印列表
     *
     * @param oaContractSeal oa合同用印
     * @return oa合同用印集合
     */
    public List<OaContractSeal> selectOaContractSealList(OaContractSeal oaContractSeal);

    /**
     * 新增oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    public int insertOaContractSeal(OaContractSeal oaContractSeal);

    /**
     * 修改oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    public int updateOaContractSeal(OaContractSeal oaContractSeal);

    /**
     * 删除oa合同用印
     *
     * @param id oa合同用印ID
     * @return 结果
     */
    public int deleteOaContractSealById(String id);

    /**
     * 批量删除oa合同用印
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaContractSealByIds(String[] ids);
}
