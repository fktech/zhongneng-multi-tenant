package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaMaterialStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.vo.BaDepotVO;
import com.business.system.domain.vo.BaMasterialStockVO;
import org.springframework.stereotype.Repository;

/**
 * 库存Mapper接口
 *
 * @author single
 * @date 2023-01-05
 */
@Repository
public interface BaMaterialStockMapper extends BaseMapper<BaMaterialStock>
{
    /**
     * 查询库存
     *
     * @param id 库存ID
     * @return 库存
     */
    public BaMaterialStock selectBaMaterialStockById(String id);

    /**
     * 查询库存列表
     *
     * @param baMaterialStock 库存
     * @return 库存集合
     */
    public List<BaMaterialStock> selectBaMaterialStockList(BaMaterialStock baMaterialStock);

    /**
     * 查询库存列表
     *
     * @param baMaterialStock 库存
     * @return 库存集合
     */
    public List<BaMaterialStock> selectBaMaterialStockListPage(BaMaterialStock baMaterialStock);

    /**
     * 新增库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    public int insertBaMaterialStock(BaMaterialStock baMaterialStock);

    /**
     * 修改库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    public int updateBaMaterialStock(BaMaterialStock baMaterialStock);

    /**
     * 删除库存
     *
     * @param id 库存ID
     * @return 结果
     */
    public int deleteBaMaterialStockById(String id);

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaMaterialStockByIds(String[] ids);

    /**
     * 库存总量
     */
    public BigDecimal totals();

    /**
     * 库存总量
     */
    public BigDecimal sumTotalByCon(BaMaterialStock baMaterialStock);

    /**
     * 库存统计
     */
    public List<BaMasterialStockVO> sumList(BaMaterialStock baMaterialStock);

    /**
     * 项目库存统计
     */
    public List<BaMasterialStockVO> sumProjectList(BaMaterialStock baMaterialStock);

    /**
     * 库位库存统计
     */
    public List<BaMasterialStockVO> sumLocationList(BaMaterialStock baMaterialStock);

    /**
     * 商品库存统计
     */
    public List<BaMasterialStockVO> sumGoodsList(BaMaterialStock baMaterialStock);

    /**
     * 项目库存总量统计
     */
    public List<BaMaterialStock> projectSumTotalByCon(BaMaterialStock baMaterialStock);

    /**
     * 智慧驾驶舱商品库存统计
     */
    public List<BaMasterialStockVO> sumGoodsLocationList(BaMaterialStock baMaterialStock);

    /**
     * 商品库存统计
     */
    public List<BaMasterialStockVO> sumGoodsDepotList(BaMaterialStock baMaterialStock);

    /**
     * 仓库列表
     */
    public List<BaDepotVO> depotList(BaMaterialStock baMaterialStock);

    /**
     * 统计商品数量
     */
    public int countGoods(BaMaterialStock baMaterialStock);
}
