package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 车辆信息Mapper接口
 *
 * @author single
 * @date 2023-12-01
 */
@Repository
public interface BaCarMapper extends BaseMapper<BaCar>
{
    /**
     * 查询车辆信息
     *
     * @param id 车辆信息ID
     * @return 车辆信息
     */
    public BaCar selectBaCarById(String id);

    /**
     * 查询车辆信息列表
     *
     * @param baCar 车辆信息
     * @return 车辆信息集合
     */
    public List<BaCar> selectBaCarList(BaCar baCar);

    /**
     * 新增车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    public int insertBaCar(BaCar baCar);

    /**
     * 修改车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    public int updateBaCar(BaCar baCar);

    /**
     * 删除车辆信息
     *
     * @param id 车辆信息ID
     * @return 结果
     */
    public int deleteBaCarById(String id);

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCarByIds(String[] ids);
}
