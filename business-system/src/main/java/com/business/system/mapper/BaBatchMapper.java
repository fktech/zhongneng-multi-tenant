package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaBatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 批次Mapper接口
 *
 * @author single
 * @date 2023-01-05
 */
@Repository
public interface BaBatchMapper extends BaseMapper<BaBatch>
{
    /**
     * 查询批次
     *
     * @param id 批次ID
     * @return 批次
     */
    public BaBatch selectBaBatchById(String id);

    /**
     * 查询批次列表
     *
     * @param baBatch 批次
     * @return 批次集合
     */
    public List<BaBatch> selectBaBatchList(BaBatch baBatch);

    /**
     * 新增批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    public int insertBaBatch(BaBatch baBatch);

    /**
     * 修改批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    public int updateBaBatch(BaBatch baBatch);

    /**
     * 删除批次
     *
     * @param id 批次ID
     * @return 结果
     */
    public int deleteBaBatchById(String id);

    /**
     * 批量删除批次
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaBatchByIds(String[] ids);
}
