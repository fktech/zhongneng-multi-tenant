package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaServiceVoucherRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 进厂确认附Mapper接口
 *
 * @author single
 * @date 2023-09-19
 */
@Repository
public interface BaServiceVoucherRecordMapper extends BaseMapper<BaServiceVoucherRecord>
{
    /**
     * 查询进厂确认附
     *
     * @param id 进厂确认附ID
     * @return 进厂确认附
     */
    public BaServiceVoucherRecord selectBaServiceVoucherRecordById(String id);

    /**
     * 查询进厂确认附列表
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 进厂确认附集合
     */
    public List<BaServiceVoucherRecord> selectBaServiceVoucherRecordList(BaServiceVoucherRecord baServiceVoucherRecord);

    /**
     * 新增进厂确认附
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 结果
     */
    public int insertBaServiceVoucherRecord(BaServiceVoucherRecord baServiceVoucherRecord);

    /**
     * 修改进厂确认附
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 结果
     */
    public int updateBaServiceVoucherRecord(BaServiceVoucherRecord baServiceVoucherRecord);

    /**
     * 删除进厂确认附
     *
     * @param id 进厂确认附ID
     * @return 结果
     */
    public int deleteBaServiceVoucherRecordById(String id);

    /**
     * 批量删除进厂确认附
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaServiceVoucherRecordByIds(String[] ids);
}
