package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaInvoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaPayment;
import com.business.system.domain.SysUserRole;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import com.business.system.domain.vo.BaInvoiceVO;
import org.springframework.stereotype.Repository;

/**
 * 发票信息列Mapper接口
 *
 * @author single
 * @date 2022-12-28
 */
@Repository
public interface BaInvoiceMapper extends BaseMapper<BaInvoice>
{
    /**
     * 查询发票信息列
     *
     * @param id 发票信息列ID
     * @return 发票信息列
     */
    public BaInvoice selectBaInvoiceById(String id);

    /**
     * 查询发票信息列列表
     *
     * @param baInvoice 发票信息列
     * @return 发票信息列集合
     */
    public List<BaInvoice> selectBaInvoiceList(BaInvoice baInvoice);
    /**
     * 查询发票总数
     *
     * @param
     * @return 发票信息列
     */
    public List<BaInvoice> selectBaInvoiceNums(BaInvoice baInvoice);
    /**
     * 查询本月度发票数和金额
     *
     * @param
     * @return 发票信息列
     */
    public List<BaInvoice> selectNumsAmountByMonth(BaInvoice baInvoice);

    /**
     * 根据条件查询发票信息列列表
     *
     * @param baInvoice 发票信息列
     * @return 发票信息列集合
     */
    public List<BaInvoice> selectBaInvoiceListByCondition(BaInvoice baInvoice);

    /**
     * 新增发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    public int insertBaInvoice(BaInvoice baInvoice);

    /**
     * 修改发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    public int updateBaInvoice(BaInvoice baInvoice);

    /**
     * 删除发票信息列
     *
     * @param id 发票信息列ID
     * @return 结果
     */
    public int deleteBaInvoiceById(String id);

    /**
     * 批量删除发票信息列
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaInvoiceByIds(String[] ids);

    /**
     * 统计发票
     */
    public BigDecimal sumBaInvoice(BaInvoice baInvoice);

    /**
     * 立项发票统计
     */
    public BigDecimal baInvoiceTotal(BaInvoice baInvoice);

    /**
     * 统计发票
     */
    public BaInvoiceVO sumBaInvoiceByCon(BaInvoice baInvoice);

    /**
     * 可视化大屏统计发票数量
     */
    public Long countBaInvoice(BaInvoice baInvoice);

    /**
     * 统计含税金额
     */
    public BigDecimal sumTaxPriceTotal(BaInvoice baInvoice);

    /**
     * 查询销项发票开票总数及金额
     */
    public NumsAndMoneyDTO selectBaInvoiceForUp(BaInvoice baInvoice);
    /**
     * 查询月度销项发票开票总数
     */
    public NumsAndMoneyDTO selectBaInvoiceForMonth(BaInvoice baInvoice);
    /**
     * * 查询年度销项发票开票总数
     */
    public NumsAndMoneyDTO selectBaInvoiceForYear(BaInvoice baInvoice);

    public List<BaInvoice> selectBaInvoiceAuthorizedList(BaInvoice baInvoice);
}
