package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaBlendingProgramme;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 配煤方案Mapper接口
 *
 * @author ljb
 * @date 2024-01-05
 */
@Repository
public interface BaBlendingProgrammeMapper extends BaseMapper<BaBlendingProgramme>
{
    /**
     * 查询配煤方案
     *
     * @param id 配煤方案ID
     * @return 配煤方案
     */
    public BaBlendingProgramme selectBaBlendingProgrammeById(String id);

    /**
     * 查询配煤方案列表
     *
     * @param baBlendingProgramme 配煤方案
     * @return 配煤方案集合
     */
    public List<BaBlendingProgramme> selectBaBlendingProgrammeList(BaBlendingProgramme baBlendingProgramme);

    /**
     * 新增配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    public int insertBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme);

    /**
     * 修改配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    public int updateBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme);

    /**
     * 删除配煤方案
     *
     * @param id 配煤方案ID
     * @return 结果
     */
    public int deleteBaBlendingProgrammeById(String id);

    /**
     * 批量删除配煤方案
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaBlendingProgrammeByIds(String[] ids);
}
