package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaWeekday;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 工作日Mapper接口
 *
 * @author single
 * @date 2023-11-24
 */
@Repository
public interface OaWeekdayMapper extends BaseMapper<OaWeekday>
{
    /**
     * 查询工作日
     *
     * @param id 工作日ID
     * @return 工作日
     */
    public OaWeekday selectOaWeekdayById(String id);

    /**
     * 查询工作日列表
     *
     * @param oaWeekday 工作日
     * @return 工作日集合
     */
    public List<OaWeekday> selectOaWeekdayList(OaWeekday oaWeekday);

    /**
     * 新增工作日
     *
     * @param oaWeekday 工作日
     * @return 结果
     */
    public int insertOaWeekday(OaWeekday oaWeekday);

    /**
     * 修改工作日
     *
     * @param oaWeekday 工作日
     * @return 结果
     */
    public int updateOaWeekday(OaWeekday oaWeekday);

    /**
     * 删除工作日
     *
     * @param id 工作日ID
     * @return 结果
     */
    public int deleteOaWeekdayById(String id);

    /**
     * 批量删除工作日
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaWeekdayByIds(String[] ids);
}
