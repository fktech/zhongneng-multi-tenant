package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaAutomobileSettlementDetail;
import org.springframework.stereotype.Repository;

/**
 * 汽运结算Mapper接口
 *
 * @author single
 * @date 2023-02-17
 */
@Repository
public interface BaAutomobileSettlementMapper extends BaseMapper<BaAutomobileSettlement>
{
    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    public BaAutomobileSettlement selectBaAutomobileSettlementById(String id);

    /**
     * 查询付款明细
     *
     * @param paymentOrderNum 汽运结算ID
     * @return 汽运结算
     */
    public BaAutomobileSettlement selectBaAutomobileSettlementByPaymentOrderNum(String paymentOrderNum);

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 汽运结算集合
     */
    public List<BaAutomobileSettlement> selectBaAutomobileSettlementList(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    public int insertBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    public int updateBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 删除汽运结算
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementById(String id);

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementByIds(String[] ids);
}
