package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDepotInventory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 入库清单Mapper接口
 *
 * @author ljb
 * @date 2023-09-07
 */
@Repository
public interface BaDepotInventoryMapper extends BaseMapper<BaDepotInventory>
{
    /**
     * 查询入库清单
     *
     * @param id 入库清单ID
     * @return 入库清单
     */
    public BaDepotInventory selectBaDepotInventoryById(String id);

    /**
     * 查询入库清单列表
     *
     * @param baDepotInventory 入库清单
     * @return 入库清单集合
     */
    public List<BaDepotInventory> selectBaDepotInventoryList(BaDepotInventory baDepotInventory);

    /**
     * 新增入库清单
     *
     * @param baDepotInventory 入库清单
     * @return 结果
     */
    public int insertBaDepotInventory(BaDepotInventory baDepotInventory);

    /**
     * 修改入库清单
     *
     * @param baDepotInventory 入库清单
     * @return 结果
     */
    public int updateBaDepotInventory(BaDepotInventory baDepotInventory);

    /**
     * 删除入库清单
     *
     * @param id 入库清单ID
     * @return 结果
     */
    public int deleteBaDepotInventoryById(String id);

    /**
     * 批量删除入库清单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotInventoryByIds(String[] ids);

    /**
     * 智慧驾驶舱出入库记录
     *
     * @param baDepotInventory 入库清单
     * @return 智慧驾驶舱出入库记录
     */
    public List<BaDepotInventory> selectBaDepotInventory(BaDepotInventory baDepotInventory);
}
