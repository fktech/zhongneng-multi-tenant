package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;

import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaWashHead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.DepotHeadDTO;
import org.springframework.stereotype.Repository;

/**
 * 洗煤配煤出入库记录Mapper接口
 *
 * @author single
 * @date 2024-01-05
 */
@Repository
public interface BaWashHeadMapper extends BaseMapper<BaWashHead>
{
    /**
     * 查询洗煤配煤出入库记录
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 洗煤配煤出入库记录
     */
    public BaWashHead selectBaWashHeadById(String id);

    /**
     * 查询洗煤配煤出入库记录列表
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 洗煤配煤出入库记录集合
     */
    public List<BaWashHead> selectBaWashHeadList(BaWashHead baWashHead);

    /**
     * 新增洗煤配煤出入库记录
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 结果
     */
    public int insertBaWashHead(BaWashHead baWashHead);

    /**
     * 修改洗煤配煤出入库记录
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 结果
     */
    public int updateBaWashHead(BaWashHead baWashHead);

    /**
     * 删除洗煤配煤出入库记录
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 结果
     */
    public int deleteBaWashHeadById(String id);

    /**
     * 批量删除洗煤配煤出入库记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaWashHeadByIds(String[] ids);

    /**
     * 出库总量
     */
    public BigDecimal outputTotals(DepotHeadDTO depotHeadDTO);

    /**
     * 出库总量
     */
    public BigDecimal intoTotals(DepotHeadDTO depotHeadDTO);

    /**
     * 统计入库
     */
    public int countIntoBaWashHead(BaWashHead baWashHead);

    /**
     * 统计出库
     */
    public int countOutputBaWashHead(BaWashHead baWashHead);

}
