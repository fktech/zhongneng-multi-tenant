package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementInvoiceCmst;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 中储智运无车承运开票Mapper接口
 *
 * @author single
 * @date 2023-08-21
 */
@Repository
public interface BaAutomobileSettlementInvoiceCmstMapper extends BaseMapper<BaAutomobileSettlementInvoiceCmst>
{
    /**
     * 查询中储智运无车承运开票
     *
     * @param id 中储智运无车承运开票ID
     * @return 中储智运无车承运开票
     */
    public BaAutomobileSettlementInvoiceCmst selectBaAutomobileSettlementInvoiceCmstById(String id);

    /**
     * 查询中储智运无车承运开票列表
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 中储智运无车承运开票集合
     */
    public List<BaAutomobileSettlementInvoiceCmst> selectBaAutomobileSettlementInvoiceCmstList(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 新增中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 修改中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 删除中储智运无车承运开票
     *
     * @param id 中储智运无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceCmstById(String id);

    /**
     * 批量删除中储智运无车承运开票
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceCmstByIds(String[] ids);
}
