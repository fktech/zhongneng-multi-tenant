package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaMonitor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 监控Mapper接口
 *
 * @author single
 * @date 2024-06-04
 */
@Repository
public interface BaMonitorMapper extends BaseMapper<BaMonitor>
{
    /**
     * 查询监控
     *
     * @param id 监控ID
     * @return 监控
     */
    public BaMonitor selectBaMonitorById(String id);

    /**
     * 查询监控列表
     *
     * @param baMonitor 监控
     * @return 监控集合
     */
    public List<BaMonitor> selectBaMonitorList(BaMonitor baMonitor);

    /**
     * 权限查询
     * @param baMonitor
     * @return
     */
    public List<BaMonitor> selectBaMonitor(BaMonitor baMonitor);

    /**
     * 新增监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    public int insertBaMonitor(BaMonitor baMonitor);

    /**
     * 修改监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    public int updateBaMonitor(BaMonitor baMonitor);

    /**
     * 删除监控
     *
     * @param id 监控ID
     * @return 结果
     */
    public int deleteBaMonitorById(String id);

    /**
     * 批量删除监控
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaMonitorByIds(String[] ids);
}
