package com.business.system.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaEnterprise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.vo.BaEnterpriseVO;
import org.springframework.stereotype.Repository;

/**
 * 用煤企业Mapper接口
 *
 * @author ljb
 * @date 2022-11-29
 */
@Repository
public interface BaEnterpriseMapper extends BaseMapper<BaEnterprise>
{
    /**
     * 查询用煤企业
     *
     * @param id 用煤企业ID
     * @return 用煤企业
     */
    public BaEnterprise selectBaEnterpriseById(String id);

    /**
     * 查询用煤企业列表
     *
     * @param baEnterprise 用煤企业
     * @return 用煤企业集合
     */
    public List<BaEnterprise> selectBaEnterpriseList(BaEnterprise baEnterprise);

    /**
     * 新增用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    public int insertBaEnterprise(BaEnterprise baEnterprise);

    /**
     * 修改用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    public int updateBaEnterprise(BaEnterprise baEnterprise);

    /**
     * 删除用煤企业
     *
     * @param id 用煤企业ID
     * @return 结果
     */
    public int deleteBaEnterpriseById(String id);

    /**
     * 批量删除用煤企业
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaEnterpriseByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth(String tenantId);

    /**
     * 终端发运排名
     *
     * @return 运输
     */
    public List<BaEnterpriseVO> countRanking();
}
