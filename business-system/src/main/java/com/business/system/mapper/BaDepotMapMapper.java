package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaDepotMap;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 仓库库位信息Mapper接口
 *
 * @author single
 * @date 2023-09-06
 */
@Repository
public interface BaDepotMapMapper extends BaseMapper<BaDepotMap>
{
    /**
     * 查询仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 仓库库位信息
     */
    public BaDepotMap selectBaDepotMapById(String id);

    /**
     * 查询仓库库位信息列表
     *
     * @param baDepotMap 仓库库位信息
     * @return 仓库库位信息集合
     */
    public List<BaDepotMap> selectBaDepotMapList(BaDepotMap baDepotMap);

    /**
     * 新增仓库库位信息
     *
     * @param baDepotMap 仓库库位信息
     * @return 结果
     */
    public int insertBaDepotMap(BaDepotMap baDepotMap);

    /**
     * 修改仓库库位信息
     *
     * @param baDepotMap 仓库库位信息
     * @return 结果
     */
    public int updateBaDepotMap(BaDepotMap baDepotMap);

    /**
     * 删除仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 结果
     */
    public int deleteBaDepotMapById(String id);

    /**
     * 批量删除仓库库位信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaDepotMapByIds(String[] ids);

    /**
     * 删除仓库库位信息
     *
     * @param relevanceId 关联ID
     * @return 结果
     */
    public int deleteBaDepotMapByRelevanceId(String relevanceId);
}
