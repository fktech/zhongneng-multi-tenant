package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaCompanyGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 集团公司Mapper接口
 *
 * @author ljb
 * @date 2023-03-01
 */
@Repository
public interface BaCompanyGroupMapper extends BaseMapper<BaCompanyGroup>
{
    /**
     * 查询集团公司
     *
     * @param id 集团公司ID
     * @return 集团公司
     */
    public BaCompanyGroup selectBaCompanyGroupById(String id);

    /**
     * 查询集团公司列表
     *
     * @param baCompanyGroup 集团公司
     * @return 集团公司集合
     */
    public List<BaCompanyGroup> selectBaCompanyGroupList(BaCompanyGroup baCompanyGroup);

    /**
     * 新增集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    public int insertBaCompanyGroup(BaCompanyGroup baCompanyGroup);

    /**
     * 修改集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    public int updateBaCompanyGroup(BaCompanyGroup baCompanyGroup);

    /**
     * 删除集团公司
     *
     * @param id 集团公司ID
     * @return 结果
     */
    public int deleteBaCompanyGroupById(String id);

    /**
     * 批量删除集团公司
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCompanyGroupByIds(String[] ids);

    /**
     * 查询本月数据
     */
    public Integer thisMonth(String tenantId);
}
