package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaQualityReport;

/**
 * 数质量报告Service接口
 *
 * @author single
 * @date 2023-07-19
 */
public interface IBaQualityReportService
{
    /**
     * 查询数质量报告
     *
     * @param id 数质量报告ID
     * @return 数质量报告
     */
    public BaQualityReport selectBaQualityReportById(String id);

    /**
     * 查询数质量报告列表
     *
     * @param baQualityReport 数质量报告
     * @return 数质量报告集合
     */
    public List<BaQualityReport> selectBaQualityReportList(BaQualityReport baQualityReport);

    /**
     * 新增数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    public int insertBaQualityReport(BaQualityReport baQualityReport);

    /**
     * 修改数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    public int updateBaQualityReport(BaQualityReport baQualityReport);

    /**
     * 批量删除数质量报告
     *
     * @param ids 需要删除的数质量报告ID
     * @return 结果
     */
    public int deleteBaQualityReportByIds(String[] ids);

    /**
     * 删除数质量报告信息
     *
     * @param id 数质量报告ID
     * @return 结果
     */
    public int deleteBaQualityReportById(String id);


}
