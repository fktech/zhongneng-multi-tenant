package com.business.system.service;


import com.business.system.domain.dto.BaProcessDefinitionDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;

import java.util.List;

/**
 * 流程实例接口
 *
 * @author : js
 * @version : 1.0
 */
public interface IBaProcessInstanceService {

    /**
     * 查询流程实例审批链路信息
     * @param processInstanceApproveLinkDTO
     * @return
     */
    List<ApproveLinkHistoryTaskVO> listInstanceApproveLink(ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO);

    /**
     * 查询流程定义审批链路信息
     * @param baProcessDefinitionDTO
     * @return
     */
    List<ApproveLinkHistoryTaskVO> listDefinitionApproveLink(BaProcessDefinitionDTO baProcessDefinitionDTO);

    /**
     * 查询流程当前节点审批链路信息
     * @param processInstanceApproveLinkDTO
     * @return
     */
    ApproveLinkHistoryTaskVO getCurrentNodeApproveLink(ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO);
}
