package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.system.domain.SysPostDept;
import com.business.system.domain.SysUserPost;
import com.business.system.mapper.SysPostDeptMapper;
import com.business.system.mapper.SysRoleMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.DingDingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.common.annotation.DataScope;
import com.business.common.constant.UserConstants;
import com.business.common.core.domain.TreeSelect;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.text.Convert;
import com.business.common.exception.ServiceException;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.spring.SpringUtils;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.service.ISysDeptService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * 部门管理 服务实现
 *
 * @author ruoyi
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService
{
    @Resource
    private SysDeptMapper deptMapper;

    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SysPostDeptMapper sysPostDeptMapper;

    @Resource
    private SysUserMapper userMapper;

    @Resource
    private DingDingService dingDingService;

    @Resource
    private GetRedisIncreID getRedisIncreID;
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysDept> selectDeptList(SysDept dept)
    {
        dept.setTenantId(SecurityUtils.getCurrComId());
        return deptMapper.selectDeptList(dept);
    }

    @Override
    public List<SysDept> deptList(SysDept dept) {
        return deptMapper.deptList(dept);
    }

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysDept> selectTreeDeptList(SysDept dept)
    {
        return deptMapper.selectTreeDeptList(dept);
    }

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    @Override
    public List<TreeSelect> selectDeptTreeList(SysDept dept)
    {
        List<SysDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
        return buildDeptTreeSelect(depts);
    }

    @Override
    public List<TreeSelect> selectDeptTreeMap(SysDept dept) {
        List<SysDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
        for (SysDept sysDept:depts) {
            SysUser user = new SysUser();
            user.setDeptId(sysDept.getDeptId());
            user.setStatus("0");
            user.setComId(SecurityUtils.getCurrComId());
            List<SysUser> sysUsers = userMapper.selectUserList(user);
            if(sysUsers.size() > 0){
                sysDept.setDeptName(sysDept.getDeptName()+" ("+sysUsers.size()+"人"+")");
            }else {
                sysDept.setDeptName(sysDept.getDeptName());
            }
        }
        return buildDeptTreeSelect(depts);
    }

    @Override
    public List<TreeSelect> deptTreeList(SysDept dept) {
        List<SysDept> depts = SpringUtils.getAopProxy(this).deptList(dept);
        return buildDeptTreeSelect(depts);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    @Override
    public List<SysDept> buildDeptTree(List<SysDept> depts)
    {
        List<SysDept> returnList = new ArrayList<SysDept>();
        List<Long> tempList = new ArrayList<Long>();
        for (SysDept dept : depts)
        {
            tempList.add(dept.getDeptId());
        }
        for (SysDept dept : depts)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId()))
            {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts)
    {
        List<SysDept> deptTrees = buildDeptTree(depts);
        deptTrees.stream().forEach(itm ->{

        });
        List<TreeSelect> collect = deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());

        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<Long> selectDeptListByRoleId(Long roleId)
    {
        SysRole role = roleMapper.selectRoleById(roleId);
        return deptMapper.selectDeptListByRoleId(String.valueOf(roleId), role.isDeptCheckStrictly(), SecurityUtils.getCurrComId());
    }

    @Override
    public List<Long> selectDeptIdByRoleId(Long roleId)
    {
        return deptMapper.selectDeptIdByRoleId(roleId);
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Override
    public SysDept selectDeptById(Long deptId)
    {
        return deptMapper.selectDeptById(deptId);
    }

    /**
     * 根据部门ID查询信息
     *
     * @return 部门信息
     */
    @Override
    public SysDept selectTopDeptBytenantId()
    {
        return deptMapper.selectTopDeptBytenantId(SecurityUtils.getCurrComId());
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    @Override
    public int selectNormalChildrenDeptById(Long deptId)
    {
        return deptMapper.selectNormalChildrenDeptById(deptId);
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public boolean hasChildByDeptId(Long deptId)
    {
        int result = deptMapper.hasChildByDeptId(deptId);
        return result > 0;
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(Long deptId)
    {
        int result = deptMapper.checkDeptExistUser(deptId);
        return result > 0;
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysDept dept)
    {
        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    @Override
    public void checkDeptDataScope(Long deptId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysDept dept = new SysDept();
            dept.setDeptId(deptId);
            List<SysDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
            if (StringUtils.isEmpty(depts))
            {
                throw new ServiceException("没有权限访问部门数据！");
            }
        }
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDept dept)
    {
        dept.setDeptId(Long.valueOf(getRedisIncreID.getId().substring(getRedisIncreID.getId().length() - 12)));
        if(null == dept.getTenantId()){
            dept.setTenantId(SecurityUtils.getCurrComId());
        }
        SysDept info = deptMapper.selectDeptById(dept.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
        if (!ObjectUtils.isEmpty(info)) {
            if (!ObjectUtils.isEmpty(info) && !UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
                throw new ServiceException("部门停用，不允许新增");
            }
            dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
        } else {
            dept.setAncestors("0");
        }
        //中能租户下数据同步至钉钉
        //钉钉新增部门
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentCreate = dingDingService.departmentCreate(dept);
                Object parse = JSONObject.parse(departmentCreate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    JSONObject result = jsonObject.getJSONObject("result");
                    String deptId = result.getString("dept_id");
                    dept.setDingDing(Long.parseLong(deptId));
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                // 新增部门岗位关联
                result = deptMapper.insertDept(dept);
                insertDeptPost(dept);
            }
        }
        return result;
    }

    /**
     * 新增用户岗位信息
     *
     * @param dept 用户对象
     */
    public void insertDeptPost(SysDept dept)
    {
        Long[] posts = dept.getPostIds();
        if (StringUtils.isNotEmpty(posts))
        {
            // 新增部门与岗位管理
            List<SysPostDept> list = new ArrayList<SysPostDept>(posts.length);
            for (Long postId : posts)
            {
                SysPostDept up = new SysPostDept();
                up.setDeptId(dept.getDeptId());
                up.setPostId(postId);
                list.add(up);
            }
            sysPostDeptMapper.batchPostDept(list);
        }
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int updateDept(SysDept dept)
    {
        //钉钉部门编辑
        String msg = "";
        //判断是否存在钉钉关系绑定并且是中能租户下数据
        if(null != dept.getDingDing() && SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentUpdate = dingDingService.departmentUpdate(dept);
                Object parse = JSONObject.parse(departmentUpdate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                SysDept newParentDept = deptMapper.selectDeptById(dept.getParentId());
                SysDept oldDept = deptMapper.selectDeptById(dept.getDeptId());
                if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept))
                {
                    String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
                    String oldAncestors = oldDept.getAncestors();
                    dept.setAncestors(newAncestors);
                    updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
                }

                // 删除用户与岗位关联
                sysPostDeptMapper.deletePostDeptByDeptId(dept.getDeptId());
                // 新增用户与岗位管理
                insertDeptPost(dept);

                result = deptMapper.updateDept(dept);
                if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && StringUtils.isNotEmpty(dept.getAncestors())
                        && !StringUtils.equals("0", dept.getAncestors()))
                {
                    // 如果该部门是启用状态，则启用该部门的所有上级部门
                    updateParentDeptStatusNormal(dept);
                }
            }
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private void updateParentDeptStatusNormal(SysDept dept)
    {
        String ancestors = dept.getAncestors();
        Long[] deptIds = Convert.toLongArray(ancestors);
        deptMapper.updateDeptStatusNormal(deptIds);
    }

    /**
     * 修改子元素关系
     *
     * @param deptId 被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors)
    {
        List<SysDept> children = deptMapper.selectChildrenDeptById(deptId);
        for (SysDept child : children)
        {
            child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            deptMapper.updateDeptChildren(children);
        }
    }

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(Long deptId)
    {
        SysDept dept = deptMapper.selectDeptById(deptId);
        //钉钉部门删除并且是中能租户下数据
        String msg = "";
        if(null != dept.getDingDing() && SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentUpdate = dingDingService.departmentDelete(dept.getDingDing());
                Object parse = JSONObject.parse(departmentUpdate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
              result = deptMapper.deleteDeptById(deptId);
            }
        }
        return result;
    }

    @Override
    public List<SysDept> selectSubCompanyList(String tenantId) {
        return deptMapper.selectSubCompanyList(tenantId);
    }

    /**
     * 查询所有集团及子公司
     */
    @Override
    public List<SysDept> selectGroupCompanyList() {
        return deptMapper.selectGroupCompanyList(SecurityUtils.getCurrComId());
    }

    @Override
    public String tenantIdDeptName(String tenantId) {

        return deptMapper.tenantIdDeptName(tenantId);
    }

    @Override
    public String insertDingdingDept(SysDept dept) {
        String departmentCreate = null;
        SysDept parentSysDept = deptMapper.selectDeptById(dept.getParentId());
        SysDept sysDept = deptMapper.selectDeptById(dept.getDeptId());
        if(ObjectUtils.isEmpty(parentSysDept.getDingDing())){
            return "0";
        }
        dept.setParentId(parentSysDept.getDingDing());
        try {
            departmentCreate = dingDingService.insertDingdingDept(dept);
            Object parse = JSONObject.parse(departmentCreate);
            JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
            String errmsg = jsonObject.getString("errmsg");
            if(errmsg.equals("ok")){
                JSONObject result = jsonObject.getJSONObject("result");
                String deptId = result.getString("dept_id");
                sysDept.setDingDing(Long.parseLong(deptId));
                deptMapper.updateDept(sysDept);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return departmentCreate;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<SysDept> list, SysDept t)
    {
        // 得到子节点列表
        List<SysDept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDept tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDept> getChildList(List<SysDept> list, SysDept t)
    {
        List<SysDept> tlist = new ArrayList<SysDept>();
        Iterator<SysDept> it = list.iterator();
        while (it.hasNext())
        {
            SysDept n = (SysDept) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getDeptId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDept> list, SysDept t)
    {
        return getChildList(list, t).size() > 0;
    }
}
