package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaCounterpartService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 对接人Service业务层处理
 *
 * @author ljb
 * @date 2022-11-29
 */
@Service
public class BaCounterpartServiceImpl extends ServiceImpl<BaCounterpartMapper, BaCounterpart> implements IBaCounterpartService
{
    private static final Logger log = LoggerFactory.getLogger(BaCounterpartServiceImpl.class);
    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private DistrictsMapper districtsMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;


    /**
     * 查询对接人
     *
     * @param id 对接人ID
     * @return 对接人
     */
    @Override
    public BaCounterpart selectBaCounterpartById(String id)
    {
        BaCounterpart counterpart = baCounterpartMapper.selectBaCounterpartById(id);
        //省、市、区
        if(StringUtils.isNotEmpty(counterpart.getProvince())){
            Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getProvince()));
            counterpart.setProvinceName(districts.getExtName());
        }
        if(StringUtils.isNotEmpty(counterpart.getCity())){
            Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getCity()));
            counterpart.setCityName(districts.getExtName());
        }
        if(StringUtils.isNotEmpty(counterpart.getArea())){
            Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getArea()));
            counterpart.setAreaName(districts.getExtName());
        }
        return counterpart;
    }

    /**
     * 查询对接人列表
     *
     * @param baCounterpart 对接人
     * @return 对接人
     */
    @Override
    public List<BaCounterpart> selectBaCounterpartList(BaCounterpart baCounterpart)
    {
        List<BaCounterpart> baCounterparts = baCounterpartMapper.selectBaCounterpartList(baCounterpart);
        for (BaCounterpart counterpart:baCounterparts) {
            //省、市、区
            if(StringUtils.isNotEmpty(counterpart.getProvince())){
                Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getProvince()));
                counterpart.setProvinceName(districts.getExtName());
            }
            if(StringUtils.isNotEmpty(counterpart.getCity())){
                Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getCity()));
                counterpart.setCityName(districts.getExtName());
            }
            if(StringUtils.isNotEmpty(counterpart.getArea())){
                Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(counterpart.getArea()));
                counterpart.setAreaName(districts.getExtName());
            }
        }
        return baCounterparts;
    }

    /**
     * 新增对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    @Override
    public int insertBaCounterpart(BaCounterpart baCounterpart)
    {
        baCounterpart.setId(getRedisIncreID.getId());
        baCounterpart.setCreateTime(DateUtils.getNowDate());
        baCounterpart.setCreateBy(SecurityUtils.getUsername());
        baCounterpart.setUserId(SecurityUtils.getUserId());
        baCounterpart.setDeptId(SecurityUtils.getDeptId());
        baCounterpart.setTenantId(SecurityUtils.getCurrComId());
        /*String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getCode());
        baCounterpart.setFlowId(flowId);*/
        int result = baCounterpartMapper.insertBaCounterpart(baCounterpart);
        /*if(result > 0){
            BaCounterpart counterpart = baCounterpartMapper.selectBaCounterpartById(baCounterpart.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(counterpart, AdminCodeEnum.COUNTERPART_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baCounterpartMapper.deleteBaCounterpartById(counterpart.getId());
                return 0;
            }else {
                counterpart.setStatus(AdminCodeEnum.COUNTERPART_STATUS_VERIFYING.getCode());
                baCounterpartMapper.updateBaCounterpart(counterpart);
            }
        }*/
        return result;
    }

    /**
     * 提交对接人审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaCounterpart counterpart, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(counterpart.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(counterpart.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getCode());
        //获取流程实例关联的业务对象
        BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(counterpart.getId());
        SysUser sysUser = iSysUserService.selectUserById(baCounterpart.getUserId());
        baCounterpart.setUserName(sysUser.getUserName());
        baCounterpart.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baCounterpart));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.COUNTERPART_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    @Override
    public int updateBaCounterpart(BaCounterpart baCounterpart)
    {
        baCounterpart.setUpdateTime(DateUtils.getNowDate());
        baCounterpart.setUpdateBy(SecurityUtils.getUsername());
        int result = baCounterpartMapper.updateBaCounterpart(baCounterpart);
        /*if(result > 0){
            BaCounterpart counterpart = baCounterpartMapper.selectBaCounterpartById(baCounterpart.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(counterpart, AdminCodeEnum.COUNTERPART_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                counterpart.setStatus(AdminCodeEnum.COUNTERPART_STATUS_VERIFYING.getCode());
                //创建时间
                counterpart.setCreateTime(counterpart.getUpdateTime());
                baCounterpartMapper.updateBaCounterpart(counterpart);
            }
        }*/
        return result;
    }

    /**
     * 批量删除对接人
     *
     * @param ids 需要删除的对接人ID
     * @return 结果
     */
    @Override
    public int deleteBaCounterpartByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
              BaCounterpart baCounterpart =  baCounterpartMapper.selectBaCounterpartById(id);
              baCounterpart.setFlag(1);
              baCounterpartMapper.updateBaCounterpart(baCounterpart);
            }
        }else {
            return 500;
        }
        return 200;
    }

    /**
     * 删除对接人信息
     *
     * @param id 对接人ID
     * @return 结果
     */
    @Override
    public int deleteBaCounterpartById(String id)
    {
        return baCounterpartMapper.deleteBaCounterpartById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baCounterpart.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.COUNTERPART_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baCounterpart.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baCounterpart.setStatus(AdminCodeEnum.COUNTERPART_STATUS_WITHDRAW.getCode());
                baCounterpartMapper.updateBaCounterpart(baCounterpart);
                String userId = String.valueOf(baCounterpart.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baCounterpart, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCounterpart baCounterpart, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baCounterpart.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_COUNTERPART.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }
}
