package com.business.system.service.workflow.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.GenConstants;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.DailyDTO;
import com.business.system.domain.dto.SysUserDepDTO;
import com.business.system.domain.dto.UserDailyDTO;
import com.business.system.mapper.*;
import com.business.system.service.IOaMaintenanceExpansionService;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaDailyService;
import org.springframework.util.ObjectUtils;

/**
 * 日报Service业务层处理
 *
 * @author single
 * @date 2023-10-09
 */
@Service
public class BaDailyServiceImpl extends ServiceImpl<BaDailyMapper, BaDaily> implements IBaDailyService
{
    @Autowired
    private BaDailyMapper baDailyMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaDailyFollowMapper baDailyFollowMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private IOaMaintenanceExpansionService iOaMaintenanceExpansionService;

    @Autowired
    private OaCalendarMapper oaCalendarMapper;

    /**
     * 查询日报
     *
     * @param id 日报ID
     * @return 日报
     */
    @Override
    public BaDaily selectBaDailyById(String id)
    {
        BaDaily baDaily = baDailyMapper.selectBaDailyById(id);
        SysUser user = sysUserMapper.selectUserById(baDaily.getUserId());
        baDaily.setUserName(user.getNickName());
        if(StringUtils.isNotEmpty(baDaily.getAffiliationProject())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baDaily.getAffiliationProject());
            baDaily.setProjectName(baProject.getName());
        }
        if(user.getUserId() != null){
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            baDaily.setDeptName(dept.getDeptName());
        }
        String maintenanceExpansion = baDaily.getMaintenanceExpansion();
        Map<String, OaMaintenanceExpansion> maintenanceExpansionMap = new HashMap<>();
        List<OaMaintenanceExpansion> maintenanceExpansionList = new ArrayList<>();
        if(StringUtils.isNotEmpty(maintenanceExpansion)){
            String[] maintenanceExpansionSplit = maintenanceExpansion.split(",");
            for(String maintenance : maintenanceExpansionSplit){
                OaMaintenanceExpansion oaMaintenanceExpansion = iOaMaintenanceExpansionService.selectOaMaintenanceExpansionById(maintenance);
                if(!ObjectUtils.isEmpty(oaMaintenanceExpansion)){
                    maintenanceExpansionList.add(oaMaintenanceExpansion);
                }
            }
            baDaily.setMaintenanceExpansionList(maintenanceExpansionList);
        }
        return baDaily;
    }

    /**
     * 查询日报列表
     *
     * @param baDaily 日报
     * @return 日报
     */
    @Override
    public List<BaDaily> selectBaDailyList(BaDaily baDaily)
    {
        List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
        baDailies.stream().forEach(itm ->{
            SysUser user = sysUserMapper.selectUserById(itm.getUserId());
            itm.setUserName(user.getNickName());
            if(StringUtils.isNotEmpty(itm.getAffiliationProject())){
                BaProject baProject = baProjectMapper.selectBaProjectById(itm.getAffiliationProject());
                itm.setProjectName(baProject.getName());
            }
        });
        return baDailies;
    }

    /**
     * 新增日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    @Override
    public int insertBaDaily(BaDaily baDaily)
    {
        //租户ID
        baDaily.setTenantId(SecurityUtils.getCurrComId());
        //判断发起人是否有保存数据
        if(StringUtils.isNotEmpty(baDaily.getId())){
            //BaDaily baDaily1 = baDailyMapper.selectBaDailyById(baDaily.getId());
            return baDailyMapper.updateBaDaily(baDaily);
        }
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String format = df.format(baDaily.getTime());

        baDaily.setId(getRedisIncreID.getId());
        baDaily.setCreateTime(DateUtils.getNowDate());
        baDaily.setUserId(SecurityUtils.getUserId());
        baDaily.setDeptId(SecurityUtils.getDeptId());
        //编辑日报消息推送
       /* BaMessage baMessage = new BaMessage();
        baMessage.setMId(baDaily.getUserId().toString());
        baMessage.setType("3");
        baMessage.setQueryTime(format);
        List<BaMessage> baMessages = baMessageMapper.selectBaMessageList(baMessage);
        if(baMessages.size() > 0){
            BaMessage baMessage1 = baMessages.get(0);
            BaMessage baMessage2 = baMessage1.setMessContent("今日已提交日报");
            baMessageMapper.updateBaMessage(baMessage2);
        }*/
        //查询未提交状态
        BaDaily baDaily1 = new BaDaily();
        baDaily1.setQueryTime(format);
        baDaily1.setUserId(baDaily.getUserId());
        baDaily1.setDailyState("2");
        List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily1);
        if(baDailies.size() > 0){
            baDailies.stream().forEach(itm->{
                baDailyMapper.deleteBaDailyById(itm.getId());
            });
        }
        return baDailyMapper.insertBaDaily(baDaily);
    }

    /**
     * 修改日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    @Override
    public int updateBaDaily(BaDaily baDaily)
    {
        baDaily.setUpdateTime(DateUtils.getNowDate());
        return baDailyMapper.updateBaDaily(baDaily);
    }

    /**
     * 批量删除日报
     *
     * @param ids 需要删除的日报ID
     * @return 结果
     */
    @Override
    public int deleteBaDailyByIds(String[] ids)
    {
        return baDailyMapper.deleteBaDailyByIds(ids);
    }

    /**
     * 删除日报信息
     *
     * @param id 日报ID
     * @return 结果
     */
    @Override
    public int deleteBaDailyById(String id)
    {
        return baDailyMapper.deleteBaDailyById(id);
    }

    @Override
    public List<UserDailyDTO> notSubmitted(BaDaily baDaily) {
        List<UserDailyDTO> userDailyDTOList = new ArrayList<>();
        //查询需要提交日报用户
        SysUserDepDTO sysUserDepDTO = new SysUserDepDTO();
        sysUserDepDTO.setHumanoid("2");
        //租户ID
        sysUserDepDTO.setTenantId(SecurityUtils.getCurrComId());
        if(baDaily.getDeptId() != null){
            sysUserDepDTO.setDeptId(String.valueOf(baDaily.getDeptId()));
        }
        if(baDaily.getUserId() != null){
            sysUserDepDTO.setUserId(String.valueOf(baDaily.getUserId()));
        }
        if(StringUtils.isNotEmpty(baDaily.getKeyWord())){
            sysUserDepDTO.setKeyWord(baDaily.getKeyWord());
        }
        List<String> list = sysUserMapper.selectUserByHumanoid(sysUserDepDTO);
        for (String itm:list) {
            UserDailyDTO userDailyDTO = new UserDailyDTO();
            //日报查询条件
            BaDaily baDaily1 = new BaDaily();
            baDaily1.setUserId(Long.valueOf(itm));
            baDaily1.setDailyState("2");
            baDaily1.setType(baDaily.getType());
            baDaily1.setQueryTime(baDaily.getQueryTime());
            baDaily1.setStart(baDaily.getStart());
            baDaily1.setEnd(baDaily.getEnd());
            baDaily1.setThisMonth(baDaily.getThisMonth());

            SysUser user = sysUserMapper.selectUserById(Long.valueOf(itm));
            userDailyDTO.setAvatar(user.getAvatar());
            userDailyDTO.setUserName(user.getNickName());
            userDailyDTO.setUserId(user.getUserId());
            String name = getName.getName(Long.valueOf(itm));
            if(StringUtils.isNotEmpty(name)){
                userDailyDTO.setPostName(name.substring(0,name.length()-1));
            }
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            userDailyDTO.setDeptName(dept.getDeptName());
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily1);
            if(baDailies.size() > 0){
                userDailyDTO.setDailyList(baDailies);
                userDailyDTOList.add(userDailyDTO);
            }
            if(baDaily.getUserId() != null){
                break;
            }
        }
        return userDailyDTOList;
    }

    @Override
    public List<UserDailyDTO> completedDaily(BaDaily baDaily) {
        List<UserDailyDTO> userDailyDTOList = new ArrayList<>();
        //查询需要提交日报用户
        SysUserDepDTO sysUserDepDTO = new SysUserDepDTO();
        sysUserDepDTO.setHumanoid("2");
        //租户
        sysUserDepDTO.setTenantId(SecurityUtils.getCurrComId());
        if(baDaily.getDeptId() != null){
            sysUserDepDTO.setDeptId(String.valueOf(baDaily.getDeptId()));
        }
        if(baDaily.getUserId() != null){
            sysUserDepDTO.setUserId(String.valueOf(baDaily.getUserId()));
        }
        if(StringUtils.isNotEmpty(baDaily.getKeyWord())){
            sysUserDepDTO.setKeyWord(baDaily.getKeyWord());
        }
        List<String> list = sysUserMapper.selectUserByHumanoid(sysUserDepDTO);
        for (String itm:list) {
            UserDailyDTO userDailyDTO = new UserDailyDTO();
            baDaily.setDailyState("1");
            baDaily.setUserId(Long.valueOf(itm));
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(itm));
            userDailyDTO.setAvatar(user.getAvatar());
            userDailyDTO.setUserName(user.getNickName());
            userDailyDTO.setUserId(user.getUserId());
            String name = getName.getName(Long.valueOf(itm));
            if(StringUtils.isNotEmpty(name)){
                userDailyDTO.setPostName(name.substring(0,name.length()-1));
            }
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            userDailyDTO.setDeptName(dept.getDeptName());
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
            if(baDailies.size() > 0){
                userDailyDTO.setDailyList(baDailies);
                userDailyDTOList.add(userDailyDTO);
            }
        }
        return userDailyDTOList;
    }

    @Override
    public int dailyFollow(BaDailyFollow baDailyFollow) {
        //删除原有关注
        QueryWrapper<BaDailyFollow> followQueryWrapper = new QueryWrapper<>();
        followQueryWrapper.eq("user_id",baDailyFollow.getUserId());
        List<BaDailyFollow> baDailyFollows = baDailyFollowMapper.selectList(followQueryWrapper);
        if(baDailyFollows.size() > 0){
            for (BaDailyFollow baDailyFollow1:baDailyFollows) {
                 baDailyFollowMapper.deleteBaDailyFollowById(baDailyFollow1.getId());
            }
        }
        if(StringUtils.isNotEmpty(baDailyFollow.getFollowed()) && !"".equals(baDailyFollow.getFollowed())){
            String[] split = baDailyFollow.getFollowed().split(",");
            for (String followedId:split) {
                //新增关联关系
                baDailyFollow.setId(getRedisIncreID.getId());
                baDailyFollow.setFollowedId(Long.parseLong(followedId));
                baDailyFollow.setCreateTime(DateUtils.getNowDate());
                SysUser user = sysUserMapper.selectUserById(baDailyFollow.getFollowedId());
                baDailyFollow.setDeptId(user.getDeptId());
                baDailyFollowMapper.insertBaDailyFollow(baDailyFollow);
            }
        }
        return 1;
    }

    @Override
    public List<BaDailyFollow> selectBaDailyFollowList(BaDailyFollow baDailyFollow) {
        List<BaDailyFollow> baDailyFollows = baDailyFollowMapper.selectBaDailyFollowList(baDailyFollow);
        for (BaDailyFollow baDailyFollow1:baDailyFollows) {
            SysUser user = sysUserMapper.selectUserById(baDailyFollow1.getFollowedId());
            baDailyFollow1.setUser(user);
        }
        return baDailyFollows;
    }

    @Override
    public int unFollow(BaDailyFollow baDailyFollow) {
        if(baDailyFollow.getUserId() != null && baDailyFollow.getFollowedId() != null){
           QueryWrapper<BaDailyFollow> followQueryWrapper = new QueryWrapper<>();
           followQueryWrapper.eq("user_id",baDailyFollow.getUserId());
           followQueryWrapper.eq("followed_id",baDailyFollow.getFollowedId());
            List<BaDailyFollow> baDailyFollows = baDailyFollowMapper.selectList(followQueryWrapper);
            for (BaDailyFollow baDailyFollow1:baDailyFollows) {
                baDailyFollowMapper.deleteBaDailyFollowById(baDailyFollow1.getId());
            }
        }
        return 1;
    }

    @Override
    public List<UserDailyDTO> followList(BaDaily baDaily) {
        List<UserDailyDTO> userDailyDTOList = new ArrayList<>();
        //查看所有关联日报
        BaDailyFollow baDailyFollow1 = new BaDailyFollow();
        baDailyFollow1.setUserId(baDaily.getUserId());
        if(baDaily.getDeptId() != null){
            baDailyFollow1.setDeptId(baDaily.getDeptId());
        }
        if(StringUtils.isNotEmpty(baDaily.getKeyWord())){
            baDailyFollow1.setKeyWord(baDaily.getKeyWord());
        }
        List<BaDailyFollow> baDailyFollows = baDailyFollowMapper.selectBaDailyFollowList(baDailyFollow1);
        for (BaDailyFollow baDailyFollow:baDailyFollows) {
            UserDailyDTO userDailyDTO = new UserDailyDTO();
            //查看关注人信息
            SysUser user = sysUserMapper.selectUserById(baDailyFollow.getFollowedId());
            userDailyDTO.setUserName(user.getNickName());
            userDailyDTO.setAvatar(user.getAvatar());
            userDailyDTO.setUserId(user.getUserId());
            String name = getName.getName(user.getUserId());
            if(StringUtils.isNotEmpty(name)){
                userDailyDTO.setPostName(name.substring(0,name.length()-1));
            }
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            userDailyDTO.setDeptName(dept.getDeptName());
            baDaily.setDailyState("1");
            baDaily.setUserId(Long.valueOf(user.getUserId()));
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
            if(baDailies.size() > 0){
                userDailyDTO.setDailyList(baDailies);
                userDailyDTOList.add(userDailyDTO);
            }
        }
        return userDailyDTOList;
    }

    @Override
    public int singleFollow(BaDailyFollow baDailyFollow) {
                //新增关联关系
                baDailyFollow.setId(getRedisIncreID.getId());
                baDailyFollow.setCreateTime(DateUtils.getNowDate());
                SysUser user = sysUserMapper.selectUserById(baDailyFollow.getFollowedId());
                if(StringUtils.isNull(user)){
                    return 0;
                }
                baDailyFollow.setDeptId(user.getDeptId());
        return baDailyFollowMapper.insertBaDailyFollow(baDailyFollow);
    }

    @Override
    public Map<String, Integer> dailyCount(BaDaily baDaily) {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(new Date());
        Map<String,Integer> map = new HashMap<>();
        //所有需要提交日报人员
        if(queryTime.equals(baDaily.getQueryTime())){
            SysUserDepDTO sysUserDepDTO = new SysUserDepDTO();
            sysUserDepDTO.setHumanoid("2");
            //租户ID
            sysUserDepDTO.setTenantId(baDaily.getTenantId());
            List<String> list = sysUserMapper.selectUserByHumanoid(sysUserDepDTO);
            map.put("应交人数",list.size());
        }else {
            baDaily.setGroup("分组");
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
            map.put("应交人数",baDailies.size());
        }
        //提交人数
        int submitNumber = 0;
        //未提交
        int notSubmitted = 0;
        baDaily.setDailyState("1");
       /* if(StringUtils.isNotEmpty(baDaily.getQueryTime())){
            baDaily.setGroup("分组");
        }else {
            baDaily.setGroup(null);
        }*/
        if(StringUtils.isNotEmpty(baDaily.getWeekTime())){
            String[] split = baDaily.getWeekTime().split(",");
            //日期遍历
            for (String dayTime:split) {
                baDaily.setQueryTime(dayTime);
                baDaily.setDailyState("1");
                List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
                submitNumber = submitNumber+baDailies.size();
                baDaily.setDailyState("2");
                List<BaDaily> dailies = baDailyMapper.selectBaDailyList(baDaily);
                notSubmitted = notSubmitted+dailies.size();
            }
            map.put("提交人数",submitNumber);
            map.put("按时提交",submitNumber);
            map.put("未提交",notSubmitted);
        }else if(StringUtils.isNotEmpty(baDaily.getThisMonth())){
            //查看日历
            OaCalendar oaCalendar = new OaCalendar();
            if(StringUtils.isNotEmpty(baDaily.getThisMonth())){
                oaCalendar.setDate(baDaily.getThisMonth());
            }
            List<OaCalendar> oaCalendars = oaCalendarMapper.selectOaCalendarList(oaCalendar);
            for (OaCalendar calendar:oaCalendars) {
                baDaily.setQueryTime(calendar.getDate());
                baDaily.setDailyState("1");
                List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
                submitNumber = submitNumber+baDailies.size();
                baDaily.setDailyState("2");
                List<BaDaily> dailies = baDailyMapper.selectBaDailyList(baDaily);
                notSubmitted = notSubmitted+dailies.size();
            }
            map.put("提交人数",submitNumber);
            map.put("按时提交",submitNumber);
            map.put("未提交",notSubmitted);
        }else {
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
            map.put("提交人数",baDailies.size());
            map.put("按时提交",baDailies.size());
            baDaily.setDailyState("2");
            List<BaDaily> dailies = baDailyMapper.selectBaDailyList(baDaily);
            map.put("未提交",dailies.size());
        }
        return map;
    }

    @Override
    public List<DailyDTO> dailyStatistics(BaDaily baDaily) {
        //日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());
        //获取年月
        int index = date.lastIndexOf("-");
        String result = date.substring(0,index);
        //日报统计数据
        List<DailyDTO> dailyDTOS = new ArrayList<>();

        //获取所有提交人员
        List<String> userList = baDailyMapper.submitUser(baDaily);
        for (String userId:userList) {
            DailyDTO dailyDTO = new DailyDTO();
            //用户信息
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
            if(StringUtils.isNotNull(user)) {
                //员工名称
                dailyDTO.setUserName(user.getNickName());
                //员工部门
                if (user.getDeptId() != null) {
                    dailyDTO.setDeptName(sysDeptMapper.selectDeptById(user.getDeptId()).getDeptName());
                }
            }
            //日报提交数
            baDaily.setUserId(Long.valueOf(userId));
            baDaily.setDailyState("1");
            baDaily.setQueryTime("");
            List<BaDaily> baDailies = baDailyMapper.selectBaDailyList(baDaily);
            dailyDTO.setSubmitNumber(baDailies.size());
            //日报提交信息
            dailyDTO.setSubmitDaily(baDailies);
            //未提交数
            baDaily.setDailyState("2");
            List<BaDaily> dailies = baDailyMapper.selectBaDailyList(baDaily);
            dailyDTO.setNotSubmitNumber(dailies.size());
            //日报未提交信息
            dailyDTO.setNotSubmitDaily(dailies);
            //查看日历
            OaCalendar oaCalendar = new OaCalendar();
            if(StringUtils.isNotEmpty(baDaily.getThisMonth())){
                oaCalendar.setDate(baDaily.getThisMonth());
            }else {
                oaCalendar.setDate(result);
            }
            List<OaCalendar> oaCalendars = oaCalendarMapper.selectOaCalendarList(oaCalendar);
            //本月执行范围
            List<OaCalendar> list = new ArrayList<>();
            for (OaCalendar calendar:oaCalendars) {
                if(calendar.getDate().compareTo(date) < 0){
                    baDaily.setQueryTime(calendar.getDate());
                    baDaily.setDailyState("");
                    List<BaDaily> baDailies1 = baDailyMapper.selectBaDailyList(baDaily);
                    if(baDailies1.size() > 0){
                        if("1".equals(baDailies1.get(0).getDailyState())){
                            calendar.setDailyState("已提交");
                        }else if("2".equals(baDailies1.get(0).getDailyState())){
                            calendar.setDailyState("未提交");
                        }
                    }
                    list.add(calendar);
                }
            }
            dailyDTO.setOaCalendars(list);
            dailyDTOS.add(dailyDTO);
        }

        return dailyDTOS;
    }

}
