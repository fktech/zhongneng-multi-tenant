package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.AssayApprovalService;
import com.business.system.service.workflow.SampleApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 化验申请审批结果:审批通过
 */
@Service("assayApprovalContent:processApproveResult:pass")
@Slf4j
public class AssayApprovalAgreeServiceImpl extends AssayApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ASSAY_STATUS_PASS.getCode());
        return result;
    }
}
