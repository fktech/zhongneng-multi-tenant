package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEvection;
import com.business.system.domain.OaOvertime;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEvectionApprovalService;
import com.business.system.service.workflow.IOaOvertimeApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 出差申请审批结果:审批通过
 */
@Service("oaEvectionApprovalContent:processApproveResult:pass")
@Slf4j
public class OaEvectionApprovalAgreeServiceImpl extends IOaEvectionApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaEvection oaEvection = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
        return result;
    }
}
