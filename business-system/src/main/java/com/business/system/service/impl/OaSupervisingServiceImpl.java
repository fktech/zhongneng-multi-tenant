package com.business.system.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.OaApprovalOpinions;
import com.business.system.domain.OaAssigned;
import com.business.system.domain.OaSupervisingRecord;
import com.business.system.domain.vo.OaSupervisingReceiveVO;
import com.business.system.domain.vo.OaSupervisingStatVO;
import com.business.system.mapper.*;
import com.business.system.service.IOaSupervisingRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.domain.OaSupervising;
import com.business.system.service.IOaSupervisingService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 督办Service业务层处理
 *
 * @author ljb
 * @date 2023-11-20
 */
@Service
public class OaSupervisingServiceImpl extends ServiceImpl<OaSupervisingMapper, OaSupervising> implements IOaSupervisingService
{
    @Autowired
    private OaSupervisingMapper oaSupervisingMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaAssignedMapper oaAssignedMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OaSupervisingRecordMapper oaSupervisingRecordMapper;

    @Autowired
    private IOaSupervisingRecordService oaSupervisingRecordService;

    @Autowired
    private OaApprovalOpinionsMapper oaApprovalOpinionsMapper;

    /**
     * 查询督办
     *
     * @param id 督办ID
     * @return 督办
     */
    @Override
    public OaSupervising selectOaSupervisingById(String id)
    {
        OaSupervising oaSupervising = oaSupervisingMapper.selectOaSupervisingById(id);
        //申请人
        if(oaSupervising.getUserId() != null){
           oaSupervising.setUserName(sysUserMapper.selectUserById(oaSupervising.getUserId()).getNickName());
        }
        //申请人部门
        if(oaSupervising.getDeptId() != null){
            oaSupervising.setDeptName(sysDeptMapper.selectDeptById(oaSupervising.getDeptId()).getDeptName());
        }
        //主办人
        if(StringUtils.isNotEmpty(oaSupervising.getSponsor())){
           String sponsorName = "";
            String[] split = oaSupervising.getSponsor().split(";");
            for (String userId:split) {
                String[] split1 = userId.split(",");
                sponsorName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + sponsorName;
            }
            oaSupervising.setSponsorName(sponsorName.substring(0,sponsorName.length()-1));
        }
        //协办人
        if(StringUtils.isNotEmpty(oaSupervising.getSupported())){
            String supportedName = "";
            String[] split = oaSupervising.getSupported().split(";");
            for (String userId:split) {
                String[] split1 = userId.split(",");
                supportedName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + supportedName;
            }
            oaSupervising.setSupportedName(supportedName.substring(0,supportedName.length()-1));
        }
        //办理记录
        OaSupervisingRecord oaSupervisingRecord = new OaSupervisingRecord();
        oaSupervisingRecord.setSupervisingId(oaSupervising.getId());
        List<OaSupervisingRecord> oaSupervisingRecords = oaSupervisingRecordService.selectOaSupervisingRecordList(oaSupervisingRecord);
        oaSupervising.setOaSupervisingRecordList(oaSupervisingRecords);
        //所有批复意见
        List<Map<String,String>> mapList = new ArrayList<>();
        QueryWrapper<OaApprovalOpinions> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("association_id",oaSupervising.getId());
        queryWrapper.orderByAsc("create_time");
        List<OaApprovalOpinions> oaApprovalOpinions = oaApprovalOpinionsMapper.selectList(queryWrapper);
        for (OaApprovalOpinions opinions:oaApprovalOpinions) {
            Map<String,String> map = new HashMap<>();
            SysUser user = sysUserMapper.selectUserById(opinions.getUserId());
            if(StringUtils.isNotNull(user)){
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                map.put(dept.getDeptName()+user.getNickName(),opinions.getOpinion());
            }
            mapList.add(map);
        }
        oaSupervising.setMap(mapList);
        return oaSupervising;
    }

    /**
     * 查询督办列表
     *
     * @param oaSupervising 督办
     * @return 督办
     */
    @Override
    public List<OaSupervising> selectOaSupervisingList(OaSupervising oaSupervising)
    {
        List<OaSupervising> oaSupervisings = oaSupervisingMapper.selectOaSupervisingList(oaSupervising);
        for (OaSupervising supervising:oaSupervisings) {
            //申请人
            if(supervising.getUserId() != null){
                supervising.setUserName(sysUserMapper.selectUserById(supervising.getUserId()).getNickName());
            }
            //部门名称
            if(supervising.getDeptId() != null){
                supervising.setDeptName(sysDeptMapper.selectDeptById(supervising.getDeptId()).getDeptName());
            }
            //主办人
            if(StringUtils.isNotEmpty(supervising.getSponsor())){
                String sponsorName = "";
                String[] split = supervising.getSponsor().split(";");
                for (String userId:split) {
                    String[] split1 = userId.split(",");
                    sponsorName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + sponsorName;
                }
                supervising.setSponsorName(sponsorName.substring(0,sponsorName.length()-1));
            }
            //协办人
            if(StringUtils.isNotEmpty(supervising.getSupported())){
                String supportedName = "";
                String[] split = supervising.getSupported().split(";");
                for (String userId:split) {
                    String[] split1 = userId.split(",");
                    supportedName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + supportedName;
                }
                supervising.setSupportedName(supportedName.substring(0,supportedName.length()-1));
            }
            //发起时间
            supervising.setCreationTime(supervising.getCreateTime());
        }
        return oaSupervisings;
    }

    /**
     * 新增督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    @Override
    public int insertOaSupervising(OaSupervising oaSupervising)
    {
        //判断是否为保存数据提交
        if(StringUtils.isNotEmpty(oaSupervising.getId())){
            return this.updateOaSupervising(oaSupervising);
        }
        oaSupervising.setId(getRedisIncreID.getId());
        oaSupervising.setCreateTime(DateUtils.getNowDate());
        oaSupervising.setCreateBy(SecurityUtils.getUsername());
        oaSupervising.setState("0");
        oaSupervising.setSubmitTime(DateUtils.getNowDate());
        //租户
        oaSupervising.setTenantId(SecurityUtils.getCurrComId());
        //主办人交办数据
        if(StringUtils.isNotEmpty(oaSupervising.getSponsor())){
            String[] split = oaSupervising.getSponsor().split(";");
            int count = 0;
            for (String userId:split) {
                count++;
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setId(getRedisIncreID.getId());
                String[] split1 = userId.split(",");
                oaAssigned.setTransactors(split1[split1.length-1]);
                oaAssigned.setSupervisingId(oaSupervising.getId());
                oaAssigned.setType("1");//主办
                oaAssigned.setCreateTime(DateUtils.getNowDate());
                //租户
                oaAssigned.setTenantId(SecurityUtils.getCurrComId());
                oaAssignedMapper.insertOaAssigned(oaAssigned);
            }
            oaSupervising.setHandleNum(count);
        }
        //代办人
        if(StringUtils.isNotEmpty(oaSupervising.getSupported())){
            String[] split = oaSupervising.getSupported().split(";");
            for (String userId:split) {
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setId(getRedisIncreID.getId());
                String[] split1 = userId.split(",");
                oaAssigned.setTransactors(split1[split1.length-1]);
                oaAssigned.setSupervisingId(oaSupervising.getId());
                oaAssigned.setType("2");//代办
                oaAssigned.setCreateTime(DateUtils.getNowDate());
                //租户
                oaAssigned.setTenantId(SecurityUtils.getCurrComId());
                oaAssignedMapper.insertOaAssigned(oaAssigned);
            }
        }
        return oaSupervisingMapper.insertOaSupervising(oaSupervising);
    }

    /**
     * 修改督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    @Override
    public int updateOaSupervising(OaSupervising oaSupervising)
    {
        oaSupervising.setUpdateTime(DateUtils.getNowDate());
        oaSupervising.setSubmitTime(DateUtils.getNowDate());
        if("3".equals(oaSupervising.getState())){
            oaSupervising.setState("0");
        }
        //删除原有交办数据
        QueryWrapper<OaAssigned> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("supervising_id",oaSupervising.getId());
        List<OaAssigned> oaAssigneds = oaAssignedMapper.selectList(queryWrapper);
        for (OaAssigned oaAssigned:oaAssigneds) {
            oaAssignedMapper.deleteOaAssignedById(oaAssigned.getId());
        }
        //主办人交办数据
        if(StringUtils.isNotEmpty(oaSupervising.getSponsor())){
            String[] split = oaSupervising.getSponsor().split(";");
            int count = 0;
            for (String userId:split) {
                count++;
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setId(getRedisIncreID.getId());
                String[] split1 = userId.split(",");
                oaAssigned.setTransactors(split1[split1.length-1]);
                oaAssigned.setSupervisingId(oaSupervising.getId());
                oaAssigned.setType("1");//主办
                oaAssigned.setCreateTime(DateUtils.getNowDate());
                //租户ID
                oaAssigned.setTenantId(SecurityUtils.getCurrComId());
                oaAssignedMapper.insertOaAssigned(oaAssigned);
            }
            oaSupervising.setHandleNum(count);
        }
        //代办人
        if(StringUtils.isNotEmpty(oaSupervising.getSupported())) {
            String[] split = oaSupervising.getSupported().split(";");
            for (String userId : split) {
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setId(getRedisIncreID.getId());
                String[] split1 = userId.split(",");
                oaAssigned.setTransactors(split1[split1.length-1]);
                oaAssigned.setSupervisingId(oaSupervising.getId());
                oaAssigned.setType("2");//代办
                oaAssigned.setCreateTime(DateUtils.getNowDate());
                //租户ID
                oaAssigned.setTenantId(SecurityUtils.getCurrComId());
                oaAssignedMapper.insertOaAssigned(oaAssigned);
            }
        }
        return oaSupervisingMapper.updateOaSupervising(oaSupervising);
    }

    /**
     * 批量删除督办
     *
     * @param ids 需要删除的督办ID
     * @return 结果
     */
    @Override
    public int deleteOaSupervisingByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaSupervising supervising = oaSupervisingMapper.selectOaSupervisingById(id);
                supervising.setFlag(1L);
                oaSupervisingMapper.updateOaSupervising(supervising);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除督办信息
     *
     * @param id 督办ID
     * @return 结果
     */
    @Override
    public int deleteOaSupervisingById(String id)
    {
        return oaSupervisingMapper.deleteOaSupervisingById(id);
    }

    @Override
    public int reply(OaSupervising oaSupervising) {
        //新增办理记录
        if(oaSupervising.getOaSupervisingRecordList().size() > 0){
            //删除原有办理记录
            OaSupervisingRecord supervisingRecord = new OaSupervisingRecord();
            supervisingRecord.setSupervisingId(oaSupervising.getId());
            List<OaSupervisingRecord> oaSupervisingRecords = oaSupervisingRecordMapper.selectOaSupervisingRecordList(supervisingRecord);
            for (OaSupervisingRecord oaSupervisingRecord:oaSupervisingRecords) {
                oaSupervisingRecordMapper.deleteOaSupervisingRecordById(oaSupervisingRecord.getId());
            }
            for (OaSupervisingRecord oaSupervisingRecord:oaSupervising.getOaSupervisingRecordList()) {
                oaSupervisingRecord.setId(getRedisIncreID.getId());
                oaSupervisingRecord.setSupervisingId(oaSupervising.getId());
                oaSupervisingRecordMapper.insertOaSupervisingRecord(oaSupervisingRecord);
            }
        }
        //修改交办状态
        if(StringUtils.isNotEmpty(oaSupervising.getAssignedId())){
            OaAssigned oaAssigned = oaAssignedMapper.selectOaAssignedById(oaSupervising.getAssignedId());
            oaAssigned.setState("1");
            //新增批复意见
            OaApprovalOpinions oaApprovalOpinions = new OaApprovalOpinions();
            oaApprovalOpinions.setId(getRedisIncreID.getId());
            oaApprovalOpinions.setOpinion(oaSupervising.getApprovalOpinions());
            oaApprovalOpinions.setCreateTime(DateUtils.getNowDate());
            oaApprovalOpinions.setUserId(Long.valueOf(oaAssigned.getTransactors()));
            oaApprovalOpinions.setAssociationId(oaAssigned.getSupervisingId());
            oaApprovalOpinionsMapper.insertOaApprovalOpinions(oaApprovalOpinions);
            oaAssignedMapper.updateOaAssigned(oaAssigned);
        }
        //修改督办
        if(StringUtils.isNotEmpty(oaSupervising.getId())){
            OaSupervising oaSupervising1 = oaSupervisingMapper.selectOaSupervisingById(oaSupervising.getId());
            oaSupervising1.setHandleNum(oaSupervising1.getHandleNum() - 1);
            if(oaSupervising1.getHandleNum() == 0){
                //清除提醒时间
                oaSupervising1.setReminderTime("");
                oaSupervising1.setState("1");
            }
            oaSupervisingMapper.updateOaSupervising(oaSupervising1);
        }
        return 1;
    }

    @Override
    public int revoke(String id) {
        if(StringUtils.isNotEmpty(id)){
            //删除交办任务
            QueryWrapper<OaAssigned> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("supervising_id",id);
            List<OaAssigned> oaAssigneds = oaAssignedMapper.selectList(queryWrapper);
            for (OaAssigned oaAssigned:oaAssigneds) {
                /*oaAssigned.setFlag(1L);
                oaAssignedMapper.updateOaAssigned(oaAssigned);*/
                oaAssignedMapper.deleteOaAssignedById(oaAssigned.getId());
            }
            //删除办理数据
           /* QueryWrapper<OaSupervisingRecord> recordQueryWrapper = new QueryWrapper<>();
            recordQueryWrapper.eq("supervising_id",id);
            List<OaSupervisingRecord> oaSupervisingRecords = oaSupervisingRecordMapper.selectList(recordQueryWrapper);
            for (OaSupervisingRecord oaSupervisingRecord:oaSupervisingRecords) {
                oaSupervisingRecord.setFlag(1L);
                oaSupervisingRecordMapper.updateOaSupervisingRecord(oaSupervisingRecord);
            }*/
            //删除所有批复意见
            QueryWrapper<OaApprovalOpinions> opinionsQueryWrapper = new QueryWrapper<>();
            opinionsQueryWrapper.eq("association_id",id);
            List<OaApprovalOpinions> oaApprovalOpinions = oaApprovalOpinionsMapper.selectList(opinionsQueryWrapper);
            for (OaApprovalOpinions opinions:oaApprovalOpinions) {
                oaApprovalOpinionsMapper.deleteOaApprovalOpinionsById(opinions.getId());
            }
            //修改督办状态
            OaSupervising supervising = oaSupervisingMapper.selectOaSupervisingById(id);
            supervising.setState("3");
            supervising.setSubmitState("1");
            oaSupervisingMapper.updateOaSupervising(supervising);
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public int approval(OaSupervising oaSupervising) {
        if(StringUtils.isNotEmpty(oaSupervising.getId())){
            OaSupervising supervising = oaSupervisingMapper.selectOaSupervisingById(oaSupervising.getId());
            //批复意见
            OaApprovalOpinions oaApprovalOpinions = new OaApprovalOpinions();
            oaApprovalOpinions.setId(getRedisIncreID.getId());
            oaApprovalOpinions.setOpinion(oaSupervising.getApprovalOpinions());
            oaApprovalOpinions.setCreateTime(DateUtils.getNowDate());
            oaApprovalOpinions.setUserId(supervising.getUserId());
            oaApprovalOpinions.setAssociationId(oaSupervising.getId());
            oaApprovalOpinionsMapper.insertOaApprovalOpinions(oaApprovalOpinions);
            //查询交办数据
            QueryWrapper<OaAssigned> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("supervising_id",supervising.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.eq("state","1");
            List<OaAssigned> oaAssigneds = oaAssignedMapper.selectList(queryWrapper);
            int count = 0;
            for (OaAssigned assigned:oaAssigneds) {
                count++;
                assigned.setState("0");
                oaAssignedMapper.updateOaAssigned(assigned);
            }
            //修改督办状态
            supervising.setHandleNum(count);
            supervising.setState("0");
            oaSupervisingMapper.updateOaSupervising(supervising);
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public int completion(OaSupervising oaSupervising) {
        OaSupervising supervising = oaSupervisingMapper.selectOaSupervisingById(oaSupervising.getId());
        supervising.setState("2");
        //批复意见
        OaApprovalOpinions oaApprovalOpinions = new OaApprovalOpinions();
        oaApprovalOpinions.setId(getRedisIncreID.getId());
        oaApprovalOpinions.setOpinion(oaSupervising.getApprovalOpinions());
        oaApprovalOpinions.setCreateTime(DateUtils.getNowDate());
        oaApprovalOpinions.setUserId(supervising.getUserId());
        oaApprovalOpinions.setAssociationId(oaSupervising.getId());
        oaApprovalOpinionsMapper.insertOaApprovalOpinions(oaApprovalOpinions);
        return oaSupervisingMapper.updateOaSupervising(supervising);
    }

    /**
     * 督办事项统计
     */
    @Override
    public OaSupervisingStatVO oaSupervisingStat(OaSupervising oaSupervising) {
        OaSupervisingStatVO oaSupervisingStatVO = new OaSupervisingStatVO();
        oaSupervising.setParamType("receive");
        oaSupervisingStatVO.setStartCount(oaSupervisingMapper.countOaSupervisingList(oaSupervising));
        oaSupervising.setParamType("end");
        oaSupervisingStatVO.setEndCount(oaSupervisingMapper.countOaSupervisingList(oaSupervising));

        long count = 0L;
        long replyCount = 0L;
        oaSupervising.setParamType("noResponse");
        List<OaSupervising> oaSupervisings = oaSupervisingMapper.selectOaSupervisingList(oaSupervising);
        if(!ObjectUtils.isEmpty(oaSupervisings)){
            for(OaSupervising supervising : oaSupervisings){
                if(!ObjectUtils.isEmpty(supervising) && StringUtils.isNotEmpty(supervising.getSponsor())){
                    String[] split = supervising.getSponsor().split(";");
                    count = count + split.length;
                }
                if(supervising.getHandleNum() == null){
                    supervising.setHandleNum(0);
                }
                replyCount = replyCount + supervising.getHandleNum();
            }
            oaSupervisingStatVO.setReadCount(count);
            oaSupervisingStatVO.setReplyCount(count - replyCount);
        }
        return oaSupervisingStatVO;
    }

    @Override
    public List<OaSupervisingReceiveVO> userOaSupervisingStat(OaSupervising oaSupervising) {
        //人员督办接收处理统计, 判断办理中或未回告
        List<OaSupervising> oaSupervisings = oaSupervisingMapper.selectOaSupervisingList(oaSupervising);
        List<OaSupervisingReceiveVO> oaSupervisingReceiveVOList = null;
        Map<String, OaSupervisingReceiveVO> oaSupervisingReceiveVOMap = new LinkedHashMap<>();
        String userId = "";
        OaSupervisingReceiveVO oaSupervisingReceiveVO = null;
        OaSupervisingRecord oaSupervisingRecord = null;
        List<OaSupervisingRecord> oaSupervisingRecords = null;
        if(!CollectionUtils.isEmpty(oaSupervisings)){
            for(OaSupervising supervising : oaSupervisings){
                if(!ObjectUtils.isEmpty(supervising)){
                    if(StringUtils.isNotEmpty(supervising.getSponsor())){
                        String[] split = supervising.getSponsor().split(";");
                        for (String userStr : split) {
                            String[] split1 = userStr.split(",");
                            userId = split1[split1.length - 1];
                            if("receive".equals(oaSupervising.getParamType())){ //人员督办接收处理统计
                                if(oaSupervisingReceiveVOMap.containsKey(userId)){
                                    oaSupervisingReceiveVO = oaSupervisingReceiveVOMap.get(userId);
                                } else {
                                    oaSupervisingReceiveVO = new OaSupervisingReceiveVO();
                                }
                                SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(userId));
                                if(!ObjectUtils.isEmpty(sysUser)){
                                    oaSupervisingReceiveVO.setUserName(sysUser.getNickName());
                                }
                                if(oaSupervisingReceiveVO.getCount() == null){
                                    oaSupervisingReceiveVO.setCount(new Long(0));
                                }
                                /*OaSupervisingRecord oaSupervisingRecord1 = new OaSupervisingRecord();
                                oaSupervisingRecord1.setUserId(Long.valueOf(userId));
                                oaSupervisingRecord1.setSupervisingId(supervising.getId());
                                List<OaSupervisingRecord> oaSupervisingRecords1 = oaSupervisingRecordMapper.selectOaSupervisingRecordList(oaSupervisingRecord1);*/
                                OaAssigned oaAssigned = new OaAssigned();
                                oaAssigned.setTransactors(userId);
                                oaAssigned.setSupervisingId(supervising.getId());
                                oaAssigned.setState("1");
                                oaAssigned.setType("1");
                                List<OaAssigned> oaAssigneds = oaAssignedMapper.selectOaAssignedList(oaAssigned);
                                if(!CollectionUtils.isEmpty(oaAssigneds)){
                                    oaSupervisingReceiveVO.setCount(Long.valueOf(oaSupervisingReceiveVO.getCount() + oaAssigneds.size()));
                                }
                                oaSupervisingReceiveVOMap.put(userId, oaSupervisingReceiveVO);
                            } else if("noResponse".equals(oaSupervising.getParamType())){
                                if(oaSupervisingReceiveVOMap.containsKey(userId)){
                                    oaSupervisingReceiveVO = oaSupervisingReceiveVOMap.get(userId);
                                } else {
                                    oaSupervisingReceiveVO = new OaSupervisingReceiveVO();
                                }
                                SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(userId));
                                if(!ObjectUtils.isEmpty(sysUser)){
                                    oaSupervisingReceiveVO.setUserName(sysUser.getNickName());
                                }
                                if(oaSupervisingReceiveVO.getCount() == null){
                                    oaSupervisingReceiveVO.setCount(new Long(0));
                                }
                                /*oaSupervisingReceiveVO.setCount(oaSupervisingReceiveVO.getCount() + 1);
                                OaSupervisingRecord oaSupervisingRecord1 = new OaSupervisingRecord();
                                oaSupervisingRecord1.setUserId(Long.valueOf(userId));
                                oaSupervisingRecord1.setSupervisingId(supervising.getId());
                                oaSupervisingRecord1.setFlag(new Long(0));
                                List<OaSupervisingRecord> oaSupervisingRecords1 = oaSupervisingRecordMapper.selectOaSupervisingRecordList(oaSupervisingRecord1);
                                if(!CollectionUtils.isEmpty(oaSupervisingRecords1)){
                                    oaSupervisingReceiveVO.setCount(Long.valueOf(oaSupervisingReceiveVO.getCount() - oaSupervisingRecords1.size()));
                                }*/
                                OaAssigned oaAssigned = new OaAssigned();
                                oaAssigned.setTransactors(userId);
                                oaAssigned.setSupervisingId(supervising.getId());
                                oaAssigned.setState("0");
                                oaAssigned.setType("1");
                                List<OaAssigned> oaAssigneds = oaAssignedMapper.selectOaAssignedList(oaAssigned);
                                if(!CollectionUtils.isEmpty(oaAssigneds)){
                                    oaSupervisingReceiveVO.setCount(Long.valueOf(oaSupervisingReceiveVO.getCount() + oaAssigneds.size()));
                                }
                                oaSupervisingReceiveVOMap.put(userId, oaSupervisingReceiveVO);
                            } else if("end".equals(oaSupervising.getParamType())){ //人员已办结督办统计
                                if (oaSupervisingReceiveVOMap.containsKey(userId)) {
                                    oaSupervisingReceiveVO = oaSupervisingReceiveVOMap.get(userId);
                                } else {
                                    oaSupervisingReceiveVO = new OaSupervisingReceiveVO();
                                }
                                SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(userId));
                                if (!ObjectUtils.isEmpty(sysUser)) {
                                    oaSupervisingReceiveVO.setUserName(sysUser.getNickName());
                                }
                                if(oaSupervisingReceiveVO.getCount() == null){
                                    oaSupervisingReceiveVO.setCount(new Long(0));
                                }
                                oaSupervisingReceiveVO.setCount(oaSupervisingReceiveVO.getCount() + 1);
                                oaSupervisingReceiveVOMap.put(userId, oaSupervisingReceiveVO);
                            }
                        }
                    }
                }
            }
            oaSupervisingReceiveVOList = oaSupervisingReceiveVOMap.values().stream().collect(Collectors.toList());
        } else {
            oaSupervisingReceiveVOList = new ArrayList<>();
        }
        List<OaSupervisingReceiveVO> sortedList = oaSupervisingReceiveVOList.stream()
                .sorted(Comparator.comparing(OaSupervisingReceiveVO::getCount).reversed())
                .collect(Collectors.toList());
        return sortedList;
    }

}
