package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.StringUtils;
import com.business.system.mapper.BaGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaEnterpriseRelevanceMapper;
import com.business.system.domain.BaEnterpriseRelevance;
import com.business.system.service.IBaEnterpriseRelevanceService;

/**
 * 终端企业关联Service业务层处理
 *
 * @author ljb
 * @date 2023-03-01
 */
@Service
public class BaEnterpriseRelevanceServiceImpl extends ServiceImpl<BaEnterpriseRelevanceMapper, BaEnterpriseRelevance> implements IBaEnterpriseRelevanceService
{
    @Autowired
    private BaEnterpriseRelevanceMapper baEnterpriseRelevanceMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    /**
     * 查询终端企业关联
     *
     * @param id 终端企业关联ID
     * @return 终端企业关联
     */
    @Override
    public BaEnterpriseRelevance selectBaEnterpriseRelevanceById(String id)
    {
        BaEnterpriseRelevance baEnterpriseRelevance = baEnterpriseRelevanceMapper.selectBaEnterpriseRelevanceById(id);
        if(StringUtils.isNotNull(baEnterpriseRelevance)){
            if(StringUtils.isNotEmpty(baEnterpriseRelevance.getGoodsId())){
                baEnterpriseRelevance.setGoodsName(baGoodsMapper.selectBaGoodsById(baEnterpriseRelevance.getGoodsId()).getName());
            }
        }
        return baEnterpriseRelevance;
    }

    /**
     * 查询终端企业关联列表
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 终端企业关联
     */
    @Override
    public List<BaEnterpriseRelevance> selectBaEnterpriseRelevanceList(BaEnterpriseRelevance baEnterpriseRelevance)
    {
        return baEnterpriseRelevanceMapper.selectBaEnterpriseRelevanceList(baEnterpriseRelevance);
    }

    /**
     * 新增终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    @Override
    public int insertBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance)
    {
        return baEnterpriseRelevanceMapper.insertBaEnterpriseRelevance(baEnterpriseRelevance);
    }

    /**
     * 修改终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    @Override
    public int updateBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance)
    {
        return baEnterpriseRelevanceMapper.updateBaEnterpriseRelevance(baEnterpriseRelevance);
    }

    /**
     * 批量删除终端企业关联
     *
     * @param ids 需要删除的终端企业关联ID
     * @return 结果
     */
    @Override
    public int deleteBaEnterpriseRelevanceByIds(String[] ids)
    {
        return baEnterpriseRelevanceMapper.deleteBaEnterpriseRelevanceByIds(ids);
    }

    /**
     * 删除终端企业关联信息
     *
     * @param id 终端企业关联ID
     * @return 结果
     */
    @Override
    public int deleteBaEnterpriseRelevanceById(String id)
    {
        return baEnterpriseRelevanceMapper.deleteBaEnterpriseRelevanceById(id);
    }

}
