package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaSupervisingRecord;

/**
 * 办理记录Service接口
 *
 * @author ljb
 * @date 2023-11-21
 */
public interface IOaSupervisingRecordService
{
    /**
     * 查询办理记录
     *
     * @param id 办理记录ID
     * @return 办理记录
     */
    public OaSupervisingRecord selectOaSupervisingRecordById(String id);

    /**
     * 查询办理记录列表
     *
     * @param oaSupervisingRecord 办理记录
     * @return 办理记录集合
     */
    public List<OaSupervisingRecord> selectOaSupervisingRecordList(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 新增办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    public int insertOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 修改办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    public int updateOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 批量删除办理记录
     *
     * @param ids 需要删除的办理记录ID
     * @return 结果
     */
    public int deleteOaSupervisingRecordByIds(String[] ids);

    /**
     * 删除办理记录信息
     *
     * @param id 办理记录ID
     * @return 结果
     */
    public int deleteOaSupervisingRecordById(String id);


}
