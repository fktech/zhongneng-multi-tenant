package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaContract;
import com.business.system.domain.vo.BaContractVO;

/**
 * 合同Service接口
 *
 * @author ljb
 * @date 2022-12-07
 */
public interface IBaContractService
{
    /**
     * 查询合同
     *
     * @param id 合同ID
     * @return 合同
     */
    public BaContract selectBaContractById(String id);

    /**
     * 查询合同列表
     *
     * @param baContract 合同
     * @return 合同集合
     */
    public List<BaContract> selectBaContractList(BaContract baContract);

    /**
     * 新增合同
     *
     * @param baContract 合同
     * @return 结果
     */
    public int insertBaContract(BaContract baContract);

    /**
     * 修改合同
     *
     * @param baContract 合同
     * @return 结果
     */
    public int updateBaContract(BaContract baContract);


    /**
     * 签订按钮
     * @param baContract
     * @return
     */
    public int signing(BaContract baContract);

    /**
     * 批量删除合同
     *
     * @param ids 需要删除的合同ID
     * @return 结果
     */
    public int deleteBaContractByIds(String[] ids);

    /**
     * 删除合同信息
     *
     * @param id 合同ID
     * @return 结果
     */
    public int deleteBaContractById(String id);


    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 归档附件
     */
    public BaContractVO getAnnexList(String id);

    /**
     * 合同邮寄
     */
    public int postOff(BaContract baContract);

    /**
     * 合同统计
     */
    public UR contractCount();

    public int bindingStart(BaContract baContract);

    public int association(BaContract baContract);

    public int unbinding(BaContract baContract);

    public List<BaContract> selectBaContractListAll(BaContract baContract);

    /**
     * 统计合同数量
     */
    public int countBaContractList(BaContract baContract);

    /**
     * 查询多租户授权信息合同列表
     * @param baContract
     * @return
     */
    public List<BaContract> selectBaContractAuthorizedList(BaContract baContract);

    /**
     * 授权合同管理
     */
    public int authorizedBaContract(BaContract baContract);
}
