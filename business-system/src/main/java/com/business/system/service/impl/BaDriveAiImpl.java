package com.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.CountBaProjectDTO;
import com.business.system.domain.dto.CountContractStartDTO;
import com.business.system.domain.dto.HomeDTO;
import com.business.system.domain.vo.BaDriveAi;
import com.business.system.domain.vo.BaOngoingContractStartVO;
import com.business.system.domain.vo.DataLayoutStatVO;
import com.business.system.mapper.*;
import com.business.system.service.workflow.IBaDriveAiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 立项管理Service业务层处理
 *
 * @author ljb
 * @date 2022-12-02
 */
@Service
public class BaDriveAiImpl  implements IBaDriveAiService {
    @Autowired
    private  BaDriveAiMapper baDriveAiMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Override
    public List<BaDriveAi> businesstype() {
        return baDriveAiMapper.businesstype();
    }

    @Override
    public UR counts() {
        Map<String, String> map = new HashMap<>();
        //供应商
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("flag",0);
        supplierQueryWrapper.eq("state",AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        map.put("supplier", String.valueOf(baSupplierMapper.selectCount(supplierQueryWrapper)));

        //本月供应商
        map.put("monthSupplier",String.valueOf(baSupplierMapper.thisMonth(SecurityUtils.getCurrComId())));

        //终端企业
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("flag",0);
        enterpriseQueryWrapper.eq("state",AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        map.put("enterprise",String.valueOf(baEnterpriseMapper.selectCount(enterpriseQueryWrapper)));

        //本月终端企业
        map.put("monthEnterprise",String.valueOf(baEnterpriseMapper.thisMonth(SecurityUtils.getCurrComId())));

        //托盘公司
        QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","托盘公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        map.put("company",String.valueOf(baCompanyMapper.selectCount(companyQueryWrapper)));

        //本月托盘公司
        map.put("monthCompany",String.valueOf(baCompanyMapper.thisMonth("托盘公司", SecurityUtils.getCurrComId())));

        //非托盘公司
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.ne("company_type","托盘公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        Integer integer = baCompanyMapper.selectCount(companyQueryWrapper);

        //本月非托盘公司
        Integer otherInteger = baCompanyMapper.otherCompanyThisMonth("托盘公司", SecurityUtils.getCurrComId());

        //集团公司
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.isNotNull("create_time");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            groupQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        Integer integer1 = baCompanyGroupMapper.selectCount(groupQueryWrapper);

        Integer otherInteger1 = baCompanyGroupMapper.thisMonth(SecurityUtils.getCurrComId());
        //其他单位
        map.put("otherCompany",String.valueOf(integer + integer1));

        //本月其他单位
        map.put("monthOtherCompany",String.valueOf(otherInteger + otherInteger1));
        return UR.ok().data("map",map);
    }

    @Override
    public List<DataLayoutStatVO> departmentRanking(Long deptId, String type) {
        //查询所有子公司
        List<DataLayoutStatVO> subDepartmentList = new ArrayList<>();//查询统计
        List<SysDept> sysDepts = null;
        SysDept tempSysDept = null;
        tempSysDept = new SysDept();
        tempSysDept.setParentId(deptId);
        tempSysDept.setDeptName("事业部");
        //租户ID
        tempSysDept.setTenantId(SecurityUtils.getCurrComId());
        sysDepts = sysDeptMapper.selectDepartmentListByParentId(tempSysDept);
        //累计运输量
        BigDecimal checkCoalNum = null;
        //将元变为万元
        BigDecimal bigDecimal = new BigDecimal("10000");
        CountBaProjectDTO countBaProjectDTO = null;
        DataLayoutStatVO dataLayoutStatVO = null;
        if (!CollectionUtils.isEmpty(sysDepts)) {
            for (SysDept sysDept : sysDepts) {
                if (!ObjectUtils.isEmpty(sysDept)) {
                    dataLayoutStatVO = new DataLayoutStatVO();
                    checkCoalNum = new BigDecimal(0);
                    //项目个数
                    countBaProjectDTO = new CountBaProjectDTO();
                    countBaProjectDTO.setDeptId(sysDept.getDeptId());
                    dataLayoutStatVO.setProjectNum(baProjectMapper.countBaProjectList(countBaProjectDTO));
                    BaTransport baTransport = new BaTransport();
                    baTransport.setDeptId(sysDept.getDeptId());
                    baTransport.setTenantId(SecurityUtils.getCurrComId());
                    baTransport.setType("2");
                    BaTransport baTransportDept = baTransportMapper.selectTransportSum(baTransport);
                    //事业部累计运输量
                    if (!ObjectUtils.isEmpty(baTransportDept)) {
                        if (baTransportDept.getCheckCoalNum() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getCheckCoalNum());
                        }
                        if (baTransportDept.getCcjweight() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getCcjweight());
                        }
                        if (baTransportDept.getConfirmedDeliveryWeight() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getConfirmedDeliveryWeight());
                        }
                    }


                    if(checkCoalNum != null) {
                        checkCoalNum = checkCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                    } else {
                        checkCoalNum = new BigDecimal(0);
                    }
                    dataLayoutStatVO.setCheckCoalNum(checkCoalNum);

                    dataLayoutStatVO.setDeptId(String.valueOf(sysDept.getDeptId()));
                    dataLayoutStatVO.setDeptName(sysDept.getDeptName());
                    //营业额
                    BigDecimal decimal = new BigDecimal(0);
                    QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("dept_id",String.valueOf(sysDept.getDeptId()));
                    queryWrapper.eq("flag",0);
                    queryWrapper.eq("state",AdminCodeEnum.CLAIM_STATUS_PASS.getCode());
                    List<BaCollection> collections = baCollectionMapper.selectList(queryWrapper);
                    for (BaCollection collection:collections) {
                        //查询认领数据
                        QueryWrapper<BaClaim> claimQueryWrapper = new QueryWrapper<>();
                        claimQueryWrapper.eq("relation_id",collection.getId());
                        List<BaClaim> baClaimList = baClaimMapper.selectList(claimQueryWrapper);
                        for (BaClaim claim:baClaimList) {
                            decimal = decimal.add(claim.getThisClaim());
                        }
                    }
                    dataLayoutStatVO.setTurnover(decimal);
                    subDepartmentList.add(dataLayoutStatVO);
                }
            }
        }

        //累计运输量
        subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getCheckCoalNum).reversed()).collect(Collectors.toList());;

        return subDepartmentList;
    }

    @Override
    public List<BaOngoingContractStartVO> contractStartList(BaContractStartDTO baContractStartDTO) {
        //供货中
        List<BaOngoingContractStartVO> startList = new ArrayList<>();

        QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
        startQueryWrapper.eq("state", "contractStart:pass");
        startQueryWrapper.eq("flag", 0);
        startQueryWrapper.orderByDesc("create_time");
        List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
        BaOngoingContractStartVO baOngoingContractStartVO = null;
        for (BaContractStart baContractStart : baContractStartList) {
            baOngoingContractStartVO = new BaOngoingContractStartVO();
            baOngoingContractStartVO.setName(baContractStart.getName());
            //事业部
            SysUser sysUser = sysUserMapper.selectUserById(baContractStart.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                SysDept sysDept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                if(!ObjectUtils.isEmpty(sysDept)){
                    baOngoingContractStartVO.setDeptName(sysDept.getDeptName());
                }
            }
            //商品
            /*if (StringUtils.isNotEmpty(baContractStart.getGoodsId())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baContractStart.getGoodsId());
                baContractStart.setGoodsName(baGoods.getName());
                baOngoingContractStartVO.setGoodsName(baGoods.getName());
            }*/
            //立项信息
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            //商品名称
            if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                List<String> goodsIdList = Arrays.asList(baProject.getGoodsId().split(","));
                baGoodsQueryWrapper.in("id", goodsIdList);
                baGoodsQueryWrapper.eq("flag", 0);
                List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                //绑定商品对象
                baProject.setGoodsList(goodsList);
                if (!CollectionUtils.isEmpty(goodsList)) {
                    StringBuilder goodName = new StringBuilder();
                    for (BaGoods baGoods : goodsList) {
                        if (goodName.length() > 0) {
                            goodName.append(",").append(baGoods.getName());
                        } else {
                            goodName.append(baGoods.getName());
                        }
                    }
                    baOngoingContractStartVO.setGoodsName(goodName.toString());
                }
            }
            //启动日期
            baOngoingContractStartVO.setStartCreateTime(baContractStart.getCreateTime());
            //供货中、待结算、待回款、已完结
            baOngoingContractStartVO.setStartType(baContractStart.getStartType());
            //物流信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", baContractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> transports = baTransportMapper.selectList(transportQueryWrapper);
            //已到货
            BigDecimal arrived = new BigDecimal(0);
            for (BaTransport baTransport : transports) {
                //验收
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 2);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    if (baCheck.getCheckCoalNum() != null) {
                        //已到货
                        arrived = arrived.add(baCheck.getCheckCoalNum());
                    }
                }
            }

            baOngoingContractStartVO.setArrived(arrived);
            //认领收款
            BigDecimal applyCollectionAmount = new BigDecimal(0);
            //查询认领金额
            /*QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
            historyQueryWrapper.eq("order_id", baContractStart.getId());
            List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
            for (BaClaimHistory baClaimHistory : baClaimHistories) {
                //查询认领信息
                BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                if(!ObjectUtils.isEmpty(baClaim)) {
                    BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                    //判断认领收款审批通过
                    if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                        if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                            applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                        }
                    }
                }
            }*/
            BaPayment baPayment = new BaPayment();
            baPayment.setTenantId(SecurityUtils.getCurrComId());
            baPayment.setStartId(baContractStart.getId());
            BigDecimal bigDecimal = baPaymentMapper.amountPaid(baPayment);
            if(null != bigDecimal){
                applyCollectionAmount = applyCollectionAmount.add(bigDecimal);
            }
            //认领收款
            baOngoingContractStartVO.setApplyCollectionAmount(applyCollectionAmount);
            startList.add(baOngoingContractStartVO);
        }

        return startList;
    }
}
