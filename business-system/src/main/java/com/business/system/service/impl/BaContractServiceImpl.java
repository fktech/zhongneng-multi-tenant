package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaContractVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.service.workflow.IDepotApprovalService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 合同Service业务层处理
 *
 * @author ljb
 * @date 2022-12-07
 */
@Service
public class BaContractServiceImpl extends ServiceImpl<BaContractMapper, BaContract> implements IBaContractService
{
    private static final Logger log = LoggerFactory.getLogger(BaContractServiceImpl.class);

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private IBaBankService iBaBankService;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private IBaProjectService iBaProjectService;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private IBaContractStartService iBaContractStartService;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    /**
     * 查询合同
     *
     * @param id 合同ID
     * @return 合同
     */
    @Override
    public BaContract selectBaContractById(String id)
    {
        BaContract baContract = baContractMapper.selectBaContractById(id);
        List<BaContractStart> list = new ArrayList<>();
        //合同启动名称
        if(StringUtils.isNotEmpty(baContract.getStartId())){
            String[] split = baContract.getStartId().split(",");
            for (String startId:split) {
                BaContractStart contractStart = iBaContractStartService.selectBaContractStartById(startId);
                //岗位信息
                String name = getName.getName(contractStart.getUserId());
                //发起人
                if (name.equals("") == false) {
                    contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
                } else {
                    contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
                }
                SysUser sysUser = iSysUserService.selectUserById(contractStart.getUserId());
                contractStart.setUser(sysUser);
                //baContract.setStartName(contractStart.getName());
                list.add(contractStart);
            }

            baContract.setBaContractStart(list);
        }
        //常规立项
        List<BaProject> projectList = new ArrayList<>();
        if(StringUtils.isNotEmpty(baContract.getProjectId())){
            String[] split = baContract.getProjectId().split(",");
            for (String projectId:split) {
                BaProject baProject = new BaProject();
                baProject.setProjectId(projectId);
                List<BaProject> baProjects = iBaProjectService.selectBaProjectList(baProject);
                if(baProjects.size() > 0){
                    projectList.add(baProjects.get(0));
                }
            }
            baContract.setBaProjects(projectList);
        }
        //仓储立项
        List<BaProject> storageList = new ArrayList<>();
        if(StringUtils.isNotEmpty(baContract.getStorageId())){
            String[] storageIdList = baContract.getStorageId().split(",");
            for (String storageId:storageIdList) {
                BaProject baProject = new BaProject();
                baProject.setProjectId(storageId);
                List<BaProject> baProjects = iBaProjectService.selectBaProjectList(baProject);
                if(baProjects.size() > 0){
                    storageList.add(baProjects.get(0));
                }

            }
            baContract.setStorageList(storageList);
        }
        //销售订单
       /* List<BaContractStart> orderList = new ArrayList<>();
        if(StringUtils.isNotEmpty(baContract.getOrderId())){
            String[] orderIdList = baContract.getOrderId().split(",");
            for (String orderId:orderIdList) {
                BaContractStart baContractStart = new BaContractStart();
                baContractStart.setOrderId(orderId);
                List<BaContractStart> contractStarts = baContractStartMapper.selectOrderList(baContractStart);
                //岗位信息
                String name = getName.getName(contractStarts.get(0).getUserId());
                //发起人
                if (name.equals("") == false) {
                    contractStarts.get(0).setUserName(sysUserMapper.selectUserById(contractStarts.get(0).getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
                } else {
                    contractStarts.get(0).setUserName(sysUserMapper.selectUserById(contractStarts.get(0).getUserId()).getNickName());
                }
                orderList.add(contractStarts.get(0));
            }
            baContract.setOrders(orderList);
        }*/
        List<BaContractStart> saleOrderList = new ArrayList<>();
        if(StringUtils.isNotEmpty(baContract.getSaleOrder())){
            String[] saleOrderIdList = baContract.getSaleOrder().split(",");
            for (String saleOrderId:saleOrderIdList) {
                BaContractStart baContractStart = new BaContractStart();
                baContractStart.setOrderId(saleOrderId);
                List<BaContractStart> contractStarts = baContractStartMapper.selectOrderList(baContractStart);
                //岗位信息
                String name = getName.getName(contractStarts.get(0).getUserId());
                //发起人
                if (name.equals("") == false) {
                    contractStarts.get(0).setUserName(sysUserMapper.selectUserById(contractStarts.get(0).getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
                } else {
                    contractStarts.get(0).setUserName(sysUserMapper.selectUserById(contractStarts.get(0).getUserId()).getNickName());
                }
                //主体公司
                if(StringUtils.isNotEmpty(contractStarts.get(0).getBelongCompanyId())){
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStarts.get(0).getBelongCompanyId()));
                    if(!ObjectUtils.isEmpty(sysDept)){
                        contractStarts.get(0).setBelongCompanyName(sysDept.getDeptName());
                    }
                }
                saleOrderList.add(contractStarts.get(0));
            }
            baContract.setSaleOrderList(saleOrderList);
        }
        //采购订单
        List<BaProcurementPlan> procureOrderList = new ArrayList<>();
        if(StringUtils.isNotEmpty(baContract.getProcureOrder())){
            String[] procureOrderIdList = baContract.getProcureOrder().split(",");
            for (String procureOrderId:procureOrderIdList) {
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(procureOrderId);
                //岗位信息
                String name = getName.getName(baProcurementPlan.getUserId());
                if (name.equals("") == false) {
                    baProcurementPlan.setUserName(sysUserMapper.selectUserById(baProcurementPlan.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
                } else {
                    baProcurementPlan.setUserName(sysUserMapper.selectUserById(baProcurementPlan.getUserId()).getNickName());
                }
                //主体公司
                if(StringUtils.isNotEmpty(baProcurementPlan.getBelongCompanyId())){
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baProcurementPlan.getBelongCompanyId()));
                    if(!ObjectUtils.isEmpty(sysDept)){
                        baProcurementPlan.setBelongCompanyName(sysDept.getDeptName());
                    }
                }
                //项目名称
                if(StringUtils.isNotEmpty(baProcurementPlan.getProjectId())){
                    BaProject baProject = baProjectMapper.selectBaProjectById(baProcurementPlan.getProjectId());
                    baProcurementPlan.setProjectName(baProject.getName());
                }
                //仓库名称
                if(StringUtils.isNotEmpty(baProcurementPlan.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(baProcurementPlan.getDepotId());
                    baProcurementPlan.setDepotName(baDepot.getName());
                }
                procureOrderList.add(baProcurementPlan);
            }
            baContract.setProcureOrderList(procureOrderList);
        }
        //我方单位名称
        if(StringUtils.isNotEmpty(baContract.getOurUtilsId())){
            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baContract.getOurUtilsId()));
            if(!ObjectUtils.isEmpty(dept)){
                baContract.setOurUtilsName(dept.getDeptName());
            }
        } else {
            baContract.setOurUtilsName(baContract.getOurUtilsName());
        }
        //对方单位名称
        if(StringUtils.isNotEmpty(baContract.getOtherUtilsName())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract.getOtherUtilsName());
            if(StringUtils.isNotNull(enterprise)){
                baContract.setOtherUtilsId(enterprise.getName());
            }
            //公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContract.getOtherUtilsName());
            if(StringUtils.isNotNull(baCompany)){
                baContract.setOtherUtilsId(baCompany.getName());
            }
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract.getOtherUtilsName());
            if(StringUtils.isNotNull(baSupplier)){
                baContract.setOtherUtilsId(baSupplier.getName());
            }
        }
        //供应商
        /*if(StringUtils.isNotEmpty(baContract.getSupplierId())){
           BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract.getSupplierId());
           baContract.setSupplierName(baSupplier.getName());
        }*/
        //用煤单位
        /*if(StringUtils.isNotEmpty(baContract.getEnterpriseId())){
           BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract.getEnterpriseId());
           baContract.setEnterpriseName(baEnterprise.getName());
        }*/
        //项目
        /*if(StringUtils.isNotEmpty(baContract.getProjectId())){
           BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
           baContract.setSettlementProportion(baProject.getSettlementProportion());
           baContract.setProjectName(baProject.getName());
        }*/
        //商品
        /*if(StringUtils.isNotEmpty(baContract.getGoodsId())){
           BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baContract.getGoodsId());
           baContract.setGoodsName(baGoods.getName());
        }*/
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baContract.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baContract.setUser(sysUser);
        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baContract.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }
       /* //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baContract.getId());
        queryWrapper.eq("flag",0);
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        for (BaBank baBank:baBanks) {
            baBank.setAccountName(baBank.getAccount());
            if(StringUtils.isNotEmpty(baBank.getAccount())){
                BaBank baBank1 = baBankMapper.selectBaBankById(baBank.getAccount());
                if(StringUtils.isNotNull(baBank1)){
                    baBank.setAccount(baBank1.getAccount());
                }
            }
            if(baBank.getCompanyType().equals("供应商")){
                //查询供应商名称
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baBank.getCompanyName());
                baBank.setCompany(baSupplier.getName());
            }else if(baBank.getCompanyType().equals("终端企业")){
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baBank.getCompanyName());
                baBank.setCompany(enterprise.getName());
            } else {
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baBank.getCompanyName());
                if(StringUtils.isNotNull(baCompany)){
                    baBank.setCompany(baCompany.getName());
                }
            }
        }
        baContract.setBankList(baBanks);*/

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baContract.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper1.ne("approve_result",AdminCodeEnum.CONTRACT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baContract.setProcessInstanceId(processInstanceIds);
        /*BaContractDTO baContractDTO = new BaContractDTO();

        //优先统计采购合同
        baContractDTO.setPurchaseContractId(id);
        //合同累计发量（吨）
        BigDecimal count = baContractMapper.countContractCumulativeVolume(baContractDTO);
        if(!ObjectUtils.isEmpty(count)){
            baContract.setCumulativeVolume(count);
        } else {
            baContractDTO = new BaContractDTO();
            baContractDTO.setContractId(id);
            count = baContractMapper.countContractCumulativeVolume(baContractDTO);
            baContract.setCumulativeVolume(count);
        }*/
        //        关联补充协议
        QueryWrapper<BaContract> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_contract", baContract.getId());
        queryWrapper.eq("flag", 0);
        queryWrapper.orderByDesc("create_time");
        List<BaContract> baContracts = baContractMapper.selectList(queryWrapper);
        if(!CollectionUtils.isEmpty(baContracts)){
            for(BaContract contract : baContracts){
                //我方单位名称
                if(StringUtils.isNotEmpty(contract.getOurUtilsId())){
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(contract.getOurUtilsId()));
                    if(!ObjectUtils.isEmpty(dept)){
                        contract.setOurUtilsName(dept.getDeptName());
                    }
                } else {
                    contract.setOurUtilsName(contract.getOurUtilsName());
                }

                //对方单位名称
                if(StringUtils.isNotEmpty(contract.getOtherUtilsName())){
                    //终端企业
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(contract.getOtherUtilsName());
                    if(StringUtils.isNotNull(enterprise)){
                        contract.setOtherUtilsId(enterprise.getName());
                    }
                    //公司
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(contract.getOtherUtilsName());
                    if(StringUtils.isNotNull(baCompany)){
                        contract.setOtherUtilsId(baCompany.getName());
                    }
                    //供应商
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(contract.getOtherUtilsName());
                    if(StringUtils.isNotNull(baSupplier)){
                        contract.setOtherUtilsId(baSupplier.getName());
                    }
                }
            }
            baContract.setBaContractList(baContracts);
        }

        BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getRelationContract());
        if(!ObjectUtils.isEmpty(baContract1)){
            baContract.setRelationContractName(baContract1.getName());
        }

        return baContract;
    }

    /**
     * 查询合同列表
     *
     * @param baContract 合同
     * @return 合同
     */
    @Override
//    @DataScope(deptAlias = "c",userAlias = "c")
    public List<BaContract> selectBaContractList(BaContract baContract)
    {
        List<BaContract> baContracts = baContractMapper.selectBaContractList(baContract);
        for (BaContract baContract1:baContracts) {
            //发起人
           SysUser sysUser = sysUserMapper.selectUserById(baContract1.getUserId());
           //岗位信息
            String name = getName.getName(baContract1.getUserId());
            if(name.equals("") == false){
                baContract1.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baContract1.setUserName(sysUser.getNickName());
            }

           //用煤单位
            if(StringUtils.isNotEmpty(baContract1.getEnterpriseId())){
               BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract1.getEnterpriseId());
               baContract1.setEnterpriseName(baEnterprise.getName());
            }

            //我方单位名称
            if(StringUtils.isNotEmpty(baContract1.getOurUtilsId())){
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baContract1.getOurUtilsId()));
                if(!ObjectUtils.isEmpty(dept)){
                    baContract1.setOurUtilsName(dept.getDeptName());
                }
            } else {
                baContract1.setOurUtilsName(baContract1.getOurUtilsName());
            }

            //对方单位名称
            if(StringUtils.isNotEmpty(baContract1.getOtherUtilsName())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(enterprise)){
                    baContract1.setOtherUtilsId(enterprise.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(baCompany)){
                    baContract1.setOtherUtilsId(baCompany.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(baSupplier)){
                    baContract1.setOtherUtilsId(baSupplier.getName());
                }
            }
            //供应商
            /*if(StringUtils.isNotEmpty(baContract1.getSupplierId())){
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract1.getSupplierId());
                baContract1.setSupplierName(baSupplier.getName());
            }*/
            //关联常规项目
            StringBuilder connProjectName = new StringBuilder();
            String[] split = null;
            if(StringUtils.isNotEmpty(baContract1.getProjectId())){
                split = baContract1.getProjectId().split(",");
                for(String projectId : split){
                    BaProject baProject = baProjectMapper.selectBaProjectById(projectId);
                    if(!ObjectUtils.isEmpty(baProject)){
                        baContract1.setSettlementProportion(baProject.getSettlementProportion());
                        baContract1.setProjectName(baProject.getName());
                        if(connProjectName.length() > 0){
                            connProjectName.append(",").append(baProject.getName());
                        } else {
                            connProjectName.append(baProject.getName());
                        }
                    }
                }
            }
            //关联仓储项目
            if(StringUtils.isNotEmpty(baContract1.getStorageId())){
                split = baContract1.getStorageId().split(",");
                for(String storageId : split) {
                    BaProject baProject = baProjectMapper.selectBaProjectById(storageId);
                    if (!ObjectUtils.isEmpty(baProject)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baProject.getName());
                        } else {
                            connProjectName.append(baProject.getName());
                        }
                    }
                }
            }

            if(StringUtils.isNotEmpty(baContract1.getOrderId())){
                split = baContract1.getOrderId().split(",");
                for(String orderId : split) {
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(orderId);
                    if (!ObjectUtils.isEmpty(baContractStart)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baContractStart.getName());
                        } else {
                            connProjectName.append(baContractStart.getName());
                        }
                    }
                }
            }

            if(StringUtils.isNotEmpty(baContract1.getStartId())){
                split = baContract1.getStartId().split(",");
                for(String startId : split) {
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(startId);
                    if (!ObjectUtils.isEmpty(baContractStart)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baContractStart.getName());
                        } else {
                            connProjectName.append(baContractStart.getName());
                        }
                    }
                }
            }
            baContract1.setConnProjectName(connProjectName.toString());
            //履约期
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            baContract1.setPerformancePeriod(sdf.format(baContract1.getStartTime())+" ~ "+sdf.format(baContract1.getEndTime()));
        }
        return baContracts;
    }

    /**
     * 新增合同
     *
     * @param baContract 合同
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertBaContract(BaContract baContract)
    {
        //判断合同编号不能重复
        QueryWrapper<BaContract> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("num",baContract.getNum());
        queryWrapper.eq("flag",0);
        //租户ID
        queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        BaContract baContracts = baContractMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(baContracts)){
            return -1;
        }
        //判断合同名称不能重复
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baContract.getName());
        queryWrapper.eq("flag",0);
        //租户ID
        queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        baContracts = baContractMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(baContracts)){
            return -2;
        }
        baContract.setId(getRedisIncreID.getId());
        baContract.setSigningStatus("待签");
        baContract.setCreateTime(DateUtils.getNowDate());
        baContract.setCreateBy(SecurityUtils.getUsername());
        baContract.setUserId(SecurityUtils.getUserId());
        baContract.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baContract.setTenantId(SecurityUtils.getCurrComId());
        //流程发起状态
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baContract.getWorkState())) {
            baContract.setWorkState("1");
            //默认审批通过
            baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        }

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(),SecurityUtils.getCurrComId());
        baContract.setFlowId(flowId);
        Integer result = baContractMapper.insertBaContract(baContract);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baContract.getWorkState())){
                BaContract contract = baContractMapper.selectBaContractById(baContract.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contract, AdminCodeEnum.CONTRACT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    baContractMapper.deleteBaContractById(baContract.getId());
                    return 0;
                }else {
                    BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
                    baContract1.setState(AdminCodeEnum.CONTRACT_STATUS_VERIFYING.getCode());
                    //全局编号
                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                    queryWrapper.eq("flag",0);
                    queryWrapper.isNotNull("serial_number");
                    queryWrapper.orderByDesc("serial_number");
                    List<BaContract> list = baContractMapper.selectList(queryWrapper);
                    if(list.size() > 0){
                        Integer serialNumber = list.get(0).getSerialNumber();
                        baContract1.setSerialNumber(serialNumber+1);
                        if(serialNumber < 10){
                            baContract1.setGlobalNumber("HT"+"-"+"000"+baContract1.getSerialNumber());
                        }else if(serialNumber >= 10){
                            baContract1.setGlobalNumber("HT"+"-"+"00"+baContract1.getSerialNumber());
                        }else if(serialNumber >= 100){
                            baContract1.setGlobalNumber("HT"+"-"+"0"+baContract1.getSerialNumber());
                        }else if(serialNumber >= 1000){
                            baContract1.setGlobalNumber("HT"+"-"+baContract1.getSerialNumber());
                        }
                    }else {
                        baContract1.setSerialNumber(1);
                        baContract1.setGlobalNumber("HT"+"-"+"000"+baContract1.getSerialNumber());
                    }
                    baContractMapper.updateBaContract(baContract1);
                    //新增开户行
                    if (StringUtils.isNotNull(baContract.getBankList())) {
                        for (BaBank baBank : baContract.getBankList()) {
                            BaBank baBank1 = baBankMapper.selectBaBankById(baBank.getAccount());
                            if (StringUtils.isNotNull(baBank1)) {
                                baBank.setOpenName(baBank1.getOpenName());
                                baBank.setInterBankNumber(baBank1.getInterBankNumber());
                            }
                            baBank.setRelationId(baContract.getId());
                            baBank.setId(getRedisIncreID.getId());
                            iBaBankService.insertBaBank(baBank);
                        }
                    }
                    //编辑合同启动关系
                    if(StringUtils.isNotEmpty(baContract.getContractType())){
                        //上游合同关联合同启动
                        if(baContract.getContractType().equals("6")){
                            if(StringUtils.isNotEmpty(baContract.getStartId())){
                                String[] split = baContract.getStartId().split(",");
                                for (String starId:split) {
                                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(starId);
                                    baContractStart.setUpstream("1");
                                    baContractStartMapper.updateBaContractStart(baContractStart);
                                }
                            }
                        }
                        //下游合同关联合同启动
                        if(baContract.getContractType().equals("7")){
                            if(StringUtils.isNotEmpty(baContract.getStartId())){
                                String[] split = baContract.getStartId().split(",");
                                for (String starId:split) {
                                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(starId);
                                    baContractStart.setDownstream("1");
                                    baContractStartMapper.updateBaContractStart(baContractStart);
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }


    /**
     * 提交合同审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaContract baContract, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baContract.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode());
        //查询入库立项ID
        if(!ObjectUtils.isEmpty(baContract)){
            BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
            if(!ObjectUtils.isEmpty(baProject)){
                map.put("projectId", baProject.getId());
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baContract.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaContract baContract1 = this.selectBaContractById(baContract.getId());
        SysUser sysUser = iSysUserService.selectUserById(baContract1.getUserId());
        baContract1.setUserName(sysUser.getUserName());
        baContract1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baContract1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改合同
     *
     * @param baContract 合同
     * @return 结果
     */
    @Override
    public int updateBaContract(BaContract baContract)
    {
        baContract.setUpdateTime(DateUtils.getNowDate());
        baContract.setUpdateBy(SecurityUtils.getUsername());
        //拿到原有合同
        BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        if(baContract1.getName().equals(baContract.getName()) == false){
            //判断合同名称不能重复
            QueryWrapper<BaContract> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name",baContract.getName());
            queryWrapper.eq("flag",0);
            BaContract baContracts = baContractMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(baContracts)){
                return -2;
            }
        }
        if(baContract1.getNum().equals(baContract.getNum()) == false){
            //判断合同名称不能重复
            QueryWrapper<BaContract> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("num",baContract.getNum());
            queryWrapper.eq("flag",0);
            BaContract baContracts = baContractMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(baContracts)){
                return -1;
            }
        }
        //修改开户行
        /*if(StringUtils.isNotEmpty(baContract.getBankList())){
            baContract.getBankList().forEach(item ->{
                BaBank baBank = baBankMapper.selectBaBankById(item.getId());
                if(item.getAccount().equals(baBank.getAccount())){
                item.setOpenName(baBank.getOpenName());
                item.setInterBankNumber(baBank.getInterBankNumber());
                }
                item.setUpdateTime(DateUtils.getNowDate());
                item.setUpdateBy(SecurityUtils.getUsername());
                baBankMapper.updateBaBank(item);
            });
        }*/
        Integer result = baContractMapper.updateBaContract(baContract);
        if(result > 0 && !baContract1.getState().equals(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode())){
            BaContract contract = baContractMapper.selectBaContractById(baContract.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",contract.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contract, AdminCodeEnum.CONTRACT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
              return 0;
            }else {
                contract.setState(AdminCodeEnum.CONTRACT_STATUS_VERIFYING.getCode());
                contract.setCreateTime(baContract.getUpdateTime());
                baContractMapper.updateBaContract(contract);
            }
        }
        return 1;
    }

    @Override
    public int signing(BaContract baContract) {
        if(StringUtils.isNotEmpty(baContract.getId())){
            BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
            baContract.setStartId(baContract1.getStartId());
        }
        baContract.setContractState("进行中");
        baContract.setSigningStatus("已签");
        Integer result = baContractMapper.updateBaContract(baContract);
        if(result > 0){
            //更新项目阶段
            BaContract contract = baContractMapper.selectBaContractById(baContract.getId());
            if(StringUtils.isNotEmpty(contract.getProjectId())){
                String[] split = contract.getProjectId().split(",");
                for(String projectId : split){
                    if(StringUtils.isNotEmpty(projectId)){
                       BaProject baProject = baProjectMapper.selectBaProjectById(projectId);
                       baProject.setProjectStage(6);
                       baProjectMapper.updateBaProject(baProject);
                    }
                }
            }
        }
        return result;
    }

    /**
     * 批量删除合同
     *
     * @param ids 需要删除的合同ID
     * @return 结果
     */
    @Override
    public int deleteBaContractByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaContract baContract = baContractMapper.selectBaContractById(id);
               baContract.setFlag(1);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
               baContractMapper.updateBaContract(baContract);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除合同信息
     *
     * @param id 合同ID
     * @return 结果
     */
    @Override
    public int deleteBaContractById(String id)
    {
        return baContractMapper.deleteBaContractById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询合同信息
          BaContract baContract = baContractMapper.selectBaContractById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baContract.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CONTRACT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baContract.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baContract.setState(AdminCodeEnum.CONTRACT_STATUS_WITHDRAW.getCode());
                //释放合同启动关系
                if(StringUtils.isNotEmpty(baContract.getContractType())){
                    //上游合同关联合同启动
                    if(baContract.getContractType().equals("6")){
                        if(StringUtils.isNotEmpty(baContract.getStartId())){
                            String[] split = baContract.getStartId().split(",");
                            for (String starId:split) {
                                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(starId);
                                baContractStart.setUpstream("0");
                                baContractStartMapper.updateBaContractStart(baContractStart);
                            }
                        }
                    }
                    //下游合同关联合同启动
                    if(baContract.getContractType().equals("7")){
                        if(StringUtils.isNotEmpty(baContract.getStartId())){
                            String[] split = baContract.getStartId().split(",");
                            for (String starId:split) {
                                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(starId);
                                baContractStart.setDownstream("0");
                                baContractStartMapper.updateBaContractStart(baContractStart);
                            }
                        }
                    }
                }
                baContractMapper.updateBaContract(baContract);
                String userId = String.valueOf(baContract.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baContract, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public BaContractVO getAnnexList(String id) {
        //查询合同
        BaContract baContract = baContractMapper.selectBaContractById(id);
        BaContractVO baContractVO = beanUtil.beanConvert(baContract, BaContractVO.class);
        //查询项目信息
        BaProject baProject = baProjectMapper.selectBaProjectById(baContractVO.getProjectId());
        baContractVO.setBaProject(baProject);

        //订单附件
        QueryWrapper<BaOrder> queryWrapperOrder = new QueryWrapper<>();
        //判断合同类型,3 销售合同 4 采购合同
        if("3".equals(baContract.getContractType())){
            queryWrapperOrder.eq("company_id", baContract.getId());
        } else if("4".equals(baContract.getContractType())){
            queryWrapperOrder.eq("purchase_company_id", baContract.getId());
        }
        queryWrapperOrder.eq("flag", 0);
        List<BaOrder> baOrders = baOrderMapper.selectList(queryWrapperOrder);
        if(!CollectionUtils.isEmpty(baOrders)){
            baOrders.stream().forEach(item -> {
                //发运附件
                QueryWrapper<BaTransport> queryWrapperBaTransport = new QueryWrapper<>();
                queryWrapperBaTransport.eq("order_id", item.getId());
                queryWrapperBaTransport.eq("flag", 0);
                BaTransport baTransport = baTransportMapper.selectOne(queryWrapperBaTransport);
                item.setBaTransport(baTransport);
                //到货附件
                /*if(StringUtils.isNotNull(baTransport)) {
                    QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("relation_id", baTransport.getId());
                    queryWrapper.eq("type", "1");
                    BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
                    item.getBaTransport().setBaCheckSend(baCheck);

                    QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("relation_id", baTransport.getId());
                    queryWrapper1.eq("type", "2");
                    baCheck = baCheckMapper.selectOne(queryWrapper1);
                    item.getBaTransport().setBaCheckReceive(baCheck);
                }*/
            });
            baContractVO.setBaOrderList(baOrders);
        }

        //结算单附件
        QueryWrapper<BaSettlement> queryWrapperBaSettlement = new QueryWrapper<>();
        queryWrapperBaSettlement.eq("flag", 0);
        queryWrapperBaSettlement.eq("contract_id", id);
        List<BaSettlement> baSettlements = baSettlementMapper.selectList(queryWrapperBaSettlement);
        baContractVO.setBaSettlementList(baSettlements);

        //付款凭证
        if("3".equals(baContract.getContractType())){
            QueryWrapper<BaCollection> queryWrapperBaCollection = new QueryWrapper<>();
            queryWrapperBaCollection.eq("flag", 0);
            queryWrapperBaCollection.eq("contract_id", id);
            List<BaCollection> baCollections = baCollectionMapper.selectList(queryWrapperBaCollection);
            baContractVO.setBaCollections(baCollections);
        } else if("4".equals(baContract.getContractType())){
            QueryWrapper<BaPayment> queryWrapperBaPayment = new QueryWrapper<>();
            queryWrapperBaPayment.eq("flag", 0);
            queryWrapperBaPayment.eq("contract_id", id);
            List<BaPayment> baPayments = baPaymentMapper.selectList(queryWrapperBaPayment);
            baContractVO.setBaPayments(baPayments);
        }

        //发票附件
        QueryWrapper<BaInvoice> queryWrapperBaInvoice = new QueryWrapper<>();
        queryWrapperBaInvoice.eq("flag", 0);
        queryWrapperBaInvoice.eq("contract_id", id);
        List<BaInvoice> baInvoices = baInvoiceMapper.selectList(queryWrapperBaInvoice);
        baContractVO.setBaInvoices(baInvoices);
        return baContractVO;
    }

    @Override
    public int postOff(BaContract baContract) {
       /* BaContract contract = baContractMapper.selectBaContractById(baContract.getId());
        BeanUtils.copyBeanProp(contract,baContract);*/
        int i = baContractMapper.updateBaContract(baContract);
        return i;
    }

    @Override
    public UR contractCount() {
        Map<String,String> map = new HashMap<>();

        //待签合同
        QueryWrapper<BaContract> contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.eq("flag",0);
        contractQueryWrapper.eq("signing_status","待签");
        //contractQueryWrapper.eq("state",AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            contractQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaContract> baContracts = baContractMapper.selectList(contractQueryWrapper);
        map.put("未签订合同",String.valueOf(baContracts.size()));
        //已签订合同
        contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.eq("flag",0);
        contractQueryWrapper.eq("signing_status","已签");
        contractQueryWrapper.eq("state",AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            contractQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaContract> baContracts1 = baContractMapper.selectList(contractQueryWrapper);
        map.put("已签订合同",String.valueOf(baContracts1.size()));

        return UR.ok().data("map",map);
    }

    @Override
    public int bindingStart(BaContract baContract) {
        int result = 0;
        //绑定合同启动
        if(StringUtils.isNotEmpty(baContract.getStartId())){
            BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
            baContract1.setStartId(baContract.getStartId());
            result = baContractMapper.updateBaContract(baContract);
        }else {
            //解绑合同启动
            BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
            if(StringUtils.isNotEmpty(baContract1.getStartId())){
                baContract1.setStartId(null);
                result = baContractMapper.updateBaContract(baContract1);
            }
            return result;
        }

        return result;
    }

    @Override
    public int association(BaContract baContract) {
        //原合同启动关系解除
        BaContract contract = baContractMapper.selectBaContractById(baContract.getId());
        if(StringUtils.isNotEmpty(contract.getStartId())){
            String[] split = contract.getStartId().split(",");
            for (String starId:split) {
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(starId);
                if (contract.getContractType().equals("6")) {
                    contractStart.setUpstream("0");
                } else if (contract.getContractType().equals("7")) {
                    contractStart.setDownstream("0");
                }
                baContractStartMapper.updateBaContractStart(contractStart);
            }
        }
        //修改订单合同ID
        if(StringUtils.isNotEmpty(baContract.getOrderId())){
            String[] orderIdSplit = baContract.getOrderId().split(",");
            if(orderIdSplit.length > 0){
                for(String orderId : orderIdSplit){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(orderId);
                    if(!ObjectUtils.isEmpty(baContractStart)){
                        baContractStart.setContractId(baContract.getId());
                        baContractStartMapper.updateBaContractStart(baContractStart);
                    }
                }
            }
        }
        //修改合同启动占用状态
        if(StringUtils.isNotEmpty(baContract.getStartId())){
            String[] split = baContract.getStartId().split(",");
            for (String starId:split) {
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(starId);
                if(StringUtils.isNotNull(baContractStart)) {
                    if (contract.getContractType().equals("6")) {
                        baContractStart.setUpstream(baContractStart.getId());
                    } else if (contract.getContractType().equals("7")) {
                        baContractStart.setDownstream(baContract.getId());
                    }
                    baContractStartMapper.updateBaContractStart(baContractStart);
                }
            }
        }
        if(!ObjectUtils.isEmpty(baContract)){
            BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
            baGlobalNumber.setStartId(baContract.getStartId());
            baGlobalNumber.setCode(baContract.getGlobalNumber());
            List<BaGlobalNumber> baGlobalNumbers = baGlobalNumberMapper.selectBaGlobalNumberList(baGlobalNumber);
            if(CollectionUtils.isEmpty(baGlobalNumbers)){
                //全局编号关系表新增数据
                baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baContract.getId());
                baGlobalNumber.setCode(baContract.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("3");
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumber.setStartId(baContract.getStartId());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }
        }
        return baContractMapper.updateBaContract(baContract);
    }

    @Override
    public int unbinding(BaContract baContract) {
        BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        if(StringUtils.isNotNull(baContract1)){
            if(StringUtils.isNotEmpty(baContract1.getStartId())){
                String newStartId = "";
                String[] split = baContract1.getStartId().split(",");
                for (String startId:split) {
                    //解除合同启动
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(startId);
                    if(baContract1.getContractType().equals("6")){
                        contractStart.setUpstream("0");
                    }else if(baContract1.getContractType().equals("7")){
                        contractStart.setDownstream("0");
                    }
                    baContractStartMapper.updateBaContractStart(contractStart);
                    //合同启动ID拼接
                    if(!startId.equals(baContract.getStartId())){
                         newStartId = startId + "," + newStartId;
                     }
                }
                if(StringUtils.isNotEmpty(newStartId) && !"".equals(newStartId)){
                    baContract1.setStartId(newStartId.substring(0, newStartId.length() - 1));
                }else {
                    baContract1.setStartId(newStartId);
                }
            }
            if(StringUtils.isNotEmpty(baContract.getStorageId())){
                String newStorageId = "";
                String[] split = baContract.getStorageId().split(",");
                for (String storageId:split) {
                    if(!storageId.equals(baContract.getStorageId())){
                        newStorageId = storageId + "," + newStorageId;
                    }
                }
                if(StringUtils.isNotEmpty(newStorageId) && !"".equals(newStorageId)){
                    baContract1.setStorageId(newStorageId.substring(0, newStorageId.length() - 1));
                }else {
                    baContract1.setStorageId(newStorageId);
                }
            }
              baContractMapper.updateBaContract(baContract1);

            //全局联查相关删除
            QueryWrapper<BaGlobalNumber> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baContract.getId());
            List<BaGlobalNumber> baGlobalNumbers = baGlobalNumberMapper.selectList(queryWrapper);
            if(baGlobalNumbers.size() > 0){
                for (BaGlobalNumber globalNumber:baGlobalNumbers) {
                        baGlobalNumberMapper.deleteBaGlobalNumberById(globalNumber.getId());
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public List<BaContract> selectBaContractListAll(BaContract baContract) {
        return baContractMapper.selectBaContractList(baContract);
    }

    @Override
    public int countBaContractList(BaContract baContract) {
        return baContractMapper.countBaContractList(baContract);
    }

    /**
     * 查询多租户授权信息合同列表
     * @param baContract
     * @return
     */
    @Override
    public List<BaContract> selectBaContractAuthorizedList(BaContract baContract) {
        List<BaContract> baContracts = baContractMapper.selectBaContractAuthorizedList(baContract);
        for (BaContract baContract1:baContracts) {
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(baContract1.getUserId());
            //岗位信息
            String name = getName.getName(baContract1.getUserId());
            if(name.equals("") == false){
                baContract1.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baContract1.setUserName(sysUser.getNickName());
            }

            //用煤单位
            if(StringUtils.isNotEmpty(baContract1.getEnterpriseId())){
                BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract1.getEnterpriseId());
                baContract1.setEnterpriseName(baEnterprise.getName());
            }

            //我方单位名称
            if(StringUtils.isNotEmpty(baContract1.getOurUtilsId())){
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baContract1.getOurUtilsId()));
                if(!ObjectUtils.isEmpty(dept)){
                    baContract1.setOurUtilsName(dept.getDeptName());
                }
            } else {
                baContract1.setOurUtilsName(baContract1.getOurUtilsName());
            }

            //对方单位名称
            if(StringUtils.isNotEmpty(baContract1.getOtherUtilsName())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(enterprise)){
                    baContract1.setOtherUtilsId(enterprise.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(baCompany)){
                    baContract1.setOtherUtilsId(baCompany.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract1.getOtherUtilsName());
                if(StringUtils.isNotNull(baSupplier)){
                    baContract1.setOtherUtilsId(baSupplier.getName());
                }
            }
            //供应商
            /*if(StringUtils.isNotEmpty(baContract1.getSupplierId())){
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract1.getSupplierId());
                baContract1.setSupplierName(baSupplier.getName());
            }*/
            //关联常规项目
            StringBuilder connProjectName = new StringBuilder();
            String[] split = null;
            if(StringUtils.isNotEmpty(baContract1.getProjectId())){
                split = baContract1.getProjectId().split(",");
                for(String projectId : split){
                    BaProject baProject = baProjectMapper.selectBaProjectById(projectId);
                    if(!ObjectUtils.isEmpty(baProject)){
                        baContract1.setSettlementProportion(baProject.getSettlementProportion());
                        baContract1.setProjectName(baProject.getName());
                        if(connProjectName.length() > 0){
                            connProjectName.append(",").append(baProject.getName());
                        } else {
                            connProjectName.append(baProject.getName());
                        }
                    }
                }
            }
            //关联仓储项目
            if(StringUtils.isNotEmpty(baContract1.getStorageId())){
                split = baContract1.getStorageId().split(",");
                for(String storageId : split) {
                    BaProject baProject = baProjectMapper.selectBaProjectById(storageId);
                    if (!ObjectUtils.isEmpty(baProject)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baProject.getName());
                        } else {
                            connProjectName.append(baProject.getName());
                        }
                    }
                }
            }

            if(StringUtils.isNotEmpty(baContract1.getOrderId())){
                split = baContract1.getOrderId().split(",");
                for(String orderId : split) {
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(orderId);
                    if (!ObjectUtils.isEmpty(baContractStart)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baContractStart.getName());
                        } else {
                            connProjectName.append(baContractStart.getName());
                        }
                    }
                }
            }

            if(StringUtils.isNotEmpty(baContract1.getStartId())){
                split = baContract1.getStartId().split(",");
                for(String startId : split) {
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(startId);
                    if (!ObjectUtils.isEmpty(baContractStart)) {
                        if (connProjectName.length() > 0) {
                            connProjectName.append(",").append(baContractStart.getName());
                        } else {
                            connProjectName.append(baContractStart.getName());
                        }
                    }
                }
            }
            baContract1.setConnProjectName(connProjectName.toString());
        }
        return baContracts;
    }

    @Override
    public int authorizedBaContract(BaContract baContract) {
        return baContractMapper.updateBaContract(baContract);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaContract baContract, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baContract.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"合同审批提醒",msgContent,baMessage.getType(),baContract.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
