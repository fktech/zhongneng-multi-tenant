package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaProjectBeforehandService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 预立项Service业务层处理
 *
 * @author ljb
 * @date 2023-09-20
 */
@Service
public class BaProjectBeforehandServiceImpl extends ServiceImpl<BaProjectBeforehandMapper, BaProjectBeforehand> implements IBaProjectBeforehandService
{
    private static final Logger log = LoggerFactory.getLogger(BaProjectBeforehandServiceImpl.class);

    @Autowired
    private BaProjectBeforehandMapper baProjectBeforehandMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaProgressInformationMapper baProgressInformationMapper;

    /**
     * 查询预立项
     *
     * @param id 预立项ID
     * @return 预立项
     */
    @Override
    public BaProjectBeforehand selectBaProjectBeforehandById(String id)
    {
        BaProjectBeforehand baProjectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(id);
        //判断是否为待定状态
        if(StringUtils.isNotEmpty(baProjectBeforehand.getPendingStatus())){
           if(baProjectBeforehand.getPendingStatus().equals("1")){
               QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
               relatedQueryWrapper.eq("business_id",baProjectBeforehand.getId());
               relatedQueryWrapper.eq("flag",0);
               //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
               relatedQueryWrapper.orderByDesc("create_time");
               List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
               String businessData = baProcessInstanceRelateds.get(0).getBusinessData();
               //审批JSON转对象
               BaProjectBeforehand baProjectBeforehand1 = JSONObject.toJavaObject(JSONObject.parseObject(businessData), BaProjectBeforehand.class);
               baProjectBeforehand1.setState(baProjectBeforehand.getState());
               baProjectBeforehand1.setPendingStatus("1");
               //收费标准
               if(StringUtils.isNotEmpty(baProjectBeforehand.getStandardsId())){
                   BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baProjectBeforehand.getStandardsId());
                   baProjectBeforehand1.setStandards(baChargingStandards);
               }
               return baProjectBeforehand1;
           }
        }
        //收费标准
        if(StringUtils.isNotEmpty(baProjectBeforehand.getStandardsId())){
            BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baProjectBeforehand.getStandardsId());
            baProjectBeforehand.setStandards(baChargingStandards);
        }
        //收费调整
        if(StringUtils.isNotEmpty(baProjectBeforehand.getAdjustId())){
            BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baProjectBeforehand.getAdjustId());
            baProjectBeforehand.setAdjusts(baChargingStandards);
        }
        //品名
        if(StringUtils.isNotEmpty(baProjectBeforehand.getGoodsId())){
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProjectBeforehand.getGoodsId());
            baProjectBeforehand.setGoodsName(baGoods.getName());
        }
        //终端企业
        if(StringUtils.isNotEmpty(baProjectBeforehand.getEnterpriseId())){
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baProjectBeforehand.getEnterpriseId());
            if(StringUtils.isNotNull(enterprise)){
                baProjectBeforehand.setEnterpriseName(enterprise.getName());
            }else {
                baProjectBeforehand.setEnterpriseName(baProjectBeforehand.getEnterpriseId());
            }
        }
        //供应商(供应商、装卸服务公司)
        if(StringUtils.isNotEmpty(baProjectBeforehand.getSupplierId())){
            DropDownBox dropDownBox = new DropDownBox();
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProjectBeforehand.getSupplierId());
            if(StringUtils.isNotNull(baSupplier)){
                baProjectBeforehand.setSupplierName(baSupplier.getName());
                dropDownBox.setName(baSupplier.getName());
                dropDownBox.setId(baSupplier.getId());
                dropDownBox.setType("1");
            }
            //装卸服务公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baProjectBeforehand.getSupplierId());
            if(StringUtils.isNotNull(baCompany)){
                dropDownBox.setId(baCompany.getId());
                dropDownBox.setName(baCompany.getName());
                dropDownBox.setType("2");
            }
            baProjectBeforehand.setDropDownBox(dropDownBox);
        }
        //托盘公司
        if(StringUtils.isNotEmpty(baProjectBeforehand.getPalletCompanyId())){
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baProjectBeforehand.getPalletCompanyId());
            baProjectBeforehand.setPalletCompanyName(baCompany.getName());
        }
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baProjectBeforehand.getUserId());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        //sysUser.setUserName(sysUser.getNickName());
        baProjectBeforehand.setUser(sysUser);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baProjectBeforehand.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baProjectBeforehand.setProcessInstanceId(processInstanceIds);
        //进度信息
        QueryWrapper<BaProgressInformation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("beforehand_id",baProjectBeforehand.getId());
        List<BaProgressInformation> baProgressInformations = baProgressInformationMapper.selectList(queryWrapper);
        baProjectBeforehand.setBaProgressInformationList(baProgressInformations);

        return baProjectBeforehand;
    }

    /**
     * 查询预立项列表
     *
     * @param baProjectBeforehand 预立项
     * @return 预立项
     */
    @Override
    @DataScope(deptAlias = "bpb", userAlias = "bpb")
    public List<BaProjectBeforehand> selectBaProjectBeforehandList(BaProjectBeforehand baProjectBeforehand)
    {
        List<BaProjectBeforehand> baProjectBeforehands = baProjectBeforehandMapper.selectBaProjectBeforehand(baProjectBeforehand);
        for (BaProjectBeforehand projectBeforehand:baProjectBeforehands) {
            //岗位信息
            String name = getName.getName(projectBeforehand.getUserId());
            //发起人
            if(name.equals("") == false){
                projectBeforehand.setUserName(sysUserMapper.selectUserById(projectBeforehand.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                projectBeforehand.setUserName(sysUserMapper.selectUserById(projectBeforehand.getUserId()).getNickName());
            }
            //事业部
            if(projectBeforehand.getDivision() != null){
                SysDept dept = sysDeptMapper.selectDeptById(projectBeforehand.getDivision());
                projectBeforehand.setDeptName(dept.getDeptName());
            }
        }
        return baProjectBeforehands;
    }

    /**
     * 新增预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    @Override
    public int insertBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand)
    {
        baProjectBeforehand.setId(getRedisIncreID.getId());
        baProjectBeforehand.setCreateTime(DateUtils.getNowDate());
        baProjectBeforehand.setUserId(SecurityUtils.getUserId());
        baProjectBeforehand.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baProjectBeforehand.setTenantId(SecurityUtils.getCurrComId());
        baProjectBeforehand.setBeforehandState("1");
        //收费标准
        if(StringUtils.isNotNull(baProjectBeforehand.getStandards())){
            if(StringUtils.isNotEmpty(baProjectBeforehand.getStandards().getId())){
                baChargingStandardsMapper.updateBaChargingStandards(baProjectBeforehand.getStandards());
            }else {
                BaChargingStandards standards = baProjectBeforehand.getStandards();
                standards.setId(getRedisIncreID.getId());
                standards.setCreateTime(DateUtils.getNowDate());
                standards.setCreateBy(SecurityUtils.getUsername());
                baChargingStandardsMapper.insertBaChargingStandards(standards);
                baProjectBeforehand.setStandardsId(standards.getId());
            }
        }
        //流程实例ID
        /*String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode(),SecurityUtils.getCurrComId());
        baProjectBeforehand.setFlowId(flowId);*/
        int result = baProjectBeforehandMapper.insertBaProjectBeforehand(baProjectBeforehand);
       /* if(result > 0){
            BaProjectBeforehand projectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(baProjectBeforehand.getId());
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(projectBeforehand, AdminCodeEnum.PROJECTBEFOREHAND_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baProjectBeforehandMapper.deleteBaProjectBeforehandById(baProjectBeforehand.getId());
                return -1;
            }else {
                projectBeforehand.setState(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_VERIFYING.getCode());
                baProjectBeforehandMapper.updateBaProjectBeforehand(projectBeforehand);
            }
        }*/
        return result;
    }

    /**
     * 提交合同启动审核
     */
  /*  public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaProjectBeforehand baProjectBeforehand, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baProjectBeforehand.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baProjectBeforehand.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaProjectBeforehand baProjectBeforehand1 = this.selectBaProjectBeforehandById(baProjectBeforehand.getId());
        SysUser sysUser = iSysUserService.selectUserById(baProjectBeforehand1.getUserId());
        baProjectBeforehand1.setUserName(sysUser.getUserName());
        baProjectBeforehand1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baProjectBeforehand1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }*/

    /**
     * 启动流程实例
     */
    /*public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.PROJECTBEFOREHAND_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }*/

    /**
     * 修改预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    @Override
    public int updateBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand)
    {
        //原数据
        //BaProjectBeforehand baProjectBeforehand1 = baProjectBeforehandMapper.selectBaProjectBeforehandById(baProjectBeforehand.getId());
        baProjectBeforehand.setUpdateTime(DateUtils.getNowDate());
        if("2".equals(baProjectBeforehand.getBeforehandState())){
            baProjectBeforehand.setProjectTime(DateUtils.getTime());
        }
        //收费标准
        if(StringUtils.isNotNull(baProjectBeforehand.getStandards())){
            if(StringUtils.isNotEmpty(baProjectBeforehand.getStandards().getId())){
                baChargingStandardsMapper.updateBaChargingStandards(baProjectBeforehand.getStandards());
            }else {
                BaChargingStandards standards = baProjectBeforehand.getStandards();
                standards.setId(getRedisIncreID.getId());
                standards.setCreateTime(DateUtils.getNowDate());
                standards.setCreateBy(SecurityUtils.getUsername());
                baChargingStandardsMapper.insertBaChargingStandards(standards);
                baProjectBeforehand.setStandardsId(standards.getId());
            }
        }
        int result = baProjectBeforehandMapper.updateBaProjectBeforehand(baProjectBeforehand);
        /*if(result > 0){
            BaProjectBeforehand projectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(baProjectBeforehand.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",projectBeforehand.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(projectBeforehand, AdminCodeEnum.PROJECTBEFOREHAND_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                //数据回滚
                baProjectBeforehandMapper.updateBaProjectBeforehand(baProjectBeforehand1);
                return -1;
            }else {
                projectBeforehand.setState(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_VERIFYING.getCode());
                baProjectBeforehandMapper.updateBaProjectBeforehand(projectBeforehand);
            }
        }*/
        return result;
    }

    @Override
    public int updateProjectBeforehand(BaProjectBeforehand baProjectBeforehand) {
        int result = 0;
        if(StringUtils.isNotEmpty(baProjectBeforehand.getId())){
            BaProjectBeforehand projectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(baProjectBeforehand.getId());
            projectBeforehand.setPendingStatus("1");
            result = baProjectBeforehandMapper.updateBaProjectBeforehand(projectBeforehand);
        }else {
            return -1;
        }
        return result;
    }

    /**
     * 批量删除预立项
     *
     * @param ids 需要删除的预立项ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectBeforehandByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaProjectBeforehand baProjectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(id);
                baProjectBeforehand.setFlag(1L);
                baProjectBeforehandMapper.updateBaProjectBeforehand(baProjectBeforehand);
                //查询流程与实例关系表
                /*QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }*/
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除预立项信息
     *
     * @param id 预立项ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectBeforehandById(String id)
    {
        return baProjectBeforehandMapper.deleteBaProjectBeforehandById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaProjectBeforehand baProjectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baProjectBeforehand.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.PROJECTBEFOREHAND_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baProjectBeforehand.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baProjectBeforehand.setState(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_WITHDRAW.getCode());
                baProjectBeforehandMapper.updateBaProjectBeforehand(baProjectBeforehand);
                String userId = String.valueOf(baProjectBeforehand.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baProjectBeforehand, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public String projectSerial() {
        QueryWrapper<BaProjectBeforehand> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq("project_type","4");
        queryWrapper.eq("flag",0);
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        queryWrapper.orderByDesc("create_time");
        List<BaProjectBeforehand> baProjectBeforehands = baProjectBeforehandMapper.selectList(queryWrapper);
        //序号
        String num = "";
        if(baProjectBeforehands.size() > 0){
            //判断立项序号是否为空或
            if(StringUtils.isNotEmpty(baProjectBeforehands.get(0).getBeforehandNum())){
                int i = baProjectBeforehands.get(0).getBeforehandNum().lastIndexOf("-");
                int number = Integer.parseInt(baProjectBeforehands.get(0).getBeforehandNum().substring(i+1));
                int serial = number + 1;
                if(String.valueOf(serial).length() == 1){
                    num = "00"+ serial;
                }else if(String.valueOf(serial).length() == 2){
                    num = "0"+ serial;
                }else if(String.valueOf(serial).length() == 3){
                    num = ""+ serial;
                }else if(String.valueOf(serial).length() == 4){
                    num =  String.valueOf(serial);
                }
            }
        }else {
            return "001";
        }
        return num;
    }

    @Override
    public int updateProcessBusinessData(BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO) {
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        baProcessInstanceRelated.setProcessInstanceId(baProjectInstanceRelatedEditDTO.getProcessInstanceId());
        baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(baProjectInstanceRelatedEditDTO.getBaProjectBeforehand()));
        try {
            baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public UR appHomeProjectBeforehandStat(BaProjectBeforehand baProjectBeforehand) {
        //审批中
        List<BaProjectBeforehand> approveingList = new ArrayList<>();
        //已通过
        List<BaProjectBeforehand> passList = new ArrayList<>();
        //已拒绝
        List<BaProjectBeforehand> rejectList = new ArrayList<>();
        //已撤回
        List<BaProjectBeforehand> withdrawList = new ArrayList<>();
        //待定中
        List<BaProjectBeforehand> noe = new ArrayList<>();
        List<BaProjectBeforehand> baProjectBeforehandList = baProjectBeforehandMapper.selectBaProjectBeforehand(baProjectBeforehand);
        if(!CollectionUtils.isEmpty(baProjectBeforehandList)){
            for(BaProjectBeforehand baProjectBeforehand1 : baProjectBeforehandList){
                //审批中
                if(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_VERIFYING.getCode().equals(baProjectBeforehand1.getState())){
                    approveingList.add(baProjectBeforehand1);
                } else if(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_PASS.getCode().equals(baProjectBeforehand1.getState())){
                    passList.add(baProjectBeforehand1);
                } else if(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_REJECT.getCode().equals(baProjectBeforehand1.getState())){
                    rejectList.add(baProjectBeforehand1);
                } else if(AdminCodeEnum.PROJECTBEFOREHAND_STATUS_WITHDRAW.getCode().equals(baProjectBeforehand1.getState())){
                    withdrawList.add(baProjectBeforehand1);
                }
                if("1".equals(baProjectBeforehand1.getPendingStatus())){
                    noe.add(baProjectBeforehand1);
                }
            }
        }
        return UR.ok().data("verifying",approveingList).data("pass",passList).data("reject",rejectList).data("withdraw",withdrawList).data("noe",noe);
    }

    /**
     * 新增进度信息
     *
     * @param baProgressInformation 进度信息
     * @return 结果
     */
    @Override
    public int insertBaProgressInformation(BaProgressInformation baProgressInformation)
    {
        baProgressInformation.setId(getRedisIncreID.getId());
        baProgressInformation.setCreateTime(DateUtils.getNowDate());
        return baProgressInformationMapper.insertBaProgressInformation(baProgressInformation);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaProjectBeforehand baProjectBeforehand, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baProjectBeforehand.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

}
