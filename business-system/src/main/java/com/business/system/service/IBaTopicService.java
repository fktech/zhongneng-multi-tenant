package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaTopic;

/**
 * 问答Service接口
 *
 * @author single
 * @date 2024-04-07
 */
public interface IBaTopicService
{
    /**
     * 查询问答
     *
     * @param id 问答ID
     * @return 问答
     */
    public BaTopic selectBaTopicById(String id);

    /**
     * 查询问答列表
     *
     * @param baTopic 问答
     * @return 问答集合
     */
    public List<BaTopic> selectBaTopicList(BaTopic baTopic);

    /**
     * 新增问答
     *
     * @param baTopic 问答
     * @return 结果
     */
    public int insertBaTopic(BaTopic baTopic);

    /**
     * 修改问答
     *
     * @param baTopic 问答
     * @return 结果
     */
    public int updateBaTopic(BaTopic baTopic);

    /**
     * 批量删除问答
     *
     * @param ids 需要删除的问答ID
     * @return 结果
     */
    public int deleteBaTopicByIds(String[] ids);

    /**
     * 删除问答信息
     *
     * @param id 问答ID
     * @return 结果
     */
    public int deleteBaTopicById(String id);


}
