package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 报销申请Service业务层处理
 *
 * @author single
 * @date 2023-12-11
 */
@Service
public class OaClaimServiceImpl extends ServiceImpl<OaClaimMapper, OaClaim> implements IOaClaimService
{

    private static final Logger log = LoggerFactory.getLogger(OaClaimServiceImpl.class);

    @Autowired
    private OaClaimMapper oaClaimMapper;

    @Autowired
    private OaClaimDetailMapper oaClaimDetailMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private OaClaimRelationMapper oaClaimRelationMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 查询报销申请
     *
     * @param id 报销申请ID
     * @return 报销申请
     */
    @Override
    public OaClaim selectOaClaimById(String id)
    {
        OaClaim oaClaim = oaClaimMapper.selectOaClaimById(id);
        if(!ObjectUtils.isEmpty(oaClaim)){
            List<String> processInstanceIds = new ArrayList<>();
            //流程实例ID
            QueryWrapper<BaProcessInstanceRelated> oaClaimQueryWrapper = new QueryWrapper<>();
            oaClaimQueryWrapper.eq("business_id", oaClaim.getId());
            oaClaimQueryWrapper.eq("flag",0);
            //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
            oaClaimQueryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(oaClaimQueryWrapper);
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            oaClaim.setProcessInstanceId(processInstanceIds);
            //报销明细
            QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
            claimDetailQueryWrapper.eq("claim_id", oaClaim.getId());
            List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
            oaClaim.setOaClaimDetailList(oaClaimDetails);
            //申请人名称
            if(!ObjectUtils.isEmpty(oaClaim.getUserId())){
                SysUser sysUser = sysUserMapper.selectUserById(oaClaim.getUserId());
                if(!ObjectUtils.isEmpty(sysUser)){
                    oaClaim.setUserName(sysUser.getNickName());
                }
            }
            //申请人部门
            if(!ObjectUtils.isEmpty(oaClaim.getDeptId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(oaClaim.getDeptId());
                if(!ObjectUtils.isEmpty(sysDept)){
                    oaClaim.setDeptName(sysDept.getDeptName());
                }
            }
            //项目名称
            if(!ObjectUtils.isEmpty(oaClaim.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(oaClaim.getProjectId());
                if(!ObjectUtils.isEmpty(baProject)){
                    oaClaim.setProjectName(baProject.getName());
                }
            }
        }
        return oaClaim;
    }

    /**
     * 查询报销申请列表
     *
     * @param oaClaim 报销申请
     * @return 报销申请
     */
    @Override
    public List<OaClaim> selectOaClaimList(OaClaim oaClaim)
    {
        List<OaClaim> oaClaims = oaClaimMapper.selectOaClaimList(oaClaim);
        //BigDecimal claimAmounts = new BigDecimal(0);
        StringBuilder typeBuilder = null;
        SysDictData sysDictData = new SysDictData();
        sysDictData.setDictType("oa_claim_status");
        List<SysDictData> dictDataList = dictDataMapper.selectDictDataList(sysDictData);
        Map<String, String> dictMap = new HashMap<>();
        if(!CollectionUtils.isEmpty(dictDataList)){
            for(SysDictData dictData : dictDataList){
                dictMap.put(dictData.getDictValue(), dictData.getDictLabel());
            }
        }
        String dictLabel = "";
        String types = "";
        if(!CollectionUtils.isEmpty(oaClaims)){
            for (OaClaim claim : oaClaims) {
                BigDecimal claimAmounts = new BigDecimal(0);
                types = "";
                typeBuilder = new StringBuilder();
                //报销明细
                QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
                claimDetailQueryWrapper.eq("claim_id",claim.getId());
                List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
                if(!CollectionUtils.isEmpty(oaClaimDetails)){
                    for(OaClaimDetail oaClaimDetail : oaClaimDetails){
                        if(oaClaimDetail.getClaimAmount() != null){
                            claimAmounts = claimAmounts.add(oaClaimDetail.getClaimAmount());
                        }
                        if(null != oaClaimDetail.getType()){
                            dictLabel = dictMap.get(String.valueOf(oaClaimDetail.getType()));
                            if(types.indexOf(dictLabel) == -1) {
                                if (StringUtils.isNotEmpty(types)) {
                                    types = types + "," + dictLabel;
                                } else {
                                    types = types + dictLabel;
                                }
                            }
                        }
                    }
                    claim.setTypes(types);
                    claim.setClaimAmounts(claimAmounts); //报销金额
                    claim.setOaClaimDetailList(oaClaimDetails);
                }

                //申请人名称
                if(claim.getUserId() != null){
                    claim.setUserName(sysUserMapper.selectUserById(claim.getUserId()).getNickName());
                }
                //申请部门
                if(claim.getDeptId() != null){
                    claim.setDeptName(sysDeptMapper.selectDeptById(claim.getDeptId()).getDeptName());
                }
            }
        }
        return oaClaims;
    }

    /**
     * 新增报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    @Override
    public int insertOaClaim(OaClaim oaClaim)
    {
        oaClaim.setId(getRedisIncreID.getId());
        oaClaim.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaClaim.setTenantId(SecurityUtils.getCurrComId());

        OaClaimRelation oaClaimRelation =  null;

        if (!CollectionUtils.isEmpty(oaClaim.getOaClaimDetailList())) {
            //绑定新报销明细
            for (OaClaimDetail claimDetail : oaClaim.getOaClaimDetailList()) {
                oaClaimRelation = new OaClaimRelation();
                oaClaimRelation = oaClaimRelationMapper.selectOaClaimRelationById(claimDetail.getRelationId());
                if(!ObjectUtils.isEmpty(oaClaimRelation)){
                    oaClaimRelation.setClaimState(2); //报销中
                    oaClaimRelation.setClaimId(oaClaim.getId());
                    oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);
                }

                claimDetail.setRelationId(claimDetail.getId());
                claimDetail.setId(getRedisIncreID.getId());
                claimDetail.setClaimState(2); //报销中
                claimDetail.setCostTime(DateUtils.getNowDate());
                claimDetail.setCreateTime(DateUtils.getNowDate());
                claimDetail.setCreateBy(String.valueOf(SecurityUtils.getUserId()));
                claimDetail.setUserId(SecurityUtils.getUserId());
                claimDetail.setDeptId(SecurityUtils.getDeptId());
                claimDetail.setClaimId(oaClaim.getId());
                oaClaimDetailMapper.insertOaClaimDetail(claimDetail);
            }
        }

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode(),SecurityUtils.getCurrComId());
        oaClaim.setFlowId(flowId);
        int result = oaClaimMapper.insertOaClaim(oaClaim);
        if(result > 0){
            OaClaim claim = oaClaimMapper.selectOaClaimById(oaClaim.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(claim, AdminCodeEnum.OACLAIM_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaClaimMapper.deleteOaClaimById(claim.getId());
                return 0;
            }else {
                claim.setState(AdminCodeEnum.OACLAIM_STATUS_VERIFYING.getCode());
                oaClaimMapper.updateOaClaim(claim);
            }
        }
        return result;
    }

    /**
     * 提交出差申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaClaim claim, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(claim.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_CLAIM.getCode(), AdminCodeEnum.TASK_TYPE_OA_CLAIM.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(claim.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CLAIM.getCode());
        //获取流程实例关联的业务对象
        OaClaim oaClaim = this.selectOaClaimById(claim.getId());
        SysUser sysUser = iSysUserService.selectUserById(oaClaim.getUserId());
        oaClaim.setUserName(sysUser.getUserName());
        oaClaim.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaClaim, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OACLAIM_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    @Override
    public int updateOaClaim(OaClaim oaClaim)
    {
        //原始数据
        OaClaim oaClaim1 = oaClaimMapper.selectOaClaimById(oaClaim.getId());
        oaClaim.setUpdateTime(DateUtils.getNowDate());
        int result = oaClaimMapper.updateOaClaim(oaClaim);
        if(result > 0){
            OaClaim claim = oaClaimMapper.selectOaClaimById(oaClaim.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", claim.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(claim, AdminCodeEnum.OACLAIM_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaClaimMapper.updateOaClaim(oaClaim1);
                return 0;
            }else {
                //解绑原有报销明细
                QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
                claimDetailQueryWrapper.eq("claim_id", oaClaim.getId());
                List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
                for (OaClaimDetail claimDetail : oaClaimDetails) {
                    oaClaimDetailMapper.deleteOaClaimDetailById(claimDetail.getId());
                }
                //判断
                if (!CollectionUtils.isEmpty(oaClaim.getOaClaimDetailList())) {
                    //绑定新报销明细
                    for (OaClaimDetail claimDetail : oaClaim.getOaClaimDetailList()) {
                        OaClaimRelation oaClaimRelation = new OaClaimRelation();
                        oaClaimRelation.setId(claimDetail.getRelationId());
                        oaClaimRelation = oaClaimRelationMapper.selectOaClaimRelationById(claimDetail.getRelationId());
                        oaClaimRelation.setClaimState(2); //报销中
                        oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);

                        claimDetail.setRelationId(claimDetail.getRelationId());
                        claimDetail.setId(getRedisIncreID.getId());
                        claimDetail.setClaimState(2); //报销中
                        claimDetail.setCostTime(DateUtils.getNowDate());
//                        claimDetail.setCreateTime(DateUtils.getNowDate());
//                        claimDetail.setCreateBy(String.valueOf(SecurityUtils.getUserId()));
                        claimDetail.setUserId(SecurityUtils.getUserId());
                        claimDetail.setDeptId(SecurityUtils.getDeptId());
                        claimDetail.setClaimId(oaClaim.getId());
                        oaClaimDetailMapper.insertOaClaimDetail(claimDetail);
                    }
                }
                claim.setState(AdminCodeEnum.OACLAIM_STATUS_VERIFYING.getCode());
                oaClaimMapper.updateOaClaim(claim);
            }
        }
        return result;
    }

    /**
     * 批量删除报销申请
     *
     * @param ids 需要删除的报销申请ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaClaim oaClaim = oaClaimMapper.selectOaClaimById(id);
                oaClaim.setFlag(1L);
                oaClaimMapper.updateOaClaim(oaClaim);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除报销申请信息
     *
     * @param id 报销申请ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimById(String id)
    {
        return oaClaimMapper.deleteOaClaimById(id);
    }

    @Override
    public int revoke(String id) {
        OaClaimRelation oaClaimRelation = null;
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaClaim claim = oaClaimMapper.selectOaClaimById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",claim.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OACLAIM_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(claim.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                claim.setState(AdminCodeEnum.OACLAIM_STATUS_WITHDRAW.getCode());
                oaClaimMapper.updateOaClaim(claim);

                //解绑报销明细
                QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
                claimDetailQueryWrapper.eq("claim_id", claim.getId());
                List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
                for (OaClaimDetail claimDetail : oaClaimDetails) {
                    oaClaimRelation = new OaClaimRelation();
                    oaClaimRelation.setId(claimDetail.getRelationId());
                    oaClaimRelation = oaClaimRelationMapper.selectOaClaimRelationById(claimDetail.getRelationId());
                    oaClaimRelation.setClaimState(1); //未报销
                    oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);

                    claimDetail.setRelationId(claimDetail.getRelationId());
                    claimDetail.setClaimState(1); //未报销
                    claimDetail.setCostTime(DateUtils.getNowDate());
//                    claimDetail.setCreateTime(DateUtils.getNowDate());
                    claimDetail.setClaimId(claim.getId());
                    oaClaimDetailMapper.updateOaClaimDetail(claimDetail);
                }
                String userId = String.valueOf(claim.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(claim, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_CLAIM.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaClaim oaClaim, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaClaim.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CLAIM.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"报销审批提醒",msgContent,"oa",oaClaim.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }
}
