package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaCheckMapper;
import com.business.system.domain.BaCheck;
import com.business.system.service.IBaCheckService;

/**
 * 验收Service业务层处理
 *
 * @author ljb
 * @date 2022-12-14
 */
@Service
public class BaCheckServiceImpl extends ServiceImpl<BaCheckMapper, BaCheck> implements IBaCheckService
{
    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询验收
     *
     * @param id 验收ID
     * @return 验收
     */
    @Override
    public BaCheck selectBaCheckById(String id)
    {
        return baCheckMapper.selectBaCheckById(id);
    }

    /**
     * 查询验收列表
     *
     * @param baCheck 验收
     * @return 验收
     */
    @Override
    public List<BaCheck> selectBaCheckList(BaCheck baCheck)
    {
        return baCheckMapper.selectBaCheckList(baCheck);
    }

    /**
     * 新增验收
     *
     * @param baCheck 验收
     * @return 结果
     */
    @Override
    public int insertBaCheck(BaCheck baCheck)
    {
        baCheck.setId(getRedisIncreID.getId());
        baCheck.setCreateTime(DateUtils.getNowDate());
        baCheck.setCreateBy(SecurityUtils.getUsername());
        return baCheckMapper.insertBaCheck(baCheck);
    }

    /**
     * 修改验收
     *
     * @param baCheck 验收
     * @return 结果
     */
    @Override
    public int updateBaCheck(BaCheck baCheck)
    {
        baCheck.setUpdateTime(DateUtils.getNowDate());
        baCheck.setUpdateBy(SecurityUtils.getUsername());
        return baCheckMapper.updateBaCheck(baCheck);
    }


    /**
     * 批量删除验收
     *
     * @param ids 需要删除的验收ID
     * @return 结果
     */
    @Override
    public int deleteBaCheckByIds(String[] ids)
    {
        return baCheckMapper.deleteBaCheckByIds(ids);
    }

    /**
     * 删除验收信息
     *
     * @param id 验收ID
     * @return 结果
     */
    @Override
    public int deleteBaCheckById(String id)
    {
        return baCheckMapper.deleteBaCheckById(id);
    }

    @Override
    public BigDecimal selectnums() {
        return baCheckMapper.selectnums();
    }

    @Override
    public String selectnums1(String relationId) {
        return baCheckMapper.selectnums1(relationId);
    }

}
