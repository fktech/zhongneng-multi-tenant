package com.business.system.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaContractStartTonVO;
import com.business.system.domain.vo.BaContractStartVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 合同启动Service业务层处理
 *
 * @author ljb
 * @date 2023-03-06
 */
@Service
public class BaContractStartServiceImpl extends ServiceImpl<BaContractStartMapper, BaContractStart> implements IBaContractStartService {

    private static final Logger log = LoggerFactory.getLogger(BaContractStartServiceImpl.class);
    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaShippingPlanServiceImpl baShippingPlanService;

    @Autowired
    private BaShippingPlanMapper baShippingPlanMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaDepotMapper depotMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private IBaChargingStandardsService iBaChargingStandardsService;

    @Autowired
    private IBaBidTransportService iBaBidTransportService;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private BaUserMailMapper baUserMailMapper;

    @Autowired
    private IBaDeclareService baDeclareService;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private IBaEnterpriseRelevanceService baEnterpriseRelevanceService;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private IBaTransportService baTransportService;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private IBaContractService iBaContractService;

    @Autowired
    private IBaDepotHeadService iBaDepotHeadService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private BaHiContractStartMapper baHiContractStartMapper;

    @Autowired
    private IBaHiContractStartService baHiContractStartService;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private BaCargoOwnerMapper baCargoOwnerMapper;

    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    @Override
    public BaContractStart selectBaContractStartById(String id) {
        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(id);
        if (!ObjectUtils.isEmpty(baContractStart)) {
            if (StringUtils.isNotEmpty(baContractStart.getSupplierId())) {
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContractStart.getSupplierId());
                if(StringUtils.isNotNull(baSupplier)){
                    baContractStart.setSupplierName(baSupplier.getName());
                }
                //装卸服务公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContractStart.getSupplierId());
                if(StringUtils.isNotNull(baCompany)){
                    baContractStart.setSupplierName(baCompany.getName());
                }
            }
            //立项信息
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            if (StringUtils.isNotNull(baProject)) {
                if (!ObjectUtils.isEmpty(baProject)) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsId());
                    if (!ObjectUtils.isEmpty(baGoods)) {
                        baProject.setGoodsName(baGoods.getName());
                    }
                    //货物种类
                    if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                        BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                        if (StringUtils.isNotNull(baGoodsType)) {
                            baContractStart.setGoodsType(baGoodsType.getName());
                        } else {
                            BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                            baContractStart.setGoodsType(baGoods1.getName());
                        }

                    }
                    //下游贸易商
                    if (StringUtils.isNotEmpty(baProject.getDownstreamTraders())) {
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baProject.getDownstreamTraders());
                        baProject.setDownstreamTradersName(baCompany.getName());
                    }
                }
                baContractStart.setBaProject(baProject);

                //终端企业
                //判断是否为采购订单或销售订单
                if (baContractStart.getType().equals("2") || baContractStart.getType().equals("3")) {
                    if (StringUtils.isNotEmpty(baContractStart.getEnterprise())) {
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContractStart.getEnterprise());
                        if (StringUtils.isNotNull(enterprise)) {
                            baContractStart.setEnterpriseName(enterprise.getName());
                            baContractStart.setEnterpriseId(enterprise.getId());
                        }
                    }
                } else {
                    if (StringUtils.isNotEmpty(baProject.getEnterpriseId())) {
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject.getEnterpriseId());
                        baContractStart.setEnterpriseName(enterprise.getName());
                        baContractStart.setEnterpriseId(enterprise.getId());
                    }
                }
                //项目编号
                if (StringUtils.isNotEmpty(baProject.getProjectNum())) {
                    baContractStart.setProjectNum(baProject.getProjectNum());
                    baContractStart.setProjectName(baProject.getName());
                }
                baContractStart.setProjectName(baProject.getName());
                //立项吨费
                if (baProject.getTonnageCharge() != null) {
                    baContractStart.setTonnageCharge(baProject.getTonnageCharge());
                }
                //立项年化
                if (baProject.getAnnualization() != null) {
                    baContractStart.setAnnualization(baProject.getAnnualization());
                }
                //货物种类
                if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                    BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                    if (StringUtils.isNotNull(baGoodsType)) {
                        baContractStart.setGoodsType(baGoodsType.getName());
                    } else {
                        BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                        baContractStart.setGoodsType(baGoods1.getName());
                    }
                }
                //商品名称
                if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsId());
                    if (StringUtils.isNotNull(baGoods)) {
                        baContractStart.setGoodsName(baGoods.getName());
                        baContractStart.setGoodsId(baGoods.getId());
                    }
                }
            }
            //托盘公司
            if (StringUtils.isNotEmpty(baContractStart.getCompanyId())) {
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContractStart.getCompanyId());
                baContractStart.setCompanyName(baCompany.getName());
                //baContractStart.setCompanyAnnualizedRate(baCompany.getAnnualizedRate());
            }
            //放标公司
            if (StringUtils.isNotEmpty(baContractStart.getLabelingCompanyId())) {
                BaCompany labelingCompany = baCompanyMapper.selectBaCompanyById(baContractStart.getLabelingCompanyId());
                if (!ObjectUtils.isEmpty(labelingCompany)) {
                    baContractStart.setLabelingCompanyName(labelingCompany.getName());
                    baContractStart.setLabelingType("1");
                } else {
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContractStart.getLabelingCompanyId());
                    if (!ObjectUtils.isEmpty(enterprise)) {
                        baContractStart.setLabelingCompanyName(enterprise.getName());
                        baContractStart.setLabelingType("2");
                    }
                }
            }
            //仓库名称
       /* if (StringUtils.isNotEmpty(baContractStart.getProjectId())) {
            //立项信息
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            //仓库信息
            if (StringUtils.isNotNull(baProject)) {
                if (StringUtils.isNotEmpty(baProject.getDepotId())) {
                    BaDepot baDepot = depotMapper.selectBaDepotById(baProject.getDepotId());
                    baContractStart.setDepotName(baDepot.getName());
                }
            }
        }*/
            if (StringUtils.isNotEmpty(baContractStart.getDepotId())) {
                BaDepot baDepot = depotMapper.selectBaDepotById(baContractStart.getDepotId());
                baContractStart.setDepotName(baDepot.getName());
            }
            //商品
            if (StringUtils.isNotEmpty(baContractStart.getGoodsId())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baContractStart.getGoodsId());
                baContractStart.setGoodsName(baGoods.getName());
                baContractStart.setBaGoods(baGoods);
            }
            //仓储立项
        /*if(StringUtils.isNotEmpty(baContractStart.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            if(StringUtils.isNotNull(baProject)){
                if(StringUtils.isNotEmpty(baProject.getDepotId())){
                    //当前库存
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id",baProject.getDepotId());
                    BaMaterialStock materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    if(StringUtils.isNotNull(materialStock)){
                        baContractStart.setInventoryTotal(materialStock.getInventoryTotal());
                    }
                }
            }
        }*/
            //发运计划
            QueryWrapper<BaShippingPlan> shippingPlanQueryWrapper = new QueryWrapper<>();
            shippingPlanQueryWrapper.eq("relevance_id", id);
            List<BaShippingPlan> baShippingPlans = baShippingPlanMapper.selectList(shippingPlanQueryWrapper);
            baContractStart.setBaShippingPlans(baShippingPlans);
            //合同启动团队
            if (StringUtils.isNotEmpty(baContractStart.getStartTeam())) {
                baContractStart.setStartTeamName(baUserMailMapper.selectBaUserMailById(baContractStart.getStartTeam()).getName());
            }
            //上游内部对接人
            if (StringUtils.isNotEmpty(baContractStart.getInternalContactPerson())) {
                baContractStart.setInternalContactPersonName(baUserMailMapper.selectBaUserMailById(baContractStart.getInternalContactPerson()).getName());
            }
            //下游内部对接人
            if (StringUtils.isNotEmpty(baContractStart.getLowInternalContactPerson())) {
                baContractStart.setLowInternalContactPersonName(baUserMailMapper.selectBaUserMailById(baContractStart.getLowInternalContactPerson()).getName());
            }
            //业务经理
        /*if(StringUtils.isNotEmpty(baContractStart.getServiceManager())){
            String[] split = baContractStart.getServiceManager().split(",");
            SysUser sysUser = sysUserMapper.selectUserById(Long.parseLong(split[split.length - 1]));
            //岗位信息
            String name = getName.getName(sysUser.getUserId());
            if(name.equals("") == false){
                baContractStart.setServiceManagerName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baContractStart.setServiceManagerName(sysUser.getNickName());
            }
        }*/
            if (StringUtils.isNotEmpty(baContractStart.getServiceManager())) {
                if (baContractStart.getServiceManager().equals("0") == false) {
                    SysUser sysUser = sysUserMapper.selectUserById(Long.parseLong(baContractStart.getServiceManager()));
                    baContractStart.setServiceManagerName(sysUser.getNickName());
                }
            }

            List<String> processInstanceIds = new ArrayList<>();
            //流程实例ID
            QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
            relatedQueryWrapper.eq("business_id", baContractStart.getId());

            relatedQueryWrapper.eq("flag", 0);
            //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
            relatedQueryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            baContractStart.setProcessInstanceId(processInstanceIds);
            //发起人
            SysUser sysUser = iSysUserService.selectUserById(baContractStart.getUserId());
            sysUser.setUserName(sysUser.getNickName());
            //岗位名称
            String name = getName.getName(sysUser.getUserId());
            if (name.equals("") == false) {
                sysUser.setPostName(name.substring(0, name.length() - 1));
            }
            baContractStart.setUser(sysUser);
            //创建人对应公司
            if (sysUser.getDeptId() != null) {
                SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                for (String depId:split) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                    if(StringUtils.isNotNull(dept1)){
                        if(dept1.getDeptType().equals("1")){
                            baContractStart.setComName(dept1.getDeptName());
                            break;
                        }
                    }
                }
            }
            //收费标准
            QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
            baChargingStandardsQueryWrapper.eq("relation_id", baContractStart.getId());
            BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectOne(baChargingStandardsQueryWrapper);
            baContractStart.setBaChargingStandards(baChargingStandards);

            //合同启动投标信息运输信息
            QueryWrapper<BaBidTransport> baBidTransportQueryWrapper = new QueryWrapper<>();
            baBidTransportQueryWrapper.eq("start_id", baContractStart.getId());
            baBidTransportQueryWrapper.eq("flag", 0);
            List<BaBidTransport> baBidTransports = baBidTransportMapper.selectList(baBidTransportQueryWrapper);
            if (!CollectionUtils.isEmpty(baBidTransports)) {
                for (BaBidTransport bidTransport : baBidTransports) {
                    if (!ObjectUtils.isEmpty(bidTransport)) {
                        BaCompany transportCompany = baCompanyMapper.selectBaCompanyById(bidTransport.getTransportCompany());
                        if (!ObjectUtils.isEmpty(transportCompany)) {
                            bidTransport.setTransportCompanyName(transportCompany.getName());
                            bidTransport.setCompanyAbbreviation(transportCompany.getCompanyAbbreviation());
                        } else {
                            bidTransport.setTransportCompanyName(bidTransport.getTransportCompany());
                        }
                    }
                }
            }

            baContractStart.setBaBidTransports(baBidTransports);
            //资金计划
            QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
            declareQueryWrapper.eq("start_id", baContractStart.getId()).and(itm -> itm.eq("source", 1).or().eq("state", "declare:pass"));
            List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);
            //计划申报去重
            List<BaDeclare> collect = baDeclares.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDeclare::getId))), ArrayList::new));
            baContractStart.setBaDeclares(collect);
            //投标委托人
            BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baContractStart.getBidPerson());
            if (!ObjectUtils.isEmpty(baUserMail)) {
                baContractStart.setBidPersonName(baUserMail.getName());
            }
            //指标信息
            if (StringUtils.isNotEmpty(baContractStart.getIndicatorInformation())) {
                BaEnterpriseRelevance baEnterpriseRelevance = baEnterpriseRelevanceService.selectBaEnterpriseRelevanceById(baContractStart.getIndicatorInformation());
                baContractStart.setBaEnterpriseRelevance(baEnterpriseRelevance);
            }
            //合同管理名称
            if (StringUtils.isNotEmpty(baContractStart.getContractId())) {
                BaContract baContract = baContractMapper.selectBaContractById(baContractStart.getContractId());
                baContractStart.setContractName(baContract.getName());
            }
            if (StringUtils.isNotEmpty(baContractStart.getBelongCompanyId())) {
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baContractStart.getBelongCompanyId()));
                if (!ObjectUtils.isEmpty(sysDept)) {
                    baContractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
            //子公司
        /*if(StringUtils.isNotEmpty(baContractStart.getSubsidiary())){
            BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baContractStart.getSubsidiary());
            baContractStart.setSubsidiaryName(baCompanyGroup.getCompanyName());
        }*/
            //事业部名称
        /*if(StringUtils.isNotEmpty(baContractStart.getDivision())){
            SysUser user = iSysUserService.selectUserById(baContractStart.getUserId());
            baContractStart.setUser(user);
        }*/
            //运营专员
            if (StringUtils.isNotEmpty(baContractStart.getPersonCharge())) {
                String[] split = baContractStart.getPersonCharge().split(",");
                String userName = "";
                for (String userId : split) {
                    if (this.isNumeric(userId) == true) {
                        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                        if (StringUtils.isNotNull(user)) {
                            userName = user.getNickName() + "," + userName;
                        }
                    }
                }
                if (StringUtils.isNotEmpty(userName) && !"".equals(userName)) {
                    baContractStart.setPersonChargeName(userName.substring(0, userName.length() - 1));
                } else {
                    baContractStart.setPersonChargeName(baContractStart.getPersonCharge());
                }
            }
            //上游公司
            if (StringUtils.isNotEmpty(baContractStart.getUpstreamCompany())) {
                //查询供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContractStart.getUpstreamCompany());
                if (StringUtils.isNotNull(baSupplier)) {
                    baContractStart.setUpstreamCompanyName(baSupplier.getName());
                }
                //查询公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baContractStart.getUpstreamCompany());
                if (StringUtils.isNotNull(baCompany)) {
                    baContractStart.setUpstreamCompanyName(baCompany.getName());
                }
            }
            //主体公司
            if (StringUtils.isNotEmpty(baContractStart.getSubjectCompany())) {
                if(this.isNumeric(baContractStart.getSubjectCompany()) == true){
                    //子公司
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baContractStart.getSubjectCompany()));
                    if (!ObjectUtils.isEmpty(sysDept)) {
                        baContractStart.setSubjectCompanyName(sysDept.getDeptName());
                    }
                }else {
                    baContractStart.setSubjectCompanyName(baContractStart.getSubjectCompany());
                }
            }
            //终端公司
            if (StringUtils.isNotEmpty(baContractStart.getTerminalCompany())) {
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContractStart.getTerminalCompany());
                if (StringUtils.isNotNull(enterprise)) {
                    baContractStart.setTerminalCompanyName(enterprise.getName());
                }
            }
            //下游公司
            if (StringUtils.isNotEmpty(baContractStart.getDownstreamCompany())) {
                //下游公司名称
                String downstreamCompanyName = "";
                String[] split = baContractStart.getDownstreamCompany().split(",");
                for (String downstream : split) {
                    String[] split1 = downstream.split(";");
                    //下游企业
                    if ("1".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                        downstreamCompanyName = split1[0] + ";" + baCompany.getName() + "," + downstreamCompanyName;
                    }
                    //终端企业
                    if ("2".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(key);
                        downstreamCompanyName = split1[0] + ";" + enterprise.getName() + "," + downstreamCompanyName;
                    }
                    //公司
                    /*if (!"1".equals(split1[0]) && !"2".equals(split1[0]) && !"6".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                        downstreamCompanyName = split1[0] + ";" + baCompany.getName() + "," + downstreamCompanyName;
                    }*/
                    //集团公司
                    /*if ("6".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(key);
                        downstreamCompanyName = split1[0] + ";" + companyGroup.getCompanyName() + "," + downstreamCompanyName;
                    }*/
                    //运输类公司
                    if ("3".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCargoOwner cargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(key);
                        if(StringUtils.isNotNull(cargoOwner)){
                            downstreamCompanyName = split1[0] + ";" + cargoOwner.getName() + "," + downstreamCompanyName;
                        }
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                        if(StringUtils.isNotNull(baCompany)){
                            downstreamCompanyName = split1[0] + ";" + baCompany.getName() + "," + downstreamCompanyName;
                        }
                    }
                }
                if (",".equals(downstreamCompanyName.substring(downstreamCompanyName.length() - 1))) {
                    baContractStart.setDownstreamCompanyName(downstreamCompanyName.substring(0, downstreamCompanyName.length() - 1));
                } else {
                    baContractStart.setDownstreamCompanyName(downstreamCompanyName);
                }

            }
            //合同信息
            BaContract baContract = new BaContract();
            baContract.setSaleOrder(id);
            baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
            baContractStart.setBaContractList(baContracts);
            //出库信息
            BaDepotHead baDepotHead = new BaDepotHead();
            baDepotHead.setOrderId(id);
            //租户ID
            baDepotHead.setTenantId(SecurityUtils.getCurrComId());
            List<BaDepotHead> baDepotHeads = iBaDepotHeadService.selectBaDepotHead(baDepotHead);
            baContractStart.setBaDepotHeadList(baDepotHeads);
        } else {
            baContractStart = new BaContractStart();
        }
        return baContractStart;
    }

    //判断String是否能转数字
    public static boolean isNumeric(String str) {
        try {
            Long.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动
     */
    @Override
    @DataScope(deptAlias = "s", userAlias = "s")
    public List<BaContractStart> selectBaContractStartList(BaContractStart baContractStart) {
        List<BaContractStart> baContractStarts = baContractStartMapper.selectBaContractStartList(baContractStart);

        for (BaContractStart contractStart : baContractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }

            //查找运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", contractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
            BigDecimal transit = new BigDecimal(0);
            for (BaTransport baTransport : baTransports) {
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    //运输中
                    transit = transit.add(baCheck.getCheckCoalNum());
                }
            }
            contractStart.setCheckCoalNum(transit);
            if(StringUtils.isNotEmpty(contractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    contractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
        }
        return baContractStarts;
    }

    @Override
    @DataScope(deptAlias = "s", userAlias = "s")
    public List<BaContractStart> baContractStartList(BaContractStart baContractStart) {

        return baContractStartMapper.selectBaContractStartList(baContractStart);
    }

    @Override
    public List<BaContractStart> selectContractStartList(BaContractStart baContractStart) {
        List<BaContractStart> contractStarts = baContractStartMapper.selectContractStartList(baContractStart);
        for (BaContractStart contractStart:contractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }
        }
        return contractStarts;
    }

    /**
     * 新增合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    @Override
    public int insertBaContractStart(BaContractStart baContractStart) {
        baContractStart.setId(getRedisIncreID.getId());
        baContractStart.setCreateTime(DateUtils.getNowDate());
        baContractStart.setCreateBy(SecurityUtils.getUsername());
        baContractStart.setUserId(SecurityUtils.getUserId());
        baContractStart.setDeptId(SecurityUtils.getDeptId());
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        //人员定责业务经理归属部门
        if(StringUtils.isNotEmpty(baContractStart.getServiceManager())){
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(baContractStart.getServiceManager()));
            baContractStart.setManageDeptId(user.getDeptId());
        }
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baContractStart.getWorkState())){
            //流程发起状态
            baContractStart.setWorkState("1");
            //默认审批流程已通过
            baContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
            //默认通过赋予编号
            //合同启动序号
            QueryWrapper<BaContractStart> baContractStartQueryWrapper = new QueryWrapper<>();
            baContractStartQueryWrapper.eq("project_id",baContractStart.getProjectId());
            baContractStartQueryWrapper.eq("type","5");
            //baContractStartQueryWrapper.eq("flag",0);
            //baContractStartQueryWrapper.eq("state","contractStart:pass");
            baContractStartQueryWrapper.orderByDesc("serial_number");
            List<BaContractStart> contractStarts = baContractStartMapper.selectList(baContractStartQueryWrapper);
            int num = 0;
            if(!CollectionUtils.isEmpty(contractStarts)){
                if(contractStarts.get(0).getStartSerial() != null){
                    num = contractStarts.get(0).getStartSerial() + 1;
                }
            } else {
                num = 1;
            }
            baContractStart.setStartSerial(num);
        }
        //运输计划
        if (StringUtils.isNotNull(baContractStart.getBaShippingPlans())) {
            for (BaShippingPlan baShippingPlan : baContractStart.getBaShippingPlans()) {
                baShippingPlan.setRelevanceId(baContractStart.getId());
                baShippingPlanService.insertBaShippingPlan(baShippingPlan);
            }
        }
        //新增收费标准
        BaChargingStandards baChargingStandards = baContractStart.getBaChargingStandards();
        if (!ObjectUtils.isEmpty(baChargingStandards)) {
            baChargingStandards.setRelationId(baContractStart.getId());
            iBaChargingStandardsService.insertBaChargingStandards(baChargingStandards);
        }

        //新增合同启动投标信息运输信息
        List<BaBidTransport> baBidTransports = baContractStart.getBaBidTransports();
        if (!CollectionUtils.isEmpty(baBidTransports)) {
            for (BaBidTransport baBidTransport : baBidTransports) {
                baBidTransport.setStartId(baContractStart.getId());
                iBaBidTransportService.insertBaBidTransport(baBidTransport);
            }
        }
        //新增资金计划
        if (!CollectionUtils.isEmpty(baContractStart.getBaDeclares())) {
            for (BaDeclare baDeclare : baContractStart.getBaDeclares()) {
                baDeclare.setId(getRedisIncreID.getId());
                baDeclare.setStartId(baContractStart.getId());
                baDeclare.setSource(1);
                baDeclare.setFlag(1L);
                baDeclare.setCreateTime(DateUtils.getNowDate());
                baDeclare.setCreateBy(SecurityUtils.getUsername());
                baDeclare.setUserId(SecurityUtils.getUserId());
                baDeclare.setDeptId(SecurityUtils.getDeptId());
                //判断资金计划是否重复
                BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                if (StringUtils.isNotNull(baDeclare1)) {
                    return -1;
                }
                baDeclareMapper.insertBaDeclare(baDeclare);
            }
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode(),SecurityUtils.getCurrComId());
        baContractStart.setFlowId(flowId);
        int result = baContractStartMapper.insertBaContractStart(baContractStart);
        if (result > 0) {
            //判断业务归属公司是否是上海子公司
            if("2".equals(baContractStart.getWorkState())) {
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractStart, AdminCodeEnum.CONTRACTSTART_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    //回滚收费标准
                    if (!ObjectUtils.isEmpty(baChargingStandards)) {
                        baChargingStandardsMapper.deleteBaChargingStandardsByRelationId(baChargingStandards.getRelationId());
                    }
                    //回滚合同启动投标信息运输信息
                    baBidTransportMapper.deleteBaChargingStandardsByStartId(baContractStart.getId());
                    //回滚
                    deleteBaContractStartById(baContractStart.getId());
                    return 0;
                } else {
                    contractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_VERIFYING.getCode());
                    baContractStartMapper.updateBaContractStart(contractStart);
                }
            }
        }

        return result;
    }

    /**
     * 提交合同启动审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaContractStart baContractStart, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baContractStart.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baContractStart.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaContractStart baContractStart1 = this.selectBaContractStartById(baContractStart.getId());
        SysUser sysUser = iSysUserService.selectUserById(baContractStart1.getUserId());
        baContractStart1.setUserName(sysUser.getUserName());
        baContractStart1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baContractStart1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 提交合同启动审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstancesHi(BaHiContractStart baHiContractStart, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baHiContractStart.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baHiContractStart.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaHiContractStart baHiContractStart1 = baHiContractStartService.selectBaHiContractStartById(baHiContractStart.getId());
        SysUser sysUser = iSysUserService.selectUserById(baHiContractStart1.getUserId());
        baHiContractStart1.setUserName(sysUser.getUserName());
        baHiContractStart1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baHiContractStart1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstancesHi(instancesRuntimeDTO, sysUser);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)) {
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if (data != null) {
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACTSTART_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstancesHi(ProcessInstancesRuntimeDTO processInstancesRuntime, SysUser user) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = new UserInfo();
        userInfo.setId(String.valueOf(user.getUserId()));
        userInfo.setName(user.getUserName());
        userInfo.setNickName(user.getNickName());
        //根据部门ID获取部门信息
        SysDept sysDept = iSysDeptService.selectDeptById(user.getDeptId());
        if(!ObjectUtils.isEmpty(sysDept)){
            userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
        }
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)) {
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if (data != null) {
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACTSTART_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    @Override
    public int updateBaContractStart(BaContractStart baContractStart) {
        //原数据
        BaContractStart start = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
        baContractStart.setUpdateTime(DateUtils.getNowDate());
        baContractStart.setUpdateBy(SecurityUtils.getUsername());
        baContractStart.setCreateTime(baContractStart.getUpdateTime());
        //运输计划
        if (StringUtils.isNotNull(baContractStart.getBaShippingPlans())) {
            for (BaShippingPlan baShippingPlan : baContractStart.getBaShippingPlans()) {
                if (StringUtils.isNotEmpty(baShippingPlan.getId())) {
                    baShippingPlanService.updateBaShippingPlan(baShippingPlan);
                } else {
                    baShippingPlan.setRelevanceId(baContractStart.getId());
                    baShippingPlanService.insertBaShippingPlan(baShippingPlan);
                }
            }
        }
        //查询收费标准
        QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
        baChargingStandardsQueryWrapper.eq("relation_id", start.getId());
        BaChargingStandards baChargingStandards1 = baChargingStandardsMapper.selectOne(baChargingStandardsQueryWrapper);
        if (!ObjectUtils.isEmpty(baChargingStandards1)) {
            //更新收费标准
            iBaChargingStandardsService.updateBaChargingStandards(baContractStart.getBaChargingStandards());
        } else {
            //新增收费标准
            iBaChargingStandardsService.insertBaChargingStandards(baContractStart.getBaChargingStandards());
        }

        //查询原合同启动投标信息运输信息
        QueryWrapper<BaBidTransport> baBidTransportsQueryWrapper = new QueryWrapper<>();
        baBidTransportsQueryWrapper.eq("start_id", start.getId());
        List<BaBidTransport> baBidTransports1 = baBidTransportMapper.selectList(baBidTransportsQueryWrapper);

        //修改合同启动投标信息运输信息
        List<BaBidTransport> baBidTransports = baContractStart.getBaBidTransports();
        //新增加运输信息
        List<String> bidTransportIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(baBidTransports)) {
            baBidTransports.stream().forEach(item -> {
                if (StringUtils.isNotEmpty(item.getId())) {
                    iBaBidTransportService.updateBaBidTransport(item);
                } else {
                    item.setStartId(baContractStart.getId());
                    item.setId(getRedisIncreID.getId());
                    bidTransportIds.add(item.getId());
                    iBaBidTransportService.insertBaBidTransport(item);
                }
            });
        }
        //查询原资金计划
        QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
        declareQueryWrapper.eq("start_id", start.getId());
        declareQueryWrapper.eq("flag", 0);
        List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);


        //新增资金计划id
        List<String> declareIds = new ArrayList<>();
        //修改原资金计划
        if (StringUtils.isNotNull(baContractStart.getBaDeclares())) {
            for (BaDeclare baDeclare : baContractStart.getBaDeclares()) {
                if (StringUtils.isNotEmpty(baDeclare.getId())) {
                    //判断自己申请是否存在
                    BaDeclare declare = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
                    if (declare.getStartId().equals(baDeclare.getStartId())) {
                        if (!declare.getPlanTime().equals(baDeclare.getPlanTime())) {
                            BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                            if (StringUtils.isNotNull(baDeclare1)) {
                                return -1;
                            }
                        }
                    }
                    if (!declare.getStartId().equals(baDeclare.getStartId())) {
                        BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                        if (StringUtils.isNotNull(baDeclare1)) {
                            return -1;
                        }
                    }
                    baDeclare.setUpdateBy(SecurityUtils.getUsername());
                    baDeclare.setUpdateTime(DateUtils.getNowDate());
                    baDeclareMapper.updateBaDeclare(baDeclare);
                } else {
                    baDeclare.setId(getRedisIncreID.getId());
                    baDeclare.setStartId(baContractStart.getId());
                    baDeclare.setSource(1);
                    baDeclare.setFlag(1L);
                    baDeclare.setCreateTime(DateUtils.getNowDate());
                    baDeclare.setCreateBy(SecurityUtils.getUsername());
                    baDeclare.setUserId(SecurityUtils.getUserId());
                    baDeclare.setDeptId(SecurityUtils.getDeptId());
                    //判断资金计划是否重复
                    BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                    if (StringUtils.isNotNull(baDeclare1)) {
                        return -1;
                    }
                    baDeclareMapper.insertBaDeclare(baDeclare);
                }
            }
        }
        int result = baContractStartMapper.updateBaContractStart(baContractStart);
        if (result > 0 && !baContractStart.getState().equals(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode())) {
            BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", contractStart.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractStart, AdminCodeEnum.CONTRACTSTART_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                //回滚原收费标准
                if (!ObjectUtils.isEmpty(baChargingStandards1)) {
                    baChargingStandardsMapper.updateBaChargingStandards(baChargingStandards1);
                }
                //回滚原合同启动投标信息运输信息
                if (!CollectionUtils.isEmpty(baBidTransports1)) {
                    baBidTransports1.stream().forEach(item -> {
                        //item.setStartId(start.getId());
                        iBaBidTransportService.updateBaBidTransport(item);
                    });
                }
                iBaBidTransportService.deleteBaBidTransportByIds(bidTransportIds.toArray(new String[0]));
                //回滚资金计划
                if (!CollectionUtils.isEmpty(baDeclares)) {
                    for (BaDeclare baDeclare : baDeclares) {
                        baDeclareService.updateBaDeclare(baDeclare);
                    }
                }
                baDeclareService.deleteBaDeclareByIds(declareIds.toArray(new String[0]));
                //流程启动失败回滚原数据
                baContractStartMapper.updateBaContractStart(start);
                return 0;
            } else {
                contractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_VERIFYING.getCode());
                baContractStartMapper.updateBaContractStart(contractStart);
            }
        }
        return result;
    }

    /**
     * 批量删除合同启动
     *
     * @param ids 需要删除的合同启动ID
     * @return 结果
     */
    @Override
    public int deleteBaContractStartByIds(String[] ids) {
        if (StringUtils.isNotEmpty(ids)) {
            for (String id : ids) {
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(id);
                baContractStart.setFlag((long) 1);
                baContractStartMapper.updateBaContractStart(baContractStart);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除合同启动信息
     *
     * @param id 合同启动ID
     * @return 结果
     */
    @Override
    public int deleteBaContractStartById(String id) {
        return baContractStartMapper.deleteBaContractStartById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if (StringUtils.isNotEmpty(id)) {
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baContractStart.getId());
            queryWrapper.eq("flag", 0);
            queryWrapper.ne("approve_result", AdminCodeEnum.CONTRACTSTART_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baContractStart.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result = workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if (result.get("code").toString().equals("200")) {
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_WITHDRAW.getCode());
                baContractStartMapper.updateBaContractStart(baContractStart);
                String userId = String.valueOf(baContractStart.getUserId());
                //给所有已审批的用户发消息
                if (!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if (!ObjectUtils.isEmpty(ajaxResult)) {
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for (LinkedHashMap map : data) {
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                                        if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                            this.insertMessage(baContractStart, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int difference(BaContractStart baContractStart) {

        if (baContractStart.getInitialSettlementDifference() != null) {
            baContractStart.setCurrentSettlementDifference(baContractStart.getInitialSettlementDifference());
            baContractStartMapper.updateBaContractStart(baContractStart);
        } else if (baContractStart.getCompletion() != null) {
            //完结同时预埋状态
            baContractStart.setStartType("4");
            baContractStartMapper.updateBaContractStart(baContractStart);
        } else if (baContractStart.getCompletion() == null) {
            return 0;
        }
        return 1;
    }

    @Override
    public UR contractStartName(String projectId, String type) {
        BaProject baProject = baProjectMapper.selectBaProjectById(projectId);
        String contractStartName = "";
        if ("1".equals(type)) {
            contractStartName = this.addname(baProject.getName(),baProject.getId());
        } else if ("2".equals(type)) {
            contractStartName = this.addname(baProject.getName() + "(采购)",baProject.getId());
        } else if ("3".equals(type)) {
            contractStartName = this.addname(baProject.getName() + "(销售)",baProject.getId());
        }

        return UR.ok().code(200).data("name", contractStartName);
    }

    public String addname(String name,String projectId) {
        int indexOf = 0;
        String orderNameReplace = null;
        int temp = 0;
        String contractStartName = "";
        BaContractStart baContractStart = new BaContractStart();
        baContractStart.setName(name);
        baContractStart.setProjectId(projectId);
        baContractStart.setFlag(0L);
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        List<BaContractStart> baContractStartList = baContractStartMapper.selectBaContractStartListbyName(baContractStart);
        if (!CollectionUtils.isEmpty(baContractStartList)) {
            indexOf = baContractStartList.get(0).getName().lastIndexOf("-");
            orderNameReplace = baContractStartList.get(0).getName().substring(indexOf + 1);
            if ("0".equals(orderNameReplace.substring(0, 1))) {
                temp = (Integer.parseInt(orderNameReplace.substring(1, 2)) + 1);
                if (temp < 10) {
                    contractStartName = name + "-0" + temp;
                } else {
                    contractStartName = name + "-" + temp;
                }
            } else {
                if (StringUtils.isNumeric(orderNameReplace)) {
                    contractStartName = name + "-" + (Integer.valueOf(orderNameReplace) + 1);
                }
            }
        } else {
            contractStartName = name + "-0" + 1;
        }
        return contractStartName;
    }

    @Override
    public String contractStartSerial(String project) {

        QueryWrapper<BaContractStart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_id", project);
        queryWrapper.eq("type", "5");
        queryWrapper.eq("flag", 0);
        queryWrapper.orderByDesc("serial_number");
        List<BaContractStart> contractStarts = baContractStartMapper.selectList(queryWrapper);
        if (contractStarts.size() > 0) {
            int num = 1;
            if (contractStarts.get(0).getSerialNumber() != null) {
                num = contractStarts.get(0).getSerialNumber() + 1;
            }
            return String.valueOf(num);
        }
        return "1";
    }

    @Override
    public List<BaContractStart> contractStartList(BaContractStartDTO baContractStartDTO) {
        //供货中
        List<BaContractStart> startList = new ArrayList<>();

        QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(baContractStartDTO.getName())) {
            startQueryWrapper.like("name", baContractStartDTO.getName());
        }
        /*LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        List<SysRole> roles = user.getRoles();
        Long[] deptId = {};
        for (SysRole role:roles) {
            startQueryWrapper.inSql("dept_id","IN ( SELECT dept_id FROM sys_role_dept WHERE role_id = "+role.getRoleId()+" )");
        }*/
        //包含部门id
       /* LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        SysDeptRoleDTO sysDeptRoleDTO = new SysDeptRoleDTO();
        sysDeptRoleDTO.setRoleId(user.getRoles().get(0).getRoleId().toString());
        if(user.getRoles().get(0).getDataScope().equals("1") == false){
            List<SysDept> sysDeptList = sysUserMapper.selectDeptsByRoleId(sysDeptRoleDTO);
            String deptId = SecurityUtils.getDeptId().toString();
            for (SysDept dept:sysDeptList) {
                deptId = dept.getDeptId() + "," + deptId;
            }
            //转换相关部门id
            List<Integer> deptId1 = new ArrayList<>();
            String[] split = deptId.split(",");
            for (String id:split) {
                deptId1.add(Integer.parseInt(id));
            }
            startQueryWrapper.in("dept_id",deptId1);
        }*/
        startQueryWrapper.eq("state", "contractStart:pass");
        startQueryWrapper.eq("flag", 0);
        if (!"all".equals(baContractStartDTO.getType())) {
            startQueryWrapper.eq("start_type", baContractStartDTO.getType());
        }
        startQueryWrapper.orderByDesc("create_time");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            startQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
        for (BaContractStart baContractStart : baContractStartList) {
            //发起人
            //baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName());
            //岗位信息
            String name = getName.getName(baContractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName());
            }
            HomeDTO homeDTO = new HomeDTO();
            //合同管理
            List<BaContract> list = new ArrayList<>();
            //合同启动标签
            if (StringUtils.isNotEmpty(baContractStart.getProjectId())) {
                //立项
                BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
                //立项名称
                baContractStart.setProjectName(baProject.getName());
                if (baProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode())) {
                    homeDTO.setBusinessType(baProject.getBusinessType());
                    homeDTO.setProjectType(baProject.getProjectType());
                    homeDTO.setShippingNum(baProject.getShippingNum());
                    if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                        BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                        if (StringUtils.isNotNull(baGoodsType)) {
                            homeDTO.setGoodsType(baGoodsType.getName());
                        }
                    }
                }
            }
            //合同启动
            homeDTO.setPrice(baContractStart.getPrice());
            homeDTO.setTonnage(baContractStart.getTonnage());
            homeDTO.setTransportWay(baContractStart.getTransportWay());
            //合同签订
            QueryWrapper<BaContract> contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 6);
            contractQueryWrapper.eq("signing_status","已签");
            List<BaContract> baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //上游合同
            homeDTO.setUpstream(baContracts.size());
            contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 7);
            contractQueryWrapper.eq("signing_status","已签");
            baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //下游合同
            homeDTO.setDownstream(baContracts.size());
            //补充协议
            contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 8);
            contractQueryWrapper.eq("signing_status","已签");
            baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //其他合同
            homeDTO.setOther(baContracts.size());
            contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 9);
            contractQueryWrapper.eq("signing_status","已签");
            baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //补充协议
            homeDTO.setAgreeOn(baContracts.size());
            //下游补充协议
            contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 10);
            contractQueryWrapper.eq("signing_status","已签");
            baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //下游补充协议
            homeDTO.setBelowAgreeOn(baContracts.size());
            //运输合同
            contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 5);
            contractQueryWrapper.eq("signing_status","已签");
            baContracts = baContractMapper.selectList(contractQueryWrapper);
            list.addAll(baContracts);
            //运输合同
            homeDTO.setTransportContract(baContracts.size());
            //合同管理
            homeDTO.setBaContractList(list);
            //物流信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", baContractStart.getId());
            //transportQueryWrapper.eq("state",AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> transports = baTransportMapper.selectList(transportQueryWrapper);
            //已发货
            BigDecimal shipped = new BigDecimal(0);
            //已到货
            BigDecimal arrived = new BigDecimal(0);
            //损耗量
            BigDecimal wastage = new BigDecimal(0);
            for (BaTransport baTransport : transports) {
                //验收
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                //已发货
                if (StringUtils.isNotNull(baCheck)) {
                    shipped = shipped.add(baCheck.getCheckCoalNum());
                }
                checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 2);
                baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    if (baCheck.getCheckCoalNum() != null) {
                        //已到货
                        arrived = arrived.add(baCheck.getCheckCoalNum());
                    }

                    if (baCheck.getConsumption() != null) {
                        //损耗量
                        wastage = wastage.add(baCheck.getConsumption());
                    }
                }
            }
            //运输中
            transportQueryWrapper.eq("transport_status", 1);
            transports = baTransportMapper.selectList(transportQueryWrapper);
            //运输中
            BigDecimal transit = new BigDecimal(0);
            for (BaTransport baTransport : transports) {
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    //运输中
                    transit = transit.add(baCheck.getCheckCoalNum());
                }
            }

            homeDTO.setShipped(shipped);
            homeDTO.setArrived(arrived);
            homeDTO.setTransit(transit);
            homeDTO.setAccepted(arrived);
            homeDTO.setWastage(wastage);
            //查询汽运
            transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", baContractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            transportQueryWrapper.eq("transport_type", 2);
            transports = baTransportMapper.selectList(transportQueryWrapper);
            if (transports.size() > 0) {
                homeDTO.setAutomobileTransportation("已存在");
            }
            //查询火运
            transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", baContractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            transportQueryWrapper.eq("transport_type", 1);
            transports = baTransportMapper.selectList(transportQueryWrapper);
            if (transports.size() > 0) {
                homeDTO.setFireTransport("已存在");
            }
            //终端结算
            BigDecimal settlementNum = new BigDecimal(0);
            //终端结算金额
            BigDecimal settlementAmount = new BigDecimal(0);
            QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
            settlementQueryWrapper.eq("contract_id", baContractStart.getId());
            settlementQueryWrapper.eq("state", "settlement:pass");
            settlementQueryWrapper.eq("flag", 0);
            settlementQueryWrapper.eq("settlement_type", 3);
            List<BaSettlement> settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
            if(settlementList.size() > 0){
                homeDTO.setSettlement("已存在");
            }
            for (BaSettlement baSettlement : settlementList) {
                if (StringUtils.isNotNull(baSettlement)) {
                    if (baSettlement.getSettlementNum() != null && baSettlement.getSettlementAmount() != null) {
                        settlementNum = settlementNum.add(baSettlement.getSettlementNum());
                        settlementAmount = settlementAmount.add(baSettlement.getSettlementAmount());
                    }
                }
            }
            homeDTO.setSettlementNum(settlementNum);
            homeDTO.setSettlementAmount(settlementAmount);
            if (settlementList.size() > 0) {
                homeDTO.setCreateTime(settlementList.get(0).getCreateTime());
            }
            //上游结算
            BigDecimal upstreamSettlementNum = new BigDecimal(0);
            //上游结算金额
            BigDecimal upstreamSettlementAmount = new BigDecimal(0);
            settlementQueryWrapper = new QueryWrapper<>();
            settlementQueryWrapper.eq("contract_id", baContractStart.getId());
            settlementQueryWrapper.eq("state", "settlement:pass");
            settlementQueryWrapper.eq("flag", 0);
            settlementQueryWrapper.eq("settlement_type", 1);
            settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
            for (BaSettlement baSettlement : settlementList) {
                if (StringUtils.isNotNull(baSettlement)) {
                    if (baSettlement.getSettlementNum() != null) {
                        upstreamSettlementNum = upstreamSettlementNum.add(baSettlement.getSettlementNum());
                    } else {
                        upstreamSettlementNum = upstreamSettlementNum.add(new BigDecimal(0));
                    }
                    if (baSettlement.getSettlementAmount() != null) {
                        upstreamSettlementAmount = upstreamSettlementAmount.add(baSettlement.getSettlementAmount());
                    } else {
                        upstreamSettlementAmount = upstreamSettlementAmount.add(new BigDecimal(0));
                    }
                }
            }
            homeDTO.setUpstreamSettlementNum(upstreamSettlementNum);
            homeDTO.setUpstreamSettlementAmount(upstreamSettlementAmount);
            if (settlementList.size() > 0) {
                homeDTO.setUpstreamCreateTime(settlementList.get(0).getCreateTime());
            }
            //下游结算
            BigDecimal downstreamSettlementNum = new BigDecimal(0);
            //下游结算金额
            BigDecimal downstreamSettlementAmount = new BigDecimal(0);
            settlementQueryWrapper = new QueryWrapper<>();
            settlementQueryWrapper.eq("contract_id", baContractStart.getId());
            settlementQueryWrapper.eq("state", "settlement:pass");
            settlementQueryWrapper.eq("flag", 0);
            settlementQueryWrapper.eq("settlement_type", 2);
            settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
            if(settlementList.size() > 0){
                homeDTO.setDownstreamSettlement("已存在");
            }
            for (BaSettlement baSettlement : settlementList) {
                if (StringUtils.isNotNull(baSettlement)) {
                    if (baSettlement.getSettlementNum() != null) {
                        downstreamSettlementNum = downstreamSettlementNum.add(baSettlement.getSettlementNum());
                    } else {
                        downstreamSettlementNum = downstreamSettlementNum.add(new BigDecimal(0));
                    }
                    if (baSettlement.getSettlementAmount() != null) {
                        downstreamSettlementAmount = downstreamSettlementAmount.add(baSettlement.getSettlementAmount());
                    } else {
                        downstreamSettlementAmount = downstreamSettlementAmount.add(new BigDecimal(0));
                    }
                }
            }
            homeDTO.setDownstreamSettlementNum(downstreamSettlementNum);
            homeDTO.setDownstreamSettlementAmount(downstreamSettlementAmount);
            if (settlementList.size() > 0) {
                homeDTO.setDownstreamCreateTime(settlementList.get(0).getCreateTime());
            }
            //付款申请
            //已付货款
            BigDecimal paymentForGoods = new BigDecimal(0);
            BaPayment payment = new BaPayment();
            payment.setStartId(baContractStart.getId());
            payment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
            payment.setType("1");//付款类型
            List<BaPayment> list1 = baPaymentService.selectBaPaymentList(payment);
            for (BaPayment payment1:list1) {
                if(payment1.getPaymentAmount() != null){
                    paymentForGoods = paymentForGoods.add(payment1.getPaymentAmount());
                }
            }
            homeDTO.setPaymentForGoods(paymentForGoods);
            //已付运费
            BigDecimal freight = new BigDecimal(0);
            QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("start_id", baContractStart.getId());
            paymentQueryWrapper.eq("state", "payment:pass");
            paymentQueryWrapper.eq("type", 2);
            paymentQueryWrapper.eq("flag", 0);
            List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment : baPayments) {
                if (StringUtils.isNotNull(baPayment)) {
                    freight = freight.add(baPayment.getApplyPaymentAmount());
                }
            }
            homeDTO.setFreight(freight);
            //服务费
            BigDecimal serviceCharge = new BigDecimal(0);
            paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("start_id", baContractStart.getId());
            paymentQueryWrapper.eq("state", "payment:pass");
            paymentQueryWrapper.eq("type", 3);
            paymentQueryWrapper.eq("flag", 0);
            baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment : baPayments) {
                if (StringUtils.isNotNull(baPayment)) {
                    serviceCharge = serviceCharge.add(baPayment.getApplyPaymentAmount());
                }
            }
            homeDTO.setServiceCharge(serviceCharge);
            //保证金
            BigDecimal paymentMargin = new BigDecimal(0);
            paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("start_id", baContractStart.getId());
            paymentQueryWrapper.eq("state", "payment:pass");
            paymentQueryWrapper.eq("type", 4);
            paymentQueryWrapper.eq("flag", 0);
            baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment : baPayments) {
                if (StringUtils.isNotNull(baPayment)) {
                    paymentMargin = paymentMargin.add(baPayment.getApplyPaymentAmount());
                }
            }
            homeDTO.setPaymentMargin(paymentMargin);
            //其他付款
            BigDecimal paymentOther = new BigDecimal(0);
            paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("start_id", baContractStart.getId());
            paymentQueryWrapper.eq("state", "payment:pass");
            paymentQueryWrapper.eq("type", 5);
            paymentQueryWrapper.eq("flag", 0);
            baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment : baPayments) {
                if (StringUtils.isNotNull(baPayment)) {
                    paymentOther = paymentOther.add(baPayment.getApplyPaymentAmount());
                }
            }
            homeDTO.setPaymentOther(paymentOther);
            //认领收款
            BigDecimal applyCollectionAmount = new BigDecimal(0);
            //查询认领金额
            QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
            historyQueryWrapper.eq("order_id", baContractStart.getId());
            List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
            for (BaClaimHistory baClaimHistory : baClaimHistories) {
                //查询认领信息
                BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                if(!ObjectUtils.isEmpty(baClaim)) {
                    BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                    //判断认领收款审批通过
                    if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                        if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                            applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                        }
                    }
                }
            }
            //认领收款
            homeDTO.setApplyCollectionAmount(applyCollectionAmount);
            //收票添加
            //已收票金额
            BigDecimal inVoIcePic = new BigDecimal(0);
            QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("start_id", baContractStart.getId());
            invoiceQueryWrapper.eq("invoice_type", 2);
            invoiceQueryWrapper.eq("state", "inputInvoice:pass");
            invoiceQueryWrapper.eq("flag", 0);
            List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            for (BaInvoice baInvoice : baInvoices) {
                if (StringUtils.isNotNull(baInvoice)) {
                    QueryWrapper<BaInvoiceDetail> detailQueryWrapper = new QueryWrapper<>();
                    detailQueryWrapper.eq("invoice_id", baInvoice.getId());
                    //detailQueryWrapper.eq("flag",0);
                    List<BaInvoiceDetail> baInvoiceDetails = baInvoiceDetailMapper.selectList(detailQueryWrapper);
                    for (BaInvoiceDetail baInvoiceDetail : baInvoiceDetails) {
                        if (StringUtils.isNotNull(baInvoiceDetail)) {
                            if (StringUtils.isNotNull(baInvoiceDetail.getTaxPriceTotal())) {
                                inVoIcePic = inVoIcePic.add(baInvoiceDetail.getTaxPriceTotal());
                            }
                        }
                    }
                }
            }
            homeDTO.setInVoIcePic(inVoIcePic);
            homeDTO.setInVoIce(baInvoices.size());
            //开票申请
            BigDecimal invoicedQuantityPic = new BigDecimal(0);
            invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("start_id", baContractStart.getId());
            invoiceQueryWrapper.eq("invoice_type", 1);
            invoiceQueryWrapper.eq("state", "invoice:pass");
            invoiceQueryWrapper.eq("flag", 0);
            baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            for (BaInvoice baInvoice : baInvoices) {
                if (StringUtils.isNotNull(baInvoice)) {
                    invoicedQuantityPic = invoicedQuantityPic.add(baInvoice.getInvoicedAmount());
                }
            }
            homeDTO.setInvoicedQuantityPic(invoicedQuantityPic);
            homeDTO.setInvoicedQuantity(baInvoices.size());
            //利润测算
            if (baContractStart.getCurrentSettlementDifference() != null) {
                if (baContractStart.getCurrentSettlementDifference().compareTo(BigDecimal.ZERO) == 0) {
                    homeDTO.setProfitCalculation(homeDTO.getInvoicedQuantityPic().subtract(homeDTO.getInVoIcePic()));
                }
            }
            baContractStart.setHomeDTO(homeDTO);
            if(StringUtils.isNotEmpty(baContractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baContractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    baContractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
            //主体公司
            if (StringUtils.isNotEmpty(baContractStart.getSubjectCompany())) {
                if(this.isNumeric(baContractStart.getSubjectCompany()) == true){
                    //子公司
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baContractStart.getSubjectCompany()));
                    if (!ObjectUtils.isEmpty(sysDept)) {
                        baContractStart.setSubjectCompanyName(sysDept.getDeptName());
                    }
                }else {
                    baContractStart.setSubjectCompanyName(baContractStart.getSubjectCompany());
                }
            }
            startList.add(baContractStart);
        }

        return startList;
    }

    @Override
    @DataScope(deptAlias = "c", userAlias = "c")
    public List<BaContractStart> selectOrderList(BaContractStart baContractStart) {
        List<BaContractStart> contractStarts = baContractStartMapper.selectOrderList(baContractStart);
        for (BaContractStart contractStart : contractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }

            //品类
            if (StringUtils.isNotEmpty(contractStart.getGoodsType())) {
                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(contractStart.getGoodsType());
                if(StringUtils.isNotNull(baGoodsType)){
                    contractStart.setGoodsType(baGoodsType.getName());
                }else {
                    BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(contractStart.getGoodsType());
                    if(!ObjectUtils.isEmpty(baGoods1)){
                        contractStart.setGoodsType(baGoods1.getName());
                    }
                }
            }
            //品名
            if(StringUtils.isNotEmpty(contractStart.getGoodsId())){
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(contractStart.getGoodsId());
                contractStart.setAppGoodsName(baGoods.getName());
            }

            if(StringUtils.isNotEmpty(contractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    contractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
        }
        return contractStarts;
    }

    @Override
    public int insertOrder(BaContractStart baContractStart) {
        baContractStart.setId(getRedisIncreID.getId());
        baContractStart.setCreateTime(DateUtils.getNowDate());
        baContractStart.setCreateBy(SecurityUtils.getUsername());
        baContractStart.setUserId(SecurityUtils.getUserId());
        baContractStart.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baContractStart.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baContractStart.getWorkState())){
            //流程发起状态
            baContractStart.setWorkState("1");
            //默认审批流程已通过
            baContractStart.setState(AdminCodeEnum.ORDER_STATUS_PASS.getCode());
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(),SecurityUtils.getCurrComId());
        baContractStart.setFlowId(flowId);
        int result = baContractStartMapper.insertBaContractStart(baContractStart);
        if (result > 0) {
            //判断业务归属公司是否是上海子公司
            if("2".equals(baContractStart.getWorkState())) {
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstancesOrder(contractStart, AdminCodeEnum.ORDER_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    deleteBaContractStartById(baContractStart.getId());
                    return 0;
                } else {
                    contractStart.setState(AdminCodeEnum.ORDER_STATUS_VERIFYING.getCode());
                    contractStart.setContractStatus("1");
                    baContractStartMapper.updateBaContractStart(contractStart);
                }
            }
        }
        return result;
    }

    @Override
    public int updateOrder(BaContractStart baContractStart) {
        //原数据
        BaContractStart start = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
        baContractStart.setUpdateTime(DateUtils.getNowDate());
        baContractStart.setUpdateBy(SecurityUtils.getUsername());
        baContractStart.setCreateTime(baContractStart.getUpdateTime());
        int result = baContractStartMapper.updateBaContractStart(baContractStart);
        if (result > 0) {
            BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", contractStart.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstancesOrder(contractStart, AdminCodeEnum.ORDER_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                //流程启动失败回滚原数据
                baContractStartMapper.updateBaContractStart(start);
                return 0;
            } else {
                contractStart.setState(AdminCodeEnum.ORDER_STATUS_VERIFYING.getCode());
                baContractStartMapper.updateBaContractStart(contractStart);
            }
        }
        return result;
    }

    @Override
    public int associatedBusiness(BaContractStart baContractStart) {
        if (StringUtils.isNotEmpty(baContractStart.getId())) {
            BaContractStart baContractStart1 = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
            baContractStart1.setContractId(baContractStart.getContractId());
            baContractStartMapper.updateBaContractStart(baContractStart1);
        } else {
            return 0;
        }
        return 1;
    }

    @Override
    public int revokeOrder(String id) {
        //判断id不为空
        if (StringUtils.isNotEmpty(id)) {
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baContractStart.getId());
            queryWrapper.eq("flag", 0);
            queryWrapper.ne("approve_result", AdminCodeEnum.ORDER_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baContractStart.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result = workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if (result.get("code").toString().equals("200")) {
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baContractStart.setState(AdminCodeEnum.ORDER_STATUS_WITHDRAW.getCode());
                baContractStartMapper.updateBaContractStart(baContractStart);
                String userId = String.valueOf(baContractStart.getUserId());
                //给所有已审批的用户发消息
                if (!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if (!ObjectUtils.isEmpty(ajaxResult)) {
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for (LinkedHashMap map : data) {
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                                        if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                            this.insertMessage(baContractStart, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int complete(String id) {
        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(id);
        baContractStart.setContractStatus("3");
        baContractStart.setCompletion(1L);
        return baContractStartMapper.updateBaContractStart(baContractStart);
    }

    @Override
    public List<SysUser> deptUser() {
        SysUser sysUser = new SysUser();
        //用户组合
        //sysUser.setDeptId(SecurityUtils.getDeptId());
        sysUser.setStatus("0");
        //查询该部门下用户
        List<SysUser> sysUsers = sysUserMapper.selectRoleUsersByRoleId(SecurityUtils.getCurrComId());
        return sysUsers;
    }

    @Override
    public Map<String, BigDecimal> businessCount(String startId) {
        //List<Map<String,BigDecimal>> list =new ArrayList<>();
        Map<String, BigDecimal> map = new HashMap<>();
        //查看发运信息
        QueryWrapper<BaTransport> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("start_id", startId);
        queryWrapper.eq("flag", 0);
        List<BaTransport> baTransports = baTransportMapper.selectList(queryWrapper);
        //矿发煤量
        BigDecimal transit = new BigDecimal(0);
        for (BaTransport baTransport : baTransports) {
            //查询矿发煤量
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id", baTransport.getId());
            checkQueryWrapper.eq("type", 1);
            BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
            if (StringUtils.isNotNull(baCheck)) {
                //运输中
                transit = transit.add(baCheck.getCheckCoalNum());
            }
        }
        map.put("发运信息", transit);
        //list.add(map);
        //结算单数量
        QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
        settlementQueryWrapper.eq("contract_id", startId);
        settlementQueryWrapper.eq("state", "settlement:pass");
        settlementQueryWrapper.eq("flag", 0);
        List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
        map.put("结算信息", BigDecimal.valueOf(baSettlementList.size()));
        //list.add(map);
        //付款信息
        BigDecimal paymentAmount = new BigDecimal(0);
        QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
        paymentQueryWrapper.eq("start_id", startId);
        paymentQueryWrapper.eq("state", "payment:pass");
        paymentQueryWrapper.eq("flag", 0);
        List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
        for (BaPayment baPayment : baPayments) {
            if (StringUtils.isNotNull(baPayment)) {
                paymentAmount = paymentAmount.add(baPayment.getApplyPaymentAmount());
            }
        }
        map.put("付款信息", paymentAmount);
        //list.add(map);
        //认领收款
        BigDecimal applyCollectionAmount = new BigDecimal(0);
        //查询认领金额
        QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
        historyQueryWrapper.eq("order_id", startId);
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
        for (BaClaimHistory baClaimHistory : baClaimHistories) {
            //查询认领信息
            BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
            if(StringUtils.isNotNull(baClaim)){
                BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                    if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                        applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                    }
                }
            }
            //判断认领收款审批通过
        }
        map.put("认领收款", applyCollectionAmount);
        //list.add(map);
        //销项发票
        BigDecimal invoicedQuantityPic = new BigDecimal(0);
        QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("start_id", startId);
        invoiceQueryWrapper.eq("invoice_type", 1);
        invoiceQueryWrapper.eq("state", "invoice:pass");
        invoiceQueryWrapper.eq("flag", 0);
        List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
        for (BaInvoice baInvoice : baInvoices) {
            if (StringUtils.isNotNull(baInvoice)) {
                invoicedQuantityPic = invoicedQuantityPic.add(baInvoice.getInvoicedAmount());
            }
        }
        map.put("销项发票", invoicedQuantityPic);
        //list.add(map);
        //进项发票
        BigDecimal inVoIcePic = new BigDecimal(0);
        invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("start_id", startId);
        invoiceQueryWrapper.eq("invoice_type", 2);
        invoiceQueryWrapper.eq("state", "inputInvoice:pass");
        invoiceQueryWrapper.eq("flag", 0);
        baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
        for (BaInvoice baInvoice : baInvoices) {
            if (StringUtils.isNotNull(baInvoice)) {
                QueryWrapper<BaInvoiceDetail> detailQueryWrapper = new QueryWrapper<>();
                detailQueryWrapper.eq("invoice_id", baInvoice.getId());
                //detailQueryWrapper.eq("flag",0);
                List<BaInvoiceDetail> baInvoiceDetails = baInvoiceDetailMapper.selectList(detailQueryWrapper);
                for (BaInvoiceDetail baInvoiceDetail : baInvoiceDetails) {
                    if (StringUtils.isNotNull(baInvoiceDetail)) {
                        if(baInvoiceDetail.getInvoicedAmount()!=null){
                            inVoIcePic = inVoIcePic.add(baInvoiceDetail.getInvoicedAmount());
                        }
                    }
                }
            }
        }
        map.put("进项发票", inVoIcePic);
        //list.add(map);
        return map;
    }

    @Override
    public UR homeStartStat(CountContractStartDTO countContractStartDTO) {
        //供货中
        Integer startList = 0;
        //待结算
        Integer list = 0;
        //待回款
        Integer starts = 0;
        //已完结
        Integer arrayList = 0;
        //全部
        Integer allList = 0;
        //租户ID
        countContractStartDTO.setTenantId(SecurityUtils.getCurrComId());
        List<CountContractStartDTO> countContractStartDTO1 = baContractStartMapper.contractStarCountByDeptId(countContractStartDTO);
        for (CountContractStartDTO contractStartDTO:countContractStartDTO1) {
            if("1".equals(contractStartDTO.getStartType())){
                startList = Integer.valueOf(contractStartDTO.getCounting());
            }
            if("2".equals(contractStartDTO.getStartType())){
                list = Integer.valueOf(contractStartDTO.getCounting());
            }
            if("3".equals(contractStartDTO.getStartType())){
                starts = Integer.valueOf(contractStartDTO.getCounting());
            }
            if("4".equals(contractStartDTO.getStartType())){
                arrayList = Integer.valueOf(contractStartDTO.getCounting());
            }
        }
        allList = startList + list + starts + arrayList;
        return UR.ok().data("供货中", startList).data("待结算", list).data("待回款", starts).data("已完结", arrayList).data("全部", allList);
    }

    @Override
    public UR contractStartListStat(BaContractStartDTO baContractStartDTO) {
        //供货中
        List<BaContractStart> startList = new ArrayList<>();
        //待结算
        List<BaContractStart> list = new ArrayList<>();
        //待回款
        List<BaContractStart> starts = new ArrayList<>();
        //已完结
        List<BaContractStart> arrayList = new ArrayList<>();
        //所有
        List<BaContractStart> allList = new ArrayList<>();

        QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(baContractStartDTO.getName())) {
            startQueryWrapper.like("name", baContractStartDTO.getName());
        }
        startQueryWrapper.eq("state", "contractStart:pass");
        startQueryWrapper.eq("flag", 0);
        if (!"all".equals(baContractStartDTO.getType())) {
            startQueryWrapper.eq("start_type", baContractStartDTO.getType());
        }
        startQueryWrapper.orderByDesc("create_time");
        List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
        for (BaContractStart baContractStart : baContractStartList) {
            if (StringUtils.isNotEmpty(baContractStart.getStartType())) {
                if (baContractStart.getStartType().equals("1")) {
                    startList.add(baContractStart);
                }
                if (baContractStart.getStartType().equals("2")) {
                    list.add(baContractStart);
                }
                if (baContractStart.getStartType().equals("3")) {
                    starts.add(baContractStart);
                }
                if (baContractStart.getStartType().equals("4")) {
                    arrayList.add(baContractStart);
                }
                allList.add(baContractStart);
            }
        }

        return UR.ok().data("供货中", startList).data("待结算", list).data("待回款", starts).data("已完结", arrayList).data("所有",allList);
    }

    @Override
    public List<BaContractStart> homeContractStartList(BaContractStartDTO baContractStartDTO) {
        List<BaContractStart> list = new ArrayList<>();

        QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
        //累计运输量
        BigDecimal checkCoalNum = null;
        //昕科累计运单量
        BigDecimal ccjweightTotal = null;
        //中储累计运单量
        BigDecimal weightTotal = null;
        //合同额
        BigDecimal contractAmount = null;
        //已收款
        BigDecimal collectionAmount = null;
        //将元变为万元
        BigDecimal bigDecimal = new BigDecimal( "10000");
        //累计发运量
        //有问题字段
        List<String> isChangeList = null;
        BaContractStart baContractStart1 = new BaContractStart();
        baContractStart1.setState("contractStart:pass");
        baContractStart1.setFlag(new Long(0));
        //baContractStart1.setType(baContractStartDTO.getType());
        baContractStart1.setDeptId(baContractStartDTO.getDeptId());
        baContractStart1.setProjectType(baContractStartDTO.getProjectType());
        baContractStart1.setBusinessType(baContractStartDTO.getBusinessType());
        baContractStart1.setStartType(baContractStartDTO.getType());
        startQueryWrapper.orderByDesc("create_time");
        List<BaContractStart> baContractStartList = baContractStartMapper.selectContractStartList(baContractStart1);
        for (BaContractStart baContractStart : baContractStartList) {
            //岗位信息
            String name = getName.getName(baContractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName());
            }
            //累计运输量
            checkCoalNum = new BigDecimal(0);
            //合同额
            contractAmount = new BigDecimal(0);
            //已收款
            collectionAmount = new BigDecimal(0);
            //有问题字段
            isChangeList = new ArrayList<>();
            //查询计划发运量
            BaBidTransport baBidTransport = new BaBidTransport();
            baBidTransport.setStartId(baContractStart.getId());
            //累计发运量
            BigDecimal saleNum = baBidTransportMapper.baBidTransportSum(baBidTransport);
            baContractStart.setSaleNum(saleNum); //累计计划发运量
//
            //判断问题合同启动
            //当前日期>履约截止日期，且合同在供货中或待结算状态，视为问题合同
            Date nowDate = DateUtils.getNowDate();
            if (baContractStart.getPerformanceEndTime() != null && nowDate.after(baContractStart.getPerformanceEndTime())
                    && ("1".equals(baContractStart.getStartType()) || "2".equals(baContractStart.getStartType()))) {
                baContractStart.setHasProblem(true); //标识合同启动有问题
                isChangeList.add("performanceEndTime");
            }

            //合同额（万元）
            if (baContractStart.getPrice() != null && baContractStart.getTonnage() != null) {
                contractAmount = contractAmount.add(baContractStart.getPrice().multiply(baContractStart.getTonnage())
                        .setScale(2, BigDecimal.ROUND_HALF_UP)).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                //发运吨数(吨)
                baContractStart.setContractAmount(contractAmount);
            }

            //累计运输量
//            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
//            transportQueryWrapper.eq("start_id", baContractStart.getId());
//            transportQueryWrapper.eq("flag", 0);
//            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
//            for (BaTransport baTransport : baTransports) {
//                //验收信息
//                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
//                checkQueryWrapper.eq("relation_id", baTransport.getId());
//                checkQueryWrapper.eq("type", 1);
//                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
//                for (BaCheck baCheck : baChecks) {
//                    checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
//                }
//                //无车承运货源数据（昕科）
//                QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
//                automobileQueryWrapper.eq("order_id", baTransport.getId());
//                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
//                if (StringUtils.isNotNull(baTransportAutomobile)) {
//                    //查询运单(昕科)
//                    QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
//                    orderQueryWrapper.eq("supplier_no", baTransportAutomobile.getId());
//                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
//                    for (BaShippingOrder baShippingOrder : shippingOrders) {
//                        if (baShippingOrder.getCcjweight() != null) {
//                            //昕科装车重量累计
//                            ccjweightTotal = ccjweightTotal.add(baShippingOrder.getCcjweight());
//                        }
//                    }
//                }
//                //无车承运货源数据（中储）
//                QueryWrapper<BaTransportCmst> cmstQueryWrapper = new QueryWrapper<>();
//                cmstQueryWrapper.eq("relation_id", baTransport.getId());
//                BaTransportCmst transportCmst = baTransportCmstMapper.selectOne(cmstQueryWrapper);
//                if (StringUtils.isNotNull(transportCmst)) {
//                    //查询运单（中储）
//                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
//                    baShippingOrderCmst1.setYardId(transportCmst.getId());
//                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst1);
//                    for (BaShippingOrderCmst baShippingOrderCmst : baShippingOrderCmsts) {
//                        if (baShippingOrderCmst.getConfirmedDeliveryWeight() != null) {
//                            //中储装车累计重量
//                            weightTotal = weightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
//                        }
//                    }
//                }
//            }

            BaTransport baTransport = new BaTransport();
            baTransport.setStartId(baContractStart.getId());
            baTransport.setTenantId(SecurityUtils.getCurrComId());
            baTransport.setType("1");
            BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
            if(!ObjectUtils.isEmpty(baTransport1)){
                if(baTransport1.getCheckCoalNum() != null){
                    checkCoalNum.add(baTransport1.getCheckCoalNum());
                }
                if(baTransport1.getCcjweight() != null){
                    checkCoalNum.add(baTransport1.getCcjweight());
                }
                if(baTransport1.getConfirmedDeliveryWeight() != null){
                    checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                }
            }
            //发运吨数(吨)
            baContractStart.setCheckCoalNum(checkCoalNum);

            //计算已收款
//            QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
//            settlementQueryWrapper.eq("flag",0);
//            settlementQueryWrapper.eq("settlement_type", 2);//销售结算单
//            settlementQueryWrapper.eq("contract_id",baContractStart.getId());
//            List<BaSettlement> settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
//            if(!CollectionUtils.isEmpty(settlementList)){
//                for(BaSettlement baSettlement : settlementList){
//                    //收款信息
//                    QueryWrapper<BaCollection> collectionQueryWrapper = new QueryWrapper<>();
//                    collectionQueryWrapper.eq("settlement_id",baSettlement.getId());
//                    collectionQueryWrapper.eq("flag",0);
//                    List<BaCollection> baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
//                    for (BaCollection baCollection : baCollectionList) {
//                        if (baCollection.getCollectionState().equals(AdminCodeEnum.COLLECTION_RECEIVED.getCode())) {
//                            collectionAmount = collectionAmount.add(baCollection.getActualCollectionAmount());
//                        }
//                    }
//                }
//            }
            BaSettlement baSettlement = new BaSettlement();
            baSettlement.setFlag(0);
            baSettlement.setSettlementType(new Long(2)); //销售结算单
            baSettlement.setContractId(baContractStart.getId());
            baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
            //租户ID
            baSettlement.setTenantId(SecurityUtils.getCurrComId());
            collectionAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);

//          //累计付款
//            BigDecimal paymentAmount = new BigDecimal(0);
//            QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
//            paymentQueryWrapper.eq("start_id",baContractStart.getId());
//            paymentQueryWrapper.eq("state","payment:pass");
//            paymentQueryWrapper.eq("flag",0);
//            List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
//            for (BaPayment baPayment:baPayments) {
//                if(StringUtils.isNotNull(baPayment)){
//                    paymentAmount = paymentAmount.add(baPayment.getApplyPaymentAmount());
//                }
//            }
            BaPayment baPayment = new BaPayment();
            baPayment.setStartId(baContractStart.getId());
            baPayment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
            BigDecimal paymentAmount = baPaymentMapper.sumBaSettlementAndPayment(baPayment);
            baContractStart.setPaymentAmount(paymentAmount);
            //①当前发运数量>合同签约数量或计划发运量，且合同在供货中或待结算状态
            //②当前发运数量<合同签约数量或计划发运量，且合同在待结算状态
            if((checkCoalNum != null && baContractStart.getTonnage() != null && checkCoalNum.compareTo(baContractStart.getTonnage()) > 0)
                    || (checkCoalNum != null && baContractStart.getSaleNum() != null && checkCoalNum.compareTo(baContractStart.getSaleNum()) > 0) &&
                    ("1".equals(baContractStart.getStartType()) || "2".equals(baContractStart.getStartType()))){
                baContractStart.setHasProblem(true); //标识合同启动有问题
                isChangeList.add("checkCoalNum");
            } else if((checkCoalNum != null && baContractStart.getTonnage() != null && checkCoalNum.compareTo(baContractStart.getTonnage()) < 0)
                    || (checkCoalNum != null && baContractStart.getSaleNum() != null && checkCoalNum.compareTo(baContractStart.getSaleNum()) < 0) &&
                    ("2".equals(baContractStart.getStartType()))){
                baContractStart.setHasProblem(true); //标识合同启动有问题
                isChangeList.add("checkCoalNum");
            }
            //累计已收款
            if(collectionAmount != null){
                baContractStart.setCollectionAmount(collectionAmount.divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros());
            }

//            QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
//            invoiceQueryWrapper.eq("start_id", baContractStart.getId());
//            invoiceQueryWrapper.eq("state", "inputInvoice:pass");
//            invoiceQueryWrapper.eq("flag", 0);
//            List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
//            for (BaInvoice baInvoice : baInvoices) {
//                if (StringUtils.isNotNull(baInvoice)) {
//                    QueryWrapper<BaInvoiceDetail> detailQueryWrapper = new QueryWrapper<>();
//                    detailQueryWrapper.eq("invoice_id", baInvoice.getId());
//                    List<BaInvoiceDetail> baInvoiceDetails = baInvoiceDetailMapper.selectList(detailQueryWrapper);
//                    for (BaInvoiceDetail baInvoiceDetail : baInvoiceDetails) {
//                        if (StringUtils.isNotNull(baInvoiceDetail)) {
//                            if("1".equals(baInvoice.getInvoiceType())){
//                                inVoIcePicOut = inVoIcePicOut.add(baInvoiceDetail.getTaxPriceTotal());
//                            } else {
//                                inVoIcePicIn = inVoIcePicIn.add(baInvoiceDetail.getTaxPriceTotal());
//                            }
//                        }
//                    }
//                }
//            }
            //销项发票已收票金额（万元）
            BigDecimal inVoIcePicOut = new BigDecimal(0);
            BaInvoice baInvoice = new BaInvoice();
            baInvoice.setStartId(baContractStart.getId());
            baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
            baInvoice.setInvoiceType("1"); //销售发票 2:采购发票
            //租户ID
            baInvoice.setTenantId(SecurityUtils.getCurrComId());
            inVoIcePicOut = baInvoiceMapper.sumBaInvoice(baInvoice);
            if(inVoIcePicOut != null){
                baContractStart.setInVoIcePic(inVoIcePicOut.divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros());
            }

            //进项发票已收票金额（万元）
            BigDecimal inVoIcePicIn = new BigDecimal(0);
            baInvoice = new BaInvoice();
            baInvoice.setStartId(baContractStart.getId());
            baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
            baInvoice.setInvoiceType("2"); //采购发票
            //租户ID
            baInvoice.setTenantId(SecurityUtils.getCurrComId());
            inVoIcePicIn = baInvoiceMapper.sumBaInvoice(baInvoice);

//            //进项发票总额 ≠付款总额（包含货款、运费、服务费、保证金、其他）；且合同在待结算状态
            if((inVoIcePicIn != null && baContractStart.getPaymentAmount() != null
                    && baContractStart.getPaymentAmount().compareTo(inVoIcePicIn) != 0) &&
                    "2".equals(baContractStart.getStartType())){
                baContractStart.setHasProblem(true); //标识合同启动有问题
                isChangeList.add("paymentAmount");
            }

            //已收款金额 ≠ 销项发票金额，且合同在待结算状态，
            if((inVoIcePicOut != null && baContractStart.getCollectionAmount() != null
                    && inVoIcePicOut.compareTo(baContractStart.getCollectionAmount()) != 0) &&
                    "2".equals(baContractStart.getStartType())){
                baContractStart.setHasProblem(true); //标识合同启动有问题
                isChangeList.add("collectionAmount");
            }
            //结余为负数，下游回款总金额<上游结算总金额，且合同在待回款或待结算状态
            if((baContractStart.getCollectionAmount() != null
                    && baContractStart.getPaymentAmount() != null
                    && baContractStart.getCollectionAmount().compareTo(baContractStart.getPaymentAmount()) < 0) &&
                    ("2".equals(baContractStart.getStartType()) || "3".equals(baContractStart.getStartType()))){
                baContractStart.setHasProblem(true); //标识合同启动有问题
            }

            //过滤掉有问题的合同启动
            if("2".equals(baContractStartDTO.getProblemType())){
                if(!baContractStart.isHasProblem()){
                    list.add(baContractStart);
                }
            } else if("1".equals(baContractStartDTO.getProblemType())){
                if(baContractStart.isHasProblem()){
                    list.add(baContractStart);
                }
            } else {
                list.add(baContractStart);
            }
        }

        return list;
    }

    @Override
    public List<BaContractStart> homeContractStart(BaContractStartDTO baContractStartDTO) {
        List<BaContractStart> list = new ArrayList<>();

        //累计运输量
        BigDecimal checkCoalNum = null;
        //合同额
        BigDecimal contractAmount = null;
        //已收款
        BigDecimal collectionAmount = null;
        //将元变为万元
        BigDecimal bigDecimal = new BigDecimal( "10000");
        BaContractStart baContractStart1 = new BaContractStart();
        baContractStart1.setState("contractStart:pass");
        baContractStart1.setFlag(new Long(0));
        //baContractStart1.setType(baContractStartDTO.getType());
        baContractStart1.setDeptId(baContractStartDTO.getDeptId());
        baContractStart1.setProjectType(baContractStartDTO.getProjectType());
        baContractStart1.setBusinessType(baContractStartDTO.getBusinessType());
        if(!"all".equals(baContractStartDTO.getType())){
            baContractStart1.setStartType(baContractStartDTO.getType());
        }
        //常规合同启动
        baContractStart1.setType("5");
        baContractStart1.setProblemMark(baContractStartDTO.getProblemMark());
        baContractStart1.setName(baContractStartDTO.getName());
        //租户ID
        baContractStart1.setTenantId(SecurityUtils.getCurrComId());
        List<BaContractStart> baContractStartList = baContractStartMapper.selectContractStartList(baContractStart1);
        for (BaContractStart baContractStart : baContractStartList) {
            //岗位信息
            String name = getName.getName(baContractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                baContractStart.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName());
            }
            //业务归属公司
            if(StringUtils.isNotEmpty(baContractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baContractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    baContractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
            //指标信息
            if (StringUtils.isNotEmpty(baContractStart.getIndicatorInformation())) {
                BaEnterpriseRelevance baEnterpriseRelevance = baEnterpriseRelevanceService.selectBaEnterpriseRelevanceById(baContractStart.getIndicatorInformation());
                baContractStart.setBaEnterpriseRelevance(baEnterpriseRelevance);
            }
            //累计运输量
            checkCoalNum = new BigDecimal(0);
            //合同额
            contractAmount = new BigDecimal(0);
            //已收款
            collectionAmount = new BigDecimal(0);
            //查询计划发运量
            BaBidTransport baBidTransport = new BaBidTransport();
            baBidTransport.setStartId(baContractStart.getId());
            //累计发运量
            BigDecimal saleNum = baBidTransportMapper.baBidTransportSum(baBidTransport);
            baContractStart.setSaleNum(saleNum); //累计计划发运量
            //合同额（万元）
            if (baContractStart.getPrice() != null && baContractStart.getTonnage() != null) {
                contractAmount = baContractStart.getPrice().multiply(baContractStart.getTonnage());
                //合同额（万元）
                baContractStart.setContractAmount(contractAmount);
            }

            BaTransport baTransport = new BaTransport();
            baTransport.setStartId(baContractStart.getId());
            baTransport.setTenantId(SecurityUtils.getCurrComId());
            baTransport.setType("1");
            BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
            if(!ObjectUtils.isEmpty(baTransport1)){
                if(baTransport1.getCheckCoalNum() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
                }
                if(baTransport1.getCcjweight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
                }
                if(baTransport1.getConfirmedDeliveryWeight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                }
            }
            //发运吨数(吨)
            baContractStart.setCheckCoalNum(checkCoalNum);
            //下游结算统计
           /* BaSettlement baSettlement = new BaSettlement();
            baSettlement.setFlag(0);
            baSettlement.setSettlementType(new Long(2)); //销售结算单
            baSettlement.setContractId(baContractStart.getId());
            baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
            collectionAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);*/

            collectionAmount = baClaimHistoryMapper.baClaimHistoryCount(baContractStart.getId(),SecurityUtils.getCurrComId());

            BaPayment baPayment = new BaPayment();
            baPayment.setStartId(baContractStart.getId());
            baPayment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
            BigDecimal paymentAmount = baPaymentMapper.sumBaSettlementAndPayment(baPayment);
            baContractStart.setPaymentAmount(paymentAmount);

            //累计已收款
            if(collectionAmount != null){
                baContractStart.setCollectionAmount(collectionAmount);
            }

            //销项发票已收票金额（万元）
            BigDecimal inVoIcePicOut = new BigDecimal(0);
            BaInvoice baInvoice = new BaInvoice();
            baInvoice.setStartId(baContractStart.getId());
            baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
            baInvoice.setInvoiceType("1"); //销售发票 2:采购发票
            //租户ID
            baInvoice.setTenantId(SecurityUtils.getCurrComId());
            inVoIcePicOut = baInvoiceMapper.sumBaInvoice(baInvoice);
            /*if(inVoIcePicOut != null){
                baContractStart.setInVoIcePic(inVoIcePicOut.divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros());
            }*/

            //进项发票已收票金额（万元）
            BigDecimal inVoIcePicIn = new BigDecimal(0);
            baInvoice = new BaInvoice();
            baInvoice.setStartId(baContractStart.getId());
            baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
            baInvoice.setInvoiceType("2"); //采购发票
            //租户ID
            baInvoice.setTenantId(SecurityUtils.getCurrComId());
            inVoIcePicIn = baInvoiceMapper.sumBaInvoice(baInvoice);
            if(inVoIcePicIn != null){
                baContractStart.setInVoIcePic(inVoIcePicIn);
            }


            list.add(baContractStart);
        }

        return list;
    }

    @Override
    public List<BaContractStart> contractLaunchPanel(BaContractStartDTO baContractStartDTO) {

        List<BaContractStart> list = new ArrayList<>();
        //合同启动查看
        BaContractStart baContractStart1 = new BaContractStart();
        baContractStart1.setState("contractStart:pass");
        baContractStart1.setFlag(new Long(0));
        baContractStart1.setDeptId(baContractStartDTO.getDeptId());
        baContractStart1.setProjectType(baContractStartDTO.getProjectType());
        baContractStart1.setBusinessType(baContractStartDTO.getBusinessType());
        if(!"all".equals(baContractStartDTO.getType())){
            baContractStart1.setStartType(baContractStartDTO.getType());
        }
        //常规合同启动
        baContractStart1.setType("5");
        baContractStart1.setProblemMark(baContractStartDTO.getProblemMark());
        baContractStart1.setName(baContractStartDTO.getName());
        //租户ID
        baContractStart1.setTenantId(SecurityUtils.getCurrComId());
        baContractStart1.setProjectId(baContractStartDTO.getProjectId());
        List<BaContractStart> baContractStartList = baContractStartMapper.selectContractStartList(baContractStart1);
        for (BaContractStart baContractStart : baContractStartList) {
            //部门名称
            SysDept dept = sysDeptMapper.selectDeptById(baContractStart.getDeptId());
            //用户名称
            SysUser user = sysUserMapper.selectUserById(baContractStart.getUserId());
            if(StringUtils.isNotNull(dept) && StringUtils.isNotNull(user)){
                baContractStart.setPartDeptName(user.getNickName() + "-" + dept.getDeptName());
            }
            //运输信息
            QueryWrapper<BaBidTransport> baBidTransportQueryWrapper = new QueryWrapper<>();
            baBidTransportQueryWrapper.eq("start_id", baContractStart.getId());
            baBidTransportQueryWrapper.eq("flag", 0);
            List<BaBidTransport> baBidTransports = baBidTransportMapper.selectList(baBidTransportQueryWrapper);
            //运输方式
            String transportMode = "";
            if(baBidTransports.size() > 0){
                for (BaBidTransport baBidTransport:baBidTransports) {
                    //查询合同启动类型
                    List<SysDictData> dictData = sysDictDataService.selectDictDataByType("transport_type");
                    for (SysDictData sysDictData:dictData) {
                        if(sysDictData.getDictValue().equals(baBidTransport.getTransportType())){
                            transportMode = sysDictData.getDictLabel() + "," + transportMode;
                        }
                    }
                }
            }
            baContractStart.setTransportWay(transportMode);
            //合同周期(下游合同)
            QueryWrapper<BaContract> contractQueryWrapper = new QueryWrapper<>();
            contractQueryWrapper.like("start_id", baContractStart.getId());
            contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            contractQueryWrapper.eq("contract_type", 7);
            contractQueryWrapper.eq("signing_status","已签");
            List<BaContract> baContracts = baContractMapper.selectList(contractQueryWrapper);
            if(baContracts.size() > 0){
                baContractStart.setDownstreamStartTime(baContracts.get(0).getStartTime());
                baContractStart.setDownstreamEndTime(baContracts.get(0).getEndTime());
                //合同吨数
                if(baContracts.get(0).getContractTotal() != null){
                    baContractStart.setTonnage(baContracts.get(0).getContractTotal());
                }
            }
            //累计运输量
            BigDecimal checkCoalNum = new BigDecimal(0);
            //发运吨数
            BaTransport baTransport = new BaTransport();
            baTransport.setStartId(baContractStart.getId());
            baTransport.setTenantId(SecurityUtils.getCurrComId());
            baTransport.setType("1");
            BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
            if(!ObjectUtils.isEmpty(baTransport1)){
                if(baTransport1.getCheckCoalNum() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
                }
                if(baTransport1.getCcjweight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
                }
                if(baTransport1.getConfirmedDeliveryWeight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                }
            }
            //发运吨数(吨)
            baContractStart.setCheckCoalNum(checkCoalNum);
            //进场吨数
            checkCoalNum = new BigDecimal(0);
            baTransport = new BaTransport();
            baTransport.setStartId(baContractStart.getId());
            baTransport.setTenantId(SecurityUtils.getCurrComId());
            //已验收状态
            baTransport.setTransportStatus("all");
            baTransport.setType("2");
            baTransport1 = baTransportMapper.selectTransportSum(baTransport);
            if(!ObjectUtils.isEmpty(baTransport1)){
                if(baTransport1.getCheckCoalNum() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
                }
                if(baTransport1.getCcjweight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
                }
                if(baTransport1.getConfirmedDeliveryWeight() != null){
                    checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                }
            }
            //进场吨数
            baContractStart.setEntryTonnage(checkCoalNum);
            //完成合同时间(拿发运的验收吨数对比合同吨数取达成合同吨数的那一次发运的发运创建时间)
            if(baContractStart.getTonnage() != null){
                int compareTo = baContractStart.getTonnage().compareTo(baContractStart.getEntryTonnage());
                if(compareTo < 0 || compareTo == 0){
                    baTransport = new BaTransport();
                    baTransport.setStartId(baContractStart.getId());
                    baTransport.setConfirmStatus("all");
                    List<BaTransport> baTransports = baTransportMapper.selectTransport(baTransport);
                    //最后一次发运时间
                    baContractStart.setTransportDate(baTransports.get(0).getTransportDate());
                }
            }
            //下游结算数量
            BigDecimal downstreamSettlementNum = new BigDecimal(0);
            //下游结算金额
            BigDecimal downstreamSettlementAmount = new BigDecimal(0);
            QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
            settlementQueryWrapper.eq("contract_id", baContractStart.getId());
            settlementQueryWrapper.eq("state", "settlement:pass");
            settlementQueryWrapper.eq("flag", 0);
            settlementQueryWrapper.eq("settlement_type", 2);
            List<BaSettlement> settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
            for (BaSettlement baSettlement : settlementList) {
                if (StringUtils.isNotNull(baSettlement)) {
                    if (baSettlement.getSettlementNum() != null) {
                        downstreamSettlementNum = downstreamSettlementNum.add(baSettlement.getSettlementNum());
                    } else {
                        downstreamSettlementNum = downstreamSettlementNum.add(new BigDecimal(0));
                    }
                    if (baSettlement.getSettlementAmount() != null) {
                        downstreamSettlementAmount = downstreamSettlementAmount.add(baSettlement.getSettlementAmount());
                    } else {
                        downstreamSettlementAmount = downstreamSettlementAmount.add(new BigDecimal(0));
                    }
                }
            }
            //结算数量
            baContractStart.setDownstreamSettlementNum(downstreamSettlementNum);
            //结算总价
            baContractStart.setDownstreamSettlementAmount(downstreamSettlementAmount);
            //结算单价
            BigDecimal downstreamSettlementPrice = new BigDecimal(0);
            if(downstreamSettlementNum.compareTo(new BigDecimal(0)) > 0){
                downstreamSettlementPrice = downstreamSettlementAmount.divide(downstreamSettlementNum,2, RoundingMode.HALF_UP);
            }
            baContractStart.setDownstreamSettlementPrice(downstreamSettlementPrice);
            //开票时间
            QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("start_id", baContractStart.getId());
            invoiceQueryWrapper.eq("invoice_type", 1);
            invoiceQueryWrapper.eq("state", "invoice:pass");
            invoiceQueryWrapper.eq("flag", 0);
            invoiceQueryWrapper.eq("collection_time",1);
            invoiceQueryWrapper.orderByDesc("create_time");
            List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            if(baInvoices.size() > 0){
                baContractStart.setInvoiceTime(baInvoices.get(0).getCreateTime());
            }

            list.add(baContractStart);
        }
        return list;
    }

    @Override
    public BaContractStartVO contractLaunchPanelDetails(String id) {

        BaContractStartVO baContractStartVO = new BaContractStartVO();
        //合同启动
        BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(id);
        //合同启动ID
        baContractStartVO.setId(contractStart.getId());
        //合同启动名称
        baContractStartVO.setName(contractStart.getName());
        //问题标识
        baContractStartVO.setProblemMark(contractStart.getProblemMark());
        //部门名称
        SysDept dept = sysDeptMapper.selectDeptById(contractStart.getDeptId());
        //用户名称
        SysUser user = sysUserMapper.selectUserById(contractStart.getUserId());
        //所属事业部
        if(StringUtils.isNotNull(dept) && StringUtils.isNotNull(user)){
            baContractStartVO.setPartDeptName(user.getNickName() + "-" + dept.getDeptName());
        }
        //业务线
        baContractStartVO.setBusinessLines(contractStart.getBusinessLines());
        //品名
        if (StringUtils.isNotEmpty(contractStart.getIndicatorInformation())) {
            BaEnterpriseRelevance baEnterpriseRelevance = baEnterpriseRelevanceService.selectBaEnterpriseRelevanceById(contractStart.getIndicatorInformation());
            baContractStartVO.setGoodsName(baEnterpriseRelevance.getGoodsName());
        }
        //完结
        if(contractStart.getCompletion() == 1){
            baContractStartVO.setCompletion(1L);
        }
        //下游合同
        QueryWrapper<BaContract> contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.like("start_id", contractStart.getId());
        contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        contractQueryWrapper.eq("contract_type", 7);
        contractQueryWrapper.eq("signing_status","已签");
        List<BaContract> baContracts = baContractMapper.selectList(contractQueryWrapper);
        if(baContracts.size() > 0){
             //合同单价
            if(baContracts.get(0).getPriceTotal() != null){
                baContractStartVO.setPrice(baContracts.get(0).getPriceTotal());
            }
            //合同吨数
            if(baContracts.get(0).getContractTotal() != null){
                baContractStartVO.setTonnage(baContracts.get(0).getContractTotal());
            }
            //下游点亮
            baContractStartVO.setDownstream("1");
        }
        //下游合同所有名称拼接
        contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.like("start_id", contractStart.getId());
        contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode()).and(query ->query.eq("contract_type", 7).or().eq("contract_type", 10));
        contractQueryWrapper.eq("signing_status","已签");
        List<BaContract> contractAnnex = baContractMapper.selectList(contractQueryWrapper);
        if(contractAnnex.size() > 0){
            //下游附件附件名称
            List<String> annexNameList = new ArrayList<>();
            for (BaContract contract:contractAnnex) {
                String annex = "";
                if(StringUtils.isNotEmpty(contract.getName())){
                    annex = contract.getName();
                }
                annexNameList.add(annex);
            }
            baContractStartVO.setDownstreamAnnex(annexNameList);
        }
        contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.like("start_id", contractStart.getId());
        contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        contractQueryWrapper.eq("contract_type", 6);
        contractQueryWrapper.eq("signing_status","已签");
        List<BaContract> list = baContractMapper.selectList(contractQueryWrapper);
        if(list.size() > 0){
            //上游点亮
            baContractStartVO.setUpstream("1");
        }
        //上游合同所有附件名称拼接
        contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.like("start_id", contractStart.getId());
        contractQueryWrapper.eq("state", AdminCodeEnum.CONTRACT_STATUS_PASS.getCode()).and(query ->query.eq("contract_type", 6).or().eq("contract_type", 9));
        contractQueryWrapper.eq("signing_status","已签");
        List<BaContract> UpContractAnnex = baContractMapper.selectList(contractQueryWrapper);
        if(UpContractAnnex.size() > 0){
            //下游附件附件名称
            List<String> annexNameList = new ArrayList<>();
            for (BaContract contract:UpContractAnnex) {
                String annex = "";
                if(StringUtils.isNotEmpty(contract.getName())){
                    annex = contract.getName();
                }
                annexNameList.add(annex);
            }
            baContractStartVO.setUpstreamAnnex(annexNameList);
        }
        //已发运数量
        BigDecimal checkCoalNum = new BigDecimal(0);
        BaTransport baTransport = new BaTransport();
        baTransport.setStartId(contractStart.getId());
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        baTransport.setType("1");
        BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
        if(!ObjectUtils.isEmpty(baTransport1)){
            if(baTransport1.getCheckCoalNum() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
            }
            if(baTransport1.getCcjweight() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
            }
            if(baTransport1.getConfirmedDeliveryWeight() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
            }
        }
        baContractStartVO.setCheckCoalNum(checkCoalNum);
        //上游物流凭证
        if(checkCoalNum.compareTo(new BigDecimal(0)) > 0){
            baContractStartVO.setLogisticsVoucher("1");
        }
        //未发运数量
        if(baContractStartVO.getTonnage() != null){
            BigDecimal subtract = baContractStartVO.getTonnage().subtract(checkCoalNum);
            baContractStartVO.setNotCheckCoalNum(subtract);
        }
        //进场吨数
        checkCoalNum = new BigDecimal(0);
        baTransport = new BaTransport();
        baTransport.setStartId(contractStart.getId());
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        //已验收状态
        baTransport.setTransportStatus("all");
        baTransport.setType("2");
        baTransport1 = baTransportMapper.selectTransportSum(baTransport);
        if(!ObjectUtils.isEmpty(baTransport1)){
            if(baTransport1.getCheckCoalNum() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
            }
            if(baTransport1.getCcjweight() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
            }
            if(baTransport1.getConfirmedDeliveryWeight() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
            }
        }
        //进场吨数
        baContractStartVO.setEntryTonnage(checkCoalNum);
        //进场数质量
        if(checkCoalNum.compareTo(new BigDecimal(0)) > 0){
            baContractStartVO.setNumberQuality("1");
        }
        //未进场数量
        BigDecimal notEntryTonnage = baContractStartVO.getCheckCoalNum().subtract(baContractStartVO.getEntryTonnage());
        baContractStartVO.setNotEntryTonnage(notEntryTonnage);
        //已付货款
        BigDecimal paymentAmount = new BigDecimal(0);
        BaPayment baPayment = new BaPayment();
        baPayment.setStartId(contractStart.getId());
        //baPayment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
        baPayment.setOnlyPass("1");
        baPayment.setType("1");//付款类型
        List<BaPayment> list1 = baPaymentService.selectBaPaymentList(baPayment);
        for (BaPayment payment:list1) {
            if(payment.getPaymentAmount() != null){
                paymentAmount = paymentAmount.add(payment.getPaymentAmount());
            }
        }
        baContractStartVO.setPaymentForGoods(paymentAmount);
        //已付货款
        if(paymentAmount != null){
            if(paymentAmount.compareTo(new BigDecimal(0)) > 0){
                baContractStartVO.setPaid("1");
            }
        }
        //已付运费
        baPayment.setType("2");//付款类型
        BigDecimal paymentFreight = baPaymentMapper.sumBaSettlementAndPayment(baPayment);
        if(paymentFreight != null){
            if(paymentFreight.compareTo(new BigDecimal(0)) > 0){
                baContractStartVO.setShippingFee("1");
            }
        }
        //已付运杂费
        BigDecimal transportIncidentals = new BigDecimal(0);
        baPayment = new BaPayment();
        baPayment.setStartId(contractStart.getId());
        //baPayment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
        baPayment.setOnlyPass("1");
        baPayment.setManyCondition("1");//查询(运费、服务费、其他)
        List<BaPayment> payments = baPaymentService.selectBaPaymentList(baPayment);
        for (BaPayment payment:payments) {
            if(payment.getPaymentAmount() != null){
                transportIncidentals = transportIncidentals.add(payment.getPaymentAmount());
            }
        }
        baContractStartVO.setTransportIncidentals(transportIncidentals);
        //累计付款
        BigDecimal add = new BigDecimal(0);
        if(baContractStartVO.getPaymentForGoods() != null){
             add = baContractStartVO.getPaymentForGoods().add(baContractStartVO.getTransportIncidentals());
        }else {
            add = baContractStartVO.getTransportIncidentals();
        }
        baContractStartVO.setPaymentAmount(add);
        //已收款
        BigDecimal collectionAmount = baClaimHistoryMapper.baClaimHistoryCount(contractStart.getId(),SecurityUtils.getCurrComId());
        baContractStartVO.setCollectionAmount(collectionAmount);
        //已收货款
        if(collectionAmount != null){
            if(collectionAmount.compareTo(new BigDecimal(0)) > 0){
                baContractStartVO.setReceivedPayment("1");
            }
        }
        //占资
        BigDecimal subtract = new BigDecimal(0);
        if(collectionAmount != null){
            subtract = add.subtract(collectionAmount);
        }
        baContractStartVO.setCapitalOccupation(subtract);
        //货转
        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
        transportQueryWrapper.eq("start_id",contractStart.getId());
        transportQueryWrapper.isNotNull("transfer_voucher");
        List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
        if(baTransports.size() > 0){
            baContractStartVO.setFreightTransfer("1");
        }
        //上游结算单
        QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
        settlementQueryWrapper.eq("contract_id", contractStart.getId());
        settlementQueryWrapper.eq("state", "settlement:pass");
        settlementQueryWrapper.eq("flag", 0);
        settlementQueryWrapper.eq("settlement_type", 1);
        List<BaSettlement> settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
        if(settlementList.size() > 0){
            //结算单名称
            List<String> settlementName = new ArrayList<>();
            for (BaSettlement settlement:settlementList) {
                if(StringUtils.isNotEmpty(settlement.getSettlementName())){
                    settlementName.add(settlement.getSettlementName());
                }
            }
            baContractStartVO.setUpstreamSettlementName(settlementName);
            baContractStartVO.setUpstreamSettlement("1");
        }
        //下游结算单
        settlementQueryWrapper = new QueryWrapper<>();
        settlementQueryWrapper.eq("contract_id", contractStart.getId());
        settlementQueryWrapper.eq("state", "settlement:pass");
        settlementQueryWrapper.eq("flag", 0);
        settlementQueryWrapper.eq("settlement_type", 2);
        List<BaSettlement> baSettlements = baSettlementMapper.selectList(settlementQueryWrapper);
        if(baSettlements.size() > 0){
            //结算单名称
            List<String> settlementName = new ArrayList<>();
            for (BaSettlement settlement:baSettlements) {
                if(StringUtils.isNotEmpty(settlement.getSettlementName())){
                    settlementName.add(settlement.getSettlementName());
                }
            }
            baContractStartVO.setDownstreamSettlementName(settlementName);
            baContractStartVO.setDownstreamSettlement("1");
        }
       //销项发票
        QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("start_id", contractStart.getId());
        invoiceQueryWrapper.eq("invoice_type", 1);
        invoiceQueryWrapper.eq("state", "invoice:pass");
        invoiceQueryWrapper.eq("flag", 0);
        List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
        if(baInvoices.size() > 0){
            //数量合计
            BigDecimal amountNum = new BigDecimal(0);
            //税价合计
            BigDecimal taxPriceTotal = new BigDecimal(0);
            for (BaInvoice invoice:baInvoices) {
                //查询税价合计
                BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
                baInvoiceDetail.setInvoiceId(invoice.getId());
                List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
                //税价合计
                BigDecimal bigDecimal=new BigDecimal(0.0000);
                //数量合计
                BigDecimal amountTotal=new BigDecimal(0);
                for(BaInvoiceDetail baInvoiceDetail1:baInvoiceDetailList){
                    if(baInvoiceDetail1.getTaxPriceTotal()!=null){
                        bigDecimal=bigDecimal.add(baInvoiceDetail1.getTaxPriceTotal());
                    }
                    if(null != baInvoiceDetail1.getAccount()){
                        amountTotal = amountTotal.add(baInvoiceDetail1.getAccount());
                    }
                }
                amountNum = amountNum.add(amountTotal);
                taxPriceTotal = taxPriceTotal.add(bigDecimal);
            }
            //发票数量
            baContractStartVO.setAmountTotal(amountNum);
            //税价合计
            baContractStartVO.setTaxPriceTotal(taxPriceTotal);
            baContractStartVO.setOutputInvoice("1");
        }
        //进项发票(货票)
        invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("start_id", contractStart.getId());
        invoiceQueryWrapper.eq("invoice_type", 2);
        invoiceQueryWrapper.eq("state", "inputInvoice:pass");
        invoiceQueryWrapper.eq("invoice_ilk",1);
        invoiceQueryWrapper.eq("flag", 0);
        List<BaInvoice> invoiceList = baInvoiceMapper.selectList(invoiceQueryWrapper);
        if(invoiceList.size() > 0){
            //数量合计
            BigDecimal amountNum = new BigDecimal(0);
            //税价合计
            BigDecimal taxPriceTotal = new BigDecimal(0);
            for (BaInvoice invoice:invoiceList) {
                //查询税价合计
                BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
                baInvoiceDetail.setInvoiceId(invoice.getId());
                List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
                //税价合计
                BigDecimal bigDecimal=new BigDecimal(0.0000);
                //数量合计
                BigDecimal amountTotal=new BigDecimal(0);
                for(BaInvoiceDetail baInvoiceDetail1:baInvoiceDetailList){
                    if(baInvoiceDetail1.getTaxPriceTotal()!=null){
                        bigDecimal=bigDecimal.add(baInvoiceDetail1.getTaxPriceTotal());
                    }
                    if(null != baInvoiceDetail1.getAccount()){
                        amountTotal = amountTotal.add(baInvoiceDetail1.getAccount());
                    }
                }
                amountNum = amountNum.add(amountTotal);
                taxPriceTotal = taxPriceTotal.add(bigDecimal);
            }
            //发票数量
            baContractStartVO.setInputAmountTotal(amountNum);
            //税价合计
            baContractStartVO.setInputTaxPriceTotal(taxPriceTotal);
            baContractStartVO.setInputInvoice("1");
        }
        //进项发票(运杂费发票)
        invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("start_id", contractStart.getId());
        invoiceQueryWrapper.eq("invoice_type", 2);
        invoiceQueryWrapper.eq("state", "inputInvoice:pass");
        invoiceQueryWrapper.eq("invoice_ilk",2);
        invoiceQueryWrapper.eq("flag", 0);
        List<BaInvoice> invoiceFreight = baInvoiceMapper.selectList(invoiceQueryWrapper);
        if(invoiceFreight.size() > 0){
            //数量合计
            BigDecimal amountNum = new BigDecimal(0);
            //税价合计
            BigDecimal taxPriceTotal = new BigDecimal(0);
            for (BaInvoice invoice:invoiceFreight) {
                //查询税价合计
                BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
                baInvoiceDetail.setInvoiceId(invoice.getId());
                List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
                //税价合计
                BigDecimal bigDecimal=new BigDecimal(0.0000);
                //数量合计
                BigDecimal amountTotal=new BigDecimal(0);
                for(BaInvoiceDetail baInvoiceDetail1:baInvoiceDetailList){
                    if(baInvoiceDetail1.getTaxPriceTotal()!=null){
                        bigDecimal=bigDecimal.add(baInvoiceDetail1.getTaxPriceTotal());
                    }
                    if(null != baInvoiceDetail1.getAccount()){
                        amountTotal = amountTotal.add(baInvoiceDetail1.getAccount());
                    }
                }
                amountNum = amountNum.add(amountTotal);
                taxPriceTotal = taxPriceTotal.add(bigDecimal);
            }
            //运杂费发票数量
            baContractStartVO.setFreightAmountTotal(amountNum);
            //云杂费税价合计
            baContractStartVO.setFreightPriceTotal(taxPriceTotal);
            baContractStartVO.setFreightInvoice("1");
        }
        //上游最终付款时间
        QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
        paymentQueryWrapper.eq("start_id", contractStart.getId());
        paymentQueryWrapper.eq("state", "payment:pass");
        paymentQueryWrapper.eq("flag", 0);
        paymentQueryWrapper.eq("type",1);
        paymentQueryWrapper.orderByDesc("create_time");
        List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
        if(baPayments.size() > 0){
            baContractStartVO.setUpstreamPaymentTime(baPayments.get(0).getCreateTime());
        }
        //下游最终回款时间
        List<BaCollection> collections = new ArrayList<>();
        QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
        historyQueryWrapper.eq("order_id", contractStart.getId());
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
        for (BaClaimHistory baClaimHistory : baClaimHistories) {
            //查询认领信息
            BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
            if(!ObjectUtils.isEmpty(baClaim)) {
                BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                //判断认领收款审批通过
                if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                    if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                        collections.add(baCollection);
                    }
                }
            }
        }
        if(collections.size() > 0){
            List<BaCollection> collect = collections.stream().sorted(Comparator.comparing(BaCollection::getCreateTime).reversed()).collect(Collectors.toList());
            baContractStartVO.setDownstreamPaymentTime(collect.get(0).getCreateTime());
        }
        //上游公司
        if(StringUtils.isNotEmpty(contractStart.getUpstreamCompany())){
            //查询供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(contractStart.getUpstreamCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baContractStartVO.setUpstreamSupplier(baSupplier.getCompanyAbbreviation());
            }
        }
        //终端公司
        if(StringUtils.isNotEmpty(contractStart.getTerminalCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(contractStart.getTerminalCompany());
            if(StringUtils.isNotNull(enterprise)){
                baContractStartVO.setDownstreamCustomer(enterprise.getCompanyAbbreviation());
            }
        }
        return baContractStartVO;
    }

    @Override
    public UR illuminate(String id) {
        Map<Integer,String> map = new HashMap<>();
        BaContractStartVO baContractStartVO = this.contractLaunchPanelDetails(id);
        //供货中是否点亮
        if(StringUtils.isNotEmpty(baContractStartVO.getDownstream()) || StringUtils.isNotEmpty(baContractStartVO.getUpstream()) ||
                StringUtils.isNotEmpty(baContractStartVO.getLogisticsVoucher()) || StringUtils.isNotEmpty(baContractStartVO.getNumberQuality()) ||
                StringUtils.isNotEmpty(baContractStartVO.getFreightTransfer())){
            map.put(1,"供货中");
        }
        //待结算是否点亮
        if(StringUtils.isNotEmpty(baContractStartVO.getPaid()) || StringUtils.isNotEmpty(baContractStartVO.getReceivedPayment()) || StringUtils.isNotEmpty(baContractStartVO.getShippingFee())  ){
            map.put(2,"待结算");
        }
        //待回款是否点亮
        if(StringUtils.isNotEmpty(baContractStartVO.getUpstreamSettlement()) || StringUtils.isNotEmpty(baContractStartVO.getDownstreamSettlement()) ||
                StringUtils.isNotEmpty(baContractStartVO.getInputInvoice()) || StringUtils.isNotEmpty(baContractStartVO.getOutputInvoice()) ||
                StringUtils.isNotEmpty(baContractStartVO.getTransportInvoice())){
            map.put(3,"待回款");
        }
        //已完结点亮
        if(baContractStartVO.getUpstreamPaymentTime() != null   || baContractStartVO.getDownstreamPaymentTime() != null){
            map.put(4,"已完成");
        }
        return UR.ok().data("map",map);
    }

    @Override
    public int countContractStartList(CountContractStartDTO countContractStartDTO) {
        countContractStartDTO.setTenantId(SecurityUtils.getCurrComId());
        return baContractStartMapper.countContractStartList(countContractStartDTO);
    }

    @Override
    public int bindingContract(BaContractStart baContractStart) {
        if(StringUtils.isNotEmpty(baContractStart.getContractId())){
            //合同启动关联多个已签合同
            String[] split = baContractStart.getContractId().split(",");
            for (String id:split) {
                //查询相关已签合同
                BaContract baContract = baContractMapper.selectBaContractById(id);
                //判断是否存在合同启动
                if(StringUtils.isNotEmpty(baContract.getStartId())){
                    baContract.setStartId(baContract.getStartId()+","+baContractStart.getId());
                }else {
                    baContract.setStartId(baContractStart.getId());
                }
                //判断上下游合同启动打标
                if(baContract.getContractType().equals("6")){
                    //上游合同
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
                    contractStart.setUpstream(baContract.getId());
                    baContractStartMapper.updateBaContractStart(contractStart);
                }
                if(baContract.getContractType().equals("7")){
                    //下游合同
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
                    contractStart.setDownstream(baContract.getId());
                    baContractStartMapper.updateBaContractStart(contractStart);
                }
                if(!ObjectUtils.isEmpty(baContract)){
                    BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                    baGlobalNumber.setStartId(baContractStart.getId());
                    baGlobalNumber.setCode(baContract.getGlobalNumber());
                    List<BaGlobalNumber> baGlobalNumbers = baGlobalNumberMapper.selectBaGlobalNumberList(baGlobalNumber);
                    if(CollectionUtils.isEmpty(baGlobalNumbers)){
                        //全局编号关系表新增数据
                        baGlobalNumber = new BaGlobalNumber();
                        baGlobalNumber.setId(getRedisIncreID.getId());
                        baGlobalNumber.setBusinessId(baContract.getId());
                        baGlobalNumber.setCode(baContract.getGlobalNumber());
                        baGlobalNumber.setHierarchy("3");
                        baGlobalNumber.setType("3");
                        baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                        baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                        baGlobalNumber.setStartId(baContractStart.getId());
                        baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
                    }
                }
                baContractMapper.updateBaContract(baContract);
            }
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public List<BaContractStartTonVO> countContractStartListByType() {
        //租户ID
        String currComId = SecurityUtils.getCurrComId();
        List<BaContractStartTonVO> baContractStartTonVOS = baContractStartMapper.countContractStartListByType(currComId);
        if(!CollectionUtils.isEmpty(baContractStartTonVOS)){
            int sumCount = baContractStartTonVOS.stream().collect(Collectors.summingInt(BaContractStartTonVO::getCount));
            if(sumCount > 0) {
                for (BaContractStartTonVO baContractStartTonVO : baContractStartTonVOS) {
                    if (!ObjectUtils.isEmpty(baContractStartTonVO)) {
                        if(!ObjectUtils.isEmpty(baContractStartTonVO.getCount())){
                            baContractStartTonVO.setPercent(PercentUtil.getPercent(baContractStartTonVO.getCount(), sumCount));
                        }
                    }
                }
            }
        }
        return baContractStartTonVOS;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int changeBaContractStart(BaContractStart baContractStart) {
        //获取原立项信息
        BaContractStart baContractStart1 = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
//        if("".equals(decideChange(baContractStart, baContractStart1))){
//            return -1;
//        }

        //伪删除历史表之前合同启动
        baHiContractStartMapper.deleteBaHiContractStartByIds(new String[]{baContractStart.getId()});

        //定义历史表数据
        BaHiContractStart baHiContractStart = new BaHiContractStart();
        BeanUtils.copyBeanProp(baHiContractStart, baContractStart);
        String redisIncreID = getRedisIncreID.getId();
        baHiContractStart.setId(redisIncreID);

        baContractStart1.setApproveFlag("2"); //变更
        baContractStartMapper.updateBaContractStart(baContractStart1);

        //设置变更标识
        baHiContractStart.setApproveFlag("2"); //变更
        //创建时间
        baHiContractStart.setCreateTime(DateUtils.getNowDate());
        baHiContractStart.setChangeTime(DateUtils.getNowDate());
        baHiContractStart.setChangeBy(SecurityUtils.getUsername());
        baHiContractStart.setCreateBy(SecurityUtils.getUsername());

        int result = 0;
        //变更，插入历史信息到历史表
        baHiContractStart.setRelationId(baContractStart.getId());
        baHiContractStart.setFlag(new Long(0));
        if(!StringUtils.isEmpty(baContractStart.getParentId())){
            baHiContractStart.setParentId(baContractStart.getParentId());
        }

        //运输计划
        if (StringUtils.isNotNull(baHiContractStart.getBaShippingPlans())) {
            for (BaShippingPlan baShippingPlan : baHiContractStart.getBaShippingPlans()) {
                baShippingPlan.setRelevanceId(baHiContractStart.getId());
                baShippingPlanService.insertBaShippingPlan(baShippingPlan);
            }
        }
        //查询收费标准
        if(!ObjectUtils.isEmpty(baHiContractStart.getBaChargingStandards())){
            baHiContractStart.getBaChargingStandards().setRelationId(baHiContractStart.getId());
            //新增收费标准
            iBaChargingStandardsService.insertBaChargingStandards(baHiContractStart.getBaChargingStandards());
        }

        //查询原合同启动投标信息运输信息
        QueryWrapper<BaBidTransport> baBidTransportsQueryWrapper = new QueryWrapper<>();
        baBidTransportsQueryWrapper.eq("start_id", baHiContractStart.getId());
        List<BaBidTransport> baBidTransports1 = baBidTransportMapper.selectList(baBidTransportsQueryWrapper);

        //修改合同启动投标信息运输信息
        List<BaBidTransport> baBidTransports = baContractStart.getBaBidTransports();
        //新增加运输信息
        List<String> bidTransportIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(baBidTransports)) {
            baBidTransports.stream().forEach(item -> {
                item.setId(getRedisIncreID.getId());
                item.setStartId(baHiContractStart.getId());
                bidTransportIds.add(item.getId());
                iBaBidTransportService.insertBaBidTransport(item);
            });
        }
        //查询原资金计划
        QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
        declareQueryWrapper.eq("start_id", baHiContractStart.getId());
        declareQueryWrapper.eq("flag", 0);
        List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);

        //新增资金计划id
        List<String> declareIds = new ArrayList<>();
        //修改原资金计划
        if (StringUtils.isNotNull(baHiContractStart.getBaDeclares())) {
            for (BaDeclare baDeclare : baHiContractStart.getBaDeclares()) {
                baDeclare.setId(getRedisIncreID.getId());
                baDeclare.setStartId(baHiContractStart.getId());
                baDeclare.setSource(1);
                baDeclare.setFlag(0L);
                baDeclare.setCreateTime(DateUtils.getNowDate());
                baDeclare.setCreateBy(SecurityUtils.getUsername());
                baDeclare.setUserId(SecurityUtils.getUserId());
                baDeclare.setDeptId(SecurityUtils.getDeptId());
                //判断资金计划是否重复
                BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                if (StringUtils.isNotNull(baDeclare1)) {
                    return -1;
                }
                baDeclareMapper.insertBaDeclare(baDeclare);
            }
        }
        baHiContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_VERIFYING.getCode());
        result = baHiContractStartMapper.insertBaHiContractStart(baHiContractStart);
        if (result > 0 && !baHiContractStart.getState().equals(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode())) {
            BaHiContractStart hiContractStart = baHiContractStartMapper.selectBaHiContractStartById(baHiContractStart.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", hiContractStart.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstancesHi(hiContractStart, AdminCodeEnum.CONTRACTSTART_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                //回滚原收费标准
                if (!ObjectUtils.isEmpty(baHiContractStart.getBaChargingStandards())) {
                    baChargingStandardsMapper.deleteBaChargingStandardsById(baHiContractStart.getBaChargingStandards().getId());
                }
                //回滚原合同启动投标信息运输信息
                if (!CollectionUtils.isEmpty(baBidTransports1)) {
                    iBaBidTransportService.deleteBaBidTransportByIds(bidTransportIds.toArray(new String[0]));
                }
                //回滚资金计划
                if (!CollectionUtils.isEmpty(baDeclares)) {
                    baDeclareService.deleteBaDeclareByIds(baDeclares.toArray(new String[0]));
                }
                return 0;
            }
        }
        return result;
    }

    /*@Override
    public List<StartComboBox> comboBox() {
        //查询合同启动类型
        List<SysDictData> dictData = sysDictDataMapper.selectDictDataByType("contract_start_type");
        List<StartComboBox> startComboBoxList = new ArrayList<>();
        for (SysDictData sysDictData:dictData) {
            StartComboBox startComboBox = new StartComboBox();
            BaContractStart baContractStart = new BaContractStart();
            baContractStart.setType(sysDictData.getDictValue());
            //查询合同信息
            List<BaContractStart> contractStarts = this.selectBaContractStartList(baContractStart);
            startComboBox.setValue(sysDictData.getDictValue());
            startComboBox.setLabel(sysDictData.getDictLabel());
            startComboBox.setChildren(contractStarts);
            startComboBoxList.add(startComboBox);
        }
        return startComboBoxList;
    }*/

    /**
     * 提交合同启动审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstancesOrder(BaContractStart baContractStart, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baContractStart.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baContractStart.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaContractStart baContractStart1 = this.selectBaContractStartById(baContractStart.getId());
        SysUser sysUser = iSysUserService.selectUserById(baContractStart1.getUserId());
        baContractStart1.setUserName(sysUser.getUserName());
        baContractStart1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baContractStart1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstancesOrder(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstancesOrder(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)) {
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if (data != null) {
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.ORDER_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(BaContractStart baContractStart, String mId, String msgContent) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baContractStart.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"合同启动审批提醒",msgContent,baMessage.getType(),baContractStart.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }


    /**
     * 判断是否有字段变更
     */
    public String decideChange(BaContractStart baContractStart, BaContractStart baContractStartBefore) {
        String result = "";
        //基本信息
        if (!baContractStart.getProjectName().equals(baContractStartBefore.getProjectName())) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getSerialNumber()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getSerialNumber())) || (
                !ObjectUtils.isEmpty(baContractStart.getSerialNumber()) &&
                        !baContractStart.getSerialNumber()
                                .equals(baContractStartBefore.getSerialNumber()))) {
            result = "isStart";
        } else if (!baContractStart.getName().equals(baContractStartBefore.getName())) {
            result = "isStart";
        } else if ((StringUtils.isEmpty(baContractStart.getStartShorter()) &&
                !StringUtils.isEmpty(baContractStartBefore.getStartShorter())) || (
                !StringUtils.isEmpty(baContractStart.getStartShorter()) &&
                        !baContractStart.getStartShorter()
                                .equals(baContractStartBefore.getStartShorter()))) {
            result = "isStart";
        } else if ((StringUtils.isEmpty(baContractStart.getEnterpriseId()) &&
                !StringUtils.isEmpty(baContractStartBefore.getEnterpriseId())) || (
                !StringUtils.isEmpty(baContractStart.getEnterpriseId()) &&
                        !baContractStart.getEnterpriseId()
                                .equals(baContractStartBefore.getEnterpriseId()))) {
            result = "isStart";
        } else if ((StringUtils.isEmpty(baContractStart.getDownstreamTradersName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getDownstreamTradersName())) || (
                !StringUtils.isEmpty(baContractStart.getDownstreamTradersName()) &&
                        !baContractStart.getDownstreamTradersName()
                                .equals(baContractStartBefore.getDownstreamTradersName()))) {
            result = "isStart";
        } else if ((StringUtils.isEmpty(baContractStart.getBusinessType()) &&
                !StringUtils.isEmpty(baContractStartBefore.getBusinessType())) || (
                !StringUtils.isEmpty(baContractStart.getBusinessType()) &&
                        !baContractStart.getBusinessType()
                                .equals(baContractStartBefore.getBusinessType()))) {
            result = "isStart";
        }

        BaChargingStandards standards = baContractStart.getBaChargingStandards();
        BaChargingStandards standardsBefore = baContractStart.getBaChargingStandards();
        if((!ObjectUtils.isEmpty(standards) && ObjectUtils.isEmpty(standardsBefore)) ||
                (ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore))
        ){
            result = "isStart";
        } else if(!ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore)){
            if((ObjectUtils.isEmpty(standards.getReserveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReserveTon())) || (
                    !ObjectUtils.isEmpty(standards.getReserveTon()) &&
                            !standards.getReserveTon()
                                    .equals(standardsBefore.getReserveTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getArriveAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveAnnualized())) || (
                    !StringUtils.isEmpty(standards.getArriveAnnualized()) &&
                            !standards.getArriveAnnualized()
                                    .equals(standardsBefore.getArriveAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getArriveTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveTon())) || (
                    !StringUtils.isEmpty(standards.getArriveTon()) &&
                            !standards.getArriveTon()
                                    .equals(standardsBefore.getArriveTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getVehicleAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleAnnualized())) || (
                    !StringUtils.isEmpty(standards.getVehicleAnnualized()) &&
                            !standards.getVehicleAnnualized()
                                    .equals(standardsBefore.getVehicleAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getVehicleTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleTon())) || (
                    !StringUtils.isEmpty(standards.getVehicleTon()) &&
                            !standards.getVehicleTon()
                                    .equals(standardsBefore.getVehicleTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveFreightAnnualized())) || (
                    !StringUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                            !standards.getArriveFreightAnnualized()
                                    .equals(standardsBefore.getArriveFreightAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getArriveFreightTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveFreightTon())) || (
                    !StringUtils.isEmpty(standards.getArriveFreightTon()) &&
                            !standards.getArriveFreightTon()
                                    .equals(standardsBefore.getArriveFreightTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleFreightAnnualized())) || (
                    !StringUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                            !standards.getVehicleFreightAnnualized()
                                    .equals(standardsBefore.getVehicleFreightAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getVehicleFreightTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleFreightTon())) || (
                    !StringUtils.isEmpty(standards.getVehicleFreightTon()) &&
                            !standards.getVehicleFreightTon()
                                    .equals(standardsBefore.getVehicleFreightTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getReplacePurchaseAnnualized())) || (
                    !StringUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                            !standards.getReplacePurchaseAnnualized()
                                    .equals(standardsBefore.getReplacePurchaseAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getReplacePurchaseTon())) || (
                    !StringUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                            !standards.getReplacePurchaseTon()
                                    .equals(standardsBefore.getReplacePurchaseTon()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getAdvancePaymentAnnualized())) || (
                    !StringUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                            !standards.getAdvancePaymentAnnualized()
                                    .equals(standardsBefore.getAdvancePaymentAnnualized()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getAdvancePaymentTon())) || (
                    !StringUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                            !standards.getAdvancePaymentTon()
                                    .equals(standardsBefore.getAdvancePaymentTon()))){
                result = "isStart";
            }
            if ((StringUtils.isEmpty(baContractStart.getBelongCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getBelongCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getBelongCompanyName()) &&
                            !baContractStart.getBelongCompanyName()
                                    .equals(baContractStartBefore.getBelongCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getSupplierName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getSupplierName())) || (
                    !StringUtils.isEmpty(baContractStart.getSupplierName()) &&
                            !baContractStart.getSupplierName()
                                    .equals(baContractStartBefore.getSupplierName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getCompanyName()) &&
                            !baContractStart.getCompanyName()
                                    .equals(baContractStartBefore.getCompanyName()))) {
                result = "isStart";
            } else if ((ObjectUtils.isEmpty(baContractStart.getCompanyAnnualizedRate()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getCompanyAnnualizedRate())) || (
                    !ObjectUtils.isEmpty(baContractStart.getCompanyAnnualizedRate()) &&
                            !baContractStart.getCompanyAnnualizedRate()
                                    .equals(baContractStartBefore.getCompanyAnnualizedRate()))) {
                result = "isStart";
            } else if ((ObjectUtils.isEmpty(baContractStart.getPrepaidFreight()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getPrepaidFreight())) || (
                    !ObjectUtils.isEmpty(baContractStart.getPrepaidFreight()) &&
                            !baContractStart.getPrepaidFreight()
                                    .equals(baContractStartBefore.getPrepaidFreight()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getAppendix()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getAppendix())) || (
                    !StringUtils.isEmpty(baContractStart.getAppendix()) &&
                            !baContractStart.getAppendix()
                                    .equals(baContractStartBefore.getAppendix()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getUpstreamCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getUpstreamCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getUpstreamCompanyName()) &&
                            !baContractStart.getUpstreamCompanyName()
                                    .equals(baContractStartBefore.getUpstreamCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getSubjectCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getSubjectCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getSubjectCompanyName()) &&
                            !baContractStart.getSubjectCompanyName()
                                    .equals(baContractStartBefore.getSubjectCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getDownstreamCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getDownstreamCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getDownstreamCompanyName()) &&
                            !baContractStart.getDownstreamCompanyName()
                                    .equals(baContractStartBefore.getDownstreamCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getTerminalCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getTerminalCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getTerminalCompanyName()) &&
                            !baContractStart.getTerminalCompanyName()
                                    .equals(baContractStartBefore.getTerminalCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getBusinessLines()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getBusinessLines())) || (
                    !StringUtils.isEmpty(baContractStart.getBusinessLines()) &&
                            !baContractStart.getBusinessLines()
                                    .equals(baContractStartBefore.getBusinessLines()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getLabelingCompanyName()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getLabelingCompanyName())) || (
                    !StringUtils.isEmpty(baContractStart.getLabelingCompanyName()) &&
                            !baContractStart.getLabelingCompanyName()
                                    .equals(baContractStartBefore.getLabelingCompanyName()))) {
                result = "isStart";
            } else if ((StringUtils.isEmpty(baContractStart.getBidType()) &&
                    !StringUtils.isEmpty(baContractStartBefore.getBidType())) || (
                    !StringUtils.isEmpty(baContractStart.getBidType()) &&
                            !baContractStart.getBidType()
                                    .equals(baContractStartBefore.getBidType()))) {
                result = "isStart";
            } else if ((ObjectUtils.isEmpty(baContractStart.getPrice()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getPrice())) || (
                    !ObjectUtils.isEmpty(baContractStart.getPrice()) &&
                            !baContractStart.getPrice()
                                    .equals(baContractStartBefore.getPrice()))) {
                result = "isStart";
            } else if ((ObjectUtils.isEmpty(baContractStart.getTonnage()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getTonnage())) || (
                    !ObjectUtils.isEmpty(baContractStart.getTonnage()) &&
                            !baContractStart.getTonnage()
                                    .equals(baContractStartBefore.getTonnage()))) {
                result = "isStart";
            } else if ((ObjectUtils.isEmpty(baContractStart.getPerformanceStartTime()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getTonnage())) || (
                    !ObjectUtils.isEmpty(baContractStart.getTonnage()) &&
                            !baContractStart.getTonnage()
                                    .equals(baContractStartBefore.getTonnage()))) {
                result = "isStart";
            }
        }
        //指标信息
        if("".equals(result)){
            BaEnterpriseRelevance enterpriseRelevance = baContractStart.getBaEnterpriseRelevance();
            BaEnterpriseRelevance enterpriseRelevanceBefore = baContractStartBefore.getBaEnterpriseRelevance();
            if((ObjectUtils.isEmpty(enterpriseRelevance) && !ObjectUtils.isEmpty(enterpriseRelevanceBefore)) ||
                    (!ObjectUtils.isEmpty(enterpriseRelevance) && ObjectUtils.isEmpty(enterpriseRelevanceBefore))){
                result = "isStart";
            } else if(!ObjectUtils.isEmpty(enterpriseRelevance)
                    && !ObjectUtils.isEmpty(enterpriseRelevanceBefore)){
                if(!enterpriseRelevance.equals(enterpriseRelevanceBefore)){
                    result = "isStart";
                } else {
                    if((StringUtils.isEmpty(enterpriseRelevance.getGoodsId()) && !StringUtils.isEmpty(enterpriseRelevanceBefore.getGoodsId())) ||
                            (!StringUtils.isEmpty(enterpriseRelevance.getGoodsId()) && !enterpriseRelevance.getGoodsId().equals(enterpriseRelevanceBefore.getGoodsId()))){
                        result = "isStart";
                    } else if((StringUtils.isEmpty(enterpriseRelevance.getTarget()) && StringUtils.isNotEmpty(enterpriseRelevanceBefore.getTarget())) ||
                            (StringUtils.isNotEmpty(enterpriseRelevance.getTarget()) && !enterpriseRelevance.getTarget().equals(enterpriseRelevanceBefore.getTarget()))){
                        result = "isStart";
                    } else if((enterpriseRelevance.getMonthlyConsumption() == null && enterpriseRelevanceBefore.getMonthlyConsumption() != null) ||
                            (enterpriseRelevance.getMonthlyConsumption() != null && enterpriseRelevanceBefore.getMonthlyConsumption() == null) ||
                            (enterpriseRelevance.getMonthlyConsumption().intValue() != enterpriseRelevanceBefore.getMonthlyConsumption().intValue())){
                        result = "isStart";
                    }
                }
            }
        }

        //运输信息
        //所需煤种判断
        if("".equals(result)){
            List<BaBidTransport> baBidTransports = baContractStart.getBaBidTransports();
            List<BaBidTransport> baBidTransportsBefore = baContractStartBefore.getBaBidTransports();
            if((CollectionUtils.isEmpty(baBidTransports) && !CollectionUtils.isEmpty(baBidTransportsBefore)) ||
                    (!CollectionUtils.isEmpty(baBidTransports) && CollectionUtils.isEmpty(baBidTransportsBefore))){
                result = "isStart";
            } else if(!CollectionUtils.isEmpty(baBidTransports)
                    && !CollectionUtils.isEmpty(baBidTransportsBefore)){
                if(baBidTransports.size() != baBidTransportsBefore.size()){
                    result = "isStart";
                } else {
                    for(int i = 0; i < baBidTransports.size(); i++){
                        BaBidTransport baBidTransport = baBidTransports.get(i);
                        BaBidTransport baBidTransportBefore = baBidTransportsBefore.get(i);
                        if((StringUtils.isEmpty(baBidTransport.getTransportType()) && !StringUtils.isEmpty(baBidTransportBefore.getTransportType())) ||
                                (!StringUtils.isEmpty(baBidTransport.getTransportType()) && !baBidTransport.getTransportType().equals(baBidTransportBefore.getTransportType()))){
                            result = "isStart";
                            break;
                        } else if((StringUtils.isEmpty(baBidTransport.getTransportCompanyName()) && StringUtils.isNotEmpty(baBidTransportBefore.getTransportCompanyName())) ||
                                (StringUtils.isNotEmpty(baBidTransport.getTransportCompanyName()) && !baBidTransport.getTransportCompanyName().equals(baBidTransportBefore.getTransportCompanyName()))){
                            result = "isStart";
                            break;
                        } else if((baBidTransport.getOrigin() == null && baBidTransportBefore.getOrigin() != null) ||
                                (baBidTransport.getOrigin() != null && baBidTransportBefore.getOrigin() == null) ||
                                (!baBidTransport.getOrigin().equals(baBidTransportBefore.getOrigin()))){
                            result = "isStart";
                            break;
                        } else if((baBidTransport.getDestination() == null && baBidTransportBefore.getDestination() != null) ||
                                (baBidTransport.getDestination() != null && baBidTransportBefore.getDestination() == null) ||
                                !(baBidTransport.getDestination().equals(baBidTransportBefore.getDestination()))){
                            result = "isStart";
                            break;
                        } else if((baBidTransport.getEstimatedFreight() == null && baBidTransportBefore.getEstimatedFreight() != null) ||
                                (baBidTransport.getEstimatedFreight() != null && baBidTransportBefore.getEstimatedFreight() == null) ||
                                (!baBidTransport.getEstimatedFreight().equals(baBidTransportBefore.getEstimatedFreight()))){
                            result = "isStart";
                            break;
                        } else if((baBidTransport.getIncidentalsCompany() == null && baBidTransportBefore.getIncidentalsCompany() != null) ||
                                (baBidTransport.getIncidentalsCompany() != null && baBidTransportBefore.getIncidentalsCompany() == null) ||
                                (!baBidTransport.getIncidentalsCompany().equals(baBidTransportBefore.getIncidentalsCompany()))){
                            result = "isStart";
                            break;
                        } else if((baBidTransport.getTransportIncidentals() == null && baBidTransportBefore.getTransportIncidentals() != null) ||
                                (baBidTransport.getTransportIncidentals() != null && baBidTransportBefore.getTransportIncidentals() == null) ||
                                (!baBidTransport.getTransportIncidentals().equals(baBidTransportBefore.getTransportIncidentals()))){
                            result = "isStart";
                            break;
                        }
                    }
                }
            }
        }
        //资金计划
        if("".equals(result)){
            List<BaDeclare> baDeclares = baContractStart.getBaDeclares();
            List<BaDeclare> baDeclaresBefore = baContractStartBefore.getBaDeclares();
            if((CollectionUtils.isEmpty(baDeclares) && !CollectionUtils.isEmpty(baDeclaresBefore)) ||
                    (!CollectionUtils.isEmpty(baDeclares) && CollectionUtils.isEmpty(baDeclaresBefore))){
                result = "isStart";
            } else if(!CollectionUtils.isEmpty(baDeclares)
                    && !CollectionUtils.isEmpty(baDeclaresBefore)){
                if(baDeclares.size() != baDeclaresBefore.size()){
                    result = "isStart";
                } else {
                    for(int i = 0; i < baDeclares.size(); i++){
                        BaDeclare baDeclare = baDeclares.get(i);
                        BaDeclare baDeclareBefore = baDeclaresBefore.get(i);
                        if((StringUtils.isEmpty(baDeclare.getPlanTime()) && !StringUtils.isEmpty(baDeclareBefore.getPlanTime())) ||
                                (!StringUtils.isEmpty(baDeclare.getPlanTime()) && !baDeclare.getPlanTime().equals(baDeclareBefore.getPlanTime()))){
                            result = "isStart";
                            break;
                        } else if((ObjectUtils.isEmpty(baDeclare.getFreightMoney()) && !ObjectUtils.isEmpty(baDeclareBefore.getFreightMoney())) ||
                                (!ObjectUtils.isEmpty(baDeclare.getFreightMoney()) && !baDeclare.getFreightMoney().equals(baDeclareBefore.getFreightMoney()))){
                            result = "isStart";
                            break;
                        } else if((ObjectUtils.isEmpty(baDeclare.getLoanMoney()) && !ObjectUtils.isEmpty(baDeclareBefore.getLoanMoney())) ||
                                (!ObjectUtils.isEmpty(baDeclare.getLoanMoney()) && !baDeclare.getLoanMoney().equals(baDeclareBefore.getLoanMoney()))){
                            result = "isStart";
                            break;
                        } else if((ObjectUtils.isEmpty(baDeclare.getPaymentMoney()) && !ObjectUtils.isEmpty(baDeclareBefore.getPaymentMoney())) ||
                                (!ObjectUtils.isEmpty(baDeclare.getPaymentMoney()) && !baDeclare.getPaymentMoney().equals(baDeclareBefore.getPaymentMoney()))){
                            result = "isStart";
                            break;
                        }
                    }
                }
            }
        }

        if ((ObjectUtils.isEmpty(baContractStart.getServiceManagerName()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getServiceManagerName())) || (
                !ObjectUtils.isEmpty(baContractStart.getServiceManagerName()) &&
                        !baContractStart.getServiceManagerName()
                                .equals(baContractStartBefore.getServiceManagerName()))) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getDivisionName()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getDivisionName())) || (
                !ObjectUtils.isEmpty(baContractStart.getDivisionName()) &&
                        !baContractStart.getDivisionName()
                                .equals(baContractStartBefore.getDivisionName()))) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getPersonChargeName()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getPersonChargeName())) || (
                !ObjectUtils.isEmpty(baContractStart.getPersonChargeName()) &&
                        !baContractStart.getPersonChargeName()
                                .equals(baContractStartBefore.getPersonChargeName()))) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getLowExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getLowExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baContractStart.getLowExternalContactPerson()) &&
                        !baContractStart.getLowExternalContactPerson()
                                .equals(baContractStartBefore.getLowExternalContactPerson()))) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                        !baContractStart.getExternalContactPerson()
                                .equals(baContractStartBefore.getExternalContactPerson()))) {
            result = "isStart";
        } else if ((ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                        !baContractStart.getExternalContactPerson()
                                .equals(baContractStartBefore.getExternalContactPerson()))) {
            result = "isStart";
        }

        return result;
    }

    @Override
    public List<String> selectChangeDataList(String id) {
        BaHiContractStart baHiContractStart = baHiContractStartService.selectBaHiContractStartById(id);
        if(!ObjectUtils.isEmpty(baHiContractStart)){
            BaContractStart baContractStart = this.selectBaContractStartById(baHiContractStart.getRelationId());
            return getChangeData(baContractStart, baHiContractStart);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BaContractStartTonVO> contractStartListByType(BaContractStartTonVO baContractStartTonVO) {
        return baContractStartMapper.contractStartListByType(baContractStartTonVO);
    }

    @Override
    public List<BaContractStart> selectBaContractStartAuthorizedList(BaContractStart baContractStart) {
        List<BaContractStart> baContractStarts = baContractStartMapper.selectBaContractStartAuthorizedList(baContractStart);

        for (BaContractStart contractStart : baContractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }

            //查找运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", contractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
            BigDecimal transit = new BigDecimal(0);
            for (BaTransport baTransport : baTransports) {
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    //运输中
                    transit = transit.add(baCheck.getCheckCoalNum());
                }
            }
            contractStart.setCheckCoalNum(transit);
            if(StringUtils.isNotEmpty(contractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    contractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
        }
        return baContractStarts;
    }

    @Override
    public int authorizedBaContractStart(BaContractStart baContractStart) {
        return baContractStartMapper.updateBaContractStart(baContractStart);
    }

    @Override
    public List<BaContractStart> selectOrderAuthorizedList(BaContractStart baContractStart) {
        List<BaContractStart> contractStarts = baContractStartMapper.selectOrderAuthorizedList(baContractStart);
        for (BaContractStart contractStart : contractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }

            //品类
            if (StringUtils.isNotEmpty(contractStart.getGoodsType())) {
                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(contractStart.getGoodsType());
                if(StringUtils.isNotNull(baGoodsType)){
                    contractStart.setGoodsType(baGoodsType.getName());
                }else {
                    BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(contractStart.getGoodsType());
                    if(!ObjectUtils.isEmpty(baGoods1)){
                        contractStart.setGoodsType(baGoods1.getName());
                    }
                }
            }
            //品名
            if(StringUtils.isNotEmpty(contractStart.getGoodsId())){
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(contractStart.getGoodsId());
                contractStart.setAppGoodsName(baGoods.getName());
            }

            if(StringUtils.isNotEmpty(contractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    contractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
        }
        return contractStarts;
    }

    @Override
    public paymentReceivedDTO integration(String id) {
        paymentReceivedDTO paymentReceivedDTO = new paymentReceivedDTO();
        //收款信息
        List<BaCollection> collectionList = new ArrayList<>();
        //所有付款
        BigDecimal paymentTotal = new BigDecimal(0);
        //所有收款
        BigDecimal collectionTotal = new BigDecimal(0);
        //认领收款
        BigDecimal applyCollectionAmount = new BigDecimal(0);
        //查询认领金额
        QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
        historyQueryWrapper.eq("order_id", id);
        historyQueryWrapper.orderByDesc("id");
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
        for (BaClaimHistory baClaimHistory : baClaimHistories) {
            //查询认领信息
            BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
            if(!ObjectUtils.isEmpty(baClaim)) {
                BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                //判断认领收款审批通过
                if (StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())) {
                    if (baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())) {
                        collectionList.add(baCollection);
                        applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                    }
                }
            }
        }
        //认领收款
        List<BaCollection> collect1 = collectionList.stream().sorted(Comparator.comparing(BaCollection::getCollectionDate).reversed()).collect(Collectors.toList());
        if(collect1.size() > 0){
            //认领首次付款
            paymentReceivedDTO.setFinalClaimPayment(collect1.get(0).getCollectionDate());
        }
        //直接收款金额
        BigDecimal directCollectionAmount = new BigDecimal(0);
        QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("start_id",id);
        queryWrapper.eq("state",AdminCodeEnum.CLAIM_STATUS_PASS.getCode());
        queryWrapper.eq("flag",0);
        queryWrapper.orderByDesc("collection_date");
        List<BaCollection> collections = baCollectionMapper.selectList(queryWrapper);
        if(collections.size() > 0){
            //首次付款
            paymentReceivedDTO.setFinalPayment(collections.get(0).getCollectionDate());
        }
        for (BaCollection baCollection:collections) {
            directCollectionAmount = directCollectionAmount.add(baCollection.getApplyCollectionAmount());
        }
        collectionTotal = collectionTotal.add(applyCollectionAmount).add(directCollectionAmount);
        //所有付款
        BigDecimal paymentForGoods = new BigDecimal(0);
        BaPayment payment = new BaPayment();
        payment.setStartId(id);
        //payment.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode()); //付款审批通过
        payment.setOnlyPass("1");
        List<BaPayment> list1 = baPaymentService.selectBaPaymentList(payment);
        //付款凭证
        List<BaPaymentVoucher> vouchers = new ArrayList<>();
        for (BaPayment payment1:list1) {
            if(payment1.getPaymentAmount() != null){
                vouchers.addAll(payment1.getBaPaymentVouchers());
                paymentForGoods = paymentForGoods.add(payment1.getPaymentAmount());
            }
        }
        //首次付款日期
        List<BaPaymentVoucher> collect = vouchers.stream().sorted(Comparator.comparing(BaPaymentVoucher::getPaymentDate).reversed()).collect(Collectors.toList());
        if(collect.size() > 0){
            paymentReceivedDTO.setDownPayment(collect.get(collect.size() - 1).getPaymentDate());
        }
        paymentTotal = paymentTotal.add(paymentForGoods);
        paymentReceivedDTO.setCollectionTotal(collectionTotal);
        paymentReceivedDTO.setPaymentTotal(paymentTotal);
        //占资金额
        paymentReceivedDTO.setActualCapitalOccupation(paymentTotal.subtract(collectionTotal));
        return paymentReceivedDTO;
    }

    @Override
    public List<DropDownBox> upstreamCompanies(String name) {
        List<DropDownBox> list = new ArrayList<>();
        //供应商
        BaSupplier baSupplier = new BaSupplier();
        baSupplier.setState(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baSupplier.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baSupplier.setName(name);
        }
        List<BaSupplier> baSuppliers = baSupplierMapper.selectBaSupplierList(baSupplier);
        for (BaSupplier supplier:baSuppliers) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(supplier.getId());
            dropDownBox.setName(supplier.getName());
            dropDownBox.setAbbreviation(supplier.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        //装卸服务公司
        BaCompany baCompany = new BaCompany();
        baCompany.setCompanyType("装卸服务公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> baCompanyList = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:baCompanyList) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        //公路运输类公司
        baCompany = new BaCompany();
        baCompany.setCompanyType("汽运公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> companyList = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:companyList) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        //货主
        BaCargoOwner cargoOwner = new BaCargoOwner();
        cargoOwner.setState(AdminCodeEnum.CARGOOWNER_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            cargoOwner.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            cargoOwner.setName(name);
        }
        List<BaCargoOwner> baCargoOwners = baCargoOwnerMapper.selectBaCargoOwnerList(cargoOwner);
        for (BaCargoOwner baCargoOwner:baCargoOwners) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(baCargoOwner.getId());
            dropDownBox.setName(baCargoOwner.getName());
            dropDownBox.setAbbreviation(baCargoOwner.getShorterForm());
            list.add(dropDownBox);
        }
        //运输托运单位
        baCompany = new BaCompany();
        baCompany.setCompanyType("铁路公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> companies = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:companies) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        //站台服务公司
        baCompany = new BaCompany();
        baCompany.setCompanyType("站台服务公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> companies1 = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:companies1) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        //代发公司
        baCompany = new BaCompany();
        baCompany.setCompanyType("代发公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> companies2 = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:companies2) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
            list.add(dropDownBox);
        }
        return list;
    }

    @Override
    public List<DropDownBox> downstreamCompanies(String type,String name) {
        List<DropDownBox> list = new ArrayList<>();
        if(StringUtils.isNotEmpty(type)){
            if(type.equals("1")){
                //托盘公司
                BaCompany baCompany = new BaCompany();
                baCompany.setCompanyType("托盘公司");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companyList = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companyList) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
                //下游贸易商
                baCompany = new BaCompany();
                baCompany.setCompanyType("下游贸易商");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companies = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companies) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
            }else if(type.equals("2")){
                 //终端数据
                BaEnterprise enterprise = new BaEnterprise();
                enterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    enterprise.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    enterprise.setName(name);
                }
                List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectBaEnterpriseList(enterprise);
                for (BaEnterprise baEnterprise:baEnterpriseList) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(baEnterprise.getId());
                    dropDownBox.setName(baEnterprise.getName());
                    dropDownBox.setAbbreviation(baEnterprise.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
            }else if(type.equals("3")){
                //公路运输类公司
                BaCompany baCompany = new BaCompany();
                baCompany.setCompanyType("汽运公司");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companyList = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companyList) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
                //货主
                BaCargoOwner cargoOwner = new BaCargoOwner();
                cargoOwner.setState(AdminCodeEnum.CARGOOWNER_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    cargoOwner.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    cargoOwner.setName(name);
                }
                List<BaCargoOwner> baCargoOwners = baCargoOwnerMapper.selectBaCargoOwnerList(cargoOwner);
                for (BaCargoOwner baCargoOwner:baCargoOwners) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(baCargoOwner.getId());
                    dropDownBox.setName(baCargoOwner.getName());
                    dropDownBox.setAbbreviation(baCargoOwner.getShorterForm());
                    list.add(dropDownBox);
                }
                //运输托运单位
                baCompany = new BaCompany();
                baCompany.setCompanyType("铁路公司");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companies = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companies) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
                //站台服务公司
                baCompany = new BaCompany();
                baCompany.setCompanyType("站台服务公司");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companies1 = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companies1) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
                //代发公司
                baCompany = new BaCompany();
                baCompany.setCompanyType("代发公司");
                baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompany.setTenantId(SecurityUtils.getCurrComId());
                }
                //名称筛选
                if(StringUtils.isNotEmpty(name)){
                    baCompany.setName(name);
                }
                List<BaCompany> companies2 = baCompanyMapper.selectBaCompanyList(baCompany);
                for (BaCompany company:companies2) {
                    DropDownBox dropDownBox = new DropDownBox();
                    dropDownBox.setId(company.getId());
                    dropDownBox.setName(company.getName());
                    dropDownBox.setAbbreviation(company.getCompanyAbbreviation());
                    list.add(dropDownBox);
                }
            }
        }
        return list;
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> getChangeData(BaContractStart baContractStart, BaHiContractStart baContractStartBefore) {
        List<String> isChangeList = new ArrayList<>();

        //基本信息
        if (!baContractStart.getProjectName().equals(baContractStartBefore.getProjectName())) {
            isChangeList.add("projectName");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getSerialNumber()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getSerialNumber())) || (
                !ObjectUtils.isEmpty(baContractStart.getSerialNumber()) &&
                        !baContractStart.getSerialNumber()
                                .equals(baContractStartBefore.getSerialNumber()))) {
            isChangeList.add("serialNumber");
        }
        if (!baContractStart.getName().equals(baContractStartBefore.getName())) {
            isChangeList.add("name");
        }
        if ((StringUtils.isEmpty(baContractStart.getStartShorter()) &&
                !StringUtils.isEmpty(baContractStartBefore.getStartShorter())) || (
                !StringUtils.isEmpty(baContractStart.getStartShorter()) &&
                        !baContractStart.getStartShorter()
                                .equals(baContractStartBefore.getStartShorter()))) {
            isChangeList.add("startShorter");
        }
        if ((StringUtils.isEmpty(baContractStart.getEnterpriseId()) &&
                !StringUtils.isEmpty(baContractStartBefore.getEnterpriseId())) || (
                !StringUtils.isEmpty(baContractStart.getEnterpriseId()) &&
                        !baContractStart.getEnterpriseId()
                                .equals(baContractStartBefore.getEnterpriseId()))) {
            isChangeList.add("enterpriseId");
        }
        if ((StringUtils.isEmpty(baContractStart.getDownstreamTradersName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getDownstreamTradersName())) || (
                !StringUtils.isEmpty(baContractStart.getDownstreamTradersName()) &&
                        !baContractStart.getDownstreamTradersName()
                                .equals(baContractStartBefore.getDownstreamTradersName()))) {
            isChangeList.add("downstreamTradersName");
        }
        if ((StringUtils.isEmpty(baContractStart.getBusinessType()) &&
                !StringUtils.isEmpty(baContractStartBefore.getBusinessType())) || (
                !StringUtils.isEmpty(baContractStart.getBusinessType()) &&
                        !baContractStart.getBusinessType()
                                .equals(baContractStartBefore.getBusinessType()))) {
            isChangeList.add("businessType");
        }

        BaChargingStandards standards = baContractStart.getBaChargingStandards();
        BaChargingStandards standardsBefore = baContractStartBefore.getBaChargingStandards();
        if ((!ObjectUtils.isEmpty(standards) && ObjectUtils.isEmpty(standardsBefore)) ||
                (ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore))
        ) {
            isChangeList.add("baChargingStandards");
            isChangeList.add("reserveTon");
            isChangeList.add("arriveAnnualized");
            isChangeList.add("arriveTon");
            isChangeList.add("vehicleAnnualized");
            isChangeList.add("vehicleTon");
            isChangeList.add("arriveFreightAnnualized");
            isChangeList.add("arriveFreightTon");
            isChangeList.add("vehicleFreightAnnualized");
            isChangeList.add("vehicleFreightTon");
            isChangeList.add("replacePurchaseAnnualized");
            isChangeList.add("replacePurchaseTon");
            isChangeList.add("advancePaymentAnnualized");
            isChangeList.add("advancePaymentTon");
        }
        if (!ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore)) {
            if ((ObjectUtils.isEmpty(standards.getReserveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReserveTon())) || (
                    !ObjectUtils.isEmpty(standards.getReserveTon()) &&
                            !standards.getReserveTon()
                                    .equals(standardsBefore.getReserveTon()))) {
                isChangeList.add("reserveTon");
            }
            if ((StringUtils.isEmpty(standards.getArriveAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveAnnualized())) || (
                    !StringUtils.isEmpty(standards.getArriveAnnualized()) &&
                            !standards.getArriveAnnualized()
                                    .equals(standardsBefore.getArriveAnnualized()))) {
                isChangeList.add("arriveAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getArriveTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveTon())) || (
                    !StringUtils.isEmpty(standards.getArriveTon()) &&
                            !standards.getArriveTon()
                                    .equals(standardsBefore.getArriveTon()))) {
                isChangeList.add("arriveTon");
            }
            if ((StringUtils.isEmpty(standards.getVehicleAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleAnnualized())) || (
                    !StringUtils.isEmpty(standards.getVehicleAnnualized()) &&
                            !standards.getVehicleAnnualized()
                                    .equals(standardsBefore.getVehicleAnnualized()))) {
                isChangeList.add("vehicleAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getVehicleTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleTon())) || (
                    !StringUtils.isEmpty(standards.getVehicleTon()) &&
                            !standards.getVehicleTon()
                                    .equals(standardsBefore.getVehicleTon()))) {
                isChangeList.add("vehicleTon");
            }
            if ((StringUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveFreightAnnualized())) || (
                    !StringUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                            !standards.getArriveFreightAnnualized()
                                    .equals(standardsBefore.getArriveFreightAnnualized()))) {
                isChangeList.add("arriveFreightAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getArriveFreightTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getArriveFreightTon())) || (
                    !StringUtils.isEmpty(standards.getArriveFreightTon()) &&
                            !standards.getArriveFreightTon()
                                    .equals(standardsBefore.getArriveFreightTon()))) {
                isChangeList.add("arriveFreightTon");
            }
            if ((StringUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleFreightAnnualized())) || (
                    !StringUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                            !standards.getVehicleFreightAnnualized()
                                    .equals(standardsBefore.getVehicleFreightAnnualized()))) {
                isChangeList.add("vehicleFreightAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getVehicleFreightTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getVehicleFreightTon())) || (
                    !StringUtils.isEmpty(standards.getVehicleFreightTon()) &&
                            !standards.getVehicleFreightTon()
                                    .equals(standardsBefore.getVehicleFreightTon()))) {
                isChangeList.add("vehicleFreightTon");
            }
            if ((StringUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getReplacePurchaseAnnualized())) || (
                    !StringUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                            !standards.getReplacePurchaseAnnualized()
                                    .equals(standardsBefore.getReplacePurchaseAnnualized()))) {
                isChangeList.add("replacePurchaseAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getReplacePurchaseTon())) || (
                    !StringUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                            !standards.getReplacePurchaseTon()
                                    .equals(standardsBefore.getReplacePurchaseTon()))) {
                isChangeList.add("replacePurchaseTon");
            }
            if ((StringUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                    !StringUtils.isEmpty(standardsBefore.getAdvancePaymentAnnualized())) || (
                    !StringUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                            !standards.getAdvancePaymentAnnualized()
                                    .equals(standardsBefore.getAdvancePaymentAnnualized()))) {
                isChangeList.add("advancePaymentAnnualized");
            }
            if ((StringUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                    !StringUtils.isEmpty(standardsBefore.getAdvancePaymentTon())) || (
                    !StringUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                            !standards.getAdvancePaymentTon()
                                    .equals(standardsBefore.getAdvancePaymentTon()))) {
                isChangeList.add("advancePaymentTon");
            }
        }
        if ((ObjectUtils.isEmpty(baContractStart.getAccountUpperLimit()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getAccountUpperLimit())) || (
                !ObjectUtils.isEmpty(baContractStart.getAccountUpperLimit()) &&
                        !baContractStart.getAccountUpperLimit()
                                .equals(baContractStartBefore.getAccountUpperLimit()))) {
            isChangeList.add("accountUpperLimit");
        }
        if ((StringUtils.isEmpty(baContractStart.getBelongCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getBelongCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getBelongCompanyName()) &&
                        !baContractStart.getBelongCompanyName()
                                .equals(baContractStartBefore.getBelongCompanyName()))) {
            isChangeList.add("belongCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getSupplierName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getSupplierName())) || (
                !StringUtils.isEmpty(baContractStart.getSupplierName()) &&
                        !baContractStart.getSupplierName()
                                .equals(baContractStartBefore.getSupplierName()))) {
            isChangeList.add("supplierName");
        }
        if ((StringUtils.isEmpty(baContractStart.getSalesman()) &&
                !StringUtils.isEmpty(baContractStartBefore.getSalesman())) || (
                !StringUtils.isEmpty(baContractStart.getSalesman()) &&
                        !baContractStart.getSalesman()
                                .equals(baContractStartBefore.getSalesman()))) {
            isChangeList.add("salesman");
        }
        if ((StringUtils.isEmpty(baContractStart.getCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getCompanyName()) &&
                        !baContractStart.getCompanyName()
                                .equals(baContractStartBefore.getCompanyName()))) {
            isChangeList.add("companyName");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getCompanyAnnualizedRate()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getCompanyAnnualizedRate())) || (
                !ObjectUtils.isEmpty(baContractStart.getCompanyAnnualizedRate()) &&
                        !baContractStart.getCompanyAnnualizedRate()
                                .equals(baContractStartBefore.getCompanyAnnualizedRate()))) {
            isChangeList.add("companyAnnualizedRate");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getPrepaidFreight()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getPrepaidFreight())) || (
                !ObjectUtils.isEmpty(baContractStart.getPrepaidFreight()) &&
                        !baContractStart.getPrepaidFreight()
                                .equals(baContractStartBefore.getPrepaidFreight()))) {
            isChangeList.add("prepaidFreight");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getWhetherExtras()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getWhetherExtras())) ||
                (!ObjectUtils.isEmpty(baContractStart.getWhetherExtras()) &&
                        ObjectUtils.isEmpty(baContractStartBefore.getWhetherExtras())) || (
                baContractStart.getWhetherExtras().intValue() !=
                        baContractStartBefore.getWhetherExtras().intValue())) {
            isChangeList.add("whetherExtras");
        }
        if ((StringUtils.isEmpty(baContractStart.getAppendix()) &&
                !StringUtils.isEmpty(baContractStartBefore.getAppendix())) || (
                !StringUtils.isEmpty(baContractStart.getAppendix()) &&
                        !baContractStart.getAppendix()
                                .equals(baContractStartBefore.getAppendix()))) {
            isChangeList.add("appendix");
        }
        if ((StringUtils.isEmpty(baContractStart.getUpstreamCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getUpstreamCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getUpstreamCompanyName()) &&
                        !baContractStart.getUpstreamCompanyName()
                                .equals(baContractStartBefore.getUpstreamCompanyName()))) {
            isChangeList.add("upstreamCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getSubjectCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getSubjectCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getSubjectCompanyName()) &&
                        !baContractStart.getSubjectCompanyName()
                                .equals(baContractStartBefore.getSubjectCompanyName()))) {
            isChangeList.add("subjectCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getDownstreamCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getDownstreamCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getDownstreamCompanyName()) &&
                        !baContractStart.getDownstreamCompanyName()
                                .equals(baContractStartBefore.getDownstreamCompanyName()))) {
            isChangeList.add("downstreamCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getTerminalCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getTerminalCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getTerminalCompanyName()) &&
                        !baContractStart.getTerminalCompanyName()
                                .equals(baContractStartBefore.getTerminalCompanyName()))) {
            isChangeList.add("terminalCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getBusinessLines()) &&
                !StringUtils.isEmpty(baContractStartBefore.getBusinessLines())) || (
                !StringUtils.isEmpty(baContractStart.getBusinessLines()) &&
                        !baContractStart.getBusinessLines()
                                .equals(baContractStartBefore.getBusinessLines()))) {
            isChangeList.add("businessLines");
        }
        if ((StringUtils.isEmpty(baContractStart.getLabelingCompanyName()) &&
                !StringUtils.isEmpty(baContractStartBefore.getLabelingCompanyName())) || (
                !StringUtils.isEmpty(baContractStart.getLabelingCompanyName()) &&
                        !baContractStart.getLabelingCompanyName()
                                .equals(baContractStartBefore.getLabelingCompanyName()))) {
            isChangeList.add("labelingCompanyName");
        }
        if ((StringUtils.isEmpty(baContractStart.getBidType()) &&
                !StringUtils.isEmpty(baContractStartBefore.getBidType())) || (
                !StringUtils.isEmpty(baContractStart.getBidType()) &&
                        !baContractStart.getBidType()
                                .equals(baContractStartBefore.getBidType()))) {
            isChangeList.add("bidType");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getPrice()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getPrice())) || (
                !ObjectUtils.isEmpty(baContractStart.getPrice()) &&
                        !baContractStart.getPrice()
                                .equals(baContractStartBefore.getPrice()))) {
            isChangeList.add("price");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getTonnage()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getTonnage())) || (
                !ObjectUtils.isEmpty(baContractStart.getTonnage()) &&
                        !baContractStart.getTonnage()
                                .equals(baContractStartBefore.getTonnage()))) {
            isChangeList.add("tonnage");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getPerformanceStartTime()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getPerformanceStartTime())) || (
                !ObjectUtils.isEmpty(baContractStart.getPerformanceStartTime()) &&
                        !baContractStart.getPerformanceStartTime()
                                .equals(baContractStartBefore.getPerformanceStartTime()))) {
            isChangeList.add("performanceStartTime");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getPerformanceEndTime()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getPerformanceEndTime())) || (
                !ObjectUtils.isEmpty(baContractStart.getPerformanceEndTime()) &&
                        !baContractStart.getPerformanceEndTime()
                                .equals(baContractStartBefore.getPerformanceEndTime()))) {
            isChangeList.add("performanceEndTime");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getIndicatorContent()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getIndicatorContent())) || (
                !ObjectUtils.isEmpty(baContractStart.getIndicatorContent()) &&
                        !baContractStart.getIndicatorContent()
                                .equals(baContractStartBefore.getIndicatorContent()))) {
            isChangeList.add("indicatorContent");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getIndicatorAssessment()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getIndicatorAssessment())) || (
                !ObjectUtils.isEmpty(baContractStart.getIndicatorAssessment()) &&
                        !baContractStart.getIndicatorAssessment()
                                .equals(baContractStartBefore.getIndicatorAssessment()))) {
            isChangeList.add("indicatorAssessment");
        }
        if ((ObjectUtils.isEmpty(baContractStart.getQuantityAssessment()) &&
                !ObjectUtils.isEmpty(baContractStartBefore.getQuantityAssessment())) || (
                !ObjectUtils.isEmpty(baContractStart.getQuantityAssessment()) &&
                        !baContractStart.getQuantityAssessment()
                                .equals(baContractStartBefore.getQuantityAssessment()))) {
            isChangeList.add("quantityAssessment");
        }

        //指标信息
        BaEnterpriseRelevance enterpriseRelevance = baContractStart.getBaEnterpriseRelevance();
        BaEnterpriseRelevance enterpriseRelevanceBefore = baContractStartBefore.getBaEnterpriseRelevance();
        if (!ObjectUtils.isEmpty(enterpriseRelevance)
                && ObjectUtils.isEmpty(enterpriseRelevanceBefore)) {
            isChangeList.add("goodsName");
            isChangeList.add("target");
            isChangeList.add("monthlyConsumption");
        } else if (ObjectUtils.isEmpty(enterpriseRelevance) && !ObjectUtils.isEmpty(enterpriseRelevanceBefore)) {
            isChangeList.add("goodsName");
            isChangeList.add("target");
            isChangeList.add("monthlyConsumption");
        } else {
            if ((StringUtils.isEmpty(enterpriseRelevance.getGoodsId()) && !StringUtils.isEmpty(enterpriseRelevanceBefore.getGoodsId())) ||
                    (!StringUtils.isEmpty(enterpriseRelevance.getGoodsId()) && !enterpriseRelevance.getGoodsId().equals(enterpriseRelevanceBefore.getGoodsId()))) {
                isChangeList.add("goodsName");
            }
            if ((StringUtils.isEmpty(enterpriseRelevance.getTarget()) && StringUtils.isNotEmpty(enterpriseRelevanceBefore.getTarget())) ||
                    (StringUtils.isNotEmpty(enterpriseRelevance.getTarget()) && !enterpriseRelevance.getTarget().equals(enterpriseRelevanceBefore.getTarget()))) {
                isChangeList.add("target");
            }
            if ((enterpriseRelevance.getMonthlyConsumption() == null && enterpriseRelevanceBefore.getMonthlyConsumption() != null) ||
                    (enterpriseRelevance.getMonthlyConsumption() != null && enterpriseRelevanceBefore.getMonthlyConsumption() == null) ||
                    (enterpriseRelevance.getMonthlyConsumption().intValue() != enterpriseRelevanceBefore.getMonthlyConsumption().intValue())) {
                isChangeList.add("monthlyConsumption");
            }
        }

        //运输信息
        List<BaBidTransport> baBidTransports = baContractStart.getBaBidTransports();
        List<BaBidTransport> baBidTransportsBefore = baContractStartBefore.getBaBidTransports();
        Set<String> set = new HashSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        //返回之前版本指标参数数据ID集合
        if (!CollectionUtils.isEmpty(baBidTransportsBefore)) {
            for(BaBidTransport bidTransport : baBidTransportsBefore){
                stringBuilder = new StringBuilder();
                stringBuilder.append(bidTransport.getTransportType())
                        .append("_")
                        .append(bidTransport.getTransportCompanyName())
                        .append("_")
                        .append(bidTransport.getOrigin())
                        .append("_")
                        .append(bidTransport.getDestination())
                        .append("_")
                        .append(bidTransport.getEstimatedFreight())
                        .append("_")
                        .append(bidTransport.getIncidentalsCompany())
                        .append("_")
                        .append(bidTransport.getTransportIncidentals());
                set.add(stringBuilder.toString());
            }
        }
        if (!CollectionUtils.isEmpty(baBidTransports)
                && !CollectionUtils.isEmpty(baBidTransportsBefore)) {
            if (baBidTransports.size() != baBidTransportsBefore.size()) {
                for (int i = 0; i < baBidTransports.size(); i++) {
                    BaBidTransport baBidTransport = baBidTransports.get(i);
                    if(!ObjectUtils.isEmpty(baBidTransport)){
                        stringBuilder = new StringBuilder();
                        stringBuilder.append(baBidTransport.getTransportType())
                                .append("_")
                                .append(baBidTransport.getTransportCompanyName())
                                .append("_")
                                .append(baBidTransport.getOrigin())
                                .append("_")
                                .append(baBidTransport.getDestination())
                                .append("_")
                                .append(baBidTransport.getEstimatedFreight())
                                .append("_")
                                .append(baBidTransport.getIncidentalsCompany())
                                .append("_")
                                .append(baBidTransport.getTransportIncidentals());
                        if(!set.contains(stringBuilder.toString())) {
                            isChangeList.add(baBidTransport.getId() + "transportType");
                            isChangeList.add(baBidTransport.getId() + "transportCompanyName");
                            isChangeList.add(baBidTransport.getId() + "origin");
                            isChangeList.add(baBidTransport.getId() + "destination");
                            isChangeList.add(baBidTransport.getId() + "estimatedFreight");
                            isChangeList.add(baBidTransport.getId() + "incidentalsCompany");
                            isChangeList.add(baBidTransport.getId() + "transportIncidentals");
                        }
                    }
                }
            }
            //资金计划
//            List<BaDeclare> baDeclares = baContractStart.getBaDeclares();
//            List<BaDeclare> baDeclaresBefore = baContractStartBefore.getBaDeclares();
//            Map<String, BaDeclare> baDeclaresBeforeMap = new HashMap<>();
//            //返回之前版本指标参数数据ID集合
//            if (!CollectionUtils.isEmpty(baDeclaresBefore)) {
//                baDeclaresBeforeMap = baDeclaresBefore
//                        .stream().collect(Collectors.toMap(BaDeclare::getId, BaDeclare -> BaDeclare));
//            }
//            if (!CollectionUtils.isEmpty(baDeclares)
//                    && !CollectionUtils.isEmpty(baDeclaresBefore)) {
//                if (baDeclares.size() != baDeclaresBefore.size()) {
//                    for (int i = 0; i < baDeclares.size(); i++) {
//                        BaDeclare baDeclare = baDeclares.get(i);
//                        BaDeclare baDeclareBefore = baDeclaresBeforeMap.get(baDeclare.getId());
//                        if((ObjectUtils.isEmpty(baDeclare) && !ObjectUtils.isEmpty(baDeclareBefore)) ||
//                                (!ObjectUtils.isEmpty(baDeclare) && ObjectUtils.isEmpty(baDeclareBefore))){
//                            if(!ObjectUtils.isEmpty(baDeclare)){
//                                isChangeList.add(baDeclare.getId() + "planTime");
//                                isChangeList.add(baDeclare.getId() + "freightMoney");
//                                isChangeList.add(baDeclare.getId() + "loanMoney");
//                                isChangeList.add(baDeclare.getId() + "paymentMoney");
//                            } else if(!ObjectUtils.isEmpty(baDeclareBefore)){
//                                isChangeList.add(baDeclareBefore.getId() + "planTime");
//                                isChangeList.add(baDeclareBefore.getId() + "freightMoney");
//                                isChangeList.add(baDeclareBefore.getId() + "loanMoney");
//                                isChangeList.add(baDeclareBefore.getId() + "paymentMoney");
//                            }
//                        } else {
//                            if (!baDeclare.getPlanTime().equals(baDeclareBefore.getPlanTime())) {
//                                isChangeList.add(baDeclare.getId() + "planTime");
//                            }
//                            if (!baDeclare.getFreightMoney().equals(baDeclareBefore.getFreightMoney())) {
//                                isChangeList.add(baDeclare.getId() + "freightMoney");
//                            }
//                            if (!baDeclare.getLoanMoney().equals(baDeclareBefore.getLoanMoney())) {
//                                isChangeList.add(baDeclare.getId() + "loanMoney");
//                            }
//                            if (!baDeclare.getPaymentMoney().equals(baDeclareBefore.getPaymentMoney())) {
//                                isChangeList.add(baDeclare.getId() + "paymentMoney");
//                            }
//                        }
//                    }
//                }
//            }

            if ((ObjectUtils.isEmpty(baContractStart.getServiceManagerName()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getServiceManagerName())) || (
                    !ObjectUtils.isEmpty(baContractStart.getServiceManagerName()) &&
                            !baContractStart.getServiceManagerName()
                                    .equals(baContractStartBefore.getServiceManagerName()))) {
                isChangeList.add("serviceManagerName");
            }
            if ((ObjectUtils.isEmpty(baContractStart.getDivisionName()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getDivisionName())) || (
                    !ObjectUtils.isEmpty(baContractStart.getDivisionName()) &&
                            !baContractStart.getDivisionName()
                                    .equals(baContractStartBefore.getDivisionName()))) {
                isChangeList.add("divisionName");
            }
            if ((ObjectUtils.isEmpty(baContractStart.getPersonChargeName()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getPersonChargeName())) || (
                    !ObjectUtils.isEmpty(baContractStart.getPersonChargeName()) &&
                            !baContractStart.getPersonChargeName()
                                    .equals(baContractStartBefore.getPersonChargeName()))) {
                isChangeList.add("personChargeName");
            }
            if ((ObjectUtils.isEmpty(baContractStart.getLowExternalContactPerson()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getLowExternalContactPerson())) || (
                    !ObjectUtils.isEmpty(baContractStart.getLowExternalContactPerson()) &&
                            !baContractStart.getLowExternalContactPerson()
                                    .equals(baContractStartBefore.getLowExternalContactPerson()))) {
                isChangeList.add("lowExternalContactPerson");
            }
            if ((ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                    !ObjectUtils.isEmpty(baContractStartBefore.getExternalContactPerson())) || (
                    !ObjectUtils.isEmpty(baContractStart.getExternalContactPerson()) &&
                            !baContractStart.getExternalContactPerson()
                                    .equals(baContractStartBefore.getExternalContactPerson()))) {
                isChangeList.add("externalContactPerson");
            }
        }
        return isChangeList;
    }

    public static String extractFileName(String path) {
        // 正则表达式匹配最后一个斜杠后的内容，直到点号
        String regex = "/([^/]+)\\.[^/]*$";
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
        java.util.regex.Matcher matcher = pattern.matcher(path);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null; // 如果没有匹配，则返回null
    }
}
