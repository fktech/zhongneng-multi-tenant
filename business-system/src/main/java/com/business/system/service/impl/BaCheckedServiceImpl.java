package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 盘点Service业务层处理
 *
 * @author single
 * @date 2023-01-05
 */
@Service
public class BaCheckedServiceImpl extends ServiceImpl<BaCheckedMapper, BaChecked> implements IBaCheckedService
{
    private static final Logger log = LoggerFactory.getLogger(BaCheckedServiceImpl.class);

    @Autowired
    private BaCheckedMapper baCheckedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaCheckedHeadMapper baCheckedHeadMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaBatchMapper baBatchMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private IBaCheckedHeadService baCheckedHeadService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    @Override
    public BaChecked selectBaCheckedById(String id)
    {
        BaChecked baChecked = baCheckedMapper.selectBaCheckedById(id);
        if(!ObjectUtils.isEmpty(baChecked)){
            BaCheckedHead baCheckedHead = new BaCheckedHead();
            baCheckedHead.setCheckedId(id);
            //盘点信息
            List<BaCheckedHead> baCheckedHeadList = baCheckedHeadMapper.selectBaCheckedHeadListByCheckId(baCheckedHead);
            baCheckedHeadList.stream().forEach(item -> {
                //设置盘点状态
                if(StringUtils.isEmpty(item.getCheckedState())){
                    //设置盘点状态为未盘点
                    item.setCheckedState("1");
                }
                /*BaBatch baBatch = baBatchMapper.selectBaBatchById(item.getBatch());
                if(!ObjectUtils.isEmpty(baBatch)){
                    //设置批次号
                    item.setBatchNum(baBatch.getBatch());
                }*/
            });
            //判断是否有盘点id;
            List<BaCheckedHead> baCheckedHeads = new ArrayList<>();
            for (BaCheckedHead baCheckedHead1:baCheckedHeadList) {
                if(StringUtils.isNotEmpty(baCheckedHead1.getCheckedId())){
                    baCheckedHeads.add(baCheckedHead1);
                }
            }
            baChecked.setBaCheckedHeadList(baCheckedHeads);
        }


        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baChecked.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baChecked.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baChecked.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baChecked.setUser(sysUser);
        return baChecked;
    }

    /**
     * 查询盘点列表
     *
     * @param baChecked 盘点
     * @return 盘点
     */
    @Override
    public List<BaChecked> selectBaCheckedList(BaChecked baChecked)
    {
        List<BaChecked> baCheckeds = baCheckedMapper.selectBaCheckedList(baChecked);
        baCheckeds.stream().forEach(item -> {
            //盘点详情
            BaCheckedHead baCheckedHead = new BaCheckedHead();
            baCheckedHead.setCheckedId(item.getId());
            List<BaCheckedHead> baCheckedHeads = baCheckedHeadMapper.selectBaCheckedHeadListByCheckId(baCheckedHead);
            if(baCheckedHeads.size() > 0){
                String name = "";
                for (BaCheckedHead baCheckedHead1:baCheckedHeads) {
                    //拼接项目名称
                    name = baCheckedHead1.getDepName() + ',' + name;
                }
                //拼接项目名称

                item.setDeptName(name.substring(0,name.length()-1));
            }
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位信息
                String name = getName.getName(item.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                }

            }
        });
        return baCheckeds;
    }

    /**
     * 新增盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertBaChecked(BaChecked baChecked)
    {
        baChecked.setId("PD" + getRedisIncreID.getId());
        baChecked.setCreateTime(DateUtils.getNowDate());
        baChecked.setCreateBy(SecurityUtils.getUsername());
        baChecked.setUserId(SecurityUtils.getUserId());
        baChecked.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baChecked.setTenantId(SecurityUtils.getCurrComId());
        List<BaCheckedHead> baCheckedHeadList = baChecked.getBaCheckedHeadList();

        String baCheckedResultType = ""; //盘点信息盘点结果
        if(!CollectionUtils.isEmpty(baCheckedHeadList)){
            for(BaCheckedHead checkedHead : baCheckedHeadList){
                checkedHead.setCheckedState("2"); //设置盘点状态为已盘点
                checkedHead.setCheckedId(baChecked.getId());//详情表中添加盘点ID
                if(StringUtils.isEmpty(baCheckedResultType)){
                    if("2".equals(checkedHead.getResultType()) || "3".equals(checkedHead.getResultType())){
                        //设置盘点状态异常
                        baCheckedResultType = "2";
                    } else {
                        //设置盘点状态正常
                        baCheckedResultType = "1";
                    }
                }
                //库存信息
                BaMaterialStock materialStock = baMaterialStockMapper.selectBaMaterialStockById(checkedHead.getMaterialId());
                checkedHead.setInventoryAverage(materialStock.getInventoryAverage());
                baCheckedHeadService.insertBaCheckedHead(checkedHead);
            }
        }
        baChecked.setResultType(baCheckedResultType);
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baChecked.getWorkState())) {
            baChecked.setWorkState("1");
            //默认审批流程已通过
            baChecked.setState(AdminCodeEnum.CHECKED_STATUS_PASS.getCode());
        }

        //关闭流程开始
        BaMaterialStock materialStock = null;
        //获取盘点详情
        QueryWrapper<BaCheckedHead> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("checked_id", baChecked.getId());
        queryWrapper.eq("flag", "0");
        baCheckedHeadList = baCheckedHeadMapper.selectList(queryWrapper);
        if(!CollectionUtils.isEmpty(baCheckedHeadList)){
            for(BaCheckedHead baCheckedHead : baCheckedHeadList){
                //根据盘点库存ID查询库存数据
                materialStock = baMaterialStockMapper.selectBaMaterialStockById(baCheckedHead.getMaterialId());
                if(!ObjectUtils.isEmpty(materialStock)){
                    //审批通过后更新盘点量为此总量
                    materialStock.setInventoryTotal(baCheckedHead.getCheckedCount());
                    //审批通过后更新库存均价为此盘点均价
                    materialStock.setInventoryAverage(baCheckedHead.getCheckedAverage());
                    //库存总量（吨）
                    if(materialStock.getInventoryTotal() != null){
                        if(materialStock.getInventoryTotal().intValue() == 0){
                            materialStock.setAverageCalorific(new BigDecimal(0));
                        } else if(materialStock.getAllAverageCalorific() != null){
                            //平均热值等于库存总热值除以库存总量
                            materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(),2,BigDecimal.ROUND_HALF_UP));
                        }
                    }
                    //计算盘点货值累计
                    if(baCheckedHead.getCheckedAverage() == null){
                        baCheckedHead.setCheckedAverage(new BigDecimal(0));
                    }
                    BigDecimal multiply = baCheckedHead.getCheckedAverage().multiply(baCheckedHead.getDifference().setScale(2, BigDecimal.ROUND_HALF_UP));
                    if(materialStock.getCheckedCargoValue() == null){
                        materialStock.setCheckedCargoValue(new BigDecimal(0));
                    }
                    //库存入库货值累计 + 本次入库货值
                    materialStock.setCheckedCargoValue(materialStock.getCheckedCargoValue().add(multiply));
                }
            }

            //盘点计算货值均价
            if(materialStock.getCheckedCargoValue() == null){
                materialStock.setCheckedCargoValue(new BigDecimal(0));
            }

            if(materialStock.getInventoryAverage() == null){
                materialStock.setInventoryAverage(new BigDecimal(0));
            }
            if(materialStock.getInventoryTotal() == null){
                materialStock.setInventoryTotal(new BigDecimal(0));
            }
            if(StringUtils.isNotNull(materialStock.getOutputDepotCargoValue())&&StringUtils.isNotNull(materialStock.getCheckedCargoValue())){
                BigDecimal subtract = materialStock.getIntoDepotCargoValue()
                        .subtract(materialStock.getOutputDepotCargoValue())
                        .subtract(materialStock.getCheckedCargoValue());
                if(subtract != null && materialStock.getInventoryTotal() != null && subtract.intValue() > 0 && materialStock.getInventoryTotal().intValue() > 0) {
                    materialStock.setInventoryAverage(subtract
                            .divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
                }
            }

            //计算库存均价

            baMaterialStockMapper.updateBaMaterialStock(materialStock);
        }
//        关闭流程结束

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode(),SecurityUtils.getCurrComId());
        baChecked.setFlowId(flowId);
        Integer result = baCheckedMapper.insertBaChecked(baChecked);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baChecked.getWorkState())) {
                baChecked = baCheckedMapper.selectBaCheckedById(baChecked.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baChecked, AdminCodeEnum.CHECKED_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baCheckedMapper.deleteBaCheckedById(baChecked.getId());
                    baCheckedHeadMapper.deleteBaCheckedHeadByCheckId(baChecked.getId());
                    return 0;
                } else {
                    BaChecked baChecked2 = baCheckedMapper.selectBaCheckedById(baChecked.getId());
                    baChecked2.setState(AdminCodeEnum.CHECKED_STATUS_VERIFYING.getCode());
                    baCheckedMapper.updateBaChecked(baChecked2);
                }
            }
        }
        return 1;
    }

    /**
     * 盘点提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaChecked checked, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(checked.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(checked.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode());
        //获取流程实例关联的业务对象
        BaChecked baChecked = baCheckedMapper.selectBaCheckedById(checked.getId());
        SysUser sysUser = iSysUserService.selectUserById(baChecked.getUserId());
        baChecked.setUserName(sysUser.getUserName());
        baChecked.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baChecked));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    @Override
    public int updateBaChecked(BaChecked baChecked)
    {
        baChecked.setUpdateTime(DateUtils.getNowDate());
        baChecked.setUpdateBy(SecurityUtils.getUsername());
        //租户ID
        baChecked.setTenantId(SecurityUtils.getCurrComId());
        //修改盘点详情信息
        if(!ObjectUtils.isEmpty(baChecked.getBaCheckedHeadList())){
            for (BaCheckedHead baCheckedHead:baChecked.getBaCheckedHeadList()) {
                if(StringUtils.isNotEmpty(baCheckedHead.getId())){
                    baCheckedHeadMapper.updateBaCheckedHead(baCheckedHead);
                }else {
                    baCheckedHead.setCheckedState("2"); //设置盘点状态为已盘点
                    baCheckedHead.setCheckedId(baChecked.getId());//详情表中添加盘点ID
                    baCheckedHeadService.insertBaCheckedHead(baCheckedHead);
                }
            }
        }
        //查询已盘点数据
        BaCheckedHead baCheckedHead = new BaCheckedHead();
        baCheckedHead.setCheckedId(baChecked.getId());
        baCheckedHead.setCheckedState("2"); //查询盘点状态为已盘点
        List<BaCheckedHead> baCheckedHeadList = baCheckedHeadMapper.selectBaCheckedHeadListByCheckId(baCheckedHead);
        String baCheckedResultType = ""; //盘点信息盘点结果
        for(BaCheckedHead checkedHead : baCheckedHeadList){
            if(StringUtils.isEmpty(baCheckedResultType)){
                if("2".equals(checkedHead.getResultType()) || "3".equals(checkedHead.getResultType())){
                    //设置盘点状态异常
                    baCheckedResultType = "2";
                } else {
                    //设置盘点状态正常
                    baCheckedResultType = "1";
                }
            }
        }
        baChecked.setResultType(baCheckedResultType);
        Integer result = baCheckedMapper.updateBaChecked(baChecked);
        if(result > 0){
            BaChecked checked = baCheckedMapper.selectBaCheckedById(baChecked.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",checked.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(checked, AdminCodeEnum.CHECKED_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                checked.setState(AdminCodeEnum.CHECKED_STATUS_VERIFYING.getCode());
                baCheckedMapper.updateBaChecked(checked);
            }
        }
        return result;
    }

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的盘点ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteBaCheckedByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaChecked baChecked = baCheckedMapper.selectBaCheckedById(id);
                QueryWrapper<BaCheckedHead> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("checked_id",baChecked.getId());
                queryWrapper.eq("flag",0);
                List<BaCheckedHead> baCheckedHeadList = baCheckedHeadMapper.selectList(queryWrapper);
                if(!CollectionUtils.isEmpty(baCheckedHeadList)){
                    baCheckedHeadList.stream().forEach(item -> {
                        item.setFlag(new Long(1));
                        baCheckedHeadMapper.updateBaCheckedHead(item);
                    });
                }
                baChecked.setFlag(new Long(1));
                baCheckedMapper.updateBaChecked(baChecked);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除盘点信息
     *
     * @param id 盘点ID
     * @return 结果
     */
    @Override
    public int deleteBaCheckedById(String id)
    {
        return baCheckedMapper.deleteBaCheckedById(id);
    }

    /**
     * 获取盘点ID
     *
     * @return 盘点
     */
    @Override
    public String getBaCheckedID() {
        return "PD"+getRedisIncreID.getId();
    }

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    @Override
    public Integer revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询盘点信息
            BaChecked baChecked = baCheckedMapper.selectBaCheckedById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baChecked.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CHECKED_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baChecked.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //盘点流程审批状态修改
                baChecked.setState(AdminCodeEnum.CHECKED_STATUS_WITHDRAW.getCode());

                baCheckedMapper.updateBaChecked(baChecked);
                String userId = String.valueOf(baChecked.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baChecked, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaChecked baChecked, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baChecked.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"盘点审批提醒",msgContent,baMessage.getType(),baChecked.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
