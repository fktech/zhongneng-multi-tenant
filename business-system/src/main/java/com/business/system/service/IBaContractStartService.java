package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaProject;
import com.business.system.domain.dto.DropDownBox;
import com.business.system.domain.dto.paymentReceivedDTO;
import com.business.system.domain.vo.BaContractStartTonVO;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.CountContractStartDTO;
import com.business.system.domain.vo.BaContractStartVO;

/**
 * 合同启动Service接口
 *
 * @author ljb
 * @date 2023-03-06
 */
public interface IBaContractStartService
{
    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    public BaContractStart selectBaContractStartById(String id);

    /**
     * 查询合同启动列表
     *
     * @param baContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaContractStart> selectBaContractStartList(BaContractStart baContractStart);

    public List<BaContractStart> baContractStartList(BaContractStart baContractStart);

    //弹框下拉
    public List<BaContractStart> selectContractStartList(BaContractStart baContractStart);

    /**
     * 新增合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int insertBaContractStart(BaContractStart baContractStart);

    /**
     * 修改合同启动
     *
     * @param baContractStart 合同启动
     * @return 结果
     */
    public int updateBaContractStart(BaContractStart baContractStart);

    /**
     * 批量删除合同启动
     *
     * @param ids 需要删除的合同启动ID
     * @return 结果
     */
    public int deleteBaContractStartByIds(String[] ids);

    /**
     * 删除合同启动信息
     *
     * @param id 合同启动ID
     * @return 结果
     */
    public int deleteBaContractStartById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 新增差额
     */
    public int difference(BaContractStart baContractStart);

    /**
     * 合同名称
     */
    public UR contractStartName(String projectId,String type);

    /**
     * 合同启动编号
     */
    public String contractStartSerial(String project);

    /**
     * 合同启动面板
     */
    public List<BaContractStart> contractStartList(BaContractStartDTO baContractStartDTO);

    /**
     * 查询合同信息
     * @param baContractStart
     * @return
     */
    public List<BaContractStart> selectOrderList(BaContractStart baContractStart);

    /**
     * 新增订单
     *
     * @param baContractStart 新增订单
     * @return 结果
     */
    public int insertOrder(BaContractStart baContractStart);

    /**
     * 修改订单
     *
     * @param baContractStart 修改订单
     * @return 结果
     */
    public int updateOrder(BaContractStart baContractStart);

    public int associatedBusiness(BaContractStart baContractStart);

    /**
     * 合同启动下拉
     */
    //public List<StartComboBox> comboBox();

    /**
     * 订单撤销按钮
     * @param id
     * @return
     */
    public int revokeOrder(String id);

    /**
     * 订单完结
     * @param id
     * @return
     */
    public int complete(String id);

    /**
     * 人员定责下拉框
     * @return
     */
    public List<SysUser> deptUser();

    /**
     * 业务统计
     * @param startId
     * @return
     */
    public Map<String, BigDecimal> businessCount(String startId);

    /**
     * PC首页合同启动面板统计
     */
    public UR homeStartStat(CountContractStartDTO countContractStartDTO);

    /**
     * 首页合同启动面板统计
     */
    public UR contractStartListStat(BaContractStartDTO baContractStartDTO);

    /**
     * 首页合同启动面板列表
     */
    public List<BaContractStart> homeContractStartList(BaContractStartDTO baContractStartDTO);

    public List<BaContractStart> homeContractStart(BaContractStartDTO baContractStartDTO);

    /**
     * 移动端合同启动面板列表
     */
    public List<BaContractStart> contractLaunchPanel(BaContractStartDTO baContractStartDTO);

    /**
     * 移动端合同启动面板列表详情
     */
    public BaContractStartVO contractLaunchPanelDetails(String id);

    /**
     * 移动端合同启动面板列表详情
     */
    public UR illuminate(String id);

    /**
     * PC端首页合同启动统计
     */
    public int countContractStartList(CountContractStartDTO countContractStartDTO);

    /**
     * 关联已签订合同
     */
    public int bindingContract(BaContractStart baContractStart);

    public List<BaContractStartTonVO> countContractStartListByType();

    /**
     * 变更合同启动
     * @param baContractStart
     * @return
     */
    public int changeBaContractStart(BaContractStart baContractStart);

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> selectChangeDataList(String id);

    /**
     * 收入占比
     */
    public List<BaContractStartTonVO> contractStartListByType(BaContractStartTonVO baContractStartTonVO);

    /**
     * 查询多租户授权信息合同启动列表
     * @param baContractStart
     * @return
     */
    public List<BaContractStart> selectBaContractStartAuthorizedList(BaContractStart baContractStart);

    /**
     * 授权合同启动
     * @param baContractStart
     * @return
     */
    public int authorizedBaContractStart(BaContractStart baContractStart);

    /**
     * 查询多租户授权信息销售申请列表
     */
    public List<BaContractStart> selectOrderAuthorizedList(BaContractStart baContractStart);

    /**
     * 收付款信息
     */
    public paymentReceivedDTO integration(String id);

    /**
     * 上游公司
     */
    public List<DropDownBox> upstreamCompanies(String name);
    /**
     * 下游公司
     */
    public List<DropDownBox> downstreamCompanies(String type,String name);
}
