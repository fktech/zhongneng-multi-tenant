package com.business.system.service;

import java.util.List;

import com.business.system.domain.BaClaim;
import com.business.system.domain.BaClaimHistory;

/**
 * 认领历史Service接口
 *
 * @author ljb
 * @date 2023-01-31
 */
public interface IBaClaimHistoryService
{
    /**
     * 查询认领历史
     *
     * @param id 认领历史ID
     * @return 认领历史
     */
    public BaClaimHistory selectBaClaimHistoryById(String id);

    /**
     * 查询认领历史列表
     *
     * @param baClaimHistory 认领历史
     * @return 认领历史集合
     */
    public List<BaClaimHistory> selectBaClaimHistoryList(BaClaimHistory baClaimHistory);

    public List<BaClaim> selectBaClaimList(BaClaimHistory baClaimHistory);

    /**
     * 新增认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    public int insertBaClaimHistory(BaClaimHistory baClaimHistory);

    /**
     * 修改认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    public int updateBaClaimHistory(BaClaimHistory baClaimHistory);

    /**
     * 批量删除认领历史
     *
     * @param ids 需要删除的认领历史ID
     * @return 结果
     */
    public int deleteBaClaimHistoryByIds(String[] ids);

    /**
     * 删除认领历史信息
     *
     * @param id 认领历史ID
     * @return 结果
     */
    public int deleteBaClaimHistoryById(String id);


    public int countBaClaimHistoryList(BaClaimHistory baClaimHistory);
}
