package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaAutomobileSettlementInvoiceDetail;

/**
 * 无车承运开票详情Service接口
 *
 * @author single
 * @date 2023-04-13
 */
public interface IBaAutomobileSettlementInvoiceDetailService
{
    /**
     * 查询无车承运开票详情
     *
     * @param id 无车承运开票详情ID
     * @return 无车承运开票详情
     */
    public BaAutomobileSettlementInvoiceDetail selectBaAutomobileSettlementInvoiceDetailById(String id);

    /**
     * 查询无车承运开票详情列表
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 无车承运开票详情集合
     */
    public List<BaAutomobileSettlementInvoiceDetail> selectBaAutomobileSettlementInvoiceDetailList(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 新增无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 修改无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail);

    /**
     * 批量删除无车承运开票详情
     *
     * @param ids 需要删除的无车承运开票详情ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceDetailByIds(String[] ids);

    /**
     * 删除无车承运开票详情信息
     *
     * @param id 无车承运开票详情ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceDetailById(String id);


}
