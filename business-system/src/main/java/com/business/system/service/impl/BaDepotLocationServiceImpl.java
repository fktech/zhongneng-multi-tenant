package com.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaDepotLocation;
import com.business.system.mapper.BaDepotLocationMapper;
import com.business.system.service.IBaDepotLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * 仓库库位信息Service业务层处理
 *
 * @author single
 * @date 2023-09-06
 */
@Service
public class BaDepotLocationServiceImpl extends ServiceImpl<BaDepotLocationMapper, BaDepotLocation> implements IBaDepotLocationService {
    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 仓库库位信息
     */
    @Override
    public BaDepotLocation selectBaDepotLocationById(String id) {
        return baDepotLocationMapper.selectBaDepotLocationById(id);
    }

    /**
     * 查询仓库库位信息列表
     *
     * @param baDepotLocation 仓库库位信息
     * @return 仓库库位信息
     */
    @Override
    public List<BaDepotLocation> selectBaDepotLocationList(BaDepotLocation baDepotLocation) {
        return baDepotLocationMapper.selectBaDepotLocationList(baDepotLocation);
    }

    /**
     * 新增仓库库位信息
     *
     * @param baDepotLocation 仓库库位信息
     * @return 结果
     */
    @Override
    public int insertBaDepotLocation(BaDepotLocation baDepotLocation) {

        //判断库位编号是否存在
        QueryWrapper<BaDepotLocation> baDepotLocationCodeQueryWrapper = null;
        BaDepotLocation depotLocation = null;
        if (!ObjectUtils.isEmpty(baDepotLocation)) {
            baDepotLocationCodeQueryWrapper = new QueryWrapper<BaDepotLocation>();
            baDepotLocationCodeQueryWrapper.eq("code", baDepotLocation.getCode());
            baDepotLocationCodeQueryWrapper.eq("flag", 0);
            depotLocation = baDepotLocationMapper.selectOne(baDepotLocationCodeQueryWrapper);
            if (!ObjectUtils.isEmpty(depotLocation)) {
                return -2;
            }
        }

        //判断页面传过来的库位名称是否有重复
        QueryWrapper<BaDepotLocation> baDepotLocationNameQueryWrapper = null;
        if (!ObjectUtils.isEmpty(baDepotLocation)) {
            baDepotLocationNameQueryWrapper = new QueryWrapper<BaDepotLocation>();
            baDepotLocationNameQueryWrapper.eq("name", baDepotLocation.getName());
            baDepotLocationNameQueryWrapper.eq("flag", 0);
            depotLocation = baDepotLocationMapper.selectOne(baDepotLocationNameQueryWrapper);
            if (!ObjectUtils.isEmpty(depotLocation)) {
                return -3;
            }
        }
        baDepotLocation.setId(getRedisIncreID.getId());
        baDepotLocation.setCreateBy(SecurityUtils.getUsername());
        baDepotLocation.setCreateTime(DateUtils.getNowDate());
        baDepotLocation.setUserId(SecurityUtils.getUserId());
        baDepotLocation.setDeptId(SecurityUtils.getDeptId());
        return baDepotLocationMapper.insertBaDepotLocation(baDepotLocation);
    }

    /**
     * 修改仓库库位信息
     *
     * @param baDepotLocation 仓库库位信息
     * @return 结果
     */
    @Override
    public int updateBaDepotLocation(BaDepotLocation baDepotLocation) {

        //判断库位编码是否已存在
        QueryWrapper<BaDepotLocation> baDepotLocationCodeQueryWrapper = null;
        BaDepotLocation depotLocation = null;
        if (!ObjectUtils.isEmpty(baDepotLocation)) {
            baDepotLocationCodeQueryWrapper = new QueryWrapper<BaDepotLocation>();
            baDepotLocationCodeQueryWrapper.eq("code", baDepotLocation.getCode());
            baDepotLocationCodeQueryWrapper.eq("flag", 0);
            depotLocation = baDepotLocationMapper.selectOne(baDepotLocationCodeQueryWrapper);
            if(!ObjectUtils.isEmpty(depotLocation) && !depotLocation.getId().equals(baDepotLocation.getId())){
                if (!ObjectUtils.isEmpty(depotLocation) && depotLocation.getCode().equals(baDepotLocation.getCode())) {
                    return -2;
                }
            }
        }

        //判断页面传过来的库位名称是否有重复
        QueryWrapper<BaDepotLocation> baDepotLocationNameQueryWrapper = null;
        if (!ObjectUtils.isEmpty(baDepotLocation)) {
            baDepotLocationNameQueryWrapper = new QueryWrapper<BaDepotLocation>();
            baDepotLocationNameQueryWrapper.eq("name", baDepotLocation.getName());
            baDepotLocationNameQueryWrapper.eq("flag", 0);
            depotLocation = baDepotLocationMapper.selectOne(baDepotLocationNameQueryWrapper);
            if(!ObjectUtils.isEmpty(depotLocation) && !depotLocation.getId().equals(baDepotLocation.getId())){
                if (!ObjectUtils.isEmpty(depotLocation) && depotLocation.getName().equals(baDepotLocation.getName())) {
                    return -3;
                }
            }

        }

        baDepotLocation.setUpdateBy(SecurityUtils.getUsername());
        baDepotLocation.setUpdateTime(DateUtils.getNowDate());
        return baDepotLocationMapper.updateBaDepotLocation(baDepotLocation);
    }

    /**
     * 批量删除仓库库位信息
     *
     * @param ids 需要删除的仓库库位信息ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotLocationByIds(String[] ids) {
        if (StringUtils.isNotEmpty(ids)) {
            for (String id : ids) {
                BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(id);
                baDepotLocation.setFlag(1);
                baDepotLocationMapper.updateBaDepotLocation(baDepotLocation);
            }
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除仓库库位信息信息
     *
     * @param id 仓库库位信息ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotLocationById(String id) {
        return baDepotLocationMapper.deleteBaDepotLocationById(id);
    }

}
