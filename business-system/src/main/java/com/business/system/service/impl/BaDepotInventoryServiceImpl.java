package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaDepotInventoryMapper;
import com.business.system.domain.BaDepotInventory;
import com.business.system.service.IBaDepotInventoryService;

/**
 * 入库清单Service业务层处理
 *
 * @author ljb
 * @date 2023-09-07
 */
@Service
public class BaDepotInventoryServiceImpl extends ServiceImpl<BaDepotInventoryMapper, BaDepotInventory> implements IBaDepotInventoryService
{
    @Autowired
    private BaDepotInventoryMapper baDepotInventoryMapper;

    /**
     * 查询入库清单
     *
     * @param id 入库清单ID
     * @return 入库清单
     */
    @Override
    public BaDepotInventory selectBaDepotInventoryById(String id)
    {
        return baDepotInventoryMapper.selectBaDepotInventoryById(id);
    }

    /**
     * 查询入库清单列表
     *
     * @param baDepotInventory 入库清单
     * @return 入库清单
     */
    @Override
    public List<BaDepotInventory> selectBaDepotInventoryList(BaDepotInventory baDepotInventory)
    {
        return baDepotInventoryMapper.selectBaDepotInventoryList(baDepotInventory);
    }

    /**
     * 新增入库清单
     *
     * @param baDepotInventory 入库清单
     * @return 结果
     */
    @Override
    public int insertBaDepotInventory(BaDepotInventory baDepotInventory)
    {
        baDepotInventory.setCreateTime(DateUtils.getNowDate());
        return baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);
    }

    /**
     * 修改入库清单
     *
     * @param baDepotInventory 入库清单
     * @return 结果
     */
    @Override
    public int updateBaDepotInventory(BaDepotInventory baDepotInventory)
    {
        baDepotInventory.setUpdateTime(DateUtils.getNowDate());
        return baDepotInventoryMapper.updateBaDepotInventory(baDepotInventory);
    }

    /**
     * 批量删除入库清单
     *
     * @param ids 需要删除的入库清单ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotInventoryByIds(String[] ids)
    {
        return baDepotInventoryMapper.deleteBaDepotInventoryByIds(ids);
    }

    /**
     * 删除入库清单信息
     *
     * @param id 入库清单ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotInventoryById(String id)
    {
        return baDepotInventoryMapper.deleteBaDepotInventoryById(id);
    }

}
