package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaAutomobileSettlementInvoiceCmst;

/**
 * 中储智运无车承运开票Service接口
 *
 * @author single
 * @date 2023-08-21
 */
public interface IBaAutomobileSettlementInvoiceCmstService
{
    /**
     * 查询中储智运无车承运开票
     *
     * @param id 中储智运无车承运开票ID
     * @return 中储智运无车承运开票
     */
    public BaAutomobileSettlementInvoiceCmst selectBaAutomobileSettlementInvoiceCmstById(String id);

    /**
     * 查询中储智运无车承运开票列表
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 中储智运无车承运开票集合
     */
    public List<BaAutomobileSettlementInvoiceCmst> selectBaAutomobileSettlementInvoiceCmstList(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 新增中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 修改中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst);

    /**
     * 批量删除中储智运无车承运开票
     *
     * @param ids 需要删除的中储智运无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceCmstByIds(String[] ids);

    /**
     * 删除中储智运无车承运开票信息
     *
     * @param id 中储智运无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceCmstById(String id);

    /**
     * 中储推送发票（发票信息）
     */
    int added(JSONObject jsonObject);




}
