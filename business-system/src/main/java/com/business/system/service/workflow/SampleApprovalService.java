package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.waybillDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 取样审批接口 服务类
 * @date: 2024/1/5 11:10
 */
@Service
public class SampleApprovalService {

    @Autowired
    protected BaSampleAssayMapper baSampleAssayMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;


    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaSampleAssay baSampleAssay = new BaSampleAssay();
        baSampleAssay.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baSampleAssay = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
        String userId = String.valueOf(baSampleAssay.getUserId());
        if(AdminCodeEnum.SAMPLE_STATUS_PASS.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //插入消息通知信息
            this.insertMessage(baSampleAssay, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.SAMPLE_STATUS_REJECT.getCode().equals(status)){
            //判断出入库
            if(StringUtils.isNotEmpty(baSampleAssay.getHeadType())){
                //入库
                if("1".equals(baSampleAssay.getHeadType())){
                    //释放对应运单状态
                    if(StringUtils.isNotEmpty(baSampleAssay.getThdno())){
                        String[] split = baSampleAssay.getThdno().split(",");
                        for (String thdno:split) {
                            //查询运单信息标记
                            BaShippingOrder baShippingOrder = new BaShippingOrder();
                            baShippingOrder.setThdno(thdno);
                            List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                            if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                                BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(thdno);
                                baShippingOrder1.setExperimentId("0");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                            }else if("2".equals(waybillDTOList.get(0).getType())){
                                BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(thdno);
                                baShippingOrderCmst.setExperimentId("0");
                                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                            }
                        }
                    }
                }else if("2".equals(baSampleAssay.getHeadType())){
                    if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                        //查询运单信息标记
                        BaShippingOrder baShippingOrder = new BaShippingOrder();
                        baShippingOrder.setSupplierNo(baSampleAssay.getGoodsSource());
                        List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                        for (waybillDTO waybillDTO:waybillDTOList) {
                            if("1".equals(waybillDTO.getType()) || "3".equals(waybillDTO.getType())){
                                BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(waybillDTO.getThdno());
                                baShippingOrder1.setExperimentId("0");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                            }else if("2".equals(waybillDTOList.get(0).getType())){
                                BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getThdno());
                                baShippingOrderCmst.setExperimentId("0");
                                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                            }
                        }
                    }
                }
            }
            //释放对应的货源
            if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                //查询货源下所有运单
                BaShippingOrder baShippingOrder = new BaShippingOrder();
                baShippingOrder.setSupplierNo(baSampleAssay.getGoodsSource());
                List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                    QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                    shippingOrderQueryWrapper.eq("experiment_id",0);
                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                    if(waybillDTOList.size() == shippingOrders.size()){
                        BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(waybillDTOList.get(0).getSupplierNo());
                        baTransportAutomobile1.setSampleType("0");
                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                    }
                }else if("2".equals(waybillDTOList.get(0).getType())){
                    QueryWrapper<BaShippingOrderCmst> cmstQueryWrapper = new QueryWrapper<>();
                    cmstQueryWrapper.eq("experiment_id",0);
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(cmstQueryWrapper);
                    if(waybillDTOList.size() == baShippingOrderCmsts.size()){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(waybillDTOList.get(0).getSupplierNo());
                        transportCmst.setSampleType("0");
                        baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                }
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baSampleAssay, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baSampleAssay, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baSampleAssay.setState(status);
        baSampleAssayMapper.updateById(baSampleAssay);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSampleAssay baSampleAssay, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSampleAssay.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"取样审批提醒",msgContent,baMessage.getType(),baSampleAssay.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaSampleAssay checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSampleAssay baSampleAssay = baSampleAssayMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baSampleAssay == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SAMPLE_001);
        }
        if (!AdminCodeEnum.SAMPLE_STATUS_VERIFYING.getCode().equals(baSampleAssay.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SAMPLE_002);
        }
        return baSampleAssay;
    }
}
