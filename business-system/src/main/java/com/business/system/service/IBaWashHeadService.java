package com.business.system.service;

import java.util.List;

import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.BaWashInventory;

/**
 * 洗煤配煤出入库记录Service接口
 *
 * @author single
 * @date 2024-01-05
 */
public interface IBaWashHeadService
{
    /**
     * 查询洗煤配煤出入库记录
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 洗煤配煤出入库记录
     */
    public BaWashHead selectBaWashHeadById(String id);

    /**
     * 查询洗煤配煤出入库记录列表
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 洗煤配煤出入库记录集合
     */
    public List<BaWashHead> selectBaWashHeadList(BaWashHead baWashHead);

    /**
     * 新增洗煤配煤出入库记录
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 结果
     */
    public int insertBaWashHead(BaWashHead baWashHead);

    /**
     * 修改洗煤配煤出入库记录
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 结果
     */
    public int updateBaWashHead(BaWashHead baWashHead);

    /**
     * 批量删除洗煤配煤出入库记录
     *
     * @param ids 需要删除的洗煤配煤出入库记录ID
     * @return 结果
     */
    public int deleteBaWashHeadByIds(String[] ids);

    /**
     * 删除洗煤配煤出入库记录信息
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 结果
     */
    public int deleteBaWashHeadById(String id);

    /**
     * 新增洗煤出库记录
     */
    public int insertWashOutputdepot(BaWashHead baWashHead);

    /**
     * 修改洗煤入库记录
     */
    public int updateWashOutputdepot(BaWashHead baWashHead);

    /** 入库撤销 */
    public int intoRevoke(String id);

    /**
     * 洗煤出库撤销
     */
    public int outputRevoke(String id);

    /**
     * 查询库存判断是否允许审批通过
     */
    public boolean getMaterialStockResult(String id);

    /**
     * 洗配煤出入库统计
     */
    public List<BaWashInventory> washHeadDetail(BaWashInventory baWashInventory);
}
