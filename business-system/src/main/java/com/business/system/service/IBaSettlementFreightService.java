package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaSettlementFreight;

/**
 * 汽运结算Service接口
 *
 * @author ljb
 * @date 2023-02-24
 */
public interface IBaSettlementFreightService
{
    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    public BaSettlementFreight selectBaSettlementFreightById(String id);

    /**
     * 查询汽运结算列表
     *
     * @param baSettlementFreight 汽运结算
     * @return 汽运结算集合
     */
    public List<BaSettlementFreight> selectBaSettlementFreightList(BaSettlementFreight baSettlementFreight);

    /**
     * 新增汽运结算
     *
     * @param baSettlementFreight 汽运结算
     * @return 结果
     */
    public int insertBaSettlementFreight(BaSettlementFreight baSettlementFreight);

    /**
     * 修改汽运结算
     *
     * @param baSettlementFreight 汽运结算
     * @return 结果
     */
    public int updateBaSettlementFreight(BaSettlementFreight baSettlementFreight);

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    public int deleteBaSettlementFreightByIds(String[] ids);

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    public int deleteBaSettlementFreightById(String id);


}
