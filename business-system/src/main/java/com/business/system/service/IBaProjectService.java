package com.business.system.service;

import java.util.List;
import java.util.Map;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaProject;
import com.business.system.domain.BaProjectLink;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.BaProjectVO;

/**
 * 立项管理Service接口
 *
 * @author ljb
 * @date 2022-12-02
 */
public interface IBaProjectService
{
    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    public BaProject selectBaProjectById(String id);

    /**
     * 查询立项管理列表
     *
     * @param baProject 立项管理
     * @return 立项管理集合
     */
    public List<BaProject> selectBaProjectList(BaProject baProject);

    public List<BaProject> baProjectList(BaProject baProject);
    /**
     * 查询立项通过的列表
     *
     * @param baProject 立项管理
     * @return 立项管理集合
     */
    public List<BaProject> selectBaProjectList1(BaProject baProject);

    /**
     * 查询立项管理草稿列表
     */
    public List<BaProject> selectDraft(BaProject baProject);

    /**
     * 新增立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    public int insertBaProject(BaProject baProject);

    /**
     * 提交方法
     * @return
     */
    public int submit(BaProject baProject);
    /**
     * 修改立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    public int updateBaProject(BaProject baProject);

    /**
     * 批量删除立项管理
     *
     * @param ids 需要删除的立项管理ID
     * @return 结果
     */
    public int deleteBaProjectByIds(String[] ids);

    /**
     * 删除立项管理信息
     *
     * @param id 立项管理ID
     * @return 结果
     */
    public int deleteBaProjectById(String id);

    /**
     * 撤销
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 项目完成按钮
     */
    public int complete(String id);

    /**
     * 查询立项管理列表
     *
     * @param baProject 立项管理
     * @return 立项管理集合
     */
    public List<BaProject> listProgress(BaProject baProject);

    /**
     * 查询是否有权限编辑草稿箱
     */
    String getUpdateAuth(BaProjectDTO baProjectDTO);

    /**
     * 运营首页数据查询
     */
    List<BaProjectDTO> baProjectDTOList(String type);
    /**
     * 智慧驾驶舱财务统计项目收益查询
     */
    List<BaProjectDTO> baProjectDTOList1();

    /**
     * 业务首页数据查询
     */
    List<BaProjectDTO> baProjectDTOS(String type);


    /**
     * 业务首页合同启动信息
     */
    List<ContractStartDTO> contractStart(BaContractStartDTO startDTO);

    /**
     * 业务总监本月数据查询
     */
    UR dataStatistics(String type);

    /**
     * 业务部排名
     */
     List<StatisticsDTO> statistics();

    /**
     * 审批办理保存编辑业务数据
     * @param baProjectInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO);
    /**
     * 移动端业务信息查询
     * @param
     * @return
     */
   Map selectNumsAndName(List<BaContractStart> list);

    /**
     * 立项序号
     * @return
     */
   String projectSerial();



     /**
     * 新增立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    public int insertBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 查询立项授权列表
     *
     * @param baProjectLink 立项授权
     * @return 立项授权集合
     */
    public BaProjectLink selectBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 运输信息(业务经理首页)
     * @return 立项集合
     */
    public List<BaProjectVO> transportInformation(BaProject baProject);

    /**
     * APP业务首页项目立项
     */
    public UR appHomeProjectStat(BaProject baProject);

    /**
     * PC端首页立项统计
     */
    public int countBaProjectList(CountBaProjectDTO countBaProjectDTO);

    /**
     * 查询立项管理
     *
     * @param businessUserDTO 立项
     * @return 立项管理
     */
    public List<String> selectBaProjectByBusinessId(BusinessUserDTO businessUserDTO);

    /**
     * 变更立项
     * @param baProject
     * @return
     */
    public int changeBaProject(BaProject baProject);


    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> selectChangeDataList(String id);

    public List<BaProject> selectBaProjectAuthorizedList(BaProject baProject);

    /**
     * 授权立项
     * @param baProject
     * @return
     */
    public int authorizedBaProject(BaProject baProject);
}
