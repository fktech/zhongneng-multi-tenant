package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.BaBank;
import com.business.system.domain.BaCompany;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 公司管理审批接口 服务类
 * @date: 2023/3/1 10:32
 */
@Service
public class ICompanyApprovalService {

    @Autowired
    protected BaCompanyMapper baCompanyMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaCompany baCompany = new BaCompany();
        baCompany.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baCompany = baCompanyMapper.selectBaCompanyById(baCompany.getId());
        baCompany.setState(status);
        String userId = String.valueOf(baCompany.getUserId());
        if(AdminCodeEnum.COMPANY_STATUS_PASS.getCode().equals(status)){
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            BaCompany baCompany1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaCompany.class);
            if(!ObjectUtils.isEmpty(baCompany1)){
                baCompany1.setState(status);
                //修改开票信息
                if(StringUtils.isNotNull(baCompany1.getBillingInformation())){
                    baBillingInformationMapper.updateBaBillingInformation(baCompany1.getBillingInformation());
                }
                //基本账户
                if(StringUtils.isNotNull(baCompany1.getBaBank())){
                    BaBank baBank = baCompany1.getBaBank();
                    baBank.setUpdateTime(DateUtils.getNowDate());
                    baBank.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(baBank);
                }
                //收款账户信息
                List<BaBank> baBankList = baCompany1.getBaBankList();
                if(!CollectionUtils.isEmpty(baBankList)){
                    baBankList.stream().forEach(item -> {
                        if(StringUtils.isNotEmpty(item.getId())){
                            item.setUpdateTime(DateUtils.getNowDate());
                            item.setUpdateBy(SecurityUtils.getUsername());
                            baBankMapper.updateBaBank(item);
                        }else {
                            item.setId(getRedisIncreID.getId());
                            item.setCreateTime(DateUtils.getNowDate());
                            item.setCreateBy(SecurityUtils.getUsername());
                            baBankMapper.insertBaBank(item);
                        }
                    });
                }

                baCompanyMapper.updateBaCompany(baCompany1);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //插入消息通知信息
            this.insertMessage(baCompany1, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.COMPANY_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baCompany, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baCompany, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baCompanyMapper.updateById(baCompany);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCompany baCompany, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baCompany.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"公司审批提醒",msgContent,baMessage.getType(),baCompany.getId(),baCompany.getCompanyType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaCompany checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaCompany baCompany = baCompanyMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baCompany == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_COMPANY_001);
        }
//        if (!AdminCodeEnum.SUPPLIER_STATUS_VERIFYING.getCode().equals(baSupplier.getState())) {
//            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SUPPLIER_002);
//        }
        return baCompany;
    }
}
