package com.business.system.service;

import java.text.MessageFormat;
import java.util.List;

import com.business.common.constant.MessageConstant;
import com.business.common.utils.DateUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProject;

/**
 * 系统消息记录Service接口
 *
 * @author single
 * @date 2023-01-10
 */
public interface IBaMessageService
{
    /**
     * 查询系统消息记录
     *
     * @param msId 系统消息记录ID
     * @return 系统消息记录
     */
    public BaMessage selectBaMessageById(String msId);

    /**
     * 查询系统消息记录列表
     *
     * @param baMessage 系统消息记录
     * @return 系统消息记录集合
     */
    public List<BaMessage> selectBaMessageList(BaMessage baMessage);

    /**
     * 新增系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int insertBaMessage(BaMessage baMessage);

    /**
     * 修改系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int updateBaMessage(BaMessage baMessage);

    /**
     * 批量删除系统消息记录
     *
     * @param msIds 需要删除的系统消息记录ID
     * @return 结果
     */
    public int deleteBaMessageByIds(String[] msIds);

    /**
     * 删除系统消息记录信息
     *
     * @param msId 系统消息记录ID
     * @return 结果
     */
    public int deleteBaMessageById(String msId);

    /**
     * 清空
     * @param
     * @return
     */
    public int empty();

    /**
     * 已读
     * @param
     * @return
     */
    public int read();

    /**
     * 批量已读
     * @param
     * @return
     */
    public int batchRead(String[] msIds);


    public BaMessage selectBaMessage();

    /**
     * 登录成功后更新手机标识
     */
    public int phoneCode(BaMessage baMessage);

    /**
     * 登录成功后更新手机标识
     */
    public List<BaMessage> selectPhoneCode(String phoneCode);

    /**
     * 手机消息推送
     */
    public String messagePush(String cid,String title,String content,String type, String id, String businessType);
}
