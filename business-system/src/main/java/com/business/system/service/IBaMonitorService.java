package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaMonitor;

/**
 * 监控Service接口
 *
 * @author single
 * @date 2024-06-04
 */
public interface IBaMonitorService
{
    /**
     * 查询监控
     *
     * @param id 监控ID
     * @return 监控
     */
    public BaMonitor selectBaMonitorById(String id);

    /**
     * 查询监控列表
     *
     * @param baMonitor 监控
     * @return 监控集合
     */
    public List<BaMonitor> selectBaMonitorList(BaMonitor baMonitor);

    /**
     * 新增监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    public int insertBaMonitor(BaMonitor baMonitor);

    /**
     * 修改监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    public int updateBaMonitor(BaMonitor baMonitor);

    /**
     * 批量删除监控
     *
     * @param ids 需要删除的监控ID
     * @return 结果
     */
    public int deleteBaMonitorByIds(String[] ids);

    /**
     * 删除监控信息
     *
     * @param id 监控ID
     * @return 结果
     */
    public int deleteBaMonitorById(String id);


}
