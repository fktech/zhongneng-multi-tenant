package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaCompany;
import com.business.system.domain.dto.BaCompanyInstanceRelatedEditDTO;
import com.business.system.domain.vo.BaCompanyVO;
import com.business.system.domain.vo.BaEnterpriseVO;

/**
 * 公司Service接口
 *
 * @author ljb
 * @date 2022-11-30
 */
public interface IBaCompanyService
{
    /**
     * 查询公司
     *
     * @param id 公司ID
     * @return 公司
     */
    public BaCompany selectBaCompanyById(String id);

    /**
     * 查询公司列表
     *
     * @param baCompany 公司
     * @return 公司集合
     */
    public List<BaCompany> selectBaCompanyList(BaCompany baCompany);

    /**
     * 新增公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    public int insertBaCompany(BaCompany baCompany);

    /**
     * 修改公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    public int updateBaCompany(BaCompany baCompany);

    /**
     * 修改银行账户信息
     */
    public int updateCompany(BaCompany baCompany);

    /**
     * 批量删除公司
     *
     * @param ids 需要删除的公司ID
     * @return 结果
     */
    public int deleteBaCompanyByIds(String[] ids);

    /**
     * 删除公司信息
     *
     * @param id 公司ID
     * @return 结果
     */
    public int deleteBaCompanyById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 审批办理保存编辑业务数据
     * @param baCompanyInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(BaCompanyInstanceRelatedEditDTO baCompanyInstanceRelatedEditDTO);

    /**
     * 公司下拉
     * @param baCompany
     * @return
     */
    public List<BaCompany> companyList(BaCompany baCompany);

    /**
     * 客商数据统计
     */
    public UR counts();

    /**
     * 承运单位发运量
     */
    public List<BaCompanyVO> countRanking();

}
