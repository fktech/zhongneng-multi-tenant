package com.business.system.service;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaBillingInformation;
import com.business.system.domain.BaCollection;
import com.business.system.domain.BaPayment;
import com.business.system.domain.dto.BaCollectionVoucherDTO;
import com.business.system.domain.dto.BaPaymentVoucherDTO;
import com.business.system.domain.vo.BaCollectionStatVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * 收款信息Service接口
 *
 * @author single
 * @date 2022-12-26
 */
public interface IBaCollectionService
{
    /**
     * 查询收款信息
     *
     * @param id 收款信息ID
     * @return 收款信息
     */
    public BaCollection selectBaCollectionById(String id);
    /**
     * 查询本年度收款金额
     *
     * @param
     * @return 收款信息
     */
    public BigDecimal selectBaCollectionByyear();
    /**
     * 查询本年度收款金额
     *
     * @param
     * @return 收款信息
     */
    public List<BaCollection> selectByyearandbiusstype(BaCollection baCollection);

    /**
     * 查询收款信息列表
     *
     * @param baCollection 收款信息
     * @return 收款信息集合
     */
    public List<BaCollection> selectBaCollectionList(BaCollection baCollection);

    /**
     * 认领收款查询
     * @param baCollection
     * @return
     */
    public List<BaCollectionVoucherDTO> claimCollection(BaCollection baCollection);

    /**
     * 统计
     */
    public UR statistics(BaCollection baCollection);

    /*  *//**
     * 微信认领收款查询
     * @param baCollection
     * @return
     *//*
    public List<BaCollectionVoucherDTO> claimCollectionInWx(BaCollection baCollection);*/
    /**
     * 新增收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    public int insertBaCollection(BaCollection baCollection);

    /**
     * 新增认领收款
     * @param baCollection
     * @return
     */
    public int insertCollection(BaCollection baCollection);

    /**
     * 修改收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    public int updateBaCollection(BaCollection baCollection);

    /**
     * 批量删除收款信息
     *
     * @param ids 需要删除的收款信息ID
     * @return 结果
     */
    public int deleteBaCollectionByIds(String[] ids);

    /**
     * 删除收款信息信息
     *
     * @param id 收款信息ID
     * @return 结果
     */
    public int deleteBaCollectionById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 新增收款凭证
     */
    int insertBaCollectionVoucher(BaCollectionVoucherDTO baCollectionVoucherDTO);

    /**
     * 认领收款发起流程
     * @param id
     * @return
     */
    int approvalButton(String id,String workState);

    /**
     * 认领收款撤销
     * @param id
     * @return
     */
    int approvalRevoke(String id);

    /**
     * 模糊查询公司名称
     * @param name
     * @return
     */
    public UR searchName(String name);

    /**
     * 开票信息
     */
    public List<BaBillingInformation> invoicingInformation(BaBillingInformation baBillingInformation);

    /**
     * 退回
     */
    public int goBack(String id);

    /**
     * 关联
     * @param baCollection
     * @return
     */
    public int association(BaCollection baCollection);

    /**
     * 可视化大屏统计应收账款
     * @return
     */
    public BaCollectionStatVO sumBaCollection();

    /**
     *
     * @param baCollection
     * @return
     */
    public List<BaCollectionVoucherDTO> claimCollectionAuthorized(BaCollection baCollection);

    /**
     * 授权收款管理
     */
    public int authorizedBaCollection(BaCollection baCollection);
}
