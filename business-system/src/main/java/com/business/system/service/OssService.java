package com.business.system.service;

import com.aliyun.oss.model.OSSObjectSummary;
import com.business.system.domain.FileUploadResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Service文件上传
 *
 * @author jiahe
 * @date 2020.7
 */
public interface OssService {
    //上传文件到oss
    FileUploadResult uploadFile(MultipartFile file, HttpServletRequest request);

    //查看所有文件
    List<OSSObjectSummary> list();

    //下载文件
    void downloadOssFile(OutputStream os, String objectName) throws IOException;

    //删除文件
    FileUploadResult delete(String objectName);

    //上传app
    FileUploadResult uploadApp(MultipartFile file, HttpServletRequest request);

    //上传图片
    FileUploadResult Certification(MultipartFile file, String type, HttpServletRequest request);

    //文件打包
    void getZipFromOSSByPaths(String zipName, File zipFile, HttpServletRequest request,
                              HttpServletResponse response, List<String> paths) throws Exception;
}
