package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaContractStartTonVO;
import com.business.system.domain.vo.BaContractStartVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 合同启动历史Service业务层处理
 *
 * @author ljb
 * @date 2023-03-06
 */
@Service
public class BaHiContractStartServiceImpl extends ServiceImpl<BaHiContractStartMapper, BaHiContractStart> implements IBaHiContractStartService {

    private static final Logger log = LoggerFactory.getLogger(BaHiContractStartServiceImpl.class);
    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaShippingPlanServiceImpl baShippingPlanService;

    @Autowired
    private BaShippingPlanMapper baShippingPlanMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaDepotMapper depotMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private IBaChargingStandardsService iBaChargingStandardsService;

    @Autowired
    private IBaBidTransportService iBaBidTransportService;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private BaUserMailMapper baUserMailMapper;

    @Autowired
    private IBaDeclareService baDeclareService;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private IBaEnterpriseRelevanceService baEnterpriseRelevanceService;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private IBaTransportService baTransportService;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private IBaContractService iBaContractService;

    @Autowired
    private IBaDepotHeadService iBaDepotHeadService;

    @Autowired
    private ISysDictDataService sysDictDataService;

    @Autowired
    private BaHiContractStartMapper baHiContractStartMapper;

    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    @Override
    public BaHiContractStart selectBaHiContractStartById(String id) {
        BaHiContractStart baHiContractStart = baHiContractStartMapper.selectBaHiContractStartById(id);
        if (!ObjectUtils.isEmpty(baHiContractStart) && StringUtils.isNotEmpty(baHiContractStart.getSupplierId())) {
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baHiContractStart.getSupplierId());
            baHiContractStart.setSupplierName(baSupplier.getName());
        }
        //立项信息
        if (!ObjectUtils.isEmpty(baHiContractStart)) {
            BaProject baProject = baProjectMapper.selectBaProjectById(baHiContractStart.getProjectId());
            if (StringUtils.isNotNull(baProject)) {
                if (!ObjectUtils.isEmpty(baProject)) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsId());
                    if (!ObjectUtils.isEmpty(baGoods)) {
                        baProject.setGoodsName(baGoods.getName());
                    }
                    //货物种类
                    if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                        BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                        if(StringUtils.isNotNull(baGoodsType)){
                            baHiContractStart.setGoodsType(baGoodsType.getName());
                        }else {
                            BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                            baHiContractStart.setGoodsType(baGoods1.getName());
                        }

                    }
                    //下游贸易商
                    if(StringUtils.isNotEmpty(baProject.getDownstreamTraders())){
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baProject.getDownstreamTraders());
                        baProject.setDownstreamTradersName(baCompany.getName());
                    }
                }
                baHiContractStart.setBaProject(baProject);

                //终端企业
                //判断是否为采购订单或销售订单
                if (baHiContractStart.getType().equals("2") || baHiContractStart.getType().equals("3")) {
                    if(StringUtils.isNotEmpty(baHiContractStart.getEnterprise())){
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baHiContractStart.getEnterprise());
                        if (StringUtils.isNotNull(enterprise)) {
                            baHiContractStart.setEnterpriseName(enterprise.getName());
                            baHiContractStart.setEnterpriseId(enterprise.getId());
                        }
                    }
                } else {
                    if (StringUtils.isNotEmpty(baProject.getEnterpriseId())) {
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject.getEnterpriseId());
                        baHiContractStart.setEnterpriseName(enterprise.getName());
                        baHiContractStart.setEnterpriseId(enterprise.getId());
                    }
                }
                //项目编号
                if (StringUtils.isNotEmpty(baProject.getProjectNum())) {
                    baHiContractStart.setProjectNum(baProject.getProjectNum());
                    baHiContractStart.setProjectName(baProject.getName());
                }
                baHiContractStart.setProjectName(baProject.getName());
                //立项吨费
                if (baProject.getTonnageCharge() != null) {
                    baHiContractStart.setTonnageCharge(baProject.getTonnageCharge());
                }
                //立项年化
                if (baProject.getAnnualization() != null) {
                    baHiContractStart.setAnnualization(baProject.getAnnualization());
                }
                //货物种类
                if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                    BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                    if(StringUtils.isNotNull(baGoodsType)){
                        baHiContractStart.setGoodsType(baGoodsType.getName());
                    }else {
                        BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                        baHiContractStart.setGoodsType(baGoods1.getName());
                    }
                }
                //商品名称
                if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsId());
                    if (StringUtils.isNotNull(baGoods)) {
                        baHiContractStart.setGoodsName(baGoods.getName());
                        baHiContractStart.setGoodsId(baGoods.getId());
                    }
                }
            }
        }
        //托盘公司
        if (StringUtils.isNotEmpty(baHiContractStart.getCompanyId())) {
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baHiContractStart.getCompanyId());
            baHiContractStart.setCompanyName(baCompany.getName());
            //baContractStart.setCompanyAnnualizedRate(baCompany.getAnnualizedRate());
        }
        //放标公司
        if (StringUtils.isNotEmpty(baHiContractStart.getLabelingCompanyId())) {
            BaCompany labelingCompany = baCompanyMapper.selectBaCompanyById(baHiContractStart.getLabelingCompanyId());
            if (!ObjectUtils.isEmpty(labelingCompany)) {
                baHiContractStart.setLabelingCompanyName(labelingCompany.getName());
                baHiContractStart.setLabelingType("1");
            } else {
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baHiContractStart.getLabelingCompanyId());
                if (!ObjectUtils.isEmpty(enterprise)) {
                    baHiContractStart.setLabelingCompanyName(enterprise.getName());
                    baHiContractStart.setLabelingType("2");
                }
            }
        }
        //仓库名称
       /* if (StringUtils.isNotEmpty(baContractStart.getProjectId())) {
            //立项信息
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            //仓库信息
            if (StringUtils.isNotNull(baProject)) {
                if (StringUtils.isNotEmpty(baProject.getDepotId())) {
                    BaDepot baDepot = depotMapper.selectBaDepotById(baProject.getDepotId());
                    baContractStart.setDepotName(baDepot.getName());
                }
            }
        }*/
        if(StringUtils.isNotEmpty(baHiContractStart.getDepotId())){
            BaDepot baDepot = depotMapper.selectBaDepotById(baHiContractStart.getDepotId());
            baHiContractStart.setDepotName(baDepot.getName());
        }
        //商品
        if (StringUtils.isNotEmpty(baHiContractStart.getGoodsId())) {
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baHiContractStart.getGoodsId());
            baHiContractStart.setGoodsName(baGoods.getName());
            baHiContractStart.setBaGoods(baGoods);
        }
        //仓储立项
        /*if(StringUtils.isNotEmpty(baContractStart.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
            if(StringUtils.isNotNull(baProject)){
                if(StringUtils.isNotEmpty(baProject.getDepotId())){
                    //当前库存
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id",baProject.getDepotId());
                    BaMaterialStock materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    if(StringUtils.isNotNull(materialStock)){
                        baContractStart.setInventoryTotal(materialStock.getInventoryTotal());
                    }
                }
            }
        }*/
        //发运计划
        QueryWrapper<BaShippingPlan> shippingPlanQueryWrapper = new QueryWrapper<>();
        shippingPlanQueryWrapper.eq("relevance_id", id);
        List<BaShippingPlan> baShippingPlans = baShippingPlanMapper.selectList(shippingPlanQueryWrapper);
        baHiContractStart.setBaShippingPlans(baShippingPlans);
        //合同启动团队
        if (StringUtils.isNotEmpty(baHiContractStart.getStartTeam())) {
            baHiContractStart.setStartTeamName(baUserMailMapper.selectBaUserMailById(baHiContractStart.getStartTeam()).getName());
        }
        //上游内部对接人
        if (StringUtils.isNotEmpty(baHiContractStart.getInternalContactPerson())) {
            baHiContractStart.setInternalContactPersonName(baUserMailMapper.selectBaUserMailById(baHiContractStart.getInternalContactPerson()).getName());
        }
        //下游内部对接人
        if (StringUtils.isNotEmpty(baHiContractStart.getLowInternalContactPerson())) {
            baHiContractStart.setLowInternalContactPersonName(baUserMailMapper.selectBaUserMailById(baHiContractStart.getLowInternalContactPerson()).getName());
        }
        //业务经理
        /*if(StringUtils.isNotEmpty(baContractStart.getServiceManager())){
            String[] split = baContractStart.getServiceManager().split(",");
            SysUser sysUser = sysUserMapper.selectUserById(Long.parseLong(split[split.length - 1]));
            //岗位信息
            String name = getName.getName(sysUser.getUserId());
            if(name.equals("") == false){
                baContractStart.setServiceManagerName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baContractStart.setServiceManagerName(sysUser.getNickName());
            }
        }*/
        if (StringUtils.isNotEmpty(baHiContractStart.getServiceManager())) {
            if (baHiContractStart.getServiceManager().equals("0") == false) {
                SysUser sysUser = sysUserMapper.selectUserById(Long.parseLong(baHiContractStart.getServiceManager()));
                baHiContractStart.setServiceManagerName(sysUser.getNickName());
            }
        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id", baHiContractStart.getId());
        relatedQueryWrapper.eq("flag", 0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baHiContractStart.setProcessInstanceId(processInstanceIds);
        //发起人
        SysUser sysUser = iSysUserService.selectUserById(baHiContractStart.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if (name.equals("") == false) {
            sysUser.setPostName(name.substring(0, name.length() - 1));
        }
        baHiContractStart.setUser(sysUser);
        //收费标准
        QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
        baChargingStandardsQueryWrapper.eq("relation_id", baHiContractStart.getId());
        BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectOne(baChargingStandardsQueryWrapper);
        baHiContractStart.setBaChargingStandards(baChargingStandards);

        //合同启动投标信息运输信息
        QueryWrapper<BaBidTransport> baBidTransportQueryWrapper = new QueryWrapper<>();
        baBidTransportQueryWrapper.eq("start_id", baHiContractStart.getId());
        baBidTransportQueryWrapper.eq("flag", 0);
        List<BaBidTransport> baBidTransports = baBidTransportMapper.selectList(baBidTransportQueryWrapper);
        if (!CollectionUtils.isEmpty(baBidTransports)) {
            for (BaBidTransport bidTransport : baBidTransports) {
                if (!ObjectUtils.isEmpty(bidTransport)) {
                    BaCompany transportCompany = baCompanyMapper.selectBaCompanyById(bidTransport.getTransportCompany());
                    if (!ObjectUtils.isEmpty(transportCompany)) {
                        bidTransport.setTransportCompanyName(transportCompany.getName());
                        bidTransport.setCompanyAbbreviation(transportCompany.getCompanyAbbreviation());
                    } else {
                        bidTransport.setTransportCompanyName(bidTransport.getTransportCompany());
                    }
                }
            }
        }

        baHiContractStart.setBaBidTransports(baBidTransports);
        //资金计划
        QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
        declareQueryWrapper.eq("start_id", baHiContractStart.getId()).and(itm -> itm.eq("source", 1).or().eq("state", "declare:pass"));
        List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);
        //计划申报去重
        List<BaDeclare> collect = baDeclares.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDeclare::getId))), ArrayList::new));
        baHiContractStart.setBaDeclares(collect);
        //投标委托人
        BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baHiContractStart.getBidPerson());
        if (!ObjectUtils.isEmpty(baUserMail)) {
            baHiContractStart.setBidPersonName(baUserMail.getName());
        }
        //指标信息
        if (StringUtils.isNotEmpty(baHiContractStart.getIndicatorInformation())) {
            BaEnterpriseRelevance baEnterpriseRelevance = baEnterpriseRelevanceService.selectBaEnterpriseRelevanceById(baHiContractStart.getIndicatorInformation());
            baHiContractStart.setBaEnterpriseRelevance(baEnterpriseRelevance);
        }
        //合同管理名称
        if (StringUtils.isNotEmpty(baHiContractStart.getContractId())) {
            BaContract baContract = baContractMapper.selectBaContractById(baHiContractStart.getContractId());
            baHiContractStart.setContractName(baContract.getName());
        }
        if(StringUtils.isNotEmpty(baHiContractStart.getBelongCompanyId())){
            SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baHiContractStart.getBelongCompanyId()));
            if(!ObjectUtils.isEmpty(sysDept)){
                baHiContractStart.setBelongCompanyName(sysDept.getDeptName());
            }
        }
        //子公司
        /*if(StringUtils.isNotEmpty(baContractStart.getSubsidiary())){
            BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baContractStart.getSubsidiary());
            baContractStart.setSubsidiaryName(baCompanyGroup.getCompanyName());
        }*/
        //事业部名称
        /*if(StringUtils.isNotEmpty(baContractStart.getDivision())){
            SysUser user = iSysUserService.selectUserById(baContractStart.getUserId());
            baContractStart.setUser(user);
        }*/
        //运营专员
        if(StringUtils.isNotEmpty(baHiContractStart.getPersonCharge())){
            String[] split = baHiContractStart.getPersonCharge().split(",");
            String userName = "";
            for (String userId:split) {
                if(this.isNumeric(userId) == true){
                    SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                    if(StringUtils.isNotNull(user)){
                        userName = user.getNickName() + "," + userName;
                    }
                }
            }
            if(StringUtils.isNotEmpty(userName) && !"".equals(userName)){
                baHiContractStart.setPersonChargeName(userName.substring(0,userName.length()-1));
            }else {
                baHiContractStart.setPersonChargeName(baHiContractStart.getPersonCharge());
            }
        }
        //上游公司
        if(StringUtils.isNotEmpty(baHiContractStart.getUpstreamCompany())){
            //查询供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baHiContractStart.getUpstreamCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baHiContractStart.setUpstreamCompanyName(baSupplier.getName());
            }
        }
        //主体公司
        if(StringUtils.isNotEmpty(baHiContractStart.getSubjectCompany())){
            if(this.isNumeric(baHiContractStart.getSubjectCompany()) == true){
                //子公司
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baHiContractStart.getSubjectCompany()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    baHiContractStart.setSubjectCompanyName(sysDept.getDeptName());
                }
            }else {
                baHiContractStart.setSubjectCompanyName(baHiContractStart.getSubjectCompany());
            }
        }
        //终端公司
        if(StringUtils.isNotEmpty(baHiContractStart.getTerminalCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baHiContractStart.getTerminalCompany());
            if(StringUtils.isNotNull(enterprise)){
                baHiContractStart.setTerminalCompanyName(enterprise.getName());
            }
        }
        //下游公司
        if(StringUtils.isNotEmpty(baHiContractStart.getDownstreamCompany())){
            //下游公司名称
            String downstreamCompanyName = "";
            String[] split = baHiContractStart.getDownstreamCompany().split(",");
            for (String downstream:split) {
                String[] split1 = downstream.split(";");
                //供应商
                if("1".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(key);
                    downstreamCompanyName = split1[0]+";"+baSupplier.getName()+","+downstreamCompanyName;
                }
                //终端企业
                if("2".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(key);
                    downstreamCompanyName = split1[0]+";"+enterprise.getName()+","+downstreamCompanyName;
                }
                //公司
                if(!"1".equals(split1[0]) && !"2".equals(split1[0]) && !"6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                    downstreamCompanyName = split1[0]+";"+baCompany.getName()+","+downstreamCompanyName;
                }
                //集团公司
                if("6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(key);
                    downstreamCompanyName = split1[0]+";"+companyGroup.getCompanyName()+","+downstreamCompanyName;
                }
            }
            if(",".equals(downstreamCompanyName.substring(downstreamCompanyName.length()-1))){
                baHiContractStart.setDownstreamCompanyName(downstreamCompanyName.substring(0,downstreamCompanyName.length()-1));
            }else {
                baHiContractStart.setDownstreamCompanyName(downstreamCompanyName);
            }

        }
        //合同信息
        BaContract baContract = new BaContract();
        baContract.setSaleOrder(id);
        baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
        baHiContractStart.setBaContractList(baContracts);
        //出库信息
        BaDepotHead baDepotHead = new BaDepotHead();
        baDepotHead.setOrderId(id);
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        List<BaDepotHead> baDepotHeads = iBaDepotHeadService.selectBaDepotHead(baDepotHead);
        baHiContractStart.setBaDepotHeadList(baDepotHeads);
        return baHiContractStart;
    }

    //判断String是否能转数字
    public static boolean isNumeric(String str) {
        try {
            Long.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 查询合同启动列表
     *
     * @param baHiContractStart 合同启动
     * @return 合同启动
     */
    @Override
    @DataScope(deptAlias = "s", userAlias = "s")
    public List<BaHiContractStart> selectBaHiContractStartList(BaHiContractStart baHiContractStart) {
        List<BaHiContractStart> baHiContractStarts = baHiContractStartMapper.selectBaHiContractStartList(baHiContractStart);

        for (BaHiContractStart contractStart : baHiContractStarts) {
            //岗位信息
            String name = getName.getName(contractStart.getUserId());
            //发起人
            if (name.equals("") == false) {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                contractStart.setUserName(sysUserMapper.selectUserById(contractStart.getUserId()).getNickName());
            }

            //查找运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", contractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
            BigDecimal transit = new BigDecimal(0);
            for (BaTransport baTransport : baTransports) {
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                if (StringUtils.isNotNull(baCheck)) {
                    //运输中
                    transit = transit.add(baCheck.getCheckCoalNum());
                }
            }
            contractStart.setCheckCoalNum(transit);
            if(StringUtils.isNotEmpty(contractStart.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(contractStart.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    contractStart.setBelongCompanyName(sysDept.getDeptName());
                }
            }
        }
        return baHiContractStarts;
    }

    @Override
    public List<BaHiContractStart> selectChangeDataBaHiContractStartList(BaHiContractStart baHiContractStart) {
        //查询变更记录
        baHiContractStart.setTenantId(SecurityUtils.getCurrComId());
        List<BaHiContractStart> baHiContractStarts = baHiContractStartMapper.selectChangeBaHiContractStartList(baHiContractStart);
        if(!CollectionUtils.isEmpty(baHiContractStarts)){
            baHiContractStarts.stream().forEach(item -> {
                SysUser sysUser = sysUserMapper.selectUserByUserName(item.getChangeBy());
                if(!ObjectUtils.isEmpty(sysUser)){
                    //岗位名称
                    String name = getName.getName(sysUser.getUserId());
                    if(StringUtils.isNotEmpty(name)){
                        sysUser.setPostName(name.substring(0,name.length()-1));
                    }
                    item.setChangeUser(sysUser);
                }
            });
        }
        return baHiContractStarts;
    }
}
