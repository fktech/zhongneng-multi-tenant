package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.BaOrderDTO;
import com.business.system.domain.dto.MonthSumDTO;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import com.business.system.mapper.*;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaClaimService;
import org.springframework.util.ObjectUtils;

/**
 * 认领Service业务层处理
 *
 * @author ljb
 * @date 2023-01-31
 */
@Service
public class BaClaimServiceImpl extends ServiceImpl<BaClaimMapper, BaClaim> implements IBaClaimService
{
    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaGoodsMapper goodsMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    /**
     * 查询认领
     *
     * @param id 认领ID
     * @return 认领
     */
    @Override
    public BaClaim selectBaClaimById(String id)
    {
        BaClaim baClaim = baClaimMapper.selectBaClaimById(id);
        //岗位信息
        String name = getName.getName(baClaim.getUserId());
        //认领人
        if(name.equals("") == false){
            baClaim.setUserName(sysUserMapper.selectUserById(baClaim.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
        }else {
            baClaim.setUserName(sysUserMapper.selectUserById(baClaim.getUserId()).getNickName());
        }

       /* //查询订单
        List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
        //查询对应历史数据
        QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
        baClaimHistoryQueryWrapper.eq("claim_id",id);
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
        for (BaClaimHistory baClaimHistory:baClaimHistories) {
            //订单信息
            //BaOrder baorder = baOrderMapper.selectBaOrderById(baClaimHistory.getOrderId());
            BaOrderDTO baOrderDTO = new BaOrderDTO();
            BaOrder baOrder = baOrderMapper.selectBaOrderById(baClaimHistory.getOrderId());
            baOrderDTO.setOrderName(baOrder.getOrderName());
            baOrderDTO.setGoodsName(baOrder.getGoodsName());
            baOrderDTO.setOrderPay(baOrder.getOrderPay());
            baOrderDTO.setOrderReceive(baOrder.getOrderReceive());
            baOrderDTO.setAccumulatedAmount(baOrder.getStayCollected());
            baOrderDTO.setId(baOrder.getId());
            //本期回款
            baOrderDTO.setCurrentCollection(baClaimHistory.getCurrentCollection());
            baOrderDTO.setHistoryId(baClaimHistory.getId());
            if(StringUtils.isNotEmpty(baOrder.getGoodsId())){
                baOrderDTO.setGoodsName(goodsMapper.selectBaGoodsById(baOrder.getGoodsId()).getName());
            }
            baOrderDTO.setTransportType(baOrder.getTransportType());
            //订单对应的运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("order_id",baOrder.getId());
            transportQueryWrapper.eq("flag",0);
            BaTransport baTransport = baTransportMapper.selectOne(transportQueryWrapper);
            if(StringUtils.isNotNull(baTransport)){
                //运输编号
                baOrderDTO.setTransportId(baTransport.getId());
                //以运输信息查询发运信息
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",baTransport.getId());
                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                for (BaCheck baCheck:baChecks) {
                    if(baCheck.getType() == 1){
                        //矿发煤量
                        baOrderDTO.setCheckCoalNum(baCheck.getCheckCoalNum());
                        //发货日期
                        baOrderDTO.setCheckTime(baCheck.getCheckTime());
                        baOrderDTO.setCarsNum(baCheck.getCarsNum());
                    }else {
                        baOrderDTO.setArrivalTime(baCheck.getArrivalTime());
                        baOrderDTO.setCheckDate(baCheck.getCheckTime());
                    }
                }
            }
            //判断是否有验收信息
            if(baOrderDTO.getArrivalTime() != null){
                baOrderDTOList.add(baOrderDTO);
            }
        }
        baClaim.setBaOrderDTOs(baOrderDTOList);*/
        List<BaContractStart> baContractStartList = new ArrayList<>();
        //查询对应历史数据
        QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
        baClaimHistoryQueryWrapper.eq("claim_id",id);
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
        for (BaClaimHistory baClaimHistory:baClaimHistories) {
            //合同启动信息
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baClaimHistory.getOrderId());
            baContractStart.setCurrentCollection(baClaimHistory.getCurrentCollection());
            baContractStart.setHistoryId(baClaimHistory.getId());
            baContractStartList.add(baContractStart);
            //终端
            if(StringUtils.isNotEmpty(baContractStart.getEnterprise())){
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baContractStart.getEnterprise());
                baContractStart.setEnterpriseName(enterprise.getName());
            }
            //立项信息
            if (!ObjectUtils.isEmpty(baContractStart)) {
                BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
                //商品名称
                if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsId());
                    if (StringUtils.isNotNull(baGoods)) {
                        baContractStart.setGoodsName(baGoods.getName());
                    }
                }
            }
        }
        baClaim.setBaContractStartList(baContractStartList);
        return baClaim;
    }

    @Override
    public BaClaim selectBaClaimById1(String id) {
        return baClaimMapper.selectBaClaimById1(id) ;
    }

    @Override
    public String selectBaClaimByrelationid(String id) {
        return baClaimMapper.selectBaClaimByrelationid(id);
    }

    /**
     * 查询认领列表
     *
     * @param baClaim 认领
     * @return 认领
     */
    @Override
    public List<BaClaim> selectBaClaimList(BaClaim baClaim)
    {
        List<BaClaim> baClaims = baClaimMapper.claimList(baClaim);
        for (BaClaim claim:baClaims) {
            //岗位信息
            String name = getName.getName(claim.getUserId());
            if(name.equals("") == false){
                claim.setUserName(sysUserMapper.selectUserById(claim.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                claim.setUserName(sysUserMapper.selectUserById(claim.getUserId()).getNickName());
            }

        }
        return baClaims;
    }

    @Override
    public BigDecimal SelectSalesVolume(BaClaim baClaim) {
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        return baClaimMapper.SelectSalesVolume(baClaim) ;
    }

    @Override
    public BigDecimal SelectSalesVolumeByMonth(BaClaim baClaim) {
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        return baClaimMapper.SelectSalesVolumeByMonth(baClaim) ;
    }

    @Override
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaClaim baClaim) {
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        List<MonthSumDTO> monthSumDTOS = baClaimMapper.SelectSalesVolumeByEveryoneMonth(baClaim);
        for (MonthSumDTO monthsum:monthSumDTOS) {
            if (monthsum.getCount()==null){
                monthsum.setCount(new BigDecimal(0.00));
            }

        }
        return monthSumDTOS;
    }

    @Override
    public BigDecimal SelectSalesVolumeBybinusstype(List<BaClaimHistory> baClaimHistories) {
        //已付款金额
        BigDecimal applyCollectionAmount = new BigDecimal(0);
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy");
        for (BaClaimHistory baClaimHistory:baClaimHistories) {
            //查询认领信息
            BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
            String format = timeFormat.format(baCollection.getCreateTime());
            if(format.equals(timeFormat.format(new Date()))){
                //判断认领收款审批通过
                if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                    if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                        applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                    }
                }
            }
        }
        return applyCollectionAmount;
    }


    /**
     * 新增认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    @Override
    public int insertBaClaim(BaClaim baClaim)
    {
        baClaim.setId(getRedisIncreID.getId());
        baClaim.setCreateTime(DateUtils.getNowDate());
        baClaim.setCreateBy(SecurityUtils.getUsername());
        baClaim.setUserId(SecurityUtils.getUserId());
        baClaim.setDeptId(SecurityUtils.getDeptId());
        baClaim.setClaimState("1");
        //租户ID
        baClaim.setTenantId(SecurityUtils.getCurrComId());
        //关联订单
        /*if(StringUtils.isNotNull(baClaim.getBaOrderDTOs())){
            BaClaimHistory baClaimHistory = new BaClaimHistory();
            for (BaOrderDTO baOrder:baClaim.getBaOrderDTOs()) {
                //添加历史数据
                baClaimHistory.setId(getRedisIncreID.getId());
                baClaimHistory.setClaimId(baClaim.getId());
                baClaimHistory.setOrderId(baOrder.getId());
                baClaimHistory.setCurrentCollection(baOrder.getCurrentCollection());
                baClaimHistory.setClaimState("1");
                baClaimHistoryMapper.insertBaClaimHistory(baClaimHistory);
            }
        }*/
        //关联合同启动
        if(StringUtils.isNotNull(baClaim.getBaContractStartList())){
            BaClaimHistory baClaimHistory = new BaClaimHistory();
            for (BaContractStart baContractStart:baClaim.getBaContractStartList()) {
                //添加历史数据
                baClaimHistory.setId(getRedisIncreID.getId());
                baClaimHistory.setClaimId(baClaim.getId());
                baClaimHistory.setOrderId(baContractStart.getId());
                baClaimHistory.setCurrentCollection(baContractStart.getCurrentCollection());
                baClaimHistory.setClaimState("1");
                //全局编号
                if(StringUtils.isNotEmpty(baContractStart.getGlobalNumber())){
                   QueryWrapper<BaClaimHistory> queryWrapper = new QueryWrapper<>();
                    queryWrapper.isNotNull("serial_number");
                    queryWrapper.orderByDesc("serial_number");
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(queryWrapper);
                    if(baClaimHistories.size() > 0){
                        Integer serialNumber = baClaimHistories.get(0).getSerialNumber();
                        baClaimHistory.setSerialNumber(serialNumber+1);
                        if(serialNumber < 10){
                            baClaimHistory.setGlobalNumber(baContractStart.getGlobalNumber()+"-"+"RLSK"+"-"+"000"+baClaimHistory.getSerialNumber());
                        }else if(serialNumber >= 10){
                            baClaimHistory.setGlobalNumber(baContractStart.getGlobalNumber()+"-"+"RLSK"+"-"+"00"+baClaimHistory.getSerialNumber());
                        }else if(serialNumber >= 100){
                            baClaimHistory.setGlobalNumber(baContractStart.getGlobalNumber()+"-"+"RLSK"+"-"+"0"+baClaimHistory.getSerialNumber());
                        }else if(serialNumber >= 1000){
                            baClaimHistory.setGlobalNumber(baContractStart.getGlobalNumber()+"-"+"RLSK"+"-"+baClaimHistory.getSerialNumber());
                        }
                    }else {
                        baClaimHistory.setSerialNumber(1);
                        baClaimHistory.setGlobalNumber(baContractStart.getGlobalNumber()+"-"+"RLSK"+"-"+"000"+baClaimHistory.getSerialNumber());
                    }
                }
                baClaimHistoryMapper.insertBaClaimHistory(baClaimHistory);
            }
        }
        Integer result = baClaimMapper.insertBaClaim(baClaim);
        if(result > 0){
            BigDecimal bigDecimal = new BigDecimal(0.0000);
            //更新已回款金额
            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
            if(baCollection.getClaimed() != null){
                baCollection.setClaimed(baCollection.getClaimed().add(baClaim.getThisClaimAll()));
            }else {
                baCollection.setClaimed(bigDecimal.add(baClaim.getThisClaimAll()));
            }
            baCollectionMapper.updateBaCollection(baCollection);
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 修改认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    @Override
    public int updateBaClaim(BaClaim baClaim)
    {
        baClaim.setUpdateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(baClaim.getId())){
            BaClaim claim = baClaimMapper.selectBaClaimById(baClaim.getId());
            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(claim.getRelationId());
            //先减去原本次认领金额再加上更新后的本次认领金额
            baCollection.setClaimed(baCollection.getClaimed().subtract(claim.getThisClaim()).add(baClaim.getThisClaim()));
            baCollectionMapper.updateBaCollection(baCollection);
        }
        //更新历史本次回款
        if(StringUtils.isNotNull(baClaim.getBaContractStartList())){
            if(StringUtils.isNotEmpty(baClaim.getBaContractStartList().get(0).getHistoryId())){
                BaClaimHistory baClaimHistory1 = baClaimHistoryMapper.selectBaClaimHistoryById(baClaim.getBaContractStartList().get(0).getHistoryId());
                //查询对应历史数据
                QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                baClaimHistoryQueryWrapper.eq("claim_id",baClaimHistory1.getClaimId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                for (BaClaimHistory claimHistory:baClaimHistories) {
                    //删除历史数据
                    baClaimHistoryMapper.deleteBaClaimHistoryById(claimHistory.getId());
                }
            }
            for (BaContractStart baContractStart:baClaim.getBaContractStartList()) {
                    BaClaimHistory baClaimHistory = new BaClaimHistory();
                if(StringUtils.isNotEmpty(baContractStart.getHistoryId())) {
                    baClaimHistory.setId(baContractStart.getHistoryId());
                }else {
                    baClaimHistory.setId(getRedisIncreID.getId());
                }
                    baClaimHistory.setClaimId(baClaim.getId());
                    baClaimHistory.setOrderId(baContractStart.getId());
                    baClaimHistory.setCurrentCollection(baContractStart.getCurrentCollection());
                    baClaimHistory.setClaimState("1");
                    baClaimHistoryMapper.insertBaClaimHistory(baClaimHistory);


            }
        }
        return baClaimMapper.updateBaClaim(baClaim);
    }

    /**
     * 批量删除认领
     *
     * @param ids 需要删除的认领ID
     * @return 结果
     */
    @Override
    public int deleteBaClaimByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                //查询需删除的本次认领金额
                BaClaim baClaim = baClaimMapper.selectBaClaimById(id);
                BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                //已认领金额减去需删除的本次认领金额
                baCollection.setClaimed(baCollection.getClaimed().subtract(baClaim.getThisClaim()));
                baCollectionMapper.updateBaCollection(baCollection);
                //删除订单历史数据
                QueryWrapper<BaClaimHistory> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("claim_id",id);
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(queryWrapper);
                for (BaClaimHistory baClaimHistory:baClaimHistories) {
                    baClaimHistoryMapper.deleteBaClaimHistoryById(baClaimHistory.getId());
                }
                baClaimMapper.deleteBaClaimById(id);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除认领信息
     *
     * @param id 认领ID
     * @return 结果
     */
    @Override
    public int deleteBaClaimById(String id)
    {
        return baClaimMapper.deleteBaClaimById(id);
    }

    @Override
    public List<BaOrderDTO> baOrderList() {
        //查询订单
        List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
        //查询合同相关订单
        QueryWrapper<BaOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_state","1").and(query ->query.eq("order_type","1").or().eq("order_type","4"));
        queryWrapper.eq("flag",0);
        List<BaOrder> orderList  = baOrderMapper.selectList(queryWrapper);
        for (BaOrder baorder:orderList) {
            if(baorder.getOrderReceive().compareTo(baorder.getStayCollected()) == 1){
                BaOrderDTO baOrderDTO = new BaOrderDTO();
                BaOrder baOrder = baOrderMapper.selectBaOrderById(baorder.getId());
                baOrderDTO.setOrderName(baOrder.getOrderName());
                baOrderDTO.setGoodsName(baOrder.getGoodsName());
                baOrderDTO.setOrderPay(baOrder.getOrderPay());
                baOrderDTO.setOrderReceive(baOrder.getOrderReceive());
                baOrderDTO.setAccumulatedAmount(baOrder.getStayCollected());
                baOrderDTO.setId(baOrder.getId());
                if(StringUtils.isNotEmpty(baOrder.getGoodsId())){
                    baOrderDTO.setGoodsName(goodsMapper.selectBaGoodsById(baOrder.getGoodsId()).getName());
                }
                baOrderDTO.setTransportType(baOrder.getTransportType());
                //订单对应的运输信息
                QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("order_id",baOrder.getId());
                transportQueryWrapper.eq("flag",0);
                BaTransport baTransport = baTransportMapper.selectOne(transportQueryWrapper);
                if(StringUtils.isNotNull(baTransport)){
                    //运输编号
                    baOrderDTO.setTransportId(baTransport.getId());
                    //以运输信息查询发运信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                    List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                    for (BaCheck baCheck:baChecks) {
                        if(baCheck.getType() == 1){
                            //矿发煤量
                            baOrderDTO.setCheckCoalNum(baCheck.getCheckCoalNum());
                            //发货日期
                            baOrderDTO.setCheckTime(baCheck.getCheckTime());
                            baOrderDTO.setCarsNum(baCheck.getCarsNum());
                        }else {
                            baOrderDTO.setArrivalTime(baCheck.getArrivalTime());
                            baOrderDTO.setCheckDate(baCheck.getCheckTime());
                        }
                    }
                }
                //判断是否有验收信息
                if(baOrderDTO.getArrivalTime() != null){
                    baOrderDTOList.add(baOrderDTO);
                }
            }
        }
        return baOrderDTOList;
    }

    @Override
    public NumsAndMoneyDTO selectBaClaimForUp(BaClaim baClaim) {
        return baClaimMapper.selectBaClaimForUp(baClaim);
    }

    @Override
    public NumsAndMoneyDTO selectBaClaimForMonth(BaClaim baClaim) {
        return baClaimMapper.selectBaClaimForMonth(baClaim);
    }

    @Override
    public NumsAndMoneyDTO selectBaClaimForYear(BaClaim baClaim) {
        return baClaimMapper.selectBaClaimForYear(baClaim);
    }

}
