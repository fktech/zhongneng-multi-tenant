package com.business.system.service;


import com.business.common.core.domain.UR;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.SysNotice;
import com.business.system.domain.dto.StatisticsDTO;
import com.business.system.domain.vo.DataLayoutStatVO;
import com.business.system.domain.vo.HomePageVo;

import java.util.List;
import java.util.Map;

/**
 * 移动端首页数据统计
 */
public interface IHomePageService {

    /**
     * 计划申报与立项草稿是否显示
     */
    public UR screen(Long userId);

    /**
     * 顶部折线
     */
    public UR lineChart(String type);

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    public List<SysNotice> selectNoticeList(SysNotice notice);

    /**
     * 首页中间数据统计
     */
    UR dataStatistics(Long deptId);

    /**
     * 本周计划申报人
     */
    public List<BaDeclare> thisWeekDeclared();

    /**
     * 数据呈现首页统计（累计合同额）
     */
    public UR statContractAmount(String deptId);

    /**
     * 数据呈现首页统计（累计合同额）
     */
    public UR statTradeAmount(String deptId);

    /**
     * 数据呈现首页统计(累计运输量)
     */
    public UR statCheckCoalNum(String deptId);

    /**
     * 数据呈现首页统计(累计运输量)
     */
    public UR profitAccumulate(String deptId);

    /**
     * 业务部数据
     */
    List<StatisticsDTO> statistics();

    public List<DataLayoutStatVO> dataLayoutSubCompany(String type);

    /**
     * 数据呈现事业部首页统计
     */
    public List<DataLayoutStatVO> dataLayoutDepartment(Long deptId, String type);

    /**
     * 二类人业务统计
     */
    public Map selectSalesVolume();
}
