package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.mapper.BaContractMapper;
import com.business.system.mapper.BaOrderMapper;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractApprovalService;
import com.business.system.service.workflow.IOrderApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 订单申请审批结果:审批通过
 */
@Service("orderApprovalContent:processApproveResult:pass")
@Slf4j
public class OrderApprovalAgreeServiceImpl extends IOrderApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ORDER_STATUS_PASS.getCode());
        return result;
    }
}
