package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaCompanyVO;
import com.business.system.domain.vo.BaEnterpriseVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.*;

/**
 * 公司Service业务层处理
 *
 * @author ljb
 * @date 2022-11-30
 */
@Service
public class BaCompanyServiceImpl extends ServiceImpl<BaCompanyMapper, BaCompany> implements IBaCompanyService
{
    private static final Logger log = LoggerFactory.getLogger(BaCompanyServiceImpl.class);
    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaLoadingStationMapper baLoadingStationMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    /**
     * 查询公司
     *
     * @param id 公司ID
     * @return 公司
     */
    @Override
    public BaCompany selectBaCompanyById(String id)
    {
        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(id);
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baCompany.getUserId());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baCompany.setUser(sysUser);
        if(StringUtils.isNotEmpty(baCompany.getStationId())){
            baCompany.setStationName(baLoadingStationMapper.selectBaLoadingStationById(baCompany.getStationId()).getName());
        }
        //终端企业名称
        String enterpriseName = "";
        if(StringUtils.isNotEmpty(baCompany.getEnterpriseId())){
            String[] ids = baCompany.getEnterpriseId().split(",");
            for (String enterpriseId:ids) {
                enterpriseName = baEnterpriseMapper.selectBaEnterpriseById(enterpriseId).getName()+","+enterpriseName;
            }
        }
        if(StringUtils.isNotEmpty(enterpriseName) && enterpriseName.equals("") == false){
            baCompany.setEnterpriseName(enterpriseName.substring(0,enterpriseName.length()-1));
        }
        //开票信息
        QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
        informationQueryWrapper.eq("relevance_id",id);
        informationQueryWrapper.orderByDesc(id);
        List<BaBillingInformation> baBillingInformation = baBillingInformationMapper.selectList(informationQueryWrapper);
        if(baBillingInformation.size() > 0){
            baCompany.setBillingInformation(baBillingInformation.get(0));
        }
        //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baCompany.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.isNull("state");
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        baCompany.setBaBankList(baBanks);
        //基本账户信息
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baCompany.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.eq("state","1");
        BaBank baBank = baBankMapper.selectOne(queryWrapper);
        baCompany.setBaBank(baBank);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baCompany.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baCompany.setProcessInstanceId(processInstanceIds);
        return baCompany;
    }

    /**
     * 查询公司列表
     *
     * @param baCompany 公司
     * @return 公司
     */
    @Override
    public List<BaCompany> selectBaCompanyList(BaCompany baCompany)
    {
        List<BaCompany> baCompanies = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:baCompanies) {
            //岗位名称
            String name = getName.getName(company.getUserId());
            //发起人
            if(name.equals("") == false){
                company.setUserName(sysUserMapper.selectUserById(company.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                company.setUserName(sysUserMapper.selectUserById(company.getUserId()).getNickName());
            }

        }
        return baCompanies;
    }

    /**
     * 新增公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    @Override
    public int insertBaCompany(BaCompany baCompany)
    {
        //判断公司简称不能重复
        QueryWrapper<BaCompany> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("company_abbreviation",baCompany.getCompanyAbbreviation());
        enterpriseQueryWrapper.eq("flag",0);
        enterpriseQueryWrapper.eq("company_type",baCompany.getCompanyType());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        BaCompany selectOne = baCompanyMapper.selectOne(enterpriseQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -1;
        }
        //判断公司名称不能重复
        enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("name",baCompany.getName());
        enterpriseQueryWrapper.eq("flag",0);
        enterpriseQueryWrapper.eq("company_type",baCompany.getCompanyType());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        selectOne = baCompanyMapper.selectOne(enterpriseQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -2;
        }
        baCompany.setId(getRedisIncreID.getId());
        baCompany.setCreateTime(DateUtils.getNowDate());
        baCompany.setCreateBy(SecurityUtils.getUsername());
        baCompany.setUserId(SecurityUtils.getUserId());
        baCompany.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baCompany.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baCompany.getWorkState())) {
            //流程发起状态
            baCompany.setWorkState("1");
            //默认审批通过
            baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        }
        //基本账户
        if(StringUtils.isNotNull(baCompany.getBaBank())){
            BaBank baBank = baCompany.getBaBank();
            baBank.setId(getRedisIncreID.getId());
            baBank.setCreateTime(DateUtils.getNowDate());
            baBank.setCreateBy(SecurityUtils.getUsername());
            baBank.setRelationId(baCompany.getId());
            baBank.setState("1");
            baBankMapper.insertBaBank(baBank);
        }
        //收款账户
        List<BaBank> baBankList = baCompany.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                item.setId(getRedisIncreID.getId());
                item.setCreateTime(DateUtils.getNowDate());
                item.setCreateBy(SecurityUtils.getUsername());
                item.setRelationId(baCompany.getId());
                baBankMapper.insertBaBank(item);
            });
        }
        //新增开票信息
        if(StringUtils.isNotNull(baCompany.getBillingInformation())){
            BaBillingInformation baBillingInformation = baCompany.getBillingInformation();
            baBillingInformation.setId(getRedisIncreID.getId());
            baBillingInformation.setRelevanceId(baCompany.getId());
            baBillingInformationMapper.insertBaBillingInformation(baBillingInformation);
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(),SecurityUtils.getCurrComId());
        baCompany.setFlowId(flowId);
        int result = baCompanyMapper.insertBaCompany(baCompany);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baCompany.getWorkState())) {
                BaCompany company = baCompanyMapper.selectBaCompanyById(baCompany.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(company, AdminCodeEnum.COMPANY_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baCompanyMapper.deleteBaCompanyById(baCompany.getId());
                    return 0;
                } else {
                    company.setState(AdminCodeEnum.COMPANY_STATUS_VERIFYING.getCode());
                    baCompanyMapper.updateBaCompany(company);
                }
            }
        }
        return result;
    }

    /**
     * 提交终端企业审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaCompany company, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(company.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(company.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //获取流程实例关联的业务对象
        //BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(company.getId());
        BaCompany baCompany = this.selectBaCompanyById(company.getId());
        SysUser sysUser = iSysUserService.selectUserById(baCompany.getUserId());
        baCompany.setUserName(sysUser.getUserName());
        baCompany.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baCompany, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.COMPANY_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改公司
     *
     * @param baCompany 公司
     * @return 结果
     */
    @Override
    public int updateBaCompany(BaCompany baCompany)
    {
        //获取原终端企业信息
        BaCompany baCompany1 = baCompanyMapper.selectBaCompanyById(baCompany.getId());
        if(baCompany1.getCompanyAbbreviation().equals(baCompany.getCompanyAbbreviation()) == false){
            QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
            companyQueryWrapper.eq("company_abbreviation",baCompany.getCompanyAbbreviation());
            companyQueryWrapper.eq("flag",0);
            companyQueryWrapper.eq("company_type",baCompany.getCompanyType());
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            }
            BaCompany selectOne = baCompanyMapper.selectOne(companyQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        if(baCompany1.getName().equals(baCompany.getName()) == false){
            QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
            companyQueryWrapper.eq("name",baCompany.getName());
            companyQueryWrapper.eq("flag",0);
            companyQueryWrapper.eq("company_type",baCompany.getCompanyType());
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            }
            BaCompany selectOne = baCompanyMapper.selectOne(companyQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
        baCompany.setUpdateTime(DateUtils.getNowDate());
        baCompany.setUpdateBy(SecurityUtils.getUsername());
        //创建时间
        baCompany.setCreateTime(baCompany.getUpdateTime());
        //修改开票信息
        if(StringUtils.isNotNull(baCompany.getBillingInformation())){
            BaBillingInformation billingInformation = baCompany.getBillingInformation();
            if(StringUtils.isNotEmpty(billingInformation.getId())){
                baBillingInformationMapper.updateBaBillingInformation(baCompany.getBillingInformation());
            }else {
                billingInformation.setId(getRedisIncreID.getId());
                billingInformation.setRelevanceId(baCompany.getId());
                baBillingInformationMapper.insertBaBillingInformation(billingInformation);
            }

        }
        //基本账户
        if(StringUtils.isNotNull(baCompany.getBaBank())){
            BaBank baBank = baCompany.getBaBank();
            baBank.setUpdateTime(DateUtils.getNowDate());
            baBank.setUpdateBy(SecurityUtils.getUsername());
            baBankMapper.updateBaBank(baBank);
        }
        //收款账户信息
        List<BaBank> baBankList = baCompany.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }
        int result = baCompanyMapper.updateBaCompany(baCompany);
        if(result > 0  && baCompany1.getState().equals(AdminCodeEnum.COMPANY_STATUS_PASS.getCode()) == false){
            BaCompany company = baCompanyMapper.selectBaCompanyById(baCompany.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",company.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(company, AdminCodeEnum.COMPANY_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baCompanyMapper.updateBaCompany(baCompany1);
                return 0;
            }else {
                company.setState(AdminCodeEnum.COMPANY_STATUS_VERIFYING.getCode());
                baCompanyMapper.updateBaCompany(company);
            }
        }
        return result;
    }

    @Override
    public int updateCompany(BaCompany baCompany) {
        //修改开票信息
        if(StringUtils.isNotNull(baCompany.getBillingInformation())){
            BaBillingInformation billingInformation = baCompany.getBillingInformation();
            if(StringUtils.isNotEmpty(billingInformation.getId())){
                baBillingInformationMapper.updateBaBillingInformation(baCompany.getBillingInformation());
            }else {
                billingInformation.setId(getRedisIncreID.getId());
                billingInformation.setRelevanceId(baCompany.getId());
                baBillingInformationMapper.insertBaBillingInformation(billingInformation);
            }

        }
        //收款账户信息
        List<BaBank> baBankList = baCompany.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }
        return baCompanyMapper.updateBaCompany(baCompany);
    }

    /**
     * 批量删除公司
     *
     * @param ids 需要删除的公司ID
     * @return 结果
     */
    @Override
    public int deleteBaCompanyByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaCompany baCompany = baCompanyMapper.selectBaCompanyById(id);
               baCompany.setFlag(1);
               baCompanyMapper.updateBaCompany(baCompany);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 500;
        }
        return 200;
    }

    /**
     * 删除公司信息
     *
     * @param id 公司ID
     * @return 结果
     */
    @Override
    public int deleteBaCompanyById(String id)
    {
        return baCompanyMapper.deleteBaCompanyById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaCompany company = baCompanyMapper.selectBaCompanyById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",company.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.COMPANY_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(company.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                company.setState(AdminCodeEnum.COMPANY_STATUS_WITHDRAW.getCode());
                baCompanyMapper.updateBaCompany(company);
                String userId = String.valueOf(company.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(company, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int updateProcessBusinessData(BaCompanyInstanceRelatedEditDTO baCompanyInstanceRelatedEditDTO) {
        BaCompany baCompany = baCompanyInstanceRelatedEditDTO.getBaCompany();
        if (!ObjectUtils.isEmpty(baCompanyInstanceRelatedEditDTO.getBaCompany())) {
            //获取原企业信息
            BaCompany baCompany1 = baCompanyMapper.selectBaCompanyById(baCompany.getId());
            if (StringUtils.isNotEmpty(baCompany1.getCompanyAbbreviation()) && baCompany1.getCompanyAbbreviation().equals(baCompany1.getCompanyAbbreviation())) {
                QueryWrapper<BaCompany> baCompanyQueryWrapper = new QueryWrapper<>();
                baCompanyQueryWrapper.eq("company_abbreviation", baCompany.getCompanyAbbreviation());
                baCompanyQueryWrapper.eq("company_type", baCompany.getCompanyType());
                baCompanyQueryWrapper.eq("flag", 0);
                baCompanyQueryWrapper.ne("id", baCompany1.getId());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baCompanyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                }
                BaCompany selectOne = baCompanyMapper.selectOne(baCompanyQueryWrapper);
                if (StringUtils.isNotNull(selectOne)) {
                    return -1;
                }
            }

            if(baCompany1.getName().equals(baCompany.getName()) == false){
                QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
                companyQueryWrapper.eq("name",baCompany.getName());
                companyQueryWrapper.eq("flag",0);
                companyQueryWrapper.eq("company_type",baCompany.getCompanyType());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                }
                BaCompany selectOne = baCompanyMapper.selectOne(companyQueryWrapper);
                if(StringUtils.isNotNull(selectOne)){
                    return -2;
                }
            }
            baCompany.setUpdateTime(DateUtils.getNowDate());
            baCompany.setUpdateBy(SecurityUtils.getUsername());
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(baCompanyInstanceRelatedEditDTO.getProcessInstanceId());
            baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(baCompany));
            try {
                baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
            } catch (Exception e) {
                return 0;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public List<BaCompany> companyList(BaCompany baCompany) {
        QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("state","company:pass");
        companyQueryWrapper.eq("flag",0);
        if(StringUtils.isNotEmpty(baCompany.getCompanyType())){
            companyQueryWrapper.eq("company_type",baCompany.getCompanyType());
        }
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanies = baCompanyMapper.selectList(companyQueryWrapper);
        return baCompanies;
    }

    @Override
    public UR counts() {
        Map<String, String> map = new HashMap<>();
        //供应商
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("flag",0);
        supplierQueryWrapper.eq("state",AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaSupplier> baSuppliers = baSupplierMapper.selectList(supplierQueryWrapper);
        map.put("供应商",String.valueOf(baSuppliers.size()));
        //终端企业
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("flag",0);
        enterpriseQueryWrapper.eq("state",AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(enterpriseQueryWrapper);
        map.put("终端企业",String.valueOf(baEnterpriseList.size()));
        //托盘公司
        QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","托盘公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanies = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("托盘公司",String.valueOf(baCompanies.size()));
        //下游贸易商
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","下游贸易商");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("下游贸易商",String.valueOf(baCompanyList.size()));
        //公路运输类公司
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","汽运公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList1 = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("公路运输类公司",String.valueOf(baCompanyList1.size()));
        //运输托运单位
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","铁路公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList2 = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("运输托运单位",String.valueOf(baCompanyList2.size()));
        //采购公司
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","采购公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList3 = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("采购公司",String.valueOf(baCompanyList3.size()));
        //装卸服务公司
        companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("company_type","装卸服务公司");
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.eq("state",AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList4 = baCompanyMapper.selectList(companyQueryWrapper);
        map.put("装卸服务公司",String.valueOf(baCompanyList4.size()));
        //集团公司
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.isNotNull("create_time");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            groupQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompanyGroup> baCompanyGroups = baCompanyGroupMapper.selectList(groupQueryWrapper);
        map.put("集团公司",String.valueOf(baCompanyGroups.size()));
        return UR.ok().data("map",map);
    }

    @Override
    public List<BaCompanyVO> countRanking() {
        return baCompanyMapper.countRanking();
    }


    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCompany company, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(company.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"公司审批提醒",msgContent,baMessage.getType(),company.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
