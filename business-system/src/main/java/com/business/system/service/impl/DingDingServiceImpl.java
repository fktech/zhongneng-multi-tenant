package com.business.system.service.impl;


import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.dingding.DingTalkUtil;
import com.business.system.domain.OaAttendanceAddress;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaWeekday;
import com.business.system.domain.OaWorkTimes;
import com.business.system.mapper.OaWorkTimesMapper;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.domain.dto.DingTalkClockDTO;
import com.business.system.service.DingDingService;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.*;
import com.dingtalk.api.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 发送邮件短信实现类
 *
 * @author jiahe
 * @date 2020.7
 */
@Service
public class DingDingServiceImpl implements DingDingService {

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OaWorkTimesMapper oaWorkTimesMapper;


    @Override
    public String getUpDate() throws Exception{
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/getupdatedata");
        OapiAttendanceGetupdatedataRequest req = new OapiAttendanceGetupdatedataRequest();
        req.setUserid("3227655557654174");
        req.setWorkDate(StringUtils.parseDateTime("2024-5-7"));
        OapiAttendanceGetupdatedataResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String getDingTalk(DingTalkClockDTO dingTalkClockDTO) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/attendance/list");
        OapiAttendanceListRequest req = new OapiAttendanceListRequest();
        //workDateFrom : "2024-5-7 9:00:00"
        req.setWorkDateFrom(dingTalkClockDTO.getWorkDateFrom());
        //workDateFrom : "2024-5-7 18:00:00"
        req.setWorkDateTo(dingTalkClockDTO.getWorkDateTo());
//        List<String> userIdList = new ArrayList<>();
//        userIdList.add("3227655557654174");
        req.setUserIdList(dingTalkClockDTO.getUserIdList());
        req.setOffset(1L);
        req.setLimit(10L);
        OapiAttendanceListResponse rsp = client.execute(req, accessToken);
        return rsp.getBody();
    }

    @Override
    public String getColumnVal() throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/getcolumnval");
        OapiAttendanceGetcolumnvalRequest req = new OapiAttendanceGetcolumnvalRequest();
        req.setUserid("3227655557654174");
        req.setColumnIdList("1069149473,1069149474,1069149475");
        req.setFromDate(StringUtils.parseDateTime("2024-5-7 12:12:12"));
        req.setToDate(StringUtils.parseDateTime("2024-5-7 12:12:12"));
        OapiAttendanceGetcolumnvalResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String getAttColumns() throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/getattcolumns");
        OapiAttendanceGetattcolumnsRequest req = new OapiAttendanceGetattcolumnsRequest();
        OapiAttendanceGetattcolumnsResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String getListRecord(List<String> userIds) throws Exception {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(new Date());
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/attendance/listRecord");
        OapiAttendanceListRecordRequest req = new OapiAttendanceListRecordRequest();
        req.setUserIds(userIds);
        req.setCheckDateFrom(queryTime+" 00:00:00");
        req.setCheckDateTo(queryTime+" 23:59:59");
        req.setIsI18n(false);
        OapiAttendanceListRecordResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String groupAdd(OaAttendanceGroup attendanceGroup) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/group/add");
        OapiAttendanceGroupAddRequest req = new OapiAttendanceGroupAddRequest();
        //操作人的userid
        req.setOpUserId("015359660932750567");
        //考勤组相关信息
        OapiAttendanceGroupAddRequest.TopGroupVo topGroupVo = new OapiAttendanceGroupAddRequest.TopGroupVo();
        //考勤组负责人
        //topGroupVo.setOwner("");
        //未排班时是否允许员工选择班次打卡。
        topGroupVo.setEnableEmpSelectClass(true);
        //企业的CorpId
        //topGroupVo.setCorpId("dinge8xxxx");
        //是否跳过节假日  true（默认）：跳过     false：不跳过
        topGroupVo.setSkipHolidays(true);
        //特殊日期配置
        //topGroupVo.setSpecialDays("{\"onDuty\":{1400000:123,1400001:123},\"offDuty\":[1400000,1400001]}");
        //是否开启外勤打卡必须拍照  true：开启    false（默认）：关闭
        topGroupVo.setEnableOutsideCameraCheck(false);
        //考勤地点相关设置信息
        List<OapiAttendanceGroupAddRequest.TopPositionVo> positionVos = new ArrayList<>();
        //打卡地址
        if(attendanceGroup.getOaAttendanceAddresses().size() > 0){
            for (OaAttendanceAddress attendanceAddress:attendanceGroup.getOaAttendanceAddresses()) {
                OapiAttendanceGroupAddRequest.TopPositionVo topPositionVo = new OapiAttendanceGroupAddRequest.TopPositionVo();
                positionVos.add(topPositionVo);
                //考勤地址
                topPositionVo.setAddress(attendanceAddress.getAddress());
                //企业的CorpId
                //topPositionVo.setCorpId("dinge8xxxx");
                //纬度
                topPositionVo.setLatitude(attendanceAddress.getLatitude());
                //精度
                //topPositionVo.setAccuracy("0");
                //考勤标题
                //topPositionVo.setTitle("青藏高原自然博物馆");
                //经度
                topPositionVo.setLongitude(attendanceAddress.getLongitude());
            }
        }
        topGroupVo.setPositions(positionVos);
        //是否有修改考勤组成员相关信息    true：修改    false：未修改
        topGroupVo.setModifyMember(true);
        //考考勤组类型   FIXED：固定班制考勤组   TURN：排班制考勤组  NONE：自由工时考勤组
        topGroupVo.setType("FIXED");
        //是是否开启人脸检测  true：开启   false（默认）：关闭
        topGroupVo.setEnableFaceCheck(false);
        //打卡是否需要健康码   true：开启   false（默认）：关闭
        topGroupVo.setCheckNeedHealthyCode(false);
        //是否开启拍照打卡   true：开启   false（默认）：关闭
        topGroupVo.setEnableCameraCheck(false);
        //班次相关配置信息
        ArrayList<OapiAttendanceGroupAddRequest.TopShiftVo> topShiftVos = new ArrayList<>();
        OapiAttendanceGroupAddRequest.TopShiftVo topShiftVo = new OapiAttendanceGroupAddRequest.TopShiftVo();
        topShiftVos.add(topShiftVo);
        //班次ID
        topShiftVo.setId(1226735003L);

        //topGroupVo.setShiftVoList(topShiftVos);
        //是否可以外勤打卡
        topGroupVo.setEnableOutsideCheck(false);
        //考勤组成员相关设置信息
        ArrayList<OapiAttendanceGroupAddRequest.TopMemberVo> memberVos = new ArrayList<>();
        OapiAttendanceGroupAddRequest.TopMemberVo topMemberVo = new OapiAttendanceGroupAddRequest.TopMemberVo();
        memberVos.add(topMemberVo);
        //获取所有考勤组成员
        String[] split = attendanceGroup.getAttendanceMembers().split(",");
        List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        Long[] deptIds = l.toArray(new Long[l.size()]);
        List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
        //角色，固定值Attendance
        topMemberVo.setRole("Attendance");
        //企业的CorpId
        //topMemberVo.setCorpId("dinge8xxxx");
        //类型，固定值StaffMember
        topMemberVo.setType("StaffMember");
        //用户userid
        topMemberVo.setUserId(sysUsers.get(0).getDingDing());

        topGroupVo.setMembers(memberVos);
        //考勤组名
        topGroupVo.setName(attendanceGroup.getAttendanceName());
        //是否第二天生效
        topGroupVo.setEnableNextDay(false);
        //考勤组子管理员userid列表
        //topGroupVo.setManagerList(Arrays.asList("userId1","userId2"));
        //默认班次ID
        topGroupVo.setDefaultClassId(1226735003L);
        //考勤范围
        topGroupVo.setOffset(attendanceGroup.getOaAttendanceAddresses().get(0).getScope());
        //工作日
        List<Long> week = new ArrayList<>();
        if(attendanceGroup.getOaWeekdays().size() > 0){
            OaWeekday oaWeekday = attendanceGroup.getOaWeekdays().get(attendanceGroup.getOaWeekdays().size() - 1);
            //获取星期天放首位
            OaWorkTimes workTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
            if(StringUtils.isNotNull(workTimes)){
                week.add(Long.valueOf(workTimes.getDingDing()));
            }else {
                week.add(0L);
            }
            for (OaWeekday weekday:attendanceGroup.getOaWeekdays()) {
                if(weekday.getName().equals("星期一")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
                if(weekday.getName().equals("星期二")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
                if(weekday.getName().equals("星期三")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
                if(weekday.getName().equals("星期四")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
                if(weekday.getName().equals("星期五")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
                if(weekday.getName().equals("星期六")){
                    //获取班次信息
                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday.getWorkTimeId());
                    if(StringUtils.isNotNull(oaWorkTimes)){
                        week.add(Long.valueOf(oaWorkTimes.getDingDing()));
                    }else {
                        week.add(0L);
                    }
                }
            }
        }
        topGroupVo.setWorkdayClassList(new ArrayList<>(week));
        //子管理员权限范围
        OapiAttendanceGroupAddRequest.TopGroupManageRolePermissionVo groupManageRolePermissionVo = new OapiAttendanceGroupAddRequest.TopGroupManageRolePermissionVo();
        //员工排班
        groupManageRolePermissionVo.setSchedule("w");
        //设置参与考勤人员
        groupManageRolePermissionVo.setGroupMember("w");
        //设置考勤类型
        groupManageRolePermissionVo.setGroupType("r");
        //设置考勤时间
        groupManageRolePermissionVo.setCheckTime("w");
        //设置打卡方式
        groupManageRolePermissionVo.setCheckPositionType("r");
        //设置加班规则
        groupManageRolePermissionVo.setOverTimeRule("r");
        //设置拍照打卡规则
        groupManageRolePermissionVo.setCameraCheck("w");
        //设置外勤打卡
        groupManageRolePermissionVo.setOutSideCheck("w");

        topGroupVo.setResourcePermissionMap(groupManageRolePermissionVo);
        //考勤wifi打卡相关配置信息
        ArrayList<OapiAttendanceGroupAddRequest.TopWifiVo> topWifiVos = new ArrayList<>();
        OapiAttendanceGroupAddRequest.TopWifiVo topWifiVo = new OapiAttendanceGroupAddRequest.TopWifiVo();
        topWifiVos.add(topWifiVo);
        //mac地址
        topWifiVo.setMacAddr("C0:E0:D0:E0:C0:0F");
        //wifi的ssid
        topWifiVo.setSsid("OFFICE-WiFi");
        //企业的CorpId
        //topWifiVo.setCorpId("dinge8xxxx");

        topGroupVo.setWifis(topWifiVos);
        //未排班时是否禁止员工打卡
        topGroupVo.setDisableCheckWithoutSchedule(false);
        //自由工时考勤组考勤开始时间与当天0点偏移分钟数
        topGroupVo.setFreecheckDayStartMinOffset(240L);
        //休息日打卡是否需审批
        topGroupVo.setDisableCheckWhenRest(true);
        req.setTopGroup(topGroupVo);
        OapiAttendanceGroupAddResponse rsp = client.execute(req, accessToken);
        return rsp.getBody();
    }

    @Override
    public String groupModify(OaAttendanceGroup attendanceGroup) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/group/modify");
        OapiAttendanceGroupModifyRequest req = new OapiAttendanceGroupModifyRequest();
        //操作人
        req.setOpUserId("015359660932750567");
        //考勤组相关信息
        OapiAttendanceGroupModifyRequest.TopGroupVo topGroupVo = new OapiAttendanceGroupModifyRequest.TopGroupVo();
        topGroupVo.setOwner("user1");
        topGroupVo.setEnableEmpSelectClass(true);
        topGroupVo.setSkipHolidays(true);
        List<OapiAttendanceGroupModifyRequest.TopPositionVo> positionVos = new ArrayList<OapiAttendanceGroupModifyRequest.TopPositionVo>();
        OapiAttendanceGroupModifyRequest.TopPositionVo topPositionVo = new OapiAttendanceGroupModifyRequest.TopPositionVo();
        positionVos.add(topPositionVo);
        topPositionVo.setAddress("生物科技产业园区经二路21号");
        topPositionVo.setCorpId("ding9f741xxxx");
        topPositionVo.setLatitude("30.123");
        topPositionVo.setAccuracy("0");
        topPositionVo.setTitle("青藏高原自然博物馆");
        topPositionVo.setLongitude("120.123");
        topGroupVo.setPositions(positionVos);
        topGroupVo.setEnableFaceCheck(true);
        topGroupVo.setEnableCameraCheck(true);
        List<OapiAttendanceGroupModifyRequest.TopShiftVo> topShiftVos = new ArrayList<OapiAttendanceGroupModifyRequest.TopShiftVo>();
        OapiAttendanceGroupModifyRequest.TopShiftVo topShiftVo = new OapiAttendanceGroupModifyRequest.TopShiftVo();
        topShiftVos.add(topShiftVo);
        topShiftVo.setId(678215070L);
        topGroupVo.setShiftVoList(topShiftVos);
        topGroupVo.setEnableOutsideCheck(true);
        topGroupVo.setName("测试考勤组1");
        topGroupVo.setId(2987L);
        topGroupVo.setManagerList(Arrays.asList("userId1"));
        topGroupVo.setOffset(500L);
        topGroupVo.setDisableCheckWithoutSchedule(false);
        topGroupVo.setWorkdayClassList(Arrays.asList(12L,12L,12L,12L,12L,0L,0L));
        req.setTopGroup(topGroupVo);
        OapiAttendanceGroupModifyResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String groupIdToKey(String groupId) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/groups/idtokey");
        OapiAttendanceGroupsIdtokeyRequest req = new OapiAttendanceGroupsIdtokeyRequest();
        req.setOpUserId("015359660932750567");
        req.setGroupId(Long.valueOf(groupId));
        OapiAttendanceGroupsIdtokeyResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String groupDelete(String groupId) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/group/delete");
        OapiAttendanceGroupDeleteRequest req = new OapiAttendanceGroupDeleteRequest();
        req.setOpUserid("015359660932750567");
        req.setGroupKey(groupId);
        OapiAttendanceGroupDeleteResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String shiftAdd(OaWorkTimes oaWorkTimes) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/shift/add");
        OapiAttendanceShiftAddRequest request = new OapiAttendanceShiftAddRequest();
        OapiAttendanceShiftAddRequest.TopAtClassVo topAtClassVo = new OapiAttendanceShiftAddRequest.TopAtClassVo();
        //班次owner
        //topAtClassVo.setOwner("015359660932750567");
        //班次组名
        //topAtClassVo.setClassGroupName(oaWorkTimes.getName());
        //企业的corpId
        //topAtClassVo.setCorpId("dinge8a56572xxxx");
        //判断是新增还是修改
        if(StringUtils.isNotEmpty(oaWorkTimes.getId())){
            //班次ID
            if(StringUtils.isNotEmpty(oaWorkTimes.getDingDing())){
                topAtClassVo.setId(Long.valueOf(oaWorkTimes.getDingDing()));
            }
        }
        //班次名称
        topAtClassVo.setName(oaWorkTimes.getName());
        //卡段
        List<OapiAttendanceShiftAddRequest.TopAtSectionVo> sectionVos = new ArrayList<>();
        OapiAttendanceShiftAddRequest.TopAtSectionVo sectionVo = new OapiAttendanceShiftAddRequest.TopAtSectionVo();
        //打卡信息
        List<OapiAttendanceShiftAddRequest.TopAtTimeVo> timeVos = new ArrayList<>();
        OapiAttendanceShiftAddRequest.TopAtTimeVo timeVo = new OapiAttendanceShiftAddRequest.TopAtTimeVo();
        //是否跨天   0：不跨天   1：跨天
        timeVo.setAcross(0L);
        //允许的最早提前打卡时间，分钟为单位
        timeVo.setBeginMin(oaWorkTimes.getGoStartMin());
        //打卡时间
        timeVo.setCheckTime(oaWorkTimes.getGoWorkTime());
        //打卡类型   OnDuty：上班   OffDuty：下班
        timeVo.setCheckType("OnDuty");
        //允许的最晚打卡时间，单位分钟（-1表示不限制）
        timeVo.setEndMin(oaWorkTimes.getGoEndMin());
        //是否免打卡
        timeVo.setFreeCheck(false);
        OapiAttendanceShiftAddRequest.TopAtTimeVo timeVo1 = new OapiAttendanceShiftAddRequest.TopAtTimeVo();
        //是否跨天   0：不跨天   1：跨天
        timeVo1.setAcross(0L);
        ////允许的最晚打卡时间，单位分钟（-1表示不限制）
        timeVo1.setBeginMin(oaWorkTimes.getOffStartMin());
        //打卡时间
        timeVo1.setCheckTime(oaWorkTimes.getOffWorkTime());
        //打卡类型   OnDuty：上班   OffDuty：下班
        timeVo1.setCheckType("OffDuty");
        //允许的最晚打卡时间，单位分钟（-1表示不限制）
        timeVo1.setEndMin(oaWorkTimes.getOffEndMin());
        //是否免打卡
        timeVo1.setFreeCheck(false);
        //上班打卡信息
        timeVos.add(timeVo);
        //下班打卡信息
        timeVos.add(timeVo1);
        //打卡信息
        sectionVo.setTimes(timeVos);
        //卡段
        sectionVos.add(sectionVo);

        topAtClassVo.setSections(sectionVos);
        //设置
        OapiAttendanceShiftAddRequest.TopAtClassSettingVo settingVo = new OapiAttendanceShiftAddRequest.TopAtClassSettingVo();
        topAtClassVo.setSetting(settingVo);
        //休息开始
        //OapiAttendanceShiftAddRequest.TopAtTimeVo restBeginTime = new OapiAttendanceShiftAddRequest.TopAtTimeVo();
        //settingVo.setRestBeginTime(restBeginTime);
        //是否免打卡
        //restBeginTime.setFreeCheck(false);
        //是否跨天，跨天是指休息时间是第二天   0：不跨天   1：跨天
        //restBeginTime.setAcross(restBeginTime.getAcross());
        //休息类型   OnDuty：休息开始    OffDuty：休息结束
        //restBeginTime.setCheckType("OnDuty");
        //是否免打卡
        //restBeginTime.setFreeCheck(false);
        //休息打卡时间
        //restBeginTime.setCheckTime(StringUtils.parseDateTime("2020-12-02 12:00:00"));
        //休息结束
        //OapiAttendanceShiftAddRequest.TopAtTimeVo restEndTime = new OapiAttendanceShiftAddRequest.TopAtTimeVo();
        //settingVo.setRestEndTime(restEndTime);
        //是否跨天，跨天是指休息时间是第二天   0：不跨天   1：跨天
        //restEndTime.setAcross(1L);
        //休息类型   OnDuty：休息开始    OffDuty：休息结束
        //restEndTime.setCheckType("OffDuty");
        //是否免打卡
        //restEndTime.setFreeCheck(false);
        //休息时间
        //restEndTime.setCheckTime(StringUtils.parseDateTime("2020-12-02 13:00:00"));
        //企业的corpId
        //settingVo.setCorpId("dinge8a56572xxxx");
        //是否删除   N：是    Y：否
        //settingVo.setIsDeleted("N");
        //旷工早退/迟到的时长，单位分钟
        //settingVo.setAbsenteeismLateMinutes(60L);
        //是否弹性  true：弹性    false：非弹性
        //settingVo.setIsFlexible(false);
        //严重早退/迟到的时长，单位分钟
        //settingVo.setSeriousLateMinutes(30L);
        //操作人userId
        request.setOpUserId("015359660932750567");
        //班次
        request.setShift(topAtClassVo);
        OapiAttendanceShiftAddResponse response = client.execute(request, accessToken);
        System.out.println(response.getBody());
        return response.getBody();
    }

    @Override
    public String shiftDelete(Long shiftId) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/shift/delete");
        OapiAttendanceShiftDeleteRequest req = new OapiAttendanceShiftDeleteRequest();
        req.setOpUserId("015359660932750567");
        req.setShiftId(shiftId);
        OapiAttendanceShiftDeleteResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String groupUsersAdd() throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/group/users/add");
        OapiAttendanceGroupUsersAddRequest req = new OapiAttendanceGroupUsersAddRequest();
        //操作人userid
        req.setOpUserid("user01");
        //考勤组ID
        req.setGroupKey("CEDDxxxx");
        //用户列表，最大值100
        req.setUserIdList("user02,user03");
        OapiAttendanceGroupUsersAddResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String groupMemberUpdate(OaAttendanceGroup attendanceGroup) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/attendance/group/member/update");
        OapiAttendanceGroupMemberUpdateRequest req = new OapiAttendanceGroupMemberUpdateRequest();
        //操作人userId
        req.setOpUserId("015359660932750567");
        //考勤组ID
        req.setGroupId(Long.valueOf(attendanceGroup.getDingDing()));
        //从哪天开始排班
        req.setScheduleFlag(0L);
        //更新考勤组信息
        OapiAttendanceGroupMemberUpdateRequest.TopGroupMemberUpdateParam updateParam = new OapiAttendanceGroupMemberUpdateRequest.TopGroupMemberUpdateParam();
        //添加考勤部门，没有的话，无需赋值，每次调用最多传20个部门ID。
        List<String> list = new ArrayList<>();
        if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){
            String[] split = attendanceGroup.getAttendanceMembers().split(",");
            for (String id:split) {
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(id));
                list.add(dept.getDingDing().toString());
            }
            updateParam.setAddDepts(list);
        }
        //删除考勤部门，没有的话，无需赋值，每次调用最多传20个部门ID。
        //updateParam.setRemoveDepts(Arrays.asList("456"));
        //添加考勤人员，没有的话，无需赋值，每次调用最多传20个userId。
        //updateParam.setAddUsers(Arrays.asList("user123"));
        //添加无需考勤的人员，没有的话，无需赋值，每次调用最多传20个userId。
        List<String> list1 = new ArrayList<>();
        if(StringUtils.isNotEmpty(attendanceGroup.getNonAttendanceMembers())){
            String[] nonAttendanceMembers = attendanceGroup.getNonAttendanceMembers().split(",");
            for (String id:nonAttendanceMembers) {
                SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(id));
                list1.add(sysUser.getDingDing());
            }
            updateParam.setAddExtraUsers(list1);
        }
        //删除无需考勤的成员，没有的话，无需赋值，每次调用最多传20个userId。
        //updateParam.setRemoveExtraUsers(Arrays.asList("user789"));
        //删除考勤人员，没有的话，无需赋值，每次调用最多传20个userId。
        //updateParam.setRemoveUsers(Arrays.asList("user121"));
        req.setUpdateParam(updateParam);
        OapiAttendanceGroupMemberUpdateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String userCreate(SysUser user) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/create");
        OapiV2UserCreateRequest req = new OapiV2UserCreateRequest();
        //员工唯一标识ID（不可修改），企业内必须唯一。
        req.setUserid(user.getUserName());
        //员工名称，长度最大80个字符。
        req.setName(user.getNickName());
        //是否开启高管模式，默认值false   true：开启
        //req.setSeniorMode(false);
        //手机号码，企业内必须唯一，不可重复。
        req.setMobile(user.getPhonenumber());
        //职位，长度最大为200个字符
        //req.setTitle("教职人员");
        //员工个人邮箱，长度最大50个字符
        //req.setEmail("test@xx.com");
        //员工的企业邮箱，长度最大100个字符
        //req.setOrgEmail("test@xxx.com");
        //员工的企业邮箱类型    profession: 标准版    base：基础版
        //req.setOrgEmailType("profession");
        //员工在对应的部门中的职位
        ArrayList<OapiV2UserCreateRequest.DeptTitle> deptTitles = new ArrayList<>();
        OapiV2UserCreateRequest.DeptTitle deptTitle = new OapiV2UserCreateRequest.DeptTitle();
        //部门ID
        //deptTitle.setDeptId(4868821L);
        //员工在部门中的职位
        //deptTitle.setTitle("测试");
        //员工在对应的部门中的职位
        OapiV2UserCreateRequest.DeptTitle deptTitle1 = new OapiV2UserCreateRequest.DeptTitle();
        //部门ID
        //deptTitle1.setDeptId(6099161L);
        //员工在部门中的职位
        //deptTitle1.setTitle("专员");
        //员工在对应的部门中的职位
        //deptTitles.add(deptTitle);
        //员工在对应的部门中的职位
        //deptTitles.add(deptTitle1);
        //req.setDeptTitleList(deptTitles);
        //是否号码隐藏
        //req.setHideMobile(false);
        //分机号，长度最大50个字符
        //req.setTelephone("010-8xxxxx6-2345");
        //员工工号，长度最大为50个字符
        //req.setJobNumber("100828");
        //入职时间，Unix时间戳，单位毫秒
        //req.setHiredDate(1615219200000L);
        //办公地点，长度最大100个字符
        //req.setWorkPlace("未来park");
        //备注，长度最大2000个字符
        //req.setRemark("备注备注");
        //所属部门id列表，每次调用最多传100个部门ID
        req.setDeptIdList(user.getDept().getDingDing().toString());
        //List<OapiV2UserCreateRequest.DeptOrder> deptOrderList = new ArrayList<OapiV2UserCreateRequest.DeptOrder>();
        //OapiV2UserCreateRequest.DeptOrder deptOrder = new OapiV2UserCreateRequest.DeptOrder();
        //部门ID
        //deptOrder.setDeptId(4868821L);
        //员工在部门中的排序
        //deptOrder.setOrder(1L);
        //OapiV2UserCreateRequest.DeptOrder deptOrder1 = new OapiV2UserCreateRequest.DeptOrder();
        ////部门ID
        //deptOrder1.setDeptId(6099161L);
        //员工在部门中的排序
        //deptOrder1.setOrder(1L);
        //员工在对应的部门中的排序
        //deptOrderList.add(deptOrder);
        //deptOrderList.add(deptOrder1);
        //req.setDeptOrderList(deptOrderList);
        //扩展属性，可以设置多种属性
        req.setExtension("{\"爱好\":\"[爱好](http://test.com?userid=#userid#&corpid=#corpid#)\"}");
        //直属主管的userId
        //req.setManagerUserid("001");
        //登录邮箱
        req.setLoginEmail("test@xxx.com");
        OapiV2UserCreateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String userUpdate(SysUser user) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/update");
        OapiV2UserUpdateRequest req = new OapiV2UserUpdateRequest();
        //员工的userId
        req.setUserid(user.getDingDing());
        //员工名称
        req.setName(user.getNickName());
        //req.setHideMobile(false);
        //req.setTelephone("010-86123456-2345");
        //req.setJobNumber("4");
        //req.setManagerUserid("0001");
        //req.setTitle("技术总监");
        //req.setEmail("test@xxx.com");
        //req.setOrgEmail("test@xxx.com");
        //req.setWorkPlace("未来park");
        //req.setRemark("备注备注");
        //部门ID
        req.setDeptIdList(user.getDept().getDingDing().toString());
        //ArrayList<OapiV2UserCreateRequest.DeptOrder> list2 = new ArrayList<>();
        //OapiV2UserCreateRequest.DeptOrder obj3 = new OapiV2UserCreateRequest.DeptOrder();
        //list2.add(obj3);
        //obj3.setDeptId(2L);
        //obj3.setOrder(1L);
        //req.setDeptOrderList(String.valueOf(list2));
        //ArrayList<OapiV2UserCreateRequest.DeptTitle> list5 = new ArrayList<>();
        //OapiV2UserCreateRequest.DeptTitle obj6 = new OapiV2UserCreateRequest.DeptTitle();
        //list5.add(obj6);
        //obj6.setDeptId(2L);
        //obj6.setTitle("资深产品经理");
        //req.setDeptTitleList(String.valueOf(list5));
        //req.setExtension("{\"爱好\":\"旅游\",\"年龄\":\"24\"}");
        //req.setSeniorMode(false);
        //req.setHiredDate(1597573616828L);
        req.setLanguage("zh_CN");
        //req.setForceUpdateFields("manager_userid");
        OapiV2UserUpdateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String userDelete(String userId) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/delete");
        OapiV2UserDeleteRequest req = new OapiV2UserDeleteRequest();
        //员工的userId
        req.setUserid(userId);
        OapiV2UserDeleteResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String departmentCreate(SysDept dept) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/create");
        OapiV2DepartmentCreateRequest req = new OapiV2DepartmentCreateRequest();
        if(dept.getParentId() == 0){
            //父部门ID，根部门ID为1
            req.setParentId(1L);
        }else {
            //获取上级部门关联ID
            SysDept dept1 = sysDeptMapper.selectDeptById(dept.getParentId());
            //父部门ID，根部门ID为1
            req.setParentId(dept1.getDingDing());
        }
        //是否限制本部门成员查看通讯录   true：开启限制。开启后本部门成员只能看到限定范围内的通讯录  false（默认值）：不限制
        req.setOuterDept(true);
        //是否隐藏本部门    true：隐藏部门，隐藏后本部门将不会显示在公司通讯录中    false（默认值）：显示部门
        //req.setHideDept(true);
        //是否创建一个关联此部门的企业群，默认为false即不创建
        //req.setCreateDeptGroup(true);
        //在父部门中的排序值，order值小的排序靠前
        req.setOrder((long)dept.getOrderNum());
        //部门名称
        req.setName(dept.getDeptName());
        //部门标识字段，开发者可用该字段来唯一标识一个部门，并与钉钉外部通讯录里的部门做映射
        req.setSourceIdentifier(dept.getDeptId().toString());
        //指定可以查看本部门的其他部门列表，总数不能超过50。当hide_dept为true时，则此值生效
        //req.setDeptPermits("3,4,5");
        //指定可以查看本部门的人员userId列表，总数不能超过50。当hide_dept为true时，则此值生效
        //req.setUserPermits("100,200");
        //指定本部门成员可查看的通讯录用户userId列表，总数不能超过50。当outer_dept为true时，此参数生效
        //req.setOuterPermitUsers("500,600");
        //指定本部门成员可查看的通讯录部门ID列表，总数不能超过50。当outer_dept为true时，此参数生效
        //req.setOuterPermitDepts("6,7,8");
        //本部门成员是否只能看到所在部门及下级部门通讯录   true：只能看到所在部门及下级部门通讯录   false：不能查看所有通讯录，在通讯录中仅能看到自己   当outer_dept为true时，此参数生效
        //req.setOuterDeptOnlySelf(true);
        OapiV2DepartmentCreateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String departmentUpdate(SysDept dept) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/update");
        OapiV2DepartmentUpdateRequest req = new OapiV2DepartmentUpdateRequest();
        //部门ID
        req.setDeptId(dept.getDingDing());
        //部门名称
        req.setName(dept.getDeptName());
        OapiV2DepartmentUpdateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String departmentDelete(Long deptId) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/delete");
        OapiV2DepartmentDeleteRequest req = new OapiV2DepartmentDeleteRequest();
        //要删除的部门ID
        req.setDeptId(deptId);
        OapiV2DepartmentDeleteResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    @Override
    public String departmentListSub() throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/listsub");
        OapiV2DepartmentListsubRequest req = new OapiV2DepartmentListsubRequest();
        //req.setDeptId(1L);
        req.setLanguage("zh_CN");
        OapiV2DepartmentListsubResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return null;
    }

    @Override
    public String insertDingdingDept(SysDept dept) throws Exception {
        //调用该接口的应用凭证
        String accessToken = DingTalkUtil.getAccessToken();
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/create");
        OapiV2DepartmentCreateRequest req = new OapiV2DepartmentCreateRequest();
        //父部门ID，根部门ID为1
        req.setParentId(dept.getParentId());
        //是否限制本部门成员查看通讯录   true：开启限制。开启后本部门成员只能看到限定范围内的通讯录  false（默认值）：不限制
        req.setOuterDept(true);
        //是否隐藏本部门    true：隐藏部门，隐藏后本部门将不会显示在公司通讯录中    false（默认值）：显示部门
        //req.setHideDept(true);
        //是否创建一个关联此部门的企业群，默认为false即不创建
        //req.setCreateDeptGroup(true);
        //在父部门中的排序值，order值小的排序靠前
        req.setOrder((long)dept.getOrderNum());
        //部门名称
        req.setName(dept.getDeptName());
        //部门标识字段，开发者可用该字段来唯一标识一个部门，并与钉钉外部通讯录里的部门做映射
        req.setSourceIdentifier(dept.getDeptId().toString());
        //指定可以查看本部门的其他部门列表，总数不能超过50。当hide_dept为true时，则此值生效
        //req.setDeptPermits("3,4,5");
        //指定可以查看本部门的人员userId列表，总数不能超过50。当hide_dept为true时，则此值生效
        //req.setUserPermits("100,200");
        //指定本部门成员可查看的通讯录用户userId列表，总数不能超过50。当outer_dept为true时，此参数生效
        //req.setOuterPermitUsers("500,600");
        //指定本部门成员可查看的通讯录部门ID列表，总数不能超过50。当outer_dept为true时，此参数生效
        //req.setOuterPermitDepts("6,7,8");
        //本部门成员是否只能看到所在部门及下级部门通讯录   true：只能看到所在部门及下级部门通讯录   false：不能查看所有通讯录，在通讯录中仅能看到自己   当outer_dept为true时，此参数生效
        //req.setOuterDeptOnlySelf(true);
        OapiV2DepartmentCreateResponse rsp = client.execute(req, accessToken);
        System.out.println(rsp.getBody());
        return rsp.getBody();
    }

    public static void main(String[] args) {
        String accessToken = DingTalkUtil.getAccessToken();
        System.out.println(accessToken);
    }
}
