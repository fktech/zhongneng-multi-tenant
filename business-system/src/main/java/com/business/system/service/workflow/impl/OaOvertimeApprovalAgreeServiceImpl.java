package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaLeave;
import com.business.system.domain.OaOvertime;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaLeaveApprovalService;
import com.business.system.service.workflow.IOaOvertimeApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 加班申请审批结果:审批通过
 */
@Service("oaOvertimeApprovalContent:processApproveResult:pass")
@Slf4j
public class OaOvertimeApprovalAgreeServiceImpl extends IOaOvertimeApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaOvertime oaOvertime = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAOVERTIME_STATUS_PASS.getCode());
        return result;
    }
}
