package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaBlendingProportioningMapper;
import com.business.system.domain.BaBlendingProportioning;
import com.business.system.service.IBaBlendingProportioningService;

/**
 * 配煤配比Service业务层处理
 *
 * @author ljb
 * @date 2024-01-05
 */
@Service
public class BaBlendingProportioningServiceImpl extends ServiceImpl<BaBlendingProportioningMapper, BaBlendingProportioning> implements IBaBlendingProportioningService
{
    @Autowired
    private BaBlendingProportioningMapper baBlendingProportioningMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询配煤配比
     *
     * @param id 配煤配比ID
     * @return 配煤配比
     */
    @Override
    public BaBlendingProportioning selectBaBlendingProportioningById(String id)
    {
        return baBlendingProportioningMapper.selectBaBlendingProportioningById(id);
    }

    /**
     * 查询配煤配比列表
     *
     * @param baBlendingProportioning 配煤配比
     * @return 配煤配比
     */
    @Override
    public List<BaBlendingProportioning> selectBaBlendingProportioningList(BaBlendingProportioning baBlendingProportioning)
    {
        return baBlendingProportioningMapper.selectBaBlendingProportioningList(baBlendingProportioning);
    }

    /**
     * 新增配煤配比
     *
     * @param baBlendingProportioning 配煤配比
     * @return 结果
     */
    @Override
    public int insertBaBlendingProportioning(BaBlendingProportioning baBlendingProportioning)
    {
        baBlendingProportioning.setId(getRedisIncreID.getId());
        baBlendingProportioning.setCreateTime(DateUtils.getNowDate());
        baBlendingProportioning.setCreateBy(SecurityUtils.getUsername());
        baBlendingProportioning.setUserId(SecurityUtils.getUserId());
        return baBlendingProportioningMapper.insertBaBlendingProportioning(baBlendingProportioning);
    }

    /**
     * 修改配煤配比
     *
     * @param baBlendingProportioning 配煤配比
     * @return 结果
     */
    @Override
    public int updateBaBlendingProportioning(BaBlendingProportioning baBlendingProportioning)
    {
        baBlendingProportioning.setUpdateTime(DateUtils.getNowDate());
        return baBlendingProportioningMapper.updateBaBlendingProportioning(baBlendingProportioning);
    }

    /**
     * 批量删除配煤配比
     *
     * @param ids 需要删除的配煤配比ID
     * @return 结果
     */
    @Override
    public int deleteBaBlendingProportioningByIds(String[] ids)
    {
        return baBlendingProportioningMapper.deleteBaBlendingProportioningByIds(ids);
    }

    /**
     * 删除配煤配比信息
     *
     * @param id 配煤配比ID
     * @return 结果
     */
    @Override
    public int deleteBaBlendingProportioningById(String id)
    {
        return baBlendingProportioningMapper.deleteBaBlendingProportioningById(id);
    }

}
