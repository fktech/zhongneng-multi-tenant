package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaDepotItem;

/**
 * 出入库副Service接口
 *
 * @author ljb
 * @date 2023-04-23
 */
public interface IBaDepotItemService
{
    /**
     * 查询出入库副
     *
     * @param id 出入库副ID
     * @return 出入库副
     */
    public BaDepotItem selectBaDepotItemById(String id);

    /**
     * 查询出入库副列表
     *
     * @param baDepotItem 出入库副
     * @return 出入库副集合
     */
    public List<BaDepotItem> selectBaDepotItemList(BaDepotItem baDepotItem);

    /**
     * 新增出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    public int insertBaDepotItem(BaDepotItem baDepotItem);

    /**
     * 修改出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    public int updateBaDepotItem(BaDepotItem baDepotItem);

    /**
     * 批量删除出入库副
     *
     * @param ids 需要删除的出入库副ID
     * @return 结果
     */
    public int deleteBaDepotItemByIds(String[] ids);

    /**
     * 删除出入库副信息
     *
     * @param id 出入库副ID
     * @return 结果
     */
    public int deleteBaDepotItemById(String id);


}
