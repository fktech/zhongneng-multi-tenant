package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessTaskQueryDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.HistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import com.business.system.util.ProcessMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: 终端企业管理审批接口 服务类
 * @date: 2023/3/1 15:12
 */
@Service
public class IEnterpriseApprovalService {

    @Autowired
    protected BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaEnterpriseRelevanceMapper enterpriseRelevanceMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaHiEnterpriseMapper baHiEnterpriseMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaEnterprise baEnterprise = new BaEnterprise();
        baEnterprise.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baEnterprise.getId());
        baEnterprise.setState(status);
        String userId = String.valueOf(baEnterprise.getUserId());
        if(AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode().equals(status)){
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            BaEnterprise baEnterprise1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaEnterprise.class);
            if(!ObjectUtils.isEmpty(baEnterprise1)){
                baEnterprise1.setState(status);
                //所需煤种
                if(StringUtils.isNotNull(baEnterprise1.getEnterpriseRelevance())){
                    List<BaEnterpriseRelevance> enterpriseRelevanceList = baEnterprise1.getEnterpriseRelevance();
                    if(!CollectionUtils.isEmpty(enterpriseRelevanceList)){
                        enterpriseRelevanceList.stream().forEach(item -> {
                            if(StringUtils.isNotEmpty(item.getId())){
                                enterpriseRelevanceMapper.updateBaEnterpriseRelevance(item);
                            }else {
                                item.setId(getRedisIncreID.getId());
                                item.setRelevanceId(baEnterprise1.getId());
                                enterpriseRelevanceMapper.insertBaEnterpriseRelevance(item);
                            }
                        });
                    }
                }
                //修改开票信息
                if(StringUtils.isNotNull(baEnterprise1.getBillingInformation())){
                    baBillingInformationMapper.updateBaBillingInformation(baEnterprise1.getBillingInformation());
                }
                baEnterprise.setUpdateTime(DateUtils.getNowDate());
                //基本账户
                if(StringUtils.isNotNull(baEnterprise1.getBaBank())){
                    BaBank baBank = baEnterprise1.getBaBank();
                    baBank.setUpdateTime(DateUtils.getNowDate());
                    baBank.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(baBank);
                }
                //收款账户信息
                List<BaBank> baBankList = baEnterprise1.getBaBankList();
                if(!CollectionUtils.isEmpty(baBankList)){
                    baBankList.stream().forEach(item -> {
                        if(StringUtils.isNotEmpty(item.getId())){
                            item.setUpdateTime(DateUtils.getNowDate());
                            item.setUpdateBy(SecurityUtils.getUsername());
                            baBankMapper.updateBaBank(item);
                        }else {
                            item.setId(getRedisIncreID.getId());
                            item.setCreateTime(DateUtils.getNowDate());
                            item.setCreateBy(SecurityUtils.getUsername());
                            baBankMapper.insertBaBank(item);
                        }
                    });
                }
                baEnterprise1.setChangeBy(null);
                baEnterprise1.setChangeTime(null);
                baEnterpriseMapper.updateBaEnterprise(baEnterprise1);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //插入消息通知信息
            this.insertMessage(baEnterprise1, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.ENTERPRISE_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //插入消息通知信息
            this.insertMessage(baEnterprise, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baEnterprise, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baEnterpriseMapper.updateById(baEnterprise);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaEnterprise baEnterprise, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baEnterprise.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"终端企业审批提醒",msgContent,baMessage.getType(),baEnterprise.getId(),baEnterprise.getState());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaEnterprise checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaEnterprise baEnterprise = baEnterpriseMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baEnterprise == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_ENTERPRISE_001);
        }
//        if (!AdminCodeEnum.SUPPLIER_STATUS_VERIFYING.getCode().equals(baSupplier.getState())) {
//            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SUPPLIER_002);
//        }
        return baEnterprise;
    }
}
