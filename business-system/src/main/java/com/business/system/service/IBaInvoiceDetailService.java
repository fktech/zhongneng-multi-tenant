package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaInvoiceDetail;
import com.business.system.domain.dto.NumsAndMoneyDTO;

/**
 * 发票信息详情Service接口
 *
 * @author single
 * @date 2023-03-14
 */
public interface IBaInvoiceDetailService
{
    /**
     * 查询发票信息详情
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public BaInvoiceDetail selectBaInvoiceDetailById(String id);

    /**
     * 查询发票信息详情列表
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 发票信息详情集合
     */
    public List<BaInvoiceDetail> selectBaInvoiceDetailList(BaInvoiceDetail baInvoiceDetail);
    /**
     * 查询开票票id查询相应的发票数量
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public int selectBaInvoiceDetailByinviceId(String id);
    /**
     * 查询开票票id查询相应的发票数量本月
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    public NumsAndMoneyDTO selectDetailByinviceIdMonth(String id);


    /**
     * 新增发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    public int insertBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail);

    /**
     * 修改发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    public int updateBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail);

    /**
     * 批量删除发票信息详情
     *
     * @param ids 需要删除的发票信息详情ID
     * @return 结果
     */
    public int deleteBaInvoiceDetailByIds(String[] ids);

    /**
     * 删除发票信息详情信息
     *
     * @param id 发票信息详情ID
     * @return 结果
     */
    public int deleteBaInvoiceDetailById(String id);


}
