package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaOther;
import com.business.system.domain.OaSupplyClock;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import com.business.system.service.workflow.IOaSupplyClockApprovalService;
import org.springframework.stereotype.Service;


/**
 * 补卡申请审批结果:审批拒绝
 */
@Service("oaSupplyClockApprovalContent:processApproveResult:reject")
public class OaSupplyClockApprovalRejectServiceImpl extends IOaSupplyClockApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OASUPPLYCLOCK_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaSupplyClock checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
