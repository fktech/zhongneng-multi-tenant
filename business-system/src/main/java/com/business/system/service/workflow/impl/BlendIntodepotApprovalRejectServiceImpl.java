package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.BlendIntodepotApprovalService;
import com.business.system.service.workflow.WashIntodepotApprovalService;
import org.springframework.stereotype.Service;


/**
 * 配煤入库申请审批结果:审批拒绝
 */
@Service("blendIntodepotApprovalContent:processApproveResult:reject")
public class BlendIntodepotApprovalRejectServiceImpl extends BlendIntodepotApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.BLENDINTODEPOT_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaWashHead checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
