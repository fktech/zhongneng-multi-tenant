package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 洗煤配煤出入库记录Service业务层处理
 *
 * @author single
 * @date 2024-01-05
 */
@Service
public class BaWashHeadServiceImpl extends ServiceImpl<BaWashHeadMapper, BaWashHead> implements IBaWashHeadService
{
    @Autowired
    private BaWashHeadMapper baWashHeadMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaWashInventoryMapper baWashInventoryMapper;

    @Autowired
    private BaDepotMapper depotMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaUnitMapper baUnitMapper;

    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;

    private static final Logger log = LoggerFactory.getLogger(BaWashHeadServiceImpl.class);

    /**
     * 查询洗煤配煤出入库记录
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 洗煤配煤出入库记录
     */
    @Override
    public BaWashHead selectBaWashHeadById(String id)
    {
        BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(id);
        BaWashHead intoWashHead = new BaWashHead();
        intoWashHead.setFlag(new Long(0));
        intoWashHead.setState(AdminCodeEnum.WASHINTODEPOT_STATUS_PASS.getCode());
        intoWashHead.setLinkNumber(id);
        //租户
        intoWashHead.setTenantId(SecurityUtils.getCurrComId());
        //查询对应入库数据
        List<BaWashHead> baWashHeads = baWashHeadMapper.selectBaWashHeadList(intoWashHead);
        if(!CollectionUtils.isEmpty(baWashHeads)){
            baWashHead.setIntoBaWashHead(baWashHeads.get(0));
        }
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baWashHead.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baWashHead.setUser(sysUser);
        //仓库名称
        if(StringUtils.isNotEmpty(baWashHead.getDepotId())){
            BaDepot baDepot = depotMapper.selectBaDepotById(baWashHead.getDepotId());
            if(!ObjectUtils.isEmpty(baDepot)){
                baWashHead.setDepotName(baDepot.getName());
            }
        }

        //入库清单
        QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
        inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
        List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
        if(!CollectionUtils.isEmpty(baWashInventories)){
            for(BaWashInventory baWashInventory : baWashInventories){
                //仓库名称
                if (StringUtils.isNotEmpty(baWashInventory.getDepotId())) {
                    BaDepot baDepot = depotMapper.selectBaDepotById(baWashInventory.getDepotId());
                    if (!ObjectUtils.isEmpty(baDepot)) {
                        baWashInventory.setDepotName(baDepot.getName());
                    }
                }

                //库位名称
                if (StringUtils.isNotEmpty(baWashInventory.getLocationId())) {
                    BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(baWashInventory.getLocationId());
                    if (!ObjectUtils.isEmpty(baDepotLocation)) {
                        baWashInventory.setLocationName(baDepotLocation.getName());
                    }
                }

                //商品名称
                if (StringUtils.isNotEmpty(baWashInventory.getGoodsId())) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baWashInventory.getGoodsId());
                    if (StringUtils.isNotNull(baGoods)) {
                        baWashInventory.setGoodsName(baGoods.getName());
                    }
                }
                if (StringUtils.isNotEmpty(baWashInventory.getUnitId())) {
                    //单位名称
                    BaUnit baUnit = baUnitMapper.selectBaUnitById(baWashInventory.getUnitId());
                    if(!ObjectUtils.isEmpty(baUnit)){
                        baWashInventory.setUnitName(baUnit.getName());
                    }
                }
            }
            baWashHead.setBaWashIntoInventories(baWashInventories);
        }

        if(StringUtils.isNotEmpty(baWashHead.getLinkNumber())) {
            //出库库清单
            inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id", baWashHead.getLinkNumber());
            baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
            if (!CollectionUtils.isEmpty(baWashInventories)) {
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //仓库名称
                    if (StringUtils.isNotEmpty(baWashInventory.getDepotId())) {
                        BaDepot baDepot = depotMapper.selectBaDepotById(baWashInventory.getDepotId());
                        if (!ObjectUtils.isEmpty(baDepot)) {
                            baWashInventory.setDepotName(baDepot.getName());
                        }
                    }

                    //库位名称
                    if (StringUtils.isNotEmpty(baWashInventory.getLocationId())) {
                        BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(baWashInventory.getLocationId());
                        if (!ObjectUtils.isEmpty(baDepotLocation)) {
                            baWashInventory.setLocationName(baDepotLocation.getName());
                        }
                    }

                    //商品名称
                    if (StringUtils.isNotEmpty(baWashInventory.getGoodsId())) {
                        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baWashInventory.getGoodsId());
                        if (StringUtils.isNotNull(baGoods)) {
                            baWashInventory.setGoodsName(baGoods.getName());
                        }
                    }
                    if (StringUtils.isNotEmpty(baWashInventory.getUnitId())) {
                        //单位名称
                        BaUnit baUnit = baUnitMapper.selectBaUnitById(baWashInventory.getUnitId());
                        if (!ObjectUtils.isEmpty(baUnit)) {
                            baWashInventory.setUnitName(baUnit.getName());
                        }
                    }
                }
            }
        }
        baWashHead.setBaWashInventories(baWashInventories);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id", baWashHead.getId());
        queryWrapper1.eq("flag",0);
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baWashHead.setProcessInstanceId(processInstanceIds);
        return baWashHead;
    }

    /**
     * 查询洗煤配煤出入库记录列表
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 洗煤配煤出入库记录
     */
    @Override
    public List<BaWashHead> selectBaWashHeadList(BaWashHead baWashHead)
    {
        List<BaWashHead> baWashHeads = baWashHeadMapper.selectBaWashHeadList(baWashHead);
        if(!CollectionUtils.isEmpty(baWashHeads)){
            for(BaWashHead washHead : baWashHeads){
                //岗位信息
                String name = getName.getName(washHead.getUserId());
                //发起人
                if(name.equals("") == false){
                    washHead.setUserName(sysUserMapper.selectUserById(washHead.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    washHead.setUserName(sysUserMapper.selectUserById(washHead.getUserId()).getNickName());
                }
                //洗煤入库清单
                QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id", washHead.getId());
                List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
                washHead.setBaWashInventories(baWashInventories);

                BaDepot baDepot = depotMapper.selectBaDepotById(washHead.getDepotId());
                if(!ObjectUtils.isEmpty(baDepot)){
                    washHead.setDepotName(baDepot.getName());
                }
            }
        }
        return baWashHeads;
    }

    /**
     * 新增洗煤入库记录
     *
     * @param baWashHead 新增洗煤入库记录
     * @return 结果
     */
    @Override
    public int insertBaWashHead(BaWashHead baWashHead)
    {
        if(StringUtils.isNotEmpty(baWashHead.getLinkNumber())){
            BaWashHead baWashHead1 = baWashHeadMapper.selectBaWashHeadById(baWashHead.getLinkNumber());
            baWashHead1.setHeadStatus("2");
            baWashHeadMapper.updateBaWashHead(baWashHead1);
        }
        baWashHead.setHeadStatus("2");
        baWashHead.setId("RK"+getRedisIncreID.getId());
        baWashHead.setCreateTime(DateUtils.getNowDate());
        baWashHead.setCreateBy(SecurityUtils.getUsername());
        baWashHead.setUserId(SecurityUtils.getUserId());
        baWashHead.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        baWashHead.setType((long) 1);
        baWashHead.setTypes("1"); //洗煤中
        //判断是否为上海公司
        //判断业务归属公司是否是上海子公司
        if("2".equals(baWashHead.getWorkState())){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode(),SecurityUtils.getCurrComId());
            baWashHead.setFlowId(flowId);
        }
        //入库清单
        if(!CollectionUtils.isEmpty(baWashHead.getBaWashInventories())){
            for(BaWashInventory baWashInventory : baWashHead.getBaWashInventories()) {
                baWashInventory.setId(getRedisIncreID.getId());
                baWashInventory.setRelationId(baWashHead.getId());
                baWashInventory.setCreateBy(DateUtils.getDate());
                baWashInventoryMapper.insertBaWashInventory(baWashInventory);
            }
        }

        if(!"2".equals(baWashHead.getWorkState())){
            baWashHead.setWorkState("1");
            baWashHead.setTypes("2");
            //默认审批流程已通过
            baWashHead.setState(AdminCodeEnum.WASHINTODEPOT_STATUS_PASS.getCode());
        }

        Integer result = baWashHeadMapper.insertBaWashHead(baWashHead);

        if(!"2".equals(baWashHead.getWorkState())){
//            默认审批通过逻辑  start
            baWashHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
            //判断采购入库是否有入库清单
            QueryWrapper<BaWashInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id", baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(queryWrapper1);
            //更新入库时间
            baWashHead.setWashTime(DateUtils.getNowDate());
            BaMaterialStock materialStock = null;
            if(!CollectionUtils.isEmpty(baWashInventories)){
                for(BaWashInventory baWashInventory : baWashInventories){
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baWashInventory.getDepotId());
                    stockQueryWrapper.eq("location_id", baWashInventory.getLocationId());
                    stockQueryWrapper.eq("goods_id", baWashInventory.getGoodsId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    //判断是否已存在库存, 不存在则新增
                    if(ObjectUtils.isEmpty(materialStock)){
                        materialStock = new BaMaterialStock();
                        materialStock.setId(getRedisIncreID.getId());
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //库位ID
                        materialStock.setCreateTime(DateUtils.getNowDate());
                        materialStock.setCreateBy(SecurityUtils.getUsername());
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock = this.calcBaMaterialStock(materialStock, baWashHead, baWashInventory);
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        baMaterialStockMapper.insertBaMaterialStock(materialStock);
                    } else {
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //库位ID
                        materialStock.setUpdateBy(SecurityUtils.getUsername());
                        materialStock.setUpdateTime(DateUtils.getNowDate());
                        materialStock = this.calcBaMaterialStock(materialStock, baWashHead, baWashInventory);
                        baMaterialStockMapper.updateBaMaterialStock(materialStock);
                    }

                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
            }
            baMaterialStockMapper.updateBaMaterialStock(materialStock);
            baWashHead.setHeadStatus("2");
            baWashHeadMapper.updateById(baWashHead);
        }
        //            默认入库审批通过逻辑  end
        //判断是否为上海分公司
        if("2".equals(baWashHead.getWorkState())){
            if(result > 0){
                BaWashHead washHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
                //清除流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",washHead.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(washHead, AdminCodeEnum.WASHINTODEPOT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    return 0;
                }else {
                    //启动流程成功改变入库状态
                    washHead.setState(AdminCodeEnum.WASHINTODEPOT_STATUS_VERIFYING.getCode());
                    washHead.setHeadStatus("2");
                    washHead.setWashTime(DateUtils.getNowDate());
                    washHead.setTotals(baWashHead.getTotals());
                    baWashHeadMapper.updateBaWashHead(washHead);
                }
            } else {
                return -2;
            }
        }
        return 1;
    }

    /**
     * 入库质检提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaWashHead washHead, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(washHead.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(washHead.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode());
        //获取流程实例关联的业务对象
        BaWashHead baWashHead = this.selectBaWashHeadById(washHead.getId());
        SysUser sysUser = iSysUserService.selectUserById(baWashHead.getUserId());
        baWashHead.setUserName(sysUser.getUserName());
        baWashHead.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baWashHead, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 入库质检启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.WASHINTODEPOT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改洗煤配煤出入库记录
     *
     * @param baWashHead 洗煤配煤出入库记录
     * @return 结果
     */
    @Override
    public int updateBaWashHead(BaWashHead baWashHead)
    {
        //原数据
        BaWashHead baWashHead1 = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
        baWashHead.setUpdateBy(SecurityUtils.getUsername());
        baWashHead.setUpdateTime(DateUtils.getNowDate());
        //入库清单
        if(StringUtils.isNotNull(baWashHead.getBaWashInventories())){
            if(baWashHead.getBaWashInventories().size() > 0){
                //查询入库单是否存在
                QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
                List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //存在就删除
                    baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
                }
                for (BaWashInventory baWashInventory : baWashHead.getBaWashInventories()) {
                    baWashInventory.setId(getRedisIncreID.getId());
                    baWashInventory.setRelationId(baWashHead.getId());
                    baWashInventory.setCreateBy(DateUtils.getDate());
                    baWashInventoryMapper.insertBaWashInventory(baWashInventory);
                }
            } else {
                //查询入库单是否存在
                QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
                List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //编辑时入库单为空删除对应数据
                    baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
                }
            }
        }else {
            //查询入库单是否存在
            QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
            for (BaWashInventory baWashInventory : baWashInventories) {
                //编辑时入库单为空删除对应数据
                baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
            }
        }
        int result = baWashHeadMapper.updateBaWashHead(baWashHead);
        if(result > 0){
            BaWashHead washHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baWashHead.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baWashHead, AdminCodeEnum.WASHINTODEPOT_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baWashHeadMapper.updateBaWashHead(baWashHead1);
                return 0;
            }else {
                washHead.setState(AdminCodeEnum.WASHINTODEPOT_STATUS_VERIFYING.getCode());
                baWashHeadMapper.updateBaWashHead(washHead);
            }
        }
        return 1;
    }

    /**
     * 批量删除洗煤配煤出入库记录
     *
     * @param ids 需要删除的洗煤配煤出入库记录ID
     * @return 结果
     */
    @Override
    public int deleteBaWashHeadByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(id);
                baWashHead.setFlag((long) 1);
                baWashHeadMapper.updateBaWashHead(baWashHead);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除洗煤配煤出入库记录信息
     *
     * @param id 洗煤配煤出入库记录ID
     * @return 结果
     */
    @Override
    public int deleteBaWashHeadById(String id)
    {
        return baWashHeadMapper.deleteBaWashHeadById(id);
    }

    /**
     * 新增洗煤出库记录
     */
    @Override
    public int insertWashOutputdepot(BaWashHead baWashHead) {
        baWashHead.setId("CK"+getRedisIncreID.getId());
        baWashHead.setCreateTime(DateUtils.getNowDate());
        baWashHead.setCreateBy(SecurityUtils.getUsername());
        baWashHead.setUserId(SecurityUtils.getUserId());
        baWashHead.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        baWashHead.setType((long) 2);
        baWashHead.setHeadStatus("1");
        baWashHead.setTypes("1"); //洗煤中
        //出库清单
        if(StringUtils.isNotNull(baWashHead.getBaWashInventories())){
            if(baWashHead.getBaWashInventories().size() > 0){
                for (BaWashInventory baWashInventory : baWashHead.getBaWashInventories()) {
                    baWashInventory.setId(getRedisIncreID.getId());
                    baWashInventory.setRelationId(baWashHead.getId());
                    baWashInventory.setCreateBy(DateUtils.getDate());
                    baWashInventoryMapper.insertBaWashInventory(baWashInventory);
                }
            }
        }
        //判断是否为上海公司
        if(!"2".equals(baWashHead.getWorkState())){
            baWashHead.setTypes("2");
            baWashHead.setWorkState("1");
            //默认审批流程已通过
            baWashHead.setState(AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_PASS.getCode());
        }
        Integer result = baWashHeadMapper.insertBaWashHead(baWashHead);
        if(!"2".equals(baWashHead.getWorkState())) {
            //        出库审批通过 start
            baWashHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
            //判断销售出库是否有出库清单
            QueryWrapper<BaWashInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id", baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(queryWrapper1);

            //查询立项信息
            BaMaterialStock materialStock = new BaMaterialStock();
            if(!CollectionUtils.isEmpty(baWashInventories)) {
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baWashInventory.getDepotId());
                    stockQueryWrapper.eq("location_id", baWashInventory.getLocationId());
                    stockQueryWrapper.eq("goods_id", baWashInventory.getGoodsId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    if (ObjectUtils.isEmpty(materialStock)) {
                        materialStock = new BaMaterialStock();
                        materialStock.setId(getRedisIncreID.getId());
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setCreateTime(DateUtils.getNowDate());
                        materialStock.setCreateBy(SecurityUtils.getUsername());
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //库位ID
                        materialStock = this.calcBaMaterialStock1(materialStock, baWashHead, baWashInventory);
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        baMaterialStockMapper.insertBaMaterialStock(materialStock);
                    } else {
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //库位ID
                        materialStock.setUpdateBy(SecurityUtils.getUsername());
                        materialStock.setUpdateTime(DateUtils.getNowDate());
                        materialStock = this.calcBaMaterialStock1(materialStock, baWashHead, baWashInventory);
                        baMaterialStockMapper.updateBaMaterialStock(materialStock);
                    }
                }
            }
            baMaterialStockMapper.updateBaMaterialStock(materialStock);
            baWashHeadMapper.updateById(baWashHead);
        }
//        出库审批通过 end
        if("2".equals(baWashHead.getWorkState())){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode(),SecurityUtils.getCurrComId());
            baWashHead.setFlowId(flowId);
        }
        baWashHeadMapper.updateById(baWashHead);
        if("2".equals(baWashHead.getWorkState())){
            if(result > 0){
                BaWashHead washHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(washHead, AdminCodeEnum.WASHIOUTPUTDEPOT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    baWashHeadMapper.deleteBaWashHeadById(washHead.getId());
                    return 0;
                }else {
                    washHead.setState(AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_VERIFYING.getCode());
                    washHead.setHeadStatus("1");
                    baWashHeadMapper.updateBaWashHead(washHead);
                }
            }
        }
        return result;
    }

    /**
     * 修改洗煤出库记录
     */
    @Override
    public int updateWashOutputdepot(BaWashHead baWashHead) {
        //原数据
        BaWashHead baWashHead1 = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
        baWashHead.setUpdateBy(SecurityUtils.getUsername());
        baWashHead.setUpdateTime(DateUtils.getNowDate());
        //出库清单
        if(StringUtils.isNotNull(baWashHead.getBaWashInventories())){
            if(baWashHead.getBaWashInventories().size() > 0){
                //查询出库单是否存在
                QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
                List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //存在就删除
                    baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
                }
                for (BaWashInventory baWashInventory : baWashHead.getBaWashInventories()) {
                    baWashInventory.setId(getRedisIncreID.getId());
                    baWashInventory.setRelationId(baWashHead.getId());
                    baWashInventory.setCreateBy(DateUtils.getDate());
                    baWashInventoryMapper.insertBaWashInventory(baWashInventory);
                }
            } else {
                //查询入库单是否存在
                QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
                List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //编辑时入库单为空删除对应数据
                    baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
                }
            }
        }else {
            //查询入库单是否存在
            QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
            for (BaWashInventory baWashInventory : baWashInventories) {
                //编辑时入库单为空删除对应数据
                baWashInventoryMapper.deleteBaWashInventoryById(baWashInventory.getId());
            }
        }
        int result = baWashHeadMapper.updateBaWashHead(baWashHead);
        if(result > 0){
            BaWashHead washHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baWashHead.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(baWashHead, AdminCodeEnum.WASHIOUTPUTDEPOT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baWashHeadMapper.updateBaWashHead(baWashHead1);
                return 0;
            }else {
                washHead.setState(AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_VERIFYING.getCode());
                baWashHeadMapper.updateBaWashHead(washHead);
            }
        }
        return 1;
    }

    @Override
    public int intoRevoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询出入库信息
            BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baWashHead.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.WASHINTODEPOT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baWashHead.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //入库流程审批状态修改
                baWashHead.setState(AdminCodeEnum.WASHINTODEPOT_STATUS_WITHDRAW.getCode());
//                baWashHead.setHeadStatus("1");

                baWashHeadMapper.updateBaWashHead(baWashHead);
                String userId = String.valueOf(baWashHead.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baWashHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int outputRevoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询出入库信息
            BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baWashHead.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baWashHead.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //出库流程审批状态修改
                baWashHead.setState(AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_WITHDRAW.getCode());
//                baWashHead.setHeadStatus("1");

                baWashHeadMapper.updateBaWashHead(baWashHead);
                String userId = String.valueOf(baWashHead.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baWashHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public boolean getMaterialStockResult(String id) {
        boolean result = true;
        BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(id);
        if(!ObjectUtils.isEmpty(baWashHead)) {
            //入库清单
            QueryWrapper<BaWashInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id", baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(inventoryQueryWrapper);
            BaMaterialStock materialStock = new BaMaterialStock();

            if (!CollectionUtils.isEmpty(baWashInventories)) {
                for (BaWashInventory baWashInventory : baWashInventories) {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baWashInventory.getDepotId());
                    stockQueryWrapper.eq("location_id", baWashInventory.getLocationId());
                    stockQueryWrapper.eq("goods_id", baWashInventory.getGoodsId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    if (!ObjectUtils.isEmpty(materialStock)) {
                        if(materialStock.getInventoryTotal() == null){
                            materialStock.setInventoryTotal(new BigDecimal(0));
                        }
                        if(materialStock.getInventoryTotal().compareTo(baWashInventory.getWashTotal()) < 0){
                            result = false;
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<BaWashInventory> washHeadDetail(BaWashInventory baWashInventory) {
        return baWashInventoryMapper.washHeadDetail(baWashInventory);
    }

    private BaMaterialStock calcBaMaterialStock(BaMaterialStock materialStock, BaWashHead baWashHead, BaWashInventory baWashInventory){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baWashInventory.getWashTotal() != null && baWashInventory.getWashTotal().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().add(baWashInventory.getWashTotal()));
        }
        return materialStock;
    }

    /**
     * 洗煤出库申请审批通过计算更新库存数据
     * @param materialStock
     * @param baWashHead
     * @return
     */
    private BaMaterialStock calcBaMaterialStock1(BaMaterialStock materialStock, BaWashHead baWashHead, BaWashInventory baWashInventory){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baWashInventory.getWashTotal() != null && baWashInventory.getWashTotal().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().subtract(baWashInventory.getWashTotal()));
        }
        return materialStock;
    }

    /**
     * 出库提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances1(BaWashHead washHead, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(washHead.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(washHead.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode());
        //获取流程实例关联的业务对象
        BaWashHead baWashHead = baWashHeadMapper.selectBaWashHeadById(washHead.getId());
        SysUser sysUser = iSysUserService.selectUserById(baWashHead.getUserId());
        baWashHead.setUserName(sysUser.getUserName());
        baWashHead.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baWashHead));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances1(instancesRuntimeDTO, baWashHead);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 出库启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances1(ProcessInstancesRuntimeDTO processInstancesRuntime, BaWashHead baWashHead) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            //查询合同启动信息
//            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getStartId());
            SysUser sysUser = sysUserMapper.selectUserByUserName(baWashHead.getCreateBy());
            //设置入库业务发起人
            redisCache.lock(Constants.BUSINESS_MANAGER_ID, String.valueOf(sysUser.getUserId()), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                redisCache.unLock(Constants.BUSINESS_MANAGER_ID);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            redisCache.unLock(Constants.BUSINESS_MANAGER_ID);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.WASHIOUTPUTDEPOT_APPROVAL_CONTENT_UPDATE.getCode())){
                //添加流程实例关系表创建日期
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaWashHead baWashHead, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baWashHead.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        if(1 == baWashHead.getType()){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode());
        }else if(2 == baWashHead.getType()){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode());
        }
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            if(1 == baWashHead.getType()){
                baMessageService.messagePush(sysUser.getPhoneCode(),"洗煤入库审批提醒",msgContent,baMessage.getType(),baWashHead.getId(),baMessage.getBusinessType());
            }else if(2 == baWashHead.getType()){
                baMessageService.messagePush(sysUser.getPhoneCode(),"洗煤出库审批提醒",msgContent,baMessage.getType(),baWashHead.getId(),baMessage.getBusinessType());
            }
        }
        baMessageMapper.insertBaMessage(baMessage);
    }
}
