package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaBankMapper;
import com.business.system.domain.BaBank;
import com.business.system.service.IBaBankService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 开户行Service业务层处理
 *
 * @author ljb
 * @date 2022-12-05
 */
@Service
public class BaBankServiceImpl extends ServiceImpl<BaBankMapper, BaBank> implements IBaBankService
{
    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询开户行
     *
     * @param id 开户行ID
     * @return 开户行
     */
    @Override
    public BaBank selectBaBankById(String id)
    {
        return baBankMapper.selectBaBankById(id);
    }

    /**
     * 查询开户行列表
     *
     * @param baBank 开户行
     * @return 开户行
     */
    @Override
    public List<BaBank> selectBaBankList(BaBank baBank)
    {
        return baBankMapper.selectBaBankList(baBank);
    }

    /**
     * 新增开户行
     *
     * @param baBank 开户行
     * @return 结果
     */
    @Override
    public int insertBaBank(BaBank baBank)
    {
        //baBank.setId(getRedisIncreID.getId());
        baBank.setCreateTime(DateUtils.getNowDate());
        baBank.setCreateBy(SecurityUtils.getUsername());
        return baBankMapper.insertBaBank(baBank);
    }

    /**
     * 修改开户行
     *
     * @param baBank 开户行
     * @return 结果
     */
    @Override
    public int updateBaBank(BaBank baBank)
    {
        baBank.setUpdateTime(DateUtils.getNowDate());
        baBank.setUpdateBy(SecurityUtils.getUsername());
        return baBankMapper.updateBaBank(baBank);
    }

    /**
     * 批量删除开户行
     *
     * @param ids 需要删除的开户行ID
     * @return 结果
     */
    @Override
    public int deleteBaBankByIds(String[] ids)
    {
        return baBankMapper.deleteBaBankByIds(ids);
    }

    /**
     * 删除开户行信息
     *
     * @param id 开户行ID
     * @return 结果
     */
    @Override
    public int deleteBaBankById(String id)
    {
        return baBankMapper.deleteBaBankById(id);
    }

}
