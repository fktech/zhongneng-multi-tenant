package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaRailwayPlatform;
import com.business.system.mapper.BaRailwayPlatformMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaLoadingStationMapper;
import com.business.system.domain.BaLoadingStation;
import com.business.system.service.IBaLoadingStationService;

/**
 * 装货站Service业务层处理
 *
 * @author ljb
 * @date 2023-01-16
 */
@Service
public class BaLoadingStationServiceImpl extends ServiceImpl<BaLoadingStationMapper, BaLoadingStation> implements IBaLoadingStationService
{
    @Autowired
    private BaLoadingStationMapper baLoadingStationMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;

    /**
     * 查询装货站
     *
     * @param id 装货站ID
     * @return 装货站
     */
    @Override
    public BaLoadingStation selectBaLoadingStationById(String id)
    {
        BaLoadingStation baLoadingStation = baLoadingStationMapper.selectBaLoadingStationById(id);
        //铁路站台名称
        if(StringUtils.isNotEmpty(baLoadingStation.getPlatformId())){
        baLoadingStation.setPlatformName(baRailwayPlatformMapper.selectBaRailwayPlatformById(baLoadingStation.getPlatformId()).getName());
        }
        return baLoadingStation;
    }

    /**
     * 查询装货站列表
     *
     * @param baLoadingStation 装货站
     * @return 装货站
     */
    @Override
    public List<BaLoadingStation> selectBaLoadingStationList(BaLoadingStation baLoadingStation)
    {
        return baLoadingStationMapper.selectBaLoadingStationList(baLoadingStation);
    }

    /**
     * 新增装货站
     *
     * @param baLoadingStation 装货站
     * @return 结果
     */
    @Override
    public int insertBaLoadingStation(BaLoadingStation baLoadingStation)
    {
        baLoadingStation.setId(getRedisIncreID.getId());
        baLoadingStation.setCreateTime(DateUtils.getNowDate());
        baLoadingStation.setTenantId(SecurityUtils.getCurrComId());
        return baLoadingStationMapper.insertBaLoadingStation(baLoadingStation);
    }

    /**
     * 修改装货站
     *
     * @param baLoadingStation 装货站
     * @return 结果
     */
    @Override
    public int updateBaLoadingStation(BaLoadingStation baLoadingStation)
    {
        baLoadingStation.setUpdateTime(DateUtils.getNowDate());
        return baLoadingStationMapper.updateBaLoadingStation(baLoadingStation);
    }

    /**
     * 批量删除装货站
     *
     * @param ids 需要删除的装货站ID
     * @return 结果
     */
    @Override
    public int deleteBaLoadingStationByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaLoadingStation baLoadingStation = baLoadingStationMapper.selectBaLoadingStationById(id);
                baLoadingStation.setFlag(1);
                baLoadingStationMapper.updateBaLoadingStation(baLoadingStation);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除装货站信息
     *
     * @param id 装货站ID
     * @return 结果
     */
    @Override
    public int deleteBaLoadingStationById(String id)
    {
        return baLoadingStationMapper.deleteBaLoadingStationById(id);
    }

}
