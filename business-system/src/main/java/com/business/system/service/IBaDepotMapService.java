package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaDepotMap;

/**
 * 仓库库位信息Service接口
 *
 * @author single
 * @date 2023-09-06
 */
public interface IBaDepotMapService
{
    /**
     * 查询仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 仓库库位信息
     */
    public BaDepotMap selectBaDepotMapById(String id);

    /**
     * 查询仓库库位信息列表
     *
     * @param baDepotMap 仓库库位信息
     * @return 仓库库位信息集合
     */
    public List<BaDepotMap> selectBaDepotMapList(BaDepotMap baDepotMap);

    /**
     * 新增仓库库位信息
     *
     * @param baDepotMaps 仓库库位信息
     * @return 结果
     */
    public int insertBaDepotMap(List<BaDepotMap> baDepotMaps);

    /**
     * 修改仓库库位信息
     *
     * @param baDepotMap 仓库库位信息
     * @return 结果
     */
    public int updateBaDepotMap(BaDepotMap baDepotMap);

    /**
     * 批量删除仓库库位信息
     *
     * @param ids 需要删除的仓库库位信息ID
     * @return 结果
     */
    public int deleteBaDepotMapByIds(String[] ids);

    /**
     * 删除仓库库位信息信息
     *
     * @param id 仓库库位信息ID
     * @return 结果
     */
    public int deleteBaDepotMapById(String id);

    /**
     * 批量修改仓库库位信息
     */
    int batchUpdateBaDepotMap(List<BaDepotMap> baDepotMaps);
}
