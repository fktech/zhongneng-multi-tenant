package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaChecked;

/**
 * 盘点Service接口
 *
 * @author single
 * @date 2023-01-05
 */
public interface IBaCheckedService
{
    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    public BaChecked selectBaCheckedById(String id);

    /**
     * 查询盘点列表
     *
     * @param baChecked 盘点
     * @return 盘点集合
     */
    public List<BaChecked> selectBaCheckedList(BaChecked baChecked);

    /**
     * 新增盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    public int insertBaChecked(BaChecked baChecked);

    /**
     * 修改盘点
     *
     * @param baChecked 盘点
     * @return 结果
     */
    public int updateBaChecked(BaChecked baChecked);

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的盘点ID
     * @return 结果
     */
    public int deleteBaCheckedByIds(String[] ids);

    /**
     * 删除盘点信息
     *
     * @param id 盘点ID
     * @return 结果
     */
    public int deleteBaCheckedById(String id);

    /**
     * 获取盘点ID
     *
     * @return 盘点
     */
    public String getBaCheckedID();

    /**
     * 撤销按钮
     */
    Integer revoke(String id);
}
