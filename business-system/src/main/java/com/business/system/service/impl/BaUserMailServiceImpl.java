package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaUserMailMapper;
import com.business.system.domain.BaUserMail;
import com.business.system.service.IBaUserMailService;

/**
 * 通讯录Service业务层处理
 *
 * @author ljb
 * @date 2023-06-02
 */
@Service
public class BaUserMailServiceImpl extends ServiceImpl<BaUserMailMapper, BaUserMail> implements IBaUserMailService
{
    @Autowired
    private BaUserMailMapper baUserMailMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询通讯录
     *
     * @param id 通讯录ID
     * @return 通讯录
     */
    @Override
    public BaUserMail selectBaUserMailById(String id)
    {
        return baUserMailMapper.selectBaUserMailById(id);
    }

    /**
     * 查询通讯录列表
     *
     * @param baUserMail 通讯录
     * @return 通讯录
     */
    @Override
    @DataScope(deptAlias = "bum",userAlias = "bum")
    public List<BaUserMail> selectBaUserMailList(BaUserMail baUserMail)
    {
        return baUserMailMapper.selectBaUserMail(baUserMail);
    }

    @Override
    public List<BaUserMail> userMailList(BaUserMail baUserMail) {
        QueryWrapper<BaUserMail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("state","1");
        if(baUserMail.getDeptNum() != null){
            //查询本部门数据
            queryWrapper.eq("dept_num",baUserMail.getDeptNum());
        }
        List<BaUserMail> baUserMails = baUserMailMapper.selectList(queryWrapper);
        return baUserMails;
    }

    /**
     * 新增通讯录
     *
     * @param baUserMail 通讯录
     * @return 结果
     */
    @Override
    public int insertBaUserMail(BaUserMail baUserMail)
    {
        baUserMail.setId(getRedisIncreID.getId());
        baUserMail.setCreateTime(DateUtils.getNowDate());
        baUserMail.setDeptId(SecurityUtils.getDeptId());
        baUserMail.setUserId(SecurityUtils.getUserId());
        //租户ID
        baUserMail.setTenantId(SecurityUtils.getCurrComId());
        return baUserMailMapper.insertBaUserMail(baUserMail);
    }

    /**
     * 修改通讯录
     *
     * @param baUserMail 通讯录
     * @return 结果
     */
    @Override
    public int updateBaUserMail(BaUserMail baUserMail)
    {
        baUserMail.setUpdateTime(DateUtils.getNowDate());
        return baUserMailMapper.updateBaUserMail(baUserMail);
    }

    /**
     * 批量删除通讯录
     *
     * @param ids 需要删除的通讯录ID
     * @return 结果
     */
    @Override
    public int deleteBaUserMailByIds(String[] ids)
    {
        return baUserMailMapper.deleteBaUserMailByIds(ids);
    }

    /**
     * 删除通讯录信息
     *
     * @param id 通讯录ID
     * @return 结果
     */
    @Override
    public int deleteBaUserMailById(String id)
    {
        return baUserMailMapper.deleteBaUserMailById(id);
    }

    @Override
    public List<BaUserMail> userMail() {
        QueryWrapper<BaUserMail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("state","1");
        List<BaUserMail> baUserMails = baUserMailMapper.selectList(queryWrapper);
        return baUserMails;
    }

}
