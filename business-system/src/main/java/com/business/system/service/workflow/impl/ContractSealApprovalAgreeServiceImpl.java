package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaContractSeal;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractSealApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 企业注册申请审批结果:审批通过
 */
@Service("contractSealApprovalContent:processApproveResult:pass")
@Slf4j
public class ContractSealApprovalAgreeServiceImpl extends IContractSealApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaContractSeal contractSeal = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CONTRACTSEAL_STATUS_PASS.getCode());
        return result;
    }
}
