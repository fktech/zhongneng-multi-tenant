package com.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.DataLayoutStatDeptVO;
import com.business.system.domain.vo.DataLayoutStatVO;
import com.business.system.domain.vo.HomePageVo;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 移动端首页数据统计业务层处理
 */
@Service
public class HomePageServiceImpl implements IHomePageService {
    @Autowired
    private BaDeclareMapper baDeclareMapper;
    @Autowired
    private IBaProjectService baProjectService;
    @Autowired
    private IBaMarketInformationService baMarketInformationService;
    @Autowired
    private SysNoticeMapper noticeMapper;
    @Autowired
    private BaProjectMapper baProjectMapper;
    @Autowired
    private BaContractStartMapper baContractStartMapper;
    @Autowired
    private BaSettlementMapper baSettlementMapper;
    @Autowired
    private BaTransportMapper baTransportMapper;
    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaSealMapper baSealMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private BaCoalSourceStorageMapper baCoalSourceStorageMapper;

    @Autowired
    private BaCargoOwnerMapper baCargoOwnerMapper;

    @Override
    public UR screen(Long userId) {
        //查询自己的立项信息
        BaProject baProject = new BaProject();
        baProject.setUserId(userId);
        List<BaProject> baProjects = baProjectService.selectDraft(baProject);
        //查询本周是否有计划申报
        Integer integer = baDeclareMapper.thisWeek(userId);
        if(baProjects.size() > 0 && integer > 0){
            return UR.ok().data("code",3);
        }
        if(baProjects.size() > 0){
            return UR.ok().data("code",1);
        }
        if(integer > 0){
            return UR.ok().data("code",2);
        }
        return UR.ok().data("code",0);
    }

    @Override
    public UR lineChart(String type) {
        BaMarketInformation marketInformation = new BaMarketInformation();
        marketInformation.setType(type);
        List<BaMarketInformation> baMarketInformations = baMarketInformationService.baMarketInformationList(marketInformation);
        List<Map<Date,BigDecimal>> list = new ArrayList<>();
        Map<Date,BigDecimal> map = new HashMap<>();
        //涨跌
        //BigDecimal upAndDown = new BigDecimal(0);
        for (BaMarketInformation baMarketInformation:baMarketInformations) {
            List<BaRenew> baRenewList = baMarketInformation.getBaRenewList();
            //求价格平均值
            BigDecimal averageBigDecimal = new BigDecimal(0);
            if(type.equals("1") || type.equals("2") || type.equals("3")){
                averageBigDecimal = baRenewList.stream().map(BaRenew::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
                //upAndDown = baRenewList.stream().map(BaRenew ->new BigDecimal(BaRenew.getUpAndDown())).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
            }
            if(type.equals("4")){
                averageBigDecimal = baRenewList.stream().map(BaRenew::getBidWinningPrice).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
                //upAndDown = upAndDown;
            }
            if(type.equals("5")){
                averageBigDecimal = baRenewList.stream().map(BaRenew::getFreightUnit).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
                //upAndDown = baRenewList.stream().map(BaRenew ->new BigDecimal(BaRenew.getUpAndDown())).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
            }
            map.put(baMarketInformation.getRecentlyTime(),averageBigDecimal);
        }
        list.add(map);
        //获取最后一次更新数据
        /*BaMarketInformation marketInformation1 = baMarketInformations.get(0);
        if(StringUtils.isNotNull(marketInformation1.getBaRenewList())){
            List<BaRenew> baRenewList = marketInformation1.getBaRenewList();
            //求价格平均值
            BigDecimal averageBigDecimal = baRenewList.stream().map(BaRenew::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add).divide(BigDecimal.valueOf(baRenewList.size()), 2, RoundingMode.HALF_UP);
            map.put(marketInformation1.getRecentlyTime(),averageBigDecimal);
            list.add(map);
        }*/

        return UR.ok().data("list",list);
    }

    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice) {
        notice.setNoticeType("1");
        return noticeMapper.selectNoticeList(notice);
    }

    @Override
    public UR dataStatistics(Long deptId) {
        HashMap<String, String> map = new HashMap<>();
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq("state", "projectStatus:pass");
        if(deptId != null){
            projectQueryWrapper.eq("dept_id", deptId);
        }
        projectQueryWrapper.eq("flag", 0);
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        map.put("项目数", String.valueOf(baProjects.size()));
        //合同启动数量
        int num = 0;
        //累计贸易额
        BigDecimal tradeAmount = new BigDecimal(0);
        //累计运输量
        BigDecimal checkCoalNum = new BigDecimal(0);
        //合同启动个数
        QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
        startQueryWrapper.eq("state", "contractStart:pass");
        startQueryWrapper.eq("completion",0);
        if(deptId != null){
            startQueryWrapper.eq("dept_id", deptId);
        }
        startQueryWrapper.eq("flag", 0);
        List<BaContractStart> starts = baContractStartMapper.selectList(startQueryWrapper);
        for (BaContractStart baContractStart : starts) {
            //下游结算单金额
            QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
            settlementQueryWrapper.eq("contract_id", baContractStart.getId());
            settlementQueryWrapper.eq("state", "settlement:pass");
            settlementQueryWrapper.eq("flag", 0);
            settlementQueryWrapper.eq("settlement_type", 2);
            List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
            for (BaSettlement baSettlement : baSettlementList) {
                if(baSettlement.getSettlementAmount() != null){
                    tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                }
            }
            //累计运输量
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id", baContractStart.getId());
            transportQueryWrapper.eq("flag", 0);
            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
            for (BaTransport baTransport : baTransports) {
                //验收信息
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id", baTransport.getId());
                checkQueryWrapper.eq("type", 1);
                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                for (BaCheck baCheck : baChecks) {
                    checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                }
            }
        }
        num = starts.size() + num;
        map.put("累计运输量",String.valueOf(checkCoalNum));
        map.put("合同启动数量",String.valueOf(num));
        map.put("累计贸易额",String.valueOf(tradeAmount));
        return UR.ok().data("map",map);
    }

    @Override
    public List<BaDeclare> thisWeekDeclared() {

        return baDeclareMapper.thisWeekDeclared();
    }

    @Override
    public UR statContractAmount(String deptId) {
        //将元变为亿元
        BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
        HashMap<String, DataLayoutStatDeptVO> map = new HashMap<>();//查询合同启动
        //累计合同额
        CountContractStartDTO countContractStartDTO = new CountContractStartDTO();
        //租户ID
        countContractStartDTO.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal contractAmount = baContractStartMapper.sumContractAmountByDeptId(countContractStartDTO);
        //累计合同额
        DataLayoutStatDeptVO contractDataLayoutStatDeptVO = new DataLayoutStatDeptVO();
        //累计合同额
        if(contractAmount != null){
            contractAmount = contractAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }else {
            contractAmount = new BigDecimal(0);
        }
        contractDataLayoutStatDeptVO.setGroupAmount(contractAmount);

        //事业部累计合同额
        if(StringUtils.isNotEmpty(deptId)){
            countContractStartDTO.setManageDeptId(deptId);
            BigDecimal deptContractAmount = baContractStartMapper.sumContractAmountByDeptId(countContractStartDTO);
            if(deptContractAmount != null){
                deptContractAmount = deptContractAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
            }else {
                deptContractAmount = new BigDecimal(0);
            }
            contractDataLayoutStatDeptVO.setDeptAmount(deptContractAmount);
        } else {
            contractDataLayoutStatDeptVO.setDeptAmount(new BigDecimal(0));
        }
        return UR.ok().data("contractDataLayoutStatDeptVO",contractDataLayoutStatDeptVO);
    }

    @Override
    public UR statTradeAmount(String deptId) {
        //将元变为亿元
        BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
        //累计贸易额
        DataLayoutStatDeptVO tradeAmountDataLayoutStatDeptVO = new DataLayoutStatDeptVO();

        //累计贸易额
        BigDecimal tradeAmount = new BigDecimal(0);
        /*BaSettlement baSettlement = new BaSettlement();
        baSettlement.setFlag(0);
        baSettlement.setSettlementType(new Long(2)); //销售结算单
        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());*/
        tradeAmount = baClaimHistoryMapper.baClaimHistoryCountByDept(null,SecurityUtils.getCurrComId());

        //计算累计贸易额
        if(tradeAmount != null){
            tradeAmount = tradeAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }else {
            tradeAmount =  new BigDecimal(0);
        }

        tradeAmountDataLayoutStatDeptVO.setGroupAmount(tradeAmount);
        //计算事业部累计贸易额
        if(StringUtils.isNotEmpty(deptId)){
            BigDecimal deptTradeAmount = new BigDecimal(0);
            deptTradeAmount = baClaimHistoryMapper.baClaimHistoryCountByDept(deptId,SecurityUtils.getCurrComId());
            if(deptTradeAmount != null){
                deptTradeAmount = deptTradeAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
            }else {
                deptTradeAmount = new BigDecimal(0);
            }
            tradeAmountDataLayoutStatDeptVO.setDeptAmount(deptTradeAmount);
        } else {
            tradeAmountDataLayoutStatDeptVO.setDeptAmount(new BigDecimal(0));
        }
        return UR.ok().data("tradeAmountDataLayoutStatDeptVO", tradeAmountDataLayoutStatDeptVO);
    }

    @Override
    public UR statCheckCoalNum(String deptId) {
        //将吨变为万吨
        BigDecimal bigDecimal = new BigDecimal( "10000");
        //累计运输量
        DataLayoutStatDeptVO checkCoalNumDataLayoutStatDeptVO = new DataLayoutStatDeptVO();
        //累计运输量
        BigDecimal checkCoalNum = new BigDecimal(0);
        BaTransport baTransport = new BaTransport();
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        baTransport.setType("1");
        BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
        if (!ObjectUtils.isEmpty(baTransport1)) {
            if (baTransport1.getCheckCoalNum() != null) {
                checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
            }
            if (baTransport1.getCcjweight() != null) {
                checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
            }
            if (baTransport1.getConfirmedDeliveryWeight() != null) {
                checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
            }
            if(baTransport1.getLoadingWeight() != null){
                checkCoalNum = checkCoalNum.add(baTransport1.getLoadingWeight());
            }
        }
        if(checkCoalNum != null){
            //累计运输量
            checkCoalNum = checkCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }else {
            checkCoalNum = new BigDecimal(0);
        }
        checkCoalNumDataLayoutStatDeptVO.setGroupAmount(checkCoalNum);

        //事业部累计运输量
        if(StringUtils.isNotEmpty(deptId)) {
            baTransport.setManageDeptId(Long.valueOf(deptId));
            baTransport.setTenantId(SecurityUtils.getCurrComId());
            baTransport.setType("1");
            BaTransport baTransportDept = baTransportMapper.selectTransportSum(baTransport);
            BigDecimal deptCheckCoalNum = new BigDecimal(0);
            if (!ObjectUtils.isEmpty(baTransportDept)) {
                if (baTransportDept.getCheckCoalNum() != null) {
                    deptCheckCoalNum = deptCheckCoalNum.add(baTransportDept.getCheckCoalNum());
                }
                if (baTransportDept.getCcjweight() != null) {
                    deptCheckCoalNum = deptCheckCoalNum.add(baTransportDept.getCcjweight());
                }
                if (baTransportDept.getConfirmedDeliveryWeight() != null) {
                    deptCheckCoalNum = deptCheckCoalNum.add(baTransportDept.getConfirmedDeliveryWeight());
                }
                if(baTransportDept.getLoadingWeight() != null){
                    deptCheckCoalNum = deptCheckCoalNum.add(baTransportDept.getLoadingWeight());
                }
            }
            if(deptCheckCoalNum != null){
                deptCheckCoalNum = deptCheckCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
            }else {
                deptCheckCoalNum = new BigDecimal(0);
            }
            checkCoalNumDataLayoutStatDeptVO.setDeptAmount(deptCheckCoalNum);
        } else {
            checkCoalNumDataLayoutStatDeptVO.setDeptAmount(new BigDecimal(0));
        }
        return UR.ok().data("checkCoalNumDataLayoutStatDeptVO",checkCoalNumDataLayoutStatDeptVO);
    }

    @Override
    public UR profitAccumulate(String deptId) {
        //利润
        BigDecimal profit = new BigDecimal(0);
        //收款数据统计
        //将元变为亿元
        BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
        //累计收款
        DataLayoutStatDeptVO profitDataLayoutStatDeptVO = new DataLayoutStatDeptVO();
        //累计收款
        BigDecimal tradeAmount = new BigDecimal(0);
        tradeAmount = baClaimHistoryMapper.baClaimHistoryCountByDept(null,SecurityUtils.getCurrComId());
        if(tradeAmount == null){
            tradeAmount = new BigDecimal(0);
        }
        //付款数据统计
        BigDecimal paymentAmount = new BigDecimal(0);
        CountPaymentDTO countPaymentDTO = new CountPaymentDTO();
        //租户ID
        countPaymentDTO.setTenantId(SecurityUtils.getCurrComId());
        paymentAmount = baPaymentMapper.countPaymentList(countPaymentDTO);
        if(paymentAmount == null){
            paymentAmount = new BigDecimal(0);
        }
        //利润计算（收款总计-付款总计）
        profit = tradeAmount.subtract(paymentAmount);
        if(profit != null){
            profit = profit.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }else {
            profit = new BigDecimal(0);
        }
        profitDataLayoutStatDeptVO.setGroupAmount(profit);
        //事业部利润
        if(StringUtils.isNotEmpty(deptId)){
           //事业部利润
            BigDecimal deptProfit = new BigDecimal(0);
            //事业部累计收款
            BigDecimal deptTradeAmount = new BigDecimal(0);
            deptTradeAmount = baClaimHistoryMapper.baClaimHistoryCountByDept(deptId,SecurityUtils.getCurrComId());
            if(deptTradeAmount == null){
                deptTradeAmount = new BigDecimal(0);
            }
            //事业部付款数据统计
            BigDecimal deptPaymentAmount = new BigDecimal(0);
            countPaymentDTO.setManageDeptId(deptId);
            //租户ID
            countPaymentDTO.setTenantId(SecurityUtils.getCurrComId());
            deptPaymentAmount = baPaymentMapper.countPaymentList(countPaymentDTO);
            if(deptPaymentAmount == null){
                deptPaymentAmount = new BigDecimal(0);
            }
            //利润计算（收款总计-付款总计）
            deptProfit = deptTradeAmount.subtract(deptPaymentAmount);
            if(deptProfit != null){
                deptProfit = deptProfit.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
            }else {
                deptProfit = new BigDecimal(0);
            }
            profitDataLayoutStatDeptVO.setDeptAmount(deptProfit);
        }else {
            profitDataLayoutStatDeptVO.setDeptAmount(new BigDecimal(0));
        }
        return UR.ok().data("checkCoalNumDataLayoutStatDeptVO",profitDataLayoutStatDeptVO);
    }

//    @Override
//    public List<HomePageVo> dataLayoutSubCompany() {
//        //查询所有子公司
//        HashMap<String, DataLayoutStatVO> subCompanyMap = new HashMap<>();//查询合同启动
//        List<HomePageVo> homePageVos=new ArrayList<>();
//        List<BaContractStart> contractStarts = baContractStartMapper.selectSubCompanyStartList();
//        //累计合同额
//        BigDecimal contractAmount = new BigDecimal(0);
//        //累计贸易额
//        BigDecimal tradeAmount = new BigDecimal(0);
//        //累计运输量
//        BigDecimal checkCoalNum = new BigDecimal(0);
//        //将元变为万元
//        BigDecimal bigDecimal = new BigDecimal( "10000");
//        DataLayoutStatVO dataLayoutStatVO = null;
//        for (BaContractStart baContractStart : contractStarts) {
//            if(!ObjectUtils.isEmpty(baContractStart)){
//                if(!subCompanyMap.containsKey(baContractStart.getDeptName())){
//                    dataLayoutStatVO = new DataLayoutStatVO();
//                    contractAmount = new BigDecimal(0);
//                    tradeAmount = new BigDecimal(0);
//                    checkCoalNum = new BigDecimal(0);
//                } else {
//                    dataLayoutStatVO = (DataLayoutStatVO) subCompanyMap.get(baContractStart.getDeptName());
//                    tradeAmount = dataLayoutStatVO.getTradeAmount();
//                    contractAmount = dataLayoutStatVO.getContractAmount();
//                    checkCoalNum = dataLayoutStatVO.getCheckCoalNum();
//                }
//                //合同额（万元）
//                if (baContractStart.getPrice() != null && baContractStart.getTonnage() != null) {
//                    contractAmount = contractAmount.add(baContractStart.getPrice().multiply(baContractStart.getTonnage())
//                            .setScale(2, BigDecimal.ROUND_HALF_UP)).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
//                }
//                //下游结算单金额
//                BaSettlement baSettlement = new BaSettlement();
//                baSettlement.setFlag(0);
//                baSettlement.setSettlementType(new Long(2)); //销售结算单
//                baSettlement.setContractId(baContractStart.getId());
//                baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
//                BigDecimal tempTradeAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);
//                if(tempTradeAmount != null){
//                    tradeAmount = tradeAmount.add(tempTradeAmount);
//                }
//                //累计运输量
//                BaTransport baTransport = new BaTransport();
//                baTransport.setStartId(baContractStart.getId());
//                BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
//                if(!ObjectUtils.isEmpty(baTransport1)){
//                    if(baTransport1.getCheckCoalNum() != null){
//                        checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
//                    }
//                    if(baTransport1.getCcjweight() != null){
//                        checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
//                    }
//                    if(baTransport1.getConfirmedDeliveryWeight() != null){
//                        checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
//                    }
//                }
//                dataLayoutStatVO.setTradeAmount(tradeAmount);
//                dataLayoutStatVO.setContractAmount(contractAmount);
//                dataLayoutStatVO.setCheckCoalNum(checkCoalNum);
//                subCompanyMap.put(String.valueOf(baContractStart.getDeptName()), dataLayoutStatVO);
//
//            }
//        }
//
//        Set keySet=subCompanyMap.entrySet();//获取键的集合
//        Iterator it=keySet.iterator();
//        while(it.hasNext()){
//            Map.Entry entry=(Map.Entry)(it.next());
//            HomePageVo homePageVo=new HomePageVo();
//
//            homePageVo.setName(entry.getKey().toString());
//            homePageVo.setCheckCoalNum(subCompanyMap.get(entry.getKey()).getCheckCoalNum());
//            homePageVo.setContractAmount(subCompanyMap.get(entry.getKey()).getContractAmount());
//            homePageVo.setTradeAmount(subCompanyMap.get(entry.getKey()).getTradeAmount());
//            homePageVos.add(homePageVo);
//        }
//        return homePageVos;
//    }

    @Override
    public List<DataLayoutStatVO> dataLayoutSubCompany(String type) {
        //查询所有子公司
        List<DataLayoutStatVO> subDepartmentList = new ArrayList<>();//查询统计
        List<SysDept> sysDepts = null;
        SysDept tempSysDept = null;
        //租户ID
        String tenantId = SecurityUtils.getCurrComId();
        sysDepts = sysDeptMapper.selectSubCompanyList(tenantId);
        //累计合同额
        BigDecimal contractAmount = null;
        //累计贸易额
        BigDecimal tradeAmount = null;
        //累计运输量
        BigDecimal checkCoalNum = null;
        //累计利润
        BigDecimal profitAccumulate = null;
        //累计付款金额
        BigDecimal paymentAmount = null;
        //将元变为万元
        BigDecimal bigDecimal = new BigDecimal("10000");
        //将元变为亿元
        BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
        DataLayoutStatVO dataLayoutStatVO = null;
        if (!CollectionUtils.isEmpty(sysDepts)) {
            for (SysDept sysDept : sysDepts) {
                if (!ObjectUtils.isEmpty(sysDept)) {
                    contractAmount = new BigDecimal(0);
                    tradeAmount = new BigDecimal(0);
                    checkCoalNum = new BigDecimal(0);
                    profitAccumulate = new BigDecimal(0);
                    paymentAmount = new BigDecimal(0);
                    //累计合同额
                    CountContractStartDTO countContractStartDTO = new CountContractStartDTO();
                    countContractStartDTO.setBelongCompanyId(String.valueOf(sysDept.getDeptId()));
                    contractAmount = baContractStartMapper.sumContractAmountByDeptId(countContractStartDTO);

                    //认领收款总计
                    /*BaSettlement baSettlement = new BaSettlement();
                    baSettlement.setFlag(0);
                    baSettlement.setSettlementType(new Long(2)); //销售结算单
                    baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
                    baSettlement.setBelongCompanyId(sysDept.getDeptId());*/
                    tradeAmount = baClaimHistoryMapper.baClaimHistoryCountBySubCompany(String.valueOf(sysDept.getDeptId()));
                    if(tradeAmount == null){
                        tradeAmount = new BigDecimal(0);
                    }

                    BaTransport baTransport = new BaTransport();
                    baTransport.setBelongCompanyId(sysDept.getDeptId());
                    baTransport.setTenantId(SecurityUtils.getCurrComId());
                    baTransport.setType("1");
                    BaTransport baTransportDept = baTransportMapper.selectTransportSum(baTransport);
                    //子公司累计运输量
                    if (!ObjectUtils.isEmpty(baTransportDept)) {
                        if (baTransportDept.getCheckCoalNum() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getCheckCoalNum());
                        }
                        if (baTransportDept.getCcjweight() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getCcjweight());
                        }
                        if (baTransportDept.getConfirmedDeliveryWeight() != null) {
                            checkCoalNum = checkCoalNum.add(baTransportDept.getConfirmedDeliveryWeight());
                        }
                        if(baTransportDept.getLoadingWeight() != null){
                            checkCoalNum = checkCoalNum.add(baTransportDept.getLoadingWeight());
                        }
                    }
                    //子公司付款总计
                    CountPaymentDTO countPaymentDTO = new CountPaymentDTO();
                    countPaymentDTO.setBelongCompanyId(String.valueOf(sysDept.getDeptId()));
                    paymentAmount = baPaymentMapper.countPaymentList(countPaymentDTO);
                    if(paymentAmount == null){
                        paymentAmount = new BigDecimal(0);
                    }
                    profitAccumulate = tradeAmount.subtract(paymentAmount);
                    dataLayoutStatVO = new DataLayoutStatVO();
                    if(tradeAmount != null) {
                        tradeAmount = tradeAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                    } else {
                        tradeAmount = new BigDecimal(0);
                    }
                    dataLayoutStatVO.setTradeAmount(tradeAmount);
                    if(contractAmount != null) {
                        contractAmount = contractAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                    } else {
                        contractAmount = new BigDecimal(0);
                    }
                    dataLayoutStatVO.setContractAmount(contractAmount);
                    if(checkCoalNum != null) {
                        checkCoalNum = checkCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                    } else {
                        checkCoalNum = new BigDecimal(0);
                    }
                    dataLayoutStatVO.setCheckCoalNum(checkCoalNum);
                    if(profitAccumulate != null){
                        profitAccumulate = profitAccumulate.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                    }else {
                        profitAccumulate = new BigDecimal(0);
                    }
                    dataLayoutStatVO.setProfitAccumulate(profitAccumulate);
                    dataLayoutStatVO.setDeptId(String.valueOf(sysDept.getDeptId()));
                    dataLayoutStatVO.setDeptName(sysDept.getDeptName());
                    subDepartmentList.add(dataLayoutStatVO);
                }
            }
        }

        //累计合同额
        if("contractAmount".equals(type)){
            subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getContractAmount).reversed()).collect(Collectors.toList());
        } else if("tradeAmount".equals(type)){ //累计贸易额
            subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getTradeAmount).reversed()).collect(Collectors.toList());
        } else if("checkCoalNum".equals(type)){ //累计运输量
            subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getCheckCoalNum).reversed()).collect(Collectors.toList());
        }else if("profitAccumulate".equals(type)){ //利润合计
            subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getProfitAccumulate).reversed()).collect(Collectors.toList());
        }
        return subDepartmentList;
    }

    @Override
    public List<DataLayoutStatVO> dataLayoutDepartment(Long deptId, String type) {
        //查询所有事业部
        List<DataLayoutStatVO> subDepartmentList = new ArrayList<>();//查询统计
        //判断是否在集团下
        if(deptId != null && deptId == 100){
            List<SysDept> sysDepts = null;
            SysDept tempSysDept = null;
            Long parentId = new Long(0);
            tempSysDept = new SysDept();
            tempSysDept.setParentId(deptId);
            //tempSysDept.setDeptName("事业部");
            //租户ID
            tempSysDept.setTenantId(SecurityUtils.getCurrComId());
            sysDepts = sysDeptMapper.selectDepartmentListByParentId(tempSysDept);
            //累计合同额
            BigDecimal contractAmount = null;
            //累计贸易额
            BigDecimal tradeAmount = null;
            //累计运输量
            BigDecimal checkCoalNum = null;
            //累计利润
            BigDecimal profitAccumulate = null;
            //累计付款金额
            BigDecimal paymentAmount = null;
            //将元变为万元
            BigDecimal bigDecimal = new BigDecimal("10000");
            //将元变为亿元
            BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
            DataLayoutStatVO dataLayoutStatVO = null;
            if (!CollectionUtils.isEmpty(sysDepts)) {
                for (SysDept sysDept : sysDepts) {
                    if (!ObjectUtils.isEmpty(sysDept)) {
                        contractAmount = new BigDecimal(0);
                        tradeAmount = new BigDecimal(0);
                        checkCoalNum = new BigDecimal(0);
                        profitAccumulate = new BigDecimal(0);
                        paymentAmount = new BigDecimal(0);
                        //累计合同额
                        CountContractStartDTO countContractStartDTO = new CountContractStartDTO();
                        countContractStartDTO.setManageDeptId(String.valueOf(sysDept.getDeptId()));
                        contractAmount = baContractStartMapper.sumContractAmountByDeptId(countContractStartDTO);

                        //下游结算单金额
                        /*BaSettlement baSettlement = new BaSettlement();
                        baSettlement.setFlag(0);
                        baSettlement.setSettlementType(new Long(2)); //销售结算单
                        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
                        baSettlement.setDeptId(sysDept.getDeptId());*/
                        tradeAmount = baClaimHistoryMapper.baClaimHistoryCountByDept(String.valueOf(sysDept.getDeptId()),SecurityUtils.getCurrComId());
                        if (tradeAmount == null){
                            tradeAmount = new BigDecimal(0);
                        }
                        BaTransport baTransport = new BaTransport();
                        baTransport.setManageDeptId(sysDept.getDeptId());
                        baTransport.setTenantId(SecurityUtils.getCurrComId());
                        baTransport.setType("1");
                        BaTransport baTransportDept = baTransportMapper.selectTransportSum(baTransport);
                        //事业部累计运输量
                        if (!ObjectUtils.isEmpty(baTransportDept)) {
                            if (baTransportDept.getCheckCoalNum() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getCheckCoalNum());
                            }
                            if (baTransportDept.getCcjweight() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getCcjweight());
                            }
                            if (baTransportDept.getConfirmedDeliveryWeight() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getConfirmedDeliveryWeight());
                            }
                            if(baTransportDept.getLoadingWeight() != null){
                                checkCoalNum = checkCoalNum.add(baTransportDept.getLoadingWeight());
                            }
                        }
                        //事业部付款统计
                        CountPaymentDTO countPaymentDTO = new CountPaymentDTO();
                        countPaymentDTO.setManageDeptId(String.valueOf(sysDept.getDeptId()));
                        paymentAmount = baPaymentMapper.countPaymentList(countPaymentDTO);
                        if(paymentAmount == null){
                            paymentAmount = new BigDecimal(0);
                        }
                        profitAccumulate = tradeAmount.subtract(paymentAmount);
                        dataLayoutStatVO = new DataLayoutStatVO();
                        if(tradeAmount != null) {
                            tradeAmount = tradeAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            tradeAmount = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setTradeAmount(tradeAmount);
                        if(contractAmount != null) {
                            contractAmount = contractAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            contractAmount = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setContractAmount(contractAmount);
                        if(checkCoalNum != null) {
                            checkCoalNum = checkCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            checkCoalNum = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setCheckCoalNum(checkCoalNum);
                        if(profitAccumulate != null){
                            profitAccumulate = profitAccumulate.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        }else {
                            profitAccumulate = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setProfitAccumulate(profitAccumulate);
                        dataLayoutStatVO.setDeptId(String.valueOf(sysDept.getDeptId()));
                        dataLayoutStatVO.setDeptName(sysDept.getDeptName());
                        subDepartmentList.add(dataLayoutStatVO);
                    }
                }
            }

            //根据类型排序
            //累计合同额
            if("contractAmount".equals(type)){
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getContractAmount).reversed()).collect(Collectors.toList());;
            } else if("tradeAmount".equals(type)){ //累计贸易额
                subDepartmentList =  subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getTradeAmount).reversed()).collect(Collectors.toList());;
            } else if("checkCoalNum".equals(type)){ //累计运输量
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getCheckCoalNum).reversed()).collect(Collectors.toList());;
            }else if("profitAccumulate".equals(type)){
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getProfitAccumulate).reversed()).collect(Collectors.toList());;
            }
            return subDepartmentList;
        }else if(deptId != null){
            List<SysDept> sysDepts = null;
            SysDept tempSysDept = null;
            Long parentId = new Long(0);
            tempSysDept = new SysDept();
            tempSysDept.setParentId(deptId);
            tempSysDept.setDeptName("事业部");
            //租户ID
            tempSysDept.setTenantId(SecurityUtils.getCurrComId());
            sysDepts = sysDeptMapper.selectDepartmentListByParentId(tempSysDept);
            //累计合同额
            BigDecimal contractAmount = null;
            //累计贸易额
            BigDecimal tradeAmount = null;
            //累计运输量
            BigDecimal checkCoalNum = null;
            //累计利润
            BigDecimal profitAccumulate = null;
            //累计付款金额
            BigDecimal paymentAmount = null;
            //将元变为万元
            BigDecimal bigDecimal = new BigDecimal("10000");
            //将元变为亿元
            BigDecimal bigDecimalBillion = new BigDecimal( "100000000");
            DataLayoutStatVO dataLayoutStatVO = null;
            if (!CollectionUtils.isEmpty(sysDepts)) {
                for (SysDept sysDept : sysDepts) {
                    if (!ObjectUtils.isEmpty(sysDept)) {
                        contractAmount = new BigDecimal(0);
                        tradeAmount = new BigDecimal(0);
                        checkCoalNum = new BigDecimal(0);
                        profitAccumulate = new BigDecimal(0);
                        paymentAmount = new BigDecimal(0);
                        //累计合同额
                        CountContractStartDTO countContractStartDTO = new CountContractStartDTO();
                        countContractStartDTO.setPartDeptId(String.valueOf(sysDept.getDeptId()));
                        contractAmount = baContractStartMapper.sumContractAmountByDeptId(countContractStartDTO);

                        //下游结算单金额
                        /*BaSettlement baSettlement = new BaSettlement();
                        baSettlement.setFlag(0);
                        baSettlement.setSettlementType(new Long(2)); //销售结算单
                        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
                        baSettlement.setDeptId(sysDept.getDeptId());*/
                        tradeAmount = baClaimHistoryMapper.baClaimHistoryCountByPartDept(String.valueOf(sysDept.getDeptId()));
                        if(tradeAmount == null){
                            tradeAmount = new BigDecimal(0);
                        }
                        BaTransport baTransport = new BaTransport();
                        baTransport.setPartDeptId(sysDept.getDeptId());
                        baTransport.setTenantId(SecurityUtils.getCurrComId());
                        baTransport.setType("1");
                        BaTransport baTransportDept = baTransportMapper.selectTransportSum(baTransport);
                        //事业部累计运输量
                        if (!ObjectUtils.isEmpty(baTransportDept)) {
                            if (baTransportDept.getCheckCoalNum() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getCheckCoalNum());
                            }
                            if (baTransportDept.getCcjweight() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getCcjweight());
                            }
                            if (baTransportDept.getConfirmedDeliveryWeight() != null) {
                                checkCoalNum = checkCoalNum.add(baTransportDept.getConfirmedDeliveryWeight());
                            }
                            if(baTransportDept.getLoadingWeight() != null){
                                checkCoalNum = checkCoalNum.add(baTransportDept.getLoadingWeight());
                            }
                        }
                        //事业部付款统计
                        CountPaymentDTO countPaymentDTO = new CountPaymentDTO();
                        countPaymentDTO.setPartDeptId(String.valueOf(sysDept.getDeptId()));
                        paymentAmount = baPaymentMapper.countPaymentList(countPaymentDTO);
                        if(paymentAmount == null){
                            paymentAmount = new BigDecimal(0);
                        }
                        profitAccumulate = tradeAmount.subtract(paymentAmount);
                        dataLayoutStatVO = new DataLayoutStatVO();
                        if(tradeAmount != null) {
                            tradeAmount = tradeAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            tradeAmount = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setTradeAmount(tradeAmount);
                        if(contractAmount != null) {
                            contractAmount = contractAmount.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            contractAmount = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setContractAmount(contractAmount);
                        if(contractAmount != null) {
                            checkCoalNum = checkCoalNum.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        } else {
                            checkCoalNum = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setCheckCoalNum(checkCoalNum);
                        if(profitAccumulate != null){
                            profitAccumulate = profitAccumulate.setScale(2, BigDecimal.ROUND_HALF_UP).divide(bigDecimalBillion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
                        }else {
                            profitAccumulate = new BigDecimal(0);
                        }
                        dataLayoutStatVO.setProfitAccumulate(profitAccumulate);
                        dataLayoutStatVO.setDeptId(String.valueOf(sysDept.getDeptId()));
                        dataLayoutStatVO.setDeptName(sysDept.getDeptName());
                        subDepartmentList.add(dataLayoutStatVO);
                    }
                }
            }

            //根据类型排序
            //累计合同额
            if("contractAmount".equals(type)){
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getContractAmount).reversed()).collect(Collectors.toList());;
            } else if("tradeAmount".equals(type)){ //累计贸易额
                subDepartmentList =  subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getTradeAmount).reversed()).collect(Collectors.toList());;
            } else if("checkCoalNum".equals(type)){ //累计运输量
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getCheckCoalNum).reversed()).collect(Collectors.toList());;
            }else if("profitAccumulate".equals(type)){
                subDepartmentList = subDepartmentList.stream().sorted(Comparator.comparing(DataLayoutStatVO::getProfitAccumulate).reversed()).collect(Collectors.toList());;
            }
            return subDepartmentList;
        }
        return subDepartmentList;
    }

    @Override
    public Map selectSalesVolume() {
        //将元变为亿元
        BigDecimal billion = new BigDecimal("100000000");
        //将吨改成万吨
        BigDecimal tenThousand = new BigDecimal("10000");
        Map map=new HashMap();
        //已通过或审批中立项总数
        QueryWrapper<BaProject> baProjectQueryWrapper = new QueryWrapper<>();
        //判断当前登录人是一类还是二类
        if(StringUtils.isNotEmpty(SecurityUtils.getSysUser().getHumanoid())){
            if(SecurityUtils.getSysUser().getHumanoid().equals("1")){
                baProjectQueryWrapper.eq("flag",0).and(baProjectQueryWrapper1 ->baProjectQueryWrapper1.eq("state","projectStatus:pass").or().eq("state","projectStatus:verifying")).eq("tenant_id",SecurityUtils.getCurrComId());
            }else {
                baProjectQueryWrapper.eq("flag",0).and(baProjectQueryWrapper1 ->baProjectQueryWrapper1.eq("state","projectStatus:pass").or().eq("state","projectStatus:verifying")).eq("dept_id", SecurityUtils.getDeptId()).eq("tenant_id",SecurityUtils.getCurrComId());
            }
        }
        List<BaProject> baProjects = baProjectMapper.selectList(baProjectQueryWrapper);
        map.put("projectNums",baProjects.size());
        //审批通过的合同启动
        TerminalDTO terminalDTO = new TerminalDTO();
        if(!SecurityUtils.getSysUser().getHumanoid().equals("1")){
            terminalDTO.setDeptId(String.valueOf(SecurityUtils.getDeptId()));
        }
        //租户ID
        terminalDTO.setTenantId(SecurityUtils.getCurrComId());
        List<TerminalDTO> terminalDTOS = baContractStartMapper.terminalStatistics(terminalDTO);
        map.put("contractStartNUms",terminalDTOS.size());
        //进行中的项目
        CountBaProjectDTO countBaProjectDTO = new CountBaProjectDTO();
        //租户ID
        countBaProjectDTO.setTenantId(SecurityUtils.getCurrComId());
        int ongoingNums = baProjectMapper.countBaProjectList(countBaProjectDTO);
        map.put("ongoingNums",ongoingNums);
        //本月计划申报
        BaDeclare baDeclare=new BaDeclare();
        baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode());
        //租户ID
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        Integer integer = baDeclareMapper.thisMonth1(baDeclare);
        map.put("declareNums",integer);
        //供应商
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("flag",0);
        supplierQueryWrapper.eq("state",AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer supplierCount = baSupplierMapper.selectCount(supplierQueryWrapper);
        //终端企业
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("flag",0);
        enterpriseQueryWrapper.eq("state",AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        //租户ID
        enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer enterpriseCount = baEnterpriseMapper.selectCount(enterpriseQueryWrapper);
        //集团公司
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("type","1");
        //租户ID
        groupQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer companyGroupCount = baCompanyGroupMapper.selectCount(groupQueryWrapper);
        //所有公司
        QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
        companyQueryWrapper.eq("flag",0);
        companyQueryWrapper.ne("company_type","铁路公司");
        companyQueryWrapper.eq("state","company:pass");
        //租户ID
        companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer companyCount = baCompanyMapper.selectCount(companyQueryWrapper);
        //货主
        QueryWrapper<BaCargoOwner> ownerQueryWrapper = new QueryWrapper<>();
        ownerQueryWrapper.eq("flag",0);
        //租户ID
        ownerQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer ownerCount = baCargoOwnerMapper.selectCount(ownerQueryWrapper);
        //客商总数
        map.put("investorNums",supplierCount+enterpriseCount+companyGroupCount+companyCount+ownerCount);
        //发票信息
        BaInvoice baInvoice = new BaInvoice();
        //销项发票
        baInvoice.setProjectType("4");
        baInvoice.setInvoiceType("1");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal = baInvoiceMapper.baInvoiceTotal(baInvoice);
        if(bigDecimal == null){
            bigDecimal = new BigDecimal(0);
        }else {
            bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).divide(billion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }
        map.put("invoicedQuantityPic",bigDecimal);
        //进项发票
        baInvoice = new BaInvoice();
        baInvoice.setProjectType("4");
        baInvoice.setInvoiceType("2");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal1 = baInvoiceMapper.baInvoiceTotal(baInvoice);
        if(bigDecimal1 == null){
            bigDecimal1 = new BigDecimal(0);
        }else {
            bigDecimal1 = bigDecimal1.setScale(2, BigDecimal.ROUND_HALF_UP).divide(billion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }
        map.put("inVoIcePic",bigDecimal1);
        //发运信息
        CountTransportDTO countTransportDTO = new CountTransportDTO();
        countTransportDTO.setProjectType("4");
        //租户ID
        countTransportDTO.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal2 = baTransportMapper.countTransportList(countTransportDTO);
        if(bigDecimal2 == null){
            bigDecimal2 = new BigDecimal(0);
        }else {
            bigDecimal2 = bigDecimal2.setScale(2, BigDecimal.ROUND_HALF_UP).divide(tenThousand, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }
         map.put("checkCoalNum",bigDecimal2);
        //付款信息
        CountPaymentDTO countPaymentDTO = new CountPaymentDTO();
        //租户ID
        countPaymentDTO.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal3 = baPaymentMapper.countPaymentList(countPaymentDTO);
        if(bigDecimal3 == null){
            bigDecimal3 = new BigDecimal(0);
        }else {
            bigDecimal3 = bigDecimal3.setScale(2, BigDecimal.ROUND_HALF_UP).divide(billion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }
        map.put("actualPaymentAmount",bigDecimal3);
        //已签合同信息
        QueryWrapper<BaContract> contractQueryWrapper = new QueryWrapper<>();
        contractQueryWrapper.eq("state",AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        contractQueryWrapper.eq("flag",0);
        contractQueryWrapper.eq("signing_status","已签");
        //contractQueryWrapper.eq("contract_state","进行中");
        //租户ID
        contractQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer integer1 = baContractMapper.selectCount(contractQueryWrapper);
        map.put("baContracts",integer1);
        //收款信息
        BigDecimal bigDecimal4 = baClaimHistoryMapper.baClaimHistoryCount(null,SecurityUtils.getCurrComId());
        if(bigDecimal4 == null){
            bigDecimal4 = new BigDecimal(0);
        }else {
            bigDecimal4 = bigDecimal4.setScale(2, BigDecimal.ROUND_HALF_UP).divide(billion, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
        }
        map.put("applyCollectionAmount",bigDecimal4);
        //订单数
        QueryWrapper<BaContractStart> contractStartQueryWrapper = new QueryWrapper<>();
        contractStartQueryWrapper.eq("flag",0);
        contractStartQueryWrapper.eq("state",AdminCodeEnum.ORDER_STATUS_PASS.getCode());
        //租户ID
        contractStartQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer integer2 = baContractStartMapper.selectCount(contractStartQueryWrapper);
        map.put("orderNUms",integer2);
        //用印数
        QueryWrapper<BaSeal> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("state",AdminCodeEnum.SEAL_STATUS_PASS.getCode());
        //租户ID
        sealQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer integer3 = baSealMapper.selectCount(sealQueryWrapper);
        map.put("baSeals",integer3);
        //结算单数量
        QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
        settlementQueryWrapper.eq("state",AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
        settlementQueryWrapper.eq("flag",0);
        //租户ID
        settlementQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        Integer integer4 = baSettlementMapper.selectCount(settlementQueryWrapper);
        map.put("baSettlements",integer4);
        //仓库数量
        BaDepot baDepot = new BaDepot();
        baDepot.setState(AdminCodeEnum.DEPOT_STATUS_PASS.getCode());
        //租户ID
        baDepot.setTenantId(SecurityUtils.getCurrComId());
        List<BaDepot> baDepots = baDepotMapper.selectBaDepotList(baDepot);
        map.put("stock",baDepots.size());
        //库存总量
        BaMaterialStock materialStock = new BaMaterialStock();
        //租户ID
        materialStock.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal sumTotalByCon = baMaterialStockMapper.sumTotalByCon(materialStock);
        map.put("sumTotalByCon",sumTotalByCon);
        //商品数量
        BaGoods baGoods = new BaGoods();
        baGoods.setFlag(0);
        //租户ID
        baGoods.setTenantId(SecurityUtils.getCurrComId());
        List<BaGoods> baGoodsList = baGoodsMapper.selectBaGoodsList(baGoods);
        map.put("goods",baGoodsList.size());
        //煤源库
        BaCoalSourceStorage baCoalSourceStorage = new BaCoalSourceStorage();
        //租户ID
        baCoalSourceStorage.setTenantId(SecurityUtils.getCurrComId());
        List<BaCoalSourceStorage> baCoalSourceStorageList = baCoalSourceStorageMapper.selectBaCoalSourceStorageList(baCoalSourceStorage);
        map.put("storage",baCoalSourceStorageList.size());
        return map;
    }

//    @Override
//    public List<DataLayoutStatVO> dataLayoutDepartment(Long deptId) {
//        //查询所有子公司
//        List<DataLayoutStatVO> subDepartmentList = new ArrayList<>();//查询统计
//        Map<String, DataLayoutStatVO> subDepartmentMap = new HashMap<>();//查询统计
//        List<SysDept> sysDepts = null;
//        SysDept tempSysDept = null;
//        Long parentId = new Long(0);
//        tempSysDept = new SysDept();
//        tempSysDept.setParentId(deptId);
//        tempSysDept.setDeptName("事业部");
//        sysDepts = sysDeptMapper.selectDepartmentListByParentId(tempSysDept);
//        //累计合同额
//        BigDecimal contractAmount = null;
//        //累计贸易额
//        BigDecimal tradeAmount = null;
//        //累计运输量
//        BigDecimal checkCoalNum = null;
//        List<BaContractStart> contractStarts = null;
//        //将元变为万元
//        BigDecimal bigDecimal = new BigDecimal("10000");
//        DataLayoutStatVO dataLayoutStatVO = null;
//        if (!CollectionUtils.isEmpty(sysDepts)) {
//            for (SysDept sysDept : sysDepts) {
//                if (!ObjectUtils.isEmpty(sysDept)) {
//                    //累计合同额
//                    contractAmount = new BigDecimal(0);
//                    //累计贸易额
//                    tradeAmount = new BigDecimal(0);
//                    //累计运输量
//                    checkCoalNum = new BigDecimal(0);
//                    contractStarts = baContractStartMapper.selectContractStartListByDeptId(sysDept.getDeptId());
//                    for (BaContractStart baContractStart : contractStarts) {
//                        if (!ObjectUtils.isEmpty(baContractStart)) {
//                            if (!subDepartmentMap.containsKey(baContractStart.getDeptName())) {
//                                dataLayoutStatVO = new DataLayoutStatVO();
//                                contractAmount = new BigDecimal(0);
//                                tradeAmount = new BigDecimal(0);
//                                checkCoalNum = new BigDecimal(0);
//                            } else {
//                                dataLayoutStatVO = (DataLayoutStatVO) subDepartmentMap.get(baContractStart.getDeptName());
//                                tradeAmount = dataLayoutStatVO.getTradeAmount();
//                                contractAmount = dataLayoutStatVO.getContractAmount();
//                                checkCoalNum = dataLayoutStatVO.getCheckCoalNum();
//                            }
//                            //合同额（万元）
//                            if (baContractStart.getPrice() != null && baContractStart.getTonnage() != null) {
//                                contractAmount = contractAmount.add(baContractStart.getPrice().multiply(baContractStart.getTonnage())
//                                        .setScale(2, BigDecimal.ROUND_HALF_UP)).divide(bigDecimal, 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
//                            }
//                            //下游结算单金额
//                            BaSettlement baSettlement = new BaSettlement();
//                            baSettlement.setFlag(0);
//                            baSettlement.setSettlementType(new Long(2)); //销售结算单
//                            baSettlement.setContractId(baContractStart.getId());
//                            baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
//                            BigDecimal tempTradeAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);
//                            if (tempTradeAmount != null) {
//                                tradeAmount = tradeAmount.add(tempTradeAmount);
//                            }
//                            //累计运输量
//                            BaTransport baTransport = new BaTransport();
//                            baTransport.setStartId(baContractStart.getId());
//                            BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
//                            if (!ObjectUtils.isEmpty(baTransport1)) {
//                                if (baTransport1.getCheckCoalNum() != null) {
//                                    checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
//                                }
//                                if (baTransport1.getCcjweight() != null) {
//                                    checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
//                                }
//                                if (baTransport1.getConfirmedDeliveryWeight() != null) {
//                                    checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
//                                }
//                            }
//                            dataLayoutStatVO.setTradeAmount(tradeAmount);
//                            dataLayoutStatVO.setContractAmount(contractAmount);
//                            dataLayoutStatVO.setCheckCoalNum(checkCoalNum);
//
//                        }
//                    }
//                    subDepartmentMap.put(String.valueOf(sysDept.getDeptName()), dataLayoutStatVO);
//                }
//            }
//        }
//
//        if (!ObjectUtils.isEmpty(subDepartmentMap)){
//            //第一种方法
//            Iterator<Map.Entry<String, DataLayoutStatVO>> entries = subDepartmentMap.entrySet().iterator();
//            while (entries.hasNext()) {
//                Map.Entry<String, DataLayoutStatVO> entry = entries.next();
//                String dataLayoutStatVOKey = entry.getKey();
//                DataLayoutStatVO dataLayoutStatVOValue = entry.getValue();
//                if (!ObjectUtils.isEmpty(dataLayoutStatVOValue)) {
//                    dataLayoutStatVOValue.setDeptId(dataLayoutStatVOKey);
//                } else {
//                    dataLayoutStatVOValue = new DataLayoutStatVO();
//                    dataLayoutStatVOValue.setDeptId(dataLayoutStatVOKey);
//                }
//                subDepartmentList.add(dataLayoutStatVOValue);
//            }
//        }
//
//        return subDepartmentList;
//    }

    @Override
    public List<StatisticsDTO> statistics() {

        List<StatisticsDTO> dtoList = new ArrayList<>();

        SysDept sysDept = new SysDept();
        List<SysDept> sysDeptList = sysDeptMapper.selectDeptList(sysDept);
        for (SysDept dept:sysDeptList) {
            if(dept.getParentId() == 101){
                StatisticsDTO statisticsDTO = new StatisticsDTO();
                statisticsDTO.setDeptName(dept.getDeptName());
                //查询合同启动数据
                QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
                startQueryWrapper.eq("state", AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
                startQueryWrapper.eq("dept_id",dept.getDeptId());
                startQueryWrapper.eq("flag",0);
                List<BaContractStart> contractStarts = baContractStartMapper.selectList(startQueryWrapper);
                //累计合同额
                BigDecimal contractAmount = new BigDecimal(0);
                //累计贸易额
                BigDecimal tradeAmount = new BigDecimal(0);
                //累计运输量
                BigDecimal checkCoalNum = new BigDecimal(0);
                //昕科累计运单量
                BigDecimal ccjweightTotal = new BigDecimal(0);
                //中储累计运单量
                BigDecimal weightTotal = new BigDecimal(0);
                for (BaContractStart baContractStart:contractStarts) {
                    //合同额
                    if(baContractStart.getPrice() != null && baContractStart.getTonnage() != null){
                        contractAmount = contractAmount.add(baContractStart.getPrice().multiply(baContractStart.getTonnage()));
                    }
                    //下游结算单金额
                    QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                    settlementQueryWrapper.eq("contract_id", baContractStart.getId());
                    settlementQueryWrapper.eq("state", "settlement:pass");
                    settlementQueryWrapper.eq("flag", 0);
                    settlementQueryWrapper.eq("settlement_type", 2);
                    List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                    for (BaSettlement baSettlement : baSettlementList) {
                        if(baSettlement.getSettlementAmount() != null){
                            tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                        }
                    }
                    //累计运输量
                    QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                    transportQueryWrapper.eq("start_id", baContractStart.getId());
                    transportQueryWrapper.eq("flag", 0);
                    List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                    for (BaTransport baTransport : baTransports) {
                        //验收信息
                        QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                        checkQueryWrapper.eq("relation_id", baTransport.getId());
                        checkQueryWrapper.eq("type", 1);
                        List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                        for (BaCheck baCheck : baChecks) {
                            checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                        }
                        //无车承运货源数据（昕科）
                        QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
                        automobileQueryWrapper.eq("order_id",baTransport.getId());
                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
                        if(StringUtils.isNotNull(baTransportAutomobile)){
                            //查询运单(昕科)
                            QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
                            orderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                            List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
                            for (BaShippingOrder baShippingOrder:shippingOrders) {
                                if(baShippingOrder.getCcjweight() != null){
                                    //昕科装车重量累计
                                    ccjweightTotal = ccjweightTotal.add(baShippingOrder.getCcjweight());
                                }
                            }
                        }
                        //无车承运货源数据（中储）
                        QueryWrapper<BaTransportCmst> cmstQueryWrapper = new QueryWrapper<>();
                        cmstQueryWrapper.eq("relation_id",baTransport.getId());
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectOne(cmstQueryWrapper);
                        if(StringUtils.isNotNull(transportCmst)){
                            //查询运单（中储）
                            BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                            baShippingOrderCmst1.setYardId(transportCmst.getId());
                            List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    /*QueryWrapper<BaShippingOrderCmst> orderCmstQueryWrapper = new QueryWrapper<>();
                    orderCmstQueryWrapper.eq("yard_id",transportCmst.getId());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(orderCmstQueryWrapper);*/
                            for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                                if(baShippingOrderCmst.getConfirmedDeliveryWeight() != null){
                                    //中储装车累计重量
                                    weightTotal = weightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                                }
                            }
                        }
                    }
                }
               statisticsDTO.setContractAmount(contractAmount);
                statisticsDTO.setTradeAmount(tradeAmount);
                statisticsDTO.setCheckCoalNum(checkCoalNum.add(ccjweightTotal.add(weightTotal)));
                dtoList.add(statisticsDTO);
            }
        }
        return dtoList;
    }
}
