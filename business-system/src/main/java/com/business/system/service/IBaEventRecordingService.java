package com.business.system.service;

import com.business.system.domain.BaEventRecording;

import java.util.List;

/**
 * 事件记录Service接口
 *
 * @author single
 * @date 2023-07-05
 */
public interface IBaEventRecordingService
{
    /**
     * 查询事件记录
     *
     * @param id 事件记录ID
     * @return 事件记录
     */
    public BaEventRecording selectBaEventRecordingById(String id);

    /**
     * 查询事件记录列表
     *
     * @param baEventRecording 事件记录
     * @return 事件记录集合
     */
    public List<BaEventRecording> selectBaEventRecordingList(BaEventRecording baEventRecording);

    /**
     * 新增事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    public int insertBaEventRecording(BaEventRecording baEventRecording);

    /**
     * 修改事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    public int updateBaEventRecording(BaEventRecording baEventRecording);

    /**
     * 批量删除事件记录
     *
     * @param ids 需要删除的事件记录ID
     * @return 结果
     */
    public int deleteBaEventRecordingByIds(String[] ids);

    /**
     * 删除事件记录信息
     *
     * @param id 事件记录ID
     * @return 结果
     */
    public int deleteBaEventRecordingById(String id);


}
