package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaBidTransport;

/**
 * 合同启动投标信息运输Service接口
 *
 * @author single
 * @date 2023-06-05
 */
public interface IBaBidTransportService
{
    /**
     * 查询合同启动投标信息运输
     *
     * @param id 合同启动投标信息运输ID
     * @return 合同启动投标信息运输
     */
    public BaBidTransport selectBaBidTransportById(String id);

    /**
     * 查询合同启动投标信息运输列表
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 合同启动投标信息运输集合
     */
    public List<BaBidTransport> selectBaBidTransportList(BaBidTransport baBidTransport);

    /**
     * 新增合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    public int insertBaBidTransport(BaBidTransport baBidTransport);

    /**
     * 修改合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    public int updateBaBidTransport(BaBidTransport baBidTransport);

    /**
     * 批量删除合同启动投标信息运输
     *
     * @param ids 需要删除的合同启动投标信息运输ID
     * @return 结果
     */
    public int deleteBaBidTransportByIds(String[] ids);

    /**
     * 删除合同启动投标信息运输信息
     *
     * @param id 合同启动投标信息运输ID
     * @return 结果
     */
    public int deleteBaBidTransportById(String id);

}
