package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaSettlementFreightMapper;
import com.business.system.domain.BaSettlementFreight;
import com.business.system.service.IBaSettlementFreightService;

/**
 * 汽运结算Service业务层处理
 *
 * @author ljb
 * @date 2023-02-24
 */
@Service
public class BaSettlementFreightServiceImpl extends ServiceImpl<BaSettlementFreightMapper, BaSettlementFreight> implements IBaSettlementFreightService
{
    @Autowired
    private BaSettlementFreightMapper baSettlementFreightMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    @Override
    public BaSettlementFreight selectBaSettlementFreightById(String id)
    {
        return baSettlementFreightMapper.selectBaSettlementFreightById(id);
    }

    /**
     * 查询汽运结算列表
     *
     * @param baSettlementFreight 汽运结算
     * @return 汽运结算
     */
    @Override
    public List<BaSettlementFreight> selectBaSettlementFreightList(BaSettlementFreight baSettlementFreight)
    {
        return baSettlementFreightMapper.selectBaSettlementFreightList(baSettlementFreight);
    }

    /**
     * 新增汽运结算
     *
     * @param baSettlementFreight 汽运结算
     * @return 结果
     */
    @Override
    public int insertBaSettlementFreight(BaSettlementFreight baSettlementFreight)
    {
        baSettlementFreight.setId(getRedisIncreID.getId());
        baSettlementFreight.setUserId(SecurityUtils.getUserId());
        baSettlementFreight.setDeptId(SecurityUtils.getDeptId());
        baSettlementFreight.setCreateBy(SecurityUtils.getUsername());
        baSettlementFreight.setCreateTime(DateUtils.getNowDate());
        return baSettlementFreightMapper.insertBaSettlementFreight(baSettlementFreight);
    }

    /**
     * 修改汽运结算
     *
     * @param baSettlementFreight 汽运结算
     * @return 结果
     */
    @Override
    public int updateBaSettlementFreight(BaSettlementFreight baSettlementFreight)
    {
        baSettlementFreight.setUpdateTime(DateUtils.getNowDate());
        baSettlementFreight.setUpdateBy(SecurityUtils.getUsername());
        return baSettlementFreightMapper.updateBaSettlementFreight(baSettlementFreight);
    }

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementFreightByIds(String[] ids)
    {
        return baSettlementFreightMapper.deleteBaSettlementFreightByIds(ids);
    }

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementFreightById(String id)
    {
        return baSettlementFreightMapper.deleteBaSettlementFreightById(id);
    }

}
