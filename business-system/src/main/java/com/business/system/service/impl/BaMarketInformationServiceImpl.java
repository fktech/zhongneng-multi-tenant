package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaGoods;
import com.business.system.domain.BaGoodsType;
import com.business.system.domain.BaRenew;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.domain.BaMarketInformation;
import com.business.system.service.IBaMarketInformationService;
import org.springframework.util.ObjectUtils;

/**
 * 市场信息管理Service业务层处理
 *
 * @author ljb
 * @date 2023-03-22
 */
@Service
public class BaMarketInformationServiceImpl extends ServiceImpl<BaMarketInformationMapper, BaMarketInformation> implements IBaMarketInformationService
{
    @Autowired
    private BaMarketInformationMapper baMarketInformationMapper;

    @Autowired
    private BaRenewMapper baRenewMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    /**
     * 查询市场信息管理
     *
     * @param id 市场信息管理ID
     * @return 市场信息管理
     */
    @Override
    public BaMarketInformation selectBaMarketInformationById(String id)
    {
        BaMarketInformation baMarketInformation = baMarketInformationMapper.selectBaMarketInformationById(id);
        if(!ObjectUtils.isEmpty(baMarketInformation)){
            //查询相关更新信息
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",baMarketInformation.getId());
            renewQueryWrapper.eq("historical_data",0);
            List<BaRenew> baRenews = baRenewMapper.selectList(renewQueryWrapper);
            for (BaRenew baRenew:baRenews) {
                if(StringUtils.isNotEmpty(baRenew.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baRenew.getGoodsId());
                    baRenew.setGoodsName(baGoods.getName());
                }
            }
            baMarketInformation.setBaRenewList(baRenews);
        }
        return baMarketInformation;
    }

    /**
     * 查询市场信息管理列表
     *
     * @param baMarketInformation 市场信息管理
     * @return 市场信息管理
     */
    @Override
    public List<BaMarketInformation> selectBaMarketInformationList(BaMarketInformation baMarketInformation)
    {
        if(StringUtils.isNotEmpty(baMarketInformation.getType())){
            baMarketInformation.setHistoricalData(0);
        }
        baMarketInformation.setSorts("1");
        List<BaMarketInformation> baMarketInformationList = baMarketInformationMapper.selectBaMarketInformationList(baMarketInformation);
        for (BaMarketInformation marketInformation:baMarketInformationList) {
            //查询相关更新信息
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",marketInformation.getId());
            renewQueryWrapper.eq("historical_data",0);
            List<BaRenew> baRenews = baRenewMapper.selectList(renewQueryWrapper);
            for (BaRenew baRenew:baRenews) {
                if(StringUtils.isNotEmpty(baRenew.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baRenew.getGoodsId());
                    baRenew.setGoodsName(baGoods.getName());
                }
            }
            marketInformation.setBaRenewList(baRenews);
        }
        return baMarketInformationList;
    }

    @Override
    public List<BaMarketInformation> baMarketInformationList(BaMarketInformation baMarketInformation) {

        List<BaMarketInformation> baMarketInformationList = baMarketInformationMapper.selectBaMarketInformationList(baMarketInformation);
        for (BaMarketInformation marketInformation:baMarketInformationList) {
            //查询相关更新信息
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",marketInformation.getId());
            renewQueryWrapper.eq("historical_data",0);
            List<BaRenew> baRenews = baRenewMapper.selectList(renewQueryWrapper);
            for (BaRenew baRenew:baRenews) {
                if(StringUtils.isNotEmpty(baRenew.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baRenew.getGoodsId());
                    baRenew.setGoodsName(baGoods.getName());
                }
            }
            marketInformation.setBaRenewList(baRenews);
        }
        return baMarketInformationList;
    }

    /**
     * 新增市场信息管理
     *
     * @param baMarketInformation 市场信息管理
     * @return 结果
     */
    @Override
    public int insertBaMarketInformation(BaMarketInformation baMarketInformation)
    {
        //查询库中是否有原始数据
        List<BaMarketInformation> baMarketInformations = baMarketInformationMapper.selectBaMarketInformationList(baMarketInformation);
        if(baMarketInformations.size() > 0 ){
            for (BaMarketInformation marketInformation:baMarketInformations) {
                marketInformation.setHistoricalData(baMarketInformations.get(0).getHistoricalData()+1);
                baMarketInformationMapper.updateBaMarketInformation(marketInformation);
            }
        }
        baMarketInformation.setId(getRedisIncreID.getId());
        int result = baMarketInformationMapper.insertBaMarketInformation(baMarketInformation);
        if(result > 0){
            for (BaRenew baRenew:baMarketInformation.getBaRenewList()) {
                baRenew.setId(getRedisIncreID.getId());
                baRenew.setCreateTime(DateUtils.getNowDate());
                baRenew.setCreateBy(SecurityUtils.getUsername());
                baRenew.setUserId(SecurityUtils.getUserId());
                baRenew.setDeptId(SecurityUtils.getDeptId());
                baRenew.setRelationId(baMarketInformation.getId());
                result = baRenewMapper.insertBaRenew(baRenew);
            }
        }
            baMarketInformation.setRecentlyTime(DateUtils.getNowDate());
            //最新更新人
            baMarketInformation.setRecently(sysUserMapper.selectUserById(SecurityUtils.getUserId()).getNickName());
            //租户ID
            baMarketInformation.setTenantId(SecurityUtils.getCurrComId());
            //查询相关更新信息数量
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",baMarketInformation.getId());
            renewQueryWrapper.eq("historical_data",0);
            List<BaRenew> baRenews = baRenewMapper.selectList(renewQueryWrapper);
            baMarketInformation.setRowCount(Long.valueOf(baRenews.size()));
            result = baMarketInformationMapper.updateBaMarketInformation(baMarketInformation);
        return result;
    }

    /**
     * 修改市场信息管理
     *
     * @param baMarketInformation 市场信息管理
     * @return 结果
     */
    @Override
    public int updateBaMarketInformation(BaMarketInformation baMarketInformation)
    {
        /*Integer result = 0;
        if(StringUtils.isNotNull(baMarketInformation.getBaRenewList())){
            //遍历相关更新数据
            for (BaRenew baRenew:baMarketInformation.getBaRenewList()) {
                if(StringUtils.isNotEmpty(baRenew.getId())){
                    BaRenew renew = baRenewMapper.selectBaRenewById(baRenew.getId());
                    renew.setHistoricalData(1);
                    baRenewMapper.updateBaRenew(renew);
                    baRenew.setId(getRedisIncreID.getId());
                    baRenew.setCreateTime(DateUtils.getNowDate());
                    baRenew.setCreateBy(SecurityUtils.getUsername());
                    baRenew.setUserId(SecurityUtils.getUserId());
                    baRenew.setDeptId(SecurityUtils.getDeptId());
                    baRenew.setRelationId(baMarketInformation.getId());
                    result = baRenewMapper.insertBaRenew(baRenew);
                }else {
                    baRenew.setId(getRedisIncreID.getId());
                    baRenew.setCreateTime(DateUtils.getNowDate());
                    baRenew.setCreateBy(SecurityUtils.getUsername());
                    baRenew.setUserId(SecurityUtils.getUserId());
                    baRenew.setDeptId(SecurityUtils.getDeptId());
                    baRenew.setRelationId(baMarketInformation.getId());
                    result = baRenewMapper.insertBaRenew(baRenew);
                }
            }
        }
        if(result > 0){
            baMarketInformation.setRecentlyTime(DateUtils.getNowDate());
            //最新更新人
            baMarketInformation.setRecently(sysUserMapper.selectUserById(SecurityUtils.getUserId()).getNickName());
            //查询相关更新信息数量
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",baMarketInformation.getId());
            renewQueryWrapper.eq("historical_data",0);
            List<BaRenew> baRenews = baRenewMapper.selectList(renewQueryWrapper);
            baMarketInformation.setRowCount(Long.valueOf(baRenews.size()));
        }*/

        return baMarketInformationMapper.updateBaMarketInformation(baMarketInformation);
    }

    /**
     * 批量删除市场信息管理
     *
     * @param ids 需要删除的市场信息管理ID
     * @return 结果
     */
    @Override
    public int deleteBaMarketInformationByIds(String[] ids)
    {
        for(String id:ids){
            BaMarketInformation marketInformation = baMarketInformationMapper.selectBaMarketInformationById(id);
            if(marketInformation.getHistoricalData() == 0){
                //查询相关数据
                QueryWrapper<BaMarketInformation> marketInformationQueryWrapper = new QueryWrapper<>();
                marketInformationQueryWrapper.eq("type",marketInformation.getType());
                marketInformationQueryWrapper.orderByDesc("historical_data");
                List<BaMarketInformation> baMarketInformations = baMarketInformationMapper.selectList(marketInformationQueryWrapper);
                BaMarketInformation baMarketInformation = baMarketInformations.get(0);
                baMarketInformation.setHistoricalData(0);
                baMarketInformationMapper.updateBaMarketInformation(baMarketInformation);
            }
            //查询相关更新信息
            QueryWrapper<BaRenew> renewQueryWrapper = new QueryWrapper<>();
            renewQueryWrapper.eq("relation_id",marketInformation.getId());
            List<BaRenew> baRenewList = baRenewMapper.selectList(renewQueryWrapper);
            for (BaRenew baRenew:baRenewList) {
                baRenewMapper.deleteBaRenewById(baRenew.getId());
            }
        }
        return baMarketInformationMapper.deleteBaMarketInformationByIds(ids);
    }

    /**
     * 删除市场信息管理信息
     *
     * @param id 市场信息管理ID
     * @return 结果
     */
    @Override
    public int deleteBaMarketInformationById(String id)
    {
        return baMarketInformationMapper.deleteBaMarketInformationById(id);
    }

}
