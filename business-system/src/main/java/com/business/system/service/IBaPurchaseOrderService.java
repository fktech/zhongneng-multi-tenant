package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaPurchaseOrder;

/**
 * 采购清单Service接口
 *
 * @author ljb
 * @date 2023-08-31
 */
public interface IBaPurchaseOrderService
{
    /**
     * 查询采购清单
     *
     * @param id 采购清单ID
     * @return 采购清单
     */
    public BaPurchaseOrder selectBaPurchaseOrderById(String id);

    /**
     * 查询采购清单列表
     *
     * @param baPurchaseOrder 采购清单
     * @return 采购清单集合
     */
    public List<BaPurchaseOrder> selectBaPurchaseOrderList(BaPurchaseOrder baPurchaseOrder);

    /**
     * 新增采购清单
     *
     * @param baPurchaseOrder 采购清单
     * @return 结果
     */
    public int insertBaPurchaseOrder(BaPurchaseOrder baPurchaseOrder);

    /**
     * 修改采购清单
     *
     * @param baPurchaseOrder 采购清单
     * @return 结果
     */
    public int updateBaPurchaseOrder(BaPurchaseOrder baPurchaseOrder);

    /**
     * 批量删除采购清单
     *
     * @param ids 需要删除的采购清单ID
     * @return 结果
     */
    public int deleteBaPurchaseOrderByIds(String[] ids);

    /**
     * 删除采购清单信息
     *
     * @param id 采购清单ID
     * @return 结果
     */
    public int deleteBaPurchaseOrderById(String id);


}
