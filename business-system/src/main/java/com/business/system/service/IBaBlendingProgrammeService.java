package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaBlendingProgramme;

/**
 * 配煤方案Service接口
 *
 * @author ljb
 * @date 2024-01-05
 */
public interface IBaBlendingProgrammeService
{
    /**
     * 查询配煤方案
     *
     * @param id 配煤方案ID
     * @return 配煤方案
     */
    public BaBlendingProgramme selectBaBlendingProgrammeById(String id);

    /**
     * 查询配煤方案列表
     *
     * @param baBlendingProgramme 配煤方案
     * @return 配煤方案集合
     */
    public List<BaBlendingProgramme> selectBaBlendingProgrammeList(BaBlendingProgramme baBlendingProgramme);

    /**
     * 新增配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    public int insertBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme);

    /**
     * 修改配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    public int updateBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme);

    /**
     * 批量删除配煤方案
     *
     * @param ids 需要删除的配煤方案ID
     * @return 结果
     */
    public int deleteBaBlendingProgrammeByIds(String[] ids);

    /**
     * 删除配煤方案信息
     *
     * @param id 配煤方案ID
     * @return 结果
     */
    public int deleteBaBlendingProgrammeById(String id);


}
