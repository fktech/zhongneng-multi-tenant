package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaGoods;
import com.business.system.domain.BaUnit;
import com.business.system.domain.vo.MaterialTypeVo;
import com.business.system.mapper.BaGoodsMapper;
import com.business.system.mapper.BaUnitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaGoodsTypeMapper;
import com.business.system.domain.BaGoodsType;
import com.business.system.service.IBaGoodsTypeService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 商品分类Service业务层处理
 *
 * @author ljb
 * @date 2022-11-30
 */
@Service
public class BaGoodsTypeServiceImpl extends ServiceImpl<BaGoodsTypeMapper, BaGoodsType> implements IBaGoodsTypeService
{
    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaUnitMapper baUnitMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    /**
     * 查询商品分类
     *
     * @param id 商品分类ID
     * @return 商品分类
     */
    @Override
    public BaGoodsType selectBaGoodsTypeById(String id)
    {
        return baGoodsTypeMapper.selectBaGoodsTypeById(id);
    }

    /**
     * 查询商品分类列表
     *
     * @param baGoodsType 商品分类
     * @return 商品分类
     */
    @Override
    public List<BaGoodsType> selectBaGoodsTypeList(BaGoodsType baGoodsType)
    {
        QueryWrapper<BaGoodsType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id","0");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        queryWrapper.orderByAsc("sort");
        List<BaGoodsType> parentLis = baGoodsTypeMapper.selectList(queryWrapper);
        //查询所有的二级分类
        for (BaGoodsType parent : parentLis){
            QueryWrapper<BaGoodsType> childQueryWrapper = new QueryWrapper<>();
            childQueryWrapper.eq("parent_id",parent.getId());
            childQueryWrapper.orderByAsc("sort");
            BaUnit baUnit = baUnitMapper.selectBaUnitById(parent.getPricingInit());
            if(!ObjectUtils.isEmpty(baUnit)){
                parent.setPricingInitName(baUnit.getName());
            }
            List<BaGoodsType> childList = baGoodsTypeMapper.selectList(childQueryWrapper);
            if (!CollectionUtils.isEmpty(childList)) {
                for(BaGoodsType baGoodsType1 : childList){
                    if(!ObjectUtils.isEmpty(baGoodsType1)){
                        baUnit = baUnitMapper.selectBaUnitById(baGoodsType1.getPricingInit());
                        if(!ObjectUtils.isEmpty(baUnit)){
                            baGoodsType1.setPricingInitName(baUnit.getName());
                        }
                    }
                }
                parent.setChildList(childList);

            }
        }
        return parentLis;
    }

    @Override
    public List<MaterialTypeVo> selectBaGoodsTypeTypeList() {
        List<MaterialTypeVo> result;
        //查询所有一级分类
        QueryWrapper<BaGoodsType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id","0");
        queryWrapper.orderByAsc("sort");
        List<BaGoodsType> parentList = baGoodsTypeMapper.selectList(queryWrapper);
        //数据组合放入一级分类
        result = new ArrayList<>();
        MaterialTypeVo materialTypeVo;
        for (BaGoodsType baGoodsType:parentList) {
            materialTypeVo = new MaterialTypeVo();
            materialTypeVo.setValue(baGoodsType.getId());
            materialTypeVo.setLabel(baGoodsType.getName());
            //数据组合查询二级分类放入
            QueryWrapper<BaGoodsType> childQueryWrapper = new QueryWrapper<>();
            childQueryWrapper.eq("parent_id",baGoodsType.getId());
            childQueryWrapper.orderByAsc("sort");
            List<BaGoodsType> childList = baGoodsTypeMapper.selectList(childQueryWrapper);
            MaterialTypeVo childListVo;
            List<MaterialTypeVo> typeVoList = new ArrayList<>();
            for (BaGoodsType child : childList){
                childListVo = new MaterialTypeVo();
                childListVo.setValue(child.getId());
                childListVo.setLabel(child.getName());
                typeVoList.add(childListVo);
            }
            materialTypeVo.setChildren(typeVoList);
            result.add(materialTypeVo);
        }

        return result;
    }

    /**
     * 新增商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    @Override
    public int insertBaGoodsType(BaGoodsType baGoodsType)
    {
        QueryWrapper<BaGoodsType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baGoodsType.getName());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaGoodsType type = baGoodsTypeMapper.selectOne(queryWrapper);
        //分类名称不能重复
        if (StringUtils.isNotNull(type))
            return -1;
        baGoodsType.setId(getRedisIncreID.getId());
        baGoodsType.setCreateTime(DateUtils.getNowDate());
        baGoodsType.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baGoodsType.setTenantId(SecurityUtils.getCurrComId());
        if (baGoodsType.getSort() == null)
            baGoodsType.setSort(0);
        return baGoodsTypeMapper.insertBaGoodsType(baGoodsType);
    }

    /**
     * 修改商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    @Override
    public int updateBaGoodsType(BaGoodsType baGoodsType)
    {
        baGoodsType.setUpdateTime(DateUtils.getNowDate());
        baGoodsType.setUpdateBy(SecurityUtils.getUsername());
        return baGoodsTypeMapper.updateBaGoodsType(baGoodsType);
    }

    /**
     * 批量删除商品分类
     *
     * @param ids 需要删除的商品分类ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsTypeByIds(String[] ids)
    {
       BaGoodsType baGoodsType =  baGoodsTypeMapper.selectBaGoodsTypeById(ids[0]);
       //查询商品信息
        QueryWrapper<BaGoods> goodsQueryWrapper = new QueryWrapper<>();
        goodsQueryWrapper.eq("goods_type",baGoodsType.getId());
        goodsQueryWrapper.eq("flag",0);
        List<BaGoods> baGoodsList = baGoodsMapper.selectList(goodsQueryWrapper);
        if(baGoodsList.size() > 0){
            return -1;
        }
        if (baGoodsType.getParentId().equals("0")){
            QueryWrapper<BaGoodsType> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id",baGoodsType.getId());
            //查询分类下的子类
            List<BaGoodsType> childList = baGoodsTypeMapper.selectList(queryWrapper);

            for (BaGoodsType child:childList) {
                //查询商品信息
                goodsQueryWrapper = new QueryWrapper<>();
                goodsQueryWrapper.eq("goods_type",child.getId());
                goodsQueryWrapper.eq("flag",0);
                List<BaGoods> goodsList = baGoodsMapper.selectList(goodsQueryWrapper);
                if(goodsList.size() > 0){
                    return -1;
                }
                baGoodsTypeMapper.deleteBaGoodsTypeById(child.getId());
            }
        }
        return baGoodsTypeMapper.deleteBaGoodsTypeByIds(ids);
    }

    /**
     * 删除商品分类信息
     *
     * @param id 商品分类ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsTypeById(String id)
    {
        return baGoodsTypeMapper.deleteBaGoodsTypeById(id);
    }

    @Override
    public List<MaterialTypeVo> selectTypeAndGoodsList() {
        List<MaterialTypeVo> result = new ArrayList<>();
        //查询所有一级分类
        QueryWrapper<BaGoodsType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id","0");
        queryWrapper.orderByAsc("sort");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaGoodsType> parentList = baGoodsTypeMapper.selectList(queryWrapper);
        //数据组合放入一级分类
        MaterialTypeVo materialTypeVo;

        QueryWrapper<BaGoods> queryGoodWrapper = new QueryWrapper<>();
        queryGoodWrapper.eq("flag","0");
        queryGoodWrapper.orderByDesc("create_time");
        for (BaGoodsType baGoodsType : parentList) {
            List<MaterialTypeVo> typeVoList = new ArrayList<>();
            queryGoodWrapper = new QueryWrapper<>();
            queryGoodWrapper.eq("goods_type", baGoodsType.getId());
            queryGoodWrapper.eq("flag","0");
            queryGoodWrapper.eq("state","goods:pass");
            queryGoodWrapper.orderByDesc("create_time");
            List<BaGoods> baGoodsList = baGoodsMapper.selectList(queryGoodWrapper);
            if(!CollectionUtils.isEmpty(baGoodsList)){
                for(BaGoods goods : baGoodsList){
                    materialTypeVo = new MaterialTypeVo();
                    materialTypeVo.setValue(goods.getId());
                    materialTypeVo.setLabel(goods.getName());
                    typeVoList.add(materialTypeVo);
                }
            }
            materialTypeVo = new MaterialTypeVo();
            materialTypeVo.setValue(baGoodsType.getId());
            materialTypeVo.setLabel(baGoodsType.getName());
            //数据组合查询二级分类放入
            QueryWrapper<BaGoodsType> childQueryWrapper = new QueryWrapper<>();
            childQueryWrapper.eq("parent_id",baGoodsType.getId());
            childQueryWrapper.orderByAsc("sort");
            List<BaGoodsType> childList = baGoodsTypeMapper.selectList(childQueryWrapper);
            MaterialTypeVo childListVo;

            if(!CollectionUtils.isEmpty(childList)){
                for(BaGoodsType child : childList){
                    childListVo = new MaterialTypeVo();
                    childListVo.setValue(child.getId());
                    childListVo.setLabel(child.getName());
                    queryGoodWrapper = new QueryWrapper<>();
                    queryGoodWrapper.eq("goods_type", child.getId());
                    queryGoodWrapper.eq("flag","0");
                    queryGoodWrapper.eq("state","goods:pass");
                    queryGoodWrapper.orderByDesc("create_time");
                    baGoodsList = baGoodsMapper.selectList(queryGoodWrapper);
                    if(!CollectionUtils.isEmpty(baGoodsList)){
                        List<MaterialTypeVo> typeVoList1 = new ArrayList<>();
                        for(BaGoods goods : baGoodsList){
                            MaterialTypeVo childListVo1 = new MaterialTypeVo();
                            childListVo1.setValue(goods.getId());
                            childListVo1.setLabel(goods.getName());
                            typeVoList1.add(childListVo1);
                        }
                        childListVo.setChildren(typeVoList1);
                    }
                    typeVoList.add(childListVo);
                }
            }
            materialTypeVo.setChildren(typeVoList);
            result.add(materialTypeVo);
        }

        return result;
    }

}
