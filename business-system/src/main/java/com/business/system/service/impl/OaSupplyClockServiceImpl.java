package com.business.system.service.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 补卡Service业务层处理
 *
 * @author single
 * @date 2023-11-27
 */
@Service
public class OaSupplyClockServiceImpl extends ServiceImpl<OaSupplyClockMapper, OaSupplyClock> implements IOaSupplyClockService
{
    private static final Logger log = LoggerFactory.getLogger(OaSupplyClockServiceImpl.class);

    @Autowired
    private OaSupplyClockMapper oaSupplyClockMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private ISysDictDataService sysDictDataService;

    /**
     * 查询补卡
     *
     * @param id 补卡ID
     * @return 补卡
     */
    @Override
    public OaSupplyClock selectOaSupplyClockById(String id)
    {
        OaSupplyClock oaSupplyClock = oaSupplyClockMapper.selectOaSupplyClockById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> supplyClockQueryWrapper = new QueryWrapper<>();
        supplyClockQueryWrapper.eq("business_id",oaSupplyClock.getId());
        supplyClockQueryWrapper.eq("flag",0);
        supplyClockQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(supplyClockQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        oaSupplyClock.setProcessInstanceId(processInstanceIds);
        //申请人名称
        if(oaSupplyClock.getUserId() != null){
            oaSupplyClock.setUserName(sysUserMapper.selectUserById(oaSupplyClock.getUserId()).getNickName());
        }
        //申请人部门
        if(oaSupplyClock.getDeptId() != null){
            oaSupplyClock.setDeptName(sysDeptMapper.selectDeptById(oaSupplyClock.getDeptId()).getDeptName());
        }
        return oaSupplyClock;
    }

    /**
     * 查询补卡列表
     *
     * @param oaSupplyClock 补卡
     * @return 补卡
     */
    @Override
    public List<OaSupplyClock> selectOaSupplyClockList(OaSupplyClock oaSupplyClock)
    {
        return oaSupplyClockMapper.selectOaSupplyClockList(oaSupplyClock);
    }

    /**
     * 新增补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    @Override
    public UR insertOaSupplyClock(OaSupplyClock oaSupplyClock)
    {
        //判断是否为保存数据
        if(StringUtils.isNotEmpty(oaSupplyClock.getId()) && "0".equals(oaSupplyClock.getSubmitState())){
            return UR.ok().code(oaSupplyClockMapper.updateOaSupplyClock(oaSupplyClock));
        }
        //判断是否是下班打卡
        if("2".equals(oaSupplyClock.getClockType())){
            OaClockDTO oaClockDTO = new OaClockDTO();
            oaClockDTO.setEmployeeId(oaSupplyClock.getUserId());
            oaClockDTO.setSearchTime(DateUtils.dateTime(oaSupplyClock.getClockDate())); // 打卡日期
            oaClockDTO.setType("1"); //内勤
            oaClockDTO.setClockType("1"); //查询上班打卡
            List<OaClock> oaClocks = oaClockMapper.statOaClockList(oaClockDTO);
            if(!CollectionUtils.isEmpty(oaClocks)) {
                //查询打卡数据,进行补卡
                OaClock clock = oaClocks.get(0);
                if (!ObjectUtils.isEmpty(clock)) {
                    if(clock.getFlag().intValue() != 0 && !clock.getClockState().equals("12")){
                        return UR.error().code(-1).message("请先进行上班补卡");
                    }
                }
            }
        }
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        //判断补卡次数是否超过指定次数
        //查询当前补卡用户补卡次数
        OaSupplyClock supplyClock1 = new OaSupplyClock();
        supplyClock1.setUserId(oaSupplyClock.getUserId());
        supplyClock1.setCreateTimeSearch(df.format(new Date()));
        supplyClock1.setCardConditions("supplyClockState");
        List<OaSupplyClock> oaSupplyClocks = oaSupplyClockMapper.selectOaSupplyClockList(supplyClock1);
        //查看补卡次数（数据字典）
        List<SysDictData> supplyNumber = sysDictDataService.selectDictDataByType("supply_number");
        Integer integer = Integer.valueOf(supplyNumber.get(0).getDictValue());
        if(integer != 0){
            if(oaSupplyClocks.size() > integer || oaSupplyClocks.size() == integer){
                return UR.error().code(-2).message("补卡次数已用完");
            }
        }
        //判断保存数据提交
        if(StringUtils.isNotEmpty(oaSupplyClock.getId()) && "1".equals(oaSupplyClock.getSubmitState())){
            OaSupplyClock supplyClock = oaSupplyClockMapper.selectOaSupplyClockById(oaSupplyClock.getId());
            supplyClock.setFlag(1L);
            oaSupplyClockMapper.updateOaSupplyClock(supplyClock);
        }
        oaSupplyClock.setId(getRedisIncreID.getId());
        oaSupplyClock.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaSupplyClock.setTenantId(SecurityUtils.getCurrComId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode(),SecurityUtils.getCurrComId());
        oaSupplyClock.setFlowId(flowId);
        int result = oaSupplyClockMapper.insertOaSupplyClock(oaSupplyClock);
        if(result > 0 && "1".equals(oaSupplyClock.getSubmitState())){
            OaSupplyClock supplyClock = oaSupplyClockMapper.selectOaSupplyClockById(oaSupplyClock.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(supplyClock, AdminCodeEnum.OASUPPLYCLOCK_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaSupplyClockMapper.deleteOaSupplyClockById(supplyClock.getId());
                return UR.error().code(0).message("流程启动失败");
            }else {
                supplyClock.setState(AdminCodeEnum.OASUPPLYCLOCK_STATUS_VERIFYING.getCode());
                oaSupplyClockMapper.updateOaSupplyClock(supplyClock);
            }
        }
        /*if(integer != 0){
            //剩余打卡次数
            List<OaSupplyClock> list = oaSupplyClockMapper.selectOaSupplyClockList(supplyClock1);
            int i = integer - list.size();
            return UR.ok().code(-3).message("补卡提交成功，剩余"+"("+i+")"+"次补卡");
        }*/
        return UR.ok().code(200);
    }

    @Override
    public Map<String, Integer> frequency() {
        Map<String,Integer> map = new HashMap<>();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        //查询当前补卡用户补卡次数
        OaSupplyClock supplyClock1 = new OaSupplyClock();
        supplyClock1.setUserId(SecurityUtils.getUserId());
        supplyClock1.setCreateTimeSearch(df.format(new Date()));
        supplyClock1.setCardConditions("supplyClockState");
        List<OaSupplyClock> oaSupplyClocks = oaSupplyClockMapper.selectOaSupplyClockList(supplyClock1);
        //查看补卡次数（数据字典）
        List<SysDictData> supplyNumber = sysDictDataService.selectDictDataByType("supply_number");
        Integer integer = Integer.valueOf(supplyNumber.get(0).getDictValue());
        if(integer != 0){
            int i = integer - oaSupplyClocks.size();
            map.put("已补卡数",oaSupplyClocks.size());
            map.put("剩余补卡数",i);
        }
        return map;
    }

    /**
     * 提交补卡申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaSupplyClock supplyClock, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(supplyClock.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode(), AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(supplyClock.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        OaSupplyClock oaSupplyClock = this.selectOaSupplyClockById(supplyClock.getId());
        SysUser sysUser = iSysUserService.selectUserById(supplyClock.getUserId());
        oaSupplyClock.setUserName(sysUser.getNickName());
        oaSupplyClock.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaSupplyClock, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OASUPPLYCLOCK_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    @Override
    public UR updateOaSupplyClock(OaSupplyClock oaSupplyClock)
    {
        //原数据
        OaSupplyClock oaSupplyClock1 = oaSupplyClockMapper.selectOaSupplyClockById(oaSupplyClock.getId());

        oaSupplyClock.setUpdateTime(DateUtils.getNowDate());

        //判断是否是下班打卡
        if("2".equals(oaSupplyClock.getClockType())){
            OaClock oaClock = new OaClock();
            oaClock.setClockDate(oaSupplyClock.getClockDate()); // 打卡日期
            oaClock.setType("1"); //内勤
            oaClock.setClockType(oaSupplyClock.getClockType()); //下班打卡
            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(oaClock);
            if(!CollectionUtils.isEmpty(oaClocks)) {
                //查询打卡数据,进行补卡
                OaClock clock = oaClocks.get(0);
                if (!ObjectUtils.isEmpty(clock)) {
                    //弹性工作制
                    if("2".equals(clock.getAttendanceType())){
                        OaClock clock1 = new OaClock();
                        clock1.setClockDate(oaSupplyClock.getClockDate()); // 打卡日期
                        clock1.setType("1"); //内勤
                        clock1.setClockType("1"); //上班打卡
                        List<OaClock> clocks = oaClockMapper.selectOaClockList(oaClock);
                        if(!CollectionUtils.isEmpty(clocks)){
                            OaClock oaClock1 = clocks.get(0);
                            if(!ObjectUtils.isEmpty(oaClock1)){
                                if(oaClock1.getFlag().intValue() != 0 && !oaClock1.getClockState().equals("12")){
                                    return UR.error().code(-1).message("请先进行上班补卡");
                                }
                            }
                        }
                    }
                }
            }
        }
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        //判断补卡次数是否超过指定次数
        //查询当前补卡用户补卡次数
        OaSupplyClock supplyClock1 = new OaSupplyClock();
        supplyClock1.setUserId(oaSupplyClock.getUserId());
        supplyClock1.setCreateTimeSearch(df.format(new Date()));
        supplyClock1.setCardConditions("supplyClockState");
        List<OaSupplyClock> oaSupplyClocks = oaSupplyClockMapper.selectOaSupplyClockList(supplyClock1);
        //查看补卡次数（数据字典）
        List<SysDictData> supplyNumber = sysDictDataService.selectDictDataByType("supply_number");
        Integer integer = Integer.valueOf(supplyNumber.get(0).getDictValue());
        if(integer != 0){
            if(oaSupplyClocks.size() > integer || oaSupplyClocks.size() == integer){
                return UR.error().code(-2).message("补卡次数已用完");
            }
        }
        int result = oaSupplyClockMapper.updateOaSupplyClock(oaSupplyClock);
        if(result > 0 && "1".equals(oaSupplyClock.getSubmitState())){
            OaSupplyClock supplyClock = oaSupplyClockMapper.selectOaSupplyClockById(oaSupplyClock.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", supplyClock.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(supplyClock, AdminCodeEnum.OASUPPLYCLOCK_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaSupplyClockMapper.updateOaSupplyClock(oaSupplyClock1);
                return UR.error().code(0).message("流程启动失败");
            }else {
                supplyClock.setState(AdminCodeEnum.OASUPPLYCLOCK_STATUS_VERIFYING.getCode());
                oaSupplyClockMapper.updateOaSupplyClock(supplyClock);
            }
        }
        if(integer != 0){
            //剩余打卡次数
            List<OaSupplyClock> list = oaSupplyClockMapper.selectOaSupplyClockList(supplyClock1);
            int i = integer - list.size();
            return UR.ok().code(result).message("剩余"+"("+i+")"+"次补卡");
        }
        return UR.ok().code(result).message("补卡次数无限制");
    }

    /**
     * 批量删除补卡
     *
     * @param ids 需要删除的补卡ID
     * @return 结果
     */
    @Override
    public int deleteOaSupplyClockByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaSupplyClock oaSupplyClock = oaSupplyClockMapper.selectOaSupplyClockById(id);
                oaSupplyClock.setFlag(1L);
                oaSupplyClockMapper.updateOaSupplyClock(oaSupplyClock);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除补卡信息
     *
     * @param id 补卡ID
     * @return 结果
     */
    @Override
    public int deleteOaSupplyClockById(String id)
    {
        return oaSupplyClockMapper.deleteOaSupplyClockById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaSupplyClock supplyClock = oaSupplyClockMapper.selectOaSupplyClockById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",supplyClock.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OASUPPLYCLOCK_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(supplyClock.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                supplyClock.setState(AdminCodeEnum.OASUPPLYCLOCK_STATUS_WITHDRAW.getCode());
                oaSupplyClockMapper.updateOaSupplyClock(supplyClock);
                String userId = String.valueOf(supplyClock.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(supplyClock, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaSupplyClock supplyClock, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(supplyClock.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"补卡申请审批提醒",msgContent,"oa",supplyClock.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }
}
