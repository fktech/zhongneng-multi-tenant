package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaMaterialStock;
import com.business.system.domain.vo.BaDepotHeadChartVO;
import com.business.system.domain.vo.BaDepotVO;
import com.business.system.domain.vo.BaMasterialStockVO;

/**
 * 库存Service接口
 *
 * @author single
 * @date 2023-01-05
 */
public interface IBaMaterialStockService
{
    /**
     * 查询库存
     *
     * @param id 库存ID
     * @return 库存
     */
    public BaMaterialStock selectBaMaterialStockById(String id);

    /**
     * 查询库存列表
     *
     * @param baMaterialStock 库存
     * @return 库存集合
     */
    public List<BaMaterialStock> selectBaMaterialStockList(BaMaterialStock baMaterialStock);

    /**
     * 新增库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    public int insertBaMaterialStock(BaMaterialStock baMaterialStock);

    /**
     * 修改库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    public int updateBaMaterialStock(BaMaterialStock baMaterialStock);

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存ID
     * @return 结果
     */
    public int deleteBaMaterialStockByIds(String[] ids);

    /**
     * 删除库存信息
     *
     * @param id 库存ID
     * @return 结果
     */
    public int deleteBaMaterialStockById(String id);

    /**
     * 库存总量
     */
    public BigDecimal totals();

    /**
     * 根据商品查询库存仓库及货位列表
     */
    public List<BaMaterialStock> selectDepotList(BaMaterialStock baMaterialStock);

    /**
     * 根据仓库、库位查询库存
     */
    public BaMaterialStock getListByDepotAndLocation(BaMaterialStock baMaterialStock);

    /**
     * 统计库存
     */
    public int selectBaMaterialStockGoodsList(BaMaterialStock baMaterialStock);

    /**
     * 库存统计
     */
    public List<BaMasterialStockVO> sumList(BaMaterialStock baMaterialStock);

    /**
     * 项目库存统计
     */
    public List<BaMasterialStockVO> sumProjectList(BaMaterialStock baMaterialStock);

    /**
     * 仓位库存统计
     */
    public List<BaMasterialStockVO> sumLocationList(BaMaterialStock baMaterialStock);

    /**
     * 商品库存统计
     */
    public List<BaMasterialStockVO> sumGoodsList(BaMaterialStock baMaterialStock);

    /**
     * 智慧驾驶舱商品库存统计
     */
    public List<BaMasterialStockVO> sumGoodsLocationList(BaMaterialStock baMaterialStock);

    /**
     * 各品类入库出库量累计占比
     */
    public List<BaDepotHeadChartVO> sumGoodsDepotList(BaMaterialStock baMaterialStock);

    public List<BaDepotVO> depotList(BaMaterialStock baMaterialStock);

    public int countGoods(BaMaterialStock baMaterialStock);
}
