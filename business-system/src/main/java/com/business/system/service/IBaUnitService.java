package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaUnit;

/**
 * 单位Service接口
 *
 * @author ljb
 * @date 2022-11-30
 */
public interface IBaUnitService
{
    /**
     * 查询单位
     *
     * @param id 单位ID
     * @return 单位
     */
    public BaUnit selectBaUnitById(String id);

    /**
     * 查询单位列表
     *
     * @param baUnit 单位
     * @return 单位集合
     */
    public List<BaUnit> selectBaUnitList(BaUnit baUnit);

    /**
     * 新增单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    public int insertBaUnit(BaUnit baUnit);

    /**
     * 修改单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    public int updateBaUnit(BaUnit baUnit);

    /**
     * 批量删除单位
     *
     * @param ids 需要删除的单位ID
     * @return 结果
     */
    public int deleteBaUnitByIds(String[] ids);

    /**
     * 删除单位信息
     *
     * @param id 单位ID
     * @return 结果
     */
    public int deleteBaUnitById(String id);


}
