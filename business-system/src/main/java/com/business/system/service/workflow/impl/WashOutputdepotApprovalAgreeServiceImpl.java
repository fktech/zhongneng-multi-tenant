package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOutputdepotApprovalService;
import com.business.system.service.workflow.WashOutputdepotApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 洗煤出库申请审批结果:审批通过
 */
@Service("washOutputdepotApprovalContent:processApproveResult:pass")
@Slf4j
public class WashOutputdepotApprovalAgreeServiceImpl extends WashOutputdepotApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_PASS.getCode());
        return result;
    }
}
