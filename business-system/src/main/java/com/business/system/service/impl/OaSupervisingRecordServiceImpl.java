package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaSupervisingRecordMapper;
import com.business.system.domain.OaSupervisingRecord;
import com.business.system.service.IOaSupervisingRecordService;

/**
 * 办理记录Service业务层处理
 *
 * @author ljb
 * @date 2023-11-21
 */
@Service
public class OaSupervisingRecordServiceImpl extends ServiceImpl<OaSupervisingRecordMapper, OaSupervisingRecord> implements IOaSupervisingRecordService
{
    @Autowired
    private OaSupervisingRecordMapper oaSupervisingRecordMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询办理记录
     *
     * @param id 办理记录ID
     * @return 办理记录
     */
    @Override
    public OaSupervisingRecord selectOaSupervisingRecordById(String id)
    {
        return oaSupervisingRecordMapper.selectOaSupervisingRecordById(id);
    }

    /**
     * 查询办理记录列表
     *
     * @param oaSupervisingRecord 办理记录
     * @return 办理记录
     */
    @Override
    public List<OaSupervisingRecord> selectOaSupervisingRecordList(OaSupervisingRecord oaSupervisingRecord)
    {
        List<OaSupervisingRecord> oaSupervisingRecords = oaSupervisingRecordMapper.selectOaSupervisingRecordList(oaSupervisingRecord);
        for (OaSupervisingRecord supervisingRecord:oaSupervisingRecords) {
            if(supervisingRecord.getUserId() != null){
                //主办人名称
                supervisingRecord.setUserName(sysUserMapper.selectUserById(supervisingRecord.getUserId()).getNickName());
            }
            if(supervisingRecord.getDeptId() != null){
                //主办人部门
                supervisingRecord.setDeptName(sysDeptMapper.selectDeptById(supervisingRecord.getDeptId()).getDeptName());
            }
        }
        return oaSupervisingRecords;
    }

    /**
     * 新增办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    @Override
    public int insertOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord)
    {
        oaSupervisingRecord.setId(getRedisIncreID.getId());
        oaSupervisingRecord.setCreateTime(DateUtils.getNowDate());
        return oaSupervisingRecordMapper.insertOaSupervisingRecord(oaSupervisingRecord);
    }

    /**
     * 修改办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    @Override
    public int updateOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord)
    {
        oaSupervisingRecord.setUpdateTime(DateUtils.getNowDate());
        return oaSupervisingRecordMapper.updateOaSupervisingRecord(oaSupervisingRecord);
    }

    /**
     * 批量删除办理记录
     *
     * @param ids 需要删除的办理记录ID
     * @return 结果
     */
    @Override
    public int deleteOaSupervisingRecordByIds(String[] ids)
    {
        return oaSupervisingRecordMapper.deleteOaSupervisingRecordByIds(ids);
    }

    /**
     * 删除办理记录信息
     *
     * @param id 办理记录ID
     * @return 结果
     */
    @Override
    public int deleteOaSupervisingRecordById(String id)
    {
        return oaSupervisingRecordMapper.deleteOaSupervisingRecordById(id);
    }

}
