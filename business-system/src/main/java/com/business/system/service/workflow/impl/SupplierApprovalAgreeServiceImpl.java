package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaBid;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IBidApprovalService;
import com.business.system.service.workflow.ISupplierApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 供应商申请审批结果:审批通过
 */
@Service("supplierApprovalContent:processApproveResult:pass")
@Slf4j
public class SupplierApprovalAgreeServiceImpl extends ISupplierApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSupplier baSupplier = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        return result;
    }
}
