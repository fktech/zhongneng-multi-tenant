package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaCompanyGroup;
import com.business.system.domain.dto.BaEnterpriseDTO;

/**
 * 集团公司Service接口
 *
 * @author ljb
 * @date 2023-03-01
 */
public interface IBaCompanyGroupService
{
    /**
     * 查询集团公司
     *
     * @param id 集团公司ID
     * @return 集团公司
     */
    public BaCompanyGroup selectBaCompanyGroupById(String id);

    /**
     * 查询集团公司列表
     *
     * @param baCompanyGroup 集团公司
     * @return 集团公司集合
     */
    public List<BaCompanyGroup> selectBaCompanyGroupList(BaCompanyGroup baCompanyGroup);

    /**
     * 子公司数据
     * @param depId
     * @return
     */
    public List<BaCompanyGroup> subsidiaryList(String depId);


    /**
     * 新增集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    public int insertBaCompanyGroup(BaCompanyGroup baCompanyGroup);

    /**
     * 修改集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    public int updateBaCompanyGroup(BaCompanyGroup baCompanyGroup);

    /**
     * 批量删除集团公司
     *
     * @param ids 需要删除的集团公司ID
     * @return 结果
     */
    public int deleteBaCompanyGroupByIds(String[] ids);

    /**
     * 删除集团公司信息
     *
     * @param id 集团公司ID
     * @return 结果
     */
    public int deleteBaCompanyGroupById(String id);

    /**
     * 首页查询终端面板统计
     */
    public BaEnterpriseDTO companyGroupListStat(BaCompanyGroup baCompanyGroup);
}
