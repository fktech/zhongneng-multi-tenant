package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaContractForward;

/**
 * 合同结转Service接口
 *
 * @author ljb
 * @date 2023-03-06
 */
public interface IBaContractForwardService
{
    /**
     * 查询合同结转
     *
     * @param id 合同结转ID
     * @return 合同结转
     */
    public BaContractForward selectBaContractForwardById(String id);

    /**
     * 查询合同结转列表
     *
     * @param baContractForward 合同结转
     * @return 合同结转集合
     */
    public List<BaContractForward> selectBaContractForwardList(BaContractForward baContractForward);

    /**
     * 新增合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    public int insertBaContractForward(BaContractForward baContractForward);

    /**
     * 修改合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    public int updateBaContractForward(BaContractForward baContractForward);

    /**
     * 批量删除合同结转
     *
     * @param ids 需要删除的合同结转ID
     * @return 结果
     */
    public int deleteBaContractForwardByIds(String[] ids);

    /**
     * 删除合同结转信息
     *
     * @param id 合同结转ID
     * @return 结果
     */
    public int deleteBaContractForwardById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);





}
