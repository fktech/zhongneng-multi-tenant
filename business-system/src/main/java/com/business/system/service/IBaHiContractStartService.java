package com.business.system.service;

import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaHiContractStart;
import com.business.system.domain.BaHiTransport;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.dto.CountContractStartDTO;
import com.business.system.domain.vo.BaContractStartTonVO;
import com.business.system.domain.vo.BaContractStartVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 合同启动历史Service接口
 *
 * @author ljb
 * @date 2023-03-06
 */
public interface IBaHiContractStartService
{
    /**
     * 查询合同启动
     *
     * @param id 合同启动ID
     * @return 合同启动
     */
    public BaHiContractStart selectBaHiContractStartById(String id);

    /**
     * 查询合同启动列表
     *
     * @param baHiContractStart 合同启动
     * @return 合同启动集合
     */
    public List<BaHiContractStart> selectBaHiContractStartList(BaHiContractStart baHiContractStart);

    /**
     * 查询合同启动管理变更记录列表
     */
    List<BaHiContractStart> selectChangeDataBaHiContractStartList(BaHiContractStart baHiContractStart);
}
