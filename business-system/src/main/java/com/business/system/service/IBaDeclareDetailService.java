package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaDeclareDetail;

/**
 * 计划申报Service接口
 *
 * @author single
 * @date 2023-03-21
 */
public interface IBaDeclareDetailService
{
    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    public BaDeclareDetail selectBaDeclareDetailById(String id);

    /**
     * 查询计划申报列表
     *
     * @param baDeclareDetail 计划申报
     * @return 计划申报集合
     */
    public List<BaDeclareDetail> selectBaDeclareDetailList(BaDeclareDetail baDeclareDetail);

    /**
     * 新增计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    public int insertBaDeclareDetail(BaDeclareDetail baDeclareDetail);

    /**
     * 修改计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    public int updateBaDeclareDetail(BaDeclareDetail baDeclareDetail);

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareDetailByIds(String[] ids);

    /**
     * 删除计划申报信息
     *
     * @param id 计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareDetailById(String id);


}
