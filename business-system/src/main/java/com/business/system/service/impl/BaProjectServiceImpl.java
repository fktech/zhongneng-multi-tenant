package com.business.system.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysMenu;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.domain.vo.BaProjectVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 立项管理Service业务层处理
 *
 * @author ljb
 * @date 2022-12-02
 */
@Service
public class BaProjectServiceImpl extends ServiceImpl<BaProjectMapper, BaProject> implements IBaProjectService {

    private static final Logger log = LoggerFactory.getLogger(BaProjectServiceImpl.class);

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaHiProjectMapper baHiProjectMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private IBaEnterpriseService baEnterpriseService;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private IBaContractService iBaContractService;

    @Autowired
    private IBaGoodsService iBaGoodsService;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaPaymentVoucherMapper baPaymentVoucherMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaUserMailMapper baUserMailMapper;

    @Autowired
    private BaProjectLinkMapper baProjectLinkMapper;

    @Autowired
    private SysPostMapper sysPostMapper;

    @Autowired
    private IBaProjectService baProjectService;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private ISysMenuService iSysMenuService;

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private IBaProcessInstanceService iBaProcessInstanceService;

    @Autowired
    private BaCargoOwnerMapper baCargoOwnerMapper;

    @Autowired
    private IBaHiProjectService baHiProjectService;

    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    @Override
    public BaProject selectBaProjectById(String id) {
        BaProject baProject = baProjectMapper.selectBaProjectById(id);
        StringBuilder stringBuilder = new StringBuilder();
        if(!ObjectUtils.isEmpty(baProject)) {
            //供应商
            if (StringUtils.isNotEmpty(baProject.getSupplierId())) {
                String[] supplierIdArray = baProject.getSupplierId().split(",");
                for (String supplierId : supplierIdArray) {
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                    if (!ObjectUtils.isEmpty(baSupplier)) {
                        if (stringBuilder.length() == 0) {
                            stringBuilder.append(baSupplier.getName());
                        } else {
                            stringBuilder.append(",").append(baSupplier.getName());
                        }
                    }
                    baProject.setSupplierName(stringBuilder.toString());
                }
            }
            //用煤单位
            if (StringUtils.isNotEmpty(baProject.getEnterpriseId())) {
                BaEnterprise baEnterprise = baEnterpriseService.selectBaEnterpriseById(baProject.getEnterpriseId());
                baProject.setEnterpriseName(baEnterprise.getName());
                BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                if (!ObjectUtils.isEmpty(baCompanyGroup)) {
                    baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
                }
                //父级ID为空
//          if(StringUtils.isEmpty(baEnterprise.getParentId())){
                BaEnterprise baEnterprise1 = baEnterpriseService.selectBaEnterpriseById(baProject.getEnterpriseId());
                if (!ObjectUtils.isEmpty(baEnterprise1)) {
                    //最新指标信息
                    QueryWrapper<BaEnterprise> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("relation_id", baEnterprise1.getRelationId());
                    queryWrapper.eq("state", AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
                    queryWrapper.orderByDesc("create_time");
                    List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(queryWrapper);
                    if (baEnterpriseList.size() > 0) {
                        BaEnterprise enterprise = baEnterpriseService.selectBaEnterpriseById(baEnterpriseList.get(0).getId());
                        baEnterprise.setEnterpriseRelevance(enterprise.getEnterpriseRelevance());
                        baEnterprise.setId(enterprise.getId());
                    } else {
                        List<BaEnterpriseRelevance> baEnterpriseRelevances = baEnterprise1.getEnterpriseRelevance();
                        baEnterprise.setEnterpriseRelevance(baEnterpriseRelevances);
                    }
//            }
                }
                baProject.setBaEnterprise(baEnterprise);
            }
            //下游贸易商
            if (StringUtils.isNotEmpty(baProject.getDownstreamTraders())) {
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baProject.getDownstreamTraders());
                baProject.setDownstreamTradersName(baCompany.getName());
            }
            //实际发运人
            if (StringUtils.isNotEmpty(baProject.getCounterpartId())) {
                BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(baProject.getCounterpartId());
                if (StringUtils.isNotNull(baCounterpart))
                    baProject.setCounterpartName(baCounterpart.getName());
            }
            //仓库信息
            if (StringUtils.isNotEmpty(baProject.getDepotId())) {
                BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
                if (StringUtils.isNotNull(baDepot)) {
                    baProject.setDepotName(baDepot.getName());
                    baProject.setDepotCompany(baDepot.getCompany());
                }
            }
            //流程实例ID
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            List<String> processInstanceIds = new ArrayList<>();
            queryWrapper.eq("business_id", baProject.getId());
            queryWrapper.eq("flag", 0);
            //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
            queryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            baProject.setProcessInstanceId(processInstanceIds);

            //发起人
            SysUser sysUser = iSysUserService.selectUserById(baProject.getUserId());
            sysUser.setUserName(sysUser.getNickName());
            List<SysRole> roles = sysUser.getRoles();
            if (!CollectionUtils.isEmpty(roles)) {
                for (SysRole sysRole : roles) {
                    if ("yewuzhuli".equals(sysRole.getRoleKey())) {
                        baProject.setIsYewuzhuli("true");
                        break;
                    }
                }
            }
            //岗位名称
            String name = getName.getName(sysUser.getUserId());
            if (name.equals("") == false) {
                sysUser.setPostName(name.substring(0, name.length() - 1));
            }
            baProject.setUser(sysUser);
            //创建人对应公司
            if (sysUser.getDeptId() != null) {
                SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                for (String depId:split) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                    if(StringUtils.isNotNull(dept1)){
                        if(dept1.getDeptType().equals("1")){
                            baProject.setComName(dept1.getDeptName());
                            break;
                        }
                    }
                }
            }

            //品名
            if ("4".equals(baProject.getProjectType())) { //判断是否是常规立项
                if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                    QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                    if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                        List<String> goodsIdList = Arrays.asList(baProject.getGoodsId().split(","));
                        baGoodsQueryWrapper.in("id", goodsIdList);
                        baGoodsQueryWrapper.eq("flag", 0);
                        List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                        //绑定商品对象
                        baProject.setGoodsList(goodsList);
                        if (!CollectionUtils.isEmpty(goodsList)) {
                            StringBuilder goodName = new StringBuilder();
                            for (BaGoods baGoods : goodsList) {
                                if (goodName.length() > 0) {
                                    goodName.append(",").append(baGoods.getName());
                                } else {
                                    goodName.append(baGoods.getName());
                                }
                            }
                            baProject.setGoodsName(goodName.toString());
                        }
                    }
                }
            } else {
                if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                    if (!ObjectUtils.isEmpty(baGoods)) {
                        baProject.setGoodsTypeName(baGoods.getName());
                        baProject.setGoodsName(baGoods.getName());
                    } else {
                        if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                            BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                            if (!ObjectUtils.isEmpty(baGoodsType)) {
                                baProject.setGoodsTypeName(baGoodsType.getName());
                                baProject.setGoodsName(baGoodsType.getName());
                            }
                        }
                    }
                }
            }

            //商品分类
            if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                if (!ObjectUtils.isEmpty(baGoodsType)) {
                    baProject.setGoodsTypeName(baGoodsType.getName());
                }
            }

            //业务经理名称
            if (StringUtils.isNotEmpty(baProject.getServiceManager())) {
                if (baProject.getServiceManager().indexOf(",") != -1) {
                    String[] serviceManagerArray = baProject.getServiceManager().split(",");
                    String serviceManagerStr = serviceManagerArray[serviceManagerArray.length - 1];
                    if (StringUtils.isNotEmpty(serviceManagerStr)) {
                        SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(serviceManagerStr));
                  /* if(!ObjectUtils.isEmpty(sysUser1)){
                       List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(sysUser1.getUserName());
                       StringBuilder sysPostName = new StringBuilder();
                       if(!CollectionUtils.isEmpty(sysPosts)){
                           for(SysPost sysPost : sysPosts){
                               if(sysPostName.length() > 0){
                                   sysPostName.append(",").append(sysPost.getPostName());
                               } else {
                                   sysPostName.append(sysPost.getPostName());
                               }
                           }
                       }
                       if(StringUtils.isNotEmpty(sysPostName)){
                           sysUser1.setNickName(sysUser1.getNickName() + "-" + sysPostName);
                       }
                   }*/
                        baProject.setServiceManagerName(sysUser1.getNickName());
                    }
                }
            }

            //现场人员名称
        /*StringBuilder stringBuilder = new StringBuilder();
        String personScene = baProject.getPersonScene();
        if(StringUtils.isNotEmpty(personScene)){
            String[] personSceneArray = personScene.trim().split(",");
            if(!ObjectUtils.isEmpty(personSceneArray)){
                for(String ps : personSceneArray){
                    if(stringBuilder.length() > 0){
                        stringBuilder.append(",").append(sysUserMapper.selectUserById(Long.valueOf(ps)).getNickName());
                    } else {
                        stringBuilder.append(sysUserMapper.selectUserById(Long.valueOf(ps)).getNickName());
                    }
                }
            }
        }
        baProject.setPersonSceneName(stringBuilder.toString());*/
            //运营责任人名称
            if (StringUtils.isNotEmpty(baProject.getProjectType())) {
                if (baProject.getProjectType().equals("4") == false && baProject.getProjectType().equals("2") == false) {
                    if (StringUtils.isNotEmpty(baProject.getPersonCharge())) {
                        if (StringUtils.isNotEmpty(baProject.getPersonCharge())) {
                            String[] personChargeArray = baProject.getPersonCharge().split(",");
                            String personChargeStr = personChargeArray[personChargeArray.length - 1];
                            if (StringUtils.isNotEmpty(personChargeStr)) {
                                baProject.setPersonChargeName(sysUserMapper.selectUserById(Long.valueOf(personChargeStr)).getNickName());
                            }
                        }
                    }
                }
            }
            //终端内部对接人
            if (StringUtils.isNotEmpty(baProject.getInternalContactPerson())) {
                BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baProject.getInternalContactPerson());
                baProject.setInternalContactPersonName(baUserMail.getName());
            }
            //下游内部对接人名称
            if (StringUtils.isNotEmpty(baProject.getLowInternalContactPerson())) {
                BaUserMail baUser = baUserMailMapper.selectBaUserMailById(baProject.getLowInternalContactPerson());
                baProject.setLowInternalContactPersonName(baUser.getName());
            }
            //收费标准
            if (StringUtils.isNotEmpty(baProject.getStandardsId())) {
                BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baProject.getStandardsId());
                baProject.setStandards(baChargingStandards);
            }
            //资金计划
            QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
            declareQueryWrapper.eq("start_id", baProject.getId());
            List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);
            baProject.setBaDeclares(baDeclares);
            //现场负责人名称
        /*if(StringUtils.isNotEmpty(baProject.getPersonSite())) {
            baProject.setPersonSiteName(sysUserMapper.selectUserById(Long.valueOf(baProject.getPersonSite())).getNickName());
        }*/
            //业务专员名称
            if (StringUtils.isNotEmpty(baProject.getSalesman())) {
                if (StringUtils.isNotEmpty(baProject.getSalesman())) {
                    String salesmanStr = "";
                    StringBuilder salesmanStringBuilder = new StringBuilder();
                    String[] salesmanArray = baProject.getSalesman().split(";");
                    for (String s : salesmanArray) {
                        if (StringUtils.isNotEmpty(s)) {
                            String[] salesmanSplit = s.split(",");
                            salesmanStr = salesmanSplit[salesmanSplit.length - 1];
                            if (StringUtils.isNotEmpty(salesmanStr)) {
                                SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(salesmanStr));
                                List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(sysUser1.getUserName(), SecurityUtils.getCurrComId());
                                StringBuilder sysPostName = new StringBuilder();
                                if (!CollectionUtils.isEmpty(sysPosts)) {
                                    for (SysPost sysPost : sysPosts) {
                                        if (sysPostName.length() > 0) {
                                            sysPostName.append(",").append(sysPost.getPostName());
                                        } else {
                                            sysPostName.append(sysPost.getPostName());
                                        }
                                    }
                                }
                                if (StringUtils.isNotEmpty(sysPostName)) {
                                    sysUser1.setNickName(sysUser1.getNickName() + "-" + sysPostName);
                                    if (salesmanStringBuilder.length() > 0) {
                                        salesmanStringBuilder.append(",").append(sysUser1.getNickName());
                                    } else {
                                        salesmanStringBuilder.append(sysUser1.getNickName());
                                    }
                                }
                            }
                        }
                    }
                    baProject.setSalesmanName(salesmanStringBuilder.toString());
                }
            }
            //子公司
            if (StringUtils.isNotEmpty(baProject.getSubsidiary())) {
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baProject.getSubsidiary()));
                baProject.setSubsidiaryName(dept.getDeptName());
                baProject.setSubsidiaryAbbreviation(dept.getAbbreviation());
            }
            //运营专员
            if (StringUtils.isNotEmpty(baProject.getPersonCharge())) {
                String[] split = baProject.getPersonCharge().split(",");
                String userName = "";
                for (String userId : split) {
                    if (this.isNumeric(userId) == true) {
                        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                        if (StringUtils.isNotNull(user)) {
                            userName = user.getNickName() + "," + userName;
                        }
                    }
                }
                if (StringUtils.isNotEmpty(userName) && !"".equals(userName)) {
                    baProject.setPersonChargeName(userName.substring(0, userName.length() - 1));
                } else {
                    baProject.setPersonChargeName(baProject.getPersonCharge());
                }
            }
            //上游公司
            if (StringUtils.isNotEmpty(baProject.getUpstreamCompany())) {
                //查询供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProject.getUpstreamCompany());
                if (StringUtils.isNotNull(baSupplier)) {
                    baProject.setUpstreamCompanyName(baSupplier.getName());
                }
            }
            //主体公司
            if (StringUtils.isNotEmpty(baProject.getSubjectCompany())) {
                //子公司
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baProject.getSubjectCompany()));
                if (!ObjectUtils.isEmpty(sysDept)) {
                    baProject.setSubjectCompanyName(sysDept.getDeptName());
                }
            }
            //终端公司
            if (StringUtils.isNotEmpty(baProject.getTerminalCompany())) {
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject.getTerminalCompany());
                if (StringUtils.isNotNull(enterprise)) {
                    baProject.setTerminalCompanyName(enterprise.getName());
                }
            }
            //下游公司
            if (StringUtils.isNotEmpty(baProject.getDownstreamCompany())) {
                //下游公司名称
                String downstreamCompanyName = "";
                String[] split = baProject.getDownstreamCompany().split(",");
                for (String downstream : split) {
                    String[] split1 = downstream.split(";");
                    //供应商
                    if ("1".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(key);
                        downstreamCompanyName = split1[0] + ";" + baSupplier.getName() + "," + downstreamCompanyName;
                    }
                    //终端企业
                    if ("2".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(key);
                        downstreamCompanyName = split1[0] + ";" + enterprise.getName() + "," + downstreamCompanyName;
                    }
                    //公司
                    if (!"1".equals(split1[0]) && !"2".equals(split1[0]) && !"6".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                        downstreamCompanyName = split1[0] + ";" + baCompany.getName() + "," + downstreamCompanyName;
                    }
                    //集团公司
                    if ("6".equals(split1[0])) {
                        String key = split1[split1.length - 1];
                        BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(key);
                        downstreamCompanyName = split1[0] + ";" + companyGroup.getCompanyName() + "," + downstreamCompanyName;
                    }
                }
                if (",".equals(downstreamCompanyName.substring(downstreamCompanyName.length() - 1))) {
                    baProject.setDownstreamCompanyName(downstreamCompanyName.substring(0, downstreamCompanyName.length() - 1));
                } else {
                    baProject.setDownstreamCompanyName(downstreamCompanyName);
                }
            }
            //货主
            if (StringUtils.isNotEmpty(baProject.getCustomId())) {
                BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(baProject.getCustomId());
                if (!ObjectUtils.isEmpty(baCargoOwner)) {
                    baProject.setCustomName(baCargoOwner.getName());
                }
            }
            //合同信息
            BaContract baContract = new BaContract();
            baContract.setStorageId(id);
            baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
            List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
            baProject.setBaContractList(baContracts);
        }
        return baProject;
    }

    //判断String是否能转数字
    public static boolean isNumeric(String str) {
        try {
            Long.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 查询立项管理列表
     *
     * @param baProject 立项管理
     * @return 立项管理
     */
    @Override
    @DataScope(deptAlias = "pj", userAlias = "pj")
    public List<BaProject>  selectBaProjectList(BaProject baProject) {
        //查询业务状态为未立项
        //baProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
        //baProject.setDeptId(SecurityUtils.getDeptId());
        List<BaProject> baProjects = baProjectMapper.selectProject(baProject);
        /*if(StringUtils.isNotEmpty(baProject.getState())){
            if(baProject.getState().equals("all") || baProject.getState().equals("projectStatus:pass")){
                //授权查询
                QueryWrapper<BaProjectLink> queryWrapper = new QueryWrapper<>();
                queryWrapper.like("dept_id",SecurityUtils.getDeptId().toString());
                if(StringUtils.isNotEmpty(baProject.getProjectType())){
                    queryWrapper.eq("project_type", baProject.getProjectType());
                }
                List<BaProjectLink> baProjectLinks = baProjectLinkMapper.selectList(queryWrapper);
                for (BaProjectLink baProjectLink1:baProjectLinks) {
                    //查询立项
                    //BaProject addBaProject = baProjectMapper.selectBaProjectById(baProjectLink1.getProjectId());
                    QueryWrapper<BaProject> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("id",baProjectLink1.getProjectId());
                    if(StringUtils.isNotEmpty(baProject.getProjectState())){
                        queryWrapper1.eq("project_state",baProject.getProjectState());
                    }
                    BaProject addBaProject = baProjectMapper.selectOne(queryWrapper1);
                    if(!ObjectUtils.isEmpty(addBaProject)){
                        addBaProject.setEmpower("1");
                        baProjects.add(addBaProject);
                    }
                }

            }
        }*/
        /*if(StringUtils.isNotEmpty(baProject.getState())){
            if(baProject.getState().equals("all") || baProject.getState().equals("projectStatus:pass")){
                //授权查询
                SecurityUtils.getUserId();
                QueryWrapper<BaProjectLink> queryWrapper = new QueryWrapper<>();
                //sysUserRoleMapper.selectforrolid()
                SysRolesByUserDTO sysUserRoleDTO=new SysRolesByUserDTO();
                sysUserRoleDTO.setUserId(SecurityUtils.getUserId().toString());
                List<String> list = sysUserMapper.selectRolesByUser(sysUserRoleDTO);
                for (String roleid:list){
                    //查询
                }
                //查询登录人的角色
                for (String roleId:list){
                    SysRole sysRole = sysRoleMapper.selectRoleById(Long.valueOf(roleId));
                    //判断是不是运营专员；
                    boolean contains = sysRole.getRoleName().contains("运营专员");
                    if(contains){
                        SysDeptRoleDTO sysDeptRoleDTO=new SysDeptRoleDTO();
                        sysDeptRoleDTO.setRoleId(roleId);
                        List<SysDept> sysDepts = sysUserMapper.selectDeptsByRoleId(sysDeptRoleDTO);
                        List deptIds=new ArrayList<>();
                        for (SysDept sysDept:sysDepts){
                            deptIds.add(sysDept.getDeptId());
                        }

                        queryWrapper.in("dept_id",deptIds);

                    }else {
                        queryWrapper.like("dept_id",SecurityUtils.getDeptId().toString());
                    }
                    if(StringUtils.isNotEmpty(baProject.getProjectType())){
                        queryWrapper.eq("project_type", baProject.getProjectType());

                    }
                    List<BaProjectLink> baProjectLinks = baProjectLinkMapper.selectList(queryWrapper);
                    for (BaProjectLink baProjectLink1:baProjectLinks) {
                        //查询立项
                        //BaProject addBaProject = baProjectMapper.selectBaProjectById(baProjectLink1.getProjectId());
                        QueryWrapper<BaProject> queryWrapper1 = new QueryWrapper<>();
                        queryWrapper1.eq("id",baProjectLink1.getProjectId());
                        if(StringUtils.isNotEmpty(baProject.getProjectState())){
                            queryWrapper1.eq("project_state",baProject.getProjectState());
                        }
                        if(StringUtils.isNotEmpty(baProject.getProjectType())){
                            queryWrapper1.eq("project_type", baProject.getProjectType());

                        }
                        BaProject addBaProject = baProjectMapper.selectOne(queryWrapper1);
                        if(!ObjectUtils.isEmpty(addBaProject)){
                            addBaProject.setEmpower("1");
                            baProjects.add(addBaProject);
                        }
                    }
                }

            }
        }*/
        //立项集合去重
        //List<BaProject> collect = baProjects.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaProject::getId))), ArrayList::new));


        if (baProjects.size() > 0){
            baProjects.stream().forEach(item ->{
                //供应商
                StringBuilder stringBuilder = new StringBuilder();
                //供应商
                if (StringUtils.isNotEmpty(item.getSupplierId())) {
                    String[] supplierIdArray = item.getSupplierId().split(",");
                    for(String supplierId : supplierIdArray){
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                        if(!ObjectUtils.isEmpty(baSupplier)){
                            if(stringBuilder.length() == 0){
                                stringBuilder.append(baSupplier.getName());
                            } else {
                                stringBuilder.append(",").append(baSupplier.getName());
                            }
                        }
                        item.setSupplierName(stringBuilder.toString());
                    }
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProject.getSupplierId());
                    item.setSupplierName(baSupplier.getName());
                }
                //下游贸易商
                if (StringUtils.isNotEmpty(item.getDownstreamTraders())) {
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getDownstreamTraders());
                    item.setDownstreamTradersName(baCompany.getName());
                }
                //用煤单位
                if (StringUtils.isNotEmpty(item.getEnterpriseId())) {
                    BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getEnterpriseId());
                    item.setEnterpriseName(baEnterprise.getName());
                    BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                    if(!ObjectUtils.isEmpty(baCompanyGroup)){
                        baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
                        baEnterprise.setBaCompanyGroup(baCompanyGroup);
                    }
                    item.setBaEnterprise(baEnterprise);
                    item.setSpliceName(baEnterprise.getName());
                }
                //实际发起人
                if (StringUtils.isNotEmpty(item.getCounterpartId())) {
                    BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(item.getCounterpartId());
                    item.setCounterpartName(baCounterpart.getName());
                    item.setSpliceName(baCounterpart.getName());
                }
                //发起人
                SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());


                //拼接归属部门
                if (sysUser.getDeptId() != null) {
                    SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                    //查询祖籍
                    String[] split = dept.getAncestors().split(",");
                    List<Long> integerList = new ArrayList<>();
                    for (String deptId:split) {
                        integerList.add(Long.valueOf(deptId));
                    }
                    //倒叙拿到第一个公司
                    Collections.reverse(integerList);
                    for (Long itm:integerList) {
                        SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                        if(StringUtils.isNotNull(dept1)){
                            if(dept1.getDeptType().equals("1")){
                                sysUser.setPartDeptCompany(dept1.getDeptName());
                                break;
                            }
                        }
                    }
                    sysUser.setPartDeptName(sysUser.getPartDeptCompany()+"-"+dept.getDeptName());
                }
                item.setUser(sysUser);

                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUser.getNickName());
                }
                //仓库信息
                if(StringUtils.isNotEmpty(item.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(item.getDepotId());
                    if(StringUtils.isNotNull(baDepot)){
                        item.setDepotName(baDepot.getName());
                    }
                }

                //商品信息
                /*BaGoods baGoods = baGoodsMapper.selectBaGoodsById(item.getGoodsId());
                if (!ObjectUtils.isEmpty(baGoods)) {
                    item.setGoodsName(baGoods.getName());
                }*/
                if("4".equals(item.getProjectType())) { //判断是否是常规立项
                    if (StringUtils.isNotEmpty(item.getGoodsId())) {
                        QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                        if(StringUtils.isNotEmpty(item.getGoodsId())){
                            List<String> goodsIdList = Arrays.asList(item.getGoodsId().split(","));
                            baGoodsQueryWrapper.in("id", goodsIdList);
                            baGoodsQueryWrapper.eq("flag", 0);
                            List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                            //绑定商品对象
                            item.setGoodsList(goodsList);
                            if(!CollectionUtils.isEmpty(goodsList)){
                                StringBuilder goodName = new StringBuilder();
                                for(BaGoods baGoods : goodsList){
                                    if(goodName.length() > 0){
                                        goodName.append(",").append(baGoods.getName());
                                    } else {
                                        goodName.append(baGoods.getName());
                                    }
                                }
                                item.setGoodsName(goodName.toString());
                            }
                        }
                    }
                } else {
                    if (StringUtils.isNotEmpty(item.getGoodsType())) {
                        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(item.getGoodsType());
                        if (!ObjectUtils.isEmpty(baGoods)) {
                            item.setGoodsName(baGoods.getName());
                        } else {
                            if(StringUtils.isNotEmpty(item.getGoodsType())){
                                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(item.getGoodsType());
                                if(!ObjectUtils.isEmpty(baGoodsType)){
                                    item.setGoodsName(baGoodsType.getName());
                                }
                            }
                        }
                    }
                }
                //收费标准
                if(StringUtils.isNotEmpty(item.getStandardsId())){
                    BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(item.getStandardsId());
                    item.setStandards(baChargingStandards);
                }
                //子公司
                if(StringUtils.isNotEmpty(item.getSubsidiary())){
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubsidiary()));
                    item.setSubsidiaryName(dept.getDeptName());
                    item.setSubsidiaryAbbreviation(dept.getAbbreviation());
                }
                List<String> processInstanceIds = new ArrayList<>();
                //流程实例ID
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", item.getId());
                queryWrapper.eq("flag", 0);
                //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
                queryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                }
                item.setProcessInstanceId(processInstanceIds);

                //主体公司
                if(StringUtils.isNotEmpty(item.getSubjectCompany())){
                    //子公司
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubjectCompany()));
                    if(!ObjectUtils.isEmpty(sysDept)){
                        item.setSubjectCompanyName(sysDept.getDeptName());
                    }
                }
                //货主
                if(StringUtils.isNotEmpty(item.getCustomId())){
                    BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(item.getCustomId());
                    if(!ObjectUtils.isEmpty(baCargoOwner)){
                        item.setCustomName(baCargoOwner.getName());
                    }
                }
            });
        }

        return baProjects;
    }



    @Override
    @DataScope(deptAlias = "pj", userAlias = "pj")
    public List<BaProject> baProjectList(BaProject baProject) {
        return baProjectMapper.selectBaProjectList(baProject);
    }

    @Override
    public List<BaProject> selectBaProjectList1(BaProject baProject) {
        return baProjectMapper.selectBaProjectList1(baProject);
    }

    /**
     * 草稿箱
     *
     * @param baProject
     * @return
     */
    @Override
    public List<BaProject> selectDraft(BaProject baProject) {
        QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_state", AdminCodeEnum.PROJECT_DRAFT.getCode());
        queryWrapper.eq("flag", 0);
        //项目名称查询
        if (StringUtils.isNotEmpty(baProject.getName())) {
            queryWrapper.like("name", baProject.getName());
        }
        if(StringUtils.isNotEmpty(baProject.getProjectType())){
            queryWrapper.like("project_type", baProject.getProjectType());
        }
        //时间段查询
        if (StringUtils.isNotEmpty(baProject.getStart())) {
            queryWrapper.ge("create_time", baProject.getStart());
        }
        if (StringUtils.isNotEmpty(baProject.getEnd())) {
            queryWrapper.le("create_time", baProject.getEnd());
        }
        //供应商查询
        if (StringUtils.isNotEmpty(baProject.getSupplierId())) {
            queryWrapper.eq("supplier_id", baProject.getSupplierId());
        }
        //个人数据查询
        if(baProject.getUserId() != null){
            queryWrapper.eq("user_id", baProject.getUserId());
        }
        if(StringUtils.isNotEmpty(baProject.getTenantId())){
            queryWrapper.eq("tenant_id",baProject.getTenantId());
        }
        queryWrapper.orderByDesc("create_time");
        List<BaProject> baProjects = baProjectMapper.selectList(queryWrapper);
        if (StringUtils.isNotNull(baProjects))
            for (BaProject baProject1 : baProjects) {
                //供应商
                StringBuilder stringBuilder = new StringBuilder();
                //供应商
                if (StringUtils.isNotEmpty(baProject.getSupplierId())) {
                    String[] supplierIdArray = baProject.getSupplierId().split(",");
                    for(String supplierId : supplierIdArray){
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                        if(!ObjectUtils.isEmpty(baSupplier)){
                            if(stringBuilder.length() == 0){
                                stringBuilder.append(baSupplier.getName());
                            } else {
                                stringBuilder.append(",").append(baSupplier.getName());
                            }
                        }
                        baProject.setSupplierName(stringBuilder.toString());
                    }
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProject.getSupplierId());
                    baProject.setSupplierName(baSupplier.getName());
                }
                //用煤单位
                if (StringUtils.isNotEmpty(baProject1.getEnterpriseId())) {
                    BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject1.getEnterpriseId());
                    baProject1.setEnterpriseName(baEnterprise.getName());
                    BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                    if(!ObjectUtils.isEmpty(baCompanyGroup)){
                        baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
                        baEnterprise.setBaCompanyGroup(baCompanyGroup);
                    }
                    baProject1.setBaEnterprise(baEnterprise);
                }
                //实际发起人
                if (StringUtils.isNotEmpty(baProject1.getCounterpartId())) {
                    BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(baProject1.getCounterpartId());
                    baProject1.setCounterpartName(baCounterpart.getName());
                }
                //发起人
                SysUser sysUser = sysUserMapper.selectUserById(baProject1.getUserId());
                baProject1.setUserName(sysUser.getNickName());
            }
        return baProjects;
    }

    /**
     * 新增立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    @Override
    public int insertBaProject(BaProject baProject) {
        //项目简称不能重复
        QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name_shorter", baProject.getNameShorter());
        queryWrapper.eq("flag", 0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaProject baProject1 = baProjectMapper.selectOne(queryWrapper);
        if (StringUtils.isNotNull(baProject1)) {
            return -1;
        }
        //项目名称不能重复
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", baProject.getName());
        queryWrapper.eq("flag", 0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaProject project = baProjectMapper.selectOne(queryWrapper);
        if (StringUtils.isNotNull(project)) {
            return -2;
        }
        baProject.setId(getRedisIncreID.getId());
        //默认立项状态
        //baProject.setProjectState(AdminCodeEnum.PROJECT_DRAFT.getCode());
        //默认立项状态为已立项
        baProject.setProjectState(AdminCodeEnum.PROJECT_UNDER_WAY.getCode());
        //默认审批状态为已通过
        baProject.setState(AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
        //审批通过时间
        baProject.setPassTime(DateUtils.getNowDate());
        baProject.setCreateTime(DateUtils.getNowDate());
        baProject.setCreateBy(SecurityUtils.getUsername());
        baProject.setUserId(SecurityUtils.getUserId());
        baProject.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        //流程发起状态
        baProject.setWorkState("1");
        //判断项目序号是否已存在
        if(StringUtils.isNotEmpty(baProject.getProjectSerial())){
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("project_serial", baProject.getProjectSerial());
            queryWrapper.eq("flag", 0);
            BaProject projectSerial = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(projectSerial)) {
                String num = this.projectSerial();
                //生成新的项目序号
                baProject.setProjectSerial(num);
            }
        }
        //收费标准
        if(StringUtils.isNotNull(baProject.getStandards())){
            if(StringUtils.isNotEmpty(baProject.getStandards().getId())){
                baChargingStandardsMapper.updateBaChargingStandards(baProject.getStandards());
            }else {
                BaChargingStandards standards = baProject.getStandards();
                standards.setId(getRedisIncreID.getId());
                standards.setCreateTime(DateUtils.getNowDate());
                standards.setCreateBy(SecurityUtils.getUsername());
                baChargingStandardsMapper.insertBaChargingStandards(standards);
                baProject.setStandardsId(standards.getId());
            }
        }
        //资金计划
        if(!CollectionUtils.isEmpty(baProject.getBaDeclares())){
            for (BaDeclare baDeclare:baProject.getBaDeclares()) {
                baDeclare.setId(getRedisIncreID.getId());
                baDeclare.setStartId(baProject.getId());
                baDeclare.setSource(1);
                baDeclare.setFlag(1L);
                baDeclare.setCreateTime(DateUtils.getNowDate());
                baDeclare.setCreateBy(SecurityUtils.getUsername());
                baDeclare.setUserId(SecurityUtils.getUserId());
                baDeclare.setDeptId(SecurityUtils.getDeptId());
                //判断资金计划是否重复
                BaDeclare baDeclare1  = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                if(StringUtils.isNotNull(baDeclare1)){
                    return -1;
                }
                baDeclareMapper.insertBaDeclare(baDeclare);
            }
        }

       /* //查询所有菜单数据
        List<SysMenu> sysMenus1 = menuMapper.menuList(new SysMenu());

        //定义角色菜单权限
        List<SysRoleMenu> roleMenuList = new ArrayList<>();
        //立项审批通过新增菜单
        SysMenu menu = new SysMenu();
        menu.setMenuId(sysMenus1.get(0).getMenuId()+1);
        menu.setComponent("system/ongoing/ongoing");
        menu.setMenuName(baProject.getName());
        menu.setIsCache("0");
        menu.setIsFrame("1");
        menu.setMark("0");
        menu.setMenuType("C");
        menu.setOrderNum(1);
        menu.setParentId(2668L);
        menu.setQuery("{"+" \"id\" "+":"+"\"" + baProject.getId() + "\""+"}");
        menu.setPath(baProject.getNameShorter());
        menu.setIcon("xiangmuyuhetong");
        iSysMenuService.insertMenu(menu);
        //权限绑定
        List<SysRoleMenu> sysRoleMenus = roleMenuMapper.sysRoleMenuList(2668L);
        for (SysRoleMenu sysRoleMenu1:sysRoleMenus) {
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setMenuId(menu.getMenuId());
            sysRoleMenu.setRoleId(sysRoleMenu1.getRoleId());
            roleMenuList.add(sysRoleMenu);
        }
        roleMenuMapper.batchRoleMenu(roleMenuList);*/
        //修改仓库选用状态
        if(StringUtils.isNotEmpty(baProject.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
            baDepot.setIsSelected(baProject.getId());
            baDepotMapper.updateBaDepot(baDepot);
        }
        //拼接归属部门
        if (baProject.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(baProject.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            List<Long> integerList = new ArrayList<>();
            for (String deptId:split) {
                integerList.add(Long.valueOf(deptId));
            }
            //倒叙拿到第一个公司
            Collections.reverse(integerList);
            String deptName = "";
            for (Long itm:integerList) {
                SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        deptName = dept1.getDeptName();
                        break;
                    }
                }
            }
            baProject.setPartDeptName(deptName + "-" + dept.getDeptName());
        }
        return baProjectMapper.insertBaProject(baProject);
    }

    @Override
    public int submit(BaProject baProject) {

        //获取当前登录人角色
        List<SysRole> sysRoles = sysRoleMapper.selectRolesListByUserId(SecurityUtils.getUserId());
        boolean isBusinessAssistant = false; //判断当前登陆人是否是业务助理角色
        if(!CollectionUtils.isEmpty(sysRoles)){
            for(SysRole sysRole : sysRoles){
                //业务总监助理
                if(sysRole.getRoleId() == 21){
                    isBusinessAssistant = true;
                    break;
                }
            }
        }
        if (StringUtils.isNotEmpty(baProject.getId())) {
            //baProjectMapper.updateBaProject(baProject);
            BaProject baProject1 = baProjectMapper.selectById(baProject.getId());
            //判断立项编简称是否相等
            if (StringUtils.isNotEmpty(baProject1.getNameShorter()) && baProject1.getNameShorter().equals(baProject.getNameShorter()) == false) {
                QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("name_shorter", baProject.getNameShorter());
                queryWrapper.eq("flag", 0);
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
                }
                BaProject project = baProjectMapper.selectOne(queryWrapper);
                if (StringUtils.isNotNull(project)) {
                    return -1;
                }
            }
            //判断立项名称是否相等
            if (StringUtils.isNotEmpty(baProject1.getName()) && baProject1.getName().equals(baProject.getName()) == false) {
                QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("name", baProject.getName());
                queryWrapper.eq("flag", 0);
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
                }
                BaProject project = baProjectMapper.selectOne(queryWrapper);
                if (StringUtils.isNotNull(project)) {
                    return -2;
                }
            }
            //收费标准
            if(StringUtils.isNotNull(baProject.getStandards())){
                if(StringUtils.isNotEmpty(baProject.getStandards().getId())){
                    baChargingStandardsMapper.updateBaChargingStandards(baProject.getStandards());
                }else {
                    BaChargingStandards standards = baProject.getStandards();
                    standards.setId(getRedisIncreID.getId());
                    standards.setCreateTime(DateUtils.getNowDate());
                    standards.setCreateBy(SecurityUtils.getUsername());
                    baChargingStandardsMapper.insertBaChargingStandards(standards);
                    baProject.setStandardsId(standards.getId());
                }
            }
            if(StringUtils.isNotEmpty(baProject.getSalesman())) {
                if(StringUtils.isNotEmpty(baProject.getSalesman())){
                    String salesmanStr = "";
                    StringBuilder salesmanStringBuilder = new StringBuilder();
                    String[] salesmanArray = baProject.getSalesman().split(";");
                    for(String s : salesmanArray){
                        if(StringUtils.isNotEmpty(s)){
                            String[] salesmanSplit = s.split(",");
                            salesmanStr = salesmanSplit[salesmanSplit.length-1];
                            if(StringUtils.isNotEmpty(salesmanStr)){
                                String nickName = sysUserMapper.selectUserById(Long.valueOf(salesmanStr)).getNickName();
                                if(salesmanStringBuilder.length() > 0){
                                    salesmanStringBuilder.append(",").append(nickName);
                                } else {
                                    salesmanStringBuilder.append(nickName);
                                }
                            }
                        }
                    }
                    baProject.setSalesmanName(salesmanStringBuilder.toString());
                }
            }
            if(!baProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode()) && !baProject.getState().equals(AdminCodeEnum.PROJEC_BUSINESS_STATUS_PASS.getCode())){
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", baProject.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }

                //业务助理发起流程
                String flowId = "";
                if(isBusinessAssistant){
                    flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode(),SecurityUtils.getCurrComId());
                } else {
                    flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(),SecurityUtils.getCurrComId());
                }
                baProject.setFlowId(flowId);
                baProject.setSubmitTime(DateUtils.getNowDate());
                //审批状态
                baProject.setState(AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
                baProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
            }
            //拼接归属部门
            if (baProject.getDeptId() != null) {
                SysDept dept = sysDeptMapper.selectDeptById(baProject.getDeptId());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                List<Integer> integerList = new ArrayList<>();
                for (String deptId:split) {
                    integerList.add(Integer.parseInt(deptId));
                }
                //倒叙拿到第一个公司
                Collections.reverse(integerList);
                String deptName = "";
                for (Integer itm:integerList) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(itm));
                    if(StringUtils.isNotNull(dept1)){
                        if(dept1.getDeptType().equals("1")){
                            deptName = dept1.getDeptName();
                            break;
                        }
                    }
                }
                baProject.setPartDeptName(deptName + "-" + dept.getDeptName());
            }
            int result = baProjectMapper.updateBaProject(baProject);
            if (result > 0 && !baProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode()) && !baProject.getState().equals(AdminCodeEnum.PROJEC_BUSINESS_STATUS_PASS.getCode())) {
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baProject, AdminCodeEnum.PROJECT_APPROVAL_CONTENT_UPDATE.getCode(), isBusinessAssistant);
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baProject1.setState(AdminCodeEnum.PROJECT_STATUS_WITHDRAW.getCode());
                    baProjectMapper.updateBaProject(baProject1);
                    return 0;
                } else {
                    //修改仓库选用状态
                    if (StringUtils.isNotEmpty(baProject1.getDepotId())) {
                        BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject1.getDepotId());
                        baDepot.setIsSelected(baProject1.getId());
                        baDepotMapper.updateBaDepot(baDepot);
                    }
                }
            }
        } else {
            //立项简称不能重复
            QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name_shorter", baProject.getNameShorter());
            queryWrapper.eq("flag", 0);
            BaProject project = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project)) {
                return -1;
            }
            //项目名称不能重复
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", baProject.getName());
            queryWrapper.eq("flag", 0);
            BaProject project1 = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project1)) {
                return -2;
            }
            baProject.setId(getRedisIncreID.getId());
            //默认立项状态
            baProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
            baProject.setCreateTime(DateUtils.getNowDate());
            baProject.setCreateBy(SecurityUtils.getUsername());
            baProject.setUserId(SecurityUtils.getUserId());
            baProject.setDeptId(SecurityUtils.getDeptId());
            //租户ID
            baProject.setTenantId(SecurityUtils.getCurrComId());
            //立项编号
            String conversion = this.nameConversion(baProject.getName());
            //查询名称转换是否存在
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name_conversion", conversion);
            queryWrapper.eq("flag", 0);
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.orderByDesc("create_time");
            List<BaProject> baProjects = baProjectMapper.selectList(queryWrapper);
            if(baProjects.size() > 0){
                Integer serialNumber = baProjects.get(0).getSerialNumber();
                baProject.setSerialNumber(serialNumber+1);
            }else {
                baProject.setSerialNumber(1);
            }
            //名称转换
            baProject.setNameConversion(conversion);
            //全局编号
            baProject.setGlobalNumber(conversion+baProject.getSerialNumber());
            //判断项目序号是否已存在
            if(StringUtils.isNotEmpty(baProject.getProjectSerial())){
                queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("project_serial", baProject.getProjectSerial());
                queryWrapper.eq("flag", 0);
                BaProject projectSerial = baProjectMapper.selectOne(queryWrapper);
                if (StringUtils.isNotNull(projectSerial)) {
                    String num = this.projectSerial();
                    //生成新的项目序号
                    baProject.setProjectSerial(num);
                }
            }
                //收费标准
            if(StringUtils.isNotNull(baProject.getStandards())){
                if(StringUtils.isNotEmpty(baProject.getStandards().getId())){
                    baChargingStandardsMapper.updateBaChargingStandards(baProject.getStandards());
                }else {
                    BaChargingStandards standards = baProject.getStandards();
                    standards.setId(getRedisIncreID.getId());
                    standards.setCreateTime(DateUtils.getNowDate());
                    standards.setCreateBy(SecurityUtils.getUsername());
                    baChargingStandardsMapper.insertBaChargingStandards(standards);
                    baProject.setStandardsId(standards.getId());
                }
            }
            //资金计划
            if(!CollectionUtils.isEmpty(baProject.getBaDeclares())){
                for (BaDeclare baDeclare:baProject.getBaDeclares()) {
                    baDeclare.setId(getRedisIncreID.getId());
                    baDeclare.setStartId(baProject.getId());
                    baDeclare.setSource(1);
                    baDeclare.setFlag(1L);
                    baDeclare.setCreateTime(DateUtils.getNowDate());
                    baDeclare.setCreateBy(SecurityUtils.getUsername());
                    baDeclare.setUserId(SecurityUtils.getUserId());
                    baDeclare.setDeptId(SecurityUtils.getDeptId());
                    //判断资金计划是否重复
                    BaDeclare baDeclare1  = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                    if(StringUtils.isNotNull(baDeclare1)){
                        return -1;
                    }
                    baDeclareMapper.insertBaDeclare(baDeclare);
                }
            }
            if(StringUtils.isNotEmpty(baProject.getSalesman())){
                String salesmanStr = "";
                StringBuilder salesmanStringBuilder = new StringBuilder();
                String[] salesmanArray = baProject.getSalesman().split(";");
                for(String s : salesmanArray){
                    if(StringUtils.isNotEmpty(salesmanArray)){
                        String[] salesmanSplit = s.split(",");
                        salesmanStr = salesmanSplit[salesmanSplit.length-1];
                        if(StringUtils.isNotEmpty(salesmanStr)){
                            String nickName = sysUserMapper.selectUserById(Long.valueOf(salesmanStr)).getNickName();
                            if(salesmanStringBuilder.length() > 0){
                                salesmanStringBuilder.append(",").append(nickName);
                            } else {
                                salesmanStringBuilder.append(nickName);
                            }
                        }
                    }
                }
                baProject.setSalesmanName(salesmanStringBuilder.toString());
            }
            //拼接归属部门
            if (baProject.getDeptId() != null) {
                SysDept dept = sysDeptMapper.selectDeptById(baProject.getDeptId());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                List<Integer> integerList = new ArrayList<>();
                for (String deptId:split) {
                    integerList.add(Integer.parseInt(deptId));
                }
                //倒叙拿到第一个公司
                Collections.reverse(integerList);
                String deptName = "";
                for (Integer itm:integerList) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(itm));
                    if(StringUtils.isNotNull(dept1)){
                        if(dept1.getDeptType().equals("1")){
                            deptName = dept1.getDeptName();
                            break;
                        }
                    }
                }
                baProject.setPartDeptName(deptName + "-" + dept.getDeptName());
            }
            //提交时间
            baProject.setSubmitTime(DateUtils.getNowDate());
            //业务助理发起流程
            String flowId = "";
            if(isBusinessAssistant){
                flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode(),SecurityUtils.getCurrComId());
            } else {
                flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(),SecurityUtils.getCurrComId());
            }
            baProject.setFlowId(flowId);
            //流程启动审批状态
            baProject.setState(AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
            baProjectMapper.insertBaProject(baProject);
            BaProject baProject1 = baProjectMapper.selectBaProjectById(baProject.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baProject1, AdminCodeEnum.PROJECT_APPROVAL_CONTENT_INSERT.getCode(), isBusinessAssistant);
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                //流程实例启动失败删除已有立项
                baProjectMapper.deleteBaProjectById(baProject1.getId());
                return 0;
            }else {
                //修改仓库选用状态
                if(StringUtils.isNotEmpty(baProject1.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject1.getDepotId());
                    baDepot.setIsSelected(baProject1.getId());
                    baDepotMapper.updateBaDepot(baDepot);
                }
            }
        }
        return 1;
    }

    /**
     * 提交立项审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaProject baProject, String taskContent, boolean isBusinessAssistant) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baProject.getFlowId());
        Map<String, Object> map = new HashMap<>();
        if(isBusinessAssistant){
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode());
        } else {
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        }
        //立项临时过滤仓储业务
        map.put("projectBusinessType", baProject.getBusinessType());
        map.put("projectProjectType", baProject.getProjectType());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baProject.getId());
        if(isBusinessAssistant) {
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode());
        } else {
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        }
        //获取流程实例关联的业务对象
        BaProject baProject1 = this.selectBaProjectById(baProject.getId());
        SysUser sysUser = iSysUserService.selectUserById(baProject1.getUserId());
        baProject1.setUserName(sysUser.getUserName());
        baProject1.setDeptName(sysUser.getDept().getDeptName());
        //baProject1.setUser(sysUser);
//
//
//        StringBuilder stringBuilder = new StringBuilder();
//        //供应商
//        if (StringUtils.isNotEmpty(baProject1.getSupplierId())) {
//            String[] supplierIdArray = baProject1.getSupplierId().split(",");
//            for(String supplierId : supplierIdArray){
//                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
//                if(!ObjectUtils.isEmpty(baSupplier)){
//                    if(stringBuilder.length() == 0){
//                        stringBuilder.append(baSupplier.getName());
//                    } else {
//                        stringBuilder.append(",").append(baSupplier.getName());
//                    }
//                }
//                baProject1.setSupplierName(stringBuilder.toString());
//            }
//        }
//        //用煤单位
//        if (StringUtils.isNotEmpty(baProject1.getEnterpriseId())) {
//            BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject1.getEnterpriseId());
//            baProject1.setEnterpriseName(baEnterprise.getName());
//            BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
//            if(!ObjectUtils.isEmpty(baCompanyGroup)){
//                baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
//            }
//            baProject1.setBaEnterprise(baEnterprise);
//        }
//        //实际发运人
//        if (StringUtils.isNotEmpty(baProject1.getCounterpartId())) {
//            BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(baProject1.getCounterpartId());
//            if (StringUtils.isNotNull(baCounterpart))
//                baProject1.setCounterpartName(baCounterpart.getName());
//        }
//
//        //业务经理名称
//        if(StringUtils.isNotEmpty(baProject1.getServiceManager())){
//            if(baProject1.getServiceManager().indexOf(",") != -1){
//                String[] serviceManagerArray = baProject1.getServiceManager().split(",");
//                String serviceManagerStr = serviceManagerArray[serviceManagerArray.length - 1];
//                if(StringUtils.isNotEmpty(serviceManagerStr)){
//                    baProject1.setServiceManagerName(sysUserMapper.selectUserById(Long.valueOf(serviceManagerStr)).getNickName());
//                }
//            }
//        }
//
//        //运营责任人名称
////        if(StringUtils.isNotEmpty(baProject1.getPersonCharge())) {
////            if(baProject1.getPersonCharge().indexOf(",") != -1){
////                String[] personChargeArray = baProject1.getPersonCharge().split(",");
////                String personChargeStr = personChargeArray[personChargeArray.length - 1];
////                if(StringUtils.isNotEmpty(personChargeStr)){
////                    baProject1.setPersonChargeName(sysUserMapper.selectUserById(Long.valueOf(personChargeStr)).getNickName());
////                }
////            }
////        }
//
//        List<String> processInstanceIds = new ArrayList<>();
//        //流程实例ID
//        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("business_id", baProject1.getId());
//        queryWrapper.eq("flag", 0);
//        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
//        queryWrapper.orderByDesc("create_time");
//        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
//        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
//            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
//        }
//        baProject1.setProcessInstanceId(processInstanceIds);
//
//        //品名
//        if (StringUtils.isNotEmpty(baProject1.getGoodsId())) {
//            QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
//            if(StringUtils.isNotEmpty(baProject1.getGoodsId())){
//                List<String> goodsIdList = Arrays.asList(baProject1.getGoodsId().split(","));
//                baGoodsQueryWrapper.in("id", goodsIdList);
//                baGoodsQueryWrapper.eq("flag", 0);
//                List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
//                if(!CollectionUtils.isEmpty(goodsList)){
//                    StringBuilder goodName = new StringBuilder();
//                    for(BaGoods baGoods : goodsList){
//                        if(goodName.length() > 0){
//                            goodName.append(",").append(baGoods.getName());
//                        } else {
//                            goodName.append(baGoods.getName());
//                        }
//                    }
//                    baProject1.setGoodsName(goodName.toString());
//                }
//            }
//        }
//
//        //商品分类
//        if (StringUtils.isNotEmpty(baProject1.getGoodsType())) {
//            BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject1.getGoodsType());
//            baProject1.setGoodsTypeName(baGoodsType.getName());
//        }
//
//        //仓库名称
//        BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject1.getDepotId());
//        if(!ObjectUtils.isEmpty(baDepot)){
//            //仓库名称
//            baProject1.setDepotName(baDepot.getName());
//            //仓库所属公司
//            baProject1.setDepotCompany(baDepot.getCompany());
//        }
//        //收费标准
//        if(StringUtils.isNotEmpty(baProject1.getStandardsId())){
//            BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baProject.getStandardsId());
//            baProject1.setStandards(baChargingStandards);
//        }
//        //终端内部对接人
//        if(StringUtils.isNotEmpty(baProject1.getInternalContactPerson())){
//            BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baProject1.getInternalContactPerson());
//            baProject1.setInternalContactPersonName(baUserMail.getName());
//        }
        relatedDO.setBusinessData(JSONObject.toJSONString(baProject1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO,sysUser, isBusinessAssistant);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 变更提交立项审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstancesHi(BaHiProject baHiProject, String taskContent, boolean isBusinessAssistant) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baHiProject.getFlowId());
        Map<String, Object> map = new HashMap<>();
        if(isBusinessAssistant){
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode());
        } else {
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        }
        //立项临时过滤仓储业务
        map.put("projectBusinessType", baHiProject.getBusinessType());
        map.put("projectProjectType", baHiProject.getProjectType());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baHiProject.getId());
        if(isBusinessAssistant) {
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode());
        } else {
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        }
        //获取流程实例关联的业务对象
        BaHiProject baHiProject1 = baHiProjectService.selectBaHiProjectById(baHiProject.getId());
        SysUser sysUser = iSysUserService.selectUserById(baHiProject.getUserId());
        baHiProject1.setUserName(sysUser.getUserName());
        baHiProject1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONUtil.toJsonStr(baHiProject1));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO,sysUser, isBusinessAssistant);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime,SysUser user, boolean isBusinessAssistant) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        //UserInfo userInfo = iSysUserService.getCurrentUserInfo(user);

        UserInfo userInfo = new UserInfo();
        userInfo.setId(String.valueOf(user.getUserId()));
        userInfo.setName(user.getUserName());
        userInfo.setNickName(user.getNickName());
        //根据部门ID获取部门信息
        SysDept sysDept = iSysDeptService.selectDeptById(user.getDeptId());
        if(!ObjectUtils.isEmpty(sysDept)){
            userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
        }
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());

        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            //放入当前发起人部门ID
            SecurityUtils.getLoginUser().setDept(user.getDeptId());
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)) {
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if (data != null) {
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.PROJECT_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(user.getUserName());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }

        // 保存业务数据与流程实例关系
        if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改立项管理
     *
     * @param baProject 立项管理
     * @return 结果
     */
    @Override
    public int updateBaProject(BaProject baProject) {
        BaProject baProject1 = baProjectMapper.selectBaProjectById(baProject.getId());
        //判断立项简称是否相等
        if (StringUtils.isNotEmpty(baProject1.getNameShorter()) && baProject1.getNameShorter().equals(baProject.getNameShorter()) == false) {
            QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name_shorter", baProject.getNameShorter());
            queryWrapper.eq("flag", 0);
            BaProject project = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project)) {
                return -1;
            }
        }
        //判断立项名称是否相等
        if (baProject1.getName().equals(baProject.getName()) == false) {
            QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", baProject.getName());
            queryWrapper.eq("flag", 0);
            BaProject project = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project)) {
                return -2;
            }
        }
        baProject.setUpdateTime(DateUtils.getNowDate());
        baProject.setUpdateBy(SecurityUtils.getUsername());
        baProject.setProjectState(AdminCodeEnum.PROJECT_DRAFT.getCode());
        baProject.setState("");
        //收费标准
        if(StringUtils.isNotNull(baProject.getStandards())){
            if(StringUtils.isNotEmpty(baProject.getStandards().getId())){
                baChargingStandardsMapper.updateBaChargingStandards(baProject.getStandards());
            }else {
                BaChargingStandards standards = baProject.getStandards();
                standards.setId(getRedisIncreID.getId());
                standards.setCreateTime(DateUtils.getNowDate());
                standards.setCreateBy(SecurityUtils.getUsername());
                baChargingStandardsMapper.insertBaChargingStandards(standards);
                baProject.setStandardsId(standards.getId());
            }
        }
        //拼接归属部门
        if (baProject.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(baProject.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            List<Integer> integerList = new ArrayList<>();
            for (String deptId:split) {
                integerList.add(Integer.parseInt(deptId));
            }
            //倒叙拿到第一个公司
            Collections.reverse(integerList);
            String deptName = "";
            for (Integer itm:integerList) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(itm));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        deptName = dept1.getDeptName();
                        break;
                    }
                }
            }
            baProject.setPartDeptName(deptName + "-" + dept.getDeptName());
        }
        return baProjectMapper.updateBaProject(baProject);
    }

    /**
     * 批量删除立项管理
     *
     * @param ids 需要删除的立项管理ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectByIds(String[] ids) {
        if (StringUtils.isNotEmpty(ids)) {
            for (String id : ids) {
                BaProject baProject = baProjectMapper.selectBaProjectById(id);
                baProject.setFlag(1);
                baProjectMapper.updateBaProject(baProject);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除立项管理信息
     *
     * @param id 立项管理ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectById(String id) {
        return baProjectMapper.deleteBaProjectById(id);
    }

    @Override
    public int revoke(String id) {
        //获取当前登录人角色
        List<SysRole> sysRoles = sysRoleMapper.selectRolesListByUserId(SecurityUtils.getUserId());
        boolean isBusinessAssistant = false; //判断当前登陆人是否是业务助理角色
        if(!CollectionUtils.isEmpty(sysRoles)){
            for(SysRole sysRole : sysRoles){
                if("业务助理".equals(sysRole.getRoleName())){
                    isBusinessAssistant = true;
                    break;
                }
            }
        }
        if (StringUtils.isNotEmpty(id)) {
            BaProject baProject = baProjectMapper.selectBaProjectById(id);
            baProject.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baProject.getId());
            queryWrapper.eq("flag", 0);
            queryWrapper.ne("approve_result", AdminCodeEnum.PROJECT_STATUS_WITHDRAW.getCode());
            queryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baProject.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result = workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if (result.get("code").toString().equals("200")) {
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baProject.setState(AdminCodeEnum.PROJECT_STATUS_WITHDRAW.getCode());
                baProjectMapper.updateBaProject(baProject);
                //修改仓库选用状态
                if(StringUtils.isNotEmpty(baProject.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
//                    baDepot.setIsSelected("0");
                    baDepotMapper.updateBaDepot(baDepot);
                }
                String userId = String.valueOf(baProject.getUserId());
                //给所有已审批的用户发消息
                if (!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if (!ObjectUtils.isEmpty(ajaxResult)) {
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for (LinkedHashMap map : data) {
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                                        if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                            this.insertMessage(baProject, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getDescription(), MessageConstant.APPROVAL_WITHDRAW), isBusinessAssistant);
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(BaProject baProject, String mId, String msgContent, boolean isBusinessAssistant) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baProject.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        //判断当前登陆人是否是业务助理角色
        if(isBusinessAssistant){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode());
        } else {
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        }
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"立项审批提醒",msgContent,baMessage.getType(),baProject.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public int complete(String id) {
        if (StringUtils.isNotEmpty(id)) {
            BaProject baProject = baProjectMapper.selectBaProjectById(id);
            baProject.setProjectState(AdminCodeEnum.PROJECT_COMPLETED.getCode());
            baProjectMapper.updateBaProject(baProject);
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * 查询立项管理列表
     *
     * @param baProject 立项管理
     * @return 立项管理
     */
    @Override
    public List<BaProject> listProgress(BaProject baProject) {
        //已收金额
        BigDecimal received = new BigDecimal(0.00);
        //认领收款
        BigDecimal claim = new BigDecimal(0.00);
        //已付金额
        BigDecimal paid = new BigDecimal(0.00);
        //已发运量
        BigDecimal trafficVolume = new BigDecimal(0.00);
        //查询业务状态为未立项
        //baProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
        List<BaProject> baProjects = baProjectMapper.selectBaProjectList(baProject);
        if (StringUtils.isNotNull(baProjects))
            for (BaProject baProject1 : baProjects) {
                //发起人
                SysUser sysUser = sysUserMapper.selectUserById(baProject1.getUserId());
                baProject1.setUserName(sysUser.getNickName());
                //根据项目查询合同
                QueryWrapper<BaContract> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("project_id", baProject1.getId());
                queryWrapper.eq("flag", 0);
                List<BaContract> baContracts = baContractMapper.selectList(queryWrapper);
                if (!CollectionUtils.isEmpty(baContracts)) {
                    for (BaContract baContract : baContracts) {
                        if (!ObjectUtils.isEmpty(baContract)) {
                            //查询订单
                            QueryWrapper<BaOrder> orderQueryWrapper = new QueryWrapper<>();
                            orderQueryWrapper.eq("company_id", baContract.getId()).or().eq("purchase_company_id", baContract.getId());
                            orderQueryWrapper.eq("flag", 0);
                            List<BaOrder> baOrders = baOrderMapper.selectList(orderQueryWrapper);
                            for (BaOrder baOrder : baOrders) {
                                //查询运输
                                //订单对应的运输信息
                                QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                                transportQueryWrapper.eq("order_id", baOrder.getId());
                                transportQueryWrapper.eq("flag", 0);
                                BaTransport baTransport = baTransportMapper.selectOne(transportQueryWrapper);
                                //汽车运输
                                QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
                                automobileQueryWrapper.eq("order_id", baOrder.getId());
                                transportQueryWrapper.eq("is_delete", 0);
                                BaTransportAutomobile automobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
                                //以运输信息查询发运信息
                                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                                if (StringUtils.isNotNull(automobile) && StringUtils.isNotNull(baTransport)) {
                                    checkQueryWrapper.eq("relation_id", baTransport.getId()).or().eq("relation_id", automobile.getId());
                                } else if (StringUtils.isNotNull(baTransport)) {
                                    checkQueryWrapper.eq("relation_id", baTransport.getId());
                                } else if (StringUtils.isNotNull(automobile)) {
                                    checkQueryWrapper.eq("relation_id", automobile.getId());
                                }
                                checkQueryWrapper.eq("type", 1);
                                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                                for (BaCheck baCheck : baChecks) {
                                    trafficVolume = trafficVolume.add(baCheck.getCheckCoalNum());
                                }
                            }
                            //查询结算单
                            QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                            settlementQueryWrapper.eq("contract_id", baContract.getId());
                            settlementQueryWrapper.eq("flag", 0);
                            List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                            for (BaSettlement baSettlement : baSettlementList) {
                                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                                paymentQueryWrapper.eq("settlement_id", baSettlement.getId());
                                paymentQueryWrapper.eq("state", "payment:pass");
                                List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
                                for (BaPayment baPayment : baPayments) {
                                    paid = paid.add(baPayment.getActualPaymentAmount());
                                }
                                //认领收款
                                QueryWrapper<BaCollection> collectionQueryWrapper = new QueryWrapper<>();
                                collectionQueryWrapper.isNull("settlement_id");
                                collectionQueryWrapper.eq("state", "claim:pass");
                                //collectionQueryWrapper.eq("settlement_id",baSettlement.getId()).and(query ->query.eq("state","collection:pass").or().eq("state","claim:pass"));
                                //collectionQueryWrapper.eq("state","collection:pass");
                                List<BaCollection> baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
                                for (BaCollection baCollection : baCollectionList) {
                                    if (StringUtils.isNotNull(baCollection.getClaimed())) {
                                        claim = claim.add(baCollection.getClaimed());
                                    }
                                }
                                //结算收款
                                collectionQueryWrapper = new QueryWrapper<>();
                                collectionQueryWrapper.eq("settlement_id", baSettlement.getId());
                                collectionQueryWrapper.eq("state", "collection:pass");
                                baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
                                for (BaCollection baCollection : baCollectionList) {
                                    if (StringUtils.isNotNull(baCollection.getApplyCollectionAmount())) {
                                        received = received.add(baCollection.getApplyCollectionAmount());
                                    }
                                }
                            }
                        }
                    }
                }
                //付款金额
                baProject1.setPaid(paid);
                //收款金额
                baProject1.setReceived(received.add(claim));
                //已发运量
                baProject1.setTrafficVolume(trafficVolume);
            }
        return baProjects;
    }

    @Override
    public String getUpdateAuth(BaProjectDTO baProjectDTO) {
        String isExist = "isNotExist";
        //查询发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(baProjectDTO.getStartUserId()));
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(!ObjectUtils.isEmpty(sysUser) && !ObjectUtils.isEmpty(loginUser)){
            String deptId = sysUser.getDeptId().toString();
            //根据部门ID和角色ID查询
            SysUserDepDTO sysUserRoleDTO = new SysUserDepDTO();
            sysUserRoleDTO.setDeptId(deptId);
           /* sysUserRoleDTO.setRoleId("4"); */
            List<String> sysUsers = sysUserMapper.selectUserByDepIdAndRole(sysUserRoleDTO);//运营专员
            if(!CollectionUtils.isEmpty(sysUsers)){
            for (String id:sysUsers) {
                SysUser sysUser1 = sysUserMapper.selectUserById(Long.parseLong(id));
                if(loginUser.getUserId().equals(sysUser1.getUserId())){
                    isExist = "isExist";
                    break;
                }
            }
              }
            /*if(!CollectionUtils.isEmpty(sysUsers)){
                for(SysUser sysUser1 : sysUsers){
                    if(loginUser.getUserId().equals(sysUser1.getUserId())){
                        isExist = "isExist";
                        break;
                    }
                }
            }*/
            if("isNotExist".equals(isExist)){
                SysUserDepDTO sysUserDepDTO = new SysUserDepDTO();
                sysUserDepDTO.setDeptId(deptId);
                List<String> strings = sysUserMapper.selectUserByDepId(sysUserDepDTO);
                if(!CollectionUtils.isEmpty(strings)){
                    if(strings.contains(String.valueOf(loginUser.getUserId()))){
                        isExist = "isExist";
                    }
                }
            }
        }

        return isExist;
    }

    @Override
    public List<BaProjectDTO> baProjectDTOList(String type) {
        //合同数量
        BigDecimal tonnageNum = new BigDecimal(0.00);
        List<BaProjectDTO> baProjectDTOS = new ArrayList<>();
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(type) && type.equals("1")){
            projectQueryWrapper.eq("state","projectStatus:pass");
        }
        if(type.equals("2")){
            projectQueryWrapper.eq("project_state",AdminCodeEnum.PROJECT_DRAFT.getCode());
        }
        projectQueryWrapper.eq("flag",0);
        projectQueryWrapper.ne("project_type",2);
        projectQueryWrapper.orderByDesc("create_time");
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        for (BaProject baProject:baProjects) {
            //已发运量
            BigDecimal checkCoalNum = new BigDecimal(0);
            //已收款金额
            BigDecimal applyCollectionAmount = new BigDecimal(0);
            //已付款金额
            BigDecimal actualPaymentAmount = new BigDecimal(0);
            //贸易金额
            BigDecimal tradeAmount = new BigDecimal(0);
            BaProjectDTO baProjectDTO = new BaProjectDTO();
            baProjectDTO.setProjectName(baProject.getName());
            baProjectDTO.setBusinessLines(baProject.getBusinessLines());
            baProjectDTO.setId(baProject.getId());
            baProjectDTO.setUserName(sysUserMapper.selectUserById(baProject.getUserId()).getNickName());
            baProjectDTO.setDeptName(sysUserMapper.selectUserById(baProject.getUserId()).getDept().getDeptName());
            baProjectDTO.setProjectType(baProject.getProjectType());
            if(StringUtils.isNotEmpty(baProject.getEnterpriseId())){
                baProjectDTO.setEnterpriseName(baEnterpriseMapper.selectBaEnterpriseById(baProject.getEnterpriseId()).getName());
            }
            //查询合同启动
            QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
            startQueryWrapper.eq("project_id",baProject.getId());
            startQueryWrapper.eq("state","contractStart:pass");
            startQueryWrapper.eq("flag",0);
            List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
            for (BaContractStart baContractStart:baContractStartList) {
                //统计合同数量
                if(baContractStart.getTonnage() != null){
                    tonnageNum = tonnageNum.add(baContractStart.getTonnage());
                }
                //查询对应的运输信息
                QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("start_id",baContractStart.getId());
                /*transportQueryWrapper.eq("confirm_status",2);
                transportQueryWrapper.eq("transport_status",1);*/
                transportQueryWrapper.eq("flag",0);
                List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                for (BaTransport baTransport:baTransports) {
                    //验收信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                    checkQueryWrapper.eq("type",1);
                    List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                    for (BaCheck baCheck:baChecks) {
                        checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                    }
                }
                //查询认领金额
                QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                historyQueryWrapper.eq("order_id",baContractStart.getId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                for (BaClaimHistory baClaimHistory:baClaimHistories) {
                    //查询认领信息
                    BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                    BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                    //判断认领收款审批通过
                    if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                        if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                            applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                        }
                    }
                }
                //查询付款信息
                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                paymentQueryWrapper.eq("start_id",baContractStart.getId());
                paymentQueryWrapper.eq("state","payment:pass");
                //paymentQueryWrapper.eq("payment_state",AdminCodeEnum.PAYMENT_PAID.getCode());
                paymentQueryWrapper.eq("flag",0);
                List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
                for (BaPayment baPayment:baPayments) {
                    actualPaymentAmount = actualPaymentAmount.add(baPayment.getApplyPaymentAmount());
                }
                //贸易金额
                QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                settlementQueryWrapper.eq("contract_id",baContractStart.getId());
                settlementQueryWrapper.eq("state","settlement:pass");
                settlementQueryWrapper.eq("flag",0);
                settlementQueryWrapper.eq("settlement_type",2);
                List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                for (BaSettlement baSettlement:baSettlementList) {
                    if(baSettlement.getSettlementAmount() != null){
                        tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                    }
                }
             }
            //设置合同数量
            baProjectDTO.setTonnage(tonnageNum);
            baProjectDTO.setActualPaymentAmount(actualPaymentAmount);
            baProjectDTO.setApplyCollectionAmount(applyCollectionAmount);
            baProjectDTO.setCheckCoalNum(checkCoalNum);
            baProjectDTO.setTradeAmount(tradeAmount);
            baProjectDTOS.add(baProjectDTO);
        }
        return baProjectDTOS;
    }

    @Override
    public List<BaProjectDTO> baProjectDTOList1() {
        List<BaProjectDTO> baProjectDTOS = new ArrayList<>();
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq("state","projectStatus:pass");
        projectQueryWrapper.eq("flag",0);
        projectQueryWrapper.orderByDesc("create_time");
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        for (BaProject baProject:baProjects) {
            //已收款金额
            BigDecimal applyCollectionAmount = new BigDecimal(0);
            //已付款金额
            BigDecimal actualPaymentAmount = new BigDecimal(0);
            BaProjectDTO baProjectDTO = new BaProjectDTO();
            baProjectDTO.setProjectName(baProject.getName());
            //查询合同启动
            QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
            startQueryWrapper.eq("project_id",baProject.getId());
            startQueryWrapper.eq("state","contractStart:pass");
            startQueryWrapper.eq("flag",0);
            List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
            for (BaContractStart baContractStart:baContractStartList) {
                //查询认领金额
                QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                historyQueryWrapper.eq("order_id",baContractStart.getId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                for (BaClaimHistory baClaimHistory:baClaimHistories) {
                    //查询认领信息
                    BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                    BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                    //判断认领收款审批通过
                    if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                        if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                            applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                        }
                    }
                }
                //查询付款信息
                BaPayment baPayment = new BaPayment();
                baPayment.setTenantId(SecurityUtils.getCurrComId());
                baPayment.setStartId(baContractStart.getId());
                BigDecimal bigDecimal = baPaymentMapper.amountPaid(baPayment);
                if(null != bigDecimal){
                    actualPaymentAmount = actualPaymentAmount.add(bigDecimal);
                }
            }

            baProjectDTO.setActualPaymentAmount(actualPaymentAmount);
            baProjectDTO.setApplyCollectionAmount(applyCollectionAmount);
            if(applyCollectionAmount.equals(new BigDecimal("0"))){
                 baProjectDTO.setYield(new BigDecimal(0));
            }else {
                BigDecimal multiply = ((applyCollectionAmount.subtract(actualPaymentAmount)).divide(applyCollectionAmount, 3, BigDecimal.ROUND_UP)).multiply(new BigDecimal(100));
                baProjectDTO.setYield(multiply);
            }
            baProjectDTOS.add(baProjectDTO);
        }
        return baProjectDTOS;
    }

    @Override
    public List<BaProjectDTO> baProjectDTOS(String type) {

        List<BaProjectDTO> baProjectDTOS = new ArrayList<>();
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(type) && type.equals("1")){
            projectQueryWrapper.eq("state","projectStatus:pass");
            projectQueryWrapper.eq("dept_id",SecurityUtils.getDeptId());
        } else if(type.equals("2")){
            projectQueryWrapper.eq("project_state",AdminCodeEnum.PROJECT_DRAFT.getCode());
        }
        projectQueryWrapper.eq("flag",0);
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        if(CollectionUtils.isEmpty(baProjects)){
            baProjects = new ArrayList<>();
        }
        if(!type.equals("2")) {
            //查询填充授权给当前登录人部门的立项
            //授权查询
            QueryWrapper<BaProjectLink> queryWrapper = new QueryWrapper<>();
            queryWrapper.like("dept_id", SecurityUtils.getDeptId().toString());
            List<BaProjectLink> baProjectLinks = baProjectLinkMapper.selectList(queryWrapper);
            for (BaProjectLink baProjectLink1 : baProjectLinks) {
                //查询立项
                QueryWrapper<BaProject> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("id", baProjectLink1.getProjectId());
                queryWrapper1.eq("state", "projectStatus:pass");
                queryWrapper1.eq("flag", 0);
                BaProject addBaProject = baProjectMapper.selectOne(queryWrapper1);
                if (!ObjectUtils.isEmpty(addBaProject)) {
                    addBaProject.setEmpower("1");
                    baProjects.add(addBaProject);
                }
            }
        }
        //立项集合去重
        List<BaProject> collect = baProjects.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaProject::getId))), ArrayList::new));

        if(!CollectionUtils.isEmpty(collect)){
            for (BaProject baProject:collect) {
                //已发运量
                BigDecimal checkCoalNum = new BigDecimal(0);
                //已收款金额
                BigDecimal applyCollectionAmount = new BigDecimal(0);
                //已付款金额
                BigDecimal actualPaymentAmount = new BigDecimal(0);
                //贸易金额
                BigDecimal tradeAmount = new BigDecimal(0);
                BaProjectDTO baProjectDTO = new BaProjectDTO();
                baProjectDTO.setProjectName(baProject.getName());
                baProjectDTO.setProjectType(baProject.getProjectType());
                baProjectDTO.setId(baProject.getId());
                baProjectDTO.setUserName(sysUserMapper.selectUserById(baProject.getUserId()).getNickName());
                baProjectDTO.setDeptName(sysUserMapper.selectUserById(baProject.getUserId()).getDept().getDeptName());
                if(StringUtils.isNotEmpty(baProject.getEnterpriseId())){
                    baProjectDTO.setEnterpriseName(baEnterpriseMapper.selectBaEnterpriseById(baProject.getEnterpriseId()).getName());
                }
                //查询合同启动
                QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
                startQueryWrapper.eq("project_id",baProject.getId());
                startQueryWrapper.eq("state","contractStart:pass");
                startQueryWrapper.eq("flag",0);
                List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
                for (BaContractStart baContractStart:baContractStartList) {
                    //查询对应的运输信息
                    QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                    transportQueryWrapper.eq("start_id",baContractStart.getId());
                    /*transportQueryWrapper.eq("confirm_status",2);
                    transportQueryWrapper.eq("transport_status",1);*/
                    transportQueryWrapper.eq("flag",0);
                    List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                    for (BaTransport baTransport:baTransports) {
                        //验收信息
                        QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                        checkQueryWrapper.eq("relation_id",baTransport.getId());
                        checkQueryWrapper.eq("type",1);
                        List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                        for (BaCheck baCheck:baChecks) {
                            checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                        }
                    }
                    //查询认领金额
                    QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                    historyQueryWrapper.eq("order_id",baContractStart.getId());
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                    for (BaClaimHistory baClaimHistory:baClaimHistories) {
                        //查询认领信息
                        BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                        if(!ObjectUtils.isEmpty(baClaim)){
                            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                            //判断认领收款审批通过
                            if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                                if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                                    applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                                }
                            }
                        }
                    }
                    //查询付款信息
                    QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                    paymentQueryWrapper.eq("start_id",baContractStart.getId());
                    paymentQueryWrapper.eq("state","payment:pass");
                    //paymentQueryWrapper.eq("payment_state",AdminCodeEnum.PAYMENT_PAID.getCode());
                    paymentQueryWrapper.eq("flag",0);
                    List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
                    for (BaPayment baPayment:baPayments) {
                        actualPaymentAmount = actualPaymentAmount.add(baPayment.getApplyPaymentAmount());
                    }
                    //贸易金额
                    QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                    settlementQueryWrapper.eq("contract_id",baContractStart.getId());
                    settlementQueryWrapper.eq("state","settlement:pass");
                    settlementQueryWrapper.eq("flag",0);
                    settlementQueryWrapper.eq("settlement_type",2);
                    List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                    for (BaSettlement baSettlement:baSettlementList) {
                        if(baSettlement.getSettlementAmount() != null){
                            tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                        }
                    }
                }
                baProjectDTO.setActualPaymentAmount(actualPaymentAmount);
                baProjectDTO.setApplyCollectionAmount(applyCollectionAmount);
                baProjectDTO.setCheckCoalNum(checkCoalNum);
                baProjectDTO.setTradeAmount(tradeAmount);
                baProjectDTOS.add(baProjectDTO);
            }
        }
        return baProjectDTOS;
    }

    @Override
    public List<ContractStartDTO> contractStart(BaContractStartDTO startDTO) {

        List<ContractStartDTO> startList = new ArrayList<>();

        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq("state","projectStatus:pass");
        if(StringUtils.isNotEmpty(startDTO.getDeptType())){
            //部门状态为1时，获取部门权限
            if(startDTO.getDeptType().equals("1")){
                projectQueryWrapper.eq("dept_id",SecurityUtils.getDeptId());
            }
        }
        projectQueryWrapper.eq("flag",0);
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);

        //查询填充授权给当前登录人部门的立项
        //授权查询
        QueryWrapper<BaProjectLink> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("dept_id", SecurityUtils.getDeptId().toString());
        List<BaProjectLink> baProjectLinks = baProjectLinkMapper.selectList(queryWrapper);
        for (BaProjectLink baProjectLink1:baProjectLinks) {
            //查询立项
            QueryWrapper<BaProject> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("id",baProjectLink1.getProjectId());
            queryWrapper1.eq("state","projectStatus:pass");
            queryWrapper1.eq("flag",0);
            BaProject addBaProject = baProjectMapper.selectOne(queryWrapper1);
            if(!ObjectUtils.isEmpty(addBaProject)){
                addBaProject.setEmpower("1");
                baProjects.add(addBaProject);
            }
        }

        //立项集合去重
        List<BaProject> collect = baProjects.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaProject::getId))), ArrayList::new));
        for (BaProject baProject:collect) {
            QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
            startQueryWrapper.eq("state","contractStart:pass");
            startQueryWrapper.eq("flag",0);
            startQueryWrapper.eq("project_id",baProject.getId());
            if(StringUtils.isNotEmpty(startDTO.getType())){
                if(startDTO.getType().equals("1")){
                 startQueryWrapper.ne("start_type",4);
                }
                if(startDTO.getType().equals("2")){
                    startQueryWrapper.eq("start_type",4);
                }
            }
            startQueryWrapper.orderByDesc("create_time");
            List<BaContractStart> starts = baContractStartMapper.selectList(startQueryWrapper);
            for (BaContractStart baContractStart:starts) {
                //实际发运量
                BigDecimal checkCoalNum = new BigDecimal(0);
                //已收款金额
                BigDecimal applyCollectionAmount = new BigDecimal(0);
                //已付款金额
                BigDecimal actualPaymentAmount = new BigDecimal(0);
                ContractStartDTO contractStartDTO = new ContractStartDTO();
                contractStartDTO.setId(baContractStart.getId());
                contractStartDTO.setBusinessLines(baContractStart.getBusinessLines());
                contractStartDTO.setCreateTime(baContractStart.getCreateTime());
                contractStartDTO.setUserName(sysUserMapper.selectUserById(baContractStart.getUserId()).getNickName());
                contractStartDTO.setTonnage(baContractStart.getTonnage());
                contractStartDTO.setName(baContractStart.getName());
                //查询对应的运输信息
                QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("start_id",baContractStart.getId());
                transportQueryWrapper.eq("flag",0);
                List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                for (BaTransport baTransport:baTransports) {
                    //验收信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                    checkQueryWrapper.eq("type",1);
                    List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                    for (BaCheck baCheck:baChecks) {
                        checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                    }
                }
                contractStartDTO.setCheckCoalNum(checkCoalNum);
                //资金占用
                //查询认领金额
                QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
                historyQueryWrapper.eq("order_id",baContractStart.getId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
                for (BaClaimHistory baClaimHistory:baClaimHistories) {
                    //查询认领信息
                    BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                    if(!ObjectUtils.isEmpty(baClaim)){
                        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                        //判断认领收款审批通过
                        if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                            if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                                applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                            }
                        }
                    }
                }
                //查询付款信息
                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                paymentQueryWrapper.eq("start_id",baContractStart.getId());
                paymentQueryWrapper.eq("state","payment:pass");
                //paymentQueryWrapper.eq("payment_state",AdminCodeEnum.PAYMENT_PAID.getCode());
                paymentQueryWrapper.eq("flag",0);
                List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
                for (BaPayment baPayment:baPayments) {
                    actualPaymentAmount = actualPaymentAmount.add(baPayment.getApplyPaymentAmount());
                }
                //资金占用
                contractStartDTO.setFundOccupancy(actualPaymentAmount.subtract(applyCollectionAmount));
                //查询销项发票
                BigDecimal invoicedQuantityPic = new BigDecimal(0);
                QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
                invoiceQueryWrapper.eq("start_id",baContractStart.getId());
                invoiceQueryWrapper.eq("invoice_type",1);
                invoiceQueryWrapper.eq("state","invoice:pass");
                invoiceQueryWrapper.eq("flag",0);
                List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
                for (BaInvoice baInvoice:baInvoices) {
                    if(StringUtils.isNotNull(baInvoice)){
                        invoicedQuantityPic = invoicedQuantityPic.add(baInvoice.getInvoicedAmount());
                    }
                }
                //进项发票
                BigDecimal inVoIcePic = new BigDecimal(0);
                invoiceQueryWrapper = new QueryWrapper<>();
                invoiceQueryWrapper.eq("start_id",baContractStart.getId());
                invoiceQueryWrapper.eq("invoice_type",2);
                invoiceQueryWrapper.eq("state","inputInvoice:pass");
                invoiceQueryWrapper.eq("flag",0);
                baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
                for (BaInvoice baInvoice:baInvoices) {
                    if(StringUtils.isNotNull(baInvoice)){
                        QueryWrapper<BaInvoiceDetail> detailQueryWrapper = new QueryWrapper<>();
                        detailQueryWrapper.eq("invoice_id",baInvoice.getId());
                        //detailQueryWrapper.eq("flag",0);
                        List<BaInvoiceDetail> baInvoiceDetails = baInvoiceDetailMapper.selectList(detailQueryWrapper);
                        for (BaInvoiceDetail baInvoiceDetail:baInvoiceDetails) {
                            if(StringUtils.isNotNull(baInvoiceDetail)){
                                inVoIcePic = inVoIcePic.add(baInvoiceDetail.getTaxPriceTotal());
                            }
                        }
                    }
                }

                if(invoicedQuantityPic.compareTo(BigDecimal.ZERO) > 0 && inVoIcePic.compareTo(BigDecimal.ZERO) > 0){
                    //毛利润
                    contractStartDTO.setGrossProfit(invoicedQuantityPic.subtract(inVoIcePic));
                }
                //下游结算单
                BigDecimal tradeAmount = new BigDecimal(0);
                QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                settlementQueryWrapper.eq("contract_id",baContractStart.getId());
                settlementQueryWrapper.eq("state","settlement:pass");
                settlementQueryWrapper.eq("flag",0);
                settlementQueryWrapper.eq("settlement_type",2);
                List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                for (BaSettlement baSettlement:baSettlementList) {
                    if(baSettlement.getSettlementAmount() != null){
                        tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                    }
                }
                //贸易金额
                contractStartDTO.setTradeAmount(tradeAmount);
                //待收款金额
                if(tradeAmount.compareTo(BigDecimal.ZERO) > 0){
                    contractStartDTO.setToBeCollected(tradeAmount.subtract(applyCollectionAmount));
                }
                startList.add(contractStartDTO);
            }
        }
        return startList;
    }

    @Override
    public UR dataStatistics(String type) {
        HashMap<String, String> map = new HashMap<>();

        if(StringUtils.isNotEmpty(type) && type.equals("1")){
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq("state","projectStatus:pass");
        projectQueryWrapper.eq("flag",0);
            projectQueryWrapper.inSql("DATE_FORMAT( submit_time, '%Y%m' )","DATE_FORMAT( CURDATE( ) , '%Y%m' )");
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        map.put("本月项目数",String.valueOf(baProjects.size()));
        }
        //查询立项信息
        QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq("state","projectStatus:pass");
        projectQueryWrapper.eq("flag",0);
        List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
        map.put("项目数",String.valueOf(baProjects.size()));
        //合同启动数量
        int num = 0;
        //累计贸易额
        BigDecimal tradeAmount = new BigDecimal(0);
        //累计运输量
        BigDecimal checkCoalNum = new BigDecimal(0);
        for (BaProject baProject:baProjects) {
            //合同启动个数
            QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
            startQueryWrapper.eq("state","contractStart:pass");
            startQueryWrapper.eq("flag",0);
            startQueryWrapper.eq("project_id",baProject.getId());
            if(StringUtils.isNotEmpty(type) && type.equals("1")){
                startQueryWrapper.inSql("DATE_FORMAT( create_time, '%Y%m' )","DATE_FORMAT( CURDATE( ) , '%Y%m' )");
            }
            List<BaContractStart> starts = baContractStartMapper.selectList(startQueryWrapper);
            for (BaContractStart baContractStart:starts) {
                //下游结算单金额
                BaSettlement baSettlement = new BaSettlement();
                baSettlement.setFlag(0);
                baSettlement.setSettlementType(new Long(2)); //销售结算单
                baSettlement.setContractId(baContractStart.getId());
                baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
                //租户ID
                baSettlement.setTenantId(SecurityUtils.getCurrComId());
                tradeAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);
                if(tradeAmount != null){
                    tradeAmount = tradeAmount.add(tradeAmount);
                }
                /*QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                settlementQueryWrapper.eq("contract_id",baContractStart.getId());
                    settlementQueryWrapper.eq("state","settlement:pass");
                settlementQueryWrapper.eq("flag",0);
                settlementQueryWrapper.eq("settlement_type",2);
                List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                for (BaSettlement baSettlement:baSettlementList) {
                    if(baSettlement.getSettlementAmount()!=null){
                        tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                    }
                }*/
                //累计运输量
                BaTransport baTransport = new BaTransport();
                baTransport.setStartId(baContractStart.getId());
                baTransport.setTenantId(SecurityUtils.getCurrComId());
                baTransport.setType("1");
                BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
                if(!ObjectUtils.isEmpty(baTransport1)){
                    if(baTransport1.getCheckCoalNum() != null){
                        checkCoalNum.add(baTransport1.getCheckCoalNum());
                    }
                    if(baTransport1.getCcjweight() != null){
                        checkCoalNum.add(baTransport1.getCcjweight());
                    }
                    if(baTransport1.getConfirmedDeliveryWeight() != null){
                        checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                    }
                }
                if(checkCoalNum != null){
                    checkCoalNum = checkCoalNum.add(checkCoalNum);
                }
                /*QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("start_id",baContractStart.getId());
                transportQueryWrapper.eq("flag",0);
                List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                for (BaTransport baTransport:baTransports) {
                    //验收信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                    checkQueryWrapper.eq("type",1);
                    List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                    for (BaCheck baCheck:baChecks) {
                        checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                    }
                }*/
            }
            num = starts.size() + num;
        }
        //checkCoalNum = checkCoalNum.add(checkCoalNum);
        //tradeAmount = tradeAmount.add(tradeAmount);
        map.put("累计运输量",String.valueOf(checkCoalNum));
        map.put("合同启动数量",String.valueOf(num));
        map.put("累计贸易额",String.valueOf(tradeAmount));
        return UR.ok().data("map",map);
    }

    @Override
    public List<StatisticsDTO> statistics() {
        //排名数据
        List<StatisticsDTO> dtoList = new ArrayList<>();

        SysDept sysDept = new SysDept();
        List<SysDept> sysDeptList = sysDeptMapper.selectDeptList(sysDept);
        for (SysDept dept:sysDeptList) {
            if(dept.getParentId() == 101){
                StatisticsDTO statisticsDTO = new StatisticsDTO();
                statisticsDTO.setDeptName(dept.getDeptName());
                //查询立项信息
                QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
                projectQueryWrapper.eq("dept_id",dept.getDeptId());
                projectQueryWrapper.eq("state","projectStatus:pass");
                projectQueryWrapper.eq("flag",0);
                List<BaProject> baProjects = baProjectMapper.selectList(projectQueryWrapper);
                statisticsDTO.setProjects(baProjects.size());
                //合同启动
                Integer contractStarts = 0;
                //发货次数
                Integer despatchNum = 0;
                //发运量
                BigDecimal checkCoalNum = new BigDecimal(0);
                for (BaProject baProject:baProjects) {
                    QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
                    startQueryWrapper.eq("project_id",baProject.getId());
                    startQueryWrapper.eq("flag",0);
                    startQueryWrapper.eq("state","contractStart:pass");
                    List<BaContractStart> starts = baContractStartMapper.selectList(startQueryWrapper);
                    contractStarts = contractStarts + starts.size();
                    for (BaContractStart baContractStart:starts) {
                        //查询发运数据
                        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                        transportQueryWrapper.eq("start_id",baContractStart.getId());
                        transportQueryWrapper.eq("flag",0);
                        List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                        for (BaTransport baTransport:baTransports) {
                            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                            checkQueryWrapper.eq("relation_id",baTransport.getId());
                            checkQueryWrapper.eq("type",1);
                            List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                            despatchNum = despatchNum + baChecks.size();
                            for (BaCheck baCheck:baChecks) {
                                checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                            }
                        }

                    }
                }
                statisticsDTO.setDespatchNum(despatchNum);
                statisticsDTO.setContractStarts(contractStarts);
                statisticsDTO.setTrafficVolume(checkCoalNum);
                dtoList.add(statisticsDTO);
            }
        }
        dtoList.sort(Comparator.comparing(StatisticsDTO::getProjects).reversed());
        return dtoList;
    }

    /**
     * 审批办理保存编辑业务数据
     * @param baProjectInstanceRelatedEditDTO
     * @return
     */
    public int updateProcessBusinessData(BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO) {
        BaProject baProject = baProjectInstanceRelatedEditDTO.getBaProject();
        BaProject baProject1 = new BaProject();
        if("2".equals(baProject.getApproveFlag())){
            BaHiProject baHiProject = baHiProjectMapper.selectBaHiProjectById(baProjectInstanceRelatedEditDTO.getBusinessId());
            BeanUtils.copyBeanProp(baProject1, baHiProject);
        } else {
            baProject1 = baProjectMapper.selectBaProjectById(baProjectInstanceRelatedEditDTO.getBusinessId());
        }
        //判断立项编号是否相等
        if (StringUtils.isNotEmpty(baProject1.getProjectNum()) && !baProject1.getProjectNum().equals(baProject.getProjectNum())) {
            QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("project_num", baProject.getProjectNum());
            queryWrapper.eq("flag", 0);
            BaProject project = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project)) {
                return -1;
            }
        }
        //判断立项名称是否相等
        if (StringUtils.isNotEmpty(baProject1.getName()) && !baProject1.getName().equals(baProject.getName())) {
            QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", baProject.getName());
            queryWrapper.eq("flag", 0);
            BaProject project = baProjectMapper.selectOne(queryWrapper);
            if (StringUtils.isNotNull(project)) {
                return -2;
            }
        }
        baProject.setUpdateTime(DateUtils.getNowDate());
        baProject.setUpdateBy(SecurityUtils.getUsername());
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        baProcessInstanceRelated.setProcessInstanceId(baProjectInstanceRelatedEditDTO.getProcessInstanceId());
        //业务经理名称
        if(StringUtils.isNotEmpty(baProject.getServiceManager())){
            if(baProject.getServiceManager().indexOf(",") != -1){
                String[] serviceManagerArray = baProject.getServiceManager().split(",");
                String serviceManagerStr = serviceManagerArray[serviceManagerArray.length - 1];
                if(StringUtils.isNotEmpty(serviceManagerStr)){
                    baProject.setServiceManagerName(sysUserMapper.selectUserById(Long.valueOf(serviceManagerStr)).getNickName());
                }
            }
        }
        //终端内部对接人
        if(StringUtils.isNotEmpty(baProject.getInternalContactPerson())){
            BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baProject.getInternalContactPerson());
            baProject.setInternalContactPersonName(baUserMail.getName());
        }
        //运营责任人名称
//        if(StringUtils.isNotEmpty(baProject.getPersonCharge())) {
//            if(baProject.getPersonCharge().indexOf(",") != -1){
//                String[] personChargeArray = baProject.getPersonCharge().split(",");
//                String personChargeStr = personChargeArray[personChargeArray.length - 1];
//                if(StringUtils.isNotEmpty(personChargeStr)){
//                    baProject.setPersonChargeName(sysUserMapper.selectUserById(Long.valueOf(personChargeStr)).getNickName());
//                }
//            }
//        }
        //收费标准
        if(StringUtils.isNotNull(baProject.getStandards())){
            if(StringUtils.isNotEmpty(baProject.getStandards().getId())){
                baChargingStandardsMapper.updateBaChargingStandards(baProject.getStandards());
            }else {
                BaChargingStandards standards = baProject.getStandards();
                standards.setId(getRedisIncreID.getId());
                standards.setCreateTime(DateUtils.getNowDate());
                standards.setCreateBy(SecurityUtils.getUsername());
                baChargingStandardsMapper.insertBaChargingStandards(standards);
                baProject.setStandardsId(standards.getId());
            }
        }

        //上游公司
        if(StringUtils.isNotEmpty(baProject.getUpstreamCompany())){
            //查询供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProject.getUpstreamCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baProject.setUpstreamCompanyName(baSupplier.getName());
            }
        }
        //主体公司
        if(StringUtils.isNotEmpty(baProject.getSubjectCompany())){
            //子公司
            SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baProject.getSubjectCompany()));
            if(!ObjectUtils.isEmpty(sysDept)){
                baProject.setSubjectCompanyName(sysDept.getDeptName());
            }
        }
        //终端公司
        if(StringUtils.isNotEmpty(baProject.getTerminalCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baProject.getTerminalCompany());
            if(StringUtils.isNotNull(enterprise)){
                baProject.setTerminalCompanyName(enterprise.getName());
            }
        }
        //下游公司
        if(StringUtils.isNotEmpty(baProject.getDownstreamCompany())){
            //下游公司名称
            String downstreamCompanyName = "";
            String[] split = baProject.getDownstreamCompany().split(",");
            for (String downstream:split) {
                String[] split1 = downstream.split(";");
                //供应商
                if("1".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(key);
                    downstreamCompanyName = split1[0]+";"+baSupplier.getName()+","+downstreamCompanyName;
                }
                //终端企业
                if("2".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(key);
                    downstreamCompanyName = split1[0]+";"+enterprise.getName()+","+downstreamCompanyName;
                }
                //公司
                if(!"1".equals(split1[0]) && !"2".equals(split1[0]) && !"6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                    downstreamCompanyName = split1[0]+";"+baCompany.getName()+","+downstreamCompanyName;
                }
                //集团公司
                if("6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(key);
                    downstreamCompanyName = split1[0]+";"+companyGroup.getCompanyName()+","+downstreamCompanyName;
                }
            }
            if(",".equals(downstreamCompanyName.substring(downstreamCompanyName.length()-1))){
                baProject.setDownstreamCompanyName(downstreamCompanyName.substring(0,downstreamCompanyName.length()-1));
            }else {
                baProject.setDownstreamCompanyName(downstreamCompanyName);
            }
        }
        //品名
        if("1".equals(baProject.getProjectType())) { //判断是否是常规立项
            if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                if (StringUtils.isNotEmpty(baProject.getGoodsId())) {
                    List<String> goodsIdList = Arrays.asList(baProject.getGoodsId().split(","));
                    baGoodsQueryWrapper.in("id", goodsIdList);
                    baGoodsQueryWrapper.eq("flag", 0);
                    List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                    //绑定商品对象
                    baProject.setGoodsList(goodsList);
                    if (!CollectionUtils.isEmpty(goodsList)) {
                        StringBuilder goodName = new StringBuilder();
                        for (BaGoods baGoods : goodsList) {
                            if (goodName.length() > 0) {
                                goodName.append(",").append(baGoods.getName());
                            } else {
                                goodName.append(baGoods.getName());
                            }
                        }
                        baProject.setGoodsName(goodName.toString());
                    }
                }
            }
        } else {
            if (StringUtils.isNotEmpty(baProject.getGoodsType())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baProject.getGoodsType());
                if (!ObjectUtils.isEmpty(baGoods)) {
                    baProject.setGoodsTypeName(baGoods.getName());
                } else {
                    if(StringUtils.isNotEmpty(baProject.getGoodsType())){
                        BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
                        if(!ObjectUtils.isEmpty(baGoodsType)){
                            baProject.setGoodsTypeName(baGoodsType.getName());
                        }
                    }
                }
            }
        }
        //仓库信息
        if(StringUtils.isNotEmpty(baProject.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
            if(StringUtils.isNotNull(baDepot)){
                baProject.setDepotName(baDepot.getName());
                baProject.setDepotCompany(baDepot.getCompany());
            }
        }
        //货主
        if(StringUtils.isNotEmpty(baProject.getCustomId())){
            BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(baProject.getCustomId());
            if(!ObjectUtils.isEmpty(baCargoOwner)){
                baProject.setCustomName(baCargoOwner.getName());
            }
        }
        baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(baProject, SerializerFeature.WriteMapNullValue));
        try {
            baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public Map selectNumsAndName( List<BaContractStart> list) {
        //发运货量
        BigDecimal checkCoalNum=new BigDecimal(0.00);
        //付款信息
        BigDecimal actualPaymentAmount=new BigDecimal(0.00);
        //认领收款
        BigDecimal applyCollectionAmount=new BigDecimal(0.00);
        //查询销项发票
        BigDecimal invoicedQuantityPic = new BigDecimal(0);
        //进项发票
        BigDecimal inVoIcePic = new BigDecimal(0);
        for (BaContractStart baContractStart:list) {
            //查询对应的运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("start_id",baContractStart.getId());
                /*transportQueryWrapper.eq("confirm_status",2);
                transportQueryWrapper.eq("transport_status",1);*/
            transportQueryWrapper.eq("flag",0);
            List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
            for (BaTransport baTransport:baTransports) {
                //验收信息
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",baTransport.getId());
                checkQueryWrapper.eq("type",1);
                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                for (BaCheck baCheck:baChecks) {
                    checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                }
            }
            //查询付款信息
            QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("start_id",baContractStart.getId());
            paymentQueryWrapper.eq("state","payment:pass");
            //paymentQueryWrapper.eq("payment_state",AdminCodeEnum.PAYMENT_PAID.getCode());
            paymentQueryWrapper.eq("flag",0);
            List<BaPayment> baPayments = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment:baPayments) {
                actualPaymentAmount = actualPaymentAmount.add(baPayment.getApplyPaymentAmount());
            }
            //查询认领金额
            QueryWrapper<BaClaimHistory> historyQueryWrapper = new QueryWrapper<>();
            historyQueryWrapper.eq("order_id",baContractStart.getId());
            List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(historyQueryWrapper);
            for (BaClaimHistory baClaimHistory:baClaimHistories) {
                //查询认领信息
                BaClaim baClaim = baClaimMapper.selectBaClaimById(baClaimHistory.getClaimId());
                if(StringUtils.isNotNull(baClaim)){
                    BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baClaim.getRelationId());
                    //判断认领收款审批通过
                    if(StringUtils.isNotNull(baCollection) && StringUtils.isNotEmpty(baCollection.getState())){
                        if(baCollection.getState().equals(AdminCodeEnum.CLAIM_STATUS_PASS.getCode())){
                            applyCollectionAmount = applyCollectionAmount.add(baClaimHistory.getCurrentCollection());
                        }
                    }
                }
            }
            QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("start_id",baContractStart.getId());
            invoiceQueryWrapper.eq("invoice_type",1);
            invoiceQueryWrapper.eq("state","invoice:pass");
            invoiceQueryWrapper.eq("flag",0);
            List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            for (BaInvoice baInvoice:baInvoices) {
                if(StringUtils.isNotNull(baInvoice)){
                    invoicedQuantityPic = invoicedQuantityPic.add(baInvoice.getInvoicedAmount());
                }
            }
            invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("start_id",baContractStart.getId());
            invoiceQueryWrapper.eq("invoice_type",2);
            invoiceQueryWrapper.eq("state","inputInvoice:pass");
            invoiceQueryWrapper.eq("flag",0);
            baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            for (BaInvoice baInvoice:baInvoices) {
                if(StringUtils.isNotNull(baInvoice)){
                    QueryWrapper<BaInvoiceDetail> detailQueryWrapper = new QueryWrapper<>();
                    detailQueryWrapper.eq("invoice_id",baInvoice.getId());
                    //detailQueryWrapper.eq("flag",0);
                    List<BaInvoiceDetail> baInvoiceDetails = baInvoiceDetailMapper.selectList(detailQueryWrapper);
                    for (BaInvoiceDetail baInvoiceDetail:baInvoiceDetails) {
                        if(StringUtils.isNotNull(baInvoiceDetail)){
                            if(baInvoiceDetail.getInvoicedAmount() != null){
                                inVoIcePic = inVoIcePic.add(baInvoiceDetail.getInvoicedAmount());
                            }
                        }
                    }
                }
            }
        }
        Map map=new HashMap();
        map.put("checkCoalNum",checkCoalNum);
        map.put("actualPaymentAmount",actualPaymentAmount);
        map.put("applyCollectionAmount",applyCollectionAmount);
        map.put("invoicedQuantityPic",invoicedQuantityPic);
        map.put("inVoIcePic",inVoIcePic);
        return map;
    }

    @Override
    public String projectSerial() {
        QueryWrapper<BaProject> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq("project_type","4");
        queryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        queryWrapper.orderByDesc("create_time");
        List<BaProject> baProjects = baProjectMapper.selectList(queryWrapper);
        //序号
        String num = "";
        if(baProjects.size() > 0){
            //判断立项序号是否为空或
            if(StringUtils.isNotEmpty(baProjects.get(0).getProjectSerial())){
                int number = Integer.parseInt(baProjects.get(0).getProjectSerial());
                int serial = number + 1;
                if(String.valueOf(serial).length() == 1){
                    num = "000"+ serial;
                }else if(String.valueOf(serial).length() == 2){
                    num = "00"+ serial;
                }else if(String.valueOf(serial).length() == 3){
                    num = "0"+ serial;
                }else if(String.valueOf(serial).length() == 4){
                    num =  String.valueOf(serial);
                }
            }
        }else {
            return "0001";
        }
        return num;
    }


    @Override
    public int insertBaProjectLink(BaProjectLink baProjectLink) {
        //编辑授权
        if(StringUtils.isNotEmpty(baProjectLink.getId())){
            baProjectLink.setUpdateTime(DateUtils.getNowDate());
            baProjectLink.setUpdateBy(SecurityUtils.getUsername());
           return baProjectLinkMapper.updateBaProjectLink(baProjectLink);
        }
        //新增授权
        baProjectLink.setId(getRedisIncreID.getId());
        baProjectLink.setCreateTime(DateUtils.getNowDate());
        baProjectLink.setCreateBy(SecurityUtils.getUsername());
        if(StringUtils.isNotEmpty(baProjectLink.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baProjectLink.getProjectId());
            baProjectLink.setProjectType(baProject.getProjectType());
        }
        return baProjectLinkMapper.insertBaProjectLink(baProjectLink);
    }

    @Override
    public BaProjectLink selectBaProjectLink(BaProjectLink baProjectLink) {
        //以立项编号查询授权信息
        QueryWrapper<BaProjectLink> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("project_id",baProjectLink.getProjectId());
        BaProjectLink baProjectLink1 = baProjectLinkMapper.selectOne(queryWrapper);

        return baProjectLink1;
    }

    @Override
    public List<BaProjectVO> transportInformation(BaProject baProject) {
        List<BaProjectVO> baProjectVOList = new ArrayList<>();
        //已发数量
        BigDecimal checkCoalNum = new BigDecimal(0.00);
        //合同数量
        BigDecimal tonnageNum = new BigDecimal(0.00);
        //查询立项信息
        baProject.setState("projectStatus:pass");
        baProject.setPepsi("project");
        //租户ID
        baProject.setTenantId(SecurityUtils.getCurrComId());
        List<BaProject> baProjects = baProjectService.selectBaProjectList(baProject);
        if(!CollectionUtils.isEmpty(baProjects)) {
            for(BaProject project : baProjects){
                checkCoalNum = new BigDecimal(0.00);
                //合同数量
                tonnageNum = new BigDecimal(0.00);
                BaProjectVO baProjectVO = beanUtil.beanConvert(project, BaProjectVO.class);
                //根据立项信息查询合同启动
                QueryWrapper<BaContractStart> startQueryWrapper = new QueryWrapper<>();
                startQueryWrapper.eq("project_id",project.getId());
                startQueryWrapper.eq("state","contractStart:pass");
                startQueryWrapper.eq("flag",0);
                List<BaContractStart> baContractStartList = baContractStartMapper.selectList(startQueryWrapper);
                for (BaContractStart baContractStart:baContractStartList) {
                    //统计合同数量
                    if(baContractStart.getTonnage() != null){
                        tonnageNum = tonnageNum.add(baContractStart.getTonnage());
                    }
                    //查询对应的运输信息
                    QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                    transportQueryWrapper.eq("start_id",baContractStart.getId());
                    transportQueryWrapper.eq("flag",0);
                    List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                    for (BaTransport baTransport:baTransports) {
                        //验收信息
                        QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                        checkQueryWrapper.eq("relation_id",baTransport.getId());
                        checkQueryWrapper.eq("type",1);
                        List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                        for (BaCheck baCheck:baChecks) {
                            if(baCheck.getCheckCoalNum() != null){
                                checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                            }
                        }
                    }
                    //判断是否存在火运与汽运
                    QueryWrapper<BaBidTransport> queryWrapper =new QueryWrapper<>();
                    queryWrapper.eq("start_id",baContractStart.getId());
                    List<BaBidTransport> baBidTransports = baBidTransportMapper.selectList(queryWrapper);
                    if(baBidTransports.size() > 0){
                        for (BaBidTransport baBidTransport:baBidTransports) {
                            if(StringUtils.isNotEmpty(baBidTransport.getTransportType())){
                                if(baBidTransport.getTransportType().equals("1")){
                                   baProjectVO.setTrain("1");
                                }else if(baBidTransport.getTransportType().equals("2")){
                                    baProjectVO.setAutomobile("1");
                                }
                            }
                        }
                    }
                }
                //设置已发运量
                baProjectVO.setCheckCoalNum(checkCoalNum);
                //设置合同数量
                baProjectVO.setTonnage(tonnageNum);

                //发起人名称
                if(!ObjectUtils.isEmpty(baProjectVO)){
                    SysUser sysUser = sysUserMapper.selectUserById(baProjectVO.getUserId());
                    if(!ObjectUtils.isEmpty(sysUser)){
                        //根据部门ID获取部门信息
                        //SysDept sysDept = iSysDeptService.selectDeptById(baProjectVO.getDeptId());
                        //根据用户ID查询岗位信息
                        String name = getName.getName(sysUser.getUserId());
                            if(name.equals("")==false){
                                baProjectVO.setUserName(sysUser.getNickName() + "-" + name.substring(0,name.length()-1));
                            }else {
                                baProjectVO.setUserName(sysUser.getNickName());
                            }
                    }
                }
                baProjectVOList.add(baProjectVO);
            }
        }
        return baProjectVOList;
    }

    @Override
    public UR appHomeProjectStat(BaProject baProject) {
        //审批中
        List<BaProject> approveingList = new ArrayList<>();
        //已通过
        List<BaProject> passList = new ArrayList<>();
        //已拒绝
        List<BaProject> rejectList = new ArrayList<>();
        //已撤回
        List<BaProject> withdrawList = new ArrayList<>();

        List<BaProject> baProjects = baProjectMapper.selectProject(baProject);
        if(!CollectionUtils.isEmpty(baProjects)){
            for(BaProject baProject1 : baProjects){
                //审批中
                if(AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode().equals(baProject1.getState())){
                    approveingList.add(baProject1);
                } else if(AdminCodeEnum.PROJECT_STATUS_PASS.getCode().equals(baProject1.getState())){
                    passList.add(baProject1);
                } else if(AdminCodeEnum.PROJECT_STATUS_REJECT.getCode().equals(baProject1.getState())){
                    rejectList.add(baProject1);
                } else if(AdminCodeEnum.PROJECT_STATUS_WITHDRAW.getCode().equals(baProject1.getState())){
                    withdrawList.add(baProject1);
                }
            }
        }
        return UR.ok().data("verifying",approveingList).data("pass",passList).data("reject",rejectList).data("withdraw",withdrawList);
    }

    /**
     * PC端首页立项统计
     */
    @Override
    public int countBaProjectList(CountBaProjectDTO countBaProjectDTO) {
        return baProjectMapper.countBaProjectList(countBaProjectDTO);
    }

    /**
     * 查询立项管理
     *
     * @param businessUserDTO 立项
     * @return 立项管理
     */
    @Override
    public List<String> selectBaProjectByBusinessId(BusinessUserDTO businessUserDTO) {
        //立项发起人ID
        List<String> userIdList = new ArrayList<>();
        userIdList.add(String.valueOf(baProjectMapper.selectBaProjectById(businessUserDTO.getProjectId()).getUserId()));
        return userIdList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int changeBaProject(BaProject baProject) {
        //获取当前登录人角色
        List<SysRole> sysRoles = sysRoleMapper.selectRolesListByUserId(SecurityUtils.getUserId());
        boolean isBusinessAssistant = false; //判断当前登陆人是否是业务助理角色
        if(!CollectionUtils.isEmpty(sysRoles)){
            for(SysRole sysRole : sysRoles){
                //业务总监助理
                if(sysRole.getRoleId() == 21){
                    isBusinessAssistant = true;
                    break;
                }
            }
        }
        //获取原立项信息
        BaProject baProject1 = this.selectBaProjectById(baProject.getId());
        if(baProject1.getNameShorter().equals(baProject.getNameShorter()) == false){
            QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
            projectQueryWrapper.eq("name_shorter", baProject.getNameShorter());
            projectQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                projectQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
            }
            BaProject selectOne = baProjectMapper.selectOne(projectQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        if(baProject1.getName().equals(baProject.getName()) == false){
            QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
            projectQueryWrapper.eq("name", baProject.getName());
            projectQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                projectQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
            }
            BaProject selectOne = baProjectMapper.selectOne(projectQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
//        if("".equals(decideChange(baProject, baProject1))){
//            return -3;
//        }

        //伪删除历史表之前立项
        baHiProjectMapper.deleteBaHiProjectByIds(new String[]{baProject.getId()});

        //定义历史表数据
        BaHiProject baHiProject = new BaHiProject();
        BeanUtils.copyBeanProp(baHiProject, baProject);
        String redisIncreID = getRedisIncreID.getId();
        baHiProject.setId(redisIncreID);

        baProject1.setApproveFlag("2"); //变更
        baProjectMapper.updateBaProject(baProject1);

        baHiProject.setApproveFlag("2"); //变更
        baHiProject.setCreateTime(DateUtils.getNowDate());
        baHiProject.setChangeTime(DateUtils.getNowDate());
        baHiProject.setChangeBy(SecurityUtils.getUsername());
        baHiProject.setCreateBy(SecurityUtils.getUsername());

        //收费标准
        if(StringUtils.isNotNull(baHiProject.getStandards())){
            BaChargingStandards standards = baHiProject.getStandards();
            standards.setId(getRedisIncreID.getId());
            standards.setCreateTime(DateUtils.getNowDate());
            standards.setCreateBy(SecurityUtils.getUsername());
            standards.setRelationId(baHiProject.getId());
            baChargingStandardsMapper.insertBaChargingStandards(standards);
            baHiProject.setStandardsId(standards.getId());
        }

        if(StringUtils.isNotEmpty(baHiProject.getSalesman())) {
            if(StringUtils.isNotEmpty(baHiProject.getSalesman())){
                String salesmanStr = "";
                StringBuilder salesmanStringBuilder = new StringBuilder();
                String[] salesmanArray = baHiProject.getSalesman().split(";");
                for(String s : salesmanArray){
                    if(StringUtils.isNotEmpty(s)){
                        String[] salesmanSplit = s.split(",");
                        salesmanStr = salesmanSplit[salesmanSplit.length-1];
                        if(StringUtils.isNotEmpty(salesmanStr)){
                            String nickName = sysUserMapper.selectUserById(Long.valueOf(salesmanStr)).getNickName();
                            if(salesmanStringBuilder.length() > 0){
                                salesmanStringBuilder.append(",").append(nickName);
                            } else {
                                salesmanStringBuilder.append(nickName);
                            }
                        }
                    }
                }
                baHiProject.setSalesmanName(salesmanStringBuilder.toString());
            }
        }
        if(!baHiProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode()) && !baHiProject.getState().equals(AdminCodeEnum.PROJEC_BUSINESS_STATUS_PASS.getCode())){
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baHiProject.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }

            //业务助理发起流程
            String flowId = "";
            if(isBusinessAssistant){
                flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJEC_BUSINESS.getCode(),SecurityUtils.getCurrComId());
            } else {
                flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(),SecurityUtils.getCurrComId());
            }
            baHiProject.setFlowId(flowId);
            baHiProject.setSubmitTime(DateUtils.getNowDate());
            //审批状态
            baHiProject.setState(AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
            baHiProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
        }
        //拼接归属部门
        if (baHiProject.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(baHiProject.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            List<Integer> integerList = new ArrayList<>();
            for (String deptId:split) {
                integerList.add(Integer.parseInt(deptId));
            }
            //倒叙拿到第一个公司
            Collections.reverse(integerList);
            String deptName = "";
            for (Integer itm:integerList) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(itm));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        deptName = dept1.getDeptName();
                        break;
                    }
                }
            }
            baHiProject.setPartDeptName(deptName + "-" + dept.getDeptName());
        }
        //变更，插入历史信息到历史表
        baHiProject.setRelationId(baProject.getId());
        baHiProject.setState(AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        int result = baHiProjectMapper.insertBaHiProject(baHiProject);
        if (result > 0 && !baHiProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode()) && !baHiProject.getState().equals(AdminCodeEnum.PROJEC_BUSINESS_STATUS_PASS.getCode())) {
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstancesHi(baHiProject, AdminCodeEnum.PROJECT_APPROVAL_CONTENT_UPDATE.getCode(), isBusinessAssistant);
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baHiProject.setState(AdminCodeEnum.PROJECT_STATUS_WITHDRAW.getCode());
                baHiProjectMapper.updateBaHiProject(baHiProject);
                return 0;
            } else {
                //修改仓库选用状态
                if (StringUtils.isNotEmpty(baHiProject.getDepotId())) {
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(baHiProject.getDepotId());
                    baDepot.setIsSelected(baHiProject.getId());
                    baDepotMapper.updateBaDepot(baDepot);
                }
            }
        }
        return result;
    }

    /**
     * 判断是否有字段变更
     */
    public String decideChange(BaProject baProject, BaProject baProjectBefore) {
        String result = "";
        //基本信息
        if(!baProject.getName().equals(baProjectBefore.getName())){
            result = "isStart";
        } else if((ObjectUtils.isEmpty(baProject.getNameShorter()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getNameShorter())) || (
                !ObjectUtils.isEmpty(baProject.getNameShorter()) &&
                        !baProject.getNameShorter()
                                .equals(baProjectBefore.getNameShorter()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baProject.getProjectSerial()) &&
                StringUtils.isNotEmpty(baProjectBefore.getProjectSerial())) || (
                StringUtils.isNotEmpty(baProject.getProjectSerial()) &&
                        !baProject.getProjectSerial()
                                .equals(baProjectBefore.getProjectSerial()))){
            result = "isStart";
        } else if((baProject.getSubsidiaryName() != null && baProjectBefore.getSubsidiaryName() != null) ||
                baProject.getSubsidiaryName() != null && baProject.getSubsidiaryName() != baProjectBefore.getSubsidiaryName()){
            result = "isStart";
        } else if((StringUtils.isEmpty(baProject.getEnterpriseId()) &&
                StringUtils.isNotEmpty(baProjectBefore.getEnterpriseId())) || (
                StringUtils.isNotEmpty(baProject.getEnterpriseId()) &&
                        !baProject.getEnterpriseId()
                                .equals(baProjectBefore.getEnterpriseId()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baProject.getDownstreamTraders()) &&
                StringUtils.isNotEmpty(baProjectBefore.getDownstreamTraders())) || (
                StringUtils.isNotEmpty(baProject.getDownstreamTraders()) &&
                        !baProject.getDownstreamTraders()
                                .equals(baProjectBefore.getDownstreamTraders()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baProject.getGoodsName()) &&
                StringUtils.isNotEmpty(baProjectBefore.getGoodsName())) || (
                StringUtils.isNotEmpty(baProject.getGoodsName()) &&
                        !baProject.getGoodsName()
                                .equals(baProjectBefore.getGoodsName()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baProject.getGoodsSourceLand()) &&
                StringUtils.isNotEmpty(baProjectBefore.getGoodsSourceLand())) || (
                StringUtils.isNotEmpty(baProject.getGoodsSourceLand()) &&
                        !baProject.getGoodsSourceLand()
                                .equals(baProjectBefore.getGoodsSourceLand()))){
            result = "isStart";
        }
        if((StringUtils.isEmpty(baProject.getPaymentCycle()) &&
                StringUtils.isNotEmpty(baProjectBefore.getPaymentCycle())) || (
                StringUtils.isNotEmpty(baProject.getPaymentCycle()) &&
                        !baProject.getPaymentCycle()
                                .equals(baProjectBefore.getPaymentCycle()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getCurrentPrice()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getCurrentPrice())) || (
                !ObjectUtils.isEmpty(baProject.getCurrentPrice()) &&
                        !baProject.getCurrentPrice()
                                .equals(baProjectBefore.getCurrentPrice()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getUpperLimit()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getUpperLimit())) || (
                !ObjectUtils.isEmpty(baProject.getUpperLimit()) &&
                        !baProject.getUpperLimit()
                                .equals(baProjectBefore.getUpperLimit()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getShippingNum()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getShippingNum())) || (
                !ObjectUtils.isEmpty(baProject.getShippingNum()) &&
                        !baProject.getShippingNum()
                                .equals(baProjectBefore.getShippingNum()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getMonthlyDemand()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getMonthlyDemand())) || (
                !ObjectUtils.isEmpty(baProject.getMonthlyDemand()) &&
                        !baProject.getMonthlyDemand()
                                .equals(baProjectBefore.getMonthlyDemand()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getRemarks()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getRemarks())) || (
                !ObjectUtils.isEmpty(baProject.getRemarks()) &&
                        !baProject.getRemarks()
                                .equals(baProjectBefore.getRemarks()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getSettlementProportion()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getSettlementProportion())) || (
                !ObjectUtils.isEmpty(baProject.getSettlementProportion()) &&
                        !baProject.getSettlementProportion()
                                .equals(baProjectBefore.getSettlementProportion()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getAnnex()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getAnnex())) || (
                !ObjectUtils.isEmpty(baProject.getAnnex()) &&
                        !baProject.getAnnex()
                                .equals(baProjectBefore.getAnnex()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getStandards()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getStandards())) || (
                !ObjectUtils.isEmpty(baProject.getStandards()) &&
                        !baProject.getStandards()
                                .equals(baProjectBefore.getStandards()))){
            result = "isStart";
        }
        BaChargingStandards standards = baProject.getStandards();
        BaChargingStandards standardsBefore = baProjectBefore.getStandards();
        if((!ObjectUtils.isEmpty(standards) && ObjectUtils.isEmpty(standardsBefore)) ||
                (ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore))
        ){
            result = "isStart";
        } else if(!ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore)){
            if((ObjectUtils.isEmpty(standards.getReserveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReserveTon())) || (
                    !ObjectUtils.isEmpty(standards.getReserveTon()) &&
                            !standards.getReserveTon()
                                    .equals(standardsBefore.getReserveTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getArriveAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getArriveAnnualized()) &&
                            !standards.getArriveAnnualized()
                                    .equals(standardsBefore.getArriveAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getArriveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveTon())) || (
                    !ObjectUtils.isEmpty(standards.getArriveTon()) &&
                            !standards.getArriveTon()
                                    .equals(standardsBefore.getArriveTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getVehicleAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleAnnualized()) &&
                            !standards.getVehicleAnnualized()
                                    .equals(standardsBefore.getVehicleAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getVehicleTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleTon())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleTon()) &&
                            !standards.getVehicleTon()
                                    .equals(standardsBefore.getVehicleTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveFreightAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                            !standards.getArriveFreightAnnualized()
                                    .equals(standardsBefore.getArriveFreightAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getArriveFreightTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveFreightTon())) || (
                    !ObjectUtils.isEmpty(standards.getArriveFreightTon()) &&
                            !standards.getArriveFreightTon()
                                    .equals(standardsBefore.getArriveFreightTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleFreightAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                            !standards.getVehicleFreightAnnualized()
                                    .equals(standardsBefore.getVehicleFreightAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getVehicleFreightTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleFreightTon())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleFreightTon()) &&
                            !standards.getVehicleFreightTon()
                                    .equals(standardsBefore.getVehicleFreightTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReplacePurchaseAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                            !standards.getReplacePurchaseAnnualized()
                                    .equals(standardsBefore.getReplacePurchaseAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReplacePurchaseTon())) || (
                    !ObjectUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                            !standards.getReplacePurchaseTon()
                                    .equals(standardsBefore.getReplacePurchaseTon()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getAdvancePaymentAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                            !standards.getAdvancePaymentAnnualized()
                                    .equals(standardsBefore.getAdvancePaymentAnnualized()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getAdvancePaymentTon())) || (
                    !ObjectUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                            !standards.getAdvancePaymentTon()
                                    .equals(standardsBefore.getAdvancePaymentTon()))){
                result = "isStart";
            }
        }
        //保证金及违约考核
        if((ObjectUtils.isEmpty(baProject.getBondCheck()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getBondCheck())) || (
                !ObjectUtils.isEmpty(baProject.getBondCheck()) &&
                        !baProject.getBondCheck()
                                .equals(baProjectBefore.getBondCheck()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getPerformancePenaltyRules()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getPerformancePenaltyRules())) || (
                !ObjectUtils.isEmpty(baProject.getPerformancePenaltyRules()) &&
                        !baProject.getPerformancePenaltyRules()
                                .equals(baProjectBefore.getPerformancePenaltyRules()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getDefaultAssessmentRules()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getDefaultAssessmentRules())) || (
                !ObjectUtils.isEmpty(baProject.getDefaultAssessmentRules()) &&
                        !baProject.getDefaultAssessmentRules()
                                .equals(baProjectBefore.getDefaultAssessmentRules()))){
            result = "isStart";
        }
        //风险把控
        if((ObjectUtils.isEmpty(baProject.getShippingProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getShippingProcess())) || (
                !ObjectUtils.isEmpty(baProject.getShippingProcess()) &&
                        !baProject.getShippingProcess()
                                .equals(baProjectBefore.getShippingProcess()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getCollectionProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getCollectionProcess())) || (
                !ObjectUtils.isEmpty(baProject.getCollectionProcess()) &&
                        !baProject.getCollectionProcess()
                                .equals(baProjectBefore.getCollectionProcess()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getPaymentProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getPaymentProcess())) || (
                !ObjectUtils.isEmpty(baProject.getPaymentProcess()) &&
                        !baProject.getPaymentProcess()
                                .equals(baProjectBefore.getPaymentProcess()))){
            result = "isStart";
        }
        //人员定责
        if((ObjectUtils.isEmpty(baProject.getServiceManagerName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getServiceManagerName())) || (
                !ObjectUtils.isEmpty(baProject.getServiceManagerName()) &&
                        !baProject.getServiceManagerName()
                                .equals(baProjectBefore.getServiceManagerName()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getDivisionName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getDivisionName())) || (
                !ObjectUtils.isEmpty(baProject.getDivisionName()) &&
                        !baProject.getDivisionName()
                                .equals(baProjectBefore.getDivisionName()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getSalesmanName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getSalesmanName())) || (
                !ObjectUtils.isEmpty(baProject.getSalesmanName()) &&
                        !baProject.getSalesmanName()
                                .equals(baProjectBefore.getSalesmanName()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getPersonChargeName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getPersonChargeName())) || (
                !ObjectUtils.isEmpty(baProject.getPersonChargeName()) &&
                        !baProject.getPersonChargeName()
                                .equals(baProjectBefore.getPersonChargeName()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getLowExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getLowExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baProject.getLowExternalContactPerson()) &&
                        !baProject.getLowExternalContactPerson()
                                .equals(baProjectBefore.getLowExternalContactPerson()))){
            result = "isStart";
        }
        if((ObjectUtils.isEmpty(baProject.getExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baProject.getExternalContactPerson()) &&
                        !baProject.getExternalContactPerson()
                                .equals(baProjectBefore.getExternalContactPerson()))){
            result = "isStart";
        }
        return result;
    }


    @Override
    public List<String> selectChangeDataList(String id) {
        BaHiProject baHiProject = baHiProjectService.selectBaHiProjectById(id);
        if(!ObjectUtils.isEmpty(baHiProject)){
            BaProject baProject = this.selectBaProjectById(baHiProject.getRelationId());
            return getChangeData(baProject, baHiProject);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BaProject> selectBaProjectAuthorizedList(BaProject baProject) {
        List<BaProject> baProjects = baProjectMapper.selectProjectAuthorized(baProject);

        if (baProjects.size() > 0){
            baProjects.stream().forEach(item ->{
                //供应商
                StringBuilder stringBuilder = new StringBuilder();
                //供应商
                if (StringUtils.isNotEmpty(item.getSupplierId())) {
                    String[] supplierIdArray = item.getSupplierId().split(",");
                    for(String supplierId : supplierIdArray){
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                        if(!ObjectUtils.isEmpty(baSupplier)){
                            if(stringBuilder.length() == 0){
                                stringBuilder.append(baSupplier.getName());
                            } else {
                                stringBuilder.append(",").append(baSupplier.getName());
                            }
                        }
                        item.setSupplierName(stringBuilder.toString());
                    }
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baProject.getSupplierId());
                    item.setSupplierName(baSupplier.getName());
                }
                //下游贸易商
                if (StringUtils.isNotEmpty(item.getDownstreamTraders())) {
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getDownstreamTraders());
                    item.setDownstreamTradersName(baCompany.getName());
                }
                //用煤单位
                if (StringUtils.isNotEmpty(item.getEnterpriseId())) {
                    BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getEnterpriseId());
                    item.setEnterpriseName(baEnterprise.getName());
                    BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                    if(!ObjectUtils.isEmpty(baCompanyGroup)){
                        baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
                        baEnterprise.setBaCompanyGroup(baCompanyGroup);
                    }
                    item.setBaEnterprise(baEnterprise);
                    item.setSpliceName(baEnterprise.getName());
                }
                //实际发起人
                if (StringUtils.isNotEmpty(item.getCounterpartId())) {
                    BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(item.getCounterpartId());
                    item.setCounterpartName(baCounterpart.getName());
                    item.setSpliceName(baCounterpart.getName());
                }
                //发起人
                SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());


                //拼接归属部门
                if (sysUser.getDeptId() != null) {
                    SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                    //查询祖籍
                    String[] split = dept.getAncestors().split(",");
                    List<Integer> integerList = new ArrayList<>();
                    for (String deptId:split) {
                        integerList.add(Integer.parseInt(deptId));
                    }
                    //倒叙拿到第一个公司
                    Collections.reverse(integerList);
                    for (Integer itm:integerList) {
                        SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(itm));
                        if(StringUtils.isNotNull(dept1)){
                            if(dept1.getDeptType().equals("1")){
                                sysUser.setPartDeptCompany(dept1.getDeptName());
                                break;
                            }
                        }
                    }
                    sysUser.setPartDeptName(sysUser.getPartDeptCompany()+"-"+dept.getDeptName());
                }
                item.setUser(sysUser);

                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUser.getNickName());
                }
                //仓库信息
                if(StringUtils.isNotEmpty(item.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(item.getDepotId());
                    if(StringUtils.isNotNull(baDepot)){
                        item.setDepotName(baDepot.getName());
                    }
                }

                if("4".equals(item.getProjectType())) { //判断是否是常规立项
                    if (StringUtils.isNotEmpty(item.getGoodsId())) {
                        QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                        if(StringUtils.isNotEmpty(item.getGoodsId())){
                            List<String> goodsIdList = Arrays.asList(item.getGoodsId().split(","));
                            baGoodsQueryWrapper.in("id", goodsIdList);
                            baGoodsQueryWrapper.eq("flag", 0);
                            List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                            //绑定商品对象
                            item.setGoodsList(goodsList);
                            if(!CollectionUtils.isEmpty(goodsList)){
                                StringBuilder goodName = new StringBuilder();
                                for(BaGoods baGoods : goodsList){
                                    if(goodName.length() > 0){
                                        goodName.append(",").append(baGoods.getName());
                                    } else {
                                        goodName.append(baGoods.getName());
                                    }
                                }
                                item.setGoodsName(goodName.toString());
                            }
                        }
                    }
                } else {
                    if (StringUtils.isNotEmpty(item.getGoodsType())) {
                        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(item.getGoodsType());
                        if (!ObjectUtils.isEmpty(baGoods)) {
                            item.setGoodsName(baGoods.getName());
                        } else {
                            if(StringUtils.isNotEmpty(item.getGoodsType())){
                                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(item.getGoodsType());
                                if(!ObjectUtils.isEmpty(baGoodsType)){
                                    item.setGoodsName(baGoodsType.getName());
                                }
                            }
                        }
                    }
                }
                //收费标准
                if(StringUtils.isNotEmpty(item.getStandardsId())){
                    BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(item.getStandardsId());
                    item.setStandards(baChargingStandards);
                }
                //子公司
                if(StringUtils.isNotEmpty(item.getSubsidiary())){
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubsidiary()));
                    item.setSubsidiaryName(dept.getDeptName());
                    item.setSubsidiaryAbbreviation(dept.getAbbreviation());
                }
                List<String> processInstanceIds = new ArrayList<>();
                //流程实例ID
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", item.getId());
                queryWrapper.eq("flag", 0);
                //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
                queryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                }
                item.setProcessInstanceId(processInstanceIds);

                //主体公司
                if(StringUtils.isNotEmpty(item.getSubjectCompany())){
                    //子公司
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubjectCompany()));
                    if(!ObjectUtils.isEmpty(sysDept)){
                        item.setSubjectCompanyName(sysDept.getDeptName());
                    }
                }
                //货主
                if(StringUtils.isNotEmpty(item.getCustomId())){
                    BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(item.getCustomId());
                    if(!ObjectUtils.isEmpty(baCargoOwner)){
                        item.setCustomName(baCargoOwner.getName());
                    }
                }
            });
        }

        return baProjects;
    }

    @Override
    public int authorizedBaProject(BaProject baProject) {
        return baProjectMapper.updateBaProject(baProject);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> getChangeData(BaProject baProject, BaHiProject baProjectBefore) {
        List<String> isChangeList = new ArrayList<>();
        //基本信息
        //基本信息
        if(!baProject.getName().equals(baProjectBefore.getName())){
            isChangeList.add("name");
        }
        if((ObjectUtils.isEmpty(baProject.getNameShorter()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getNameShorter())) || (
                !ObjectUtils.isEmpty(baProject.getNameShorter()) &&
                        !baProject.getNameShorter()
                                .equals(baProjectBefore.getNameShorter()))){
            isChangeList.add("nameShorter");
        }
        if((StringUtils.isEmpty(baProject.getProjectSerial()) &&
                StringUtils.isNotEmpty(baProjectBefore.getProjectSerial())) || (
                StringUtils.isNotEmpty(baProject.getProjectSerial()) &&
                        !baProject.getProjectSerial()
                                .equals(baProjectBefore.getProjectSerial()))){
            isChangeList.add("projectSerial");
        }
        if((StringUtils.isEmpty(baProject.getSubsidiaryName()) &&
                    StringUtils.isNotEmpty(baProjectBefore.getSubsidiaryName())) || (
                    StringUtils.isNotEmpty(baProject.getSubsidiaryName()) &&
                            !baProject.getSubsidiaryName()
                                    .equals(baProjectBefore.getSubsidiaryName()))){
            isChangeList.add("subsidiaryName");
        }
        if((StringUtils.isEmpty(baProject.getTransportType()) &&
                StringUtils.isNotEmpty(baProjectBefore.getTransportType())) || (
                StringUtils.isNotEmpty(baProject.getTransportType()) &&
                        !baProject.getTransportType()
                                .equals(baProjectBefore.getTransportType()))){
            isChangeList.add("transportType");
        }
        if((StringUtils.isEmpty(baProject.getEnterpriseId()) &&
                StringUtils.isNotEmpty(baProjectBefore.getEnterpriseId())) || (
                StringUtils.isNotEmpty(baProject.getEnterpriseId()) &&
                        !baProject.getEnterpriseId()
                                .equals(baProjectBefore.getEnterpriseId()))){
            isChangeList.add("enterpriseName");
        }
        if((StringUtils.isEmpty(baProject.getDownstreamTraders()) &&
                StringUtils.isNotEmpty(baProjectBefore.getDownstreamTraders())) || (
                StringUtils.isNotEmpty(baProject.getDownstreamTraders()) &&
                        !baProject.getDownstreamTraders()
                                .equals(baProjectBefore.getDownstreamTraders()))){
            isChangeList.add("downstreamTraders");
        }
        if((StringUtils.isEmpty(baProject.getGoodsName()) &&
                StringUtils.isNotEmpty(baProjectBefore.getGoodsName())) || (
                StringUtils.isNotEmpty(baProject.getGoodsName()) &&
                        !baProject.getGoodsName()
                                .equals(baProjectBefore.getGoodsName()))){
            isChangeList.add("goodsName");
        }
        if((StringUtils.isEmpty(baProject.getGoodsSourceLand()) &&
                StringUtils.isNotEmpty(baProjectBefore.getGoodsSourceLand())) || (
                StringUtils.isNotEmpty(baProject.getGoodsSourceLand()) &&
                        !baProject.getGoodsSourceLand()
                                .equals(baProjectBefore.getGoodsSourceLand()))){
            isChangeList.add("goodsSourceLand");
        }
        if((StringUtils.isEmpty(baProject.getPaymentCycle()) &&
                StringUtils.isNotEmpty(baProjectBefore.getPaymentCycle())) || (
                StringUtils.isNotEmpty(baProject.getPaymentCycle()) &&
                        !baProject.getPaymentCycle()
                                .equals(baProjectBefore.getPaymentCycle()))){
            isChangeList.add("paymentCycle");
        }
        if((ObjectUtils.isEmpty(baProject.getCurrentPrice()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getCurrentPrice())) || (
                !ObjectUtils.isEmpty(baProject.getCurrentPrice()) &&
                        !baProject.getCurrentPrice()
                                .equals(baProjectBefore.getCurrentPrice()))){
            isChangeList.add("currentPrice");
        }
        if((ObjectUtils.isEmpty(baProject.getUpperLimit()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getUpperLimit())) || (
                !ObjectUtils.isEmpty(baProject.getUpperLimit()) &&
                        !baProject.getUpperLimit()
                                .equals(baProjectBefore.getUpperLimit()))){
            isChangeList.add("upperLimit");
        }
        if((ObjectUtils.isEmpty(baProject.getShippingNum()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getShippingNum())) || (
                !ObjectUtils.isEmpty(baProject.getShippingNum()) &&
                        !baProject.getShippingNum()
                                .equals(baProjectBefore.getShippingNum()))){
            isChangeList.add("shippingNum");
        }
        if((ObjectUtils.isEmpty(baProject.getMonthlyDemand()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getMonthlyDemand())) || (
                !ObjectUtils.isEmpty(baProject.getMonthlyDemand()) &&
                        !baProject.getMonthlyDemand()
                                .equals(baProjectBefore.getMonthlyDemand()))){
            isChangeList.add("monthlyDemand");
        }
        if((ObjectUtils.isEmpty(baProject.getRemarks()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getRemarks())) || (
                !ObjectUtils.isEmpty(baProject.getRemarks()) &&
                        !baProject.getRemarks()
                                .equals(baProjectBefore.getRemarks()))){
            isChangeList.add("remarks");
        }
        if((ObjectUtils.isEmpty(baProject.getSettlementProportion()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getSettlementProportion())) || (
                !ObjectUtils.isEmpty(baProject.getSettlementProportion()) &&
                        !baProject.getSettlementProportion()
                                .equals(baProjectBefore.getSettlementProportion()))){
            isChangeList.add("settlementProportion");
        }
        if((ObjectUtils.isEmpty(baProject.getAnnex()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getAnnex())) || (
                !ObjectUtils.isEmpty(baProject.getAnnex()) &&
                        !baProject.getAnnex()
                                .equals(baProjectBefore.getAnnex()))){
            isChangeList.add("annex");
        }
        BaChargingStandards standards = baProject.getStandards();
        BaChargingStandards standardsBefore = baProjectBefore.getStandards();
        if(!ObjectUtils.isEmpty(standards) && !ObjectUtils.isEmpty(standardsBefore)){
            if((ObjectUtils.isEmpty(standards.getReserveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReserveTon())) || (
                    !ObjectUtils.isEmpty(standards.getReserveTon()) &&
                            !standards.getReserveTon()
                                    .equals(standardsBefore.getReserveTon()))){
                isChangeList.add("reserveTon");
            }
            if((ObjectUtils.isEmpty(standards.getArriveAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getArriveAnnualized()) &&
                            !standards.getArriveAnnualized()
                                    .equals(standardsBefore.getArriveAnnualized()))){
                isChangeList.add("arriveAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getArriveTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveTon())) || (
                    !ObjectUtils.isEmpty(standards.getArriveTon()) &&
                            !standards.getArriveTon()
                                    .equals(standardsBefore.getArriveTon()))){
                isChangeList.add("arriveTon");
            }
            if((ObjectUtils.isEmpty(standards.getVehicleAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleAnnualized()) &&
                            !standards.getVehicleAnnualized()
                                    .equals(standardsBefore.getVehicleAnnualized()))){
                isChangeList.add("vehicleAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getVehicleTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleTon())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleTon()) &&
                            !standards.getVehicleTon()
                                    .equals(standardsBefore.getVehicleTon()))){
                isChangeList.add("vehicleTon");
            }
            if((ObjectUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveFreightAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getArriveFreightAnnualized()) &&
                            !standards.getArriveFreightAnnualized()
                                    .equals(standardsBefore.getArriveFreightAnnualized()))){
                isChangeList.add("arriveFreightAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getArriveFreightTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getArriveFreightTon())) || (
                    !ObjectUtils.isEmpty(standards.getArriveFreightTon()) &&
                            !standards.getArriveFreightTon()
                                    .equals(standardsBefore.getArriveFreightTon()))){
                isChangeList.add("arriveFreightTon");
            }
            if((ObjectUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleFreightAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleFreightAnnualized()) &&
                            !standards.getVehicleFreightAnnualized()
                                    .equals(standardsBefore.getVehicleFreightAnnualized()))){
                isChangeList.add("vehicleFreightAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getVehicleFreightTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getVehicleFreightTon())) || (
                    !ObjectUtils.isEmpty(standards.getVehicleFreightTon()) &&
                            !standards.getVehicleFreightTon()
                                    .equals(standardsBefore.getVehicleFreightTon()))){
                isChangeList.add("vehicleFreightTon");
            }
            if((ObjectUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReplacePurchaseAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getReplacePurchaseAnnualized()) &&
                            !standards.getReplacePurchaseAnnualized()
                                    .equals(standardsBefore.getReplacePurchaseAnnualized()))){
                isChangeList.add("replacePurchaseAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getReplacePurchaseTon())) || (
                    !ObjectUtils.isEmpty(standards.getReplacePurchaseTon()) &&
                            !standards.getReplacePurchaseTon()
                                    .equals(standardsBefore.getReplacePurchaseTon()))){
                isChangeList.add("replacePurchaseTon");
            }
            if((ObjectUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getAdvancePaymentAnnualized())) || (
                    !ObjectUtils.isEmpty(standards.getAdvancePaymentAnnualized()) &&
                            !standards.getAdvancePaymentAnnualized()
                                    .equals(standardsBefore.getAdvancePaymentAnnualized()))){
                isChangeList.add("advancePaymentAnnualized");
            }
            if((ObjectUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                    !ObjectUtils.isEmpty(standardsBefore.getAdvancePaymentTon())) || (
                    !ObjectUtils.isEmpty(standards.getAdvancePaymentTon()) &&
                            !standards.getAdvancePaymentTon()
                                    .equals(standardsBefore.getAdvancePaymentTon()))){
                isChangeList.add("advancePaymentTon");
            }
        }
        //保证金及违约考核
        if((StringUtils.isEmpty(baProject.getBondCheck()) &&
                !StringUtils.isEmpty(baProjectBefore.getBondCheck())) || (
                !StringUtils.isEmpty(baProject.getBondCheck()) &&
                        !baProject.getBondCheck()
                                .equals(baProjectBefore.getBondCheck()))){
            isChangeList.add("bondCheck");
        }
        if((StringUtils.isEmpty(baProject.getPerformancePenaltyRules()) &&
                !StringUtils.isEmpty(baProjectBefore.getPerformancePenaltyRules())) || (
                !StringUtils.isEmpty(baProject.getPerformancePenaltyRules()) &&
                        !baProject.getPerformancePenaltyRules()
                                .equals(baProjectBefore.getPerformancePenaltyRules()))){
            isChangeList.add("performancePenaltyRules");
        }
        if((StringUtils.isEmpty(baProject.getDefaultAssessmentRules()) &&
                !StringUtils.isEmpty(baProjectBefore.getDefaultAssessmentRules())) || (
                !StringUtils.isEmpty(baProject.getDefaultAssessmentRules()) &&
                        !baProject.getDefaultAssessmentRules()
                                .equals(baProjectBefore.getDefaultAssessmentRules()))){
            isChangeList.add("defaultAssessmentRules");
        }
        //风险把控
        if((ObjectUtils.isEmpty(baProject.getShippingProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getShippingProcess())) || (
                !ObjectUtils.isEmpty(baProject.getShippingProcess()) &&
                        !baProject.getShippingProcess()
                                .equals(baProjectBefore.getShippingProcess()))){
            isChangeList.add("shippingProcess");
        }
        if((ObjectUtils.isEmpty(baProject.getCollectionProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getCollectionProcess())) || (
                !ObjectUtils.isEmpty(baProject.getCollectionProcess()) &&
                        !baProject.getCollectionProcess()
                                .equals(baProjectBefore.getCollectionProcess()))){
            isChangeList.add("collectionProcess");
        }
        if((ObjectUtils.isEmpty(baProject.getPaymentProcess()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getPaymentProcess())) || (
                !ObjectUtils.isEmpty(baProject.getPaymentProcess()) &&
                        !baProject.getPaymentProcess()
                                .equals(baProjectBefore.getPaymentProcess()))){
            isChangeList.add("paymentProcess");
        }
        //人员定责
        if((ObjectUtils.isEmpty(baProject.getServiceManagerName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getServiceManagerName())) || (
                !ObjectUtils.isEmpty(baProject.getServiceManagerName()) &&
                        !baProject.getServiceManagerName()
                                .equals(baProjectBefore.getServiceManagerName()))){
            isChangeList.add("serviceManagerName");
        }
        if((ObjectUtils.isEmpty(baProject.getDivisionName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getDivisionName())) || (
                !ObjectUtils.isEmpty(baProject.getDivisionName()) &&
                        !baProject.getDivisionName()
                                .equals(baProjectBefore.getDivisionName()))){
            isChangeList.add("divisionName");
        }
        if((ObjectUtils.isEmpty(baProject.getSalesmanName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getSalesmanName())) || (
                !ObjectUtils.isEmpty(baProject.getSalesmanName()) &&
                        !baProject.getSalesmanName()
                                .equals(baProjectBefore.getSalesmanName()))){
            isChangeList.add("salesmanName");
        }
        if((ObjectUtils.isEmpty(baProject.getPersonChargeName()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getPersonChargeName())) || (
                !ObjectUtils.isEmpty(baProject.getPersonChargeName()) &&
                        !baProject.getPersonChargeName()
                                .equals(baProjectBefore.getPersonChargeName()))){
            isChangeList.add("personChargeName");
        }
        if((ObjectUtils.isEmpty(baProject.getLowExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getLowExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baProject.getLowExternalContactPerson()) &&
                        !baProject.getLowExternalContactPerson()
                                .equals(baProjectBefore.getLowExternalContactPerson()))){
            isChangeList.add("lowExternalContactPerson");
        }
        if((ObjectUtils.isEmpty(baProject.getExternalContactPerson()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getExternalContactPerson())) || (
                !ObjectUtils.isEmpty(baProject.getExternalContactPerson()) &&
                        !baProject.getExternalContactPerson()
                                .equals(baProjectBefore.getExternalContactPerson()))){
            isChangeList.add("externalContactPerson");
        }
        if((ObjectUtils.isEmpty(baProject.getAccountUpperLimit()) &&
                !ObjectUtils.isEmpty(baProjectBefore.getAccountUpperLimit())) || (
                !ObjectUtils.isEmpty(baProject.getAccountUpperLimit()) &&
                        !baProject.getAccountUpperLimit()
                                .equals(baProjectBefore.getAccountUpperLimit()))){
            isChangeList.add("accountUpperLimit");
        }

        return isChangeList;
    }

    /**
     * 名称转字母
     */
    public String nameConversion(String name) {
        StringBuilder sb = new StringBuilder();
        char[] nameCharArray = name.toCharArray();
        HanyuPinyinOutputFormat outputFormat = new HanyuPinyinOutputFormat();
        outputFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        outputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

        for (int i = 0; i < nameCharArray.length; i++) {
            if (Character.toString(nameCharArray[i]).matches("[\u4e00-\u9fa5]")) { // 判断是否为中文字符
                try {
                    String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(nameCharArray[i], outputFormat);
                    sb.append(pinyinArray[0].charAt(0));
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                sb.append(nameCharArray[i]);
            }
        }
        return sb.toString();
    }
}
