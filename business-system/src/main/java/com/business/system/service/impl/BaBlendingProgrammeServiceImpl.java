package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaBlendingProportioning;
import com.business.system.mapper.BaBlendingProportioningMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaBlendingProportioningService;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaBlendingProgrammeMapper;
import com.business.system.domain.BaBlendingProgramme;
import com.business.system.service.IBaBlendingProgrammeService;

/**
 * 配煤方案Service业务层处理
 *
 * @author ljb
 * @date 2024-01-05
 */
@Service
public class BaBlendingProgrammeServiceImpl extends ServiceImpl<BaBlendingProgrammeMapper, BaBlendingProgramme> implements IBaBlendingProgrammeService
{
    @Autowired
    private BaBlendingProgrammeMapper baBlendingProgrammeMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaBlendingProportioningService iBaBlendingProportioningService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaBlendingProportioningMapper baBlendingProportioningMapper;

    /**
     * 查询配煤方案
     *
     * @param id 配煤方案ID
     * @return 配煤方案
     */
    @Override
    public BaBlendingProgramme selectBaBlendingProgrammeById(String id)
    {
        BaBlendingProgramme baBlendingProgramme = baBlendingProgrammeMapper.selectBaBlendingProgrammeById(id);
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baBlendingProgramme.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baBlendingProgramme.setUser(sysUser);
        //配煤配比信息
        QueryWrapper<BaBlendingProportioning> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",id);
        List<BaBlendingProportioning> baBlendingProportionings = baBlendingProportioningMapper.selectList(queryWrapper);
        baBlendingProgramme.setBaBlendingProportioningList(baBlendingProportionings);
        return baBlendingProgramme;
    }

    /**
     * 查询配煤方案列表
     *
     * @param baBlendingProgramme 配煤方案
     * @return 配煤方案
     */
    @Override
    public List<BaBlendingProgramme> selectBaBlendingProgrammeList(BaBlendingProgramme baBlendingProgramme)
    {
        return baBlendingProgrammeMapper.selectBaBlendingProgrammeList(baBlendingProgramme);
    }

    /**
     * 新增配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    @Override
    public int insertBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme)
    {
        QueryWrapper<BaBlendingProgramme> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baBlendingProgramme.getName());
        queryWrapper.eq("flag",0);
        BaBlendingProgramme blendingProgramme = baBlendingProgrammeMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(blendingProgramme)){
            return 0;
        }
        baBlendingProgramme.setId(getRedisIncreID.getId());
        baBlendingProgramme.setCreateTime(DateUtils.getNowDate());
        baBlendingProgramme.setUserId(SecurityUtils.getUserId());
        baBlendingProgramme.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baBlendingProgramme.setTenantId(SecurityUtils.getCurrComId());
        //配煤配比
        if(baBlendingProgramme.getBaBlendingProportioningList().size()>0){
            for (BaBlendingProportioning baBlendingProportioning:baBlendingProgramme.getBaBlendingProportioningList()) {
                baBlendingProportioning.setId(getRedisIncreID.getId());
                baBlendingProportioning.setRelationId(baBlendingProgramme.getId());
                iBaBlendingProportioningService.insertBaBlendingProportioning(baBlendingProportioning);
            }
        }
        return baBlendingProgrammeMapper.insertBaBlendingProgramme(baBlendingProgramme);
    }

    /**
     * 修改配煤方案
     *
     * @param baBlendingProgramme 配煤方案
     * @return 结果
     */
    @Override
    public int updateBaBlendingProgramme(BaBlendingProgramme baBlendingProgramme)
    {
        baBlendingProgramme.setUpdateTime(DateUtils.getNowDate());
        //删除配煤配比信息
        QueryWrapper<BaBlendingProportioning> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baBlendingProgramme.getId());
        List<BaBlendingProportioning> baBlendingProportionings = baBlendingProportioningMapper.selectList(queryWrapper);
        for (BaBlendingProportioning blendingProportioning:baBlendingProportionings) {
            baBlendingProportioningMapper.deleteBaBlendingProportioningById(blendingProportioning.getId());
        }
        //编辑新增配煤信息
        if(baBlendingProgramme.getBaBlendingProportioningList().size() > 0){
            for (BaBlendingProportioning blendingProportioning:baBlendingProgramme.getBaBlendingProportioningList()) {
                if(StringUtils.isEmpty(blendingProportioning.getId())){
                    blendingProportioning.setId(getRedisIncreID.getId());
                    blendingProportioning.setRelationId(baBlendingProgramme.getId());
                }
                baBlendingProportioningMapper.insertBaBlendingProportioning(blendingProportioning);
            }
        }
        return baBlendingProgrammeMapper.updateBaBlendingProgramme(baBlendingProgramme);
    }

    /**
     * 批量删除配煤方案
     *
     * @param ids 需要删除的配煤方案ID
     * @return 结果
     */
    @Override
    public int deleteBaBlendingProgrammeByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaBlendingProgramme baBlendingProgramme = baBlendingProgrammeMapper.selectBaBlendingProgrammeById(id);
                baBlendingProgramme.setFlag(1L);
                baBlendingProgrammeMapper.updateBaBlendingProgramme(baBlendingProgramme);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除配煤方案信息
     *
     * @param id 配煤方案ID
     * @return 结果
     */
    @Override
    public int deleteBaBlendingProgrammeById(String id)
    {
        return baBlendingProgrammeMapper.deleteBaBlendingProgrammeById(id);
    }

}
