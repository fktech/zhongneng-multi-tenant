package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaServiceVoucher;
import com.business.system.domain.BaServiceVoucherRecord;

/**
 * 进厂确认单Service接口
 *
 * @author single
 * @date 2023-09-19
 */
public interface IBaServiceVoucherService
{
    /**
     * 查询进厂确认单
     *
     * @param id 进厂确认单ID
     * @return 进厂确认单
     */
    public BaServiceVoucher selectBaServiceVoucherById(String id);

    /**
     * 查询进厂确认单列表
     *
     * @param baServiceVoucher 进厂确认单
     * @return 进厂确认单集合
     */
    public List<BaServiceVoucher> selectBaServiceVoucherList(BaServiceVoucher baServiceVoucher);

    /**
     * 新增进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    public int insertBaServiceVoucher(BaServiceVoucher baServiceVoucher);

    public int insertServiceVoucher(List<BaServiceVoucherRecord> baServiceVoucherRecords);

    /**
     * 修改进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    public int updateBaServiceVoucher(BaServiceVoucher baServiceVoucher);

    /**
     * 批量删除进厂确认单
     *
     * @param ids 需要删除的进厂确认单ID
     * @return 结果
     */
    public int deleteBaServiceVoucherByIds(String[] ids);

    /**
     * 删除进厂确认单信息
     *
     * @param id 进厂确认单ID
     * @return 结果
     */
    public int deleteBaServiceVoucherById(String id);


}
