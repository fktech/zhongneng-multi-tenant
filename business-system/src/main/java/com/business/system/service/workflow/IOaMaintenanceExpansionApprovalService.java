package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 业务维护/拓展申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaMaintenanceExpansionApprovalService {

    @Autowired
    protected OaMaintenanceExpansionMapper oaMaintenanceExpansionMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OaClaimRelationMapper oaClaimRelationMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaMaintenanceExpansion oaMaintenanceExpansion = new OaMaintenanceExpansion();
        oaMaintenanceExpansion.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaMaintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(oaMaintenanceExpansion.getId());
        oaMaintenanceExpansion.setState(status);
        String userId = String.valueOf(oaMaintenanceExpansion.getUserId());
        if(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_PASS.getCode().equals(status)){
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            OaMaintenanceExpansion oaMaintenanceExpansion1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), OaMaintenanceExpansion.class);
            //更新项目阶段
            oaMaintenanceExpansion1.setState(status);
            oaMaintenanceExpansionMapper.updateById(oaMaintenanceExpansion1);

            OaClaimRelation oaClaimRelation = new OaClaimRelation();
            oaClaimRelation.setId(getRedisIncreID.getId());
            oaClaimRelation.setClaimState(1); //未报销
            oaClaimRelation.setCreateTime(DateUtils.getNowDate());
            oaClaimRelation.setCreateBy(String.valueOf(oaMaintenanceExpansion1.getUserId()));
            oaClaimRelation.setUserId(oaMaintenanceExpansion1.getUserId());
            oaClaimRelation.setDeptId(oaMaintenanceExpansion1.getDeptId());
            oaClaimRelation.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode()); //业务拓展维护
            oaClaimRelation.setApplyTime(oaMaintenanceExpansion1.getCreateTime()); //申请时间
            oaClaimRelation.setBusinessId(oaMaintenanceExpansion1.getId());
            if(oaMaintenanceExpansion1.getTravelMode().equals("1")){
                if(null != oaMaintenanceExpansion1.getRoadToll()){
                    oaClaimRelation.setApplyAmount(oaMaintenanceExpansion1.getRoadToll().add(oaMaintenanceExpansion1.getFuelCost()));
                }else {
                    oaClaimRelation.setApplyAmount(oaMaintenanceExpansion1.getFuelCost());
                }
                oaClaimRelation.setInvoice(oaMaintenanceExpansion1.getOilInvoice()+","+oaMaintenanceExpansion1.getTollInvoice());
            }else {
                oaClaimRelation.setApplyAmount(oaMaintenanceExpansion1.getCostAmount());
                oaClaimRelation.setInvoice(oaMaintenanceExpansion1.getReachPicture());
            }
            oaClaimRelation.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            //申请人名称
            SysUser sysUser = sysUserMapper.selectUserById(oaMaintenanceExpansion1.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                oaClaimRelation.setProcessName(sysUser.getNickName() + "的" + AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getDescription());
            }
            oaClaimRelation.setRelationId(oaClaimRelation.getId());
            oaClaimRelationMapper.insertOaClaimRelation(oaClaimRelation);

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaMaintenanceExpansion, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaMaintenanceExpansion, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaMaintenanceExpansion, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
                oaMaintenanceExpansionMapper.updateById(oaMaintenanceExpansion);
            }
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaMaintenanceExpansion oaMaintenanceExpansion, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaMaintenanceExpansion.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"业务拓展维护审批提醒",msgContent,"oa",oaMaintenanceExpansion.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaMaintenanceExpansion checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaMaintenanceExpansion oaMaintenanceExpansion = oaMaintenanceExpansionMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaMaintenanceExpansion == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAMAINTENANCEEXPANSION_001);
        }
        if (!AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_VERIFYING.getCode().equals(oaMaintenanceExpansion.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAMAINTENANCEEXPANSION_002);
        }
        return oaMaintenanceExpansion;
    }
}
