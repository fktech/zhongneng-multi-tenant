package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaClaim;
import com.business.system.domain.BaContract;
import com.business.system.domain.BaInvoice;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import com.business.system.domain.vo.BaInvoiceStatVO;
import com.business.system.domain.vo.BaInvoiceVO;

/**
 * 发票信息列Service接口
 *
 * @author single
 * @date 2022-12-28
 */
public interface IBaInvoiceService
{
    /**
     * 查询发票信息列
     *
     * @param id 发票信息列ID
     * @return 发票信息列
     */
    public BaInvoice selectBaInvoiceById(String id);

    /**
     * 查询发票总数
     *
     * @param
     * @return 发票信息列
     */
    public List<BaInvoice> selectBaInvoiceNums(String s);
    /**
     * 查询本月度发票数和金额
     *
     * @param
     * @return 发票信息列
     */
    public List<BaInvoice> selectNumsAmountByMonth(BaInvoice baInvoice);

    /**
     * 查询发票信息列
     *
     * @param id 发票信息列ID
     * @return 发票信息列
     */
    public BaInvoice selectBaInvoiceInfoById(String id);

    /**
     * 查询发票信息列列表
     *
     * @param baInvoice 发票信息列
     * @return 发票信息列集合
     */
    public List<BaInvoice> selectBaInvoiceList(BaInvoice baInvoice);

    /**
     * 新增发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    public int insertBaInvoice(BaInvoice baInvoice);

    /**
     * 修改发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    public int updateBaInvoice(BaInvoice baInvoice);

    /**
     * 批量删除发票信息列
     *
     * @param ids 需要删除的发票信息列ID
     * @return 结果
     */
    public int deleteBaInvoiceByIds(String[] ids);

    /**
     * 删除发票信息列信息
     *
     * @param id 发票信息列ID
     * @return 结果
     */
    public int deleteBaInvoiceById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 立项发票统计
     */
    public UR baInvoiceTotal(BaInvoice baInvoice);

    /**
     * 纳税金额统计
     */
    public BaInvoiceVO sumTaxAmount();

    /**
     * 发票统计
     */
    public BaInvoiceStatVO sumInvoiceAmount(BaInvoice baInvoice);

    /**
     * 查询销项发票开票总数及金额
     */
    public NumsAndMoneyDTO selectBaInvoiceForUp(BaInvoice baInvoice);
    /**
     * 查询月度销项发票开票总数
     */
    public NumsAndMoneyDTO selectBaInvoiceForMonth(BaInvoice baInvoice);
    /**
     * * 查询年度销项发票开票总数
     */
    public NumsAndMoneyDTO selectBaInvoiceForYear(BaInvoice baInvoice);

    public List<BaInvoice> selectBaInvoiceAuthorizedList(BaInvoice baInvoice);

    /**
     * 授权发票管理
     */
    public int authorizedBaInvoice(BaInvoice baInvoice);
}
