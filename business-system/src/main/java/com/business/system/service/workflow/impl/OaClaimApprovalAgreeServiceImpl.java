package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaClaim;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaClaimApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 报销申请审批结果:审批通过
 */
@Service("oaClaimApprovalContent:processApproveResult:pass")
@Slf4j
public class OaClaimApprovalAgreeServiceImpl extends IOaClaimApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaClaim oaClaim = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OACLAIM_STATUS_PASS.getCode());
        return result;
    }
}
