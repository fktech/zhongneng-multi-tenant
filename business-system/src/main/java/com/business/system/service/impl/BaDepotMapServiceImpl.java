package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaDepotMapMapper;
import com.business.system.domain.BaDepotMap;
import com.business.system.service.IBaDepotMapService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 仓库库位信息Service业务层处理
 *
 * @author single
 * @date 2023-09-06
 */
@Service
public class BaDepotMapServiceImpl extends ServiceImpl<BaDepotMapMapper, BaDepotMap> implements IBaDepotMapService
{
    @Autowired
    private BaDepotMapMapper baDepotMapMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询仓库库位信息
     *
     * @param id 仓库库位信息ID
     * @return 仓库库位信息
     */
    @Override
    public BaDepotMap selectBaDepotMapById(String id)
    {
        return baDepotMapMapper.selectBaDepotMapById(id);
    }

    /**
     * 查询仓库库位信息列表
     *
     * @param baDepotMap 仓库库位信息
     * @return 仓库库位信息
     */
    @Override
    public List<BaDepotMap> selectBaDepotMapList(BaDepotMap baDepotMap)
    {
        return baDepotMapMapper.selectBaDepotMapList(baDepotMap);
    }

    /**
     * 新增仓库库位信息
     *
     * @param baDepotMaps 仓库库位信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBaDepotMap(List<BaDepotMap> baDepotMaps)
    {
        int result = 0;
        if(!CollectionUtils.isEmpty(baDepotMaps)){
            baDepotMapMapper.deleteBaDepotMapByRelevanceId(baDepotMaps.get(0).getRelevanceId());
            for(BaDepotMap baDepotMap : baDepotMaps){
                baDepotMap.setId(getRedisIncreID.getId());
                baDepotMap.setUserId(SecurityUtils.getUserId());
                baDepotMap.setDeptId(SecurityUtils.getDeptId());
                baDepotMap.setCreateBy(SecurityUtils.getUsername());
                baDepotMap.setCreateTime(DateUtils.getNowDate());
                baDepotMap.setUserId(SecurityUtils.getUserId());
                baDepotMap.setDeptId(SecurityUtils.getDeptId());
                result = result + baDepotMapMapper.insertBaDepotMap(baDepotMap);
            }
        }

        return result;
    }

    /**
     * 修改仓库库位信息
     *
     * @param baDepotMap 仓库库位信息
     * @return 结果
     */
    @Override
    public int updateBaDepotMap(BaDepotMap baDepotMap)
    {
        baDepotMap.setUpdateTime(DateUtils.getNowDate());
        return baDepotMapMapper.updateBaDepotMap(baDepotMap);
    }

    /**
     * 批量删除仓库库位信息
     *
     * @param ids 需要删除的仓库库位信息ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotMapByIds(String[] ids)
    {
        return baDepotMapMapper.deleteBaDepotMapByIds(ids);
    }

    /**
     * 删除仓库库位信息信息
     *
     * @param id 仓库库位信息ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotMapById(String id)
    {
        return baDepotMapMapper.deleteBaDepotMapById(id);
    }

    @Override
    @Transactional
    public int batchUpdateBaDepotMap(List<BaDepotMap> baDepotMaps) {
        if(!CollectionUtils.isEmpty(baDepotMaps)){
            BaDepotMap baDepotMap = baDepotMaps.get(0);
            if(!ObjectUtils.isEmpty(baDepotMap)){
                baDepotMapMapper.deleteBaDepotMapByRelevanceId(baDepotMap.getRelevanceId());

                baDepotMaps.stream().forEach(item -> {
                    baDepotMapMapper.insertBaDepotMap(item);
                });
            }
        }
        return 0;
    }

}
