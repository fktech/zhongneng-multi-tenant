package com.business.system.service;

import java.util.List;

import com.business.system.domain.BaTransport;
import com.business.system.domain.BaTransportCmst;

/**
 * 中储智运Service接口
 *
 * @author single
 * @date 2023-08-21
 */
public interface IBaTransportCmstService
{
    /**
     * 查询中储智运
     *
     * @param id 中储智运ID
     * @return 中储智运
     */
    public BaTransportCmst selectBaTransportCmstById(String id);

    /**
     * 查询中储智运列表
     *
     * @param baTransportCmst 中储智运
     * @return 中储智运集合
     */
    public List<BaTransportCmst> selectBaTransportCmstList(BaTransportCmst baTransportCmst);

    /**
     * 查询中储智运列表
     *
     * @param baTransportCmst 中储智运
     * @return 中储智运集合
     */
    public List<BaTransportCmst> baTransportCmstList(BaTransportCmst baTransportCmst);

    /**
     * 新增中储智运
     *
     * @param baTransportCmst 中储智运
     * @return 结果
     */
    public int insertBaTransportCmst(BaTransportCmst baTransportCmst);

    /**
     * 修改中储智运
     *
     * @param baTransportCmst 中储智运
     * @return 结果
     */
    public int updateBaTransportCmst(BaTransportCmst baTransportCmst);

    /**
     * 批量删除中储智运
     *
     * @param ids 需要删除的中储智运ID
     * @return 结果
     */
    public int deleteBaTransportCmstByIds(String[] ids);

    /**
     * 删除中储智运信息
     *
     * @param id 中储智运ID
     * @return 结果
     */
    public int deleteBaTransportCmstById(String id);

    /**
     * 关联发运
     */
    public int transport(BaTransport baTransport);


}
