package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.BaContractStartMapper;
import com.business.system.service.IBaClaimService;
import com.business.system.service.IBaCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaClaimHistoryMapper;
import com.business.system.service.IBaClaimHistoryService;

/**
 * 认领历史Service业务层处理
 *
 * @author ljb
 * @date 2023-01-31
 */
@Service
public class BaClaimHistoryServiceImpl extends ServiceImpl<BaClaimHistoryMapper, BaClaimHistory> implements IBaClaimHistoryService
{
    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private IBaClaimService baClaimService;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private IBaCollectionService baCollectionService;

    /**
     * 查询认领历史
     *
     * @param id 认领历史ID
     * @return 认领历史
     */
    @Override
    public BaClaimHistory selectBaClaimHistoryById(String id)
    {
        BaClaimHistory baClaimHistory = baClaimHistoryMapper.selectBaClaimHistoryById(id);
        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baClaimHistory.getOrderId());
        if(StringUtils.isNotNull(baContractStart)){
            baClaimHistory.setStartName(baContractStart.getName());
        }
        return baClaimHistory;
    }

    /**
     * 查询认领历史列表
     *
     * @param baClaimHistory 认领历史
     * @return 认领历史
     */
    @Override
    public List<BaClaimHistory> selectBaClaimHistoryList(BaClaimHistory baClaimHistory)
    {
        List<BaClaimHistory> claimHistories = baClaimHistoryMapper.selectBaClaimHistoryList(baClaimHistory);
        for (BaClaimHistory claimHistory:claimHistories) {
            //查询认领信息
            BaClaim baClaim = baClaimService.selectBaClaimById(claimHistory.getClaimId());
            //收款数据
            BaCollection baCollection = baCollectionService.selectBaCollectionById(baClaim.getRelationId());
            //付款单位
            claimHistory.setPaymentCompanyName(baCollection.getPaymentCompanyName());
            //收款类型
            claimHistory.setCollectionCategory(baCollection.getCollectionCategory());
            //收款日期
            claimHistory.setApplyCollectionDate(baCollection.getCollectionDate());
            //收款金额
            claimHistory.setApplyCollectionAmount(baCollection.getApplyCollectionAmount());
            //已认领金额
            claimHistory.setClaimed(baCollection.getClaimed());
            //待认领金额
            claimHistory.setStayClaim(baCollection.getApplyCollectionAmount().subtract(baCollection.getClaimed()));
            //认领状态
            claimHistory.setClaimState(baClaim.getClaimState());
            //认领人
            claimHistory.setUserName(baClaim.getUserName());
            //认领时间
            claimHistory.setClaimTime(baClaim.getCreateTime());
        }
        return claimHistories;
    }

    @Override
    public List<BaClaim> selectBaClaimList(BaClaimHistory baClaimHistory) {
        List<BaClaim> baClaimList = new ArrayList<>();
        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectBaClaimHistoryList(baClaimHistory);
        for (BaClaimHistory baClaimHistory1:baClaimHistories) {
            //查询父级认领信息
            BaClaim baClaim = new BaClaim();
            baClaim.setId(baClaimHistory1.getClaimId());
            //租户ID
            baClaim.setTenantId(SecurityUtils.getCurrComId());
            List<BaClaim> baClaimList1 = baClaimService.selectBaClaimList(baClaim);
            if(baClaimList1.size() > 0){
                baClaimList1.get(0).setThisClaimHistory(baClaimHistory1.getCurrentCollection());
                //全局编号
                baClaimList1.get(0).setGlobalNumber(baClaimHistory1.getGlobalNumber());
                //认领时间（创建时间）
                baClaimList1.get(0).setClaimTime(baClaimList1.get(0).getCreateTime());
                baClaimList.add(baClaimList1.get(0));
            }
        }
        return baClaimList;
    }


    /**
     * 新增认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    @Override
    public int insertBaClaimHistory(BaClaimHistory baClaimHistory)
    {
        return baClaimHistoryMapper.insertBaClaimHistory(baClaimHistory);
    }

    /**
     * 修改认领历史
     *
     * @param baClaimHistory 认领历史
     * @return 结果
     */
    @Override
    public int updateBaClaimHistory(BaClaimHistory baClaimHistory)
    {
        return baClaimHistoryMapper.updateBaClaimHistory(baClaimHistory);
    }

    /**
     * 批量删除认领历史
     *
     * @param ids 需要删除的认领历史ID
     * @return 结果
     */
    @Override
    public int deleteBaClaimHistoryByIds(String[] ids)
    {
        return baClaimHistoryMapper.deleteBaClaimHistoryByIds(ids);
    }

    /**
     * 删除认领历史信息
     *
     * @param id 认领历史ID
     * @return 结果
     */
    @Override
    public int deleteBaClaimHistoryById(String id)
    {
        return baClaimHistoryMapper.deleteBaClaimHistoryById(id);
    }

    @Override
    public int countBaClaimHistoryList(BaClaimHistory baClaimHistory) {
        //租户ID
        baClaimHistory.setTenantId(SecurityUtils.getCurrComId());
        return baClaimHistoryMapper.countBaClaimHistoryList(baClaimHistory);
    }

}
