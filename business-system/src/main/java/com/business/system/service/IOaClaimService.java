package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaClaim;

/**
 * 报销申请Service接口
 *
 * @author single
 * @date 2023-12-11
 */
public interface IOaClaimService
{
    /**
     * 查询报销申请
     *
     * @param id 报销申请ID
     * @return 报销申请
     */
    public OaClaim selectOaClaimById(String id);

    /**
     * 查询报销申请列表
     *
     * @param oaClaim 报销申请
     * @return 报销申请集合
     */
    public List<OaClaim> selectOaClaimList(OaClaim oaClaim);

    /**
     * 新增报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    public int insertOaClaim(OaClaim oaClaim);

    /**
     * 修改报销申请
     *
     * @param oaClaim 报销申请
     * @return 结果
     */
    public int updateOaClaim(OaClaim oaClaim);

    /**
     * 批量删除报销申请
     *
     * @param ids 需要删除的报销申请ID
     * @return 结果
     */
    public int deleteOaClaimByIds(String[] ids);

    /**
     * 删除报销申请信息
     *
     * @param id 报销申请ID
     * @return 结果
     */
    public int deleteOaClaimById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

}
