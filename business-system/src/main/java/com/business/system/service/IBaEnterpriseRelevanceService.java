package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaEnterpriseRelevance;

/**
 * 终端企业关联Service接口
 *
 * @author ljb
 * @date 2023-03-01
 */
public interface IBaEnterpriseRelevanceService
{
    /**
     * 查询终端企业关联
     *
     * @param id 终端企业关联ID
     * @return 终端企业关联
     */
    public BaEnterpriseRelevance selectBaEnterpriseRelevanceById(String id);

    /**
     * 查询终端企业关联列表
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 终端企业关联集合
     */
    public List<BaEnterpriseRelevance> selectBaEnterpriseRelevanceList(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 新增终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    public int insertBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 修改终端企业关联
     *
     * @param baEnterpriseRelevance 终端企业关联
     * @return 结果
     */
    public int updateBaEnterpriseRelevance(BaEnterpriseRelevance baEnterpriseRelevance);

    /**
     * 批量删除终端企业关联
     *
     * @param ids 需要删除的终端企业关联ID
     * @return 结果
     */
    public int deleteBaEnterpriseRelevanceByIds(String[] ids);

    /**
     * 删除终端企业关联信息
     *
     * @param id 终端企业关联ID
     * @return 结果
     */
    public int deleteBaEnterpriseRelevanceById(String id);


}
