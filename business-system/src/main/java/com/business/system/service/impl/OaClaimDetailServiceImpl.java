package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.system.domain.OaClaimDetail;
import com.business.system.domain.vo.OaClaimStatVO;
import com.business.system.mapper.OaClaimDetailMapper;
import com.business.system.service.IOaClaimDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报销明细Service业务层处理
 *
 * @author single
 * @date 2023-12-11
 */
@Service
public class OaClaimDetailServiceImpl extends ServiceImpl<OaClaimDetailMapper, OaClaimDetail> implements IOaClaimDetailService
{
    @Autowired
    private OaClaimDetailMapper oaClaimDetailMapper;

    /**
     * 查询报销明细
     *
     * @param id 报销明细ID
     * @return 报销明细
     */
    @Override
    public OaClaimDetail selectOaClaimDetailById(String id)
    {
        return oaClaimDetailMapper.selectOaClaimDetailById(id);
    }

    /**
     * 查询报销明细列表
     *
     * @param oaClaimDetail 报销明细
     * @return 报销明细
     */
    @Override
    public List<OaClaimDetail> selectOaClaimDetailList(OaClaimDetail oaClaimDetail)
    {
        return oaClaimDetailMapper.selectOaClaimDetailList(oaClaimDetail);
    }

    /**
     * 新增报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    @Override
    public int insertOaClaimDetail(OaClaimDetail oaClaimDetail)
    {
        oaClaimDetail.setCreateTime(DateUtils.getNowDate());
        return oaClaimDetailMapper.insertOaClaimDetail(oaClaimDetail);
    }

    /**
     * 修改报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    @Override
    public int updateOaClaimDetail(OaClaimDetail oaClaimDetail)
    {
        oaClaimDetail.setUpdateTime(DateUtils.getNowDate());
        return oaClaimDetailMapper.updateOaClaimDetail(oaClaimDetail);
    }

    /**
     * 批量删除报销明细
     *
     * @param ids 需要删除的报销明细ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimDetailByIds(String[] ids)
    {
        return oaClaimDetailMapper.deleteOaClaimDetailByIds(ids);
    }

    /**
     * 删除报销明细信息
     *
     * @param id 报销明细ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimDetailById(String id)
    {
        return oaClaimDetailMapper.deleteOaClaimDetailById(id);
    }

}
