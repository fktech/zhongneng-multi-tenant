package com.business.system.service.workflow;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationErrorCode;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 汽运管理审批接口 服务类
 * @date: 2023/2/17 11:09
 */
@Slf4j
@Service
public class IAutomobileApprovalService {

    @Autowired
    protected BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    protected BaProjectMapper baProjectMapper;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    protected BaAutomobuleStandardMapper baAutomobuleStandardMapper;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private BaSettlementFreightMapper baSettlementFreightMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/business/supplier/addSource";
        //本地环境
        //String url = "https://4942h442k4.yicp.fun/business/supplier/addSource";

//        String url = " https://zncy.xkgo.net/prod-api/business/supplier/addSource";
        //汽车运输/货源对象
        BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
        baTransportAutomobile.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        //查询货源信息
        BaTransportAutomobile transportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(approvalWorkflowDTO.getTargetId());
        //收发货人信息
        BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(transportAutomobile.getDepName());
        //发货人
        baTransportAutomobile.setDepName(baCounterpart.getName());
        //收货人
        baTransportAutomobile.setDesName(baCounterpartMapper.selectBaCounterpartById(transportAutomobile.getDesName()).getName());
        baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());
        //发货人
        baTransportAutomobile.setDepName(baCounterpartMapper.selectBaCounterpartById(transportAutomobile.getDepName()).getName());
        //收货人
        baTransportAutomobile.setDesName(baCounterpartMapper.selectBaCounterpartById(transportAutomobile.getDesName()).getName());
        baTransportAutomobile.setState(status);
        String userId = String.valueOf(baTransportAutomobile.getUserId());

        if(AdminCodeEnum.AUTOMOBILE_STATUS_PASS.getCode().equals(status)){
            //查询货源运价
            QueryWrapper<BaAutomobuleStandard> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id", baTransportAutomobile.getId());
            List<BaAutomobuleStandard> baAutomobuleStandards = baAutomobuleStandardMapper.selectList(queryWrapper);
            //设置货源中运价对象集合
            baTransportAutomobile.setRatesItemList(baAutomobuleStandards);
            baTransportAutomobile.setIsShowPrice(baAutomobuleStandards.get(0).getIsShowPrice());
            baTransportAutomobile.setJsyjtype(baAutomobuleStandards.get(0).getJsyjtype());
            baTransportAutomobile.setKkbz(baAutomobuleStandards.get(0).getKkbz());
            baTransportAutomobile.setLuhaobz(baAutomobuleStandards.get(0).getLuhaobz());
            baTransportAutomobile.setLuhaotype(baAutomobuleStandards.get(0).getLuhaotype());
            baTransportAutomobile.setPrice(baAutomobuleStandards.get(0).getPrice());
            baTransportAutomobile.setRelationId(baAutomobuleStandards.get(0).getRelationId());
            baTransportAutomobile.setYfprecision(baAutomobuleStandards.get(0).getYfprecision());
            baTransportAutomobile.setZdbz(baAutomobuleStandards.get(0).getZdbz());
            baTransportAutomobile.setZdkkbz(baAutomobuleStandards.get(0).getZdkkbz());
            baTransportAutomobile.setZdtype(baAutomobuleStandards.get(0).getZdtype());

            baTransportAutomobile.setDeliveryTime(baTransportAutomobile.getYfStartDate());
            baTransportAutomobile.setValidityTime(baTransportAutomobile.getYfEndDate());
            //货主公司名称
            baTransportAutomobile.setCompanyName(baTransportAutomobile.getCompany());
            List<String> time = new ArrayList<>();
            time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfStartDate()));
            time.add(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baTransportAutomobile.getYfEndDate()));
            baTransportAutomobile.setTime(time);
            baTransportAutomobile.setSupplierId(new Long(10101010));
            baTransportAutomobile.setSupplierName("张非");

            //调用推送信息接口
            JSONObject jsonObj = (JSONObject) JSON.toJSON(baTransportAutomobile);
            log.info(JSON.toJSONString(jsonObj));
            System.out.println("----------"+JSONObject.toJSONString(baTransportAutomobile));
            try {
                String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(baTransportAutomobile));
                System.out.println("+++++++++++++++++"+postData);
                Object parse = JSONObject.parse(postData);
                JSONObject obj = (JSONObject) JSON.toJSON(parse);
                Object data = obj.getJSONObject("data");
                JSONObject obj1 = (JSONObject) JSON.toJSON(data);
                baTransportAutomobile.setQrcode(obj1.getString("qrcode"));

            } catch (Exception e) {
                log.error("调用货源新增接口失败，原因：{}", e.getMessage(), e);
                throw new VerificationException(VerificationErrorCode.VERIFICATION_PROCESS_ERROR);
            }
            //修改业务状态
            baTransportAutomobile.setConfirmStatus("2");
            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
            //插入货源ID
            /*BaSettlementFreight baSettlementFreight = new BaSettlementFreight();
            baSettlementFreight.setAutomobileId(baTransportAutomobile.getId());
            baSettlementFreightMapper.insertBaSettlementFreight(baSettlementFreight);*/

            //释放锁
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baTransportAutomobile, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.AUTOMOBILE_STATUS_REJECT.getCode().equals(status)){

            //释放锁
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baTransportAutomobile, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baTransportAutomobile, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baTransportAutomobileMapper.updateById(baTransportAutomobile);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaTransportAutomobile baTransportAutomobile, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baTransportAutomobile.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode());

        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaTransportAutomobile checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baTransportAutomobile == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_AUTOMOBILE_001);
        }
        if (!AdminCodeEnum.AUTOMOBILE_STATUS_VERIFYING.getCode().equals(baTransportAutomobile.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_AUTOMOBILE_002);
        }
        return baTransportAutomobile;
    }
}
