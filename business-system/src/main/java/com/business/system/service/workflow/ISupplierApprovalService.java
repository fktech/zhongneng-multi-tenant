package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.BaBank;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 供应商管理审批接口 服务类
 * @date: 2023/2/28 15:12
 */
@Service
public class ISupplierApprovalService {

    @Autowired
    protected BaSupplierMapper baSupplierMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaSupplier baSupplier = new BaSupplier();
        baSupplier.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baSupplier = baSupplierMapper.selectBaSupplierById(baSupplier.getId());
        baSupplier.setState(status);
        String userId = String.valueOf(baSupplier.getUserId());
        if(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode().equals(status)){

            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            BaSupplier baSupplier1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaSupplier.class);
            if(!ObjectUtils.isEmpty(baSupplier1)){
                baSupplier1.setState(status);

                List<BaBank> baBankList = new ArrayList<>();
                //基本账户
                if (StringUtils.isNotNull(baSupplier1.getBaBank())) {
                    BaBank baBank = baSupplier1.getBaBank();
                    baBank.setUpdateTime(DateUtils.getNowDate());
                    baBank.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(baBank);
                }
                //收款账户
                if (!CollectionUtils.isEmpty(baBankList)) {
                    baBankList.stream().forEach(item -> {
                        if (StringUtils.isNotEmpty(item.getId())) {
                            item.setUpdateTime(DateUtils.getNowDate());
                            item.setUpdateBy(SecurityUtils.getUsername());
                            baBankMapper.updateBaBank(item);
                        } else {
                            item.setId(getRedisIncreID.getId());
                            item.setCreateTime(DateUtils.getNowDate());
                            item.setCreateBy(SecurityUtils.getUsername());
                            baBankMapper.insertBaBank(item);
                        }
                    });
                }
                //修改开票信息
                if (StringUtils.isNotNull(baSupplier1.getBillingInformation())) {
                    baBillingInformationMapper.updateBaBillingInformation(baSupplier1.getBillingInformation());
                }
                baSupplierMapper.updateBaSupplier(baSupplier1);
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baSupplier1, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.SUPPLIER_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baSupplier, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baSupplier, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baSupplierMapper.updateById(baSupplier);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSupplier baSupplier, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSupplier.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"供应商审批提醒",msgContent,baMessage.getType(),baSupplier.getId(),baSupplier.getState());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaSupplier checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSupplier baSupplier = baSupplierMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baSupplier == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SUPPLIER_001);
        }
//        if (!AdminCodeEnum.SUPPLIER_STATUS_VERIFYING.getCode().equals(baSupplier.getState())) {
//            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SUPPLIER_002);
//        }
        return baSupplier;
    }
}
