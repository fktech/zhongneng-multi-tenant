package com.business.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaEarlyWarningService;

/**
 * 预警Service业务层处理
 *
 * @author single
 * @date 2024-05-29
 */
@Service
public class BaEarlyWarningServiceImpl extends ServiceImpl<BaEarlyWarningMapper, BaEarlyWarning> implements IBaEarlyWarningService
{
    @Autowired
    private BaEarlyWarningMapper baEarlyWarningMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    /**
     * 查询预警
     *
     * @param id 预警ID
     * @return 预警
     */
    @Override
    public BaEarlyWarning selectBaEarlyWarningById(String id)
    {
        return baEarlyWarningMapper.selectBaEarlyWarningById(id);
    }

    /**
     * 查询预警列表
     *
     * @param baEarlyWarning 预警
     * @return 预警
     */
    @Override
    public List<BaEarlyWarning> selectBaEarlyWarningList(BaEarlyWarning baEarlyWarning)
    {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        List<BaEarlyWarning> baEarlyWarnings = baEarlyWarningMapper.selectBaEarlyWarning(baEarlyWarning);
        for (BaEarlyWarning earlyWarning:baEarlyWarnings) {
            //下游公司简称
            if(StringUtils.isNotEmpty(earlyWarning.getDownstreamCompany())){
                String[] split = earlyWarning.getDownstreamCompany().split(";");
                String type = split[0];
                if(type.equals("1")){
                   //供应商
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(split[1]);
                    earlyWarning.setCompanyShort(baSupplier.getCompanyAbbreviation());
                }else if(type.equals("2")){
                   //终端企业
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(split[1]);
                    earlyWarning.setCompanyShort(enterprise.getCompanyAbbreviation());
                }else if(type.equals("6")){
                  //集团公司
                    BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(split[1]);
                    earlyWarning.setCompanyShort(companyGroup.getCompanyAbbreviation());
                }else if(type.equals("3") || type.equals("4") || type.equals("5") || type.equals("7") || type.equals("8")){
                    //公司
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(split[1]);
                    earlyWarning.setCompanyShort(baCompany.getCompanyAbbreviation());
                }
            }
            //上游公司
            if(StringUtils.isNotEmpty(earlyWarning.getUpstreamCompany())){
                //查询供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(earlyWarning.getUpstreamCompany());
                if (StringUtils.isNotNull(baSupplier)) {
                    earlyWarning.setCompanyAbbreviation(baSupplier.getCompanyAbbreviation());
                }
                //查询公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(earlyWarning.getUpstreamCompany());
                if (StringUtils.isNotNull(baCompany)) {
                    earlyWarning.setCompanyAbbreviation(baCompany.getCompanyAbbreviation());
                }
            }
            //合同信息
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(earlyWarning.getRelevanceId());
            //查询相关合同信息
            //合同信息
            if(StringUtils.isNotEmpty(baContractStart.getDownstream())){
                BaContract contract = baContractMapper.selectBaContractById(baContractStart.getDownstream());
                if(StringUtils.isNotNull(contract)) {
                    //下游合同履约期
                    Date startTime = contract.getStartTime();
                    Date endTime = contract.getEndTime();
                    earlyWarning.setStartTime(df.format(startTime));
                    earlyWarning.setEndTime(df.format(endTime));
                    //查找运输信息
                    QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                    transportQueryWrapper.eq("start_id", baContractStart.getId());
                    transportQueryWrapper.eq("flag", 0);
                    transportQueryWrapper.orderByDesc("create_time");
                    List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                    if(baTransports.size() > 0) {
                        List<BaCheck> checks = new ArrayList<>();
                        for (BaTransport transport:baTransports) {
                            //发货信息
                            QueryWrapper<BaCheck> queryWrapper0 = new QueryWrapper<>();
                            queryWrapper0.eq("relation_id", transport.getId());
                            queryWrapper0.eq("type", "1");
                            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper0);
                            if(StringUtils.isNotNull(baCheck)){
                                checks.add(baCheck);
                            }
                        }
                        if(StringUtils.isNotNull(checks)){
                            List<BaCheck> collect = checks.stream().sorted(Comparator.comparing(BaCheck::getId).reversed()).collect(Collectors.toList());
                            BaCheck baCheck = collect.get(0);
                            //发货开始时间
                            earlyWarning.setCheckTime(baCheck.getCheckTime());
                            //发货结束时间
                            earlyWarning.setCheckEndTime(baCheck.getCheckEndTime());
                        }
                    }
            }
             }
        }
        return baEarlyWarnings;
    }

    @Override
    public List<BaEarlyWarning> selectEarlyWarning(BaEarlyWarning baEarlyWarning) {

        return baEarlyWarningMapper.selectEarlyWarning(baEarlyWarning);
    }

    /**
     * 新增预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    @Override
    public int insertBaEarlyWarning(BaEarlyWarning baEarlyWarning)
    {
        return baEarlyWarningMapper.insertBaEarlyWarning(baEarlyWarning);
    }

    /**
     * 修改预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    @Override
    public int updateBaEarlyWarning(BaEarlyWarning baEarlyWarning)
    {
        BaEarlyWarning earlyWarning = baEarlyWarningMapper.selectBaEarlyWarningById(baEarlyWarning.getId());
        //应收账款预警状态
        Date date = new Date();
        if(null != baEarlyWarning.getEarlyWarning() && null != baEarlyWarning.getEstimatedPaymentDate()){
            if(!earlyWarning.getRepaymentCompleted().equals("1")){
                //【进行中】：当【预估回款时间-当前时间】大于预警天数时为进行中
                long l = baEarlyWarning.getEstimatedPaymentDate().getTime() - date.getTime();
                long diffInDays = TimeUnit.MILLISECONDS.toDays(l)+1;
                if(diffInDays > baEarlyWarning.getEarlyWarning()){
                    baEarlyWarning.setReceivableState("1");
                }else {
                    baEarlyWarning.setReceivableState("0");
                }
            }
        }
        return baEarlyWarningMapper.updateBaEarlyWarning(baEarlyWarning);
    }

    /**
     * 批量删除预警
     *
     * @param ids 需要删除的预警ID
     * @return 结果
     */
    @Override
    public int deleteBaEarlyWarningByIds(String[] ids)
    {
        return baEarlyWarningMapper.deleteBaEarlyWarningByIds(ids);
    }

    /**
     * 删除预警信息
     *
     * @param id 预警ID
     * @return 结果
     */
    @Override
    public int deleteBaEarlyWarningById(String id)
    {
        return baEarlyWarningMapper.deleteBaEarlyWarningById(id);
    }

    @Override
    public int cancelBaEarlyWarning(BaEarlyWarning baEarlyWarning) {

        return baEarlyWarningMapper.updateBaEarlyWarning(baEarlyWarning);
    }

}
