package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaRailwayBureau;

/**
 * 铁路局Service接口
 *
 * @author ljb
 * @date 2023-01-16
 */
public interface IBaRailwayBureauService
{
    /**
     * 查询铁路局
     *
     * @param id 铁路局ID
     * @return 铁路局
     */
    public BaRailwayBureau selectBaRailwayBureauById(String id);

    /**
     * 查询铁路局列表
     *
     * @param baRailwayBureau 铁路局
     * @return 铁路局集合
     */
    public List<BaRailwayBureau> selectBaRailwayBureauList(BaRailwayBureau baRailwayBureau);

    /**
     * 新增铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    public int insertBaRailwayBureau(BaRailwayBureau baRailwayBureau);

    /**
     * 修改铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    public int updateBaRailwayBureau(BaRailwayBureau baRailwayBureau);

    /**
     * 批量删除铁路局
     *
     * @param ids 需要删除的铁路局ID
     * @return 结果
     */
    public int deleteBaRailwayBureauByIds(String[] ids);

    /**
     * 删除铁路局信息
     *
     * @param id 铁路局ID
     * @return 结果
     */
    public int deleteBaRailwayBureauById(String id);


}
