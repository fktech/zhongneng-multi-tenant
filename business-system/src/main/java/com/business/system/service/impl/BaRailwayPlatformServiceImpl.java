package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaRailwayBureau;
import com.business.system.domain.BaRailwayFreightTable;
import com.business.system.domain.Districts;
import com.business.system.mapper.BaRailwayBureauMapper;
import com.business.system.mapper.BaRailwayFreightTableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaRailwayPlatformMapper;
import com.business.system.domain.BaRailwayPlatform;
import com.business.system.service.IBaRailwayPlatformService;

/**
 * 铁路站台Service业务层处理
 *
 * @author ljb
 * @date 2022-11-29
 */
@Service
public class BaRailwayPlatformServiceImpl extends ServiceImpl<BaRailwayPlatformMapper, BaRailwayPlatform> implements IBaRailwayPlatformService
{
    @Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaRailwayFreightTableMapper baRailwayFreightTableMapper;

    @Autowired
    private BaRailwayBureauMapper baRailwayBureauMapper;

    /**
     * 查询铁路站台
     *
     * @param id 铁路站台ID
     * @return 铁路站台
     */
    @Override
    public BaRailwayPlatform selectBaRailwayPlatformById(String id)
    {
        BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(id);
        //铁路局名称
        if(StringUtils.isNotEmpty(baRailwayPlatform.getBureauId())){
            baRailwayPlatform.setBureauName(baRailwayBureauMapper.selectBaRailwayBureauById(baRailwayPlatform.getBureauId()).getName());
        }
        return baRailwayPlatform;
    }

    /**
     * 查询铁路站台列表
     *
     * @param baRailwayPlatform 铁路站台
     * @return 铁路站台
     */
    @Override
    public List<BaRailwayPlatform> selectBaRailwayPlatformList(BaRailwayPlatform baRailwayPlatform)
    {
        return baRailwayPlatformMapper.selectBaRailwayPlatformList(baRailwayPlatform);
    }

    @Override
    public List<BaRailwayPlatform> freightTable() {
        QueryWrapper<BaRailwayPlatform> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("flag",0);
        List<BaRailwayPlatform> baRailwayPlatformList =baRailwayPlatformMapper.selectList(queryWrapper);
        //查询对应的运费表数据
        for (BaRailwayPlatform baRailwayPlatform:baRailwayPlatformList) {
            QueryWrapper<BaRailwayFreightTable> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("platform_id",baRailwayPlatform.getId());
            List<BaRailwayFreightTable> baRailwayFreightTables = baRailwayFreightTableMapper.selectList(queryWrapper1);
            baRailwayPlatform.setFreightTables(baRailwayFreightTables);
        }

        return baRailwayPlatformList;
    }

    /**
     * 新增铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    @Override
    public int insertBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform)
    {
        //查询站台信息
        QueryWrapper<BaRailwayPlatform> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("flag",0);
        queryWrapper.eq("name",baRailwayPlatform.getName());
        List<BaRailwayPlatform> baRailwayPlatforms = baRailwayPlatformMapper.selectList(queryWrapper);
        if(baRailwayPlatforms.size() > 0){
            return 0;
        }
        baRailwayPlatform.setId(getRedisIncreID.getId());
        baRailwayPlatform.setCreateTime(DateUtils.getNowDate());
        baRailwayPlatform.setCreateBy(SecurityUtils.getUsername());
        int a = baRailwayPlatformMapper.insertBaRailwayPlatform(baRailwayPlatform);
        //添加运费信息
        /*if(a > 0){
            QueryWrapper<BaRailwayPlatform> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("flag",0);
            List<BaRailwayPlatform> baRailwayPlatformAll = baRailwayPlatformMapper.selectList(queryWrapper1);
            if(baRailwayPlatformAll.size() > 0){
                for (BaRailwayPlatform ba:baRailwayPlatformAll) {
                    BaRailwayFreightTable  baRailwayFreightTable = new BaRailwayFreightTable();
                    baRailwayFreightTable.setId(getRedisIncreID.getId());
                    baRailwayFreightTable.setPlatformId(ba.getId());
                    baRailwayFreightTable.setPlatformLevel(baRailwayPlatform.getId());
                    baRailwayFreightTableMapper.insertBaRailwayFreightTable(baRailwayFreightTable);
                    //反向存储数据始发站台对应
                    if(ba.getId().equals(baRailwayPlatform.getId()) == false){
                        baRailwayFreightTable.setId(getRedisIncreID.getId());
                        baRailwayFreightTable.setPlatformId(baRailwayPlatform.getId());
                        baRailwayFreightTable.setPlatformLevel(ba.getId());
                        baRailwayFreightTableMapper.insertBaRailwayFreightTable(baRailwayFreightTable);
                    }
                }
            }else {
                BaRailwayFreightTable  baRailwayFreightTable = new BaRailwayFreightTable();
                baRailwayFreightTable.setId(getRedisIncreID.getId());
                baRailwayFreightTable.setPlatformId(baRailwayPlatform.getId());
                baRailwayFreightTable.setPlatformLevel(baRailwayPlatform.getId());
                baRailwayFreightTableMapper.insertBaRailwayFreightTable(baRailwayFreightTable);
            }
        }else {
            return -1;
        }*/

        return a;
    }

    /**
     * 修改铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    @Override
    public int updateBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform)
    {
        baRailwayPlatform.setUpdateTime(DateUtils.getNowDate());
        return baRailwayPlatformMapper.updateBaRailwayPlatform(baRailwayPlatform);
    }

    /**
     * 批量删除铁路站台
     *
     * @param ids 需要删除的铁路站台ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayPlatformByIds(String[] ids)
    {
        int a = baRailwayPlatformMapper.deleteBaRailwayPlatformByIds(ids);
        if(a > 0){
            for (String id:ids) {
                QueryWrapper<BaRailwayFreightTable> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("platform_id",id).or().eq("platform_level",id);
                List<BaRailwayFreightTable> baRailwayFreightTables = baRailwayFreightTableMapper.selectList(queryWrapper);
                for (BaRailwayFreightTable baRailwayFreightTable:baRailwayFreightTables) {
                    baRailwayFreightTableMapper.deleteBaRailwayFreightTableById(baRailwayFreightTable.getId());
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除铁路站台信息
     *
     * @param id 铁路站台ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayPlatformById(String id)
    {
        return baRailwayPlatformMapper.deleteBaRailwayPlatformById(id);
    }

}
