package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.*;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用煤企业Service业务层处理
 *
 * @author ljb
 * @date 2022-11-29
 */
@Service
public class BaEnterpriseServiceImpl extends ServiceImpl<BaEnterpriseMapper, BaEnterprise> implements IBaEnterpriseService
{
    private static final Logger log = LoggerFactory.getLogger(BaEnterpriseServiceImpl.class);
    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysDictDataMapper sysDictDataMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaEnterpriseRelevanceMapper enterpriseRelevanceMapper;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private IBaChargingStandardsService iBaChargingStandardsService;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaHiEnterpriseMapper baHiEnterpriseMapper;

    @Autowired
    private IBaEnterpriseService iBaEnterpriseService;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    /**
     * 查询用煤企业
     *
     * @param id 用煤企业ID
     * @return 用煤企业
     */
    @Override
    public BaEnterprise selectBaEnterpriseById(String id)
    {
        BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(id);
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baEnterprise.getUserId());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baEnterprise.setUser(sysUser);
        //所属集团
        if(StringUtils.isNotEmpty(baEnterprise.getCompanyNature())){
            BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
            baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
        }
        //所需煤种
        QueryWrapper<BaEnterpriseRelevance> relevanceQueryWrapper = new QueryWrapper<>();
        relevanceQueryWrapper.eq("relevance_id",baEnterprise.getId());
        List<BaEnterpriseRelevance> baEnterpriseRelevances = new ArrayList<>();
        baEnterpriseRelevances = enterpriseRelevanceMapper.selectList(relevanceQueryWrapper);
        for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterpriseRelevances) {
            if(StringUtils.isNotEmpty(baEnterpriseRelevance.getGoodsId())){
                baEnterpriseRelevance.setGoodsName(baGoodsMapper.selectBaGoodsById(baEnterpriseRelevance.getGoodsId()).getName());
            }
        }
        baEnterprise.setEnterpriseRelevance(baEnterpriseRelevances);
        //开票信息
        QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
        informationQueryWrapper.eq("relevance_id",id);
        BaBillingInformation baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
        if(ObjectUtils.isEmpty(baBillingInformation)){
            informationQueryWrapper = new QueryWrapper<>();
            informationQueryWrapper.eq("relevance_id",baEnterprise.getRelationId());
            baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
        }
        if(StringUtils.isNotNull(baBillingInformation)){
            baEnterprise.setBillingInformation(baBillingInformation);
        }
        //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baEnterprise.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.isNull("state");
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        if(CollectionUtils.isEmpty(baBanks)){
            queryWrapper.eq("relation_id",baEnterprise.getRelationId());
            baBanks = baBankMapper.selectList(queryWrapper);
        }
        baEnterprise.setBaBankList(baBanks);
        //基本账户信息
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baEnterprise.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.eq("state","1");
        BaBank baBank = baBankMapper.selectOne(queryWrapper);
        if(ObjectUtils.isEmpty(baBank)){
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",baEnterprise.getRelationId());
            queryWrapper.eq("flag",0);
            queryWrapper.eq("state","1");
            baBank = baBankMapper.selectOne(queryWrapper);
        }
        baEnterprise.setBaBank(baBank);

//        //收费标准
//        QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
//        baChargingStandardsQueryWrapper.eq("relation_id",baEnterprise.getId());
//        BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectOne(baChargingStandardsQueryWrapper);
//        baEnterprise.setBaChargingStandards(baChargingStandards);

        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id", baEnterprise.getId());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = new ArrayList<>();
        baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
        }
        if(CollectionUtils.isEmpty(processInstanceIds)){
            enterpriseQueryWrapper.eq("relation_id",baEnterprise.getRelationId());
//        relatedQueryWrapper.eq("flag",0);
            //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
            enterpriseQueryWrapper.eq("state","enterprise:pass");
            enterpriseQueryWrapper.orderByDesc("create_time");
            List<BaEnterprise> baEnterprises = baEnterpriseMapper.selectList(enterpriseQueryWrapper);
            processInstanceIds = new ArrayList<>();
            if(!CollectionUtils.isEmpty(baEnterprises)){
                for(BaEnterprise baEnterprise1 : baEnterprises){
                    //流程实例ID
                    relatedQueryWrapper = new QueryWrapper<>();
                    relatedQueryWrapper.eq("business_id", baEnterprise1.getId());
                    relatedQueryWrapper.orderByDesc("create_time");
                    baProcessInstanceRelateds = new ArrayList<>();
                    baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                    if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                        }
                        break;
                    }
                }
            }
        }

        baEnterprise.setProcessInstanceId(processInstanceIds);
        return baEnterprise;
    }

    /**
     * 查询用煤企业列表
     *
     * @param baEnterprise 用煤企业
     * @return 用煤企业
     */
    @Override
    public List<BaEnterprise> selectBaEnterpriseList(BaEnterprise baEnterprise)
    {
        List<BaEnterprise> enterprises = baEnterpriseMapper.selectBaEnterpriseList(baEnterprise);
        for (BaEnterprise enterprise:enterprises) {
            //所属集团
            if(StringUtils.isNotEmpty(enterprise.getCompanyNature())){
                BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(enterprise.getCompanyNature());
                enterprise.setCompanyName(baCompanyGroup.getCompanyName());
            }

            //所需煤种（商品名称）
            String goodsName = "";
            //所需煤种
            QueryWrapper<BaEnterpriseRelevance> relevanceQueryWrapper = new QueryWrapper<>();
            relevanceQueryWrapper.eq("relevance_id",enterprise.getId());
            List<BaEnterpriseRelevance> baEnterpriseRelevances = enterpriseRelevanceMapper.selectList(relevanceQueryWrapper);
            for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterpriseRelevances) {
                if(StringUtils.isNotEmpty(baEnterpriseRelevance.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baEnterpriseRelevance.getGoodsId());
                    goodsName = baGoods.getName()+";"+goodsName;
                }
            }
            if(StringUtils.isNotEmpty(goodsName) && goodsName.equals("") == false){
                enterprise.setGoodsName(goodsName.substring(0,goodsName.length()-1));
            }

            //岗位名称
            String name = getName.getName(enterprise.getUserId());
            //发起人
            if(name.equals("") == false){
                enterprise.setUserName(sysUserMapper.selectUserById(enterprise.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                enterprise.setUserName(sysUserMapper.selectUserById(enterprise.getUserId()).getNickName());
            }

        }
        return enterprises;
    }

    /**
     * 新增用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    @Override
    public int insertBaEnterprise(BaEnterprise baEnterprise)
    {
        //判断公司简称不能重复
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("company_abbreviation",baEnterprise.getCompanyAbbreviation());
        enterpriseQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -1;
        }
        //名称不能重复
        enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("name",baEnterprise.getName());
        enterpriseQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -2;
        }
        String redisIncreID = getRedisIncreID.getId();
        baEnterprise.setId(redisIncreID);
//        baEnterprise.setParentId(redisIncreID);
        baEnterprise.setRelationId(redisIncreID);
        baEnterprise.setCreateTime(DateUtils.getNowDate());
        baEnterprise.setCreateBy(SecurityUtils.getUsername());
        baEnterprise.setUserId(SecurityUtils.getUserId());
        baEnterprise.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baEnterprise.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baEnterprise.getWorkState())) {
            //流程发起状态
            baEnterprise.setWorkState("1");
            //默认审批通过
            baEnterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        }
        //新增所需煤种
        if(StringUtils.isNotNull(baEnterprise.getEnterpriseRelevance())){
            for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterprise.getEnterpriseRelevance()) {
                 baEnterpriseRelevance.setId(getRedisIncreID.getId());
                 baEnterpriseRelevance.setRelevanceId(baEnterprise.getId());
                enterpriseRelevanceMapper.insertBaEnterpriseRelevance(baEnterpriseRelevance);
            }
        }
        //新增开票信息
        if(StringUtils.isNotNull(baEnterprise.getBillingInformation())){
            BaBillingInformation baBillingInformation = baEnterprise.getBillingInformation();
            baBillingInformation.setId(getRedisIncreID.getId());
            baBillingInformation.setRelevanceId(baEnterprise.getId());
            baBillingInformationMapper.insertBaBillingInformation(baBillingInformation);
        }
        //基本账户
        if(StringUtils.isNotNull(baEnterprise.getBaBank())){
            BaBank baBank = baEnterprise.getBaBank();
            baBank.setId(getRedisIncreID.getId());
            baBank.setCreateTime(DateUtils.getNowDate());
            baBank.setCreateBy(SecurityUtils.getUsername());
            baBank.setRelationId(baEnterprise.getId());
            baBank.setState("1");
            baBankMapper.insertBaBank(baBank);
        }
        //收款账户
        List<BaBank> baBankList = baEnterprise.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                item.setId(getRedisIncreID.getId());
                item.setCreateTime(DateUtils.getNowDate());
                item.setCreateBy(SecurityUtils.getUsername());
                item.setRelationId(baEnterprise.getId());
                baBankMapper.insertBaBank(item);
            });
        }

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(),SecurityUtils.getCurrComId());
        baEnterprise.setFlowId(flowId);
        int result = baEnterpriseMapper.insertBaEnterprise(baEnterprise);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baEnterprise.getWorkState())) {
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baEnterprise, AdminCodeEnum.ENTERPRISE_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
//                if(!ObjectUtils.isEmpty(baChargingStandards)){
//                    baChargingStandardsMapper.deleteBaChargingStandardsByRelationId(baChargingStandards.getRelationId());
//                }
                    baEnterpriseMapper.deleteBaEnterpriseById(baEnterprise.getId());
                    return 0;
                } else {
                    baEnterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_VERIFYING.getCode());
                    baEnterpriseMapper.updateBaEnterprise(baEnterprise);
                }
            }
        }
        return result;
    }

    /**
     * 提交终端企业审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaEnterprise enterprise, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(enterprise.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(enterprise.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //获取流程实例关联的业务对象
        BaEnterprise baEnterprise = this.selectBaEnterpriseById(enterprise.getId());
        baEnterprise.setEnterpriseRelevance(enterprise.getEnterpriseRelevance());
        baEnterprise.setBaBankList(enterprise.getBaBankList());
        baEnterprise.setBaBank(enterprise.getBaBank());
        SysUser sysUser = iSysUserService.selectUserById(baEnterprise.getUserId());
        baEnterprise.setUserName(sysUser.getUserName());
        baEnterprise.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baEnterprise, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO, sysUser);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime, SysUser user) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = new UserInfo();
        userInfo.setId(String.valueOf(user.getUserId()));
        userInfo.setName(user.getUserName());
        userInfo.setNickName(user.getNickName());
        //根据部门ID获取部门信息
        SysDept sysDept = iSysDeptService.selectDeptById(user.getDeptId());
        if(!ObjectUtils.isEmpty(sysDept)){
            userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
        }
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        LoginUser loginUser1 = SecurityUtils.getLoginUser();
        loginUser1.setDept(user.getDeptId());
        loginUser1.setDeptId(user.getDeptId());
        loginUser1.setUserId(user.getUserId());
        try {
            redisCache.lock(Constants.LOGIN_USER, loginUser1, lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.ENTERPRISE_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(user.getUserName());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }
    /**
     * 修改用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    @Override
    public int updateBaEnterprise(BaEnterprise baEnterprise)
    {
        //获取原终端企业信息
        BaEnterprise baEnterprise1 = baEnterpriseMapper.selectBaEnterpriseById(baEnterprise.getId());
        if(baEnterprise1.getCompanyAbbreviation().equals(baEnterprise.getCompanyAbbreviation()) == false){
            QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("company_abbreviation",baEnterprise.getCompanyAbbreviation());
            enterpriseQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            }
            BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        if(baEnterprise1.getName().equals(baEnterprise.getName()) == false){
            QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("name",baEnterprise.getName());
            enterpriseQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            }
            BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
//        //查询收费标准
//        QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
//        baChargingStandardsQueryWrapper.eq("relation_id",baEnterprise1.getId());
//
//        BaChargingStandards baChargingStandards1 = baChargingStandardsMapper.selectOne(baChargingStandardsQueryWrapper);
//        if(!ObjectUtils.isEmpty(baChargingStandards1)){
//            //更新收费标准
//            iBaChargingStandardsService.updateBaChargingStandards(baEnterprise.getBaChargingStandards());
//        } else {
//            //新增收费标准
//            iBaChargingStandardsService.insertBaChargingStandards(baEnterprise.getBaChargingStandards());
//        }
        if(StringUtils.isNotNull(baEnterprise.getEnterpriseRelevance())){
            for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterprise.getEnterpriseRelevance()) {
                if(StringUtils.isNotEmpty(baEnterpriseRelevance.getId())){
                    enterpriseRelevanceMapper.updateBaEnterpriseRelevance(baEnterpriseRelevance);
                }else {
                    baEnterpriseRelevance.setId(getRedisIncreID.getId());
                    baEnterpriseRelevance.setRelevanceId(baEnterprise.getId());
                    enterpriseRelevanceMapper.insertBaEnterpriseRelevance(baEnterpriseRelevance);
                }
            }
        }
        //设置变更标识
        baEnterprise.setApproveFlag("1"); //编辑
        //修改开票信息
        if(StringUtils.isNotNull(baEnterprise.getBillingInformation())){
            BaBillingInformation billingInformation = baEnterprise.getBillingInformation();
            if(StringUtils.isNotEmpty(billingInformation.getId())){
                baBillingInformationMapper.updateBaBillingInformation(baEnterprise.getBillingInformation());
            }else {
                billingInformation.setId(getRedisIncreID.getId());
                billingInformation.setRelevanceId(baEnterprise.getId());
                baBillingInformationMapper.insertBaBillingInformation(billingInformation);
            }
        }
        baEnterprise.setUpdateTime(DateUtils.getNowDate());
        baEnterprise.setUpdateBy(SecurityUtils.getUsername());
        //创建时间
        baEnterprise.setCreateTime(baEnterprise.getUpdateTime());
        //基本账户
        if(StringUtils.isNotNull(baEnterprise.getBaBank())){
            BaBank baBank = baEnterprise.getBaBank();
            baBank.setUpdateTime(DateUtils.getNowDate());
            baBank.setUpdateBy(SecurityUtils.getUsername());
            baBankMapper.updateBaBank(baBank);
        }
        //收款账户信息
        List<BaBank> baBankList = baEnterprise.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }

        int result = baEnterpriseMapper.updateBaEnterprise(baEnterprise);
        if(result > 0 && baEnterprise1.getState().equals(AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode()) == false){
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baEnterprise.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baEnterprise, AdminCodeEnum.ENTERPRISE_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                //回滚原收费标准
//                if(!ObjectUtils.isEmpty(baChargingStandards1)){
//                    baChargingStandardsMapper.updateBaChargingStandards(baChargingStandards1);
//                }
                baEnterpriseMapper.updateBaEnterprise(baEnterprise1);
                return 0;
            }else {
                baEnterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_VERIFYING.getCode());
                baEnterpriseMapper.updateBaEnterprise(baEnterprise);
            }
        }
        return result;
    }

    @Override
    public int updateEnterprise(BaEnterprise baEnterprise) {
        //收款账户信息
        List<BaBank> baBankList = baEnterprise.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }
        return baEnterpriseMapper.updateBaEnterprise(baEnterprise);
    }

    /**
     * 批量删除用煤企业
     *
     * @param ids 需要删除的用煤企业ID
     * @return 结果
     */
    @Override
    public int deleteBaEnterpriseByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
              BaEnterprise baEnterprise =  baEnterpriseMapper.selectBaEnterpriseById(id);
              baEnterprise.setFlag(1);
              baEnterpriseMapper.updateBaEnterprise(baEnterprise);
              //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 500;
        }
        return 200;
    }

    /**
     * 删除用煤企业信息
     *
     * @param id 用煤企业ID
     * @return 结果
     */
    @Override
    public int deleteBaEnterpriseById(String id)
    {
        return baEnterpriseMapper.deleteBaEnterpriseById(id);
    }

    /**
     * 封装终端企业下拉列表
     */
    @Override
    public List<BaEnterprise> selectEnterpriseList() {
        //查询企业分类数据字典
        List<SysDictData> sysDictDataList = sysDictDataMapper.selectDictDataByType("company_classify");
        List<SysDictDataVO> sysDictDataVOS = beanUtil.listConvert(sysDictDataList, SysDictDataVO.class);
        List<BaEnterprise> allAaEnterprisesList = new ArrayList<>();
        for(SysDictDataVO sysDictDataVO : sysDictDataVOS){
            BaEnterprise baEnterprise = new BaEnterprise();
            baEnterprise.setCompanyClassify(Integer.parseInt(sysDictDataVO.getDictValue()));
            baEnterprise.setBaEnterprises(this.selectBaEnterpriseList(baEnterprise));
            baEnterprise.setName(sysDictDataVO.getDictLabel());
            baEnterprise.setId(sysDictDataVO.getDictValue());
            allAaEnterprisesList.add(baEnterprise);
        }
        return allAaEnterprisesList;
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",enterprise.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.ENTERPRISE_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(enterprise.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                enterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_WITHDRAW.getCode());
                baEnterpriseMapper.updateBaEnterprise(enterprise);
                String userId = String.valueOf(enterprise.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(enterprise, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int updateProcessBusinessData(BaEnterpriseInstanceRelatedEditDTO baEnterpriseInstanceRelatedEditDTO) {
        BaEnterprise baEnterprise = baEnterpriseInstanceRelatedEditDTO.getBaEnterprise();
        if (!ObjectUtils.isEmpty(baEnterpriseInstanceRelatedEditDTO.getBaEnterprise())) {
            //获取原企业信息
            BaEnterprise baEnterprise1 = baEnterpriseMapper.selectBaEnterpriseById(baEnterprise.getId());
            if (StringUtils.isNotEmpty(baEnterprise1.getCompanyAbbreviation()) && baEnterprise1.getCompanyAbbreviation().equals(baEnterprise1.getCompanyAbbreviation())) {
                QueryWrapper<BaEnterprise> baEnterpriseQueryWrapper = new QueryWrapper<>();
                baEnterpriseQueryWrapper.eq("company_abbreviation", baEnterprise.getCompanyAbbreviation());
                baEnterpriseQueryWrapper.eq("flag", 0);
                baEnterpriseQueryWrapper.ne("id", baEnterprise1.getId());
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    baEnterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                }
                BaEnterprise selectOne = baEnterpriseMapper.selectOne(baEnterpriseQueryWrapper);
                if (StringUtils.isNotNull(selectOne)) {
                    return -1;
                }
            }
            if(baEnterprise1.getName().equals(baEnterprise.getName()) == false){
                QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
                enterpriseQueryWrapper.eq("name",baEnterprise.getName());
                enterpriseQueryWrapper.eq("flag",0);
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                }
                BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
                if(StringUtils.isNotNull(selectOne)){
                    return -2;
                }
            }
            baEnterprise.setUpdateTime(DateUtils.getNowDate());
            baEnterprise.setUpdateBy(SecurityUtils.getUsername());
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(baEnterpriseInstanceRelatedEditDTO.getProcessInstanceId());
            //所需煤种
            List<BaEnterpriseRelevance> baEnterpriseRelevances = baEnterprise.getEnterpriseRelevance();
            if(!CollectionUtils.isEmpty(baEnterpriseRelevances)){
                for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterpriseRelevances) {
                    if(StringUtils.isNotEmpty(baEnterpriseRelevance.getGoodsId())){
                        baEnterpriseRelevance.setGoodsName(baGoodsMapper.selectBaGoodsById(baEnterpriseRelevance.getGoodsId()).getName());
                    }
                }
                baEnterprise.setEnterpriseRelevance(baEnterpriseRelevances);
            }
            baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(baEnterprise));
            try {
                baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
            } catch (Exception e) {
                return 0;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public int delete(String id) {
        if(StringUtils.isNotEmpty(id)){
            String[] enterpriseRelevanceArray = id.split(",");
            for(String enterpriseRelevanceId : enterpriseRelevanceArray){
                try {
                    enterpriseRelevanceMapper.deleteBaEnterpriseRelevanceById(enterpriseRelevanceId);
                } catch (Exception e) {
                    return 0;
                }
            }
        }
        return 1;
    }

    @Override
    public List<BaEnterprise> enterpriseList() {

        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("state","enterprise:pass");
        enterpriseQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        enterpriseQueryWrapper.orderByDesc("initiation_time","id");
        List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(enterpriseQueryWrapper);

        return baEnterpriseList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int changeBaEnterprise(BaEnterprise baEnterprise) {
        //获取原终端企业信息
        BaEnterprise baEnterprise1 = this.selectBaEnterpriseById(baEnterprise.getId());
        if(baEnterprise1.getCompanyAbbreviation().equals(baEnterprise.getCompanyAbbreviation()) == false){
            QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("company_abbreviation",baEnterprise.getCompanyAbbreviation());
            enterpriseQueryWrapper.eq("flag",0);
            BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        if(baEnterprise1.getName().equals(baEnterprise.getName()) == false){
            QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("name",baEnterprise.getName());
            enterpriseQueryWrapper.eq("flag",0);
            BaEnterprise selectOne = baEnterpriseMapper.selectOne(enterpriseQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
        if("".equals(decideChange(baEnterprise, baEnterprise1))){
            return -3;
        }
        //伪删除当前表之前终端企业
        iBaEnterpriseService.deleteBaEnterpriseByIds(new String[]{baEnterprise.getId()});
        baEnterprise.setParentId(baEnterprise.getId());
        String redisIncreID = getRedisIncreID.getId();
        baEnterprise.setId(redisIncreID);
        //设置变更标识
        baEnterprise.setApproveFlag("2"); //变更
        if(StringUtils.isNotNull(baEnterprise.getEnterpriseRelevance())){
            for (BaEnterpriseRelevance baEnterpriseRelevance:baEnterprise.getEnterpriseRelevance()) {
                baEnterpriseRelevance.setParentId(baEnterpriseRelevance.getId());
                baEnterpriseRelevance.setId(getRedisIncreID.getId());
                baEnterpriseRelevance.setRelevanceId(redisIncreID);
                enterpriseRelevanceMapper.insertBaEnterpriseRelevance(baEnterpriseRelevance);
            }
        }
        //修改开票信息
        if(StringUtils.isNotNull(baEnterprise.getBillingInformation())){
            baEnterprise.getBillingInformation().setId(getRedisIncreID.getId());
            baEnterprise.getBillingInformation().setRelevanceId(redisIncreID);
            baBillingInformationMapper.insertBaBillingInformation(baEnterprise.getBillingInformation());
        }
        baEnterprise.setUpdateTime(DateUtils.getNowDate());
        baEnterprise.setUpdateBy(SecurityUtils.getUsername());
        //创建时间
        baEnterprise.setCreateTime(baEnterprise.getUpdateTime());
        baEnterprise.setChangeTime(DateUtils.getNowDate());
        baEnterprise.setChangeBy(SecurityUtils.getUsername());
        //基本账户
        if(StringUtils.isNotNull(baEnterprise.getBaBank())){
            BaBank baBank = baEnterprise.getBaBank();
            baBank.setUpdateTime(DateUtils.getNowDate());
            baBank.setUpdateBy(SecurityUtils.getUsername());
            baBank.setParentId(baBank.getId());
            baBank.setId(getRedisIncreID.getId());
            baBank.setRelationId(redisIncreID);
            baBankMapper.insertBaBank(baBank);
        }
        //收款账户信息
        List<BaBank> baBankList = baEnterprise.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                item.setParentId(item.getId());
                item.setId(getRedisIncreID.getId());
                item.setRelationId(redisIncreID);
                item.setCreateTime(DateUtils.getNowDate());
                item.setCreateBy(SecurityUtils.getUsername());
                baBankMapper.insertBaBank(item);
            });
        }
        int result = baEnterpriseMapper.insertBaEnterprise(baEnterprise);
        //变更，插入历史信息到历史表
        BaHiEnterprise baHiEnterprise = beanUtil.beanConvert(baEnterprise1, BaHiEnterprise.class);
        baHiEnterprise.setId(baEnterprise1.getId());
        baHiEnterprise.setRelationId(baEnterprise1.getRelationId());
        baHiEnterprise.setFlag(0);
        if(!StringUtils.isEmpty(baEnterprise1.getParentId())){
            baHiEnterprise.setParentId(baEnterprise1.getParentId());
        }
        BaHiEnterprise baHiEnterprise1 = baHiEnterpriseMapper.selectBaHiEnterpriseById(baHiEnterprise.getId());
        if(ObjectUtils.isEmpty(baHiEnterprise1)){
            baHiEnterpriseMapper.insertBaHiEnterprise(baHiEnterprise);
        } else {
            baHiEnterprise.setUpdateBy(SecurityUtils.getUsername());
            baHiEnterprise.setUpdateTime(DateUtils.getNowDate());
            baHiEnterpriseMapper.updateBaHiEnterprise(baHiEnterprise);
        }

        //变更包含启动流程字段启动流程
        if("isStart".equals(decideChange(baEnterprise, baEnterprise1))){
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baEnterprise.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baEnterprise, AdminCodeEnum.ENTERPRISE_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                //回滚原收费标准
//                if(!ObjectUtils.isEmpty(baChargingStandards1)){
//                    baChargingStandardsMapper.updateBaChargingStandards(baChargingStandards1);
//                }
                baEnterpriseMapper.updateBaEnterprise(baEnterprise1);
                return 0;
            }else {
                baEnterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_VERIFYING.getCode());
                baEnterpriseMapper.updateBaEnterprise(baEnterprise);
            }
        } else {
            //变更后知会
            if("2".equals(baEnterprise.getApproveFlag())){ //变更
                //查询之前版本一条数据
                QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
                enterpriseQueryWrapper.eq("relation_id", baEnterprise1.getRelationId());
                enterpriseQueryWrapper.orderByDesc("create_time");
                List<BaEnterprise> baEnterprises = baEnterpriseMapper.selectList(enterpriseQueryWrapper);
                List<String> processInstanceIds = new ArrayList<>();
                if(!CollectionUtils.isEmpty(baEnterprises)){
                    for(BaEnterprise enterprise : baEnterprises){
                        //流程实例ID
                        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
                        relatedQueryWrapper.eq("business_id", enterprise.getId());
                        relatedQueryWrapper.orderByDesc("create_time");
                        List<BaProcessInstanceRelated> baProcessInstanceRelateds = new ArrayList<>();
                        baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                        if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                            }
                            break;
                        }
                    }

                    if(!CollectionUtils.isEmpty(processInstanceIds)){
                        String processInstanceId = processInstanceIds.get(0);
                        //给所有已审批的用户及抄送人发消息
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(String.valueOf(baEnterprise.getUserId()));
                        processInstanceApproveLinkDTO.setProcessInstanceId(processInstanceId);
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baEnterprise, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.CHANGE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_CHANGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                        ProcessTaskQueryDTO queryDTO = new ProcessTaskQueryDTO();
//                        queryDTO.setUserId(String.valueOf(baEnterprise.getUserId()));
                        queryDTO.setProcessInstanceId(processInstanceId);
                        List<ProcessHiIdentitylink> processHiIdentitylinks = workFlowFeignClient.listSendUser(queryDTO);
                        if(!CollectionUtils.isEmpty(processHiIdentitylinks)){
                            for(ProcessHiIdentitylink processHiIdentitylink : processHiIdentitylinks){
                                this.insertMessage(baEnterprise, processHiIdentitylink.getUserId(), MessageFormat.format(MessageConstant.CHANGE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_CHANGE));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<BaHiEnterprise> selectChangeBaHiEnterpriseList(BaHiEnterprise baHiEnterprise) {
        return baHiEnterpriseMapper.selectChangeBaHiEnterpriseList(baHiEnterprise);
    }

    @Override
    public List<String> selectChangeDataList(String id) {
        BaEnterprise baEnterprise = this.selectBaEnterpriseById(id);
        //获取原终端企业信息
        BaEnterprise baEnterprise1 = this.selectBaEnterpriseById(baEnterprise.getParentId());
        return getChangeData(baEnterprise, baEnterprise1);
    }

    @Override
    public List<BaHiEnterprise> selectChangeDataBaHiEnterpriseList(BaHiEnterprise baHiEnterprise) {
        //查询变更记录
        baHiEnterprise.setParentId("parentId");
        baHiEnterprise.setTenantId(SecurityUtils.getCurrComId());
        List<BaHiEnterprise> hiEnterprises = baHiEnterpriseMapper.selectChangeDataBaHiEnterpriseList(baHiEnterprise);
        if(!CollectionUtils.isEmpty(hiEnterprises)){
            hiEnterprises.stream().forEach(item -> {
                SysUser sysUser = sysUserMapper.selectUserByUserName(item.getChangeBy());
                if(!ObjectUtils.isEmpty(sysUser)){
                    //岗位名称
                    String name = getName.getName(sysUser.getUserId());
                    if(StringUtils.isNotEmpty(name)){
                        sysUser.setPostName(name.substring(0,name.length()-1));
                    }
                    item.setChangeUser(sysUser);
                }
            });
        }
        return hiEnterprises;
    }

    @Override
    public String getCompanyType(String id) {
        String companyType = "";
        //查询终端企业
        BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(id);
        if(!ObjectUtils.isEmpty(baEnterprise)){
            companyType = baEnterprise.getState();
        } else if(StringUtils.isEmpty(companyType)){
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(id);
            if(!ObjectUtils.isEmpty(baSupplier)){
                companyType = baSupplier.getState();
            }
        } else if(StringUtils.isEmpty(companyType)){
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(id);
            if(!ObjectUtils.isEmpty(baCompany)){
                companyType = baCompany.getState();
            }
        }
        return companyType;
    }

    @Override
    public List<BaEnterpriseVO> countRanking() {
        return baEnterpriseMapper.countRanking();
    }

    /**
     * 判断是否有字段变更
     */
    public String decideChange(BaEnterprise baEnterprise, BaEnterprise baEnterpriseBefore) {
        String result = "";
        //判断开票信息
        if((ObjectUtils.isEmpty(baEnterprise.getBillingInformation()) && !ObjectUtils.isEmpty(baEnterpriseBefore.getBillingInformation())) ||
                (!ObjectUtils.isEmpty(baEnterprise.getBillingInformation()) && ObjectUtils.isEmpty(baEnterpriseBefore.getBillingInformation()))){
            result = "isStart";
        } else if(!ObjectUtils.isEmpty(baEnterprise.getBillingInformation()) && !ObjectUtils.isEmpty(baEnterpriseBefore.getBillingInformation())){
            if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getName()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getName())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getName()) &&
                !baEnterprise.getBillingInformation().getName().equals(baEnterpriseBefore.getBillingInformation().getName()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getDutyParagraph()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getDutyParagraph())) || (
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getDutyParagraph()) &&
                            !baEnterprise.getBillingInformation().getDutyParagraph()
                            .equals(baEnterpriseBefore.getBillingInformation().getDutyParagraph()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getWorkAddress()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getWorkAddress())) || (
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getWorkAddress()) &&
                    !baEnterprise.getBillingInformation().getWorkAddress()
                            .equals(baEnterpriseBefore.getBillingInformation().getWorkAddress()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getPhone()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getPhone())) ||
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getPhone()) &&
                            !baEnterprise.getBillingInformation().getPhone()
                            .equals(baEnterpriseBefore.getBillingInformation().getPhone())){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getBankName()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getBankName())) || (
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getBankName()) &&
                            !baEnterprise.getBillingInformation().getBankName()
                            .equals(baEnterpriseBefore.getBillingInformation().getBankName()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBillingInformation().getAccount()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getBillingInformation().getAccount())) || (
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getAccount()) &&
                    !baEnterprise.getBillingInformation().getAccount()
                            .equals(baEnterpriseBefore.getBillingInformation().getAccount()))){
                result = "isStart";
            }
        }
        if("".equals(result)){
            //需要流程审批字段
            if((StringUtils.isEmpty(baEnterprise.getCompanyNature()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getCompanyNature())) || (
                    StringUtils.isNotEmpty(baEnterprise.getCompanyNature()) &&
                    !baEnterprise.getCompanyNature().equals(baEnterpriseBefore.getCompanyNature()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getProvince()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getProvince())) || (
                    StringUtils.isNotEmpty(baEnterprise.getProvince()) &&
                    !baEnterprise.getProvince().equals(baEnterpriseBefore.getProvince()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getCity()) &&
                    StringUtils.isNotEmpty(baEnterpriseBefore.getCity())) || (
                    StringUtils.isNotEmpty(baEnterprise.getCity()) &&
                    !baEnterprise.getCity().equals(baEnterpriseBefore.getCity()))){
                result = "isStart";
            } else if((ObjectUtils.isEmpty(baEnterprise.getTenableDate()) &&
                    !ObjectUtils.isEmpty(baEnterpriseBefore.getTenableDate())) || (
                    !ObjectUtils.isEmpty(baEnterprise.getTenableDate()) &&
                    !baEnterprise.getTenableDate().equals(baEnterpriseBefore.getTenableDate()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getCounterpart()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getCounterpart())) || (
                    !StringUtils.isEmpty(baEnterprise.getCounterpart()) &&
                    !baEnterprise.getCounterpart().equals(baEnterpriseBefore.getCounterpart()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getMobilePhone()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getMobilePhone())) || (
                    !StringUtils.isEmpty(baEnterprise.getMobilePhone()) &&
                    !baEnterprise.getMobilePhone().equals(baEnterpriseBefore.getMobilePhone()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getCompanyAbbreviation()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getCompanyAbbreviation())) || (
                    !StringUtils.isEmpty(baEnterprise.getCompanyAbbreviation()) &&
                    !baEnterprise.getCompanyAbbreviation().equals(baEnterpriseBefore.getCompanyAbbreviation()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getIndustry()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getIndustry())) || (
                    !StringUtils.isEmpty(baEnterprise.getIndustry()) &&
                    !baEnterprise.getIndustry().equals(baEnterpriseBefore.getIndustry()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getEnterpriseNature()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getEnterpriseNature())) || (
                    !StringUtils.isEmpty(baEnterprise.getEnterpriseNature()) &&
                    !baEnterprise.getEnterpriseNature().equals(baEnterpriseBefore.getEnterpriseNature()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getStation()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getStation())) || (
                    StringUtils.isNotEmpty(baEnterprise.getStation()) &&
                    !baEnterprise.getStation().equals(baEnterpriseBefore.getStation()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getStationAddress()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getStationAddress())) || (
                    !StringUtils.isEmpty(baEnterprise.getStationAddress()) &&
                    !baEnterprise.getStationAddress().equals(baEnterpriseBefore.getStationAddress()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBankName()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getBankName())) || (
                    !StringUtils.isEmpty(baEnterprise.getBankName()) &&
                    !baEnterprise.getBankName().equals(baEnterpriseBefore.getBankName()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getAccount()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getAccount())) || (
                    !StringUtils.isEmpty(baEnterprise.getAccount()) &&
                    !baEnterprise.getAccount().equals(baEnterpriseBefore.getAccount()))){
                result = "isStart";
            } else if((baEnterprise.getCeiling() != null && baEnterpriseBefore.getCeiling() == null) ||
                    (baEnterprise.getCeiling() == null && baEnterpriseBefore.getCeiling() != null) || (
                    baEnterprise.getCeiling() != null && baEnterpriseBefore.getCeiling() != null &&
                    baEnterprise.getCeiling().intValue() != baEnterpriseBefore.getCeiling().intValue())){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getName()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getName())) || (
                    !StringUtils.isEmpty(baEnterprise.getName()) &&
                    !baEnterprise.getName().equals(baEnterpriseBefore.getName()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getTaxNum()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getTaxNum())) || (
                    !StringUtils.isEmpty(baEnterprise.getTaxNum()) &&
                    !baEnterprise.getTaxNum().equals(baEnterpriseBefore.getTaxNum()))){
                result = "isStart";
            } else if((StringUtils.isEmpty(baEnterprise.getEnterpriseDesc()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getEnterpriseDesc())) || (
                    !StringUtils.isEmpty(baEnterprise.getEnterpriseDesc()) &&
                    !baEnterprise.getEnterpriseDesc().equals(baEnterpriseBefore.getEnterpriseDesc()))){  //不需要流程审批字段
                result = "noStart";
            } else if((StringUtils.isEmpty(baEnterprise.getPaymentTime()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getPaymentTime())) || (
                    !StringUtils.isEmpty(baEnterprise.getPaymentTime()) &&
                    !baEnterprise.getPaymentTime().equals(baEnterpriseBefore.getPaymentTime()))){
                result = "noStart";
            } else if((StringUtils.isEmpty(baEnterprise.getTransportType()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getTransportType())) || (
                    !StringUtils.isEmpty(baEnterprise.getTransportType()) &&
                    !baEnterprise.getTransportType().equals(baEnterpriseBefore.getTransportType()))){
                result = "noStart";
            } else if((StringUtils.isEmpty(baEnterprise.getBidPenaltyRules()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getBidPenaltyRules())) || (
                    !StringUtils.isEmpty(baEnterprise.getBidPenaltyRules()) &&
                    !baEnterprise.getBidPenaltyRules().equals(baEnterpriseBefore.getBidPenaltyRules()))){
                result = "noStart";
            } else if((StringUtils.isEmpty(baEnterprise.getPerformanceBond()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getPerformanceBond())) || (
                    !StringUtils.isEmpty(baEnterprise.getPerformanceBond()) &&
                    !baEnterprise.getPerformanceBond().equals(baEnterpriseBefore.getPerformanceBond()))){
                result = "noStart";
            } else if((StringUtils.isEmpty(baEnterprise.getDefaultAssessment()) &&
                    !StringUtils.isEmpty(baEnterpriseBefore.getDefaultAssessment())) || (
                    !StringUtils.isEmpty(baEnterprise.getDefaultAssessment()) &&
                    !baEnterprise.getDefaultAssessment().equals(baEnterpriseBefore.getDefaultAssessment()))){
                result = "noStart";
            }
        }
        //所需煤种判断
        if("".equals(result)){
            List<BaEnterpriseRelevance> enterpriseRelevance = baEnterprise.getEnterpriseRelevance();
            List<BaEnterpriseRelevance> enterpriseRelevanceBefore = baEnterpriseBefore.getEnterpriseRelevance();
            if((CollectionUtils.isEmpty(enterpriseRelevance) && !CollectionUtils.isEmpty(enterpriseRelevanceBefore)) ||
                    (!CollectionUtils.isEmpty(enterpriseRelevance) && CollectionUtils.isEmpty(enterpriseRelevanceBefore))){
                result = "noStart";
            } else if(!CollectionUtils.isEmpty(enterpriseRelevance)
                && !CollectionUtils.isEmpty(enterpriseRelevanceBefore)){
                if(enterpriseRelevance.size() != enterpriseRelevanceBefore.size()){
                    result = "noStart";
                } else {
                    for(int i = 0; i < enterpriseRelevance.size(); i++){
                        BaEnterpriseRelevance baEnterpriseRelevance = enterpriseRelevance.get(i);
                        BaEnterpriseRelevance baEnterpriseRelevanceBefore = enterpriseRelevanceBefore.get(i);
                        if((StringUtils.isEmpty(baEnterpriseRelevance.getGoodsId()) && !StringUtils.isEmpty(baEnterpriseRelevanceBefore.getGoodsId())) ||
                                (!StringUtils.isEmpty(baEnterpriseRelevance.getGoodsId()) && !baEnterpriseRelevance.getGoodsId().equals(baEnterpriseRelevanceBefore.getGoodsId()))){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baEnterpriseRelevance.getTarget()) && StringUtils.isNotEmpty(baEnterpriseRelevanceBefore.getTarget())) ||
                                (StringUtils.isNotEmpty(baEnterpriseRelevance.getTarget()) && !baEnterpriseRelevance.getTarget().equals(baEnterpriseRelevanceBefore.getTarget()))){
                            result = "noStart";
                            break;
                        } else if((baEnterpriseRelevance.getMonthlyConsumption() == null && baEnterpriseRelevanceBefore.getMonthlyConsumption() != null) ||
                                (baEnterpriseRelevance.getMonthlyConsumption() != null && baEnterpriseRelevanceBefore.getMonthlyConsumption() == null) ||
                                (baEnterpriseRelevance.getMonthlyConsumption().intValue() != baEnterpriseRelevanceBefore.getMonthlyConsumption().intValue())){
                            result = "noStart";
                            break;
                        }
                    }
                }
            }
        }
        //银行账户信息List<BaBank> baBankList
        if("".equals(result)){
            List<BaBank> baBanks = baEnterprise.getBaBankList();
            List<BaBank> baBanksBefore = baEnterpriseBefore.getBaBankList();
            if((CollectionUtils.isEmpty(baBanks)
                    && !CollectionUtils.isEmpty(baBanksBefore)) || (!CollectionUtils.isEmpty(baBanks)
                    && CollectionUtils.isEmpty(baBanksBefore))){
                result = "noStart";
            }
            if(!CollectionUtils.isEmpty(baBanks)
                    && !CollectionUtils.isEmpty(baBanksBefore)){
                if(baBanks.size() != baBanksBefore.size()){
                    result = "noStart";
                } else {
                    for(int i = 0; i < baBanks.size(); i++){
                        BaBank baBank = baBanks.get(i);
                        BaBank baBankBefore = baBanksBefore.get(i);
                        if((StringUtils.isEmpty(baBank.getRelationId()) && StringUtils.isNotEmpty(baBankBefore.getRelationId())) ||
                                (StringUtils.isNotEmpty(baBank.getRelationId()) && !baBank.getRelationId().equals(baBankBefore.getRelationId()))){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baBank.getOpenName()) && StringUtils.isNotEmpty(baBankBefore.getOpenName())) ||
                                StringUtils.isNotEmpty(baBank.getOpenName()) && !baBank.getOpenName().equals(baBankBefore.getOpenName())){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baBank.getBankName()) && StringUtils.isNotEmpty(baBankBefore.getBankName())) ||
                                (StringUtils.isNotEmpty(baBank.getBankName()) && !baBank.getBankName().equals(baBankBefore.getBankName()))){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baBank.getAccount()) && StringUtils.isNotEmpty(baBankBefore.getAccount())) ||
                                (StringUtils.isNotEmpty(baBank.getAccount()) && !baBank.getAccount().equals(baBankBefore.getAccount()))){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baBank.getInterBankNumber()) && StringUtils.isNotEmpty(baBankBefore.getInterBankNumber())) ||
                                (StringUtils.isNotEmpty(baBank.getInterBankNumber()) && !baBank.getInterBankNumber().equals(baBankBefore.getInterBankNumber()))){
                            result = "noStart";
                            break;
                        } else if((StringUtils.isEmpty(baBank.getMark()) && StringUtils.isNotEmpty(baBankBefore.getMark())) ||
                                (StringUtils.isNotEmpty(baBank.getMark()) && !baBank.getMark().equals(baBankBefore.getMark()))){
                            result = "noStart";
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> getChangeData(BaEnterprise baEnterprise, BaEnterprise baEnterpriseBefore) {
        List<String> isChangeList = new ArrayList<>();
        //判断开票信息
        if(!ObjectUtils.isEmpty(baEnterprise.getBillingInformation())){
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getName()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getName())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getName()) && !baEnterprise.getBillingInformation().getName().equals(baEnterpriseBefore.getBillingInformation().getName()))){
                isChangeList.add("billingInformationName");
            }
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getDutyParagraph()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getDutyParagraph())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getDutyParagraph())
                            && !baEnterprise.getBillingInformation().getDutyParagraph()
                            .equals(baEnterpriseBefore.getBillingInformation().getDutyParagraph()))){
                isChangeList.add("billingInformationDutyParagraph");
            }
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getWorkAddress()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getWorkAddress())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getWorkAddress()) &&
                            !baEnterprise.getBillingInformation().getWorkAddress()
                            .equals(baEnterpriseBefore.getBillingInformation().getWorkAddress()))){
                isChangeList.add("billingInformationWorkAddress");
            }
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getPhone()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getPhone())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getPhone()) &&
                    !baEnterprise.getBillingInformation().getPhone()
                            .equals(baEnterpriseBefore.getBillingInformation().getPhone()))){
                isChangeList.add("billingInformationPhone");
            }
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getBankName()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getBankName())) ||
                    (StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getBankName()) &&
                    !baEnterprise.getBillingInformation().getBankName()
                            .equals(baEnterpriseBefore.getBillingInformation().getBankName()))){
                isChangeList.add("billingInformationBankName");
            }
            if((StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getAccount()) &&
                    StringUtils.isEmpty(baEnterpriseBefore.getBillingInformation().getAccount()) ||
                    StringUtils.isNotEmpty(baEnterprise.getBillingInformation().getAccount()) &&
                    !baEnterprise.getBillingInformation().getAccount()
                            .equals(baEnterpriseBefore.getBillingInformation().getAccount()))){
                isChangeList.add("billingInformationAccount");
            }
        }
        //需要流程审批字段
        if((StringUtils.isEmpty(baEnterprise.getCompanyNature()) &&
                StringUtils.isNotEmpty(baEnterpriseBefore.getCompanyNature())) || (
                StringUtils.isNotEmpty(baEnterprise.getCompanyNature()) &&
                        !baEnterprise.getCompanyNature().equals(baEnterpriseBefore.getCompanyNature()))){
            isChangeList.add("companyNature");
        }
        if((StringUtils.isEmpty(baEnterprise.getProvince()) &&
                StringUtils.isNotEmpty(baEnterpriseBefore.getProvince())) || (
                StringUtils.isNotEmpty(baEnterprise.getProvince()) &&
                        !baEnterprise.getProvince().equals(baEnterpriseBefore.getProvince()))){
            isChangeList.add("province");
        }
        if((StringUtils.isEmpty(baEnterprise.getCity()) &&
                StringUtils.isNotEmpty(baEnterpriseBefore.getCity())) || (
                StringUtils.isNotEmpty(baEnterprise.getCity()) &&
                        !baEnterprise.getCity().equals(baEnterpriseBefore.getCity()))){
            isChangeList.add("city");
        }
        if((ObjectUtils.isEmpty(baEnterprise.getTenableDate()) &&
                !ObjectUtils.isEmpty(baEnterpriseBefore.getTenableDate())) || (
                !ObjectUtils.isEmpty(baEnterprise.getTenableDate()) &&
                        !baEnterprise.getTenableDate().equals(baEnterpriseBefore.getTenableDate()))){
            isChangeList.add("tenableDate");
        }
        if((StringUtils.isEmpty(baEnterprise.getCounterpart()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getCounterpart())) || (
                !StringUtils.isEmpty(baEnterprise.getCounterpart()) &&
                        !baEnterprise.getCounterpart().equals(baEnterpriseBefore.getCounterpart()))){
            isChangeList.add("counterpart");
        }
        if((StringUtils.isEmpty(baEnterprise.getMobilePhone()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getMobilePhone())) || (
                !StringUtils.isEmpty(baEnterprise.getMobilePhone()) &&
                        !baEnterprise.getMobilePhone().equals(baEnterpriseBefore.getMobilePhone()))){
            isChangeList.add("mobilePhone");
        }
        if((StringUtils.isEmpty(baEnterprise.getCompanyAbbreviation()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getCompanyAbbreviation())) || (
                !StringUtils.isEmpty(baEnterprise.getCompanyAbbreviation()) &&
                        !baEnterprise.getCompanyAbbreviation().equals(baEnterpriseBefore.getCompanyAbbreviation()))){
            isChangeList.add("companyAbbreviation");
        }
        if((StringUtils.isEmpty(baEnterprise.getIndustry()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getIndustry())) || (
                !StringUtils.isEmpty(baEnterprise.getIndustry()) &&
                        !baEnterprise.getIndustry().equals(baEnterpriseBefore.getIndustry()))){
            isChangeList.add("industry");
            baEnterprise.setIndustry(baEnterpriseBefore.getIndustry());
        }
        if((StringUtils.isEmpty(baEnterprise.getEnterpriseNature()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getEnterpriseNature())) || (
                !StringUtils.isEmpty(baEnterprise.getEnterpriseNature()) &&
                        !baEnterprise.getEnterpriseNature().equals(baEnterpriseBefore.getEnterpriseNature()))){
            isChangeList.add("enterpriseNature");
        }
        if((StringUtils.isEmpty(baEnterprise.getStation()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getStation())) || (
                StringUtils.isNotEmpty(baEnterprise.getStation()) &&
                        !baEnterprise.getStation().equals(baEnterpriseBefore.getStation()))){
            isChangeList.add("station");
        }
        if((StringUtils.isEmpty(baEnterprise.getStationAddress()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getStationAddress())) || (
                !StringUtils.isEmpty(baEnterprise.getStationAddress()) &&
                        !baEnterprise.getStationAddress().equals(baEnterpriseBefore.getStationAddress()))){
            isChangeList.add("stationAddress");
        }
        if((StringUtils.isEmpty(baEnterprise.getBankName()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getBankName())) || (
                !StringUtils.isEmpty(baEnterprise.getBankName()) &&
                        !baEnterprise.getBankName().equals(baEnterpriseBefore.getBankName()))){
            isChangeList.add("bankName");
        }
        if((StringUtils.isEmpty(baEnterprise.getAccount()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getAccount())) || (
                !StringUtils.isEmpty(baEnterprise.getAccount()) &&
                        !baEnterprise.getAccount().equals(baEnterpriseBefore.getAccount()))){
            isChangeList.add("account");
        }
        if((baEnterprise.getCeiling() != null && baEnterpriseBefore.getCeiling() == null) ||
                (baEnterprise.getCeiling() == null && baEnterpriseBefore.getCeiling() != null) || (
                baEnterprise.getCeiling() != null && baEnterpriseBefore.getCeiling() != null &&
                        baEnterprise.getCeiling().intValue() != baEnterpriseBefore.getCeiling().intValue())){
            isChangeList.add("ceiling");
        }
        if((StringUtils.isEmpty(baEnterprise.getName()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getName())) || (
                !StringUtils.isEmpty(baEnterprise.getName()) &&
                        !baEnterprise.getName().equals(baEnterpriseBefore.getName()))){
            isChangeList.add("name");
        }
        if((StringUtils.isEmpty(baEnterprise.getEnterpriseDesc()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getEnterpriseDesc())) || (
                !StringUtils.isEmpty(baEnterprise.getEnterpriseDesc()) &&
                        !baEnterprise.getEnterpriseDesc().equals(baEnterpriseBefore.getEnterpriseDesc()))){  //不需要流程审批字段
            isChangeList.add("enterpriseDesc");
        }
        if((StringUtils.isEmpty(baEnterprise.getPaymentTime()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getPaymentTime())) || (
                !StringUtils.isEmpty(baEnterprise.getPaymentTime()) &&
                        !baEnterprise.getPaymentTime().equals(baEnterpriseBefore.getPaymentTime()))){
            isChangeList.add("paymentTime");
        }
        if((StringUtils.isEmpty(baEnterprise.getTransportType()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getTransportType())) || (
                !StringUtils.isEmpty(baEnterprise.getTransportType()) &&
                        !baEnterprise.getTransportType().equals(baEnterpriseBefore.getTransportType()))){
            isChangeList.add("transportType");
        }
        if((StringUtils.isEmpty(baEnterprise.getBidPenaltyRules()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getBidPenaltyRules())) || (
                !StringUtils.isEmpty(baEnterprise.getBidPenaltyRules()) &&
                        !baEnterprise.getBidPenaltyRules().equals(baEnterpriseBefore.getBidPenaltyRules()))){
            isChangeList.add("bidPenaltyRules");
        }
        if((StringUtils.isEmpty(baEnterprise.getPerformanceBond()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getPerformanceBond())) || (
                !StringUtils.isEmpty(baEnterprise.getPerformanceBond()) &&
                        !baEnterprise.getPerformanceBond().equals(baEnterpriseBefore.getPerformanceBond()))){
            isChangeList.add("performanceBond");
        }
        if((StringUtils.isEmpty(baEnterprise.getDefaultAssessment()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getDefaultAssessment())) || (
                !StringUtils.isEmpty(baEnterprise.getDefaultAssessment()) &&
                        !baEnterprise.getDefaultAssessment().equals(baEnterpriseBefore.getDefaultAssessment()))){
            isChangeList.add("defaultAssessment");
        }
        if((StringUtils.isEmpty(baEnterprise.getTaxNum()) &&
                !StringUtils.isEmpty(baEnterpriseBefore.getTaxNum())) || (
                !StringUtils.isEmpty(baEnterprise.getTaxNum()) &&
                        !baEnterprise.getTaxNum().equals(baEnterpriseBefore.getTaxNum()))){
            isChangeList.add("taxNum");
        }
        //所需煤种判断
        List<BaEnterpriseRelevance> enterpriseRelevance = baEnterprise.getEnterpriseRelevance();
        List<BaEnterpriseRelevance> enterpriseRelevanceBefore = baEnterpriseBefore.getEnterpriseRelevance();
        Map<String, BaEnterpriseRelevance> enterpriseRelevanceBeforeMap = new HashMap<>();
        //返回之前版本指标参数数据ID集合
        if(!CollectionUtils.isEmpty(enterpriseRelevanceBefore)){
            enterpriseRelevanceBeforeMap = enterpriseRelevanceBefore
                    .stream().collect(Collectors.toMap(BaEnterpriseRelevance::getId, BaEnterpriseRelevance->BaEnterpriseRelevance));
        }

        if(!CollectionUtils.isEmpty(enterpriseRelevance)){
            for(int i = 0; i < enterpriseRelevance.size(); i++){
                BaEnterpriseRelevance baEnterpriseRelevance = enterpriseRelevance.get(i);
                if(!enterpriseRelevanceBeforeMap.keySet().contains(baEnterpriseRelevance.getParentId())){
                    isChangeList.add(baEnterpriseRelevance.getId());
                } else {
                    BaEnterpriseRelevance baEnterpriseRelevanceBefore = enterpriseRelevanceBeforeMap.get(baEnterpriseRelevance.getParentId());
                    if(!ObjectUtils.isEmpty(baEnterpriseRelevance) && !ObjectUtils.isEmpty(baEnterpriseRelevanceBefore)){
                        if(!baEnterpriseRelevance.getGoodsId().equals(baEnterpriseRelevanceBefore.getGoodsId())){
                            isChangeList.add(baEnterpriseRelevance.getId() + "goodsId");
                        }
                        String target = baEnterpriseRelevance.getTarget();
                        String targetBefore = baEnterpriseRelevanceBefore.getTarget();
//                        if(StringUtils.isNotEmpty(target)){
//                            //判断是否是jsonOjbect 还是 json数组
//                            Object object = JSON.parse(target);
//                            Object beforeObject = JSON.parse(targetBefore);
//                            if (object instanceof JSONObject && beforeObject instanceof  JSONObject) {
//                                JSONObject targetJson = (JSONObject) object;
//                                JSONObject targetBeforeJson = (JSONObject) beforeObject;
//                                String heat = targetJson.getString("heat");
//                                String heatBefore = targetBeforeJson.getString("heat");
//                                if((StringUtils.isEmpty(heat) && StringUtils.isNotEmpty(heatBefore)) ||
//                                        StringUtils.isNotEmpty(heat) && !heat.equals(heatBefore)){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "heat");
//                                }
//
//                                String wet = targetJson.getString("wet");
//                                String wetBefore = targetBeforeJson.getString("wet");
//                                if((StringUtils.isEmpty(wet) && StringUtils.isNotEmpty(wetBefore)) ||
//                                        StringUtils.isNotEmpty(wet) && !wet.equals(wetBefore) ){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "wet");
//                                }
//
//                                String sulfur = targetJson.getString("sulfur");
//                                String sulfurBefore = targetBeforeJson.getString("sulfur");
//                                if((StringUtils.isEmpty(sulfur) && StringUtils.isNotEmpty(sulfurBefore)) ||
//                                        StringUtils.isNotEmpty(sulfur) && !sulfur.equals(sulfurBefore) ){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "sulfur");
//                                }
//
//                                String ash = targetJson.getString("ash");
//                                String ashBefore = targetBeforeJson.getString("ash");
//                                if((StringUtils.isEmpty(ash) && StringUtils.isNotEmpty(ashBefore)) ||
//                                        StringUtils.isNotEmpty(ash) && !ash.equals(ashBefore) ){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "ash");
//                                }
//
//                                String volatilize = targetJson.getString("volatilize");
//                                String volatilizeBefore = targetBeforeJson.getString("volatilize");
//                                if((StringUtils.isEmpty(volatilize) && StringUtils.isNotEmpty(volatilizeBefore)) ||
//                                        StringUtils.isNotEmpty(volatilize) && !volatilize.equals(volatilizeBefore) ){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "volatilize");
//                                }
//
//                                String rest = targetJson.getString("rest");
//                                String restBefore = targetBeforeJson.getString("rest");
//                                if((StringUtils.isEmpty(rest) && StringUtils.isNotEmpty(restBefore)) ||
//                                        StringUtils.isNotEmpty(rest) && !volatilize.equals(restBefore) ){
//                                    isChangeList.add(baEnterpriseRelevance.getId() + "rest");
//                                }
//                            } else if (object instanceof JSONArray && beforeObject instanceof  JSONArray) {
//                                JSONArray targetJsonArray = (JSONArray) object;
////                                List<BaEnterpriseRelevanceVO> targetJsonList = JSONObject.parseArray(target, BaEnterpriseRelevanceVO.class);
//                                JSONArray targetBeforeJsonArray = (JSONArray) beforeObject;
//                                BaEnterpriseRelevanceVO beforeEnterpriseRelevanceVO = null;
//                                String labelStr = "";
//                                //拼接标识证明指标信息的标识与其他页面label不重复
//                                if(targetBeforeJsonArray.isEmpty() && targetBeforeJsonArray.size()>0){
//                                        for(int j = 0; j < targetBeforeJsonArray.size(); j++) {
//                                            // 遍历 jsonarray 数组，把每一个对象转成 json 对象
//                                            JSONObject targetBeforeObject = targetBeforeJsonArray.getJSONObject(i);
//                                            Object label = String.valueOf(targetBeforeObject.get("label"));
//                                            isChangeList.add("relevance" + "_" + label);
//                                        }
//                                } else {
//                                    List<BaEnterpriseRelevanceVO> targetBeforeJsonList = JSONObject.parseArray(targetBefore, BaEnterpriseRelevanceVO.class);
//                                    Map<String, BaEnterpriseRelevanceVO> targetBeforeJsonMap = new HashMap<>();
//                                    String label = "";
//                                    String value = "";
//                                    if(!CollectionUtils.isEmpty(targetBeforeJsonList)){
//                                        for(BaEnterpriseRelevanceVO enterpriseRelevanceVO : targetBeforeJsonList){
//                                            if(!ObjectUtils.isEmpty(enterpriseRelevanceVO)){
//                                                JSONObject label1 = enterpriseRelevanceVO.getLabel();
//                                                label = enterpriseRelevanceVO.getLabel().getString("label");
////                                                if(!targetBeforeJsonMap.containsKey(labelMap.)){
////                                                    targetBeforeJsonMap.put(label, beforeRelevanceVO);
////                                                }
//                                            }
//                                        }
////                                        for (BaEnterpriseRelevanceVO enterpriseRelevanceVO : targetJsonList) {
////                                            //判断targetBeforeJsonMap中是否包含label，如果不包含，说明是新增内容，页面显示红色
////                                            beforeEnterpriseRelevanceVO = targetBeforeJsonMap.get(enterpriseRelevanceVO.getLabel());
////                                            if(StringUtils.isNotEmpty(enterpriseRelevanceVO.getValue()) && !enterpriseRelevanceVO.getValue().equals(beforeEnterpriseRelevanceVO.getValue())){
////                                                isChangeList.add("relevance" + "_" + enterpriseRelevanceVO.getLabel());
////                                            }
////                                        }
//                                    }
//                                }
//                            }
//                        }

                        if((baEnterpriseRelevance.getMonthlyConsumption() != null && baEnterpriseRelevanceBefore.getMonthlyConsumption() == null) ||
                                (baEnterpriseRelevance.getMonthlyConsumption() == null && baEnterpriseRelevanceBefore.getMonthlyConsumption() != null) || (
                                baEnterpriseRelevance.getMonthlyConsumption() != null && baEnterpriseRelevanceBefore.getMonthlyConsumption() != null &&
                                        baEnterpriseRelevance.getMonthlyConsumption().intValue() != baEnterpriseRelevanceBefore.getMonthlyConsumption().intValue())){
                            isChangeList.add(baEnterpriseRelevance.getId() + "monthlyConsumption");
                        }
                    }
                }
            }
        }
        //银行账户信息List<BaBank> baBankList
        List<BaBank> baBanks = baEnterprise.getBaBankList();
        List<BaBank> baBanksBefore = baEnterpriseBefore.getBaBankList();
        Map<String, BaBank> baBanksBeforeMap = new HashMap<>();
        //返回之前版本指标参数数据ID集合
        if(!CollectionUtils.isEmpty(baBanksBefore)){
            baBanksBeforeMap = baBanksBefore
                    .stream().collect(Collectors.toMap(BaBank::getId, BaBank->BaBank));
        }

        if(!CollectionUtils.isEmpty(baBanks)){
            for(int i = 0; i < baBanks.size(); i++){
                BaBank baBank = baBanks.get(i);
                if(!baBanksBeforeMap.keySet().contains(baBank.getParentId())){
                    isChangeList.add(baBank.getId());
                } else {
                    BaBank baBankBefore = baBanksBeforeMap.get(baBank.getParentId());
                    if(!ObjectUtils.isEmpty(baBank) && !ObjectUtils.isEmpty(baBankBefore)){
                        if(!baBank.getOpenName().equals(baBankBefore.getOpenName())){
                            isChangeList.add(baBank.getId() + "openName");
                        }
                        if(!baBank.getBankName().equals(baBankBefore.getBankName())){
                            isChangeList.add(baBank.getId() + "bankName");
                        }
                        if(!baBank.getAccount().equals(baBankBefore.getAccount())){
                            isChangeList.add(baBank.getId() + "account");
                        }
                        if(!baBank.getInterBankNumber().equals(baBankBefore.getInterBankNumber())){
                            isChangeList.add(baBank.getId() + "interBankNumber");
                        }
                        if(!baBank.getMark().equals(baBankBefore.getMark())){
                            isChangeList.add(baBank.getId() + "mark");
                        }
                    }
                }
            }
        }
        return isChangeList;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaEnterprise enterprise, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(enterprise.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }
}
