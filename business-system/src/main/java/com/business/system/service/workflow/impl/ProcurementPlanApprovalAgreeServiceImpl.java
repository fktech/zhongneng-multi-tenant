package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IDeclareApprovalService;
import com.business.system.service.workflow.IProcurementPlanApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 采购计划审批结果:审批通过
 */
@Service("procurementPlanApprovalContent:processApproveResult:pass")
@Slf4j
public class ProcurementPlanApprovalAgreeServiceImpl extends IProcurementPlanApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaProcurementPlan baProcurementPlan = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.PROCUREMENTPLAN_STATUS_PASS.getCode());
        return result;
    }
}
