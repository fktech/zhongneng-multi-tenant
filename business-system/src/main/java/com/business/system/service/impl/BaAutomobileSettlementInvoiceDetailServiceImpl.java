package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAutomobileSettlementInvoiceDetailMapper;
import com.business.system.domain.BaAutomobileSettlementInvoiceDetail;
import com.business.system.service.IBaAutomobileSettlementInvoiceDetailService;

/**
 * 无车承运开票详情Service业务层处理
 *
 * @author single
 * @date 2023-04-13
 */
@Service
public class BaAutomobileSettlementInvoiceDetailServiceImpl extends ServiceImpl<BaAutomobileSettlementInvoiceDetailMapper, BaAutomobileSettlementInvoiceDetail> implements IBaAutomobileSettlementInvoiceDetailService
{
    @Autowired
    private BaAutomobileSettlementInvoiceDetailMapper baAutomobileSettlementInvoiceDetailMapper;

    /**
     * 查询无车承运开票详情
     *
     * @param id 无车承运开票详情ID
     * @return 无车承运开票详情
     */
    @Override
    public BaAutomobileSettlementInvoiceDetail selectBaAutomobileSettlementInvoiceDetailById(String id)
    {
        return baAutomobileSettlementInvoiceDetailMapper.selectBaAutomobileSettlementInvoiceDetailById(id);
    }

    /**
     * 查询无车承运开票详情列表
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 无车承运开票详情
     */
    @Override
    public List<BaAutomobileSettlementInvoiceDetail> selectBaAutomobileSettlementInvoiceDetailList(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        return baAutomobileSettlementInvoiceDetailMapper.selectBaAutomobileSettlementInvoiceDetailList(baAutomobileSettlementInvoiceDetail);
    }

    /**
     * 新增无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        baAutomobileSettlementInvoiceDetail.setCreateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceDetailMapper.insertBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail);
    }

    /**
     * 修改无车承运开票详情
     *
     * @param baAutomobileSettlementInvoiceDetail 无车承运开票详情
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlementInvoiceDetail(BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail)
    {
        baAutomobileSettlementInvoiceDetail.setUpdateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceDetailMapper.updateBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail);
    }

    /**
     * 批量删除无车承运开票详情
     *
     * @param ids 需要删除的无车承运开票详情ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceDetailByIds(String[] ids)
    {
        return baAutomobileSettlementInvoiceDetailMapper.deleteBaAutomobileSettlementInvoiceDetailByIds(ids);
    }

    /**
     * 删除无车承运开票详情信息
     *
     * @param id 无车承运开票详情ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceDetailById(String id)
    {
        return baAutomobileSettlementInvoiceDetailMapper.deleteBaAutomobileSettlementInvoiceDetailById(id);
    }

}
