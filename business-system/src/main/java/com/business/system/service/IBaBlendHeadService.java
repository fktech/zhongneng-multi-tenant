package com.business.system.service;

import com.business.system.domain.BaWashHead;

import java.util.List;

/**
 * 配煤出入库记录Service接口
 *
 * @author single
 * @date 2024-01-05
 */
public interface IBaBlendHeadService
{
    /**
     * 查询配煤出入库记录
     *
     * @param id 配煤出入库记录ID
     * @return 配煤出入库记录
     */
    public BaWashHead selectBaBlendHeadById(String id);

    /**
     * 查询配煤出入库记录列表
     *
     * @param baWashHead 配煤出入库记录
     * @return 配煤出入库记录集合
     */
    public List<BaWashHead> selectBaBlendHeadList(BaWashHead baWashHead);

    /**
     * 新增配煤出入库记录
     *
     * @param baWashHead 配煤出入库记录
     * @return 结果
     */
    public int insertBaBlendHead(BaWashHead baWashHead);

    /**
     * 修改配煤出入库记录
     *
     * @param baWashHead 配煤出入库记录
     * @return 结果
     */
    public int updateBaBlendHead(BaWashHead baWashHead);

    /**
     * 批量删除配煤出入库记录
     *
     * @param ids 需要删除的配煤出入库记录ID
     * @return 结果
     */
    public int deleteBaBlendHeadByIds(String[] ids);

    /**
     * 删除配煤出入库记录信息
     *
     * @param id 配煤出入库记录ID
     * @return 结果
     */
    public int deleteBaBlendHeadById(String id);

    /**
     * 新增配煤出库记录
     */
    public int insertBlendOutputdepot(BaWashHead baWashHead);

    /**
     * 修改配煤入库记录
     */
    public int updateBlendOutputdepot(BaWashHead baWashHead);

    /** 入库撤销 */
    public int intoRevoke(String id);

    /**
     * 配煤出库撤销
     */
    public int outputRevoke(String id);
}
