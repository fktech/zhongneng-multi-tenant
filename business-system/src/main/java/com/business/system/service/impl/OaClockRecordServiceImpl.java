package com.business.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.utils.DateUtils;
import com.business.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaClockRecordMapper;
import com.business.system.domain.OaClockRecord;
import com.business.system.service.IOaClockRecordService;

/**
 * 打卡记录Service业务层处理
 *
 * @author single
 * @date 2023-11-27
 */
@Service
public class OaClockRecordServiceImpl extends ServiceImpl<OaClockRecordMapper, OaClockRecord> implements IOaClockRecordService
{
    @Autowired
    private OaClockRecordMapper oaClockRecordMapper;

    @Autowired
    private SysUserMapper userMapper;

    /**
     * 查询打卡记录
     *
     * @param id 打卡记录ID
     * @return 打卡记录
     */
    @Override
    public OaClockRecord selectOaClockRecordById(String id)
    {
        return oaClockRecordMapper.selectOaClockRecordById(id);
    }

    /**
     * 查询打卡记录列表
     *
     * @param oaClockRecord 打卡记录
     * @return 打卡记录
     */
    @Override
    public List<OaClockRecord> selectOaClockRecordList(OaClockRecord oaClockRecord)
    {
        //时间格式化
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");
        //日期格式化
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

        List<OaClockRecord> oaClockRecords = oaClockRecordMapper.selectOaClockRecordList(oaClockRecord);
        for (OaClockRecord clockRecord:oaClockRecords) {
             //查看员工姓名
            SysUser user = userMapper.selectUserById(clockRecord.getEmployeeId());
            clockRecord.setEmployeeName(user.getNickName());
            //打卡时间
            clockRecord.setDateTime(date.format(clockRecord.getClockDate())+" "+time.format(clockRecord.getClockTime()));
        }
        return oaClockRecords;
    }

    /**
     * 新增打卡记录
     *
     * @param oaClockRecord 打卡记录
     * @return 结果
     */
    @Override
    public int insertOaClockRecord(OaClockRecord oaClockRecord)
    {
        oaClockRecord.setCreateTime(DateUtils.getNowDate());
        return oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
    }

    /**
     * 修改打卡记录
     *
     * @param oaClockRecord 打卡记录
     * @return 结果
     */
    @Override
    public int updateOaClockRecord(OaClockRecord oaClockRecord)
    {
        oaClockRecord.setUpdateTime(DateUtils.getNowDate());
        return oaClockRecordMapper.updateOaClockRecord(oaClockRecord);
    }

    /**
     * 批量删除打卡记录
     *
     * @param ids 需要删除的打卡记录ID
     * @return 结果
     */
    @Override
    public int deleteOaClockRecordByIds(String[] ids)
    {
        return oaClockRecordMapper.deleteOaClockRecordByIds(ids);
    }

    /**
     * 删除打卡记录信息
     *
     * @param id 打卡记录ID
     * @return 结果
     */
    @Override
    public int deleteOaClockRecordById(String id)
    {
        return oaClockRecordMapper.deleteOaClockRecordById(id);
    }

}
