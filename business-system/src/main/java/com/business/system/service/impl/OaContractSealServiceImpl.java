package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.OaEgress;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.BaProcessInstanceRelatedMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaContractSealMapper;
import com.business.system.domain.OaContractSeal;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * oa合同用印Service业务层处理
 *
 * @author single
 * @date 2024-06-06
 */
@Service
public class OaContractSealServiceImpl extends ServiceImpl<OaContractSealMapper, OaContractSeal> implements IOaContractSealService
{
    private static final Logger log = LoggerFactory.getLogger(OaContractSealServiceImpl.class);
    @Autowired
    private OaContractSealMapper oaContractSealMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    /**
     * 查询oa合同用印
     *
     * @param id oa合同用印ID
     * @return oa合同用印
     */
    @Override
    public OaContractSeal selectOaContractSealById(String id)
    {
        OaContractSeal contractSeal = oaContractSealMapper.selectOaContractSealById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",contractSeal.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        contractSeal.setProcessInstanceId(processInstanceIds);
        //用户信息
        SysUser user = sysUserMapper.selectUserById(contractSeal.getUserId());
        //岗位名称
        String name = getName.getName(user.getUserId());
        if (name.equals("") == false) {
            user.setPostName(name.substring(0, name.length() - 1));
        }
        contractSeal.setUser(user);
        return contractSeal;
    }

    /**
     * 查询oa合同用印列表
     *
     * @param oaContractSeal oa合同用印
     * @return oa合同用印
     */
    @Override
    public List<OaContractSeal> selectOaContractSealList(OaContractSeal oaContractSeal)
    {
        List<OaContractSeal> oaContractSeals = oaContractSealMapper.selectOaContractSealList(oaContractSeal);
        for (OaContractSeal contractSeal:oaContractSeals) {
            //用户名称
            SysUser user = sysUserMapper.selectUserById(contractSeal.getUserId());
            //岗位名称
            String name = getName.getName(user.getUserId());
            if (name.equals("") == false) {
                contractSeal.setUserName(user.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                contractSeal.setUserName(user.getNickName());
            }
        }
        return oaContractSeals;
    }

    /**
     * 新增oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    @Override
    public int insertOaContractSeal(OaContractSeal oaContractSeal)
    {
        oaContractSeal.setId(getRedisIncreID.getId());
        oaContractSeal.setCreateTime(DateUtils.getNowDate());
        oaContractSeal.setCreateBy(SecurityUtils.getUsername());
        oaContractSeal.setUserId(SecurityUtils.getUserId());
        oaContractSeal.setDeptId(SecurityUtils.getDeptId());
        oaContractSeal.setTenantId(SecurityUtils.getCurrComId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode(),SecurityUtils.getCurrComId());
        oaContractSeal.setFlowId(flowId);
        int result = oaContractSealMapper.insertOaContractSeal(oaContractSeal);
        if(result > 0){
            OaContractSeal contractSeal = oaContractSealMapper.selectOaContractSealById(oaContractSeal.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractSeal, AdminCodeEnum.CONTRACTSEAL_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaContractSealMapper.deleteOaContractSealById(contractSeal.getId());
                return 0;
            }else {
                contractSeal.setState(AdminCodeEnum.CONTRACTSEAL_STATUS_VERIFYING.getCode());
                oaContractSealMapper.updateOaContractSeal(contractSeal);
            }
        }
        return result;
    }

    /**
     * 提交外出申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaContractSeal oaContractSeal, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(oaContractSeal.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode(), AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(oaContractSeal.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());;
        OaContractSeal contractSeal = this.selectOaContractSealById(oaContractSeal.getId());
        SysUser sysUser = iSysUserService.selectUserById(contractSeal.getUserId());
        contractSeal.setUserName(sysUser.getNickName());
        contractSeal.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(contractSeal, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACTSEAL_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    @Override
    public int updateOaContractSeal(OaContractSeal oaContractSeal)
    {
        //原数据
        OaContractSeal contractSeal1 = oaContractSealMapper.selectOaContractSealById(oaContractSeal.getId());

        oaContractSeal.setUpdateTime(DateUtils.getNowDate());
        oaContractSeal.setUpdateBy(SecurityUtils.getUsername());
        int result = oaContractSealMapper.updateOaContractSeal(oaContractSeal);
        if(result > 0){
            OaContractSeal contractSeal = oaContractSealMapper.selectOaContractSealById(oaContractSeal.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", contractSeal.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractSeal, AdminCodeEnum.CONTRACTSEAL_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaContractSealMapper.updateOaContractSeal(contractSeal1);
                return 0;
            }else {
                contractSeal.setState(AdminCodeEnum.CONTRACTSEAL_STATUS_VERIFYING.getCode());
                oaContractSealMapper.updateOaContractSeal(contractSeal);
            }
        }
        return result;
    }

    /**
     * 批量删除oa合同用印
     *
     * @param ids 需要删除的oa合同用印ID
     * @return 结果
     */
    @Override
    public int deleteOaContractSealByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaContractSeal contractSeal = oaContractSealMapper.selectOaContractSealById(id);
                contractSeal.setFlag(1L);
                oaContractSealMapper.updateOaContractSeal(contractSeal);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除oa合同用印信息
     *
     * @param id oa合同用印ID
     * @return 结果
     */
    @Override
    public int deleteOaContractSealById(String id)
    {
        return oaContractSealMapper.deleteOaContractSealById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaContractSeal contractSeal = oaContractSealMapper.selectOaContractSealById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",contractSeal.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CONTRACTSEAL_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(contractSeal.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                contractSeal.setState(AdminCodeEnum.CONTRACTSEAL_STATUS_WITHDRAW.getCode());
                oaContractSealMapper.updateOaContractSeal(contractSeal);
                String userId = String.valueOf(contractSeal.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(contractSeal, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int updateProcessBusinessData(OaContractSealInstanceRelatedEditDTO oaContractSealInstanceRelatedEditDTO) {
        OaContractSeal oaContractSeal = oaContractSealInstanceRelatedEditDTO.getOaContractSeal();
        if (!ObjectUtils.isEmpty(oaContractSealInstanceRelatedEditDTO.getOaContractSeal())) {

            oaContractSeal.setUpdateTime(DateUtils.getNowDate());
            oaContractSeal.setUpdateBy(SecurityUtils.getUsername());
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(oaContractSealInstanceRelatedEditDTO.getProcessInstanceId());
            baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(oaContractSeal));
            try {
                baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
            } catch (Exception e) {
                return 0;
            }
            return 1;
        }
        return 0;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaContractSeal oaContractSeal, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaContractSeal.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"oa合同用印审批提醒",msgContent,"oa",oaContractSeal.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
