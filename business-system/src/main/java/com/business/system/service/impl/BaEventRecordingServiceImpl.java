package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.system.domain.BaEventRecording;
import com.business.system.mapper.BaEventRecordingMapper;
import com.business.system.service.IBaEventRecordingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 事件记录Service业务层处理
 *
 * @author single
 * @date 2023-07-05
 */
@Service
public class BaEventRecordingServiceImpl extends ServiceImpl<BaEventRecordingMapper, BaEventRecording> implements IBaEventRecordingService
{
    @Autowired
    private BaEventRecordingMapper baEventRecordingMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询事件记录
     *
     * @param id 事件记录ID
     * @return 事件记录
     */
    @Override
    public BaEventRecording selectBaEventRecordingById(String id)
    {
        return baEventRecordingMapper.selectBaEventRecordingById(id);
    }

    /**
     * 查询事件记录列表
     *
     * @param baEventRecording 事件记录
     * @return 事件记录
     */
    @Override
    public List<BaEventRecording> selectBaEventRecordingList(BaEventRecording baEventRecording)
    {
        return baEventRecordingMapper.selectBaEventRecordingList(baEventRecording);
    }

    /**
     * 新增事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    @Override
    public int insertBaEventRecording(BaEventRecording baEventRecording)
    {
        baEventRecording.setId(getRedisIncreID.getId());
        return baEventRecordingMapper.insertBaEventRecording(baEventRecording);
    }

    /**
     * 修改事件记录
     *
     * @param baEventRecording 事件记录
     * @return 结果
     */
    @Override
    public int updateBaEventRecording(BaEventRecording baEventRecording)
    {
        return baEventRecordingMapper.updateBaEventRecording(baEventRecording);
    }

    /**
     * 批量删除事件记录
     *
     * @param ids 需要删除的事件记录ID
     * @return 结果
     */
    @Override
    public int deleteBaEventRecordingByIds(String[] ids)
    {
        return baEventRecordingMapper.deleteBaEventRecordingByIds(ids);
    }

    /**
     * 删除事件记录信息
     *
     * @param id 事件记录ID
     * @return 结果
     */
    @Override
    public int deleteBaEventRecordingById(String id)
    {
        return baEventRecordingMapper.deleteBaEventRecordingById(id);
    }

}
