package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaContractSeal;
import com.business.system.domain.dto.BaCompanyInstanceRelatedEditDTO;
import com.business.system.domain.dto.OaContractSealInstanceRelatedEditDTO;

/**
 * oa合同用印Service接口
 *
 * @author single
 * @date 2024-06-06
 */
public interface IOaContractSealService
{
    /**
     * 查询oa合同用印
     *
     * @param id oa合同用印ID
     * @return oa合同用印
     */
    public OaContractSeal selectOaContractSealById(String id);

    /**
     * 查询oa合同用印列表
     *
     * @param oaContractSeal oa合同用印
     * @return oa合同用印集合
     */
    public List<OaContractSeal> selectOaContractSealList(OaContractSeal oaContractSeal);

    /**
     * 新增oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    public int insertOaContractSeal(OaContractSeal oaContractSeal);

    /**
     * 修改oa合同用印
     *
     * @param oaContractSeal oa合同用印
     * @return 结果
     */
    public int updateOaContractSeal(OaContractSeal oaContractSeal);

    /**
     * 批量删除oa合同用印
     *
     * @param ids 需要删除的oa合同用印ID
     * @return 结果
     */
    public int deleteOaContractSealByIds(String[] ids);

    /**
     * 删除oa合同用印信息
     *
     * @param id oa合同用印ID
     * @return 结果
     */
    public int deleteOaContractSealById(String id);


    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 审批办理保存编辑业务数据
     * @param oaContractSealInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(OaContractSealInstanceRelatedEditDTO oaContractSealInstanceRelatedEditDTO);


}
