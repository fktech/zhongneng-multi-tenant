package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaChargingStandardsMapper;
import com.business.system.domain.BaChargingStandards;
import com.business.system.service.IBaChargingStandardsService;

/**
 * 收费标准Service业务层处理
 *
 * @author single
 * @date 2023-06-02
 */
@Service
public class BaChargingStandardsServiceImpl extends ServiceImpl<BaChargingStandardsMapper, BaChargingStandards> implements IBaChargingStandardsService
{
    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询收费标准
     *
     * @param id 收费标准ID
     * @return 收费标准
     */
    @Override
    public BaChargingStandards selectBaChargingStandardsById(String id)
    {
        return baChargingStandardsMapper.selectBaChargingStandardsById(id);
    }

    /**
     * 查询收费标准列表
     *
     * @param baChargingStandards 收费标准
     * @return 收费标准
     */
    @Override
    public List<BaChargingStandards> selectBaChargingStandardsList(BaChargingStandards baChargingStandards)
    {
        return baChargingStandardsMapper.selectBaChargingStandardsList(baChargingStandards);
    }

    /**
     * 新增收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    @Override
    public int insertBaChargingStandards(BaChargingStandards baChargingStandards)
    {
        baChargingStandards.setId(getRedisIncreID.getId());
        baChargingStandards.setCreateTime(DateUtils.getNowDate());
        baChargingStandards.setCreateBy(SecurityUtils.getUsername());
        return baChargingStandardsMapper.insertBaChargingStandards(baChargingStandards);
    }

    /**
     * 修改收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    @Override
    public int updateBaChargingStandards(BaChargingStandards baChargingStandards)
    {
        baChargingStandards.setUpdateTime(DateUtils.getNowDate());
        baChargingStandards.setUpdateBy(SecurityUtils.getUsername());
        return baChargingStandardsMapper.updateBaChargingStandards(baChargingStandards);
    }

    /**
     * 批量删除收费标准
     *
     * @param ids 需要删除的收费标准ID
     * @return 结果
     */
    @Override
    public int deleteBaChargingStandardsByIds(String[] ids)
    {
        return baChargingStandardsMapper.deleteBaChargingStandardsByIds(ids);
    }

    /**
     * 删除收费标准信息
     *
     * @param id 收费标准ID
     * @return 结果
     */
    @Override
    public int deleteBaChargingStandardsById(String id)
    {
        return baChargingStandardsMapper.deleteBaChargingStandardsById(id);
    }

}
