package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaCar;

/**
 * 车辆信息Service接口
 *
 * @author single
 * @date 2023-12-01
 */
public interface IBaCarService
{
    /**
     * 查询车辆信息
     *
     * @param id 车辆信息ID
     * @return 车辆信息
     */
    public BaCar selectBaCarById(String id);

    /**
     * 查询车辆信息列表
     *
     * @param baCar 车辆信息
     * @return 车辆信息集合
     */
    public List<BaCar> selectBaCarList(BaCar baCar);

    /**
     * 新增车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    public int insertBaCar(BaCar baCar);

    /**
     * 修改车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    public int updateBaCar(BaCar baCar);

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的车辆信息ID
     * @return 结果
     */
    public int deleteBaCarByIds(String[] ids);

    /**
     * 删除车辆信息信息
     *
     * @param id 车辆信息ID
     * @return 结果
     */
    public int deleteBaCarById(String id);


}
