package com.business.system.service;

import com.business.system.domain.BaOnlineMent;

import java.util.List;

/**
 * 监控点在线状态Service接口
 *
 * @author single
 * @date 2023-07-06
 */
public interface IBaOnlineMentService
{
    /**
     * 查询监控点在线状态
     *
     * @param id 监控点在线状态ID
     * @return 监控点在线状态
     */
    public BaOnlineMent selectBaOnlineMentById(String id);

    /**
     * 查询监控点在线状态列表
     *
     * @param baOnlineMent 监控点在线状态
     * @return 监控点在线状态集合
     */
    public List<BaOnlineMent> selectBaOnlineMentList(BaOnlineMent baOnlineMent);

    /**
     * 新增监控点在线状态
     *
     * @param baOnlineMent 监控点在线状态
     * @return 结果
     */
    public int insertBaOnlineMent(BaOnlineMent baOnlineMent);

    /**
     * 修改监控点在线状态
     *
     * @param baOnlineMent 监控点在线状态
     * @return 结果
     */
    public int updateBaOnlineMent(BaOnlineMent baOnlineMent);

    /**
     * 批量删除监控点在线状态
     *
     * @param ids 需要删除的监控点在线状态ID
     * @return 结果
     */
    public int deleteBaOnlineMentByIds(String[] ids);

    /**
     * 删除监控点在线状态信息
     *
     * @param id 监控点在线状态ID
     * @return 结果
     */
    public int deleteBaOnlineMentById(String id);


}
