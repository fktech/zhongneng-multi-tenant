package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaAutomobileSettlement;

/**
 * 汽运结算Service接口
 *
 * @author single
 * @date 2023-02-17
 */
public interface IBaAutomobileSettlementService
{
    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    public BaAutomobileSettlement selectBaAutomobileSettlementById(String id);

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 汽运结算集合
     */
    public List<BaAutomobileSettlement> selectBaAutomobileSettlementList(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    public int insertBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    public int updateBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement);

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementByIds(String[] ids);

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementById(String id);

    /**
     * 昕科推送汽运结算（付款单）
     */
    int addAutomobileSettlement(JSONObject object);

    /**
     * 昕科推送汽运结算（付款单）
     */
    int selectSettlement(String paymentOrderNum);
}
