package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaBatch;

/**
 * 批次Service接口
 *
 * @author single
 * @date 2023-01-05
 */
public interface IBaBatchService
{
    /**
     * 查询批次
     *
     * @param id 批次ID
     * @return 批次
     */
    public BaBatch selectBaBatchById(String id);

    /**
     * 查询批次列表
     *
     * @param baBatch 批次
     * @return 批次集合
     */
    public List<BaBatch> selectBaBatchList(BaBatch baBatch);

    /**
     * 新增批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    public int insertBaBatch(BaBatch baBatch);

    /**
     * 修改批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    public int updateBaBatch(BaBatch baBatch);

    /**
     * 批量删除批次
     *
     * @param ids 需要删除的批次ID
     * @return 结果
     */
    public int deleteBaBatchByIds(String[] ids);

    /**
     * 删除批次信息
     *
     * @param id 批次ID
     * @return 结果
     */
    public int deleteBaBatchById(String id);

    /**
     * 校验批次号是否是停用状态
     * @param batch
     * @return
     */
    public int selectBaBatch(String batch);


}
