package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaPoundRoomService;

/**
 * 磅房数据Service业务层处理
 *
 * @author ljb
 * @date 2023-09-04
 */
@Service
public class BaPoundRoomServiceImpl extends ServiceImpl<BaPoundRoomMapper, BaPoundRoom> implements IBaPoundRoomService
{
    @Autowired
    private BaPoundRoomMapper baPoundRoomMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaPurchaseOrderMapper baPurchaseOrderMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;


    /**
     * 查询磅房数据
     *
     * @param id 磅房数据ID
     * @return 磅房数据
     */
    @Override
    public BaPoundRoom selectBaPoundRoomById(String id)
    {
        BaPoundRoom poundRoom = baPoundRoomMapper.selectBaPoundRoomById(id);
        if(StringUtils.isNotEmpty(poundRoom.getGoodsId())){
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(poundRoom.getGoodsId());
            if(StringUtils.isNotNull(baGoods)){
                poundRoom.setGoodsName(baGoods.getName());
            }
        }
        //项目类型
        if(StringUtils.isNotEmpty(poundRoom.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(poundRoom.getProjectId());
            poundRoom.setProjectType(baProject.getProjectType());
        }
        //供应商信息
        if(StringUtils.isNotEmpty(poundRoom.getSupplierId())){
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(poundRoom.getSupplierId());
            poundRoom.setSupplierName(baSupplier.getName());
        }
        //采购计划信息
        if(StringUtils.isNotEmpty(poundRoom.getPurchaseId())){
            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(poundRoom.getPurchaseId());
            poundRoom.setProcurementVolume(baPurchaseOrder.getProcurementVolume());
            poundRoom.setIncludingPrice(baPurchaseOrder.getIncludingPrice());
            poundRoom.setExcludingPrice(baPurchaseOrder.getExcludingPrice());
            //查看采购计划
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
            poundRoom.setPlanName(baProcurementPlan.getPlanName());
            poundRoom.setPlanTime(baProcurementPlan.getPlanTime());
        }
        //订单信息
        if(StringUtils.isNotEmpty(poundRoom.getOrderId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(poundRoom.getOrderId());
            poundRoom.setOrderName(baContractStart.getName());
        }
        //运输信息
        if(StringUtils.isNotEmpty(poundRoom.getTransportId())){
            BaTransport baTransport = baTransportMapper.selectBaTransportById(poundRoom.getTransportId());
            poundRoom.setPlanTime(baTransport.getTransportDate());
            poundRoom.setProcurementVolume(baTransport.getSaleNum());
            //运输计划
            BaBidTransport baBidTransport = baBidTransportMapper.selectBaBidTransportById(baTransport.getRelationId());
            //承运单位
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baBidTransport.getTransportCompany());
            if(StringUtils.isNotNull(baCompany)){
                poundRoom.setPlanName(baBidTransport.getOrigin()+"-"+baBidTransport.getDestination()+"-"+baCompany.getCompanyAbbreviation());
            }else {
                poundRoom.setPlanName(baBidTransport.getOrigin()+"-"+baBidTransport.getDestination());
            }
        }
        //合同启动信息
        if(StringUtils.isNotEmpty(poundRoom.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(poundRoom.getStartId());
            poundRoom.setIncludingPrice(baContractStart.getPrice());
        }
        return poundRoom;
    }

    /**
     * 查询磅房数据列表
     *
     * @param baPoundRoom 磅房数据
     * @return 磅房数据
     */
    @Override
    public List<BaPoundRoom> selectBaPoundRoomList(BaPoundRoom baPoundRoom)
    {
        List<BaPoundRoom> poundRoomList = baPoundRoomMapper.selectBaPoundRoomList(baPoundRoom);
        for (BaPoundRoom poundRoom:poundRoomList) {
            if(StringUtils.isNotEmpty(poundRoom.getGoodsId())){
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(poundRoom.getGoodsId());
                if(StringUtils.isNotNull(baGoods)){
                    poundRoom.setGoodsName(baGoods.getName());
                }
            }
            //采购计划信息
            if(StringUtils.isNotEmpty(poundRoom.getPurchaseId())){
                BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(poundRoom.getPurchaseId());
                poundRoom.setProcurementVolume(baPurchaseOrder.getProcurementVolume());
                poundRoom.setIncludingPrice(baPurchaseOrder.getIncludingPrice());
                poundRoom.setExcludingPrice(baPurchaseOrder.getExcludingPrice());
                //查看采购计划
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                poundRoom.setPlanName(baProcurementPlan.getPlanName());
                poundRoom.setPlanTime(baProcurementPlan.getPlanTime());
            }
            //订单信息
            if(StringUtils.isNotEmpty(poundRoom.getOrderId())){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(poundRoom.getOrderId());
                poundRoom.setOrderName(baContractStart.getName());
            }
            //运输信息
            if(StringUtils.isNotEmpty(poundRoom.getTransportId())){
                BaTransport baTransport = baTransportMapper.selectBaTransportById(poundRoom.getTransportId());
                poundRoom.setPlanTime(baTransport.getTransportDate());
                poundRoom.setProcurementVolume(baTransport.getSaleNum());
                //运输计划
                BaBidTransport baBidTransport = baBidTransportMapper.selectBaBidTransportById(baTransport.getRelationId());
                //承运单位
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baBidTransport.getTransportCompany());
                if(StringUtils.isNotNull(baCompany)){
                    poundRoom.setPlanName(baBidTransport.getOrigin()+"-"+baBidTransport.getDestination()+"-"+baCompany.getCompanyAbbreviation());
                }else {
                    poundRoom.setPlanName(baBidTransport.getOrigin()+"-"+baBidTransport.getDestination());
                }

            }
            //合同启动信息
            if(StringUtils.isNotEmpty(poundRoom.getStartId())){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(poundRoom.getStartId());
                poundRoom.setIncludingPrice(baContractStart.getPrice());
            }
        }
        return poundRoomList;
    }

    /**
     * 新增磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    @Override
    public int insertBaPoundRoom(BaPoundRoom baPoundRoom)
    {
        if(StringUtils.isNotEmpty(baPoundRoom.getWaybillCode())){
            QueryWrapper<BaPoundRoom> roomQueryWrapper = new QueryWrapper<>();
            roomQueryWrapper.eq("waybill_code",baPoundRoom.getWaybillCode());
            BaPoundRoom baPoundRoom1 = baPoundRoomMapper.selectOne(roomQueryWrapper);
            if(StringUtils.isNotNull(baPoundRoom1)){
                return 0;
            }
        }
        baPoundRoom.setId(getRedisIncreID.getId());
        baPoundRoom.setCreateTime(DateUtils.getNowDate());
        baPoundRoom.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baPoundRoom.setTenantId(SecurityUtils.getCurrComId());
        return baPoundRoomMapper.insertBaPoundRoom(baPoundRoom);
    }

    @Override
    public String importDepotHead(List<BaPoundRoom> poundRoomList) {
        if (StringUtils.isNull(poundRoomList) || poundRoomList.size() == 0) {
            return "导入数据不能为空！";
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        //返回错误数据
        for (BaPoundRoom baPoundRoom:poundRoomList) {
          /*  if(StringUtils.isNotEmpty(baPoundRoom.getProjectName())){*/
                //查看项目是否存在
                /*QueryWrapper<BaProject> projectQueryWrapper = new QueryWrapper<>();
                projectQueryWrapper.eq("name",baPoundRoom.getProjectName());
                projectQueryWrapper.eq("flag",0);
                projectQueryWrapper.eq("state", AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
                BaProject baProject = baProjectMapper.selectOne(projectQueryWrapper);
                //项目名称为后台已创建审核通过的项目
                if(StringUtils.isNull(baProject)){
                    failureMsg.append("<br/>" + baPoundRoom.getSerial() + "此序号对应项目名称 " + baPoundRoom.getProjectName() + " 不存在");
                    continue;
                }*/
                //运单编号、车牌号、司机姓名、司机手机号、品名为必填项；
                if(StringUtils.isEmpty(baPoundRoom.getWaybillCode()) || StringUtils.isEmpty(baPoundRoom.getGoodsName())){
                    failureNum++;
                    failureMsg.append("<br/>"+ "序号为" + baPoundRoom.getSerial() + "的数据:运单编号、品名 " + "存在空值");
                    continue;
                }
                if(StringUtils.isNotEmpty(baPoundRoom.getGoodsName())){
                    QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("name",baPoundRoom.getGoodsName());
                    BaGoods baGoods = baGoodsMapper.selectOne(queryWrapper);
                    if(StringUtils.isNull(baGoods)){
                        failureNum++;
                        failureMsg.append("<br/>"+ "序号为"  + baPoundRoom.getSerial() + "的数据:商品名称 " + "不存在");
                        continue;
                    }
                    baPoundRoom.setGoodsId(baGoods.getId());
                }
                if(StringUtils.isNotEmpty(baPoundRoom.getWaybillCode())){
                    QueryWrapper<BaPoundRoom> roomQueryWrapper = new QueryWrapper<>();
                    roomQueryWrapper.eq("waybill_code",baPoundRoom.getWaybillCode());
                    BaPoundRoom baPoundRoom1 = baPoundRoomMapper.selectOne(roomQueryWrapper);
                    if(StringUtils.isNotNull(baPoundRoom1)){
                       /* baPoundRoom1.setPlateNumber(baPoundRoom.getPlateNumber());
                        baPoundRoom1.setDriverUserName(baPoundRoom.getDriverUserName());
                        baPoundRoom1.setDriverMobile(baPoundRoom.getDriverMobile());
                        baPoundRoom1.setDeliveryTime(baPoundRoom.getDeliveryTime());
                        baPoundRoom1.setLoadingWeight(baPoundRoom.getLoadingWeight());
                        baPoundRoom1.setUnloadWeight(baPoundRoom.getUnloadWeight());
                        baPoundRoom1.setReceivingTime(baPoundRoom.getReceivingTime());
                        baPoundRoom1.setGoodsName(baPoundRoom.getGoodsName());
                        baPoundRoom1.setWaybillCode(baPoundRoom.getWaybillCode());
                        baPoundRoom1.setGoodsId(baPoundRoom.getGoodsId());
                        this.updateBaPoundRoom(baPoundRoom1);*/
                        failureNum++;
                        failureMsg.append("<br/>"+ "序号为"  + baPoundRoom.getSerial() + "的运单编号 " + "已存在");
                        continue;
                    }
                }
                this.insertBaPoundRoom(baPoundRoom);
        }
        if(StringUtils.isNotEmpty(failureMsg.toString())){
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        return successMsg.toString();
    }

    /**
     * 修改磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    @Override
    public int updateBaPoundRoom(BaPoundRoom baPoundRoom)
    {
        baPoundRoom.setUpdateTime(DateUtils.getNowDate());
        return baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);
    }

    /**
     * 批量删除磅房数据
     *
     * @param ids 需要删除的磅房数据ID
     * @return 结果
     */
    @Override
    public int deleteBaPoundRoomByIds(String[] ids)
    {
        return baPoundRoomMapper.deleteBaPoundRoomByIds(ids);
    }

    /**
     * 删除磅房数据信息
     *
     * @param id 磅房数据ID
     * @return 结果
     */
    @Override
    public int deleteBaPoundRoomById(String id)
    {
        return baPoundRoomMapper.deleteBaPoundRoomById(id);
    }

    @Override
    public int association(BaPoundRoom baPoundRoom) {
        if(StringUtils.isNotEmpty(baPoundRoom.getProjectId())){
            //判断立项类型
            BaProject baProject = baProjectMapper.selectBaProjectById(baPoundRoom.getProjectId());
            if(baProject.getProjectType().equals("4")){
                baPoundRoom.setTransportWay("2");
            }else {
                if(StringUtils.isNotEmpty(baPoundRoom.getPurchaseId())){
                    BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baPoundRoom.getPurchaseId());
                    //查看采购计划
                    BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                    baPoundRoom.setTransportWay(baProcurementPlan.getTransportWay());
                }
            }
        }
        return baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);
    }

}
