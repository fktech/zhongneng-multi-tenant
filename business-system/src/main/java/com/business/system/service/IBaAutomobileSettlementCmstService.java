package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaAutomobileSettlementCmst;

/**
 * 汽运结算Service接口
 *
 * @author single
 * @date 2023-08-21
 */
public interface IBaAutomobileSettlementCmstService
{
    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    public BaAutomobileSettlementCmst selectBaAutomobileSettlementCmstById(String id);

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 汽运结算集合
     */
    public List<BaAutomobileSettlementCmst> selectBaAutomobileSettlementCmstList(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    public int insertBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    public int updateBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst);

    /**
     * 中储货主结算通知
     */
    int addSettlement(JSONObject object);

    /**
     * 中储承运方结算通知
     */
    int editSettlement(JSONObject object);

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementCmstByIds(String[] ids);

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementCmstById(String id);


}
