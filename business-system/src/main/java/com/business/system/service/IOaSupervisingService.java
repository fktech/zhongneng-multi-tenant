package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaSupervising;
import com.business.system.domain.vo.OaSupervisingReceiveVO;
import com.business.system.domain.vo.OaSupervisingStatVO;

/**
 * 督办Service接口
 *
 * @author ljb
 * @date 2023-11-20
 */
public interface IOaSupervisingService
{
    /**
     * 查询督办
     *
     * @param id 督办ID
     * @return 督办
     */
    public OaSupervising selectOaSupervisingById(String id);

    /**
     * 查询督办列表
     *
     * @param oaSupervising 督办
     * @return 督办集合
     */
    public List<OaSupervising> selectOaSupervisingList(OaSupervising oaSupervising);

    /**
     * 新增督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    public int insertOaSupervising(OaSupervising oaSupervising);

    /**
     * 修改督办
     *
     * @param oaSupervising 督办
     * @return 结果
     */
    public int updateOaSupervising(OaSupervising oaSupervising);

    /**
     * 批量删除督办
     *
     * @param ids 需要删除的督办ID
     * @return 结果
     */
    public int deleteOaSupervisingByIds(String[] ids);

    /**
     * 删除督办信息
     *
     * @param id 督办ID
     * @return 结果
     */
    public int deleteOaSupervisingById(String id);

    /**
     * 回告
     * @param oaSupervising
     * @return
     */
    public int reply(OaSupervising oaSupervising);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 批复按钮
     * @param oaSupervising
     * @return
     */
    public int approval(OaSupervising oaSupervising);

    /**
     * 办结按钮
     * @param oaSupervising
     * @return
     */
    public int completion(OaSupervising oaSupervising);


    /**
     * 督办事项统计
     */
    public OaSupervisingStatVO oaSupervisingStat(OaSupervising oaSupervising);

    /**
     * 人员督办接收处理统计
     */
    public List<OaSupervisingReceiveVO> userOaSupervisingStat(OaSupervising oaSupervising);
}
