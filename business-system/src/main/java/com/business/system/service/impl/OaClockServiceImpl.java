package com.business.system.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.*;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.OaCalendar;
import com.business.system.domain.OaClockRecord;
import com.business.system.domain.OaLeave;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.dto.UserClockDTO;
import com.business.system.mapper.*;
import com.business.system.service.IOaLeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.domain.OaClock;
import com.business.system.service.IOaClockService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 打卡Service业务层处理
 *
 * @author single
 * @date 2023-11-27
 */
@Service
public class OaClockServiceImpl extends ServiceImpl<OaClockMapper, OaClock> implements IOaClockService
{
    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaClockRecordMapper oaClockRecordMapper;

    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaCalendarMapper oaCalendarMapper;

    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    @Override
    public OaClock selectOaClockById(String id)
    {
        return oaClockMapper.selectOaClockById(id);
    }

    /**
     * 查询打卡列表
     *
     * @param oaClock 打卡
     * @return 打卡
     */
    @Override
    public List<OaClock> selectOaClockList(OaClock oaClock)
    {
        return oaClockMapper.selectOaClockList(oaClock);
    }

    /**
     * 新增打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    @Override
    public UR insertOaClock(OaClock oaClock)
    {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(oaClock.getClockDate());
        //打卡状态
        String clockState = "";
        if("1".equals(oaClock.getType())){


            OaClock clock = new OaClock();
            clock.setQueryTime(queryTime);
            clock.setEmployeeId(oaClock.getEmployeeId());
            clock.setClockType(oaClock.getClockType());
            clock.setFlag(1L);
            //查询对应缓存数据
            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
            if(oaClocks.size() > 0){
                OaClock clock1 = oaClocks.get(0);
                oaClock.setId(clock1.getId());
                oaClock.setAttendanceType(clock1.getAttendanceType());
                oaClock.setWorkHours(clock1.getWorkHours());
                oaClock.setInitialTime(clock1.getInitialTime());
                oaClock.setAttendanceStart(clock1.getAttendanceStart());
                oaClock.setAllowedLate(clock1.getAllowedLate());
                oaClock.setSeverelyLate(clock1.getSeverelyLate());
                oaClock.setAbsenteeismLate(clock1.getAbsenteeismLate());
                oaClock.setAbsenteeismEarly(clock1.getAbsenteeismEarly());
                BeanUtils.copyBeanProp(clock1, oaClock);
                clock1.setFlag(0L);
                //判断是否是休息日
                if(StringUtils.isNotEmpty(clock.getHoliday()) && "0".equals(clock.getHoliday())){
                    oaClockMapper.updateOaClock(clock1);
                }else {
                    if("1".equals(clock1.getAttendanceType())){
                        if("1".equals(oaClock.getClockType())){
                            //判断是否迟到
                            if(clock1.getClockTime().compareTo(clock1.getInitialTime()) > 0){
                                //计算上班时间差
                                LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),clock1.getInitialTime().getMinutes());
                                LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                                Duration between = Duration.between(startTime, endTime);

                                long l = between.toMinutes();
                                //判断是否上班迟到(迟到分钟数不等于空其他都为空)
                                if(clock1.getAllowedLate() > 0 && clock1.getSeverelyLate() == 0 && clock1.getAbsenteeismLate() == 0){
                                    //时间差小于允许迟到分钟数打卡正常
                                    if(between.toMinutes() <= clock1.getAllowedLate()){
                                        clock1.setClockState("1");
                                        clockState = "1";
                                    }else {
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("2");
                                        clockState = "2";
                                    }
                                }else {
                                    //迟到时间
                                    clock1.setDuration(l);
                                    clock1.setClockState("2");
                                    clockState = "2";
                                }
                                //允许旷工为领
                                if(clock1.getAllowedLate() > 0 && clock1.getSeverelyLate() > 0 && clock1.getAbsenteeismLate() == 0){
                                    //时间差小于允许迟到分钟数打卡正常
                                    if(between.toMinutes() <= clock1.getAllowedLate()){
                                        clock1.setClockState("1");
                                        clockState = "1";
                                    }else if(clock1.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock1.getSeverelyLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("2");
                                        clockState = "2";
                                    }else if(between.toMinutes() >= clock1.getSeverelyLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("6");
                                        clockState = "6";
                                    }
                                }
                                //迟到旷工
                                if(clock1.getAllowedLate() > 0 && clock1.getSeverelyLate() > 0 && clock1.getAbsenteeismLate() > 0){
                                    //时间差小于允许迟到分钟数打卡正常
                                    if(between.toMinutes() <= clock1.getAllowedLate()){
                                        clock1.setClockState("1");
                                        clockState = "1";
                                    }else if(clock1.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock1.getSeverelyLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("2");
                                        clockState = "2";
                                    }else if(between.toMinutes() >= clock1.getSeverelyLate() && between.toMinutes() < clock1.getAbsenteeismLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("6");
                                        clockState = "6";
                                    }else if(between.toMinutes() >= clock1.getAbsenteeismLate()){
                                        clock1.setClockState("7");
                                        clockState = "7";
                                    }
                                }
                                //允许迟到分钟数为零
                                if(clock1.getAllowedLate() == 0 && clock1.getSeverelyLate() > 0 && clock1.getAbsenteeismLate() > 0){
                                    if(clock1.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock1.getSeverelyLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("2");
                                        clockState = "2";
                                    }else if(between.toMinutes() >= clock1.getSeverelyLate() && between.toMinutes() < clock1.getAbsenteeismLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("6");
                                        clockState = "6";
                                    }else if(between.toMinutes() >= clock1.getAbsenteeismLate()){
                                        clock1.setClockState("7");
                                        clockState = "7";
                                    }else {
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("2");
                                        clockState = "2";
                                    }
                                }
                                //严重迟到分钟数为零
                                if(clock1.getAllowedLate() > 0 && clock1.getSeverelyLate() == 0 && clock1.getAbsenteeismLate() > 0){
                                    //时间差小于允许迟到分钟数打卡正常
                                    if(between.toMinutes() < clock1.getAllowedLate()){
                                        clock1.setClockState("1");
                                        clockState = "1";
                                    }else if(between.toMinutes() > clock1.getSeverelyLate() && between.toMinutes() < clock1.getAbsenteeismLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("6");
                                        clockState = "6";
                                    }else if(between.toMinutes() > clock1.getAbsenteeismLate()){
                                        clock1.setClockState("7");
                                        clockState = "7";
                                    }
                                }
                                //只存在严重迟到
                                if(clock1.getAllowedLate() == 0 && clock1.getSeverelyLate() > 0 && clock1.getAbsenteeismLate() == 0){
                                    if(between.toMinutes() >= clock1.getSeverelyLate()){
                                        //迟到时间
                                        clock1.setDuration(l);
                                        clock1.setClockState("6");
                                        clockState = "6";
                                    }
                                }
                                //只存在迟到旷工
                                if(clock1.getAllowedLate() == 0 && clock1.getSeverelyLate() == 0 && clock1.getAbsenteeismLate() > 0){
                                    if(between.toMinutes() >= clock1.getSeverelyLate()){
                                        clock1.setClockState("7");
                                        clockState = "7";
                                    }
                                }
                               /* clock1.setClockState("2");
                                clockState = "2";*/
                            }else {
                                clock1.setClockState("1");
                                clockState = "1";
                            }
                        }else if("2".equals(oaClock.getClockType())){
                            //下班是否早退
                            if(clock1.getClockTime().compareTo(clock1.getInitialTime()) < 0){
                                //计算上班时间差
                                LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),clock1.getInitialTime().getMinutes());
                                LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                                Duration between = Duration.between(endTime, startTime);
                                //早退旷工
                                if(clock1.getAbsenteeismEarly() > 0){
                                    if(between.toMinutes() >= clock1.getAbsenteeismEarly()){
                                        clock1.setClockState("8");
                                        clockState = "8";
                                    }else {
                                        //早退时间
                                        clock1.setDuration(between.toMinutes());
                                        clock1.setClockState("3");
                                        clockState = "3";
                                    }
                                }else {
                                    //早退时间
                                    clock1.setDuration(between.toMinutes());
                                    clock1.setClockState("3");
                                    clockState = "3";
                                }

                            }else {
                                clock1.setClockState("1");
                                clockState = "1";
                            }
                        }

                    }else if("2".equals(clock1.getAttendanceType())){
                        //弹性工作制下班
                        if("2".equals(clock1.getClockType())){
                            SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
                            try {
                                Date parse = time.parse(clock1.getOfficeHours());
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(parse);
                                calendar.add(Calendar.HOUR_OF_DAY,clock1.getWorkHours().intValue());
                                //计算上班时间差
                                LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),calendar.getTime().getMinutes());
                                LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                                Duration between = Duration.between(endTime, startTime);
                                if(clock1.getClockTime().compareTo(calendar.getTime()) < 0){
                                    //早退时间
                                    clock1.setDuration(between.toMinutes());
                                    clock1.setClockState("3");
                                    clockState = "3";
                                }else {
                                    clock1.setClockState("1");
                                    clockState = "1";
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }else {
                            clock1.setClockState("1");
                            clockState = "1";
                        }
                    }
                    oaClockMapper.updateOaClock(clock1);
                }
            }else if("2".equals(oaClock.getClockType())){
                //查询下班打卡是否存在
                clock.setFlag(0L);
                List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                OaClock clock1 = oaClockList.get(0);
                oaClock.setId(clock1.getId());
                oaClock.setAttendanceType(clock1.getAttendanceType());
                oaClock.setWorkHours(clock1.getWorkHours());
                oaClock.setAttendanceStart(clock1.getAttendanceStart());
                oaClock.setInitialTime(clock1.getInitialTime());
                oaClock.setAllowedLate(clock1.getAllowedLate());
                oaClock.setSeverelyLate(clock1.getSeverelyLate());
                oaClock.setAbsenteeismLate(clock1.getAbsenteeismLate());
                oaClock.setAbsenteeismEarly(clock1.getAbsenteeismEarly());
                BeanUtils.copyBeanProp(clock1, oaClock);
                clock1.setFlag(0L);
                //判断是否休假打卡
                if(StringUtils.isNotEmpty(oaClock.getHoliday()) && "0".equals(oaClock.getHoliday())){
                    //下班重复打卡走编辑
                    oaClockMapper.updateOaClock(clock1);
                }else {
                    if("1".equals(clock1.getAttendanceType())){
                        //判断是否早退
                        if(clock1.getClockTime().compareTo(clock1.getInitialTime()) < 0){
                            //计算下班打卡时间（早退时长）
                            LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),clock1.getInitialTime().getMinutes());
                            LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                            Duration between = Duration.between(endTime, startTime);
                            long l = between.toMinutes();
                            //早退旷工
                            if(clock1.getAbsenteeismEarly() > 0){
                                if(between.toMinutes() >= oaClock.getAbsenteeismEarly()){
                                    clock1.setClockState("8");
                                    clockState = "8";
                                }else {
                                    //早退时间
                                    clock1.setDuration(l);
                                    clock1.setClockState("3");
                                    clockState = "3";
                                }
                            }else {
                                //早退时间
                                clock1.setDuration(l);
                                clock1.setClockState("3");
                                clockState = "3";
                            }
                        }else {
                            clock1.setClockState("1");
                            clockState = "1";
                        }
                    }else if("2".equals(clock1.getAttendanceType())){
                        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
                        try {
                            Date parse = time.parse(clock1.getOfficeHours());
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(parse);
                            calendar.add(Calendar.HOUR_OF_DAY,clock1.getWorkHours().intValue());
                            //计算下班打卡时间（早退时长）
                            LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),calendar.getTime().getMinutes());
                            LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                            Duration between = Duration.between(endTime, startTime);
                            long l = between.toMinutes();
                            //判断打卡时间是否在下班打卡之前
                            if(clock1.getClockTime().compareTo(calendar.getTime()) < 0){
                                //早退时间
                                clock1.setDuration(l);
                                clock1.setClockState("3");
                                clockState = "3";
                            }else {
                                clock1.setClockState("1");
                                //考勤状态
                                clockState = "1";
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    //下班重复打卡走编辑
                    oaClockMapper.updateOaClock(clock1);
                }
            }
            //打卡记录表新增数据
            OaClockRecord oaClockRecord = new OaClockRecord();
            BeanUtils.copyBeanProp(oaClockRecord, oaClock);
            oaClockRecord.setId(getRedisIncreID.getId());
            oaClockRecord.setRelevanceId(oaClock.getId());
            oaClock.setCreateTime(DateUtils.getNowDate());
            if(StringUtils.isNotEmpty(clockState) && !"".equals(clockState)){
                oaClockRecord.setClockState(clockState);
            }else {
                oaClockRecord.setClockState("5");
            }
            //租户ID
            oaClockRecord.setTenantId(SecurityUtils.getCurrComId());
            oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
        }else if("2".equals(oaClock.getType())){
            // 创建 Calendar 对象
            Calendar calendar = Calendar.getInstance();
            // 设置小时为12（0-23）
            //calendar.set(Calendar.HOUR_OF_DAY, 11);
            // 格式化日期输出
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentTime = sdf.format(calendar.getTime());
            System.out.println("当前时间：" + currentTime);

            oaClock.setId(getRedisIncreID.getId());
            oaClock.setCreateTime(DateUtils.getNowDate());
            OaClock clock = new OaClock();
            clock.setQueryTime(queryTime);
            clock.setEmployeeId(oaClock.getEmployeeId());
            clock.setType(oaClock.getType());
            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
            //判断内勤打卡数据
            clock = new OaClock();
            clock.setQueryTime(queryTime);
            clock.setEmployeeId(oaClock.getEmployeeId());
            clock.setType("1");
            if(oaClockList.size() == 0){
                //首次外勤打卡时间不大于17点编辑上班打卡否则修改下班打卡
                if (currentTime.compareTo("17:00:00") < 0) {
                    //第一次外勤卡
                    clock.setClockType("1");
                    clock.setFlag(1L);
                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                    OaClock oaClock1 = oaClocks.get(0);
                    if (oaClock1.getClockTime() == null) {
                        oaClock1.setClockState("12");
                        oaClock1.setLongitude(oaClock.getLongitude());
                        oaClock1.setLatitude(oaClock.getLatitude());
                        oaClock1.setClockDate(oaClock.getClockDate());
                        oaClock1.setAddress(oaClock.getAddress());
                        oaClock1.setClockTime(oaClock.getClockTime());
                        oaClockMapper.updateOaClock(oaClock1);
                    }
                }else {
                    clock.setClockType("2");
                    clock.setFlag(1L);
                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                    OaClock oaClock1 = oaClocks.get(0);
                    if(oaClock1.getClockTime() == null){
                        oaClock1.setClockState("12");
                        oaClock1.setLongitude(oaClock.getLongitude());
                        oaClock1.setLatitude(oaClock.getLatitude());
                        oaClock1.setClockDate(oaClock.getClockDate());
                        oaClock1.setAddress(oaClock.getAddress());
                        oaClock1.setClockTime(oaClock.getClockTime());
                        oaClockMapper.updateOaClock(oaClock1);
                    }
                }
            }else {
                //外勤打卡时间大于17点编辑下班打卡
                if (currentTime.compareTo("17:00:00") > 0) {
                    clock.setClockType("2");
                    clock.setFlag(1L);
                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                    OaClock oaClock1 = oaClocks.get(0);
                    if (oaClock1.getClockTime() == null) {
                        oaClock1.setClockState("12");
                        oaClock1.setLongitude(oaClock.getLongitude());
                        oaClock1.setLatitude(oaClock.getLatitude());
                        oaClock1.setClockDate(oaClock.getClockDate());
                        oaClock1.setAddress(oaClock.getAddress());
                        oaClock1.setClockTime(oaClock.getClockTime());
                        oaClockMapper.updateOaClock(oaClock1);
                    }
                }
            }
            //租户ID
            oaClock.setTenantId(SecurityUtils.getCurrComId());
            oaClockMapper.insertOaClock(oaClock);
            //打卡记录表新增数据
            OaClockRecord oaClockRecord = new OaClockRecord();
            BeanUtils.copyBeanProp(oaClockRecord, oaClock);
            oaClockRecord.setId(getRedisIncreID.getId());
            oaClockRecord.setRelevanceId(oaClock.getId());
            oaClockRecord.setClockState("12");
            //租户ID
            oaClockRecord.setTenantId(SecurityUtils.getCurrComId());
            oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
        }
        return UR.ok().code(200).data("考勤状态",clockState);
    }

    /**
     * 修改打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    @Override
    public int updateOaClock(OaClock oaClock)
    {
        oaClock.setUpdateTime(DateUtils.getNowDate());
        //打卡记录表新增数据
        OaClockRecord oaClockRecord = new OaClockRecord();
        BeanUtils.copyBeanProp(oaClockRecord, oaClock);
        oaClockRecord.setId(getRedisIncreID.getId());
        oaClockRecord.setRelevanceId(oaClock.getId());
        oaClockRecordMapper.insertOaClockRecord(oaClockRecord);
        return oaClockMapper.updateOaClock(oaClock);
    }

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的打卡ID
     * @return 结果
     */
    @Override
    public int deleteOaClockByIds(String[] ids)
    {
        return oaClockMapper.deleteOaClockByIds(ids);
    }

    /**
     * 删除打卡信息
     *
     * @param id 打卡ID
     * @return 结果
     */
    @Override
    public int deleteOaClockById(String id)
    {
        return oaClockMapper.deleteOaClockById(id);
    }

    /**
     * APP打卡统计
     */
    @Override
    public Map statClock(OaClockDTO oaClockDTO) {
        Map<String, List<String>> statClockMap = new HashMap<String, List<String>>();
        oaClockDTO.setType("1"); //内勤
        List<OaClock> oaClocks = oaClockMapper.statOaClockList(oaClockDTO);
        List<String> normalList = new ArrayList<>(); //正常考勤日期
        List<String> abnormalList = new ArrayList<>(); //异常考勤日期
        List<String> leaveList = new ArrayList<>(); //请假考勤日期
        String date = "";
        //当前登录人每天打卡数据封装
        Map<String, List<OaClock>> statEmployeeMap = new LinkedHashMap<String, List<OaClock>>();
        List<OaClock> employeeOaClockList = null;
        if(!CollectionUtils.isEmpty(oaClocks)){
            for(OaClock oaClock : oaClocks){
                date = DateUtils.dateTime(oaClock.getCreateTime());
                if(statEmployeeMap.containsKey(date)){
                    employeeOaClockList = statEmployeeMap.get(date);
                    employeeOaClockList.add(oaClock);
                    statEmployeeMap.put(date, employeeOaClockList);
                } else {
                    employeeOaClockList = new ArrayList<OaClock>();
                    employeeOaClockList.add(oaClock);
                    statEmployeeMap.put(date, employeeOaClockList);
                }
            }
            if(!CollectionUtils.isEmpty(statEmployeeMap)){
                for (Map.Entry<String, List<OaClock>> entry : statEmployeeMap.entrySet()) {
                    List<OaClock> clockList = entry.getValue();
                    if(!CollectionUtils.isEmpty(clockList)){
                        date = entry.getKey();
                        if(clockList.size() >= 2){
                            OaClock oaClock0 = clockList.get(0);
                            OaClock oaClock1 = clockList.get(1);
                            if(!ObjectUtils.isEmpty(oaClock0) && !ObjectUtils.isEmpty(oaClock1)){
                                //上午请假
                                OaLeave oaLeave = new OaLeave();
                                oaLeave.setUserId(oaClockDTO.getEmployeeId());
                                oaLeave.setSubmitDate(DateUtils.dateTime(oaClock0.getCreateTime()));
                                oaLeave.setState("oaLeave:pass");
                                oaLeave.setFlag(new Long(1));
                                List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeaveList(oaLeave);
                                if(!CollectionUtils.isEmpty(oaLeaves)){
                                    leaveList.add(date);
                                }
                                //下午请假
                                oaLeave = new OaLeave();
                                oaLeave.setUserId(oaClockDTO.getEmployeeId());
                                oaLeave.setSubmitDate(DateUtils.dateTime(oaClock1.getCreateTime()));
                                oaLeave.setState("oaLeave:pass");
                                oaLeave.setFlag(new Long(1));
                                oaLeaves = oaLeaveMapper.selectOaLeaveList(oaLeave);
                                if(!CollectionUtils.isEmpty(oaLeaves) && !leaveList.contains(date)){
                                    leaveList.add(date);
                                }

                                if((("1".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState())) || ("12".equals(oaClock0.getClockState()) && "12".equals(oaClock1.getClockState()))
                                        || ("5".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState())) || ("9".equals(oaClock0.getClockState()) && "9".equals(oaClock1.getClockState()))
                                        || ("10".equals(oaClock0.getClockState()) && "10".equals(oaClock1.getClockState()))
                                        || ("1".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState())) || ("1".equals(oaClock0.getClockState()) && "12".equals(oaClock1.getClockState()))
                                        || ("1".equals(oaClock0.getClockState()) && "9".equals(oaClock1.getClockState())) || ("1".equals(oaClock0.getClockState()) && "10".equals(oaClock1.getClockState()))
                                        /*|| ("1".equals(oaClock0.getClockState()) && "4".equals(oaClock1.getClockState())) || ("4".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState()))*/
                                        || ("5".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState())) || ("5".equals(oaClock0.getClockState()) && "12".equals(oaClock1.getClockState()))
                                        || ("5".equals(oaClock0.getClockState()) && "9".equals(oaClock1.getClockState())) || ("5".equals(oaClock0.getClockState()) && "10".equals(oaClock1.getClockState()))
                                        /*|| ("5".equals(oaClock0.getClockState()) && "4".equals(oaClock1.getClockState()))  || ("4".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState()))*/
                                        || ("12".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState())) || ("12".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState()))
                                        || ("12".equals(oaClock0.getClockState()) && "9".equals(oaClock1.getClockState())) || ("12".equals(oaClock0.getClockState()) && "10".equals(oaClock1.getClockState()))

                                        || ("9".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState())) || ("9".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState()))
                                        || ("9".equals(oaClock0.getClockState()) && "12".equals(oaClock1.getClockState())) || ("9".equals(oaClock0.getClockState()) && "10".equals(oaClock1.getClockState()))

                                        || ("10".equals(oaClock0.getClockState()) && "1".equals(oaClock1.getClockState())) || ("10".equals(oaClock0.getClockState()) && "5".equals(oaClock1.getClockState()))
                                        || ("10".equals(oaClock0.getClockState()) && "12".equals(oaClock1.getClockState())) || ("10".equals(oaClock0.getClockState()) && "9".equals(oaClock1.getClockState()))
                                )){
                                    normalList.add(date);
                                }
                                if(("4".equals(oaClock0.getClockState()) && "4".equals(oaClock1.getClockState())) || "4".equals(oaClock0.getClockState()) || "4".equals(oaClock1.getClockState())){
                                    leaveList.add(date);
                                }
                                if(((!"9".equals(oaClock0.getClockState()) && !"9".equals(oaClock1.getClockState()))
                                        || (!"10".equals(oaClock0.getClockState()) && !"10".equals(oaClock1.getClockState()))) && !abnormalList.contains(date) && !leaveList.contains(date) && !normalList.contains(date)){
                                    abnormalList.add(date);
                                }
                            }
                        }
                    }
                }
            }
            statClockMap.put("normalList", normalList); //保存正常考勤日期
            statClockMap.put("abnormalList", abnormalList); //保存异常考勤日期
            statClockMap.put("leaveList", leaveList); //保存请假考勤日期
        }

        return statClockMap;
    }

    @Override
    public List<OaClock> statOaClockList(OaClockDTO oaClockDTO) {
        //查询内勤
        oaClockDTO.setType("1");
        List<OaClock> oaClockList = oaClockMapper.statOaClockList(oaClockDTO);
        oaClockDTO.setType("2");
        List<OaClock> clockList = oaClockMapper.statOaClockList(oaClockDTO);
        oaClockList.addAll(clockList);
        if(!CollectionUtils.isEmpty(oaClockList)){
            for(OaClock oaClock : oaClockList){
                OaLeave oaLeave = new OaLeave();
                oaLeave.setUserId(oaClockDTO.getEmployeeId());
                oaLeave.setSubmitDate(DateUtils.dateTime(oaClock.getCreateTime()));
                oaLeave.setState("oaLeave:pass");
                //查询请假数据
                oaClock.setLeaveList(oaLeaveMapper.selectOaLeaveList(oaLeave));
            }
        }
        return oaClockList;
    }

    @Override
    public String initClockData() {
        // 获取当前日期
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1; // 月份从0开始，所以要加1

        // 设置日期为当前月的第一天
        calendar.set(year, month - 1, 1);

        // 获取当前月的最大天数
        int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        // 创建SimpleDateFormat对象，用于格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        // 遍历当前月的每一天，并将日期添加到List中
        for (int i = 1; i <= maxDay; i++) {
            if(i < 2){
                calendar.set(year, month - 1, i);
                OaClock oaClock = new OaClock();
                oaClock.setId(getRedisIncreID.getId());
                oaClock.setEmployeeId(new Long(19));
                oaClock.setType("1");
                oaClock.setClockType("1");
                oaClock.setClockDate(calendar.getTime());
                oaClock.setClockTime(calendar.getTime());
                oaClock.setCreateTime(new Date());
                oaClock.setFlag(new Long(0));
                oaClock.setClockState("1");
                oaClock.setClockFlag(new Long(0));
                oaClock.setInitialTime(calendar.getTime());
                oaClock.setAttendanceType("1");
                oaClock.setAttendanceStart("09:00:00");
                oaClock.setWorkHours(new Long(8));
                oaClockMapper.insertOaClock(oaClock);
                oaClock = new OaClock();
                oaClock.setId(getRedisIncreID.getId());
                oaClock.setEmployeeId(new Long(19));
                oaClock.setType("1");
                oaClock.setClockType("2");
                oaClock.setClockDate(calendar.getTime());
                oaClock.setClockTime(calendar.getTime());
                oaClock.setCreateTime(new Date());
                oaClock.setFlag(new Long(0));
                oaClock.setClockState("1");
                oaClock.setClockFlag(new Long(0));
                oaClock.setInitialTime(calendar.getTime());
                oaClock.setAttendanceType("1");
                oaClock.setAttendanceStart("09:00:00");
                oaClock.setWorkHours(new Long(8));
                oaClockMapper.insertOaClock(oaClock);
            }
        }
        return null;
    }

    @Override
    public Map<String,Integer> dayData(OaClock oaClock) {
        oaClock.setGroup("分组");
        Map<String, Integer> objectMap = new HashMap<>();
        //查询当天考勤人数
        List<OaClock> should = oaClockMapper.selectOaClockState(oaClock);
        objectMap.put("应到人数",should.size());
        //实际打卡人数
        oaClock.setReality("0");
        List<OaClock> reality = oaClockMapper.selectOaClockState(oaClock);
        objectMap.put("打卡人数",reality.size());
        //缺卡人数
        oaClock.setReality(null);
        oaClock.setClockState("0");
        List<OaClock> lack = oaClockMapper.selectOaClockState(oaClock);
        objectMap.put("缺卡人数",lack.size());
        //迟到人数
        oaClock.setClockState(null);
        oaClock.setBeLate("迟到");
        List<OaClock> beLate = oaClockMapper.selectOaClockState(oaClock);
        objectMap.put("迟到人数",beLate.size());
        //外勤人数
        oaClock.setBeLate(null);
        oaClock.setClockState("12");
        List<OaClock> fieldPersonnel = oaClockMapper.selectOaClockState(oaClock);
        objectMap.put("外勤人数",fieldPersonnel.size());
        return objectMap;
    }

    @Override
    public List<UserClockDTO> dayDataClock(OaClock oaClock) {
        List<UserClockDTO> userClockDTOS = new ArrayList<>();
        //查询用户名称
        oaClock.setGroup("分组");
        oaClock.setUserIds(oaClock.getEmployeeName());
        List<OaClock> oaClockList = oaClockMapper.selectOaClockState(oaClock);
        for (OaClock clock:oaClockList) {
            UserClockDTO userClockDTO = new UserClockDTO();
            //员工姓名
            SysUser user = sysUserMapper.selectUserById(clock.getEmployeeId());
            userClockDTO.setUserName(user.getNickName());
            userClockDTO.setUserId(user.getUserId());
            //员工部门
            if(user.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                userClockDTO.setDeptName(dept.getDeptName());
            }
            //当天所有打卡信息
            OaClock clock1 = new OaClock();
            clock1.setEmployeeId(clock.getEmployeeId());
            clock1.setQueryTime(oaClock.getQueryTime());
            clock1.setType("1");
            List<OaClock> oaClockList1 = oaClockMapper.selectOaClockList(clock1);
            userClockDTO.setClocks(oaClockList1);
            userClockDTOS.add(userClockDTO);
        }
        return userClockDTOS;
    }

    @Override
    public UserClockDTO weeklyData(OaClock oaClock) {
        UserClockDTO userClockDTO = new UserClockDTO();
        //正常（上下班都正常）
        int normal = 0;
        //迟到
        int beLate = 0;
        //早退
        int leaveEarly = 0;
        //缺卡
        int lackCards = 0;
        //外勤
        int fieldPersonnel = 0;
        //旷工
        int absenteeism = 0;
        //查询当前日期所有需打卡人员
        oaClock.setGroup("分组");
        List<OaClock> oaClockList = oaClockMapper.selectOaClockState(oaClock);
        for (OaClock clock:oaClockList) {
            OaClock clock1 = new OaClock();
            if(StringUtils.isNotEmpty(oaClock.getWeekTime())){
                String[] split = oaClock.getWeekTime().split(",");
                //日期遍历
                for (String queryTime:split) {
                    clock1.setEmployeeId(clock.getEmployeeId());
                    clock1.setQueryTime(queryTime);
                    clock1.setType("1");
                    List<OaClock> oaClockList1 = oaClockMapper.selectOaClockList(clock1);
                    if(oaClockList1.size() > 0){
                        //上下班都为正常
                        if(!"0".equals(oaClockList1.get(0).getAttendanceType()) && !"0".equals(oaClockList1.get(1).getAttendanceType())){
                            if("1".equals(oaClockList1.get(0).getClockState()) && "1".equals(oaClockList1.get(1).getClockState())){
                                normal++;
                            }
                        }
                        //迟到
                        if("1".equals(oaClockList1.get(0).getClockType())){
                            if("2".equals(oaClockList1.get(0).getClockState()) || "6".equals(oaClockList1.get(0).getClockState()) || "7".equals(oaClockList1.get(0).getClockState())){
                                beLate++;
                            }
                        }else if("1".equals(oaClockList1.get(1).getClockType())){
                            if("2".equals(oaClockList1.get(1).getClockState()) || "6".equals(oaClockList1.get(1).getClockState()) || "7".equals(oaClockList1.get(1).getClockState())){
                                beLate++;
                            }
                        }
                        //早退
                        if("2".equals(oaClockList1.get(0).getClockType())){
                            if("3".equals(oaClockList1.get(0).getClockState()) || "8".equals(oaClockList1.get(0).getClockState())){
                                leaveEarly++;
                            }
                        }else if("2".equals(oaClockList1.get(1).getClockType())){
                            if("3".equals(oaClockList1.get(1).getClockState()) || "8".equals(oaClockList1.get(1).getClockState())){
                                leaveEarly++;
                            }
                        }

                        //缺卡
                        if(("0".equals(oaClockList1.get(0).getClockState()) && !"0".equals(oaClockList1.get(1).getClockState())) || ("0".equals(oaClockList1.get(1).getClockState()) && !"0".equals(oaClockList1.get(0).getClockState()))){
                            lackCards++;
                        }
                        //外勤
                        if("12".equals(oaClockList1.get(0).getClockState()) || "12".equals(oaClockList1.get(1).getClockState())){
                            fieldPersonnel++;
                        }
                        //旷工
                        if("11".equals(oaClockList1.get(0).getClockState()) || "11".equals(oaClockList1.get(1).getClockState())){
                            absenteeism++;
                        }
                    }
                }
            }else {
                //查看日历
                OaCalendar oaCalendar = new OaCalendar();
                if(StringUtils.isNotEmpty(oaClock.getThisMonth())){
                    oaCalendar.setDate(oaClock.getThisMonth());
                }
                List<OaCalendar> oaCalendars = oaCalendarMapper.selectOaCalendarList(oaCalendar);
                for (OaCalendar calendar:oaCalendars) {
                    clock1.setEmployeeId(clock.getEmployeeId());
                    clock1.setQueryTime(calendar.getDate());
                    clock1.setType("1");
                    List<OaClock> oaClockList1 = oaClockMapper.selectOaClockList(clock1);
                    if(oaClockList1.size() > 0){
                        //上下班都为正常
                        if(!"0".equals(oaClockList1.get(0).getAttendanceType()) && !"0".equals(oaClockList1.get(1).getAttendanceType())){
                            if("1".equals(oaClockList1.get(0).getClockState()) && "1".equals(oaClockList1.get(1).getClockState())){
                                normal++;
                            }
                        }
                        //迟到
                        if("1".equals(oaClockList1.get(0).getClockType())){
                            if("2".equals(oaClockList1.get(0).getClockState()) || "6".equals(oaClockList1.get(0).getClockState()) || "7".equals(oaClockList1.get(0).getClockState())){
                                beLate++;
                            }
                        }else if("1".equals(oaClockList1.get(1).getClockType())){
                            if("2".equals(oaClockList1.get(1).getClockState()) || "6".equals(oaClockList1.get(1).getClockState()) || "7".equals(oaClockList1.get(1).getClockState())){
                                beLate++;
                            }
                        }
                        //早退
                        if("2".equals(oaClockList1.get(0).getClockType())){
                            if("3".equals(oaClockList1.get(0).getClockState()) || "8".equals(oaClockList1.get(0).getClockState())){
                                leaveEarly++;
                            }
                        }else if("2".equals(oaClockList1.get(1).getClockType())){
                            if("3".equals(oaClockList1.get(1).getClockState()) || "8".equals(oaClockList1.get(1).getClockState())){
                                leaveEarly++;
                            }
                        }
                        //缺卡
                        if(("0".equals(oaClockList1.get(0).getClockState()) && !"0".equals(oaClockList1.get(1).getClockState())) || ("0".equals(oaClockList1.get(1).getClockState()) && !"0".equals(oaClockList1.get(0).getClockState()))){
                            lackCards++;
                        }
                        //外勤
                        if("12".equals(oaClockList1.get(0).getClockState()) || "12".equals(oaClockList1.get(1).getClockState())){
                            fieldPersonnel++;
                        }
                        //旷工
                        if("11".equals(oaClockList1.get(0).getClockState()) || "11".equals(oaClockList1.get(1).getClockState())){
                            absenteeism++;
                        }
                    }
                }
            }
        }
        userClockDTO.setNormal(Long.valueOf(normal));
        userClockDTO.setBeLate(Long.valueOf(beLate));
        userClockDTO.setLeaveEarly(Long.valueOf(leaveEarly));
        userClockDTO.setLackCards(Long.valueOf(lackCards));
        userClockDTO.setFieldPersonnel(Long.valueOf(fieldPersonnel));
        userClockDTO.setAbsenteeism(Long.valueOf(absenteeism));
        return userClockDTO;
    }

    @Override
    public List<UserClockDTO> weeklyDataClock(OaClock oaClock) {
        List<UserClockDTO> userClockDTOS = new ArrayList<>();
        //查询当前日期所有需打卡人员
        oaClock.setGroup("分组");
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        List<OaClock> oaClockList = oaClockMapper.selectOaClockState(oaClock);
        for (OaClock clock:oaClockList) {
            UserClockDTO userClockDTO = new UserClockDTO();
            //员工姓名
            SysUser user = sysUserMapper.selectUserById(clock.getEmployeeId());
            userClockDTO.setUserName(user.getNickName());
            userClockDTO.setUserId(user.getUserId());
            //员工部门
            if(user.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                userClockDTO.setDeptName(dept.getDeptName());
            }
            //打卡信息
            List<OaClock> list = new ArrayList<>();
            //周期内所有打卡信息
            OaClock clock1 = new OaClock();
            if(StringUtils.isNotEmpty(oaClock.getWeekTime())){
                String[] split = oaClock.getWeekTime().split(",");
                //日期遍历
                for (String queryTime:split) {
                    clock1.setEmployeeId(clock.getEmployeeId());
                    clock1.setQueryTime(queryTime);
                    clock1.setType("1");
                    if(StringUtils.isNotEmpty(oaClock.getClockState())){
                        clock1.setClockState(oaClock.getClockState());
                    }
                    List<OaClock> oaClockList1 = oaClockMapper.selectOaClockList(clock1);
                    if(oaClockList1.size() > 0){
                        if(oaClockList1.size() == 2){
                            if(!"0".equals(oaClockList1.get(0).getClockState()) && !"0".equals(oaClockList1.get(1).getClockState())){
                                OaClock clock2 = oaClockList1.get(1);
                                try {
                                    String week = DateUtils.dateToWeek(queryTime);
                                    clock2.setDateWeek(queryTime+"（"+week+"）");
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                list.add(clock2);
                            }
                        }
                        if(oaClockList1.size() == 1 && !"1".equals(clock1.getClockState())){
                            OaClock clock2 = oaClockList1.get(0);
                            try {
                                String week = DateUtils.dateToWeek(queryTime);
                                clock2.setDateWeek(queryTime+"（"+week+"）");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            list.add(clock2);
                        }
                    }
                }
                userClockDTO.setClocks(list);
            }else {
                //查看日历
                OaCalendar oaCalendar = new OaCalendar();
                if(StringUtils.isNotEmpty(oaClock.getThisMonth())){
                    oaCalendar.setDate(oaClock.getThisMonth());
                }
                List<OaCalendar> oaCalendars = oaCalendarMapper.selectOaCalendarList(oaCalendar);
                for (OaCalendar calendar:oaCalendars) {
                    clock1.setEmployeeId(clock.getEmployeeId());
                    clock1.setQueryTime(calendar.getDate());
                    clock1.setType("1");
                    if(StringUtils.isNotEmpty(oaClock.getClockState())){
                        clock1.setClockState(oaClock.getClockState());
                    }
                    List<OaClock> oaClockList1 = oaClockMapper.selectOaClockList(clock1);
                    if(oaClockList1.size() > 0){
                        if(oaClockList1.size() == 2){
                            if(!"0".equals(oaClockList1.get(0).getClockState()) && !"0".equals(oaClockList1.get(1).getClockState())){
                                OaClock clock2 = oaClockList1.get(0);
                                try {
                                    String week = DateUtils.dateToWeek(calendar.getDate());
                                    clock2.setDateWeek(calendar.getDate()+"（"+week+"）");
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                list.add(clock2);
                            }
                        }
                        if(oaClockList1.size() == 1 && !"1".equals(clock1.getClockState())){
                            OaClock clock2 = oaClockList1.get(0);
                            try {
                                String week = DateUtils.dateToWeek(calendar.getDate());
                                clock2.setDateWeek(calendar.getDate()+"（"+week+"）");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            list.add(clock2);
                        }
                    }
                }
                userClockDTO.setClocks(list);
            }
            if(userClockDTO.getClocks().size() > 0){
                userClockDTOS.add(userClockDTO);
            }
        }
        return userClockDTOS;
    }


}
