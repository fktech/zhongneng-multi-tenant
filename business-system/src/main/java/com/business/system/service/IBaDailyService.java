package com.business.system.service;

import java.util.List;
import java.util.Map;

import com.business.system.domain.BaDaily;
import com.business.system.domain.BaDailyFollow;
import com.business.system.domain.dto.DailyDTO;
import com.business.system.domain.dto.UserDailyDTO;

/**
 * 日报Service接口
 *
 * @author single
 * @date 2023-10-09
 */
public interface IBaDailyService
{
    /**
     * 查询日报
     *
     * @param id 日报ID
     * @return 日报
     */
    public BaDaily selectBaDailyById(String id);

    /**
     * 查询日报列表
     *
     * @param baDaily 日报
     * @return 日报集合
     */
    public List<BaDaily> selectBaDailyList(BaDaily baDaily);

    /**
     * 新增日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    public int insertBaDaily(BaDaily baDaily);

    /**
     * 修改日报
     *
     * @param baDaily 日报
     * @return 结果
     */
    public int updateBaDaily(BaDaily baDaily);

    /**
     * 批量删除日报
     *
     * @param ids 需要删除的日报ID
     * @return 结果
     */
    public int deleteBaDailyByIds(String[] ids);

    /**
     * 删除日报信息
     *
     * @param id 日报ID
     * @return 结果
     */
    public int deleteBaDailyById(String id);

    /**
     * 查询未提交数据
     */
    public List<UserDailyDTO> notSubmitted(BaDaily baDaily);

    /**
     * 查询已完成日报
     */
    public List<UserDailyDTO> completedDaily(BaDaily baDaily);

    /**
     * 关注关联关系
     */
    public int dailyFollow(BaDailyFollow baDailyFollow);

    /**
     * 查询所有关注
     */
    public List<BaDailyFollow> selectBaDailyFollowList(BaDailyFollow baDailyFollow);

    /**
     * 取消关注
     */
    public int unFollow(BaDailyFollow baDailyFollow);

    /**
     * 关注列表
     */
    public List<UserDailyDTO> followList(BaDaily baDaily);

    /**
     * 单个关注
     */
    public int singleFollow(BaDailyFollow baDailyFollow);

    /**
     * 日报统计
     */
    public Map<String,Integer> dailyCount(BaDaily baDaily);

    /**
     * 报表统计
     */
    public List<DailyDTO> dailyStatistics(BaDaily baDaily);


}
