package com.business.system.service;

import com.business.system.domain.OaBeforehandStat;
import com.business.system.domain.vo.OaBeforehandStatVO;

import java.util.List;
import java.util.Map;

/**
 * 事前申请统计Service接口
 *
 * @author single
 * @date 2023-11-27
 */
public interface IOaBeforehandStatService
{
    /**
     * APP打卡统计
     */
    public Map beforehandStat(OaBeforehandStat oaBeforehandStat);

    /**
     * 申请部门分布
     */
    public List<OaBeforehandStatVO> applyDeptStat(OaBeforehandStat oaBeforehandStat);
}
