package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaEvection;
import com.business.system.domain.OaEvectionTrip;

/**
 * 出差申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaEvectionService
{
    /**
     * 查询出差申请
     *
     * @param id 出差申请ID
     * @return 出差申请
     */
    public OaEvection selectOaEvectionById(String id);

    /**
     * 查询出差申请列表
     *
     * @param oaEvection 出差申请
     * @return 出差申请集合
     */
    public List<OaEvection> selectOaEvectionList(OaEvection oaEvection);

    /**
     * 新增出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    public int insertOaEvection(OaEvection oaEvection);

    /**
     * 修改出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    public int updateOaEvection(OaEvection oaEvection);

    /**
     * 批量删除出差申请
     *
     * @param ids 需要删除的出差申请ID
     * @return 结果
     */
    public int deleteOaEvectionByIds(String[] ids);

    /**
     * 删除出差申请信息
     *
     * @param id 出差申请ID
     * @return 结果
     */
    public int deleteOaEvectionById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 出差报表统计
     * @param oaEvectionTrip
     * @return
     */
    public List<OaEvectionTrip> selectOaEvectionTrip(OaEvectionTrip oaEvectionTrip);
}
