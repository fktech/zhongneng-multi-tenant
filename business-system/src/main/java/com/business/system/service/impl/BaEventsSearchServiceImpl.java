package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaEventsSearch;
import com.business.system.domain.dto.SysEventsSearchDTO;
import com.business.system.mapper.BaEventsSearchMapper;
import com.business.system.service.IBaEventsSearchService;
import com.hikvision.artemis.sdk.ArtemisHttpUtil;
import com.hikvision.artemis.sdk.config.ArtemisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 联动事件Service业务层处理
 *
 * @author single
 * @date 2023-07-05
 */
@Service
public class BaEventsSearchServiceImpl extends ServiceImpl<BaEventsSearchMapper, BaEventsSearch> implements IBaEventsSearchService
{


    private static final String ARTEMIS_PATH = "/artemis";

    String host = "111.53.49.229:50443";
    String appkey = "20947274";
    String appSecret = "0s4VfibB33BTCj5YmQiS";


    @Autowired
    private BaEventsSearchMapper baEventsSearchMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询联动事件
     *
     * @param id 联动事件ID
     * @return 联动事件
     */
    @Override
    public BaEventsSearch selectBaEventsSearchById(String id)
    {
        return baEventsSearchMapper.selectBaEventsSearchById(id);
    }

    /**
     * 查询联动事件列表
     *
     * @param baEventsSearch 联动事件
     * @return 联动事件
     */
    @Override
    public List<BaEventsSearch> selectBaEventsSearchList(BaEventsSearch baEventsSearch)
    {
        return baEventsSearchMapper.selectBaEventsSearchList(baEventsSearch);
    }

    /**
     * 新增联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    @Override
    public int insertBaEventsSearch(BaEventsSearch baEventsSearch)
    {
        baEventsSearch.setCreateTime(DateUtils.getNowDate());
        return baEventsSearchMapper.insertBaEventsSearch(baEventsSearch);
    }

    /**
     * 修改联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    @Override
    public int updateBaEventsSearch(BaEventsSearch baEventsSearch)
    {
        baEventsSearch.setUpdateTime(DateUtils.getNowDate());
        return baEventsSearchMapper.updateBaEventsSearch(baEventsSearch);
    }

    /**
     * 批量删除联动事件
     *
     * @param ids 需要删除的联动事件ID
     * @return 结果
     */
    @Override
    public int deleteBaEventsSearchByIds(String[] ids)
    {
        return baEventsSearchMapper.deleteBaEventsSearchByIds(ids);
    }

    /**
     * 删除联动事件信息
     *
     * @param id 联动事件ID
     * @return 结果
     */
    @Override
    public int deleteBaEventsSearchById(String id)
    {
        return baEventsSearchMapper.deleteBaEventsSearchById(id);
    }

    @Override
    public String eventsSearchList(SysEventsSearchDTO sysEventsSearchDTO){
        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/els/v1/events/search";
        //Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        JSONObject paramMap = new JSONObject();
        paramMap.put("startTime", sysEventsSearchDTO.getStartTime());
        paramMap.put("endTime", sysEventsSearchDTO.getEndTime());
        paramMap.put("pageSize", sysEventsSearchDTO.getPageSize());
        paramMap.put("pageNo", sysEventsSearchDTO.getPageNo());
        //左侧树筛选
        paramMap.put("eventTypes",sysEventsSearchDTO.getEventTypes());
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s = "";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config, path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        Object parse = JSONObject.parse(s);
        String code = ((JSONObject) parse).getString("code");
        if(code.equals("0")){
            ((JSONObject) parse).put("code",200);
        }
        return parse.toString();
    }

    @Override
    public String cameraList(String status, String[] indexCodes) {
        ArtemisConfig config = new ArtemisConfig();
        config.setHost(host); // 代理API网关nginx服务器ip端口
        config.setAppKey(appkey);  // 秘钥appkey
        config.setAppSecret(appSecret);// 秘钥appSecret
        final String getCamsApi = ARTEMIS_PATH + "/api/nms/v1/online/camera/get";
        //Map<String, String> paramMap = new HashMap<String, String>();// post请求Form表单参数
        JSONObject paramMap = new JSONObject();
        paramMap.put("status",status);
        if(StringUtils.isNotEmpty(indexCodes)){
            paramMap.put("indexCodes",indexCodes);
        }
        paramMap.put("pageSize", "50");
        paramMap.put("pageNo", "1");
        String body = JSON.toJSON(paramMap).toString();
        Map<String, String> path = new HashMap<String, String>(5) {
            {
                put("https://", getCamsApi);
            }
        };
        String s ="";
        try {
            s = ArtemisHttpUtil.doPostStringArtemis(config,path, body, null, null, "application/json");
        }catch (Exception e){
            throw new ServiceException("监控设备请求超时，请刷新重试");
        }
        return s;
    }


}
