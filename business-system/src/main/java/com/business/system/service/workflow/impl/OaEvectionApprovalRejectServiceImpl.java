package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEvection;
import com.business.system.domain.OaOvertime;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEvectionApprovalService;
import com.business.system.service.workflow.IOaOvertimeApprovalService;
import org.springframework.stereotype.Service;


/**
 * 出差申请审批结果:审批拒绝
 */
@Service("oaEvectionApprovalContent:processApproveResult:reject")
public class OaEvectionApprovalRejectServiceImpl extends IOaEvectionApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAEVECTION_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaEvection checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
