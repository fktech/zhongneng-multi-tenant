package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaInvoiceStatVO;
import com.business.system.domain.vo.BaInvoiceVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaInvoiceService;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 发票信息列Service业务层处理
 *
 * @author single
 * @date 2022-12-28
 */
@Service
public class BaInvoiceServiceImpl extends ServiceImpl<BaInvoiceMapper, BaInvoice> implements IBaInvoiceService
{
    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    private static final Logger log = LoggerFactory.getLogger(BaInvoiceServiceImpl.class);

    /**
     * 查询发票信息列
     *
     * @param id 发票信息列ID
     * @return 发票信息列
     */
    @Override
    public BaInvoice selectBaInvoiceById(String id)
    {
        BaInvoice baInvoice = baInvoiceMapper.selectBaInvoiceById(id);

        BaBank baBank = baBankMapper.selectBaBankById(baInvoice.getBankId());
        baInvoice.setBaBank(baBank);

        //转company
        if(StringUtils.isNotEmpty(baInvoice.getBuyer())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baInvoice.getBuyer());
            if(StringUtils.isNotNull(enterprise)){
                baInvoice.setBuyerName(enterprise.getName());
                //税号查询
                QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
                informationQueryWrapper.eq("relevance_id",enterprise.getId());
                BaBillingInformation baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
                baInvoice.setBuyerUnit(baBillingInformation.getDutyParagraph());
            }
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baInvoice.getBuyer());
            if(StringUtils.isNotNull(baSupplier)){
                baInvoice.setBuyerName(baSupplier.getName());
                baInvoice.setBuyerUnit(baSupplier.getTaxNum());
            }
            //公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baInvoice.getBuyer());
            if(StringUtils.isNotNull(baCompany)){
                baInvoice.setBuyerName(baCompany.getName());
                baInvoice.setBuyerUnit(baCompany.getTaxNum());
            }
        }

        //合同启动信息
        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baInvoice.getStartId());
        baInvoice.setBaContractStart(baContractStart);

        //品名
        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baInvoice.getGoodsId());
        if(!ObjectUtils.isEmpty(baGoods)){
            baInvoice.setGoodsName(baGoods.getName());
        }

//        if(StringUtils.isNotEmpty(baInvoice.getSettlementId())){
            //结算信息
//            BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(baInvoice.getSettlementId());
//            baInvoice.setBaSettlement(baSettlement);
            //账户信息
//            QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
//            queryWrapper.eq("relation_id",baSettlement.getContractId());
//            queryWrapper.eq("flag",0);
//            List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);


//            baBanks.forEach(item ->{
//                if(StringUtils.isNotEmpty(item.getCompanyName())){
//                    //终端企业
//                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getCompanyName());
//                    if(StringUtils.isNotNull(enterprise)){
//                        item.setCompanyName(enterprise.getName());
//                    }
//                    //供应商
//                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getCompanyName());
//                    if(StringUtils.isNotNull(baSupplier)){
//                        item.setCompanyName(baSupplier.getName());
//                    }
//                    //公司
//                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getCompanyName());
//                    if(StringUtils.isNotNull(baCompany)){
//                        item.setCompanyName(baCompany.getName());
//                    }
//                }
//            });
//            baInvoice.setBankList(baBanks);
//        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baInvoice.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baInvoice.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baInvoice.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baInvoice.setUser(sysUser);

        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baInvoice.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }

        //发票详情信息
        BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
        baInvoiceDetail.setInvoiceId(baInvoice.getId());
        List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
        baInvoice.setBaInvoiceDetailList(baInvoiceDetailList);

        //采购单名称
        if(StringUtils.isNotEmpty(baInvoice.getPlanId())){
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baInvoice.getPlanId());
            baInvoice.setPlanName(baProcurementPlan.getPlanName());
        }
        //仓储立项名称
        if(StringUtils.isNotEmpty(baInvoice.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baInvoice.getProjectId());
            baInvoice.setProjectName(baProject.getName());
        }
        //销售订单
        if(StringUtils.isNotEmpty(baInvoice.getOrderId())){
            BaContractStart baContractStart1 = baContractStartMapper.selectBaContractStartById(baInvoice.getOrderId());
            baInvoice.setOrderName(baContractStart1.getName());
        }
        return baInvoice;
    }

    @Override
    public List<BaInvoice> selectBaInvoiceNums(String s) {
        BaInvoice baInvoice=new BaInvoice();
        baInvoice.setFlag(0L);
        baInvoice.setState(s);
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        List<BaInvoice> baInvoices = baInvoiceMapper.selectBaInvoiceNums(baInvoice);
        return baInvoices;
    }

    @Override
    public List<BaInvoice> selectNumsAmountByMonth(BaInvoice baInvoice) {
        return baInvoiceMapper.selectNumsAmountByMonth(baInvoice);
    }

    /**
     * 查询发票信息列
     *
     * @param id 发票信息列ID
     * @return 发票信息列
     */
    @Override
    public BaInvoice selectBaInvoiceInfoById(String id)
    {
        BaInvoice baInvoice = new BaInvoice();
        if(StringUtils.isNotEmpty(id)){
            //结算信息
            BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);

            //账户信息
            QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",baSettlement.getContractId());
            queryWrapper.eq("flag",0);
            List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
            for (BaBank baBank:baBanks) {
                //银行账户号
                if(StringUtils.isNotNull(baBank.getAccount())){
                    BaBank baBank1 = baBankMapper.selectBaBankById(baBank.getAccount());
                    if(StringUtils.isNotNull(baBank1)){
                        baBank.setAccount(baBank1.getAccount());
                    }
                }
                if(StringUtils.isNotEmpty(baBank.getCompanyName())){
                    //终端企业
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baBank.getCompanyName());
                    if(StringUtils.isNotNull(enterprise)){
                        baBank.setCompanyName(enterprise.getName());
                    }
                    //供应商
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baBank.getCompanyName());
                    if(StringUtils.isNotNull(baSupplier)){
                        baBank.setCompanyName(baSupplier.getName());
                    }
                    //公司
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baBank.getCompanyName());
                    if(StringUtils.isNotNull(baCompany)){
                        baBank.setCompanyName(baCompany.getName());
                    }
                }
            }
            baInvoice.setBankList(baBanks);
        }
        return baInvoice;
    }

    /**
     * 查询发票信息列列表
     *
     * @param baInvoice 发票信息列
     * @return 发票信息列
     */
    @Override
    public List<BaInvoice> selectBaInvoiceList(BaInvoice baInvoice)
    {
        List<BaInvoice> baInvoices = baInvoiceMapper.selectBaInvoiceListByCondition(baInvoice);
        baInvoices.stream().forEach(item -> {
            //封装合同启动
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
            if(!ObjectUtils.isEmpty(baContractStart)){
                item.setBaContractStart(baContractStart);
            }
            //售票单位名称
            if(StringUtils.isNotEmpty(item.getBuyer())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getBuyer());
                if(StringUtils.isNotNull(enterprise)){
                    item.setBuyerName(enterprise.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getBuyer());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setBuyerName(baSupplier.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getBuyer());
                if(StringUtils.isNotNull(baCompany)){
                    item.setBuyerName(baCompany.getName());
                }
            }
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位信息
                String name = getName.getName(item.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                }

            }
            //查询税价合计
            BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
            baInvoiceDetail.setInvoiceId(item.getId());
            List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
            //税价合计
            BigDecimal bigDecimal=new BigDecimal(0.0000);
            //数量合计
            BigDecimal amountTotal=new BigDecimal(0);
            for(BaInvoiceDetail baInvoiceDetail1:baInvoiceDetailList){
                if(baInvoiceDetail1.getTaxPriceTotal()!=null){
                    bigDecimal=bigDecimal.add(baInvoiceDetail1.getTaxPriceTotal());
                }
                if(null != baInvoiceDetail1.getAccount()){
                    amountTotal = amountTotal.add(baInvoiceDetail1.getAccount());
                }
            }
            item.setTaxPriceTotal(bigDecimal);
            item.setAmountTotal(amountTotal);
            item.setBaInvoiceDetailList(baInvoiceDetailList);
            //采购单名称
            if(StringUtils.isNotEmpty(item.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(item.getPlanId());
                item.setPlanName(baProcurementPlan.getPlanName());
            }
            //仓储立项名称
            if(StringUtils.isNotEmpty(item.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(item.getProjectId());
                item.setProjectName(baProject.getName());
            }
            //销售订单
            if(StringUtils.isNotEmpty(item.getOrderId())){
                BaContractStart baContractStart1 = baContractStartMapper.selectBaContractStartById(item.getOrderId());
                item.setOrderName(baContractStart1.getName());
            }


//            BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(item.getSettlementId());
//            BaContract baContract = baContractMapper.selectBaContractById(baSettlement.getContractId());
//            if(!ObjectUtils.isEmpty(baContract)){
//                item.setContractName(baContract.getName());
//            }
        });
        return baInvoices;
    }

    /**
     * 新增发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    @Override
    public int insertBaInvoice(BaInvoice baInvoice)
    {
        baInvoice.setId(getRedisIncreID.getId());
        baInvoice.setCreateTime(DateUtils.getNowDate());
        baInvoice.setCreateBy(SecurityUtils.getUsername());
        baInvoice.setUpdateBy(SecurityUtils.getUsername());
        baInvoice.setUpdateTime(DateUtils.getNowDate());
        baInvoice.setUserId(SecurityUtils.getUserId());
        baInvoice.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
//        if(StringUtils.isNotEmpty(baInvoice.getSettlementId())){
//            查询合同编号
//           BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(baInvoice.getSettlementId());
//           baInvoice.setContractId(baSettlement.getContractId());
//        }
        //合同启动全局编码
        String startGlobalNumber = baInvoice.getStartGlobalNumber();
        //判断是否为上海公司
        String judge = "";
        //查看合同启动
        if(StringUtils.isNotEmpty(baInvoice.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baInvoice.getStartId());
            judge = baContractStart.getBelongCompanyId();
        }
        //判断是否是销售发票
        if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())){
            if(!"2".equals(baInvoice.getWorkState())){
                //流程发起状态
                baInvoice.setWorkState("1");
                //默认审批流程已通过
                baInvoice.setState(AdminCodeEnum.INVOICE_STATUS_PASS.getCode());
                baInvoice.setInvoiceState("1"); //发票状态待开票
            }
            if("false".equals(baInvoice.getOpenBallot())) {
                if("2".equals(baInvoice.getWorkState())){
                    //流程实例ID
                    String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode(),SecurityUtils.getCurrComId());
                    baInvoice.setFlowId(flowId);
                }
                Integer result = baInvoiceMapper.insertBaInvoice(baInvoice);
                if("2".equals(baInvoice.getWorkState())){
                    if (result > 0) {
                        baInvoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                        baInvoice.setStartGlobalNumber(startGlobalNumber);
                        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                        //启动流程实例
                        baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baInvoice, AdminCodeEnum.INVOICE_APPROVAL_CONTENT_INSERT.getCode());
                        if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                            baInvoiceMapper.deleteBaInvoiceById(baInvoice.getId());
                            return 0;
                        } else {
                            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
                                AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(baProcessInstancesRuntimeVO.getId());
                                //判断流程实例是否结束
                                String processInstanceIsEndResult = "";
                                if (!ObjectUtils.isEmpty(ajaxResult)) {
                                    processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
                                }
                                BaInvoice baInvoice2 = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                                // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
                                if (processInstanceIsEndResult.equals(Constants.SUCCESS)) {
                                    BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(baProcessInstancesRuntimeVO.getId());
                                    if (!ObjectUtils.isEmpty(applyBusinessDataByProcessInstance)) {
                                        applyBusinessDataByProcessInstance.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
                                        baProcessInstanceRelatedMapper.updateById(applyBusinessDataByProcessInstance);
                                    }
                                    baInvoice2.setState(AdminCodeEnum.INVOICE_STATUS_PASS.getCode());
                                    baInvoice2.setWorkState("1");
                                } else {
                                    baInvoice2 = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                                    baInvoice2.setState(AdminCodeEnum.INVOICE_STATUS_VERIFYING.getCode());
                                }
                                baInvoice2.setInvoiceType("1");
                                baInvoice2.setInvoiceState("1");
                                //合同启动全局编号
                                if (StringUtils.isNotEmpty(baInvoice.getStartGlobalNumber())) {
                                    //全局编号
                                    QueryWrapper<BaInvoice> queryWrapper = new QueryWrapper<>();
                                    queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
                                    queryWrapper.eq("invoice_type", AdminCodeEnum.SALE_INVOICE.getCode());
                                    queryWrapper.eq("flag", 0);
                                    queryWrapper.isNotNull("serial_number");
                                    queryWrapper.orderByDesc("serial_number");
                                    List<BaInvoice> invoiceList = baInvoiceMapper.selectList(queryWrapper);
                                    if (invoiceList.size() > 0) {
                                        Integer serialNumber = invoiceList.get(0).getSerialNumber();
                                        baInvoice2.setSerialNumber(serialNumber + 1);
                                        if (serialNumber < 10) {
                                            baInvoice2.setGlobalNumber(baInvoice.getStartGlobalNumber() + "-" + "XXFP" + "-" + "000" + baInvoice2.getSerialNumber());
                                        } else if (serialNumber >= 10) {
                                            baInvoice2.setGlobalNumber(baInvoice.getStartGlobalNumber() + "-" + "XXFP" + "-" + "00" + baInvoice2.getSerialNumber());
                                        } else if (serialNumber >= 100) {
                                            baInvoice2.setGlobalNumber(baInvoice.getStartGlobalNumber() + "-" + "XXFP" + "-" + "0" + baInvoice2.getSerialNumber());
                                        } else if (serialNumber >= 1000) {
                                            baInvoice2.setGlobalNumber(baInvoice.getStartGlobalNumber() + "-" + "XXFP" + "-" + baInvoice2.getSerialNumber());
                                        }
                                    } else {
                                        baInvoice2.setSerialNumber(1);
                                        baInvoice2.setGlobalNumber(baInvoice.getStartGlobalNumber() + "-" + "XXFP" + "-" + "000" + baInvoice2.getSerialNumber());
                                    }
                                }
                                baInvoiceMapper.updateBaInvoice(baInvoice2);
                            }
                        }
                    }
                }
            } else {
                baInvoice.setUpdateBy(SecurityUtils.getUsername());
                baInvoice.setUpdateTime(DateUtils.getNowDate());
                baInvoice.setInvoiceState("2"); //发票状态已开票

                //插入发票详情信息
                List<BaInvoiceDetail> baInvoiceDetailList = baInvoice.getBaInvoiceDetailList();
                if(!CollectionUtils.isEmpty(baInvoiceDetailList)){
                    for(BaInvoiceDetail baInvoiceDetail : baInvoiceDetailList){
                        baInvoiceDetail.setId(getRedisIncreID.getId());
                        baInvoiceDetail.setInvoiceId(baInvoice.getId());
                        baInvoiceDetail.setCreateBy(SecurityUtils.getUsername());
                        baInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                    }
                    baInvoiceDetailMapper.batchInsertBaInvoiceDetail(baInvoiceDetailList);
                }
                baInvoiceMapper.updateBaInvoice(baInvoice);
            }
        } else {
//            流程关闭开始
            //发票无业务状态
            if("2".equals(baInvoice.getInvoiceType())){
                baInvoice.setInvoiceState("2"); //已开票
            }
//            流程关闭结束
            //流程发起状态
            if(!"2".equals(baInvoice.getWorkState())){
                baInvoice.setWorkState("1");
                //默认审批流程已通过
                baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
            }
            //插入发票详情信息
            List<BaInvoiceDetail> baInvoiceDetailList = baInvoice.getBaInvoiceDetailList();
            if(!CollectionUtils.isEmpty(baInvoiceDetailList)){
                for(BaInvoiceDetail baInvoiceDetail : baInvoiceDetailList){
                    baInvoiceDetail.setId(getRedisIncreID.getId());
                    baInvoiceDetail.setInvoiceId(baInvoice.getId());
                    baInvoiceDetail.setCreateBy(SecurityUtils.getUsername());
                    baInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                }
                baInvoiceDetailMapper.batchInsertBaInvoiceDetail(baInvoiceDetailList);
            }
            if("2".equals(baInvoice.getWorkState())){
                //流程实例ID
                String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode(),SecurityUtils.getCurrComId());
                baInvoice.setFlowId(flowId);
            }
            //合同启动全局编号
            if(StringUtils.isNotEmpty(baInvoice.getStartGlobalNumber())){
                //全局编号
                QueryWrapper<BaInvoice> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                queryWrapper.eq("flag",0);
                queryWrapper.eq("invoice_type",AdminCodeEnum.PURCHASE_INVOICE.getCode());
                queryWrapper.isNotNull("serial_number");
                queryWrapper.orderByDesc("serial_number");
                List<BaInvoice> invoiceList = baInvoiceMapper.selectList(queryWrapper);
                if(invoiceList.size() > 0){
                    Integer serialNumber = invoiceList.get(0).getSerialNumber();
                    baInvoice.setSerialNumber(serialNumber+1);
                    if(serialNumber < 10){
                        baInvoice.setGlobalNumber(baInvoice.getStartGlobalNumber()+"-"+"JXFP"+"-"+"000"+baInvoice.getSerialNumber());
                    }else if(serialNumber >= 10){
                        baInvoice.setGlobalNumber(baInvoice.getStartGlobalNumber()+"-"+"JXFP"+"-"+"00"+baInvoice.getSerialNumber());
                    }else if(serialNumber >= 100){
                        baInvoice.setGlobalNumber(baInvoice.getStartGlobalNumber()+"-"+"JXFP"+"-"+"0"+baInvoice.getSerialNumber());
                    }else if(serialNumber >= 1000){
                        baInvoice.setGlobalNumber(baInvoice.getStartGlobalNumber()+"-"+"JXFP"+"-"+baInvoice.getSerialNumber());
                    }
                }else {
                    baInvoice.setSerialNumber(1);
                    baInvoice.setGlobalNumber(baInvoice.getStartGlobalNumber()+"-"+"JXFP"+"-"+"000"+baInvoice.getSerialNumber());
                }
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baInvoice.getId());
                baGlobalNumber.setCode(baInvoice.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("9");
                baGlobalNumber.setStartId(baInvoice.getStartId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }
            Integer result = baInvoiceMapper.insertBaInvoice(baInvoice);
            if("2".equals(baInvoice.getWorkState())){
                if(result > 0){
                    baInvoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                    //全局编号
                    baInvoice.setStartGlobalNumber(startGlobalNumber);
                    BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                    //启动流程实例
                    baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baInvoice, AdminCodeEnum.INPUTINVOICE_APPROVAL_CONTENT_INSERT.getCode());
                    if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                        baInvoiceMapper.deleteBaInvoiceById(baInvoice.getId());
                        baInvoiceDetailMapper.deleteBaInvoiceDetailByInvoiceId(baInvoice.getId());
                        return 0;
                    }else {
                        if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
                            AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(baProcessInstancesRuntimeVO.getId());
                            //判断流程实例是否结束
                            String processInstanceIsEndResult = "";
                            if (!ObjectUtils.isEmpty(ajaxResult)) {
                                processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
                            }
                            BaInvoice baInvoice2 = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                            // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
                            if (processInstanceIsEndResult.equals(Constants.SUCCESS)) {
                                BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(baProcessInstancesRuntimeVO.getId());
                                if (!ObjectUtils.isEmpty(applyBusinessDataByProcessInstance)) {
                                    applyBusinessDataByProcessInstance.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
                                    baProcessInstanceRelatedMapper.updateById(applyBusinessDataByProcessInstance);
                                }
                                baInvoice2.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
                            } else {
                                baInvoice2.setState(AdminCodeEnum.INPUTINVOICE_STATUS_VERIFYING.getCode());
                            }
                            baInvoice2.setInvoiceType("2");
                            baInvoice2.setInvoiceState("1");
                            baInvoiceMapper.updateBaInvoice(baInvoice2);
                        }
                    }
                }
            }
//            //更新项目阶段
//            if(StringUtils.isNotEmpty(baInvoice.getContractId())){
//                //合同信息
//                BaContract baContract = baContractMapper.selectBaContractById(baInvoice.getContractId());
//                if(StringUtils.isNotEmpty(baContract.getProjectId())){
//                    //项目信息
//                    BaProject baProject = projectMapper.selectBaProjectById(baContract.getProjectId());
//                    baProject.setProjectStage(12);
//                    projectMapper.updateBaProject(baProject);
//                }
//            }
        }
        return 1;
    }

    /**
     * 发票提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaInvoice invoice, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(invoice.getFlowId());
        Map<String, Object> map = new HashMap<>();
        if(AdminCodeEnum.SALE_INVOICE.getCode().equals(invoice.getInvoiceType())){
            //销项发票
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode());
        } else {
            map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode());
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(invoice.getId());
        if(AdminCodeEnum.SALE_INVOICE.getCode().equals(invoice.getInvoiceType())) {
            //销项发票
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode());
        } else {
            relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode());
        }
        //获取流程实例关联的业务对象
        BaInvoice baInvoice = baInvoiceMapper.selectBaInvoiceById(invoice.getId());
        SysUser sysUser = iSysUserService.selectUserById(baInvoice.getUserId());
        baInvoice.setUserName(sysUser.getUserName());
        baInvoice.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baInvoice));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);
        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO, baInvoice);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime, BaInvoice invoice) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(AdminCodeEnum.SALE_INVOICE.getCode().equals(invoice.getInvoiceType())) {
                if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.INVOICE_APPROVAL_CONTENT_UPDATE.getCode())) {
                    baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                    baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                } else {
                    baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                }
            } else {
                if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.INPUTINVOICE_APPROVAL_CONTENT_UPDATE.getCode())) {
                    baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                    baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                } else {
                    baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                }
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改发票信息列
     *
     * @param baInvoice 发票信息列
     * @return 结果
     */
    @Override
    public int updateBaInvoice(BaInvoice baInvoice)
    {
        BaInvoice baInvoice1 = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
        baInvoice.setUpdateTime(DateUtils.getNowDate());
        baInvoice.setUpdateBy(SecurityUtils.getUsername());
        Integer result = baInvoiceMapper.updateBaInvoice(baInvoice);
        //项目阶段
        if(result > 0 && "2".equals(baInvoice.getInvoiceState())){
            if(StringUtils.isNotEmpty(baInvoice.getId())){
                BaInvoice invoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                //更新项目阶段
//                if(StringUtils.isNotEmpty(invoice.getContractId())){
//                    //合同信息
//                    BaContract baContract = baContractMapper.selectBaContractById(invoice.getContractId());
//                    if(StringUtils.isNotEmpty(baContract.getProjectId())){
//                        //项目信息
//                        BaProject baProject = projectMapper.selectBaProjectById(baContract.getProjectId());
//                        baProject.setProjectStage(11);
//                        projectMapper.updateBaProject(baProject);
//                    }
//                }
            }
        }
        //判断是否是销售发票
        if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())){
            if("false".equals(baInvoice.getOpenBallot())){
                if(result > 0 && !baInvoice1.getState().equals(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode())){
                    BaInvoice invoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                    //清除流程与实例关系表
                    QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("business_id",invoice.getId());
                    List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                    if(baProcessInstanceRelateds.size() > 0){
                        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                            baProcessInstanceRelated.setFlag(1);
                            baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                        }
                    }
                    //启动流程实例
                    BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(invoice, AdminCodeEnum.INVOICE_APPROVAL_CONTENT_UPDATE.getCode());
                    if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                        return 0;
                    }else {
                        invoice.setState(AdminCodeEnum.INVOICE_STATUS_VERIFYING.getCode());
                        invoice.setCreateTime(baInvoice.getUpdateTime());
                        baInvoiceMapper.updateBaInvoice(invoice);
                    }
                }
            } else {
                baInvoice.setInvoiceState("2"); //发票状态已开票
                baInvoice.setUpdateBy(SecurityUtils.getUsername());
                baInvoice.setUpdateTime(DateUtils.getNowDate());
                baInvoiceDetailMapper.deleteBaInvoiceDetailByInvoiceId(baInvoice.getId());
                //插入发票详情信息
                List<BaInvoiceDetail> baInvoiceDetailList = baInvoice.getBaInvoiceDetailList();
                if(!CollectionUtils.isEmpty(baInvoiceDetailList)){
                    for(BaInvoiceDetail baInvoiceDetail : baInvoiceDetailList){
                        baInvoiceDetail.setId(getRedisIncreID.getId());
                        baInvoiceDetail.setInvoiceId(baInvoice.getId());
                        baInvoiceDetail.setCreateBy(SecurityUtils.getUsername());
                        baInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                    }
                    baInvoiceDetailMapper.batchInsertBaInvoiceDetail(baInvoiceDetailList);
                }
                baInvoiceMapper.updateBaInvoice(baInvoice);
            }
        } else if(AdminCodeEnum.PURCHASE_INVOICE.getCode().equals(baInvoice.getInvoiceType())){
            if(result > 0 && !baInvoice1.getState().equals(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode())){
                BaInvoice invoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
                //清除流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",invoice.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(invoice, AdminCodeEnum.INPUTINVOICE_APPROVAL_CONTENT_UPDATE.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    return 0;
                }else {
                    baInvoiceDetailMapper.deleteBaInvoiceDetailByInvoiceId(baInvoice.getId());
                    //插入发票详情信息
                    List<BaInvoiceDetail> baInvoiceDetailList = baInvoice.getBaInvoiceDetailList();
                    if(!CollectionUtils.isEmpty(baInvoiceDetailList)){
                        for(BaInvoiceDetail baInvoiceDetail : baInvoiceDetailList) {
                            baInvoiceDetail.setId(getRedisIncreID.getId());
                            baInvoiceDetail.setInvoiceId(baInvoice.getId());
                            baInvoiceDetail.setCreateBy(SecurityUtils.getUsername());
                            baInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                        }
                        baInvoiceDetailMapper.batchInsertBaInvoiceDetail(baInvoiceDetailList);
                    }

                    invoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_VERIFYING.getCode());
                    invoice.setCreateTime(baInvoice.getUpdateTime());
                    baInvoiceMapper.updateBaInvoice(invoice);
                }
            }
        }
        return result;
    }

    /**
     * 批量删除发票信息列
     *
     * @param ids 需要删除的发票信息列ID
     * @return 结果
     */
    @Override
    public int deleteBaInvoiceByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaInvoice baInvoice = baInvoiceMapper.selectBaInvoiceById(id);
                baInvoice.setFlag(new Long(1));
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapperRelated = new QueryWrapper<>();
                queryWrapperRelated.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapperRelated);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                baInvoiceMapper.updateBaInvoice(baInvoice);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除发票信息列信息
     *
     * @param id 发票信息列ID
     * @return 结果
     */
    @Override
    public int deleteBaInvoiceById(String id)
    {
        return baInvoiceMapper.deleteBaInvoiceById(id);
    }

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询收付款信息
            BaInvoice baInvoice = baInvoiceMapper.selectBaInvoiceById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baInvoice.getId());
            queryWrapper.eq("flag",0);
            if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())){
                queryWrapper.ne("approve_result",AdminCodeEnum.INVOICE_STATUS_WITHDRAW.getCode());
            } else {
                queryWrapper.ne("approve_result",AdminCodeEnum.INPUTINVOICE_STATUS_WITHDRAW.getCode());
            }
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baInvoice.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //发票流程审批状态修改
                if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())) {
                    baInvoice.setState(AdminCodeEnum.INVOICE_STATUS_WITHDRAW.getCode());
                } else {
                    baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_WITHDRAW.getCode());
                }

                baInvoiceMapper.updateBaInvoice(baInvoice);
                String userId = String.valueOf(baInvoice.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())) {
                                                this.insertMessage(baInvoice, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                            } else {
                                                this.insertMessage(baInvoice, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public UR baInvoiceTotal(BaInvoice baInvoice) {
        Map<String,BigDecimal> map = new HashMap<>();
        //销项发票
        baInvoice.setProjectType("4");
        baInvoice.setInvoiceType("1");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal = baInvoiceMapper.baInvoiceTotal(baInvoice);
        map.put("常规销项发票",bigDecimal);
        baInvoice.setProjectType("2");
        BigDecimal storage = baInvoiceMapper.baInvoiceTotal(baInvoice);
        map.put("仓储销项发票",storage);
        //进项发票
        baInvoice = new BaInvoice();
        baInvoice.setProjectType("4");
        baInvoice.setInvoiceType("2");
        //租户ID
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal1 = baInvoiceMapper.baInvoiceTotal(baInvoice);
        map.put("常规进项发票",bigDecimal1);
        baInvoice.setProjectType("2");
        BigDecimal bigDecimal2 = baInvoiceMapper.baInvoiceTotal(baInvoice);
        map.put("仓储进项发票",bigDecimal2);
        return UR.ok().data("map",map);
    }

    /**
     * 纳税金额统计
     */
    @Override
    public BaInvoiceVO sumTaxAmount() {
        BaInvoice baInvoice = new BaInvoice();
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        return baInvoiceMapper.sumBaInvoiceByCon(baInvoice);
    }

    /**
     * 发票统计
     */
    @Override
    public BaInvoiceStatVO sumInvoiceAmount(BaInvoice baInvoice) {
        //进项发票
        baInvoice.setTenantId(SecurityUtils.getCurrComId());
        if("2".equals(baInvoice.getInvoiceType())){
            baInvoice.setState(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode());
        } else {
            baInvoice.setState(AdminCodeEnum.INVOICE_STATUS_PASS.getCode());
        }
        BaInvoiceStatVO baInvoiceStatVO = new BaInvoiceStatVO();
        //发票数量
        baInvoiceStatVO.setCount(baInvoiceMapper.countBaInvoice(baInvoice));
        //含税金额
        baInvoiceStatVO.setTaxPriceTotal(baInvoiceMapper.sumTaxPriceTotal(baInvoice));
        return baInvoiceStatVO;
    }

    @Override
    public NumsAndMoneyDTO selectBaInvoiceForUp(BaInvoice baInvoice) {
        return baInvoiceMapper.selectBaInvoiceForUp(baInvoice);
    }

    @Override
    public NumsAndMoneyDTO selectBaInvoiceForMonth(BaInvoice baInvoice) {
        return baInvoiceMapper.selectBaInvoiceForMonth(baInvoice);
    }

    @Override
    public NumsAndMoneyDTO selectBaInvoiceForYear(BaInvoice baInvoice) {
        return baInvoiceMapper.selectBaInvoiceForYear(baInvoice);
    }

    @Override
    public List<BaInvoice> selectBaInvoiceAuthorizedList(BaInvoice baInvoice) {
        List<BaInvoice> baInvoices = baInvoiceMapper.selectBaInvoiceAuthorizedList(baInvoice);
        baInvoices.stream().forEach(item -> {
            //封装合同启动
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
            if(!ObjectUtils.isEmpty(baContractStart)){
                item.setBaContractStart(baContractStart);
            }
            //售票单位名称
            if(StringUtils.isNotEmpty(item.getBuyer())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getBuyer());
                if(StringUtils.isNotNull(enterprise)){
                    item.setBuyerName(enterprise.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getBuyer());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setBuyerName(baSupplier.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getBuyer());
                if(StringUtils.isNotNull(baCompany)){
                    item.setBuyerName(baCompany.getName());
                }
            }
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位信息
                String name = getName.getName(item.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                }

            }
            //查询税价合计
            BaInvoiceDetail baInvoiceDetail = new BaInvoiceDetail();
            baInvoiceDetail.setInvoiceId(item.getId());
            List<BaInvoiceDetail> baInvoiceDetailList = baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
            BigDecimal bigDecimal=new BigDecimal(0.0000);
            for(BaInvoiceDetail baInvoiceDetail1:baInvoiceDetailList){
                if(baInvoiceDetail1.getTaxPriceTotal()!=null){
                    bigDecimal=bigDecimal.add(baInvoiceDetail1.getTaxPriceTotal());
                }
            }
            item.setTaxPriceTotal(bigDecimal);
            //采购单名称
            if(StringUtils.isNotEmpty(item.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(item.getPlanId());
                item.setPlanName(baProcurementPlan.getPlanName());
            }
            //仓储立项名称
            if(StringUtils.isNotEmpty(item.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(item.getProjectId());
                item.setProjectName(baProject.getName());
            }
            //销售订单
            if(StringUtils.isNotEmpty(item.getOrderId())){
                BaContractStart baContractStart1 = baContractStartMapper.selectBaContractStartById(item.getOrderId());
                item.setOrderName(baContractStart1.getName());
            }
        });
        return baInvoices;
    }

    @Override
    public int authorizedBaInvoice(BaInvoice baInvoice) {
        return baInvoiceMapper.updateBaInvoice(baInvoice);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaInvoice baInvoice, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baInvoice.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        if(AdminCodeEnum.SALE_INVOICE.getCode().equals(baInvoice.getInvoiceType())) {
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode());
        } else {
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }
}
