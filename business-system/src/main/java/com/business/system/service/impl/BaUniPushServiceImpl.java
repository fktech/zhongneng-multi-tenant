package com.business.system.service.impl;

import com.business.system.service.IUniPushService;
import com.getui.push.v2.sdk.ApiHelper;
import com.getui.push.v2.sdk.GtApiConfiguration;
import com.getui.push.v2.sdk.api.PushApi;
import com.getui.push.v2.sdk.common.ApiResult;
import com.getui.push.v2.sdk.dto.req.Audience;
import com.getui.push.v2.sdk.dto.req.message.PushChannel;
import com.getui.push.v2.sdk.dto.req.message.PushDTO;
import com.getui.push.v2.sdk.dto.req.message.PushMessage;
import com.getui.push.v2.sdk.dto.req.message.android.AndroidDTO;
import com.getui.push.v2.sdk.dto.req.message.android.GTNotification;
import com.getui.push.v2.sdk.dto.req.message.android.ThirdNotification;
import com.getui.push.v2.sdk.dto.req.message.android.Ups;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 离线消息推送Service业务层处理
 *
 * @author single
 * @date 2024-8-28
 */
@Slf4j
@Service
public class BaUniPushServiceImpl implements IUniPushService{

    @Resource
    private GtApiConfiguration gtApiConfiguration;

    @Override
    public boolean pushMessage(String cid, String title, String body, String payload) {

        PushApi pushApi = ApiHelper.build(gtApiConfiguration).creatApi(PushApi.class);
        //根据cid进行单推
        PushDTO<Audience> pushDTO = new PushDTO<>();
        // 设置推送参数
        pushDTO.setRequestId(System.currentTimeMillis() + "");
        PushMessage pushMessage = new PushMessage();
        pushDTO.setPushMessage(pushMessage);
        GTNotification notification = new GTNotification();
        pushMessage.setNotification(notification);
        notification.setTitle(title);
        notification.setBody(body);
        notification.setClickType("payload");
        notification.setPayload(payload);
        notification.setChannelLevel("4");
        notification.setCategory("CATEGORY_REMINDER");
        //设置厂商推送消息参数
        PushChannel pushChannel = new PushChannel();
        pushDTO.setPushChannel(pushChannel);
        AndroidDTO androidDTO = new AndroidDTO();
        pushChannel.setAndroid(androidDTO);
        Ups ups = new Ups();
        androidDTO.setUps(ups);
        ThirdNotification thirdNotification = new ThirdNotification();
        ups.setNotification(thirdNotification);
        thirdNotification.setTitle(title);
        thirdNotification.setBody(body);
        thirdNotification.setPayload(payload);
        thirdNotification.setClickType("intent");
        thirdNotification.setIntent("intent://io.dcloud.unipush/?#Intent;scheme=unipush;launchFlags=0x4000000;component=uni.UNI0866B69/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title=" + title + ";S.content=" + body + ";S.payload=" + payload + ";end");
        // 设置接收人信息
        Audience audience = new Audience();
        pushDTO.setAudience(audience);
        audience.addCid(cid);
        // 进行cid单推
        ApiResult<Map<String, Map<String, String>>> apiResult = pushApi.pushToSingleByCid(pushDTO);
        if (apiResult.isSuccess()) {
            // success
            log.info("发送信息成功" + apiResult);
            return true;
        } else {
            // failed
            log.info("发送信息失败" + apiResult);
            return false;
        }
    }
}
