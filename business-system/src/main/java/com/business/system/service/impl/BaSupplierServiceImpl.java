package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.IBaSupplierService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.*;

/**
 * 供应商Service业务层处理
 *
 * @author ljb
 * @date 2022-11-28
 */
@Service
public class BaSupplierServiceImpl extends ServiceImpl<BaSupplierMapper, BaSupplier> implements IBaSupplierService
{
    private static final Logger log = LoggerFactory.getLogger(BaSupplierServiceImpl.class);
    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    /**
     * 查询供应商
     *
     * @param id 供应商ID
     * @return 供应商
     */
    @Override
    public BaSupplier selectBaSupplierById(String id)
    {
        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(id);
        //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baSupplier.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.isNull("state");
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        baSupplier.setBaBankList(baBanks);
        //基本账户信息
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baSupplier.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.eq("state","1");
        BaBank baBank = baBankMapper.selectOne(queryWrapper);
        baSupplier.setBaBank(baBank);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baSupplier.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baSupplier.setProcessInstanceId(processInstanceIds);
        //开票信息
        QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
        informationQueryWrapper.eq("relevance_id",id);
        informationQueryWrapper.orderByDesc(id);
        List<BaBillingInformation> baBillingInformation = baBillingInformationMapper.selectList(informationQueryWrapper);
        if(baBillingInformation.size() > 0){
            baSupplier.setBillingInformation(baBillingInformation.get(0));
        }
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baSupplier.getUserId());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }

        baSupplier.setUser(sysUser);
        return baSupplier;
    }

    /**
     * 查询供应商列表
     *
     * @param baSupplier 供应商
     * @return 供应商
     */
    @Override
    public List<BaSupplier> selectBaSupplierList(BaSupplier baSupplier)
    {
        List<BaSupplier> baSuppliers = baSupplierMapper.selectBaSupplierList(baSupplier);
        for (BaSupplier supplier:baSuppliers) {
            SysUser sysUser = sysUserMapper.selectUserById(supplier.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位名称
                String name = getName.getName(sysUser.getUserId());
                //发起人
                if(name.equals("") == false){
                    supplier.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    supplier.setUserName(sysUser.getNickName());
                }

            }
        }
        return baSuppliers;
    }

    /**
     * 新增供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBaSupplier(BaSupplier baSupplier)
    {
        //判断公司简称不能重复
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("company_abbreviation",baSupplier.getCompanyAbbreviation());
        supplierQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaSupplier selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -1;
        }
        //判断名称不能重复
        supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("name",baSupplier.getName());
        supplierQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -2;
        }
        baSupplier.setId(getRedisIncreID.getId());
        baSupplier.setCreateTime(DateUtils.getNowDate());
        baSupplier.setCreateBy(SecurityUtils.getUsername());
        baSupplier.setUserId(SecurityUtils.getUserId());
        baSupplier.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baSupplier.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baSupplier.getWorkState())) {
            //流程发起状态
            baSupplier.setWorkState("1");
            //默认审批通过
            baSupplier.setState(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        }
        List<BaBank> baBankList = baSupplier.getBaBankList();
        //基本账户
        if(StringUtils.isNotNull(baSupplier.getBaBank())){
            BaBank baBank = baSupplier.getBaBank();
            baBank.setId(getRedisIncreID.getId());
            baBank.setRelationId(baSupplier.getId());
            baBank.setCreateTime(DateUtils.getNowDate());
            baBank.setCreateBy(SecurityUtils.getUsername());
            baBank.setState("1");
            baBankMapper.insertBaBank(baBank);
        }
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                item.setId(getRedisIncreID.getId());
                item.setRelationId(baSupplier.getId());
                item.setCreateTime(DateUtils.getNowDate());
                item.setCreateBy(SecurityUtils.getUsername());
                baBankMapper.insertBaBank(item);
            });
        }
        //新增开票信息
        if(StringUtils.isNotNull(baSupplier.getBillingInformation())){
            BaBillingInformation baBillingInformation = baSupplier.getBillingInformation();
            baBillingInformation.setId(getRedisIncreID.getId());
            baBillingInformation.setRelevanceId(baSupplier.getId());
            baBillingInformationMapper.insertBaBillingInformation(baBillingInformation);
        }
        //流程ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(),SecurityUtils.getCurrComId());
        baSupplier.setFlowId(flowId);
        int result = baSupplierMapper.insertBaSupplier(baSupplier);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baSupplier.getWorkState())) {
                BaSupplier supplier = baSupplierMapper.selectBaSupplierById(baSupplier.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(supplier, AdminCodeEnum.SUPPLIER_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baSupplierMapper.deleteBaSupplierById(supplier.getId());
                    return 0;
                } else {
                    supplier.setState(AdminCodeEnum.SUPPLIER_STATUS_VERIFYING.getCode());
                    baSupplierMapper.updateBaSupplier(supplier);
                }
            }
        }
        return result;
    }

    /**
     * 提交供应商审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaSupplier supplier, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(supplier.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(supplier.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        //获取流程实例关联的业务对象
        /*BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplier.getId());*/

        /*baSupplier.setUserName(sysUser.getUserName());
        baSupplier.setDeptName(sysUser.getDept().getDeptName());*/
        BaSupplier baSupplier = this.selectBaSupplierById(supplier.getId());
        SysUser sysUser = iSysUserService.selectUserById(baSupplier.getUserId());
        baSupplier.setUserName(sysUser.getUserName());
        baSupplier.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baSupplier, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.SUPPLIER_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    @Override
    public int updateBaSupplier(BaSupplier baSupplier)
    {
        //获取原供应商信息
        BaSupplier baSupplier1 = baSupplierMapper.selectBaSupplierById(baSupplier.getId());
        if(baSupplier1.getCompanyAbbreviation().equals(baSupplier.getCompanyAbbreviation()) == false){
            QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
            supplierQueryWrapper.eq("company_abbreviation",baSupplier.getCompanyAbbreviation());
            supplierQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                supplierQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
            }
            BaSupplier selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        //名称不能重复
        if(baSupplier1.getName().equals(baSupplier.getName()) == false){
            QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
            supplierQueryWrapper.eq("name",baSupplier.getName());
            supplierQueryWrapper.eq("flag",0);
            //租户ID
            if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                supplierQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
            }
            BaSupplier selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
        baSupplier.setUpdateTime(DateUtils.getNowDate());
        baSupplier.setUpdateBy(SecurityUtils.getUsername());
        //创建时间
        baSupplier.setCreateTime(baSupplier.getUpdateTime());
        List<BaBank> baBankList = baSupplier.getBaBankList();
        //基本账户
        if(StringUtils.isNotNull(baSupplier.getBaBank())){
            BaBank baBank = baSupplier.getBaBank();
            baBank.setUpdateTime(DateUtils.getNowDate());
            baBank.setUpdateBy(SecurityUtils.getUsername());
            baBankMapper.updateBaBank(baBank);
        }
        //收款账户
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }
        //修改开票信息
        if(StringUtils.isNotNull(baSupplier.getBillingInformation())){
            BaBillingInformation billingInformation = baSupplier.getBillingInformation();
            if(StringUtils.isNotEmpty(billingInformation.getId())){
                baBillingInformationMapper.updateBaBillingInformation(baSupplier.getBillingInformation());
            }else {
                billingInformation.setId(getRedisIncreID.getId());
                billingInformation.setRelevanceId(baSupplier.getId());
                baBillingInformationMapper.insertBaBillingInformation(billingInformation);
            }
        }
        int result = baSupplierMapper.updateBaSupplier(baSupplier);
        if(result > 0 && baSupplier1.getState().equals(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode()) == false){
            BaSupplier supplier = baSupplierMapper.selectBaSupplierById(baSupplier.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",supplier.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(supplier, AdminCodeEnum.SUPPLIER_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baSupplierMapper.updateBaSupplier(baSupplier1);
                return 0;
            }else {
                supplier.setState(AdminCodeEnum.SUPPLIER_STATUS_VERIFYING.getCode());
                baSupplierMapper.updateBaSupplier(supplier);
            }
        }
        return result;
    }

    @Override
    public int updateSupplier(BaSupplier baSupplier) {
        List<BaBank> baBankList = baSupplier.getBaBankList();
        //收款账户
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getId())){
                    item.setUpdateTime(DateUtils.getNowDate());
                    item.setUpdateBy(SecurityUtils.getUsername());
                    baBankMapper.updateBaBank(item);
                }else {
                    item.setId(getRedisIncreID.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baBankMapper.insertBaBank(item);
                }
            });
        }
        return baSupplierMapper.updateBaSupplier(baSupplier);
    }

    /**
     * 批量删除供应商
     *
     * @param ids 需要删除的供应商ID
     * @return 结果
     */
    @Override
    public int deleteBaSupplierByIds(String[] ids)
    {
        return baSupplierMapper.deleteBaSupplierByIds(ids);
    }


    /**
     * 删除供应商信息
     *
     * @param ids 供应商ID
     * @return 结果
     */
    @Override
    public int deleteBaSupplierById(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(id);
                baSupplier.setFlag(1);
                baSupplierMapper.updateBaSupplier(baSupplier);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 500;
        }
        return 200;
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baSupplier.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.SUPPLIER_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baSupplier.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baSupplier.setState(AdminCodeEnum.SUPPLIER_STATUS_WITHDRAW.getCode());
                baSupplierMapper.updateBaSupplier(baSupplier);
                String userId = String.valueOf(baSupplier.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baSupplier, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int updateProcessBusinessData(BaSupplierInstanceRelatedEditDTO baSupplierInstanceRelatedEditDTO) {
        BaSupplier baSupplier = baSupplierInstanceRelatedEditDTO.getBaSupplier();
        if (!ObjectUtils.isEmpty(baSupplierInstanceRelatedEditDTO.getBaSupplier())) {
            //获取原供应商信息
            BaSupplier baSupplier1 = baSupplierMapper.selectBaSupplierById(baSupplier.getId());
            if (StringUtils.isNotEmpty(baSupplier1.getCompanyAbbreviation()) && baSupplier1.getCompanyAbbreviation().equals(baSupplier.getCompanyAbbreviation())) {
                QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
                supplierQueryWrapper.eq("company_abbreviation", baSupplier.getCompanyAbbreviation());
                supplierQueryWrapper.eq("flag", 0);
                supplierQueryWrapper.ne("id", baSupplier1.getId());
                BaSupplier selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
                if (StringUtils.isNotNull(selectOne)) {
                    return -1;
                }
            }
            //名称不能重复
            if(baSupplier1.getName().equals(baSupplier.getName()) == false){
                QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
                supplierQueryWrapper.eq("name",baSupplier.getName());
                supplierQueryWrapper.eq("flag",0);
                BaSupplier selectOne = baSupplierMapper.selectOne(supplierQueryWrapper);
                if(StringUtils.isNotNull(selectOne)){
                    return -2;
                }
            }

            baSupplier.setUpdateTime(DateUtils.getNowDate());
            baSupplier.setUpdateBy(SecurityUtils.getUsername());
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(baSupplierInstanceRelatedEditDTO.getProcessInstanceId());
            baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(baSupplier));
            try {
                baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
            } catch (Exception e) {
                return 0;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public List<BaSupplier> baSupplierList() {

        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("state","supplier:pass");
        supplierQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaSupplier> baSuppliers = baSupplierMapper.selectList(supplierQueryWrapper);

        return baSuppliers;
    }

    @Override
    public List<DropDownBox> dropDownBox(String name) {
        List<DropDownBox> list = new ArrayList<>();
        //供应商
        BaSupplier baSupplier = new BaSupplier();
        baSupplier.setState(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baSupplier.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baSupplier.setName(name);
        }
        List<BaSupplier> baSuppliers = baSupplierMapper.selectBaSupplierList(baSupplier);
        for (BaSupplier supplier:baSuppliers) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(supplier.getId());
            dropDownBox.setName(supplier.getName());
            list.add(dropDownBox);
        }
        //装卸服务公司
        BaCompany baCompany = new BaCompany();
        baCompany.setCompanyType("装卸服务公司");
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baCompany.setTenantId(SecurityUtils.getCurrComId());
        }
        //名称筛选
        if(StringUtils.isNotEmpty(name)){
            baCompany.setName(name);
        }
        List<BaCompany> baCompanyList = baCompanyMapper.selectBaCompanyList(baCompany);
        for (BaCompany company:baCompanyList) {
            DropDownBox dropDownBox = new DropDownBox();
            dropDownBox.setId(company.getId());
            dropDownBox.setName(company.getName());
            list.add(dropDownBox);
        }
        return list;
    }

    @Override
    public UR statTraderInfo() {
        Map<String, Object> statTraderMap = new HashMap<>();
        //供应商总数
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        supplierQueryWrapper.eq("state","supplier:pass");
        supplierQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        statTraderMap.put("supplierAllCount", baSupplierMapper.selectCount(supplierQueryWrapper));
        //供应商本月新增
        statTraderMap.put("supplierThisMonth", baSupplierMapper.thisMonth(SecurityUtils.getCurrComId()));

        //托盘公司总数
        QueryWrapper<BaCompany> palletQueryWrapper = new QueryWrapper<>();
        palletQueryWrapper.eq("state","company:pass");
        palletQueryWrapper.eq("flag",0);
        palletQueryWrapper.eq("company_type","托盘公司");
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            palletQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        statTraderMap.put("palletAllCount", baCompanyMapper.selectCount(palletQueryWrapper));
        //托盘公司本月新增
        statTraderMap.put("palletThisMonth", baCompanyMapper.thisMonth("托盘公司", SecurityUtils.getCurrComId()));
        //终端企业总数
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        enterpriseQueryWrapper.eq("state","enterprise:pass");
        enterpriseQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        statTraderMap.put("enterpriseAllCount", baEnterpriseMapper.selectCount(enterpriseQueryWrapper));
        //终端企业本月新增
        statTraderMap.put("enterpriseThisMonth", baEnterpriseMapper.thisMonth(SecurityUtils.getCurrComId()));

        //其他单位总数
        QueryWrapper<BaCompany> otherCompanyQueryWrapper = new QueryWrapper<>();
        otherCompanyQueryWrapper.eq("state","company:pass");
        otherCompanyQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            otherCompanyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        statTraderMap.put("otherCompanyAllCount", baCompanyMapper.selectCount(otherCompanyQueryWrapper));
        //其他单位本月新增
        statTraderMap.put("otherCompanyThisMonth", baCompanyMapper.otherCompanyThisMonth("", SecurityUtils.getCurrComId()));
        return UR.ok().code(200).data(statTraderMap);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSupplier baSupplier, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSupplier.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

}
