package com.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import com.business.system.service.IBaContractStartService;
import com.business.system.service.IBaHiTransportService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 运输Service业务层处理
 *
 * @author ljb
 * @date 2022-12-13
 */
@Service
public class BaHiTransportServiceImpl extends ServiceImpl<BaHiTransportMapper, BaHiTransport> implements IBaHiTransportService
{
    private static final Logger log = LoggerFactory.getLogger(BaHiTransportServiceImpl.class);

    @Autowired
    private BaHiTransportMapper baHiTransportMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private IBaContractStartService iBaContractStartService;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    @Override
    public BaHiTransport selectBaHiTransportById(String id)
    {
        BaHiTransport transport = baHiTransportMapper.selectBaHiTransportById(id);
        if (!ObjectUtils.isEmpty(transport)) {
            //合同启动名称
            if (StringUtils.isNotEmpty(transport.getStartId())) {
                BaContractStart contractStart = iBaContractStartService.selectBaContractStartById(transport.getStartId());
                transport.setStartName(contractStart.getName());
                transport.setEnterpriseName(contractStart.getEnterpriseName());
                //合同启动简称
                transport.setStartShorter(contractStart.getStartShorter());
                //供应商名称
                transport.setSupplierName(contractStart.getSupplierName());
            }
            //订单编号
            if (StringUtils.isNotEmpty(transport.getOrderId())) {
                BaOrder baOrder = baOrderMapper.selectBaOrderById(transport.getOrderId());
                transport.setOrderNum(baOrder.getOrderName());
                transport.setReceiveId(baOrder.getEnterpriseId());
            }
            //始发站
            if (StringUtils.isNotEmpty(transport.getOrigin())) {
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getOrigin());
                if (StringUtils.isNotNull(baRailwayPlatform)) {
                    transport.setOriginName(baRailwayPlatform.getName());
                } else {
                    transport.setOriginName(transport.getOrigin());
                }
            }
            //收货单位
            if (StringUtils.isNotEmpty(transport.getReceiveCompany())) {
                transport.setReceiveId(transport.getReceiveCompany());
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(transport.getReceiveCompany());
                transport.setReceiveCompany(enterprise.getName());

            }
            //目的地
            if (StringUtils.isNotEmpty(transport.getDestination())) {
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getDestination());
                if (StringUtils.isNotNull(baRailwayPlatform)) {
                    transport.setDestinationName(baRailwayPlatform.getName());
                } else {
                    transport.setDestinationName(transport.getDestination());
                }
            }
            //实控人
            if (StringUtils.isNotEmpty(transport.getActualController())) {
                BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(transport.getActualController());
                transport.setActualName(baCounterpart.getName());
            }
            //商品名称
            if (StringUtils.isNotEmpty(transport.getGoodsId())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(transport.getGoodsId());
                transport.setGoodsName(baGoods.getName());
            }
            //托运公司名称
            if (StringUtils.isNotEmpty(transport.getTransportCompany())) {
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(transport.getTransportCompany());
                transport.setTransportCompanyName(baCompany.getName());
            }

            /*if(StringUtils.isNotEmpty(transport.getTransportStatus())){*/
            //发货信息
            QueryWrapper<BaCheck> queryWrapper0 = new QueryWrapper<>();
            queryWrapper0.eq("relation_id", transport.getId());
            queryWrapper0.eq("type", "1");
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper0);
            transport.setDelivery(baCheck);
            //验收信息
            /*if(transport.getTransportStatus().equals("2") || transport.getTransportStatus().equals("3")){*/
            QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id", transport.getId());
            queryWrapper1.eq("type", "2");
            BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper1);
            transport.setReceipt(baCheck1);
            /*}*/
            /*}*/
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(transport.getUserId());
            sysUser.setUserName(sysUser.getNickName());
            //岗位名称
            String name = getName.getName(sysUser.getUserId());
            if (name.equals("") == false) {
                sysUser.setPostName(name.substring(0, name.length() - 1));
            }
            transport.setUser(sysUser);

            List<String> processInstanceIds = new ArrayList<>();
            //流程实例ID
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", transport.getId());
            queryWrapper.eq("flag", 0);
            //queryWrapper.ne("approve_result",AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode());
            queryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            transport.setProcessInstanceId(processInstanceIds);
            //运单信息
            if (StringUtils.isNotEmpty(transport.getAutomobileType())) {
                if (transport.getAutomobileType().equals("2")) {
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(transport.getAutomobileId());
                    //查看中储摘单
                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                    baShippingOrderCmst1.setYardId(transportCmst.getYardld());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    transport.setBaShippingOrderCmsts(baShippingOrderCmsts);
                }
                if (transport.getAutomobileType().equals("1")) {
                    //查看昕科运单
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(transport.getAutomobileId());
           /* QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
            shippingOrderQueryWrapper.eq("supplier_no",baTransportAutomobile.getSupplierNo());
            List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
            for (BaShippingOrder baShippingOrder:baShippingOrders) {
                //查询付款明细
                QueryWrapper<BaAutomobileSettlementDetail> SettlementQueryWrapper =new QueryWrapper<>();
                queryWrapper.eq("external_order",baShippingOrder.getId());
                List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(SettlementQueryWrapper);
                for (BaAutomobileSettlementDetail baAutomobileSettlementDetail:baAutomobileSettlementDetails) {
                    if(baAutomobileSettlementDetail.getState().equals("2")){
                        baShippingOrder.setPaymentStatus("1");
                    }
                }
                //开票状态
                if(StringUtils.isNotEmpty(baShippingOrder.getThdno())){
                    BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceByexternalorder(baShippingOrder.getThdno());
                    if(StringUtils.isNotNull(baAutomobileSettlementInvoice)){
                        baShippingOrder.setCheckFlag(baAutomobileSettlementInvoice.getCheckFlag());
                    }

                }
            }*/
                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.waybillList(baTransportAutomobile.getSupplierNo());
                    for (BaShippingOrder baShippingOrder : shippingOrders) {
                        //查询付款明细
                        QueryWrapper<BaAutomobileSettlementDetail> SettlementQueryWrapper = new QueryWrapper<>();
                        SettlementQueryWrapper.eq("external_order", baShippingOrder.getId());
                        SettlementQueryWrapper.eq("state", "2");
                        List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(SettlementQueryWrapper);
                        if (baAutomobileSettlementDetails.size() > 0) {
                            baShippingOrder.setPaymentStatus("1");
                        }
                    }
                    transport.setBaShippingOrders(shippingOrders);
                }
            }
        }
        return transport;
    }

    @Override
    public List<BaHiTransport> selectChangeDataBaHiTransportList(BaHiTransport baHiTransport) {
        //查询变更记录
        baHiTransport.setTenantId(SecurityUtils.getCurrComId());
        List<BaHiTransport> baHiTransports = baHiTransportMapper.selectChangeBaHiTransportList(baHiTransport);
        if(!CollectionUtils.isEmpty(baHiTransports)){
            baHiTransports.stream().forEach(item -> {
                if(StringUtils.isNotEmpty(item.getStartId())){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
                    if(!ObjectUtils.isEmpty(baContractStart)){
                        item.setStartName(baContractStart.getName());
                    }
                }
                SysUser sysUser = sysUserMapper.selectUserByUserName(item.getChangeBy());
                if(!ObjectUtils.isEmpty(sysUser)){
                    //岗位名称
                    String name = getName.getName(sysUser.getUserId());
                    if(StringUtils.isNotEmpty(name)){
                        sysUser.setPostName(name.substring(0,name.length()-1));
                    }
                    item.setChangeUser(sysUser);
                }
            });
        }
        return baHiTransports;
    }
}
