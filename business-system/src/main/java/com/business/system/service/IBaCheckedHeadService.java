package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaCheckedHead;

/**
 * 盘点Service接口
 *
 * @author single
 * @date 2023-01-05
 */
public interface IBaCheckedHeadService
{
    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    public BaCheckedHead selectBaCheckedHeadById(String id);

    /**
     * 查询盘点列表
     *
     * @param baCheckedHead 盘点
     * @return 盘点集合
     */
    public List<BaCheckedHead> selectBaCheckedHeadList(BaCheckedHead baCheckedHead);

    /**
     * 新增盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    public int insertBaCheckedHead(BaCheckedHead baCheckedHead);

    /**
     * 修改盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    public int updateBaCheckedHead(BaCheckedHead baCheckedHead);

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的盘点ID
     * @return 结果
     */
    public int deleteBaCheckedHeadByIds(String[] ids);

    /**
     * 删除盘点信息
     *
     * @param id 盘点ID
     * @return 结果
     */
    public int deleteBaCheckedHeadById(String id);

    /**
     * 查询列表
     * @param baCheckedHead
     * @return
     */
    List<BaCheckedHead> selectBaCheckedHeadListByCheckId(BaCheckedHead baCheckedHead);

    /**
     * 查询盘点信息
     * @param baCheckedHead
     * @return
     */
    BaCheckedHead selectBaCheckedHead(BaCheckedHead baCheckedHead);

    /**
     * 根据盘点ID删除盘点详情
     * @param checkedId
     * @return
     */
    int deleteBaCheckedHeadByCheckedId(String checkedId);

    /**
     * 统计库存盘点
     */
    int countBaCheckedHead(BaCheckedHead baCheckedHead);
}
