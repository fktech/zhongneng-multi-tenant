package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaGlobalNumberMapper;

/**
 * 全局编号Service业务层处理
 *
 * @author ljb
 * @date 2024-06-21
 */
@Service
public class BaGlobalNumberServiceImpl extends ServiceImpl<BaGlobalNumberMapper, BaGlobalNumber> implements IBaGlobalNumberService
{
    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private IBaProjectService baProjectService;

    @Autowired
    private IBaContractStartService baContractStartService;

    @Autowired
    private IBaTransportService baTransportService;

    @Autowired
    private IBaSettlementService baSettlementService;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private IBaCollectionService baCollectionService;

    @Autowired
    private IBaClaimService baClaimService;

    @Autowired
    private IBaClaimHistoryService baClaimHistoryService;

    @Autowired
    private IBaInvoiceService baInvoiceService;

    @Autowired
    private IBaContractService baContractService;

    /**
     * 查询全局编号
     *
     * @param id 全局编号ID
     * @return 全局编号
     */
    @Override
    public BaGlobalNumber selectBaGlobalNumberById(String id)
    {
        return baGlobalNumberMapper.selectBaGlobalNumberById(id);
    }

    /**
     * 查询全局编号列表
     *
     * @param baGlobalNumber 全局编号
     * @return 全局编号
     */
    @Override
    public List<BaGlobalNumber> selectBaGlobalNumberList(BaGlobalNumber baGlobalNumber)
    {
        List<BaGlobalNumber> baGlobalNumbers = baGlobalNumberMapper.selectBaGlobalNumberList(baGlobalNumber);
        if(baGlobalNumbers.size() > 0) {
            BaGlobalNumber globalNumber = baGlobalNumbers.get(0);
            if (StringUtils.isNotEmpty(globalNumber.getHierarchy())) {
                if (globalNumber.getHierarchy().equals("1")) {
                    //立项信息
                    BaProject baProject = baProjectService.selectBaProjectById(globalNumber.getBusinessId());
                    globalNumber.setBaProject(baProject);
                    //合同启动
                    BaContractStart baContractStart = new BaContractStart();
                    baContractStart.setProjectId(baProject.getId());
                    baContractStart.setState(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
                    List<BaContractStart> contractStarts = baContractStartService.selectBaContractStartList(baContractStart);
                    globalNumber.setBaContractStarts(contractStarts);
                } else if (globalNumber.getHierarchy().equals("2")) {
                    List<BaContractStart> list = new ArrayList<>();
                    //合同启动信息
                    BaContractStart baContractStart = baContractStartService.selectBaContractStartById(globalNumber.getBusinessId());
                    list.add(baContractStart);
                    globalNumber.setBaContractStarts(list);
                    //立项信息
                    BaProject baProject = baProjectService.selectBaProjectById(baContractStart.getProjectId());
                    globalNumber.setBaProject(baProject);
                    baGlobalNumber = new BaGlobalNumber();
                    baGlobalNumber.setStartId(baContractStart.getId());
                    BaGlobalNumber splicing = this.splicing(baGlobalNumber);
                    //合同
                    globalNumber.setBaContract(splicing.getBaContract());
                    //运输
                    globalNumber.setBaTransports(splicing.getBaTransports());
                    //结算
                    globalNumber.setBaSettlements(splicing.getBaSettlements());
                    //付款
                    globalNumber.setBaPayments(splicing.getBaPayments());
                    //收款
                    globalNumber.setBaCollections(splicing.getBaCollections());
                    //认领收款
                    globalNumber.setBaClaimHistories(splicing.getBaClaimHistories());
                    //发票
                    globalNumber.setBaInvoices(splicing.getBaInvoices());
                }else if(globalNumber.getHierarchy().equals("3")){
                    List<BaContractStart> list = new ArrayList<>();
                    //合同启动信息
                    BaContractStart baContractStart = baContractStartService.selectBaContractStartById(globalNumber.getStartId());
                    list.add(baContractStart);
                    globalNumber.setBaContractStarts(list);
                    //立项信息
                    BaProject baProject = baProjectService.selectBaProjectById(baContractStart.getProjectId());
                    globalNumber.setBaProject(baProject);
                    baGlobalNumber = new BaGlobalNumber();
                    baGlobalNumber.setStartId(baContractStart.getId());
                    BaGlobalNumber splicing = this.splicing(baGlobalNumber);
                    //合同
                    globalNumber.setBaContract(splicing.getBaContract());
                    //运输
                    globalNumber.setBaTransports(splicing.getBaTransports());
                    //结算
                    globalNumber.setBaSettlements(splicing.getBaSettlements());
                    //付款
                    globalNumber.setBaPayments(splicing.getBaPayments());
                    //收款
                    globalNumber.setBaCollections(splicing.getBaCollections());
                    //认领收款
                    globalNumber.setBaClaimHistories(splicing.getBaClaimHistories());
                    //发票
                    globalNumber.setBaInvoices(splicing.getBaInvoices());
                }
            }
        }
        return baGlobalNumbers;
    }

    public BaGlobalNumber splicing(BaGlobalNumber baGlobalNumber){
        BaGlobalNumber globalNumber = new BaGlobalNumber();
        //合同启动向下所有关联数据
        //合同
        List<BaContract> contracts = new ArrayList<>();
        //发运
        List<BaTransport> transports = new ArrayList<>();
        //结算
        List<BaSettlement> settlements = new ArrayList<>();
        //收款
        List<BaCollection> collections = new ArrayList<>();
        //认领收款
        List<BaClaimHistory> claimHistories = new ArrayList<>();
        //付款
        List<BaPayment> payments = new ArrayList<>();
        //发票
        List<BaInvoice> invoices = new ArrayList<>();
        List<BaGlobalNumber> globalNumbers = baGlobalNumberMapper.selectBaGlobalNumberList(baGlobalNumber);
        for (BaGlobalNumber baGlobalNumber1 : globalNumbers) {
            //合同
            if(baGlobalNumber1.getType().equals("3")){
                BaContract baContract = baContractService.selectBaContractById(baGlobalNumber1.getBusinessId());
                contracts.add(baContract);
            }
            //发运
            if (baGlobalNumber1.getType().equals("4")) {
                BaTransport baTransport = baTransportService.selectBaTransportById(baGlobalNumber1.getBusinessId());
                transports.add(baTransport);
            }
            //结算
            if(baGlobalNumber1.getType().equals("5")){
                BaSettlement baSettlement = baSettlementService.selectBaSettlementById(baGlobalNumber1.getBusinessId());
                settlements.add(baSettlement);
            }
            //付款
            if(baGlobalNumber1.getType().equals("6")){
                BaPayment baPayment = baPaymentService.selectBaPaymentById(baGlobalNumber1.getBusinessId());
                payments.add(baPayment);
            }
            //收款
            if(baGlobalNumber1.getType().equals("7")){
                BaCollection baCollection = baCollectionService.selectBaCollectionById(baGlobalNumber1.getBusinessId());
                collections.add(baCollection);
            }
            //认领收款
            if(baGlobalNumber1.getType().equals("8")){
                BaClaimHistory baClaimHistory = baClaimHistoryService.selectBaClaimHistoryById(baGlobalNumber1.getBusinessId());
                //上级认领信息
                if(StringUtils.isNotEmpty(baClaimHistory.getClaimId())){
                    BaClaim baClaim = baClaimService.selectBaClaimById(baClaimHistory.getClaimId());
                    if(StringUtils.isNotNull(baClaim)){
                        baClaimHistory.setClaimState(baClaim.getClaimState());
                    }
                }
                claimHistories.add(baClaimHistory);
            }
            //进项发票
            if(baGlobalNumber1.getType().equals("9")){
                BaInvoice baInvoice = baInvoiceService.selectBaInvoiceById(baGlobalNumber1.getBusinessId());
                invoices.add(baInvoice);
            }
            //销项发票
            if(baGlobalNumber1.getType().equals("10")){
                BaInvoice baInvoice = baInvoiceService.selectBaInvoiceById(baGlobalNumber1.getBusinessId());
                invoices.add(baInvoice);
            }
        }
        globalNumber.setBaContract(contracts);
        globalNumber.setBaTransports(transports);
        globalNumber.setBaSettlements(settlements);
        globalNumber.setBaPayments(payments);
        globalNumber.setBaCollections(collections);
        globalNumber.setBaClaimHistories(claimHistories);
        globalNumber.setBaInvoices(invoices);
        return globalNumber;
    }


    /**
     * 新增全局编号
     *
     * @param baGlobalNumber 全局编号
     * @return 结果
     */
    @Override
    public int insertBaGlobalNumber(BaGlobalNumber baGlobalNumber)
    {
        baGlobalNumber.setCreateTime(DateUtils.getNowDate());
        return baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
    }

    /**
     * 修改全局编号
     *
     * @param baGlobalNumber 全局编号
     * @return 结果
     */
    @Override
    public int updateBaGlobalNumber(BaGlobalNumber baGlobalNumber)
    {
        baGlobalNumber.setUpdateTime(DateUtils.getNowDate());
        return baGlobalNumberMapper.updateBaGlobalNumber(baGlobalNumber);
    }

    /**
     * 批量删除全局编号
     *
     * @param ids 需要删除的全局编号ID
     * @return 结果
     */
    @Override
    public int deleteBaGlobalNumberByIds(String[] ids)
    {
        return baGlobalNumberMapper.deleteBaGlobalNumberByIds(ids);
    }

    /**
     * 删除全局编号信息
     *
     * @param id 全局编号ID
     * @return 结果
     */
    @Override
    public int deleteBaGlobalNumberById(String id)
    {
        return baGlobalNumberMapper.deleteBaGlobalNumberById(id);
    }

}
