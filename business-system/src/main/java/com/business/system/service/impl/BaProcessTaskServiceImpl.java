package com.business.system.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.BusinessConstant;
import com.business.common.constant.Constants;
import com.business.common.constant.HttpStatus;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.VerificationErrorCode;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.system.adapter.WorkflowUpdateStatusAdapter;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;
import com.business.system.domain.vo.HistoryTaskVO;
import com.business.system.domain.vo.ProcessTaskVO;
import com.business.system.domain.vo.RuntimeTaskVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.github.pagehelper.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 审批任务实现类
 * @date: 2022/12/8 20:37
 */
@Service
@Slf4j
public class BaProcessTaskServiceImpl implements IBaProcessTaskService {

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private WorkflowUpdateStatusAdapter workflowUpdateStatusAdapter;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysPostService sysPostService;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    /**
     * 获取我的已办审批任务列表(我的已办审批、我发起的审批)
     * @param queryDTO
     * @return 已办任务对象集合
     */
    @Override
    public TableDataInfo listRuntimeProcessTask(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        TableDataInfo runtimeTaskDataInfo = workFlowFeignClient.listRuntimeTask(queryDTO);
        List<RuntimeTaskVO> runtimeTaskVOS = beanUtil.listConvert(runtimeTaskDataInfo.getRows(), RuntimeTaskVO.class);
        if(ObjectUtils.isEmpty(runtimeTaskVOS)){
            return runtimeTaskDataInfo;
        }
        List<String> processInstanceIds = new ArrayList<>();
        runtimeTaskVOS.forEach(runtimeTaskVO -> {
            processInstanceIds.add(runtimeTaskVO.getProcessInstanceId());
        });
        // 拿到任务列表里面的实例ID 查询业务数据
        List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
        );
        if(ObjectUtils.isEmpty(list)){
            return runtimeTaskDataInfo;
        }
        // 将业务数据组装到任务列表里
        Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
        runtimeTaskDataInfo.setRows(beanUtil.listConvert(mergeRuntimeTaskList(BaProcessInstanceRelatedMap, runtimeTaskVOS), ProcessTaskVO.class));
        return runtimeTaskDataInfo;
    }

    /**
     * 获取首页待办审批任务列表
     * @param queryDTO
     * @return 待办任务对象集合
     */
    @Override
    public TableDataInfo listHomeRuntimeProcessTask(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        TableDataInfo runtimeTaskDataInfo = workFlowFeignClient.listRuntimeTask(queryDTO);
        List<RuntimeTaskVO> runtimeTaskVOS = beanUtil.listConvert(runtimeTaskDataInfo.getRows(), RuntimeTaskVO.class);
        if(ObjectUtils.isEmpty(runtimeTaskVOS)){
            return runtimeTaskDataInfo;
        }
        List<String> processInstanceIds = new ArrayList<>();
        runtimeTaskVOS.forEach(runtimeTaskVO -> {
            processInstanceIds.add(runtimeTaskVO.getProcessInstanceId());
        });
        // 拿到任务列表里面的实例ID 查询业务数据
        List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
        );
        if(ObjectUtils.isEmpty(list)){
            return runtimeTaskDataInfo;
        }
        // 将业务数据组装到任务列表里
        Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
        runtimeTaskDataInfo.setRows(beanUtil.listConvert(mergeRuntimeTaskList(BaProcessInstanceRelatedMap, runtimeTaskVOS), ProcessTaskVO.class));
        return runtimeTaskDataInfo;
    }

    @Override
    public void completeTask(ProcessTaskDTO processTaskDTO) {
        //校验审核理由在不通过情况下必填
        if(BusinessConstant.OPINION_DISAGREE.equals(processTaskDTO.getOpinion()) && !StringUtils.hasText(processTaskDTO.getReason())){
            throw new VerificationException(VerificationErrorCode.VERIFICATION_TASK_VERIFY_ERROR);
        }
        // 根据任务ID办理任务
        workFlowFeignClient.completeTask(processTaskDTO);

        // 拒绝，更新业务与流程实例关联关系
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        AcceptApprovalWorkflowDTO updateStatusDTO = new AcceptApprovalWorkflowDTO();
        if(BusinessConstant.OPINION_DISAGREE.equals(processTaskDTO.getOpinion())){
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_REJECT.getCode());
            updateStatusDTO.setApprovalResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_REJECT.getCode());
        }
        //判断流程实例是否结束
        String processInstanceIsEndResult = "";
        AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(processTaskDTO.getProcessInstanceId());
        if(!ObjectUtils.isEmpty(ajaxResult)){
            processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
        }
        // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
        if(BusinessConstant.AGREE.equals(processTaskDTO.getOpinion())
                && processInstanceIsEndResult.equals(Constants.SUCCESS)){
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
            updateStatusDTO.setApprovalResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
        }
        baProcessInstanceRelated.setReason(processTaskDTO.getReason());
        LambdaQueryWrapper<BaProcessInstanceRelated> baProcessInstanceRelatedLambdaQueryWrapper = new LambdaQueryWrapper<>();
        baProcessInstanceRelatedLambdaQueryWrapper.eq(BaProcessInstanceRelated::getProcessInstanceId,processTaskDTO.getProcessInstanceId());
        iBaProcessBusinessInstanceRelatedService.update(baProcessInstanceRelated,baProcessInstanceRelatedLambdaQueryWrapper);

        // 拒绝或者通过并且是最后一个节点 更新业务表
        if(BusinessConstant.OPINION_DISAGREE.equals(processTaskDTO.getOpinion())
                || (BusinessConstant.AGREE.equals(processTaskDTO.getOpinion()) && processInstanceIsEndResult.equals(Constants.SUCCESS))){

            BaProcessInstanceRelated one = iBaProcessBusinessInstanceRelatedService.getOne(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .eq(BaProcessInstanceRelated::getProcessInstanceId, processTaskDTO.getProcessInstanceId())
            );
            if(one == null){
                throw new VerificationException("当前办理业务数据异常");
            }
            updateStatusDTO.setTargetId(one.getBusinessId());
            updateStatusDTO.setTargetType(one.getApproveType());
            updateStatusDTO.setProcessInstanceId(one.getProcessInstanceId());
            workflowUpdateStatusAdapter.adapterAcceptApprovalResult(updateStatusDTO);
        }
    }

    @Override
    public void refuse(ProcessTaskDTO processTaskDTO) {
        //校验审核理由在不通过情况下必填
        if(BusinessConstant.REFUSE.equals(processTaskDTO.getOpinion()) && !StringUtils.hasText(processTaskDTO.getReason())){
            throw new VerificationException(VerificationErrorCode.VERIFICATION_TASK_VERIFY_ERROR);
        }
        // 根据任务ID办理任务
        workFlowFeignClient.refuse(processTaskDTO);

        // 拒绝，更新业务与流程实例关联关系
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        AcceptApprovalWorkflowDTO updateStatusDTO = new AcceptApprovalWorkflowDTO();
        if(BusinessConstant.REFUSE.equals(processTaskDTO.getOpinion())){
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_REFUSE.getCode());
            updateStatusDTO.setApprovalResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_REFUSE.getCode());
        }
        //判断流程实例是否结束
        String processInstanceIsEndResult = "";
        AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(processTaskDTO.getProcessInstanceId());
        if(!ObjectUtils.isEmpty(ajaxResult)){
            processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
        }
        // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
        if(BusinessConstant.AGREE.equals(processTaskDTO.getOpinion())
                && processInstanceIsEndResult.equals(Constants.SUCCESS)){
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
            updateStatusDTO.setApprovalResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
        }
        baProcessInstanceRelated.setReason(processTaskDTO.getReason());
        LambdaQueryWrapper<BaProcessInstanceRelated> baProcessInstanceRelatedLambdaQueryWrapper = new LambdaQueryWrapper<>();
        baProcessInstanceRelatedLambdaQueryWrapper.eq(BaProcessInstanceRelated::getProcessInstanceId,processTaskDTO.getProcessInstanceId());
        iBaProcessBusinessInstanceRelatedService.update(baProcessInstanceRelated,baProcessInstanceRelatedLambdaQueryWrapper);

        // 拒绝或者通过并且是最后一个节点 更新业务表
        if(BusinessConstant.REFUSE.equals(processTaskDTO.getOpinion())
                || (BusinessConstant.AGREE.equals(processTaskDTO.getOpinion()) && processInstanceIsEndResult.equals(Constants.SUCCESS))){

            BaProcessInstanceRelated one = iBaProcessBusinessInstanceRelatedService.getOne(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .eq(BaProcessInstanceRelated::getProcessInstanceId, processTaskDTO.getProcessInstanceId())
            );
            if(one == null){
                throw new VerificationException("当前办理业务数据异常");
            }
            updateStatusDTO.setTargetId(one.getBusinessId());
            updateStatusDTO.setTargetType(one.getApproveType());
            updateStatusDTO.setProcessInstanceId(one.getProcessInstanceId());
            workflowUpdateStatusAdapter.adapterAcceptApprovalResult(updateStatusDTO);
        }
    }

    @Override
    public TableDataInfo supervisingHistoryProcess(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        StringBuilder processInstances = new StringBuilder();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("flag", 0);
        if(StringUtils.hasText(queryDTO.getFilterCondition())){
            queryWrapper.eq("business_type", queryDTO.getFilterCondition());
        }
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        if(StringUtils.hasText(queryDTO.getBusinessData())) {
            queryWrapper.like("business_data", queryDTO.getBusinessData());
        }
        queryWrapper.eq("approve_result", "processApproveResult:pass");
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        TableDataInfo historyTaskDataInfo = new TableDataInfo();
        if(StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition())){
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                if (processInstances.length() > 0) {
                    processInstances.append(",").append(baProcessInstanceRelated.getProcessInstanceId());
                } else {
                    processInstances.append(baProcessInstanceRelated.getProcessInstanceId());
                }
            }
            queryDTO.setProcessInstanceIds(processInstances.toString());
        }
        if((StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition())) && !StringUtils.hasText(queryDTO.getProcessInstanceIds())) {
            historyTaskDataInfo.setRows(new ArrayList<HistoryTaskVO>());
        } else {
            historyTaskDataInfo = workFlowFeignClient.listHistoryProcessTask(queryDTO);
            List<HistoryTaskVO> historyTaskVOS = beanUtil.listConvert(historyTaskDataInfo.getRows(), HistoryTaskVO.class);
            if (ObjectUtils.isEmpty(historyTaskVOS)) {
                return historyTaskDataInfo;
            }

            List<String> processInstanceIds = new ArrayList<>();
            historyTaskVOS.forEach(historyTaskVO -> {
                processInstanceIds.add(historyTaskVO.getProcessInstanceId());
            });
            // 拿到任务列表里面的实例ID 查询业务数据
            List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
            );
            if (ObjectUtils.isEmpty(list)) {
                return historyTaskDataInfo;
            }

            // 将业务数据组装到任务列表里
            List<Map> historyTaskList = beanUtil.listConvert(historyTaskVOS, Map.class);

            // 将业务数据组装到任务列表里
            Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
            List<HistoryTaskVO> historyTaskVOS1 = mergeHistoryProcessTaskList(BaProcessInstanceRelatedMap, historyTaskVOS);
            historyTaskDataInfo.setRows(beanUtil.listConvert(historyTaskVOS1, ProcessTaskVO.class));
        }
        return historyTaskDataInfo;
    }

    @Override
    public TableDataInfo listRuntimeProcessTaskApp(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        List<String> processInstanceList = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("flag", 0);
        if(StringUtils.hasText(queryDTO.getFilterCondition())){
            queryWrapper.eq("business_type", queryDTO.getFilterCondition());
        }
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        if(StringUtils.hasText(queryDTO.getBusinessData())) {
            queryWrapper.like("business_data", queryDTO.getBusinessData());
        }
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
            processInstanceList.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        TableDataInfo runtimeTaskDataInfo = new TableDataInfo();
        if(!CollectionUtils.isEmpty(processInstanceList)) {
            queryDTO.setProcessInstanceIdList(processInstanceList);
            runtimeTaskDataInfo = workFlowFeignClient.listRuntimeTaskApp(queryDTO);
            List<RuntimeTaskVO> runtimeTaskVOS = beanUtil.listConvert(runtimeTaskDataInfo.getRows(), RuntimeTaskVO.class);
            if (ObjectUtils.isEmpty(runtimeTaskVOS)) {
                return runtimeTaskDataInfo;
            }
            List<String> processInstanceIds = new ArrayList<>();
            runtimeTaskVOS.forEach(runtimeTaskVO -> {
                processInstanceIds.add(runtimeTaskVO.getProcessInstanceId());
            });
            // 拿到任务列表里面的实例ID 查询业务数据
            List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
            );
            if (ObjectUtils.isEmpty(list)) {
                return runtimeTaskDataInfo;
            }
            // 将业务数据组装到任务列表里
            Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
            runtimeTaskDataInfo.setRows(beanUtil.listConvert(mergeRuntimeTaskList(BaProcessInstanceRelatedMap, runtimeTaskVOS), ProcessTaskVO.class));
        } else {
            runtimeTaskDataInfo.setRows(new ArrayList<ProcessTaskVO>());
        }
        return runtimeTaskDataInfo;
    }

    @Override
    public TableDataInfo listHistoryProcessTaskApp(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        if(StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition())){
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setBusinessType(queryDTO.getFilterCondition());
            baProcessInstanceRelated.setBusinessData(queryDTO.getBusinessData());
            List<String> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listProcessInstanceIds(baProcessInstanceRelated);
            queryDTO.setProcessInstanceIdList(baProcessInstanceRelateds);
        }
        TableDataInfo historyTaskDataInfo = new TableDataInfo();
        if((StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition())) && CollectionUtils.isEmpty(queryDTO.getProcessInstanceIdList())) {
            historyTaskDataInfo.setRows(new ArrayList<ProcessTaskVO>());
        } else {
            historyTaskDataInfo = workFlowFeignClient.listHistoryProcessTaskApp(queryDTO);
            List<HistoryTaskVO> historyTaskVOS = beanUtil.listConvert(historyTaskDataInfo.getRows(), HistoryTaskVO.class);
            if (ObjectUtils.isEmpty(historyTaskVOS)) {
                return historyTaskDataInfo;
            }

            List<String> processInstanceIds = new ArrayList<>();
            historyTaskVOS.forEach(historyTaskVO -> {
                processInstanceIds.add(historyTaskVO.getProcessInstanceId());
            });
            // 拿到任务列表里面的实例ID 查询业务数据
            List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
            );
            if (ObjectUtils.isEmpty(list)) {
                return historyTaskDataInfo;
            }

            // 将业务数据组装到任务列表里
            List<Map> historyTaskList = beanUtil.listConvert(historyTaskVOS, Map.class);

            // 将业务数据组装到任务列表里
            Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
            List<HistoryTaskVO> historyTaskVOS1 = mergeHistoryProcessTaskList(BaProcessInstanceRelatedMap, historyTaskVOS);
            historyTaskDataInfo.setRows(beanUtil.listConvert(historyTaskVOS1, ProcessTaskVO.class));
        }
        return historyTaskDataInfo;
    }

    @Override
    public TableDataInfo listRuntimeProcessTaskSendApp(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        StringBuilder processInstances = new StringBuilder();
        if(StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition()) ){
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setBusinessType(queryDTO.getFilterCondition());
            baProcessInstanceRelated.setBusinessData(queryDTO.getBusinessData());
            List<String> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listProcessInstanceIds(baProcessInstanceRelated);
            if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                for(String baProcessInstance : baProcessInstanceRelateds){
                    if(processInstances.length() > 0){
                        processInstances.append(",").append(baProcessInstance);
                    } else {
                        processInstances.append(baProcessInstance);
                    }
                }
            }
            queryDTO.setProcessInstanceIds(processInstances.toString());
        }
        TableDataInfo runtimeTaskDataInfo = new TableDataInfo();
        if((StringUtils.hasText(queryDTO.getBusinessData()) || StringUtils.hasText(queryDTO.getFilterCondition())) && !StringUtils.hasText(queryDTO.getProcessInstanceIds())) {
            runtimeTaskDataInfo.setRows(new ArrayList<ProcessTaskVO>());
        } else {
            runtimeTaskDataInfo = workFlowFeignClient.listRuntimeTaskSendApp(queryDTO);
            List<HistoryTaskVO> historyTaskVOS = beanUtil.listConvert(runtimeTaskDataInfo.getRows(), HistoryTaskVO.class);
            if (ObjectUtils.isEmpty(historyTaskVOS)) {
                return runtimeTaskDataInfo;
            }
            List<String> processInstanceIds = new ArrayList<>();
            historyTaskVOS.forEach(historyTaskVO -> {
                processInstanceIds.add(historyTaskVO.getProcessInstanceId());
            });
            // 拿到任务列表里面的实例ID 查询业务数据
            List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                    .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
            );
            if (ObjectUtils.isEmpty(list)) {
                return runtimeTaskDataInfo;
            }
            // 将业务数据组装到任务列表里
            Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
            runtimeTaskDataInfo.setRows(beanUtil.listConvert(mergeHistoryProcessTaskList(BaProcessInstanceRelatedMap, historyTaskVOS), ProcessTaskVO.class));
        }
        return runtimeTaskDataInfo;
    }

    /**
     * 封装待办任务数据
     * @param BaProcessInstanceRelatedMap 业务与流程实例数据
     * @param runtimeTaskList 待办任务数据
     * @return
     */
    public List<RuntimeTaskVO> mergeRuntimeTaskList(Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap, List<RuntimeTaskVO> runtimeTaskList) {
        //查询运营部所有子部门 dep_id = 102
        List<SysDept> operationSysDeptList = sysDeptMapper.selectChildrenDeptById(new Long(101));
        Set<Long> commonDeptSet = new HashSet<>();
        commonDeptSet.add(new Long(101));
        if(!CollectionUtils.isEmpty(operationSysDeptList)){
            commonDeptSet.addAll(operationSysDeptList.stream().map(SysDept::getDeptId).collect(Collectors.toSet()));
        }
        //查询业务部所有子部门 dep_id = 101
        List<SysDept> businessSysDeptList = sysDeptMapper.selectChildrenDeptById(new Long(132));
        commonDeptSet.add(new Long(132));
        if(!CollectionUtils.isEmpty(businessSysDeptList)){
            commonDeptSet.addAll(businessSysDeptList.stream().map(SysDept::getDeptId).collect(Collectors.toSet()));
        }

        //查询用户
        Map<String, SysUser> userMap = iSysUserService.selectUserMapByUserName(new SysUser());
        //查询用户
        Map<Long, SysUser> userIdMap = iSysUserService.selectUserMapByUserId(new SysUser());
        runtimeTaskList.stream().forEach(item1 -> {
            SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(item1.getAssignee()));
            if(!ObjectUtils.isEmpty(sysUser1) && commonDeptSet.contains(sysUser1.getDeptId())){
                item1.setApproveEditType("true");
            } else {
                item1.setApproveEditType("false");
            }
            BaProcessInstanceRelated baProcessInstanceRelated = BaProcessInstanceRelatedMap.get(item1.getProcessInstanceId());
            if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                item1.setBusinessType(baProcessInstanceRelated.getBusinessType());
                item1.setBusinessData(baProcessInstanceRelated.getBusinessData());
                //处理业务数据
                JSONObject jsonObject = (JSONObject) JSON.parse(baProcessInstanceRelated.getBusinessData());
                if(!ObjectUtils.isEmpty(jsonObject)){
                    String userName = String.valueOf(jsonObject.get("userName"));
                    String userId = "";
                    if(StringUtils.hasText(userName)){
                        SysUser sysUser = userMap.get(userName);
                        if(!ObjectUtils.isEmpty(sysUser)) {
                            userId = String.valueOf(sysUser.getUserId());
                        } else {
                            userId = String.valueOf(jsonObject.get("userId"));
                        }
                        //根据用户查询岗位集合
                        List<SysPost> sysPosts = sysPostService.selectPostMapByUserId(userId);
                        String postName = "";
                        for (SysPost sysPost : sysPosts) {
                            postName = sysPost.getPostName() + "-" + postName;
                        }
                        if(ObjectUtils.isEmpty(sysUser)){
                            sysUser = userIdMap.get(Long.parseLong(userId));
                        }
                        if(!ObjectUtils.isEmpty(sysUser)) {
                            item1.setStartUserName(sysUser.getNickName());
                            if (postName.equals("") == false) {
                                item1.setStartNickName(sysUser.getNickName() + "-" + postName.substring(0, postName.length() - 1));
                            } else {
                                item1.setStartNickName(sysUser.getNickName());
                            }
                        }
                    }

                    //配置待办任务业务名称
                    item1.setBusiName(getBusiName(baProcessInstanceRelated.getBusinessType(), jsonObject));
                }
                if(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode().equals(baProcessInstanceRelated.getBusinessType())){
                    item1.setCompanyType(baProcessInstanceRelated.getApproveType().split(":")[0]);
                }
                item1.setApproveType(baProcessInstanceRelated.getApproveType());
                item1.setApproveResult(baProcessInstanceRelated.getApproveResult());
            }
            SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(String.valueOf(item1.getAssignee())));
            if(!ObjectUtils.isEmpty(sysUser)){
                item1.setAssigneeName(sysUser.getUserName());
                item1.setAssigneeNickName(sysUser.getNickName());
            }
            if(StringUtils.hasText(item1.getCreateTime())){
                Date createDate = DateUtils.dateTime(DateUtils.YYYY_MM_DD_HH_MM_SS, item1.getCreateTime());
                Date endDate = DateUtils.getNowDate();
                String timeDifference = DateUtils.formatDuration(createDate, endDate);
                //设置办理用时
                item1.setTimeDifference(timeDifference);
            }
        });
        return runtimeTaskList;
    }

    /**
     * 根据业务获取名称
     */
    public String getBusiName(String businessType, JSONObject jsonObject){
        String businessId = "";
        String businessName = "";
        //项目类型
        String projectTypes = "";
        JSONObject business = null;
        //常规立项，仓库立项，合同启动
        if(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_FINANCIALSEAL.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_GENERALSEAL.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_FINANCIALSEAL.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode().equals(businessType)){
            return jsonObject.getString("name");
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode().equals(businessType)){
            return jsonObject.getString("planName");
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode().equals(businessType)){
            businessId = jsonObject.getString("startId");
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(businessId);
            if(!ObjectUtils.isEmpty(baContractStart)){
                businessName = baContractStart.getName();
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode().equals(businessType)){
            businessId = jsonObject.getString("contractId");
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(businessId);
            if(!ObjectUtils.isEmpty(baContractStart)){
                businessName = baContractStart.getName();
            }
            if(!StringUtils.hasText(businessName)){
                businessId = jsonObject.getString("projectId");
                BaProject baProject = baProjectMapper.selectBaProjectById(businessId);
                if(!ObjectUtils.isEmpty(baProject)){
                    businessName = baProject.getName();
                }
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode().equals(businessType)){ //付款，销项发票，进项发票
            projectTypes = jsonObject.getString("projectTypes");
            //选择合同启动
            businessId = jsonObject.getString("startId");
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(businessId);
            if(!ObjectUtils.isEmpty(baContractStart)){
                businessName = baContractStart.getName();
            }
            if(!StringUtils.hasText(businessName)) {
                //仓库立项
                businessId = jsonObject.getString("projectId");
                BaProject baProject = baProjectMapper.selectBaProjectById(businessId);
                if (!ObjectUtils.isEmpty(baProject)) {
                    businessName = baProject.getName();
                }
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode().equals(businessType)){ //入库
            businessId = jsonObject.getString("purchaseId"); //采购计划ID
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(businessId);
            if(!ObjectUtils.isEmpty(baProcurementPlan)){
                businessName = baProcurementPlan.getPlanName();
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode().equals(businessType)){ //出库
            businessId = jsonObject.getString("orderId"); //销售订单ID
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(businessId);
            if(!ObjectUtils.isEmpty(baContractStart)){
                businessName = baContractStart.getName();
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode().equals(businessType)){ //结转
            business = (JSONObject) jsonObject.get("baContractStart"); //销售订单ID
            if(!ObjectUtils.isEmpty(business)){
                businessName = business.getString("name");
            }
        } else if(AdminCodeEnum.TASK_TYPE_PRODUCT_WASHINTODEPOT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_WASHIOUTPUTDEPOT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDINTODEPOT.getCode().equals(businessType) ||
                AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT.getCode().equals(businessType)){ //结转
            businessName = jsonObject.getString("idCode"); //销售订单ID
        }
        return businessName;
    }

    /**
     * 设置发起人ID
     */
    public void setStartUserId(ProcessTaskQueryDTO queryDTO){
//        SysUser sysUser = iSysUserService.selectUserByUserName(queryDTO.getStartUserName());
//        if(!ObjectUtils.isEmpty(sysUser)){
            //设置发起人ID
//            queryDTO.setUserId(String.valueOf(sysUser.getUserId()));
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
//        }
//        else {
//            queryDTO.setUserId(String.valueOf("-1"));
//        }
    }

    @Override
    public TableDataInfo listHistoryProcessTask(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        TableDataInfo historyTaskDataInfo = workFlowFeignClient.listHistoryProcessTask(queryDTO);
        List<HistoryTaskVO> historyTaskVOS = beanUtil.listConvert(historyTaskDataInfo.getRows(), HistoryTaskVO.class);
        if(ObjectUtils.isEmpty(historyTaskVOS)){
            return historyTaskDataInfo;
        }

        List<String> processInstanceIds = new ArrayList<>();
        historyTaskVOS.forEach(historyTaskVO -> {
            processInstanceIds.add(historyTaskVO.getProcessInstanceId());
        });
        // 拿到任务列表里面的实例ID 查询业务数据
        List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
        );
        if(ObjectUtils.isEmpty(list)){
            return historyTaskDataInfo;
        }

        // 将业务数据组装到任务列表里
        List<Map> historyTaskList = beanUtil.listConvert(historyTaskVOS, Map.class);

        // 将业务数据组装到任务列表里
        Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
        List<HistoryTaskVO> historyTaskVOS1 = mergeHistoryProcessTaskList(BaProcessInstanceRelatedMap, historyTaskVOS);
        historyTaskDataInfo.setRows(beanUtil.listConvert(historyTaskVOS1, ProcessTaskVO.class));
        return historyTaskDataInfo;
    }

    /**
     * 封装已办任务数据
     * @param BaProcessInstanceRelatedMap 业务与流程实例数据
     * @param historyTaskVOList 已办任务数据
     * @return
     */
    public List<HistoryTaskVO> mergeHistoryProcessTaskList(Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap, List<HistoryTaskVO> historyTaskVOList) {
        historyTaskVOList.stream().forEach(item1 -> {
            BaProcessInstanceRelated baProcessInstanceRelated = BaProcessInstanceRelatedMap.get(item1.getProcessInstanceId());
            if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                item1.setBusinessType(baProcessInstanceRelated.getBusinessType());
                item1.setBusinessData(baProcessInstanceRelated.getBusinessData());
                //处理业务数据
                JSONObject jsonObject = (JSONObject) JSON.parse(baProcessInstanceRelated.getBusinessData());
                if(!ObjectUtils.isEmpty(jsonObject)){
                    String userName = String.valueOf(jsonObject.get("userName"));
                    if(StringUtils.hasText(userName)){
                        SysUser sysUser = iSysUserService.selectUserByUserName(userName);
                        if(!ObjectUtils.isEmpty(sysUser)){
                            //以用户id查询岗位信息
                            List<Long> longs = sysPostService.selectPostListByUserId(sysUser.getUserId());
                            String postName = "";
                            for (Long postId:longs) {
                                SysPost sysPost = sysPostService.selectPostById(postId);
                                postName = sysPost.getPostName() + "-" + postName;
                            }
                            if(postName.equals("") == false){
                                item1.setStartNickName(sysUser.getNickName()+"-"+postName.substring(0,postName.length()-1));
                                item1.setStartUserName(sysUser.getNickName());
                            }else {
                                item1.setStartNickName(sysUser.getNickName());
                                item1.setStartUserName(sysUser.getNickName());
                            }
                        } else {
                            String userId = String.valueOf(jsonObject.get("userId"));
                            if(StringUtils.hasText(userId)){
                                //以用户id查询岗位信息
                                List<Long> longs = sysPostService.selectPostListByUserId(Long.parseLong(userId));
                                String postName = "";
                                for (Long postId:longs) {
                                    SysPost sysPost = sysPostService.selectPostById(postId);
                                    postName = sysPost.getPostName() + "-" + postName;
                                }
                                item1.setStartUserName(userName);
                                if(postName.equals("") == false){
                                    item1.setStartNickName(userName+"-"+postName.substring(0,postName.length()-1));
                                }else {
                                    item1.setStartNickName(userName);
                                }
                            }else {
                                item1.setStartNickName(userName);
                                item1.setStartUserName(userName);
                            }
                        }
                    }

                    //配置待办任务业务名称
                    item1.setBusiName(getBusiName(baProcessInstanceRelated.getBusinessType(), jsonObject));
                }

                if(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode().equals(baProcessInstanceRelated.getBusinessType())){
                    item1.setCompanyType(baProcessInstanceRelated.getApproveType().split(":")[0]);
                }
                item1.setApproveType(baProcessInstanceRelated.getApproveType());
                item1.setApproveResult(baProcessInstanceRelated.getApproveResult());
            }
            if(StringUtils.hasText(item1.getAssignee())){
                SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(String.valueOf(item1.getAssignee())));
                if(!ObjectUtils.isEmpty(sysUser)){
                    item1.setAssigneeName(sysUser.getUserName());
                    item1.setAssigneeNickName(sysUser.getNickName());
                }
            }
        });
        return historyTaskVOList;
    }

    @Override
    public Map<String, Integer> countRuntimeTask() {
        Map<String, Integer> resultMap = new HashMap<>();
        //立项申请
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode()));
        //投标
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode()));
        //合同
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode()));
        //订单
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode()));
        //发运
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode()));
        //结算
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode()));
        //付款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode()));
        //收款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode()));
        //发票
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode(), getRuntimeTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode()));
        return resultMap;
    }

    @Override
    public Map<String, Integer> countHistoryProcessTask() {
        Map<String, Integer> resultMap = new HashMap<>();
        //立项申请
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode()));
        //投标
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode()));
        //合同
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode()));
        //订单
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode()));
        //发运
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode()));
        //结算
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode()));
        //付款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode()));
        //收款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode()));
        //发票
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode(), getHistoryProcessTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode()));
        return resultMap;
    }

    @Override
    public Map<String, Integer> countHistoryProcessIsEndTask() {
        Map<String, Integer> resultMap = new HashMap<>();
        //立项申请
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode()));
        //投标
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode()));
        //合同
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode()));
        //订单
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode()));
        //发运
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode()));
        //结算
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode()));
        //付款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode()));
        //收款
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode()));
        //发票
        resultMap.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode(), getHistoryProcessIsEndTaskCount(AdminCodeEnum.TASK_TYPE_PRODUCT_INVOICE.getCode()));
        return resultMap;
    }

    /**
     * 统计查询我的申请
     * @param queryDTO
     * @return
     */
    @Override
    public List<BaProcessInstanceRelated> listMyApply(BaProcessInstanceRelatedQueryDTO queryDTO) {
        BaProcessInstanceRelated baProcessInstanceRelated = beanUtil.beanConvert(queryDTO, BaProcessInstanceRelated.class);
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listProcessInstanceGroupByCreatetime(baProcessInstanceRelated);
        List<BaProcessBusinessInstanceRelatedVO> baProcessBusinessInstanceRelatedVOS = beanUtil.listConvert(baProcessInstanceRelateds, BaProcessBusinessInstanceRelatedVO.class);
        if(!CollectionUtils.isEmpty(baProcessBusinessInstanceRelatedVOS)){
            baProcessBusinessInstanceRelatedVOS.stream().forEach(item -> {
                SysUser sysUser = iSysUserService.selectUserByUserName(item.getCreateBy());
                item.setSysUser(sysUser);
                item.setSysDept(iSysDeptService.selectDeptById(sysUser.getDeptId()));
            });
        }
        return baProcessInstanceRelateds;
    }

    @Override
    public long countRuntimeTask(ProcessTaskQueryDTO queryDTO) {
        if(!StringUtils.hasText(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        List<RuntimeTaskVO> list = workFlowFeignClient.listRuntimeTaskAll(queryDTO);
        if(CollectionUtils.isEmpty(list)){
            return 0;
        }
        return list.size();
    }

    @Override
    public int countHistoryProcess(ProcessTaskQueryDTO queryDTO) {
        if(!StringUtils.hasText(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        List<HistoryTaskVO> list = workFlowFeignClient.listHistoryProcessTaskAll(queryDTO);
        if(CollectionUtils.isEmpty(list)){
            return 0;
        }
        return list.size();
    }

    @Override
    public TableDataInfo myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setCode(HttpStatus.SUCCESS);
        tableDataInfo.setMsg("查询成功");
//        BaProcessInstanceRelated baProcessInstanceRelated = beanUtil.beanConvert(queryDTO, BaProcessInstanceRelated.class);
//        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
//        baProcessInstanceRelated.setBusinessType(queryDTO.getBusinessType());
//        baProcessInstanceRelated.setCreateBy(queryDTO.getCreateBy());
//        baProcessInstanceRelated.setApproveResult(queryDTO.getApproveResult());
        if(!StringUtils.isEmpty(queryDTO.getFilterConditionNotEqual())) {
            queryDTO.setFilterConditionNotEquals(Arrays.asList(queryDTO.getFilterConditionNotEqual().split(",")));
        }
        //增加删除条件过滤
        queryDTO.setFlag(0);
        queryDTO.setTenantId(SecurityUtils.getCurrComId());

        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listProcessInstanceOrderByCreatetime(queryDTO);
        List<BaProcessBusinessInstanceRelatedVO> baProcessBusinessInstanceRelatedVOS = beanUtil.listConvert(baProcessInstanceRelateds, BaProcessBusinessInstanceRelatedVO.class);
        //查询所有记录
        queryDTO.setSize(new Long(99999));
        List<BaProcessInstanceRelated> baProcessInstanceRelatedAlls = baProcessInstanceRelatedMapper.listProcessInstanceOrderByCreatetime(queryDTO);
        if(!CollectionUtils.isEmpty(baProcessBusinessInstanceRelatedVOS)){
            baProcessBusinessInstanceRelatedVOS.stream().forEach(item -> {
                SysUser sysUser = iSysUserService.selectUserByUserName(item.getCreateBy());
                item.setSysUser(sysUser);
                item.setSysDept(iSysDeptService.selectDeptById(sysUser.getDeptId()));

                if(!ObjectUtils.isEmpty(item) && StringUtils.hasText(item.getBusinessData())){
                    //处理业务数据
                    JSONObject jsonObject = (JSONObject) JSON.parse(item.getBusinessData());
                    //配置待办任务业务名称
                    item.setBusiName(getBusiName(item.getBusinessType(), jsonObject));
                }

                if(!ObjectUtils.isEmpty(item.getSysUser())){
                    List<Long> longs = sysPostService.selectPostListByUserId(item.getSysUser().getUserId());
                    //岗位信息
                    List<SysPost> postList = new ArrayList<>();
                    for (Long postId:longs) {
                        SysPost sysPost = sysPostService.selectPostById(postId);
                        postList.add(sysPost);
                    }
                    item.setSysPost(postList);
                }
            });
        }
        if(!CollectionUtils.isEmpty(baProcessInstanceRelatedAlls)){
            tableDataInfo.setTotal(baProcessInstanceRelatedAlls.size());
        }
        tableDataInfo.setRows(baProcessBusinessInstanceRelatedVOS);
        return tableDataInfo;
    }

    @Override
    public TableDataInfo listRuntimeProcessTaskSend(ProcessTaskQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        if(!StringUtils.hasText(queryDTO.getUserId()) && !"allUser".equals(queryDTO.getUserId())){
            queryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        }
        queryDTO.setUserFilterCondition(queryDTO.getStartUserId());
        TableDataInfo runtimeTaskDataInfo = workFlowFeignClient.listRuntimeTaskSend(queryDTO);
        List<HistoryTaskVO> historyTaskVOS = beanUtil.listConvert(runtimeTaskDataInfo.getRows(), HistoryTaskVO.class);
        if(ObjectUtils.isEmpty(historyTaskVOS)){
            return runtimeTaskDataInfo;
        }
        List<String> processInstanceIds = new ArrayList<>();
        historyTaskVOS.forEach(historyTaskVO -> {
            processInstanceIds.add(historyTaskVO.getProcessInstanceId());
        });
        // 拿到任务列表里面的实例ID 查询业务数据
        List<BaProcessInstanceRelated> list = iBaProcessBusinessInstanceRelatedService.list(new LambdaQueryWrapper<BaProcessInstanceRelated>()
                .in(BaProcessInstanceRelated::getProcessInstanceId, processInstanceIds)
        );
        if(ObjectUtils.isEmpty(list)){
            return runtimeTaskDataInfo;
        }
        // 将业务数据组装到任务列表里
        Map<String, BaProcessInstanceRelated> BaProcessInstanceRelatedMap = list.stream().collect(Collectors.toMap(BaProcessInstanceRelated::getProcessInstanceId, Function.identity(), (key1, key2) -> key2));
        runtimeTaskDataInfo.setRows(beanUtil.listConvert(mergeHistoryProcessTaskList(BaProcessInstanceRelatedMap, historyTaskVOS), ProcessTaskVO.class));
        return runtimeTaskDataInfo;
    }

    /**
     * 根据业务类型返回待办任务统计数据
     * @param taskType
     * @return
     */
    private int getRuntimeTaskCount(String taskType){
        ProcessTaskQueryDTO processTaskQueryDTO = new ProcessTaskQueryDTO();
        processTaskQueryDTO.setTenantId(SecurityUtils.getCurrComId());
        processTaskQueryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        processTaskQueryDTO.setFilterCondition(taskType);
        AjaxResult ajaxResult = workFlowFeignClient.countRuntimeTask(processTaskQueryDTO);
        if(!ObjectUtils.isEmpty(ajaxResult)){
            return Integer.parseInt(String.valueOf(ajaxResult.get("data")));
        }
        return 0;
    }

    /**
     * 根据业务类型返回已办任务统计数据
     * @param taskType
     * @return
     */
    private int getHistoryProcessTaskCount(String taskType){
        ProcessTaskQueryDTO processTaskQueryDTO = new ProcessTaskQueryDTO();
        processTaskQueryDTO.setTenantId(SecurityUtils.getCurrComId());
        processTaskQueryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        processTaskQueryDTO.setFilterCondition(taskType);
        AjaxResult ajaxResult = workFlowFeignClient.countHistoryProcessTask(processTaskQueryDTO);
        if(!ObjectUtils.isEmpty(ajaxResult)){
            return Integer.parseInt(String.valueOf(ajaxResult.get("data")));
        }
        return 0;
    }

    /**
     * 根据业务类型返回已完结任务统计数据
     * @param taskType
     * @return
     */
    private int getHistoryProcessIsEndTaskCount(String taskType){
        ProcessTaskQueryDTO processTaskQueryDTO = new ProcessTaskQueryDTO();
        processTaskQueryDTO.setTenantId(SecurityUtils.getCurrComId());
        processTaskQueryDTO.setUserId(String.valueOf(SecurityUtils.getUserId()));
        processTaskQueryDTO.setFilterCondition(taskType);
        processTaskQueryDTO.setProcessIsEnd(BusinessConstant.PROCESS_HISTORY_END);
        AjaxResult ajaxResult = workFlowFeignClient.countHistoryProcessTask(processTaskQueryDTO);
        if(!ObjectUtils.isEmpty(ajaxResult)){
            return Integer.parseInt(String.valueOf(ajaxResult.get("data")));
        }
        return 0;
    }
}
