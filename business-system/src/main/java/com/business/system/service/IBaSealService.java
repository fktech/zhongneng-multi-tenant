package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaSeal;

/**
 * 用印Service接口
 *
 * @author ljb
 * @date 2023-03-14
 */
public interface IBaSealService
{
    /**
     * 查询用印
     *
     * @param id 用印ID
     * @return 用印
     */
    public BaSeal selectBaSealById(String id);

    /**
     * 查询用印列表
     *
     * @param baSeal 用印
     * @return 用印集合
     */
    public List<BaSeal> selectBaSealList(BaSeal baSeal);

    /**
     * 新增用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    public int insertBaSeal(BaSeal baSeal);

    /**
     * 修改用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    public int updateBaSeal(BaSeal baSeal);

    /**
     * 批量删除用印
     *
     * @param ids 需要删除的用印ID
     * @return 结果
     */
    public int deleteBaSealByIds(String[] ids);

    /**
     * 删除用印信息
     *
     * @param id 用印ID
     * @return 结果
     */
    public int deleteBaSealById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 查询多租户授权信息业务用印列表
     */
    public List<BaSeal> selectBaSealAuthorizedList(BaSeal baSeal);

    /**
     * 授权业务用印
     */
    public int authorizedBaSeal(BaSeal baSeal);
}
