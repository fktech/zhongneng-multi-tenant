package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaTransport;
import com.business.system.domain.BaTransportAutomobile;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.mapper.BaTransportAutomobileMapper;
import com.business.system.mapper.BaTransportMapper;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IAutomobileApprovalService;
import com.business.system.service.workflow.ITransportApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 汽运申请审批结果:审批通过
 */
@Service("automobileApprovalContent:processApproveResult:pass")
@Slf4j
public class AutomobileApprovalAgreeServiceImpl extends IAutomobileApprovalService implements IWorkflowUpdateStatusService {
    @Override

    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.AUTOMOBILE_STATUS_PASS.getCode());
        return result;
    }
}
