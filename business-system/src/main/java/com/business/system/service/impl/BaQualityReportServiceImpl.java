package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.system.domain.BaHiTransport;
import com.business.system.domain.BaTransport;
import com.business.system.service.IBaHiTransportService;
import com.business.system.service.IBaTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaQualityReportMapper;
import com.business.system.domain.BaQualityReport;
import com.business.system.service.IBaQualityReportService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 数质量报告Service业务层处理
 *
 * @author single
 * @date 2023-07-19
 */
@Service
public class BaQualityReportServiceImpl extends ServiceImpl<BaQualityReportMapper, BaQualityReport> implements IBaQualityReportService
{
    @Autowired
    private BaQualityReportMapper baQualityReportMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaHiTransportService baHiTransportService;

    /**
     * 查询数质量报告
     *
     * @param id 数质量报告ID
     * @return 数质量报告
     */
    @Override
    public BaQualityReport selectBaQualityReportById(String id)
    {
        return baQualityReportMapper.selectBaQualityReportById(id);
    }

    /**
     * 查询数质量报告列表
     *
     * @param baQualityReport 数质量报告
     * @return 数质量报告
     */
    @Override
    public List<BaQualityReport> selectBaQualityReportList(BaQualityReport baQualityReport)
    {
        return baQualityReportMapper.baQualityReportList(baQualityReport);
    }

    /**
     * 新增数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    @Override
    public int insertBaQualityReport(BaQualityReport baQualityReport)
    {
        baQualityReport.setId(getRedisIncreID.getId());
        baQualityReport.setCreateTime(DateUtils.getNowDate());
        return baQualityReportMapper.insertBaQualityReport(baQualityReport);
    }

    /**
     * 修改数质量报告
     *
     * @param baQualityReport 数质量报告
     * @return 结果
     */
    @Override
    public int updateBaQualityReport(BaQualityReport baQualityReport)
    {
        baQualityReport.setUpdateTime(DateUtils.getNowDate());
        return baQualityReportMapper.updateBaQualityReport(baQualityReport);
    }

    /**
     * 批量删除数质量报告
     *
     * @param ids 需要删除的数质量报告ID
     * @return 结果
     */
    @Override
    public int deleteBaQualityReportByIds(String[] ids)
    {
        return baQualityReportMapper.deleteBaQualityReportByIds(ids);
    }

    /**
     * 删除数质量报告信息
     *
     * @param id 数质量报告ID
     * @return 结果
     */
    @Override
    public int deleteBaQualityReportById(String id)
    {
        return baQualityReportMapper.deleteBaQualityReportById(id);
    }

}
