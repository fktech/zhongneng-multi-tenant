package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEgressApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import org.springframework.stereotype.Service;


/**
 * 其他申请审批结果:审批拒绝
 */
@Service("oaOtherApprovalContent:processApproveResult:reject")
public class OaOtherApprovalRejectServiceImpl extends IOaOtherApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAOTHER_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaOther checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
