package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaWashInventory;

/**
 * 洗煤配煤入库清单Service接口
 *
 * @author single
 * @date 2024-01-05
 */
public interface IBaWashInventoryService
{
    /**
     * 查询洗煤配煤入库清单
     *
     * @param id 洗煤配煤入库清单ID
     * @return 洗煤配煤入库清单
     */
    public BaWashInventory selectBaWashInventoryById(String id);

    /**
     * 查询洗煤配煤入库清单列表
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 洗煤配煤入库清单集合
     */
    public List<BaWashInventory> selectBaWashInventoryList(BaWashInventory baWashInventory);

    /**
     * 新增洗煤配煤入库清单
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 结果
     */
    public int insertBaWashInventory(BaWashInventory baWashInventory);

    /**
     * 修改洗煤配煤入库清单
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 结果
     */
    public int updateBaWashInventory(BaWashInventory baWashInventory);

    /**
     * 批量删除洗煤配煤入库清单
     *
     * @param ids 需要删除的洗煤配煤入库清单ID
     * @return 结果
     */
    public int deleteBaWashInventoryByIds(String[] ids);

    /**
     * 删除洗煤配煤入库清单信息
     *
     * @param id 洗煤配煤入库清单ID
     * @return 结果
     */
    public int deleteBaWashInventoryById(String id);


}
