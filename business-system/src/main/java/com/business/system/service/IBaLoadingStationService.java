package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaLoadingStation;

/**
 * 装货站Service接口
 *
 * @author ljb
 * @date 2023-01-16
 */
public interface IBaLoadingStationService
{
    /**
     * 查询装货站
     *
     * @param id 装货站ID
     * @return 装货站
     */
    public BaLoadingStation selectBaLoadingStationById(String id);

    /**
     * 查询装货站列表
     *
     * @param baLoadingStation 装货站
     * @return 装货站集合
     */
    public List<BaLoadingStation> selectBaLoadingStationList(BaLoadingStation baLoadingStation);

    /**
     * 新增装货站
     *
     * @param baLoadingStation 装货站
     * @return 结果
     */
    public int insertBaLoadingStation(BaLoadingStation baLoadingStation);

    /**
     * 修改装货站
     *
     * @param baLoadingStation 装货站
     * @return 结果
     */
    public int updateBaLoadingStation(BaLoadingStation baLoadingStation);

    /**
     * 批量删除装货站
     *
     * @param ids 需要删除的装货站ID
     * @return 结果
     */
    public int deleteBaLoadingStationByIds(String[] ids);

    /**
     * 删除装货站信息
     *
     * @param id 装货站ID
     * @return 结果
     */
    public int deleteBaLoadingStationById(String id);


}
