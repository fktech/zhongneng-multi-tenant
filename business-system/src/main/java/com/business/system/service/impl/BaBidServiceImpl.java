package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 投标管理Service业务层处理
 *
 * @author ljb
 * @date 2022-12-06
 */
@Service
public class BaBidServiceImpl extends ServiceImpl<BaBidMapper, BaBid> implements IBaBidService
{

    private static final Logger log = LoggerFactory.getLogger(BaBidServiceImpl.class);
    @Autowired
    private BaBidMapper baBidMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaProjectMapper projectMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private ISysPostService sysPostService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;


    /**
     * 查询投标管理
     *
     * @param id 投标管理ID
     * @return 投标管理
     */
    @Override
    public BaBid selectBaBidById(String id)
    {
       BaBid baBid = baBidMapper.selectBaBidById(id);
       //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baBid.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baBid.setUser(sysUser);
       //项目名称
        if(StringUtils.isNotEmpty(baBid.getProjectId())){
           BaProject baProject = projectMapper.selectBaProjectById(baBid.getProjectId());
           if(StringUtils.isNotNull(baProject)){
               baBid.setProjectName(baProject.getName());
           }else {
               baBid.setProjectName(baBid.getProjectId());
           }
        }
        //品名
        if(StringUtils.isNotEmpty(baBid.getGoodsId())){
           BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baBid.getGoodsId());
           baBid.setGoodsName(baGoods.getName());
        }
       //用煤单位信息
        if(StringUtils.isNotEmpty(baBid.getEnterpriseId())){
           BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baBid.getEnterpriseId());
           baBid.setEnterpriseName(baEnterprise.getName());
           baBid.setBidWebsite(baEnterprise.getBidWebsite());
           baBid.setBidAddress(baEnterprise.getBidAddress());
        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id",baBid.getId());
        queryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baBid.setProcessInstanceId(processInstanceIds);

        return baBid;
    }

    /**
     * 查询投标管理列表
     *
     * @param baBid 投标管理
     * @return 投标管理
     */
    @Override
    @DataScope(deptAlias = "bid",userAlias = "bid")
    public List<BaBid> selectBaBidList(BaBid baBid)
    {
        List<BaBid> baBids =  baBidMapper.selectBaBidList(baBid);

        for (BaBid baBidName:baBids) {
            //项目名称
            if(StringUtils.isNotEmpty(baBidName.getProjectId())){
               BaProject baProject = projectMapper.selectBaProjectById(baBidName.getProjectId());
               if(StringUtils.isNotNull(baProject)){
                   baBidName.setProjectName(baProject.getName());
               }else {
                   baBidName.setProjectName(baBidName.getProjectId());
               }
            }
            //用煤单位
            if(StringUtils.isNotEmpty(baBidName.getEnterpriseId())){
                BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baBidName.getEnterpriseId());
                if(!ObjectUtils.isEmpty(baEnterprise)){
                    baBidName.setEnterpriseName(baEnterprise.getName());
                }
            }
            //发起人名称
            SysUser sysUser = sysUserMapper.selectUserById(baBidName.getUserId());
            baBidName.setUserName(sysUser.getNickName());

        }
        return baBids;
    }

    /**
     * 新增投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    @Override
    public int insertBaBid(BaBid baBid)
    {
        baBid.setBidState("待投标");
        baBid.setId(getRedisIncreID.getId());
        baBid.setCreateTime(DateUtils.getNowDate());
        baBid.setCreateBy(SecurityUtils.getUsername());
        baBid.setUserId(SecurityUtils.getUserId());
        baBid.setDeptId(SecurityUtils.getDeptId());
        Integer result = baBidMapper.insertBaBid(baBid);
        if(result > 0){
        BaBid baBid1 = baBidMapper.selectBaBidById(baBid.getId());
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode(),SecurityUtils.getCurrComId());
        baBid1.setFlowId(flowId);
        Integer updateResult = baBidMapper.updateBaBid(baBid1);
        if(updateResult > 0){
           BaBid bid = baBidMapper.selectBaBidById(baBid1.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(bid, AdminCodeEnum.BID_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baBidMapper.deleteBaBidById(bid.getId());
                return 0;
            }else {
                bid.setState(AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                baBidMapper.updateBaBid(bid);
            }
        }
            }
        return 1;
    }

    /**
     * 提交投标审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaBid baBid, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baBid.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baBid.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode());
        //获取流程实例关联的业务对象
        BaBid baBid1 = baBidMapper.selectBaBidById(baBid.getId());
        SysUser sysUser = iSysUserService.selectUserById(baBid1.getUserId());
        baBid1.setUserName(sysUser.getUserName());
        baBid1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baBid1));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.BID_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }


    /**
     * 修改投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    @Override
    public int updateBaBid(BaBid baBid)
    {
        baBid.setUpdateTime(DateUtils.getNowDate());
        baBid.setUpdateBy(SecurityUtils.getUsername());
        Integer result = baBidMapper.updateBaBid(baBid);
       if(result > 0){
           BaBid bid = baBidMapper.selectBaBidById(baBid.getId());
           //启动流程实例
           BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(bid, AdminCodeEnum.BID_APPROVAL_CONTENT_UPDATE.getCode());
           if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
               //baBidMapper.updateBaBid(bid);
               return 0;
           }else {
               bid.setState(AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
               //创建时间
               bid.setCreateTime(baBid.getUpdateTime());
               baBidMapper.updateBaBid(bid);
           }
       }
        return 1;
    }

    @Override
    public int bidSubmit(BaBid baBid) {
        //投标结果为已投标
        if(StringUtils.isEmpty(baBid.getResult())){
            baBid.setBidState("已投标");
            baBid.setResult("1");

        }else {
            //baBid.setBidState("已投标");
            baBid.setResult(baBid.getResult());
        }
        BaBid bid = baBidMapper.selectBaBidById(baBid.getId());
        //项目阶段
        if(StringUtils.isNotEmpty(bid.getProjectId())){
            BaProject baProject = projectMapper.selectBaProjectById(bid.getProjectId());
            if("2".equals(baBid.getResult())){
                baProject.setProjectStage(3);
            }else if("3".equals(baBid.getResult())){
                baProject.setProjectStage(4);
            }else if("1".equals(baBid.getResult())){
                baProject.setProjectStage(2);
            }
            projectMapper.updateBaProject(baProject);
        }
        return baBidMapper.updateBaBid(baBid);
    }

    /**
     * 批量删除投标管理
     *
     * @param ids 需要删除的投标管理ID
     * @return 结果
     */
    @Override
    public int deleteBaBidByIds(String[] ids)
    {
        //伪删除
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaBid baBid = baBidMapper.selectBaBidById(id);
               baBid.setFlag(1);
               baBidMapper.updateBaBid(baBid);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除投标管理信息
     *
     * @param id 投标管理ID
     * @return 结果
     */
    @Override
    public int deleteBaBidById(String id)
    {
        return baBidMapper.deleteBaBidById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
           BaBid baBid = baBidMapper.selectBaBidById(id);
           //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baBid.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baBid.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
               //修改业务中的审批状态
                baBid.setState(AdminCodeEnum.BID_STATUS_WITHDRAW.getCode());
                baBidMapper.updateBaBid(baBid);
                String userId = String.valueOf(baBid.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baBid, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaBid baBid, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baBid.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_BID.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }


}
