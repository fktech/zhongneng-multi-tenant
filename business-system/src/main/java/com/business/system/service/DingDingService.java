package com.business.system.service;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.DingTalkClockDTO;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaWorkTimes;

import java.util.List;
import java.util.Map;

/**
 * please change here
 *
 * @author ljb
 * @date 2024.7
 */
public interface DingDingService {

    //获取用户考勤数据
    public String getUpDate() throws Exception;

    //获取钉钉人脸识别打卡数据
    public String getDingTalk(DingTalkClockDTO dingTalkClockDTO) throws Exception;

    //获取考勤报表
    public String getColumnVal() throws Exception;

    //获取考勤报表列定义
    public String getAttColumns() throws Exception;

    //获取打卡详情
    public String getListRecord(List<String> userIds) throws Exception;

    //新增考勤组
    public String groupAdd(OaAttendanceGroup attendanceGroup) throws  Exception;

    //修改考勤组
    public String groupModify(OaAttendanceGroup attendanceGroup) throws  Exception;

    //groupId转换为groupKey
    public String groupIdToKey(String groupId) throws  Exception;

    //删除考勤组
    public String groupDelete(String groupId) throws  Exception;

    //新增班次
    public String shiftAdd(OaWorkTimes oaWorkTimes) throws  Exception;

    //删除班次
    public String shiftDelete(Long shiftId) throws  Exception;

    //批量新增参与考勤人员
    public String groupUsersAdd() throws  Exception;

    //更新参与考勤人员
    public String groupMemberUpdate(OaAttendanceGroup attendanceGroup) throws  Exception;

    //创建用户
    public String userCreate(SysUser user) throws  Exception;

    //修改用户
    public String userUpdate(SysUser user) throws  Exception;

    //删除用户
    public String userDelete(String userId) throws  Exception;

    //创建部门
    public String departmentCreate(SysDept dept) throws  Exception;

    //修改部门
    public String departmentUpdate(SysDept dept) throws  Exception;

    //删除部门
    public String departmentDelete(Long deptId) throws  Exception;

    //获取部门列表
    public String departmentListSub() throws  Exception;

    //创建钉钉部门
    public String insertDingdingDept(SysDept dept) throws  Exception;
}
