package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaProjectLink;

/**
 * 立项授权Service接口
 *
 * @author ljb
 * @date 2023-06-05
 */
public interface IBaProjectLinkService
{
    /**
     * 查询立项授权
     *
     * @param id 立项授权ID
     * @return 立项授权
     */
    public BaProjectLink selectBaProjectLinkById(String id);

    /**
     * 查询立项授权列表
     *
     * @param baProjectLink 立项授权
     * @return 立项授权集合
     */
    public List<BaProjectLink> selectBaProjectLinkList(BaProjectLink baProjectLink);

    /**
     * 新增立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    public int insertBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 修改立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    public int updateBaProjectLink(BaProjectLink baProjectLink);

    /**
     * 批量删除立项授权
     *
     * @param ids 需要删除的立项授权ID
     * @return 结果
     */
    public int deleteBaProjectLinkByIds(String[] ids);

    /**
     * 删除立项授权信息
     *
     * @param id 立项授权ID
     * @return 结果
     */
    public int deleteBaProjectLinkById(String id);


}
