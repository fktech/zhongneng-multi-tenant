package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaDeclare;

/**
 * 计划申报Service接口
 *
 * @author single
 * @date 2023-03-21
 */
public interface IBaDeclareService
{
    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    public BaDeclare selectBaDeclareById(String id);

    /**
     * 查询计划申报列表
     *
     * @param baDeclare 计划申报
     * @return 计划申报集合
     */
    public List<BaDeclare> selectBaDeclareList(BaDeclare baDeclare);

    /**
     * 新增计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    public int insertBaDeclare(BaDeclare baDeclare);

    /**
     * 修改计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    public int updateBaDeclare(BaDeclare baDeclare);

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareByIds(String[] ids);

    /**
     * 删除计划申报信息
     *
     * @param id 计划申报ID
     * @return 结果
     */
    public int deleteBaDeclareById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 查询本月数据
     */
    public UR thisMonth();
    /**
     * 查询本月审核通过数据
     */
    public UR thisMonth1(BaDeclare baDeclare);

    /**
     * 查询多租户授权信息计划申报列表
     */
    public List<BaDeclare> selectBaDeclareAuthorizedList(BaDeclare baDeclare);

    /**
     * 授权计划申报
     */
    public int authorizedBaDeclare(BaDeclare baDeclare);
}
