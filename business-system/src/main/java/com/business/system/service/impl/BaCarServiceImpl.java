package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaCarMapper;
import com.business.system.domain.BaCar;
import com.business.system.service.IBaCarService;

/**
 * 车辆信息Service业务层处理
 *
 * @author single
 * @date 2023-12-01
 */
@Service
public class BaCarServiceImpl extends ServiceImpl<BaCarMapper, BaCar> implements IBaCarService
{
    @Autowired
    private BaCarMapper baCarMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询车辆信息
     *
     * @param id 车辆信息ID
     * @return 车辆信息
     */
    @Override
    public BaCar selectBaCarById(String id)
    {
        return baCarMapper.selectBaCarById(id);
    }

    /**
     * 查询车辆信息列表
     *
     * @param baCar 车辆信息
     * @return 车辆信息
     */
    @Override
    public List<BaCar> selectBaCarList(BaCar baCar)
    {
        return baCarMapper.selectBaCarList(baCar);
    }

    /**
     * 新增车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    @Override
    public int insertBaCar(BaCar baCar)
    {
        baCar.setId(getRedisIncreID.getId());
        baCar.setCreateTime(DateUtils.getNowDate());
        return baCarMapper.insertBaCar(baCar);
    }

    /**
     * 修改车辆信息
     *
     * @param baCar 车辆信息
     * @return 结果
     */
    @Override
    public int updateBaCar(BaCar baCar)
    {
        baCar.setUpdateTime(DateUtils.getNowDate());
        return baCarMapper.updateBaCar(baCar);
    }

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的车辆信息ID
     * @return 结果
     */
    @Override
    public int deleteBaCarByIds(String[] ids)
    {
        return baCarMapper.deleteBaCarByIds(ids);
    }

    /**
     * 删除车辆信息信息
     *
     * @param id 车辆信息ID
     * @return 结果
     */
    @Override
    public int deleteBaCarById(String id)
    {
        return baCarMapper.deleteBaCarById(id);
    }

}
