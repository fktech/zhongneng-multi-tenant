package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysDictData;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 招待申请Service业务层处理
 *
 * @author ljb
 * @date 2023-11-13
 */
@Service
public class OaEntertainServiceImpl extends ServiceImpl<OaEntertainMapper, OaEntertain> implements IOaEntertainService
{
    private static final Logger log = LoggerFactory.getLogger(OaEntertainServiceImpl.class);
    @Autowired
    private OaEntertainMapper oaEntertainMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private ISysDictDataService sysDictDataService;


    /**
     * 查询招待申请
     *
     * @param id 招待申请ID
     * @return 招待申请
     */
    @Override
    public OaEntertain selectOaEntertainById(String id)
    {
        OaEntertain oaEntertain = oaEntertainMapper.selectOaEntertainById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",oaEntertain.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        oaEntertain.setProcessInstanceId(processInstanceIds);
        //申请人名称
        if(oaEntertain.getUserId() != null){
            oaEntertain.setUserName(sysUserMapper.selectUserById(oaEntertain.getUserId()).getNickName());
        }
        //申请人部门
        if(oaEntertain.getDeptId() != null){
            oaEntertain.setDeptName(sysDeptMapper.selectDeptById(oaEntertain.getDeptId()).getDeptName());
        }
        //项目名称
        if(StringUtils.isNotEmpty(oaEntertain.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(oaEntertain.getProjectId());
            oaEntertain.setProjectName(baProject.getName());
        }
        //归属事业部
        if(StringUtils.isNotEmpty(oaEntertain.getBelongDept())){
            oaEntertain.setBelongDeptName(sysDeptMapper.selectDeptById(Long.valueOf(oaEntertain.getBelongDept())).getDeptName());
        }
        return oaEntertain;
    }

    /**
     * 查询招待申请列表
     *
     * @param oaEntertain 招待申请
     * @return 招待申请
     */
    @Override
    public List<OaEntertain> selectOaEntertainList(OaEntertain oaEntertain)
    {
        List<OaEntertain> oaEntertains = oaEntertainMapper.selectOaEntertainList(oaEntertain);
        for (OaEntertain entertain:oaEntertains) {
             //申请人
            if(entertain.getUserId() != null){
                SysUser user = sysUserMapper.selectUserById(entertain.getUserId());
                entertain.setUserName(user.getNickName());
            }
            //部门
            if(entertain.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(entertain.getDeptId());
                entertain.setDeptName(dept.getDeptName());
            }
            //创建时间
            entertain.setCreationTime(entertain.getCreateTime());
            //费用类型转换
            List<SysDictData> dictData = sysDictDataService.selectDictDataByType("fee_ilk");
            if(StringUtils.isNotEmpty(entertain.getFeeIlk())){
                for (SysDictData sysDictData:dictData) {
                    if(sysDictData.getDictValue().equals(entertain.getFeeIlk())){
                        //判断费用类型是否为项目
                        if("2".equals(entertain.getFeeIlk())){
                            //判断是否存在项目信息
                            if(StringUtils.isNotEmpty(entertain.getProjectId())){
                                //查询项目信息
                                BaProject baProject = baProjectMapper.selectBaProjectById(entertain.getProjectId());
                                if(StringUtils.isNotEmpty(baProject.getSubsidiary())){
                                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baProject.getSubsidiary()));
                                    baProject.setSubsidiaryName(dept.getDeptName());
                                    baProject.setSubsidiaryAbbreviation(dept.getAbbreviation());
                                    entertain.setFeeIlkName(sysDictData.getDictLabel()+"-"+baProject.getName()+"("+baProject.getSubsidiaryAbbreviation()+")");
                                }else {
                                    entertain.setFeeIlkName(sysDictData.getDictLabel()+"-"+baProject.getName());
                                }
                            }else {
                                entertain.setFeeIlkName(sysDictData.getDictLabel());
                            }
                        }else {
                            entertain.setFeeIlkName(sysDictData.getDictLabel());
                        }
                    }
                }
            }
        }
        return oaEntertains;
    }

    /**
     * 新增招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    @Override
    public int insertOaEntertain(OaEntertain oaEntertain)
    {
        //判断保存数据是否存在
        if(StringUtils.isNotEmpty(oaEntertain.getId()) && "0".equals(oaEntertain.getSubmitState())){
            return oaEntertainMapper.updateOaEntertain(oaEntertain);
        }
        //判断保存数据是否提交
        if(StringUtils.isNotEmpty(oaEntertain.getId()) && "1".equals(oaEntertain.getSubmitState())){
            OaEntertain entertain = oaEntertainMapper.selectOaEntertainById(oaEntertain.getId());
            entertain.setFlag(1L);
            oaEntertainMapper.updateOaEntertain(entertain);
        }
        oaEntertain.setId(getRedisIncreID.getId());
        oaEntertain.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaEntertain.setTenantId(SecurityUtils.getCurrComId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getCode(),SecurityUtils.getCurrComId());
        oaEntertain.setFlowId(flowId);
        int result = oaEntertainMapper.insertOaEntertain(oaEntertain);
        if(result > 0 && "1".equals(oaEntertain.getSubmitState())){
            OaEntertain entertain = oaEntertainMapper.selectOaEntertainById(oaEntertain.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(entertain, AdminCodeEnum.OAENTERTAIN_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaEntertainMapper.deleteOaEntertainById(entertain.getId());
                return 0;
            }else {
                entertain.setState(AdminCodeEnum.OAENTERTAIN_STATUS_VERIFYING.getCode());
                oaEntertainMapper.updateOaEntertain(entertain);
            }
        }
        return result;
    }

    /**
     * 提交招待申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaEntertain entertain, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(entertain.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getCode(), AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(entertain.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        OaEntertain oaEntertain = this.selectOaEntertainById(entertain.getId());
        SysUser sysUser = iSysUserService.selectUserById(oaEntertain.getUserId());
        oaEntertain.setUserName(sysUser.getNickName());
        oaEntertain.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaEntertain, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OAENTERTAIN_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    @Override
    public int updateOaEntertain(OaEntertain oaEntertain)
    {
        //原数据
        OaEntertain oaEntertain1 = oaEntertainMapper.selectOaEntertainById(oaEntertain.getId());

        oaEntertain.setUpdateTime(DateUtils.getNowDate());
        int result = oaEntertainMapper.updateOaEntertain(oaEntertain);
        if(result > 0 && "1".equals(oaEntertain.getSubmitState())){
            OaEntertain entertain = oaEntertainMapper.selectOaEntertainById(oaEntertain.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", entertain.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(entertain, AdminCodeEnum.OAENTERTAIN_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaEntertainMapper.updateOaEntertain(oaEntertain1);
                return 0;
            }else {
                entertain.setState(AdminCodeEnum.OAENTERTAIN_STATUS_VERIFYING.getCode());
                oaEntertainMapper.updateOaEntertain(entertain);
            }
        }
        return result;
    }

    /**
     * 批量删除招待申请
     *
     * @param ids 需要删除的招待申请ID
     * @return 结果
     */
    @Override
    public int deleteOaEntertainByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaEntertain oaEntertain = oaEntertainMapper.selectOaEntertainById(id);
                oaEntertain.setFlag(1L);
                oaEntertainMapper.updateOaEntertain(oaEntertain);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除招待申请信息
     *
     * @param id 招待申请ID
     * @return 结果
     */
    @Override
    public int deleteOaEntertainById(String id)
    {
        return oaEntertainMapper.deleteOaEntertainById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaEntertain entertain = oaEntertainMapper.selectOaEntertainById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",entertain.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OAENTERTAIN_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(entertain.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                entertain.setState(AdminCodeEnum.OAENTERTAIN_STATUS_WITHDRAW.getCode());
                oaEntertainMapper.updateOaEntertain(entertain);
                String userId = String.valueOf(entertain.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(entertain, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 审批办理保存编辑业务数据
     * @param oaEntertainInstanceRelatedEditDTO
     * @return
     */
    public int updateProcessBusinessData(OaEntertainInstanceRelatedEditDTO oaEntertainInstanceRelatedEditDTO) {
        OaEntertain oaEntertain = oaEntertainInstanceRelatedEditDTO.getOaEntertain();
        oaEntertain.setUpdateTime(DateUtils.getNowDate());
        oaEntertain.setUpdateBy(SecurityUtils.getUsername());
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        baProcessInstanceRelated.setProcessInstanceId(oaEntertainInstanceRelatedEditDTO.getProcessInstanceId());
        baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(oaEntertain));
        try {
            baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaEntertain oaEntertain, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaEntertain.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_ENTERTAIN.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"招待申请审批提醒",msgContent,"oa",oaEntertain.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
