package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaContract;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaOrder;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractApprovalService;
import com.business.system.service.workflow.IOrderApprovalService;
import org.springframework.stereotype.Service;


/**
 * 订单申请审批结果:审批拒绝
 */
@Service("orderApprovalContent:processApproveResult:reject")
public class OrderApprovalRejectServiceImpl extends IOrderApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ORDER_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaContractStart checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
