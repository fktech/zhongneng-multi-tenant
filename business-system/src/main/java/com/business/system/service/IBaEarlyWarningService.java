package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaEarlyWarning;

/**
 * 预警Service接口
 *
 * @author single
 * @date 2024-05-29
 */
public interface IBaEarlyWarningService
{
    /**
     * 查询预警
     *
     * @param id 预警ID
     * @return 预警
     */
    public BaEarlyWarning selectBaEarlyWarningById(String id);

    /**
     * 查询预警列表
     *
     * @param baEarlyWarning 预警
     * @return 预警集合
     */
    public List<BaEarlyWarning> selectBaEarlyWarningList(BaEarlyWarning baEarlyWarning);

    public List<BaEarlyWarning> selectEarlyWarning(BaEarlyWarning baEarlyWarning);

    /**
     * 新增预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    public int insertBaEarlyWarning(BaEarlyWarning baEarlyWarning);

    /**
     * 修改预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    public int updateBaEarlyWarning(BaEarlyWarning baEarlyWarning);

    /**
     * 批量删除预警
     *
     * @param ids 需要删除的预警ID
     * @return 结果
     */
    public int deleteBaEarlyWarningByIds(String[] ids);

    /**
     * 删除预警信息
     *
     * @param id 预警ID
     * @return 结果
     */
    public int deleteBaEarlyWarningById(String id);

    /**
     * 终止预警
     *
     * @param baEarlyWarning 预警
     * @return 结果
     */
    public int cancelBaEarlyWarning(BaEarlyWarning baEarlyWarning);


}
