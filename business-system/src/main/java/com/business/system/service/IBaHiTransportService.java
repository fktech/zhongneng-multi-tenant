package com.business.system.service;

import com.business.system.domain.BaEnterprise;
import com.business.system.domain.BaHiEnterprise;
import com.business.system.domain.BaHiTransport;

import java.util.List;

/**
 * 运输Service接口
 *
 * @author ljb
 * @date 2022-12-13
 */
public interface IBaHiTransportService
{
    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    public BaHiTransport selectBaHiTransportById(String id);

    /**
     * 查询火运管理变更记录列表
     */
    List<BaHiTransport> selectChangeDataBaHiTransportList(BaHiTransport baHiTransport);

}
