package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * @Author: js
 * @Description: 财务认领收款审批接口 服务类
 * @date: 2023/1/31 10:01
 */
@Service
public class IClaimApprovalService {

    @Autowired
    protected BaCollectionMapper baCollectionMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaCollection baCollection = new BaCollection();
        baCollection.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baCollection = baCollectionMapper.selectBaCollectionById(baCollection.getId());
        String userId = String.valueOf(baCollection.getUserId());
        if(AdminCodeEnum.CLAIM_STATUS_PASS.getCode().equals(status)){
            //认领收款
            QueryWrapper<BaClaim> baClaimQueryWrapper = new QueryWrapper<>();
            baClaimQueryWrapper.eq("relation_id", baCollection.getId());
            List<BaClaim> baClaims = baClaimMapper.selectList(baClaimQueryWrapper);
            if(!CollectionUtils.isEmpty(baClaims)){
                baClaims.stream().forEach(item -> {
                    item.setClaimState("2"); //已认领
                    baClaimMapper.updateBaClaim(item);

                    //根据认领查询订单数据，统计更新认领订单累积回款
                    QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                    baClaimHistoryQueryWrapper.eq("claim_id", item.getId());
                    double currentCollection = 0;
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                    Map<String, Double> orderMap = new HashMap<>();
                    if(!CollectionUtils.isEmpty(baClaimHistories)){
                        for(BaClaimHistory baClaimHistory : baClaimHistories){
                            if(orderMap.containsKey(baClaimHistory.getOrderId())){
                                currentCollection = orderMap.get(baClaimHistory.getOrderId());
                                if(baClaimHistory.getCurrentCollection() != null){
                                    currentCollection = currentCollection + baClaimHistory.getCurrentCollection().doubleValue();
                                    orderMap.put(baClaimHistory.getOrderId(), currentCollection);
                                }
                            } else {
                                orderMap.put(baClaimHistory.getOrderId(), baClaimHistory.getCurrentCollection().doubleValue());
                            }
                            //认领收款全局编号
                            if(StringUtils.isNotEmpty(baClaimHistory.getGlobalNumber())){
                                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                                baGlobalNumber.setId(getRedisIncreID.getId());
                                baGlobalNumber.setBusinessId(baClaimHistory.getId());
                                baGlobalNumber.setCode(baClaimHistory.getGlobalNumber());
                                baGlobalNumber.setHierarchy("3");
                                baGlobalNumber.setType("8");
                                baGlobalNumber.setStartId(baClaimHistory.getOrderId());
                                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
                            }
                        }
                    }
                    if(!CollectionUtils.isEmpty(orderMap)){
                        orderMap.entrySet().stream().forEach((entry) -> {
                            BaOrder baOrder = baOrderMapper.selectBaOrderById(entry.getKey());
                            if(!ObjectUtils.isEmpty(baOrder)){
                                baOrder.setStayCollected(new BigDecimal(entry.getValue()).add(baOrder.getStayCollected()));
                                baOrder.setUpdateBy(SecurityUtils.getUsername());
                                baOrder.setUpdateTime(DateUtils.getNowDate());
                                baOrderMapper.updateBaOrder(baOrder);
                            }
                        });
                    }
                });
            }

            //全局编号
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baCollection.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baCollection.getId());
                baGlobalNumber.setCode(baCollection.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("7");
                baGlobalNumber.setStartId(baCollection.getStartId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baCollection, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.CLAIM_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baCollection, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baCollection, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baCollection.setState(status);
        baCollectionMapper.updateById(baCollection);

        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCollection baCollection, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("RL" + getRedisIncreID.getId());
        baMessage.setBusinessId(baCollection.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"收款审批提醒",msgContent,baMessage.getType(),baCollection.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaCollection checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaCollection baCollection = baCollectionMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baCollection == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CLAIM_001);
        }
        if (!AdminCodeEnum.CLAIM_STATUS_VERIFYING.getCode().equals(baCollection.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CLAIM_002);
        }
        return baCollection;
    }
}
