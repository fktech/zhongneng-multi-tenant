package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaCompany;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaTransport;
import com.business.system.mapper.BaCompanyMapper;
import com.business.system.mapper.BaContractStartMapper;
import com.business.system.service.IBaTransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaBidTransportMapper;
import com.business.system.domain.BaBidTransport;
import com.business.system.service.IBaBidTransportService;

/**
 * 合同启动投标信息运输Service业务层处理
 *
 * @author single
 * @date 2023-06-05
 */
@Service
public class BaBidTransportServiceImpl extends ServiceImpl<BaBidTransportMapper, BaBidTransport> implements IBaBidTransportService
{
    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private IBaTransportService baTransportService;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    /**
     * 查询合同启动投标信息运输
     *
     * @param id 合同启动投标信息运输ID
     * @return 合同启动投标信息运输
     */
    @Override
    public BaBidTransport selectBaBidTransportById(String id)
    {
        return baBidTransportMapper.selectBaBidTransportById(id);
    }

    /**
     * 查询合同启动投标信息运输列表
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 合同启动投标信息运输
     */
    @Override
    @DataScope(deptAlias = "bt",userAlias = "bt")
    public List<BaBidTransport> selectBaBidTransportList(BaBidTransport baBidTransport)
    {
        List<BaBidTransport> baBidTransports = baBidTransportMapper.baBidTransportList(baBidTransport);
        for (BaBidTransport bidTransport:baBidTransports) {
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(bidTransport.getTransportCompany());
            if(StringUtils.isNotNull(baCompany)){
                bidTransport.setTransportCompanyName(baCompany.getName());
            }
            //查询相关的运输信息
            BaTransport baTransport = new BaTransport();
            baTransport.setStartId(bidTransport.getStartId());
            baTransport.setRelationId(bidTransport.getId());
            baTransport.setJudge("1");
            List<BaTransport> baTransports = baTransportService.selectBaTransportList(baTransport);
            bidTransport.setBaTransports(baTransports);
            //查询合同启动名称
            if(StringUtils.isNotEmpty(bidTransport.getStartId())){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(bidTransport.getStartId());
                bidTransport.setStartName(baContractStart.getName());
            }
        }
        return baBidTransports;
    }

    /**
     * 新增合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    @Override
    public int insertBaBidTransport(BaBidTransport baBidTransport)
    {
        if(StringUtils.isEmpty(baBidTransport.getId())){
            baBidTransport.setId(getRedisIncreID.getId());
        }
        baBidTransport.setCreateTime(DateUtils.getNowDate());
        baBidTransport.setCreateBy(SecurityUtils.getUsername());
        baBidTransport.setUserId(SecurityUtils.getUserId());
        baBidTransport.setDeptId(SecurityUtils.getDeptId());
        return baBidTransportMapper.insertBaBidTransport(baBidTransport);
    }

    /**
     * 修改合同启动投标信息运输
     *
     * @param baBidTransport 合同启动投标信息运输
     * @return 结果
     */
    @Override
    public int updateBaBidTransport(BaBidTransport baBidTransport)
    {
        baBidTransport.setUpdateTime(DateUtils.getNowDate());
        baBidTransport.setUpdateBy(SecurityUtils.getUsername());
        return baBidTransportMapper.updateBaBidTransport(baBidTransport);
    }

    /**
     * 批量删除合同启动投标信息运输
     *
     * @param ids 需要删除的合同启动投标信息运输ID
     * @return 结果
     */
    @Override
    public int deleteBaBidTransportByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaBidTransport baBidTransport = baBidTransportMapper.selectBaBidTransportById(id);
                baBidTransport.setFlag(new Long(1));
                baBidTransportMapper.updateBaBidTransport(baBidTransport);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除合同启动投标信息运输信息
     *
     * @param id 合同启动投标信息运输ID
     * @return 结果
     */
    @Override
    public int deleteBaBidTransportById(String id)
    {
        return baBidTransportMapper.deleteBaBidTransportById(id);
    }


}
