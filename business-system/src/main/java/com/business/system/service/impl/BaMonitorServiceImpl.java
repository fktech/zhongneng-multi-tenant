package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaMonitorMapper;
import com.business.system.domain.BaMonitor;
import com.business.system.service.IBaMonitorService;

/**
 * 监控Service业务层处理
 *
 * @author single
 * @date 2024-06-04
 */
@Service
public class BaMonitorServiceImpl extends ServiceImpl<BaMonitorMapper, BaMonitor> implements IBaMonitorService
{
    @Autowired
    private BaMonitorMapper baMonitorMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询监控
     *
     * @param id 监控ID
     * @return 监控
     */
    @Override
    public BaMonitor selectBaMonitorById(String id)
    {
        BaMonitor baMonitor = baMonitorMapper.selectBaMonitorById(id);
        if(baMonitor.getAuthorityMark().equals("2")){
            String[] split = baMonitor.getAuthorityRange().split(",");
            String userName = "";
            for (String userId:split) {
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                userName = user.getNickName()+","+userName;
            }
            baMonitor.setAuthorityName(userName.substring(0,userName.length()-1));
        }else if(baMonitor.getAuthorityMark().equals("3")){
            String[] split = baMonitor.getAuthorityRange().split(",");
            String deptName = "";
            for (String deptId:split) {
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                deptName = dept.getDeptName()+","+deptName;
            }
            baMonitor.setAuthorityName(deptName.substring(0,deptName.length()-1));
        }
        return baMonitor;
    }

    /**
     * 查询监控列表
     *
     * @param baMonitor 监控
     * @return 监控
     */
    @Override
    public List<BaMonitor> selectBaMonitorList(BaMonitor baMonitor)
    {
        //获取当前登录人信息
        SysUser user = SecurityUtils.getSysUser();
        //管理员拦截
            if(null == user.getAdminFlag()){
                baMonitor.setUserId(SecurityUtils.getUserId());
                baMonitor.setDeptId(SecurityUtils.getDeptId());
            }
        return baMonitorMapper.selectBaMonitor(baMonitor);
    }

    /**
     * 新增监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    @Override
    public int insertBaMonitor(BaMonitor baMonitor)
    {
        //判断摄像头是否存在
        BaMonitor monitor = baMonitorMapper.selectBaMonitorById(baMonitor.getIndexCode());
        if(StringUtils.isNotNull(monitor)){
            return -1;
        }
        baMonitor.setId(baMonitor.getIndexCode());
        baMonitor.setCreateTime(DateUtils.getNowDate());
        //租户ID
        baMonitor.setTenantId(SecurityUtils.getCurrComId());
        baMonitor.setDeptId(SecurityUtils.getDeptId());
        baMonitor.setUserId(SecurityUtils.getUserId());
        int result = baMonitorMapper.insertBaMonitor(baMonitor);
        return result;
    }

    /**
     * 修改监控
     *
     * @param baMonitor 监控
     * @return 结果
     */
    @Override
    public int updateBaMonitor(BaMonitor baMonitor)
    {
        baMonitor.setUpdateTime(DateUtils.getNowDate());
        return baMonitorMapper.updateBaMonitor(baMonitor);
    }

    /**
     * 批量删除监控
     *
     * @param ids 需要删除的监控ID
     * @return 结果
     */
    @Override
    public int deleteBaMonitorByIds(String[] ids)
    {
        return baMonitorMapper.deleteBaMonitorByIds(ids);
    }

    /**
     * 删除监控信息
     *
     * @param id 监控ID
     * @return 结果
     */
    @Override
    public int deleteBaMonitorById(String id)
    {
        return baMonitorMapper.deleteBaMonitorById(id);
    }

}
