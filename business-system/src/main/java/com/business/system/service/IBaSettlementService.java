package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaSettlement;
import com.business.system.domain.dto.BaOrderDTO;
import com.business.system.domain.dto.NumsAndMoneyDTO;

/**
 * 结算信息Service接口
 *
 * @author ljb
 * @date 2022-12-26
 */
public interface IBaSettlementService
{
    /**
     * 查询结算信息
     *
     * @param id 结算信息ID
     * @return 结算信息
     */
    public BaSettlement selectBaSettlementById(String id);

    public BaSettlement selectSettlementById(String id);

    /**
     * 查询结算信息列表
     *
     * @param baSettlement 结算信息
     * @return 结算信息集合
     */
    public List<BaSettlement> selectBaSettlementList(BaSettlement baSettlement);

    /**
     * 新增结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    public int insertBaSettlement(BaSettlement baSettlement);

    public int insertSettlement(BaSettlement baSettlement);

    /**
     * 修改结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    public int updateBaSettlement(BaSettlement baSettlement);

    /**
     * 批量删除结算信息
     *
     * @param ids 需要删除的结算信息ID
     * @return 结果
     */
    public int deleteBaSettlementByIds(String[] ids);

    /**
     * 删除结算信息信息
     *
     * @param id 结算信息ID
     * @return 结果
     */
    public int deleteBaSettlementById(String id);

    /**
     * 相关订单信息
     */
     public List<BaOrderDTO> orderDto(String contractId,String type);

    /**
     * 结算申请列表
     */
    public List<BaSettlement> selectBaSettlement(BaSettlement baSettlement);
    /**
     * 查询结算表中总数及金额
     */
    public NumsAndMoneyDTO selectBaSettlementforup(BaSettlement baSettlement);
    /**
     * 查询结算表中总数及金本月度
     */
    public NumsAndMoneyDTO selectBaSettlementforMonth(BaSettlement baSettlement);
    /**
     * * 查询结算表中总数及金本年度
     */
    public NumsAndMoneyDTO selectBaSettlementforYear(BaSettlement baSettlement);


    /**
     * 已完结
     */
    public int settlementEnd(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 统计结算
     */
    public int  countBaSettlementList(BaSettlement baSettlement);

    /**
     * 查询多租户授权信息结算信息列表
     * @param baSettlement
     * @return
     */
    public List<BaSettlement> selectBaSettlementAuthorizedList(BaSettlement baSettlement);

    /**
     * 授权结算管理
     */
    public int authorizedBaSettlement(BaSettlement baSettlement);
}
