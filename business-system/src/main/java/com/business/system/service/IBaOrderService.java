package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaOrder;
import com.business.system.domain.vo.OrderStatVO;

/**
 * 订单Service接口
 *
 * @author ljb
 * @date 2022-12-09
 */
public interface IBaOrderService
{
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public BaOrder selectBaOrderById(String id);

    /**
     * 查询订单列表
     *
     * @param baOrder 订单
     * @return 订单集合
     */
    public List<BaOrder> selectBaOrderList(BaOrder baOrder);

    /**
     * 新增订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    public int insertBaOrder(BaOrder baOrder);

    /**
     * 修改订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    public int updateBaOrder(BaOrder baOrder);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    public int deleteBaOrderByIds(String[] ids);

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteBaOrderById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 订单完成
     * @param id
     * @return
     */
    public int complete(String id);


    /**
     * 根据类型统计订单数量(订单进度报表总量统计)
     */
    public OrderStatVO statOrderInfo(BaOrder baOrder);

    /**
     * 订单进度表(采购)
     * @return
     */
    public List<BaOrder> statPurchaseProgress(BaOrder baOrder);
}
