package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaContractForward;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractForwardApprovalService;
import com.business.system.service.workflow.IContractStartApprovalService;
import org.springframework.stereotype.Service;


/**
 * 合同结转审批结果:审批拒绝
 */
@Service("contractForwardApprovalContent:processApproveResult:reject")
public class ContractForwardApprovalRejectServiceImpl extends IContractForwardApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CONTRACTFORWARD_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaContractForward checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}