package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaProject;
import com.business.system.domain.BaProjectBeforehand;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IProjectApprovalService;
import com.business.system.service.workflow.IProjectBeforehandApprovalService;
import org.springframework.stereotype.Service;


/**
 * 预立项申请审批结果:审批拒绝
 */
@Service("projectBeforehandApprovalContent:processApproveResult:reject")
public class ProjectBeforehandApprovalRejectServiceImpl extends IProjectBeforehandApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.PROJECTBEFOREHAND_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaProjectBeforehand checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
