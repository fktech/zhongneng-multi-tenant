package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaGlobalNumber;
import com.business.system.domain.BaInvoice;
import com.business.system.domain.BaMessage;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaGlobalNumberMapper;
import com.business.system.mapper.BaInvoiceMapper;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 进项发票审批接口 服务类
 * @date: 2022/12/28 11:53
 */
@Service
public class IInputInvoiceApprovalService {

    @Autowired
    protected BaInvoiceMapper baInvoiceMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaInvoice baInvoice = new BaInvoice();
        baInvoice.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baInvoice = baInvoiceMapper.selectBaInvoiceById(baInvoice.getId());
        String userId = String.valueOf(baInvoice.getUserId());
        if(AdminCodeEnum.INPUTINVOICE_STATUS_PASS.getCode().equals(status)){

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //发票无业务状态

            //全局编号
            //全局编号关系表新增数据
            BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
            baGlobalNumber.setId(getRedisIncreID.getId());
            baGlobalNumber.setBusinessId(baInvoice.getId());
            baGlobalNumber.setCode(baInvoice.getGlobalNumber());
            baGlobalNumber.setHierarchy("6");
            baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
            baGlobalNumber.setCreateTime(DateUtils.getNowDate());
            baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);

            //插入消息通知信息
            this.insertMessage(baInvoice, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.INPUTINVOICE_STATUS_REJECT.getCode().equals(status)){

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baInvoice, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baInvoice, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baInvoice.setState(status);
        baInvoiceMapper.updateById(baInvoice);

        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaInvoice baInvoice, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baInvoice.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INPUTINVOICE.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"进项发票审批提醒",msgContent,baMessage.getType(),baInvoice.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaInvoice checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaInvoice baInvoice = baInvoiceMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baInvoice == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_INPUTINVOICE_001);
        }
        if (!AdminCodeEnum.INPUTINVOICE_STATUS_VERIFYING.getCode().equals(baInvoice.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_INPUTINVOICE_002);
        }
        return baInvoice;
    }
}
