package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;
import com.business.system.domain.BaClaim;
import com.business.system.domain.BaClaimHistory;
import com.business.system.domain.BaOrder;
import com.business.system.domain.dto.BaOrderDTO;
import com.business.system.domain.dto.MonthSumDTO;
import com.business.system.domain.dto.NumsAndMoneyDTO;

/**
 * 认领Service接口
 *
 * @author ljb
 * @date 2023-01-31
 */
public interface IBaClaimService
{
    /**
     * 查询认领
     *
     * @param id 认领ID
     * @return 认领
     */
    public BaClaim selectBaClaimById(String id);
    /**
     * 查询认领完的数居
     *
     * @param id 认领ID
     * @return 认领
     */
    public BaClaim selectBaClaimById1(String id);
    /**
     * 根据合同查询认领
     *
     * @param id 合同ID
     * @return 认领金额
     */
    public String selectBaClaimByrelationid(String id);


    /**
     * 查询认领列表
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public List<BaClaim> selectBaClaimList(BaClaim baClaim);
    /**
     * 查询财务统计中销售额
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolume(BaClaim baClaim);
    /**
     * 查询财务统计中销售额本年度
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByMonth(BaClaim baClaim);
    /**
     * 查询财务统计中营业额从本月去之前12个月的
     *
     * @param baClaim 认领
     * @return 认领集合
     */
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaClaim baClaim);
    /**
     * 查询根据业务类行查询收入金额
     *
     * @param
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeBybinusstype(List<BaClaimHistory> baClaimHistories);



    /**
     * 新增认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    public int insertBaClaim(BaClaim baClaim);

    /**
     * 修改认领
     *
     * @param baClaim 认领
     * @return 结果
     */
    public int updateBaClaim(BaClaim baClaim);

    /**
     * 批量删除认领
     *
     * @param ids 需要删除的认领ID
     * @return 结果
     */
    public int deleteBaClaimByIds(String[] ids);

    /**
     * 删除认领信息
     *
     * @param id 认领ID
     * @return 结果
     */
    public int deleteBaClaimById(String id);

    /**
     * 订单列表
     * @return
     */
    public List<BaOrderDTO> baOrderList();


    /**
     * 查询收款总数及金额
     */
    public NumsAndMoneyDTO selectBaClaimForUp(BaClaim baClaim);
    /**
     * 查询月度收款总数
     */
    public NumsAndMoneyDTO selectBaClaimForMonth(BaClaim baClaim);
    /**
     * * 查询年度收款总数
     */
    public NumsAndMoneyDTO selectBaClaimForYear(BaClaim baClaim);


}
