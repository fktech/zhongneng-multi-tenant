package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.system.mapper.BaAutomobuleStandardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.domain.BaAutomobuleStandard;
import com.business.system.service.IBaAutomobuleStandardService;

/**
 * 货源中运价Service业务层处理
 *
 * @author ljb
 * @date 2023-02-17
 */
@Service
public class BaAutomobuleStandardServiceImpl extends ServiceImpl<BaAutomobuleStandardMapper, BaAutomobuleStandard> implements IBaAutomobuleStandardService
{
    @Autowired
    private BaAutomobuleStandardMapper baAutomobuleStandardMapper;

    /**
     * 查询货源中运价
     *
     * @param id 货源中运价ID
     * @return 货源中运价
     */
    @Override
    public BaAutomobuleStandard selectBaAutomobuleStandardById(String id)
    {
        return baAutomobuleStandardMapper.selectBaAutomobuleStandardById(id);
    }

    /**
     * 查询货源中运价列表
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 货源中运价
     */
    @Override
    public List<BaAutomobuleStandard> selectBaAutomobuleStandardList(BaAutomobuleStandard baAutomobuleStandard)
    {
        return baAutomobuleStandardMapper.selectBaAutomobuleStandardList(baAutomobuleStandard);
    }

    /**
     * 新增货源中运价
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 结果
     */
    @Override
    public int insertBaAutomobuleStandard(BaAutomobuleStandard baAutomobuleStandard)
    {
        return baAutomobuleStandardMapper.insertBaAutomobuleStandard(baAutomobuleStandard);
    }

    /**
     * 修改货源中运价
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 结果
     */
    @Override
    public int updateBaAutomobuleStandard(BaAutomobuleStandard baAutomobuleStandard)
    {
        return baAutomobuleStandardMapper.updateBaAutomobuleStandard(baAutomobuleStandard);
    }

    /**
     * 批量删除货源中运价
     *
     * @param ids 需要删除的货源中运价ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobuleStandardByIds(String[] ids)
    {
        return baAutomobuleStandardMapper.deleteBaAutomobuleStandardByIds(ids);
    }

    /**
     * 删除货源中运价信息
     *
     * @param id 货源中运价ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobuleStandardById(String id)
    {
        return baAutomobuleStandardMapper.deleteBaAutomobuleStandardById(id);
    }

}
