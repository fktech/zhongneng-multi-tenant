package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaRailwayPlatform;
import com.business.system.mapper.BaRailwayPlatformMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaRailwayFreightTableMapper;
import com.business.system.domain.BaRailwayFreightTable;
import com.business.system.service.IBaRailwayFreightTableService;

/**
 * 铁路运费Service业务层处理
 *
 * @author ljb
 * @date 2022-11-29
 */
@Service
public class BaRailwayFreightTableServiceImpl extends ServiceImpl<BaRailwayFreightTableMapper, BaRailwayFreightTable> implements IBaRailwayFreightTableService
{
    @Autowired
    private BaRailwayFreightTableMapper baRailwayFreightTableMapper;

    @Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询铁路运费
     *
     * @param id 铁路运费ID
     * @return 铁路运费
     */
    @Override
    public BaRailwayFreightTable selectBaRailwayFreightTableById(String id)
    {
       BaRailwayFreightTable baRailwayFreightTable = baRailwayFreightTableMapper.selectBaRailwayFreightTableById(id);
       //始发站名称
        BaRailwayPlatform platformIdName = baRailwayPlatformMapper.selectBaRailwayPlatformById(baRailwayFreightTable.getPlatformId());
        baRailwayFreightTable.setPlatformIdName(platformIdName.getName());
        //终点站名称
        BaRailwayPlatform platformLevelName = baRailwayPlatformMapper.selectBaRailwayPlatformById(baRailwayFreightTable.getPlatformLevel());
        baRailwayFreightTable.setPlatformLevelName(platformLevelName.getName());
        return baRailwayFreightTable;
    }

    /**
     * 查询铁路运费列表
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 铁路运费
     */
    @Override
    public List<BaRailwayFreightTable> selectBaRailwayFreightTableList(BaRailwayFreightTable baRailwayFreightTable)
    {
        List<BaRailwayFreightTable> baRailwayFreightTables = baRailwayFreightTableMapper.selectBaRailwayFreightTableList(baRailwayFreightTable);
        for (BaRailwayFreightTable baRailwayFreightTable1:baRailwayFreightTables) {
            //始发站名称
            BaRailwayPlatform platformIdName = baRailwayPlatformMapper.selectBaRailwayPlatformById(baRailwayFreightTable1.getPlatformId());
            baRailwayFreightTable1.setPlatformIdName(platformIdName.getName());
            //终点站名称
            BaRailwayPlatform platformLevelName = baRailwayPlatformMapper.selectBaRailwayPlatformById(baRailwayFreightTable1.getPlatformLevel());
            baRailwayFreightTable1.setPlatformLevelName(platformLevelName.getName());
        }
        return baRailwayFreightTables;
    }

    /**
     * 新增铁路运费
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 结果
     */
    @Override
    public int insertBaRailwayFreightTable(BaRailwayFreightTable baRailwayFreightTable)
    {
        //线路不能重复
        QueryWrapper<BaRailwayFreightTable> freightTableQueryWrapper = new QueryWrapper<>();
        freightTableQueryWrapper.eq("line_name",baRailwayFreightTable.getLineName());
        BaRailwayFreightTable baRailwayFreightTable1 = baRailwayFreightTableMapper.selectOne(freightTableQueryWrapper);
        if(StringUtils.isNotNull(baRailwayFreightTable1)){
            return 0;
        }
        //起始点已存在
        QueryWrapper<BaRailwayFreightTable> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("platform_id",baRailwayFreightTable.getPlatformId());
        queryWrapper.eq("platform_level",baRailwayFreightTable.getPlatformLevel());
        BaRailwayFreightTable freightTable = baRailwayFreightTableMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(freightTable)){
            return -1;
        }
        baRailwayFreightTable.setId(getRedisIncreID.getId());
        baRailwayFreightTable.setCreateTime(DateUtils.getNowDate());
        return baRailwayFreightTableMapper.insertBaRailwayFreightTable(baRailwayFreightTable);
    }

    /**
     * 修改铁路运费
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 结果
     */
    @Override
    public int updateBaRailwayFreightTable(BaRailwayFreightTable baRailwayFreightTable)
    {
        baRailwayFreightTable.setUpdateTime(DateUtils.getNowDate());
        return baRailwayFreightTableMapper.updateBaRailwayFreightTable(baRailwayFreightTable);
    }

    /**
     * 批量删除铁路运费
     *
     * @param ids 需要删除的铁路运费ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayFreightTableByIds(String[] ids)
    {
        return baRailwayFreightTableMapper.deleteBaRailwayFreightTableByIds(ids);
    }

    /**
     * 删除铁路运费信息
     *
     * @param id 铁路运费ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayFreightTableById(String id)
    {
        return baRailwayFreightTableMapper.deleteBaRailwayFreightTableById(id);
    }

}
