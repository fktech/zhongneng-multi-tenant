package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 运输Service业务层处理
 *
 * @author ljb
 * @date 2022-12-13
 */
@Service
public class BaTransportServiceImpl extends ServiceImpl<BaTransportMapper, BaTransport> implements IBaTransportService
{
    private static final Logger log = LoggerFactory.getLogger(BaTransportServiceImpl.class);

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaHiTransportMapper baHiTransportMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private IBaContractStartService iBaContractStartService;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaQualityReportMapper baQualityReportMapper;

    @Autowired
    private IBaQualityReportService qualityReportService;

    @Autowired
    private ISysPostService sysPostService;

    @Autowired
    private PostName getName;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;

    @Autowired
    private BaAutomobileSettlementInvoiceMapper baAutomobileSettlementInvoiceMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private IBaQualityReportService iBaQualityReportService;

    @Autowired
    private IBaHiTransportService iBaHiTransportService;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private IBaTransportAutomobileService baTransportAutomobileService;

    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    @Override
    public BaTransport selectBaTransportById(String id)
    {
        BaTransport transport = baTransportMapper.selectBaTransportById(id);
        if(ObjectUtils.isEmpty(transport)){
            BaHiTransport baHiTransport = baHiTransportMapper.selectBaHiTransportById(id);
            if(!ObjectUtils.isEmpty(baHiTransport)){
                transport = baTransportMapper.selectBaTransportById(baHiTransport.getRelationId());
            }
        }
        if(!ObjectUtils.isEmpty(transport)){
            //合同启动名称
            if (StringUtils.isNotEmpty(transport.getStartId())) {
                BaContractStart contractStart = iBaContractStartService.selectBaContractStartById(transport.getStartId());
                if (!ObjectUtils.isEmpty(contractStart)) {
                    transport.setStartName(contractStart.getName());
                    transport.setEnterpriseName(contractStart.getEnterpriseName());
                    //合同启动简称
                    transport.setStartShorter(contractStart.getStartShorter());
                    //供应商名称
                    transport.setSupplierName(contractStart.getSupplierName());
                }
            }
            //订单编号
            if (StringUtils.isNotEmpty(transport.getOrderId())) {
                BaOrder baOrder = baOrderMapper.selectBaOrderById(transport.getOrderId());
                transport.setOrderNum(baOrder.getOrderName());
                transport.setReceiveId(baOrder.getEnterpriseId());
            }
            //始发站
            if (StringUtils.isNotEmpty(transport.getOrigin())) {
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getOrigin());
                if (StringUtils.isNotNull(baRailwayPlatform)) {
                    transport.setOriginName(baRailwayPlatform.getName());
                } else {
                    transport.setOriginName(transport.getOrigin());
                }
            }
            //收货单位
            if (StringUtils.isNotEmpty(transport.getReceiveCompany())) {
                transport.setReceiveId(transport.getReceiveCompany());
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(transport.getReceiveCompany());
                transport.setReceiveCompany(enterprise.getName());

            }
            //目的地
            if (StringUtils.isNotEmpty(transport.getDestination())) {
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getDestination());
                if (StringUtils.isNotNull(baRailwayPlatform)) {
                    transport.setDestinationName(baRailwayPlatform.getName());
                } else {
                    transport.setDestinationName(transport.getDestination());
                }
            }
            //实控人
            if (StringUtils.isNotEmpty(transport.getActualController())) {
                BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(transport.getActualController());
                transport.setActualName(baCounterpart.getName());
            }
            //商品名称
            if (StringUtils.isNotEmpty(transport.getGoodsId())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(transport.getGoodsId());
                transport.setGoodsName(baGoods.getName());
            }
            //托运公司名称
            if (StringUtils.isNotEmpty(transport.getTransportCompany())) {
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(transport.getTransportCompany());
                transport.setTransportCompanyName(baCompany.getName());
            }

            /*if(StringUtils.isNotEmpty(transport.getTransportStatus())){*/
            //发货信息
            QueryWrapper<BaCheck> queryWrapper0 = new QueryWrapper<>();
            queryWrapper0.eq("relation_id", transport.getId());
            queryWrapper0.eq("type", "1");
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper0);
            transport.setDelivery(baCheck);
            //验收信息
            /*if(transport.getTransportStatus().equals("2") || transport.getTransportStatus().equals("3")){*/
            QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id", transport.getId());
            queryWrapper1.eq("type", "2");
            BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper1);
            transport.setReceipt(baCheck1);
            /*}*/
            /*}*/
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(transport.getUserId());
            sysUser.setUserName(sysUser.getNickName());
            //岗位名称
            String name = getName.getName(sysUser.getUserId());
            if (name.equals("") == false) {
                sysUser.setPostName(name.substring(0, name.length() - 1));
            }
            transport.setUser(sysUser);

            List<String> processInstanceIds = new ArrayList<>();
            //流程实例ID
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", transport.getId());
            queryWrapper.eq("flag", 0);
            //queryWrapper.ne("approve_result",AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode());
            queryWrapper.orderByDesc("create_time");
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            transport.setProcessInstanceId(processInstanceIds);
            //运单信息
            if (StringUtils.isNotEmpty(transport.getAutomobileType())) {
                if (transport.getAutomobileType().equals("2")) {
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(transport.getAutomobileId());
                    //查看中储摘单
                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                    baShippingOrderCmst1.setYardId(transportCmst.getYardld());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    transport.setBaShippingOrderCmsts(baShippingOrderCmsts);
                }
                if (transport.getAutomobileType().equals("1")) {
                    //查看昕科运单
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(transport.getAutomobileId());
               /* QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                shippingOrderQueryWrapper.eq("supplier_no",baTransportAutomobile.getSupplierNo());
                List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                for (BaShippingOrder baShippingOrder:baShippingOrders) {
                    //查询付款明细
                    QueryWrapper<BaAutomobileSettlementDetail> SettlementQueryWrapper =new QueryWrapper<>();
                    queryWrapper.eq("external_order",baShippingOrder.getId());
                    List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(SettlementQueryWrapper);
                    for (BaAutomobileSettlementDetail baAutomobileSettlementDetail:baAutomobileSettlementDetails) {
                        if(baAutomobileSettlementDetail.getState().equals("2")){
                            baShippingOrder.setPaymentStatus("1");
                        }
                    }
                    //开票状态
                    if(StringUtils.isNotEmpty(baShippingOrder.getThdno())){
                        BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceByexternalorder(baShippingOrder.getThdno());
                        if(StringUtils.isNotNull(baAutomobileSettlementInvoice)){
                            baShippingOrder.setCheckFlag(baAutomobileSettlementInvoice.getCheckFlag());
                        }

                    }
                }*/
                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.waybillList(baTransportAutomobile.getSupplierNo());
                    for (BaShippingOrder baShippingOrder : shippingOrders) {
                        //查询付款明细
                        QueryWrapper<BaAutomobileSettlementDetail> SettlementQueryWrapper = new QueryWrapper<>();
                        SettlementQueryWrapper.eq("external_order", baShippingOrder.getId());
                        SettlementQueryWrapper.eq("state", "2");
                        List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(SettlementQueryWrapper);
                        if (baAutomobileSettlementDetails.size() > 0) {
                            baShippingOrder.setPaymentStatus("1");
                        }
                    }
                    transport.setBaShippingOrders(shippingOrders);
                }
            }
        }
        return transport;
    }

    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输
     */
    @Override
    @DataScope(deptAlias = "t",userAlias = "t")
    public List<BaTransport> selectBaTransportList(BaTransport baTransport)
    {
        List<BaTransport> baTransports = baTransportMapper.selectTransport(baTransport);
        for (BaTransport transport:baTransports) {
            /*//订单编号
            if(StringUtils.isNotEmpty(transport.getOrderId())){
               BaOrder baOrder = baOrderMapper.selectBaOrderById(transport.getOrderId());
               transport.setOrderNum(baOrder.getOrderName());
            }
            //始发站
            if(StringUtils.isNotEmpty(transport.getOrigin())){
              BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getOrigin());
              if(StringUtils.isNotNull(baRailwayPlatform)){
                  transport.setOriginName(baRailwayPlatform.getName());
              }else {
                  transport.setOriginName(transport.getOrigin());
              }
            }*/
            //验收的业务状态
            if(transport.getConfirmStatus().equals("3") && transport.getTransportStatus().equals("3") == false){
                transport.setTransportStatus("2");
                transport.setJudge("3");
            }
                //发货信息
                QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id",transport.getId());
                queryWrapper.eq("type","1");
                BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
                transport.setDelivery(baCheck);
                //验收信息
                    QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("relation_id",transport.getId());
                    queryWrapper1.eq("type","2");
                    BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper1);
                    transport.setReceipt(baCheck1);
            //完成状态
            if(transport.getConfirmStatus().equals("3") && transport.getTransportStatus().equals("3")){
                transport.setTransportStatus("4");
            }
            /*//目的地
            if(StringUtils.isNotEmpty(transport.getDestination())){
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getDestination());
                if(StringUtils.isNotNull(baRailwayPlatform)){
                    transport.setDestinationName(baRailwayPlatform.getName());
                }else {
                    transport.setDestinationName(transport.getDestination());
                }
            }*/
            //岗位名称
            String name = getName.getName(transport.getUserId());
            //发起人
            if(name.equals("") == false){
                transport.setUserName(sysUserMapper.selectUserById(transport.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                transport.setUserName(sysUserMapper.selectUserById(transport.getUserId()).getNickName());
            }
            if(StringUtils.isNotEmpty(transport.getAutomobileType())){
                if(transport.getAutomobileType().equals("1")){
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(transport.getAutomobileId());
                    //查看昕科对应运单数据
                    QueryWrapper<BaShippingOrder> baShippingOrderQueryWrapper = new QueryWrapper<>();
                    baShippingOrderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                    baShippingOrderQueryWrapper.eq("sign_for","1");
                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(baShippingOrderQueryWrapper);
                    //总出场净重
                    BigDecimal ccjweightTotal = new BigDecimal(0);
                    //总入场净重
                    BigDecimal rcjweightTotal = new BigDecimal(0);
                    for (BaShippingOrder shippingOrder:shippingOrders) {
                        ccjweightTotal = ccjweightTotal.add(shippingOrder.getCcjweight());
                        rcjweightTotal = rcjweightTotal.add(shippingOrder.getRcjweight());
                    }
                    transport.setLoadingCapacity(ccjweightTotal);
                    transport.setUnloadingVolume(rcjweightTotal);
                }
                if(transport.getAutomobileType().equals("2")){
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(transport.getAutomobileId());
                    //查看中储摘单
                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                    baShippingOrderCmst1.setYardId(transportCmst.getYardld());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    //确认收货
                    BigDecimal confirmedReceiveWeightTotal = new BigDecimal(0);
                    //确认发货
                    BigDecimal confirmedDeliveryWeightTotal = new BigDecimal(0);
                    for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                        if(baShippingOrderCmst.getConfirmedReceiveWeight() != null){
                            confirmedReceiveWeightTotal = confirmedReceiveWeightTotal.add(baShippingOrderCmst.getConfirmedReceiveWeight());
                        }
                        if(baShippingOrderCmst.getConfirmedDeliveryWeight() != null){
                            confirmedDeliveryWeightTotal = confirmedDeliveryWeightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                        }
                    }
                    transport.setLoadingCapacity(confirmedDeliveryWeightTotal);
                    transport.setUnloadingVolume(confirmedReceiveWeightTotal);
                }
            }
        }
        return baTransports;
    }

    @Override
    public List<BaTransport> selectBaTransport(BaTransport baTransport) {
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        return baTransportMapper.selectBaTransport(baTransport);
    }

    @Override
    public List<BaTransport> selectBaTransport1(BaTransport baTransport) {
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        return baTransportMapper.selectBaTransport1(baTransport);
    }


    /**
     * 新增运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertBaTransport(BaTransport baTransport)
    {
        baTransport.setId(getRedisIncreID.getId());
        baTransport.setCreateTime(DateUtils.getNowDate());
        baTransport.setUserId(SecurityUtils.getUserId());
        baTransport.setDeptId(SecurityUtils.getDeptId());
        baTransport.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baTransport.setTenantId(SecurityUtils.getCurrComId());
        //baTransport.setTransportStatus("1");
        baTransport.setConfirmStatus("1");
        BaContractStart contractStart = null;
        //合同启动名称
        if(StringUtils.isNotEmpty(baTransport.getStartId())){
            contractStart = iBaContractStartService.selectBaContractStartById(baTransport.getStartId());
            //判断业务归属公司是否是上海子公司
            if("121123456".equals(contractStart.getBelongCompanyId())) {
                //流程发起状态
                baTransport.setWorkState("1");
                //默认审批流程已通过
                baTransport.setState(AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode());
            }
        }

        //判断合同启动全局编号
        if(StringUtils.isNotEmpty(baTransport.getStartGlobalNumber())){
            //全局编号
            QueryWrapper<BaTransport> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.eq("flag",0);
            queryWrapper.isNotNull("serial_number");
            queryWrapper.orderByDesc("serial_number");
            List<BaTransport> baTransports = baTransportMapper.selectList(queryWrapper);
            if(baTransports.size() > 0){
                Integer serialNumber = baTransports.get(0).getSerialNumber();
                baTransport.setSerialNumber(serialNumber+1);
                if(serialNumber < 10){
                    baTransport.setGlobalNumber(baTransport.getStartGlobalNumber()+"-"+"YS"+"-"+"000"+baTransport.getSerialNumber());
                }else if(serialNumber >= 10){
                    baTransport.setGlobalNumber(baTransport.getStartGlobalNumber()+"-"+"YS"+"-"+"00"+baTransport.getSerialNumber());
                }else if(serialNumber >= 100){
                    baTransport.setGlobalNumber(baTransport.getStartGlobalNumber()+"-"+"YS"+"-"+"0"+baTransport.getSerialNumber());
                }else if(serialNumber >= 1000){
                    baTransport.setGlobalNumber(baTransport.getStartGlobalNumber()+"-"+"YS"+"-"+baTransport.getSerialNumber());
                }
            }else {
                baTransport.setSerialNumber(1);
                baTransport.setGlobalNumber(baTransport.getStartGlobalNumber()+"-"+"YS"+"-"+"000"+baTransport.getSerialNumber());
            }
            //全局编号
            BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
            baGlobalNumber.setId(getRedisIncreID.getId());
            baGlobalNumber.setBusinessId(baTransport.getId());
            baGlobalNumber.setCode(baTransport.getGlobalNumber());
            baGlobalNumber.setHierarchy("3");
            baGlobalNumber.setType("4");
            baGlobalNumber.setStartId(baTransport.getStartId());
            baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
            baGlobalNumber.setCreateTime(DateUtils.getNowDate());
            baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
        }

        //关闭流程开始
        BaOrder baOrder = baOrderMapper.selectBaOrderById(baTransport.getOrderId());
        if(!ObjectUtils.isEmpty(baOrder)){
            BaContract baContract = baContractMapper.selectBaContractById(baOrder.getCompanyId());//查询合同
            if(!ObjectUtils.isEmpty(baContract)){
                BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
                //更新项目阶段
                baProject.setProjectStage(7); //订单发运
                baProjectMapper.updateBaProject(baProject);
            }
        }
        //关闭流程结束
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(),SecurityUtils.getCurrComId());
        baTransport.setFlowId(flowId);
        Integer result = baTransportMapper.insertBaTransport(baTransport);
        //关联数质量报告
        if(StringUtils.isNotEmpty(baTransport.getAnnex())){
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baTransport.getId());
            baQualityReport.setStarId(baTransport.getStartId());
            baQualityReport.setCirculationType("1");
            baQualityReport.setAttachmentType("1");
            baQualityReport.setAnnex(baTransport.getAnnex());
            //火运
            if(baTransport.getTransportType().equals("1")){
                baQualityReport.setTransportWay("1");
            }
            //汽运
            if(baTransport.getTransportType().equals("2")){
                baQualityReport.setTransportWay("2");
            }
            qualityReportService.insertBaQualityReport(baQualityReport);
        }
        if(result > 0){
            if(!ObjectUtils.isEmpty(contractStart)) {
                BaTransport transport = baTransportMapper.selectBaTransportById(baTransport.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(transport, AdminCodeEnum.TRANSPORT_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baTransportMapper.selectBaTransportById(baTransport.getId());
                    return 0;
                } else {
                    BaTransport baTransport1 = baTransportMapper.selectBaTransportById(transport.getId());
                    baTransport1.setState(AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode());
                    baTransportMapper.updateBaTransport(baTransport1);
                    if (StringUtils.isNotEmpty(baTransport1.getOrderId())) {
                        baOrder = baOrderMapper.selectBaOrderById(baTransport1.getOrderId());
                        baOrder.setTransportState(1);
                        baOrderMapper.updateBaOrder(baOrder);
                    }
                    if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
                        AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(baProcessInstancesRuntimeVO.getId());
                        //判断流程实例是否结束
                        String processInstanceIsEndResult = "";
                        if (!ObjectUtils.isEmpty(ajaxResult)) {
                            processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
                        }
                        // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
                        if (processInstanceIsEndResult.equals(Constants.SUCCESS)) {
                            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(baProcessInstancesRuntimeVO.getId());
                            if (!ObjectUtils.isEmpty(applyBusinessDataByProcessInstance)) {
                                applyBusinessDataByProcessInstance.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
                                baProcessInstanceRelatedMapper.updateById(applyBusinessDataByProcessInstance);
                            }
                            BaTransport baTransport2 = baTransportMapper.selectBaTransportById(transport.getId());
                            baTransport2.setState(AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode());
                            baTransportMapper.updateBaTransport(baTransport2);
                        }
                    }
                }
            }
        }
        return 1;
    }

    /**
     * 提交运输审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaTransport transport, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(transport.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(transport.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        //获取流程实例关联的业务对象
        BaTransport baTransport = this.selectBaTransportById(transport.getId());
        SysUser sysUser = iSysUserService.selectUserById(baTransport.getUserId());
        baTransport.setUserName(sysUser.getUserName());
        baTransport.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baTransport, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 提交运输审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstancesHi(BaHiTransport transport, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(transport.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(transport.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        //获取流程实例关联的业务对象
        SysUser sysUser = iSysUserService.selectUserById(transport.getUserId());
        transport.setUserName(sysUser.getUserName());
        transport.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(transport, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.TRANSPORT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateBaTransport(BaTransport baTransport)
    {
        //原数据
        BaTransport baTransport1 = baTransportMapper.selectBaTransportById(baTransport.getId());
        baTransport.setUpdateTime(DateUtils.getNowDate());
        baTransport.setUpdateBy(SecurityUtils.getUsername());
        //修改创建时间
        baTransport.setCreateTime(baTransport.getUpdateTime());
        Integer result = baTransportMapper.updateBaTransport(baTransport);
        if(result > 0 && !baTransport.getState().equals(AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode())){
           BaTransport transport = baTransportMapper.selectBaTransportById(baTransport.getId());
           //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(transport, AdminCodeEnum.TRANSPORT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baTransportMapper.updateBaTransport(baTransport1);
                return 0;
            }else {
                transport.setState(AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode());
                baTransportMapper.updateBaTransport(transport);
            }
        }
        return result;
    }

    /**
     * 批量删除运输
     *
     * @param ids 需要删除的运输ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for(String id:ids){
               BaTransport baTransport = baTransportMapper.selectBaTransportById(id);
               baTransport.setFlag(1);
               Integer result = baTransportMapper.updateBaTransport(baTransport);
               if(result > 0 && StringUtils.isNotEmpty(baTransport.getOrderId())){
                   //删除运输信息改变订单运输状态
                  BaOrder baOrder = baOrderMapper.selectBaOrderById(baTransport.getOrderId());
                  baOrder.setTransportState(0);
                  baOrderMapper.updateBaOrder(baOrder);
               }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除运输信息
     *
     * @param id 运输ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportById(String id)
    {
        return baTransportMapper.deleteBaTransportById(id);
    }

    @Override
    public int revoke(String id) {
        if(StringUtils.isNotEmpty(id)){
          BaTransport baTransport = baTransportMapper.selectBaTransportById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baTransport.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.TRANSPORT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baTransport.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //运输流程审批状态修改
                baTransport.setState(AdminCodeEnum.TRANSPORT_STATUS_WITHDRAW.getCode());
                baTransportMapper.updateBaTransport(baTransport);
                String userId = String.valueOf(baTransport.getUserId());
                //撤销修改订单运输状态
                /*if(StringUtils.isNotEmpty(baTransport.getOrderId())){
                    BaOrder baOrder = baOrderMapper.selectBaOrderById(baTransport.getOrderId());
                    baOrder.setTransportState(0);
                    baOrderMapper.updateBaOrder(baOrder);
                }*/
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baTransport, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaTransport baTransport, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baTransport.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public int delivery(BaTransport baTransport) {
        //查询运输信息
        BaTransport baTransport1 = baTransportMapper.selectBaTransportById(baTransport.getId());
        int result = 0;
        if(StringUtils.isNotNull(baTransport.getDelivery())){
            BaCheck baCheck = baTransport.getDelivery();
            //过滤无车承运
        if(baTransport1.getTransportType().equals("4") == false) {
            if (StringUtils.isNotEmpty(baCheck.getId())) {
                result = baCheckMapper.updateBaCheck(baCheck);
            } else {
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(1);
                //查看运输单是否存在收货数据
                BaCheck baCheck1 = new BaCheck();
                baCheck1.setRelationId(baCheck.getRelationId());
                baCheck1.setType(baCheck.getType());
                List<BaCheck> baChecks = baCheckMapper.selectBaCheckList(baCheck1);
                if(baChecks.size() > 0){
                    return -1;
                }
                result = baCheckMapper.insertBaCheck(baCheck);
            }
        }
            //数质量报告
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baTransport1.getId());
            baQualityReport.setStarId(baTransport1.getStartId());
            baQualityReport.setCirculationType("2");
            if(baTransport1.getTransportType().equals("2")){
                //汽运发货新增附件
                if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                    baQualityReport.setTransportWay("2");
                    baQualityReport.setAttachmentType("1");
                    baQualityReport.setAnnex(baCheck.getEnclosure());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
            }
            if(baTransport1.getTransportType().equals("4")){
                //货源数据绑定运单
                if(StringUtils.isNotEmpty(baTransport.getAutomobileType())){
                    if(baTransport.getAutomobileType().equals("1")){
                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransport.getAutomobileId());
                        baTransportAutomobile.setOrderId(baTransport.getId());
                        result = baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                    }
                    if(baTransport.getAutomobileType().equals("2")){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(baTransport.getAutomobileId());
                        transportCmst.setRelationId(baTransport.getId());
                        result = baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                }
                baTransport1.setAutomobileId(baTransport.getAutomobileId());
                baTransport1.setAutomobileType(baTransport.getAutomobileType());
                baTransportMapper.updateBaTransport(baTransport1);
            }
            if(baTransport1.getTransportType().equals("1")){
                baQualityReport.setTransportWay("1");
                //火运其他附件
                if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                    baQualityReport.setAttachmentType("6");
                    baQualityReport.setAnnex(baCheck.getEnclosure());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运随车化验单
                if(StringUtils.isNotEmpty(baCheck.getLaboratoryTest())){
                    baQualityReport.setAttachmentType("2");
                    baQualityReport.setAnnex(baCheck.getLaboratoryTest());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运铁路大票
                if(StringUtils.isNotEmpty(baCheck.getRailwayTicket())){
                    baQualityReport.setAttachmentType("3");
                    baQualityReport.setAnnex(baCheck.getRailwayTicket());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运轨道衡
                if(StringUtils.isNotEmpty(baCheck.getTrackScale())){
                    baQualityReport.setAttachmentType("4");
                    baQualityReport.setAnnex(baCheck.getTrackScale());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运随车附表
                if(StringUtils.isNotEmpty(baCheck.getAccompanyingSchedule())){
                    baQualityReport.setAttachmentType("5");
                    baQualityReport.setAnnex(baCheck.getAccompanyingSchedule());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
            }
        }
       if(result > 0){
           baTransport = baTransportMapper.selectBaTransportById(baTransport.getId());
          baTransport.setConfirmStatus("2");
          baTransport.setTransportStatus("1");
          baTransportMapper.updateBaTransport(baTransport);
       }
        return result;
    }

    @Override
    public int receipt(BaTransport baTransport) {
        //查询运输信息
        BaTransport baTransport1 = baTransportMapper.selectBaTransportById(baTransport.getId());
        int result = 0;
        if(StringUtils.isNotNull(baTransport.getReceipt())){
            BaCheck baCheck = baTransport.getReceipt();
            if(StringUtils.isNotEmpty(baCheck.getId())){
                result = baCheckMapper.updateBaCheck(baCheck);
            }else {
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(2);
                //查看运输单是否存在收货数据
                BaCheck baCheck1 = new BaCheck();
                baCheck1.setRelationId(baCheck.getRelationId());
                baCheck1.setType(baCheck.getType());
                List<BaCheck> baChecks = baCheckMapper.selectBaCheckList(baCheck1);
                if(baChecks.size() > 0){
                    return -1;
                }
                result = baCheckMapper.insertBaCheck(baCheck);
            }
            //数质量报告
            if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                //验收附件
                BaQualityReport baQualityReport = new BaQualityReport();
                baQualityReport.setTransportId(baTransport1.getId());
                baQualityReport.setStarId(baTransport1.getStartId());
                baQualityReport.setCirculationType("3");
                baQualityReport.setAttachmentType("1");
                //火运验收
                if(baTransport1.getTransportType().equals("1")){
                    baQualityReport.setTransportWay(baTransport1.getTransportType());
                }
                //汽运验收
                if(baTransport1.getTransportType().equals("2")){
                    baQualityReport.setTransportWay(baTransport1.getTransportType());
                }
                baQualityReport.setAnnex(baCheck.getEnclosure());
                qualityReportService.insertBaQualityReport(baQualityReport);
            }
        }
        if(result > 0){
             baTransport = baTransportMapper.selectBaTransportById(baTransport.getId());
            //baTransport.setTransportStatus("2");
            baTransport.setConfirmStatus("3");
            baTransportMapper.updateBaTransport(baTransport);
        }
        //查询运输信息
        BaTransport transport = baTransportMapper.selectBaTransportById(baTransport.getId());
        if(StringUtils.isNotEmpty(transport.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(transport.getStartId());
            if(StringUtils.isNotEmpty(baContractStart.getStartType())){
                //完结的合同启动不更改原有状态
                if(baContractStart.getStartType().equals("4") == false){
                    baContractStart.setStartType("2");
                }
            }
            baContractStartMapper.updateBaContractStart(baContractStart);
        }
        return result;
    }

    @Override
    public int transfer(BaTransport baTransport) {
        //查询运输信息
        BaTransport baTransport1 = baTransportMapper.selectBaTransportById(baTransport.getId());
        baTransport.setTransportStatus("3");
        //数质量报告货转
        if(StringUtils.isNotEmpty(baTransport.getTransferVoucher())){
            //货转附件
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baTransport1.getId());
            baQualityReport.setStarId(baTransport1.getStartId());
            baQualityReport.setCirculationType("4");
            baQualityReport.setAttachmentType("1");
            //火运货转
            if(baTransport1.getTransportType().equals("1")){
                baQualityReport.setTransportWay(baTransport1.getTransportType());
            }
            //汽运货转
            if(baTransport1.getTransportType().equals("2")){
                baQualityReport.setTransportWay(baTransport1.getTransportType());
            }
            baQualityReport.setAnnex(baTransport.getTransferVoucher());
            baQualityReport.setOtherAnnex(baTransport.getOtherAnnex());
            qualityReportService.insertBaQualityReport(baQualityReport);
        }
        return baTransportMapper.updateBaTransport(baTransport);
    }

    @Override
    public List<BaTransport> select(String startId) {

        List<BaTransport> transports = new ArrayList<>();
        //查询运输信息
        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
        transportQueryWrapper.eq("start_id",startId);
        transportQueryWrapper.eq("flag",0);
        transportQueryWrapper.eq("payment_status",0);
        transportQueryWrapper.eq("pitch_on",0);
        List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
        for (BaTransport baTransport:baTransports) {
            //查询发货信息
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baTransport.getId());
            checkQueryWrapper.eq("type",1);
            BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
            if(StringUtils.isNotNull(baCheck)){
                baTransport.setDelivery(baCheck);
            }
            transports.add(baTransport);
        }
        return transports;
    }

    @Override
    public int saveButton(BaTransport baTransport) {
        if(StringUtils.isNotNull(baTransport.getDelivery())){
            BaCheck baCheck = baTransport.getDelivery();
            if(StringUtils.isNotEmpty(baCheck.getId())){
                baCheckMapper.updateBaCheck(baCheck);
            }else {
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(1);
                //查看运输单是否存在收货数据
                BaCheck baCheck1 = new BaCheck();
                baCheck1.setRelationId(baCheck.getRelationId());
                baCheck1.setType(baCheck.getType());
                List<BaCheck> baChecks = baCheckMapper.selectBaCheckList(baCheck1);
                if(baChecks.size() > 0){
                    return -1;
                }
                baCheckMapper.insertBaCheck(baCheck);
            }
        }
        if(StringUtils.isNotNull(baTransport.getReceipt())){
            BaCheck baCheck = baTransport.getReceipt();
            if(StringUtils.isNotEmpty(baCheck.getId())){
                baCheckMapper.updateBaCheck(baCheck);
            }else {
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(2);
                //查看运输单是否存在收货数据
                BaCheck baCheck1 = new BaCheck();
                baCheck1.setRelationId(baCheck.getRelationId());
                baCheck1.setType(baCheck.getType());
                List<BaCheck> baChecks = baCheckMapper.selectBaCheckList(baCheck1);
                if(baChecks.size() > 0){
                    return -1;
                }
                baCheckMapper.insertBaCheck(baCheck);
            }
        }
        return 1;
    }

    @Override
    public Integer updateBaCheckTransferRights(BaCheck baCheck) {
        BaCheck baCheck1 = baCheckMapper.selectBaCheckById(baCheck.getId());
        baCheck1.setTransferFlag("1"); // 已填充货转信息
        baCheck1.setTransferRights(baCheck.getTransferRights());
        return baCheckMapper.updateBaCheck(baCheck1);
    }


    @Override
    public BigDecimal countTransportList(CountTransportDTO countTransportDTO) {
        //租户ID
        countTransportDTO.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal = baTransportMapper.countTransportList(countTransportDTO);
        if(bigDecimal == null){
            return new BigDecimal(0);
        }
        return bigDecimal;
    }

    @Override
    public BigDecimal storageCountTransportList(BaTransportAutomobile baTransportAutomobile) {
        baTransportAutomobile.setTenantId(SecurityUtils.getCurrComId());
        baTransportAutomobile.setState("1");
        //入库
        List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileService.sourceGoodsDTOList(baTransportAutomobile);
        //出库
        baTransportAutomobile.setState("2");
        List<sourceGoodsDTO> sourceGoodsDTOS1 = baTransportAutomobileService.sourceGoodsDTOList(baTransportAutomobile);
        //出库+入库
        sourceGoodsDTOS.addAll(sourceGoodsDTOS1);
        //仓储累计发运
        BigDecimal bigDecimal = new BigDecimal(0);
        for (sourceGoodsDTO goodsDTO:sourceGoodsDTOS) {
             bigDecimal = bigDecimal.add(goodsDTO.getSaleNum());
        }
        return bigDecimal;
    }

    /**
     * 变更火车运输
     */
    @Override
    public int changeBaTransport(BaTransport baTransport) throws Exception {

        //获取原火车运输信息
        BaTransport baTransport1 = this.selectBaTransportById(baTransport.getId());
//        if("".equals(decideChange(baTransport, baTransport1))){
//            return -1;
//        }

        //伪删除历史表之前火车运输
        baHiTransportMapper.deleteBaHiTransportByIds(new String[]{baTransport.getId()});

        //定义历史表数据
        BaHiTransport baHiTransport = new BaHiTransport();
        BeanUtils.copyBeanProp(baHiTransport, baTransport);
        String redisIncreID = getRedisIncreID.getId();
        baHiTransport.setId(redisIncreID);

        baTransport1.setApproveFlag("2"); //变更
        int result = baTransportMapper.updateBaTransport(baTransport1);

        //设置变更标识
        baHiTransport.setApproveFlag("2"); //变更
        //创建时间
        baHiTransport.setCreateTime(DateUtils.getNowDate());
        baHiTransport.setChangeTime(DateUtils.getNowDate());
        baHiTransport.setChangeBy(SecurityUtils.getUsername());
        //变更，插入历史信息到历史表
        baHiTransport.setRelationId(baTransport.getId());
        baHiTransport.setFlag(0);
        if(!StringUtils.isEmpty(baTransport1.getParentId())){
            baHiTransport.setParentId(baTransport1.getParentId());
        }

        //历史发货
        if(StringUtils.isNotNull(baTransport.getDelivery())){
            BaCheck baCheck = baTransport.getDelivery();
            //过滤无车承运
            if(baTransport.getTransportType().equals("4") == false) {
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(1);
                baCheck.setRelationId(baHiTransport.getId());
                result = baCheckMapper.insertBaCheck(baCheck);
            }
            //数质量报告
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baHiTransport.getId());
            baQualityReport.setStarId(baHiTransport.getStartId());
            baQualityReport.setCirculationType("2");
            if(baTransport.getTransportType().equals("2")){
                //汽运发货新增附件
                if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                    baQualityReport.setTransportWay("2");
                    baQualityReport.setAttachmentType("1");
                    baQualityReport.setAnnex(baCheck.getEnclosure());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
            }
            if(baTransport.getTransportType().equals("1")){
                baQualityReport.setTransportId(baHiTransport.getId());
                baQualityReport.setTransportWay("1");
                //火运其他附件
                if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                    baQualityReport.setAttachmentType("6");
                    baQualityReport.setAnnex(baCheck.getEnclosure());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运随车化验单
                if(StringUtils.isNotEmpty(baCheck.getLaboratoryTest())){
                    baQualityReport.setAttachmentType("2");
                    baQualityReport.setAnnex(baCheck.getLaboratoryTest());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运铁路大票
                if(StringUtils.isNotEmpty(baCheck.getRailwayTicket())){
                    baQualityReport.setAttachmentType("3");
                    baQualityReport.setAnnex(baCheck.getRailwayTicket());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运轨道衡
                if(StringUtils.isNotEmpty(baCheck.getTrackScale())){
                    baQualityReport.setAttachmentType("4");
                    baQualityReport.setAnnex(baCheck.getTrackScale());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
                //火运随车附表
                if(StringUtils.isNotEmpty(baCheck.getAccompanyingSchedule())){
                    baQualityReport.setAttachmentType("5");
                    baQualityReport.setAnnex(baCheck.getAccompanyingSchedule());
                    qualityReportService.insertBaQualityReport(baQualityReport);
                }
            }
        }
        //历史验收
        if(StringUtils.isNotNull(baTransport.getReceipt())){
            BaCheck baCheck = baTransport.getReceipt();
            baCheck.setId(getRedisIncreID.getId());
            baCheck.setType(2);
            baCheck.setRelationId(baHiTransport.getId());
            result = baCheckMapper.insertBaCheck(baCheck);
            //数质量报告
            if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                //验收附件
                BaQualityReport baQualityReport = new BaQualityReport();
                baQualityReport.setTransportId(baHiTransport.getId());
                baQualityReport.setStarId(baTransport.getStartId());
                baQualityReport.setCirculationType("3");
                baQualityReport.setAttachmentType("1");
                //火运验收
                if(baTransport1.getTransportType().equals("1")){
                    baQualityReport.setTransportWay(baTransport1.getTransportType());
                }
                //汽运验收
                if(baTransport1.getTransportType().equals("2")){
                    baQualityReport.setTransportWay(baTransport1.getTransportType());
                }
                baQualityReport.setAnnex(baCheck.getEnclosure());
                qualityReportService.insertBaQualityReport(baQualityReport);
            }
        }

        //历史数质量报告货转
        if(StringUtils.isNotEmpty(baTransport.getTransferVoucher())){
            //货转附件
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baHiTransport.getId());
            baQualityReport.setStarId(baTransport.getStartId());
            baQualityReport.setCirculationType("4");
            baQualityReport.setAttachmentType("1");
            //火运货转
            if(baTransport.getTransportType().equals("1")){
                baQualityReport.setTransportWay(baTransport.getTransportType());
            }
            //汽运货转
            if(baTransport.getTransportType().equals("2")){
                baQualityReport.setTransportWay(baTransport.getTransportType());
            }
            baQualityReport.setAnnex(baTransport.getTransferVoucher());
            qualityReportService.insertBaQualityReport(baQualityReport);
        }

        baHiTransportMapper.insertBaHiTransport(baHiTransport);

        //变更包含启动流程字段启动流程
//        if("isStart".equals(decideChange(baTransport, baTransport1))){
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baTransport.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstancesHi(baHiTransport, AdminCodeEnum.TRANSPORT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                //回滚原收费标准
//                if(!ObjectUtils.isEmpty(baChargingStandards1)){
//                    baChargingStandardsMapper.updateBaChargingStandards(baChargingStandards1);
//                }
                qualityReportService.deleteBaQualityReportById(baHiTransport.getId());
                baHiTransportMapper.deleteBaHiTransportById(baHiTransport.getId());
                return 0;
            }else {
                //历史版本ID
                String id = baHiTransport.getId();
                //变更前版本
                BaTransport baTransportBefore = this.selectBaTransportById(baHiTransport.getRelationId());
                //变更状态
                baHiTransport = iBaHiTransportService.selectBaHiTransportById(baHiTransport.getId());
                if(!ObjectUtils.isEmpty(baHiTransport)) {
                    baHiTransport.setApproveFlag("0");
                    baHiTransportMapper.updateById(baHiTransport);
                }
                //原发货修改为新发货
                if (StringUtils.isNotNull(baHiTransport.getDelivery())) {
                    BaCheck baCheck = baHiTransport.getDelivery();
                    //过滤无车承运
                    if (baTransport.getTransportType().equals("4") == false) {
                        if (StringUtils.isNotEmpty(baCheck.getId())) {
                            baCheck.setRelationId(baTransport.getId());
                            baCheckMapper.updateBaCheck(baCheck);
                        }
                    }

                    //数质量报告
                    BaQualityReport baQualityReport = new BaQualityReport();
                    if(StringUtils.isNotEmpty(baHiTransport.getId())){
                        baQualityReport.setTransportId(baHiTransport.getId());
                        baQualityReport.setStarId(baHiTransport.getStartId());
                        List<BaQualityReport> baQualityReports = qualityReportService.selectBaQualityReportList(baQualityReport);
                        if(!CollectionUtils.isEmpty(baQualityReports)){
                            for(BaQualityReport baQualityReport1 : baQualityReports){
                                baQualityReport1.setTransportId(baTransport.getId());
                                baQualityReport.setStarId(baTransport.getStartId());
                                qualityReportService.updateBaQualityReport(baQualityReport1);
                            }
                        }
                    }
                }

                //新发货修改为原发货
                if (StringUtils.isNotNull(baTransportBefore.getDelivery())) {
                    BaCheck baCheck = baTransportBefore.getDelivery();
                    //过滤无车承运
                    if (baTransportBefore.getTransportType().equals("4") == false) {
                        if (StringUtils.isNotEmpty(baCheck.getId())) {
                            baCheck.setRelationId(baHiTransport.getId());
                            baCheckMapper.updateBaCheck(baCheck);
                        }
                    }

                    //数质量报告
                    BaQualityReport baQualityReport = new BaQualityReport();
                    if(StringUtils.isNotEmpty(baTransportBefore.getId())){
                        baQualityReport.setTransportId(baTransportBefore.getId());
                        baQualityReport.setStarId(baTransportBefore.getStartId());
                        List<BaQualityReport> baQualityReports = qualityReportService.selectBaQualityReportList(baQualityReport);
                        if(!CollectionUtils.isEmpty(baQualityReports)){
                            for(BaQualityReport baQualityReport1 : baQualityReports){
                                baQualityReport1.setTransportId(baHiTransport.getId());
                                baQualityReport1.setStarId(baHiTransport.getStartId());
                                qualityReportService.updateBaQualityReport(baQualityReport1);
                            }
                        }
                    }
                }

                //原验收修改为新验收
                if (StringUtils.isNotNull(baHiTransport.getReceipt())) {
                    BaCheck baCheck = baHiTransport.getReceipt();
                    if (StringUtils.isNotEmpty(baCheck.getId())) {
                        baCheck.setRelationId(baTransportBefore.getId());
                        baCheckMapper.updateBaCheck(baCheck);
                    }
                }

                //新验收修改为原验收
                if (StringUtils.isNotNull(baTransportBefore.getReceipt())) {
                    BaCheck baCheck = baTransportBefore.getReceipt();
                    if (StringUtils.isNotEmpty(baCheck.getId())) {
                        baCheck.setRelationId(baHiTransport.getId());
                        baCheckMapper.updateBaCheck(baCheck);
                    }
                }

                if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
                    AjaxResult ajaxResult = workFlowFeignClient.processInstanceIsEnd(baProcessInstancesRuntimeVO.getId());
                    //判断流程实例是否结束
                    String processInstanceIsEndResult = "";
                    if (!ObjectUtils.isEmpty(ajaxResult)) {
                        processInstanceIsEndResult = String.valueOf(ajaxResult.get("data"));
                    }
                    // 通过，判断流程实例是否已经结束，如果是更新业务与流程实例关联关系
                    if (processInstanceIsEndResult.equals(Constants.SUCCESS)) {
                        BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(baProcessInstancesRuntimeVO.getId());
                        if (!ObjectUtils.isEmpty(applyBusinessDataByProcessInstance)) {
                            applyBusinessDataByProcessInstance.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_PASS.getCode());
                            baProcessInstanceRelatedMapper.updateById(applyBusinessDataByProcessInstance);
                        }
                        BaHiTransport baHiTransport1 = baHiTransportMapper.selectBaHiTransportById(id);
                        baHiTransport1.setState(AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode());
                        baHiTransportMapper.updateBaHiTransport(baHiTransport1);
                    }
                }

                //查询原数据流程实例ID
                QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", baTransport.getId());

                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                BaProcessInstanceRelated baProcessInstanceRelatedBefore = new BaProcessInstanceRelated();
                BaProcessInstanceRelated baProcessInstanceRelatedBeforeTemp = new BaProcessInstanceRelated();
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelatedBefore = baProcessInstanceRelateds.get(0);
                    BeanUtils.copyBeanProp(baProcessInstanceRelatedBeforeTemp, baProcessInstanceRelatedBefore);
                }

                BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
                relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", id);
                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                }

                if(StringUtils.isNotEmpty(baProcessInstanceRelatedBefore.getId()) && StringUtils.isNotEmpty(baProcessInstanceRelated.getProcessInstanceId())){
                    baProcessInstanceRelatedBefore.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                    baProcessInstanceRelatedBefore.setBusinessData(baProcessInstanceRelated.getBusinessData());
                    baProcessInstanceRelatedBefore.setApproveType(baProcessInstanceRelated.getApproveType());
                    baProcessInstanceRelatedBefore.setApproveResult(baProcessInstanceRelated.getApproveResult());
                    baProcessInstanceRelatedBefore.setReason(baProcessInstanceRelated.getReason());
                    baProcessInstanceRelatedBefore.setCreateBy(baProcessInstanceRelated.getCreateBy());
                    baProcessInstanceRelatedBefore.setCreateTime(baProcessInstanceRelated.getCreateTime());
                    baProcessInstanceRelatedBefore.setUpdateBy(baProcessInstanceRelated.getUpdateBy());
                    baProcessInstanceRelatedBefore.setUpdateTime(baProcessInstanceRelated.getUpdateTime());
                    baProcessInstanceRelatedBefore.setState(baProcessInstanceRelated.getState());
                    baProcessInstanceRelatedBefore.setFlag(baProcessInstanceRelated.getFlag());
                    baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelatedBefore); //对换流程实例ID
                }

                if(StringUtils.isNotEmpty(baProcessInstanceRelated.getId()) && StringUtils.isNotEmpty(baProcessInstanceRelatedBeforeTemp.getProcessInstanceId())) {
                    baProcessInstanceRelated.setProcessInstanceId(baProcessInstanceRelatedBeforeTemp.getProcessInstanceId());
                    baProcessInstanceRelated.setBusinessData(baProcessInstanceRelatedBeforeTemp.getBusinessData());
                    baProcessInstanceRelated.setApproveType(baProcessInstanceRelatedBeforeTemp.getApproveType());
                    baProcessInstanceRelated.setApproveResult(baProcessInstanceRelatedBeforeTemp.getApproveResult());
                    baProcessInstanceRelated.setReason(baProcessInstanceRelatedBeforeTemp.getReason());
                    baProcessInstanceRelated.setCreateBy(baProcessInstanceRelatedBeforeTemp.getCreateBy());
                    baProcessInstanceRelated.setCreateTime(baProcessInstanceRelatedBeforeTemp.getCreateTime());
                    baProcessInstanceRelated.setUpdateBy(baProcessInstanceRelatedBeforeTemp.getUpdateBy());
                    baProcessInstanceRelated.setUpdateTime(baProcessInstanceRelatedBeforeTemp.getUpdateTime());
                    baProcessInstanceRelated.setState(baProcessInstanceRelatedBeforeTemp.getState());
                    baProcessInstanceRelated.setFlag(baProcessInstanceRelatedBeforeTemp.getFlag());
                    baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelated); //对换流程实例ID
                }

                baTransport1 = new BaTransport();
                BeanUtils.copyBeanProp(baTransport, baHiTransport);
                baTransport.setApproveFlag("0");
                baTransport.setId(baTransportBefore.getId());
                baTransportMapper.updateBaTransportChange(baTransport);

                BeanUtils.copyBeanProp(baHiTransport, baTransportBefore);
                baHiTransport.setApproveFlag("0");
                baHiTransport.setId(id);
                baHiTransport.setRelationId(baTransportBefore.getId());
                baHiTransportMapper.updateBaHiTransportChange(baHiTransport);
//                baTransportMapper.updateBaTransport(baTransport);
            }
//        }
        return result;
    }

    @Override
    public List<BaHiTransport> selectChangeBaHiTransportList(BaHiTransport baHiTransport) {
        return baHiTransportMapper.selectChangeBaHiTransportList(baHiTransport);
    }

    /**
     * 判断是否有字段变更
     */
    public String decideChange(BaTransport baTransport, BaTransport baTransportBefore) {
        String result = "";
        //基本信息
        if(!baTransport.getStartId().equals(baTransportBefore.getStartId())){
            result = "isStart";
        } else if((ObjectUtils.isEmpty(baTransport.getTransportDate()) &&
                !ObjectUtils.isEmpty(baTransportBefore.getTransportDate())) || (
                !ObjectUtils.isEmpty(baTransport.getTransportDate()) &&
                        !baTransport.getTransportDate()
                                .equals(baTransportBefore.getTransportDate()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baTransport.getTrainNumber()) &&
                StringUtils.isNotEmpty(baTransportBefore.getTrainNumber())) || (
                StringUtils.isNotEmpty(baTransport.getTrainNumber()) &&
                        !baTransport.getTrainNumber()
                                .equals(baTransportBefore.getTrainNumber()))){
            result = "isStart";
        } else if((baTransport.getSaleNum() != null && baTransportBefore.getSaleNum() != null) ||
                baTransport.getSaleNum() != null && baTransportBefore.getSaleNum() != null && baTransport.getSaleNum().intValue() != baTransportBefore.getSaleNum().intValue()){
            result = "isStart";
        } else if((StringUtils.isEmpty(baTransport.getTransportCompany()) &&
                StringUtils.isNotEmpty(baTransportBefore.getTransportCompany())) || (
                StringUtils.isNotEmpty(baTransport.getTransportCompany()) &&
                        !baTransport.getTransportCompany()
                                .equals(baTransportBefore.getTransportCompany()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baTransport.getOrigin()) &&
                StringUtils.isNotEmpty(baTransportBefore.getOrigin())) || (
                StringUtils.isNotEmpty(baTransport.getOrigin()) &&
                        !baTransport.getOrigin()
                                .equals(baTransportBefore.getOrigin()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baTransport.getDestination()) &&
                StringUtils.isNotEmpty(baTransportBefore.getDestination())) || (
                StringUtils.isNotEmpty(baTransport.getDestination()) &&
                        !baTransport.getDestination()
                                .equals(baTransportBefore.getDestination()))){
            result = "isStart";
        } else if((StringUtils.isEmpty(baTransport.getRemarks()) &&
                StringUtils.isNotEmpty(baTransportBefore.getRemarks())) || (
                StringUtils.isNotEmpty(baTransport.getRemarks()) &&
                        !baTransport.getRemarks()
                                .equals(baTransportBefore.getRemarks()))){
            result = "isStart";
        }
        //发货信息
        if((StringUtils.isEmpty(baTransport.getRemarks()) &&
                StringUtils.isNotEmpty(baTransportBefore.getRemarks())) || (
                StringUtils.isNotEmpty(baTransport.getRemarks()) &&
                        !baTransport.getRemarks()
                                .equals(baTransportBefore.getRemarks()))){
            result = "isStart";
        }
        if(StringUtils.isNotNull(baTransport.getDelivery())){
            BaCheck baCheck = baTransport.getDelivery();
            BaCheck baCheckBefore = baTransportBefore.getDelivery();
            if((!ObjectUtils.isEmpty(baCheck) && ObjectUtils.isEmpty(baCheckBefore)) ||
                    (ObjectUtils.isEmpty(baCheck) && !ObjectUtils.isEmpty(baCheckBefore))
            ){
                result = "isStart";
            } else if(!ObjectUtils.isEmpty(baCheck) && !ObjectUtils.isEmpty(baCheckBefore)){
                if((ObjectUtils.isEmpty(baCheck.getCheckTime()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getCheckTime())) || (
                        !ObjectUtils.isEmpty(baCheck.getCheckTime()) &&
                                !baCheck.getCheckTime()
                                        .equals(baCheckBefore.getCheckTime()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getCheckCoalNum())) || (
                        !ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) &&
                                !baCheck.getCheckCoalNum()
                                        .equals(baCheckBefore.getCheckCoalNum()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getCarsNum()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getCarsNum())) || (
                        !ObjectUtils.isEmpty(baCheck.getCarsNum()) &&
                                !baCheck.getCarsNum()
                                        .equals(baCheckBefore.getCarsNum()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getRemarks()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getRemarks())) || (
                        !ObjectUtils.isEmpty(baCheck.getRemarks()) &&
                                !baCheck.getRemarks()
                                        .equals(baCheckBefore.getRemarks()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getLaboratoryTest()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getLaboratoryTest())) || (
                        !ObjectUtils.isEmpty(baCheck.getLaboratoryTest()) &&
                                !baCheck.getLaboratoryTest()
                                        .equals(baCheckBefore.getLaboratoryTest()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getRailwayTicket())) || (
                        !ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                                !baCheck.getRailwayTicket()
                                        .equals(baCheckBefore.getRailwayTicket()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getTrackScale()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getTrackScale())) || (
                        !ObjectUtils.isEmpty(baCheck.getTrackScale()) &&
                                !baCheck.getTrackScale()
                                        .equals(baCheckBefore.getTrackScale()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getRailwayTicket())) || (
                        !ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                                !baCheck.getRailwayTicket()
                                        .equals(baCheckBefore.getRailwayTicket()))){
                    result = "isStart";
                } else if((ObjectUtils.isEmpty(baCheck.getEnclosure()) &&
                        !ObjectUtils.isEmpty(baCheckBefore.getEnclosure())) || (
                        !ObjectUtils.isEmpty(baCheck.getEnclosure()) &&
                                !baCheck.getEnclosure()
                                        .equals(baCheckBefore.getEnclosure()))){
                    result = "isStart";
                }
            }

            //数质量报告
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baTransportBefore.getId());
            baQualityReport.setStarId(baTransportBefore.getStartId());
            List<BaQualityReport> baQualityReports = baQualityReportMapper.selectBaQualityReportList(baQualityReport);
            List<BaQualityReport> baQualityReportDeliveryList = baTransport.getBaQualityReportDeliveryList();
            Map<String,String> baQualityReportDeliveryMap = new HashMap<>();
            if(!CollectionUtils.isEmpty(baQualityReportDeliveryList)){
                baQualityReportDeliveryMap = baQualityReportDeliveryList.stream().collect(Collectors.toMap(k->
                        k.getTransportId() + "_" + k.getStarId() + "_" + k.getCirculationType() + "_" + k.getTransportWay()
                                + "_" + k.getAttachmentType(), h-> h.getAnnex(),(oldValue, newValue) -> newValue));
            }
            String annex = "";
            StringBuilder stringBuilder = new StringBuilder();
            if(!CollectionUtils.isEmpty(baQualityReports)){
                for(BaQualityReport baQualityReport1 : baQualityReports){
                    if(baTransport.getTransportType().equals("2")){
                        //汽运发货新增附件
                        if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                            //CirculationType = 2, TransportWay = 2, AttachmentType = 1
                            stringBuilder.append(baTransport.getId())
                                    .append("_")
                                    .append(baTransport.getStartId())
                                    .append("_2")
                                    .append("_2")
                                    .append("_1");
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString());
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                    }
                    if(baTransport.getTransportType().equals("1")){
                        stringBuilder = new StringBuilder();
                        //CirculationType = 2, TransportWay = 1, AttachmentType = 6
                        stringBuilder.append(baTransport.getId())
                                .append("_")
                                .append(baTransport.getStartId())
                                .append("_2")
                                .append("_1").toString();
                        //火运其他附件
                        if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_6");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                        //火运随车化验单
                        if(StringUtils.isNotEmpty(baCheck.getLaboratoryTest())){
                            //AttachmentType = 2
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_2");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                        //火运铁路大票
                        if(StringUtils.isNotEmpty(baCheck.getRailwayTicket())){
                            //AttachmentType = 3
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_3");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                        //火运轨道衡
                        if(StringUtils.isNotEmpty(baCheck.getTrackScale())){
                            //AttachmentType = 4
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_4");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                        //火运随车附表
                        if(StringUtils.isNotEmpty(baCheck.getAccompanyingSchedule())){
                            //AttachmentType = 5
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_5");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                    }

                    //验收
                    if(StringUtils.isNotNull(baTransport.getReceipt())){
                        //数质量报告
                        if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                            //验收附件
                            //CirculationType = 3, TransportWay = baTransport.getTransportType(), AttachmentType = 1
                            stringBuilder.append(baTransport.getId())
                                    .append("_")
                                    .append(baTransport.getStartId())
                                    .append("_3")
                                    .append(baTransport.getTransportType())
                                    .append("_1");
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString());
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                result = "isStart";
                                break;
                            }
                        }
                    }

                    //历史数质量报告货转
                    if(StringUtils.isNotEmpty(baTransport.getTransferVoucher())){
                        //货转附件
                        //CirculationType = 4, TransportWay = baTransport.getTransportType(), AttachmentType = 1
                        stringBuilder.append(baTransport.getId())
                                .append("_")
                                .append(baTransport.getStartId())
                                .append("_4")
                                .append(baTransport.getTransportType())
                                .append("_1");
                        annex = baQualityReportDeliveryMap.get(stringBuilder.toString());
                        if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                            result = "isStart";
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<String> selectChangeDataList(String id) {
        BaHiTransport baHiTransport = iBaHiTransportService.selectBaHiTransportById(id);

        if(!ObjectUtils.isEmpty(baHiTransport)){
            BaTransport baTransport = this.selectBaTransportById(baHiTransport.getRelationId());
            return getChangeData(baTransport, baHiTransport);
        }
        return new ArrayList<>();
    }

    @Override
    public List<BaTransport> selectBaTransportAuthorizedList(BaTransport baTransport) {
        List<BaTransport> baTransports = baTransportMapper.selectTransportAuthorized(baTransport);
        for (BaTransport transport:baTransports) {
            //验收的业务状态
            if(transport.getConfirmStatus().equals("3") && transport.getTransportStatus().equals("3") == false){
                transport.setTransportStatus("2");
                transport.setJudge("3");
            }
            //发货信息
            QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",transport.getId());
            queryWrapper.eq("type","1");
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
            transport.setDelivery(baCheck);
            //验收信息
            QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id",transport.getId());
            queryWrapper1.eq("type","2");
            BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper1);
            transport.setReceipt(baCheck1);
            //完成状态
            if(transport.getConfirmStatus().equals("3") && transport.getTransportStatus().equals("3")){
                transport.setTransportStatus("4");
            }
            /*//目的地
            if(StringUtils.isNotEmpty(transport.getDestination())){
                BaRailwayPlatform baRailwayPlatform = baRailwayPlatformMapper.selectBaRailwayPlatformById(transport.getDestination());
                if(StringUtils.isNotNull(baRailwayPlatform)){
                    transport.setDestinationName(baRailwayPlatform.getName());
                }else {
                    transport.setDestinationName(transport.getDestination());
                }
            }*/
            //岗位名称
            String name = getName.getName(transport.getUserId());
            //发起人
            if(name.equals("") == false){
                transport.setUserName(sysUserMapper.selectUserById(transport.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                transport.setUserName(sysUserMapper.selectUserById(transport.getUserId()).getNickName());
            }
            if(StringUtils.isNotEmpty(transport.getAutomobileType())){
                if(transport.getAutomobileType().equals("1")){
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(transport.getAutomobileId());
                    //查看昕科对应运单数据
                    QueryWrapper<BaShippingOrder> baShippingOrderQueryWrapper = new QueryWrapper<>();
                    baShippingOrderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                    baShippingOrderQueryWrapper.eq("sign_for","1");
                    List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(baShippingOrderQueryWrapper);
                    //总出场净重
                    BigDecimal ccjweightTotal = new BigDecimal(0);
                    //总入场净重
                    BigDecimal rcjweightTotal = new BigDecimal(0);
                    for (BaShippingOrder shippingOrder:shippingOrders) {
                        ccjweightTotal = ccjweightTotal.add(shippingOrder.getCcjweight());
                        rcjweightTotal = rcjweightTotal.add(shippingOrder.getRcjweight());
                    }
                    transport.setLoadingCapacity(ccjweightTotal);
                    transport.setUnloadingVolume(rcjweightTotal);
                }
                if(transport.getAutomobileType().equals("2")){
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(transport.getAutomobileId());
                    //查看中储摘单
                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                    baShippingOrderCmst1.setYardId(transportCmst.getYardld());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    //确认收货
                    BigDecimal confirmedReceiveWeightTotal = new BigDecimal(0);
                    //确认发货
                    BigDecimal confirmedDeliveryWeightTotal = new BigDecimal(0);
                    for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                        if(baShippingOrderCmst.getConfirmedReceiveWeight() != null){
                            confirmedReceiveWeightTotal = confirmedReceiveWeightTotal.add(baShippingOrderCmst.getConfirmedReceiveWeight());
                        }
                        if(baShippingOrderCmst.getConfirmedDeliveryWeight() != null){
                            confirmedDeliveryWeightTotal = confirmedDeliveryWeightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                        }
                    }
                    transport.setLoadingCapacity(confirmedDeliveryWeightTotal);
                    transport.setUnloadingVolume(confirmedReceiveWeightTotal);
                }
            }
        }
        return baTransports;
    }

    @Override
    public int authorizedBaTransport(BaTransport baTransport) {
        return baTransportMapper.updateBaTransport(baTransport);
    }

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> getChangeData(BaTransport baTransport, BaHiTransport baHiTransport) {
        List<String> isChangeList = new ArrayList<>();
        //基本信息
        if(!baTransport.getStartId().equals(baHiTransport.getStartId())){
            isChangeList.add("startId");
        }
        if((ObjectUtils.isEmpty(baTransport.getTransportDate()) &&
                !ObjectUtils.isEmpty(baHiTransport.getTransportDate())) || (
                !ObjectUtils.isEmpty(baTransport.getTransportDate()) &&
                        !baTransport.getTransportDate()
                                .equals(baHiTransport.getTransportDate()))){
            isChangeList.add("transportDate");
        }
        if((StringUtils.isEmpty(baTransport.getTrainNumber()) &&
                StringUtils.isNotEmpty(baHiTransport.getTrainNumber())) || (
                StringUtils.isNotEmpty(baTransport.getTrainNumber()) &&
                        !baTransport.getTrainNumber()
                                .equals(baHiTransport.getTrainNumber()))){
            isChangeList.add("trainNumber");
        }
        if((baTransport.getSaleNum() != null && baHiTransport.getSaleNum() == null) ||
                (baTransport.getSaleNum() == null && baHiTransport.getSaleNum() != null) ||
                baTransport.getSaleNum() != null && baTransport.getSaleNum().intValue() != baHiTransport.getSaleNum().intValue()){
            isChangeList.add("saleNum");
        }
        if((StringUtils.isEmpty(baTransport.getTransportCompany()) &&
                StringUtils.isNotEmpty(baHiTransport.getTransportCompany())) || (
                StringUtils.isNotEmpty(baTransport.getTransportCompany()) &&
                        !baTransport.getTransportCompany()
                                .equals(baHiTransport.getTransportCompany()))){
            isChangeList.add("transportCompany");
        }
        if((StringUtils.isEmpty(baTransport.getOrigin()) &&
                StringUtils.isNotEmpty(baHiTransport.getOrigin())) || (
                StringUtils.isNotEmpty(baTransport.getOrigin()) &&
                        !baTransport.getOrigin()
                                .equals(baHiTransport.getOrigin()))){
            isChangeList.add("origin");
        }
        if((StringUtils.isEmpty(baTransport.getDestination()) &&
                StringUtils.isNotEmpty(baHiTransport.getDestination())) || (
                StringUtils.isNotEmpty(baTransport.getDestination()) &&
                        !baTransport.getDestination()
                                .equals(baHiTransport.getDestination()))){
            isChangeList.add("destination");
        }
        if((StringUtils.isEmpty(baTransport.getRemarks()) &&
                StringUtils.isNotEmpty(baHiTransport.getRemarks())) || (
                StringUtils.isNotEmpty(baTransport.getRemarks()) &&
                        !baTransport.getRemarks()
                                .equals(baHiTransport.getRemarks()))){
            isChangeList.add("remarks");
        }//发货信息
        if(StringUtils.isNotNull(baTransport.getDelivery())){
            BaCheck baCheck = baTransport.getDelivery();
            BaCheck baCheckBefore = baHiTransport.getDelivery();
            if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    !ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore) ||
                    !ObjectUtils.isEmpty(baCheck.getCheckTime()) &&
                            !baCheck.getCheckTime()
                                    .equals(baCheckBefore.getCheckTime())){
                isChangeList.add("checkTime");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    !ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore) || (
                    !ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) &&
                            !baCheck.getCheckCoalNum()
                                    .equals(baCheckBefore.getCheckCoalNum()))){
                isChangeList.add("checkCoalNum");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) || (!ObjectUtils.isEmpty(baCheck) &&
                    ObjectUtils.isEmpty(baCheckBefore)) ||  (
                    !ObjectUtils.isEmpty(baCheck.getCarsNum()) &&
                            !baCheck.getCarsNum()
                                    .equals(baCheckBefore.getCarsNum()))){
                isChangeList.add("carsNum");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) || (!ObjectUtils.isEmpty(baCheck) &&
                    ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getRemarks()) &&
                            !baCheck.getRemarks()
                                    .equals(baCheckBefore.getRemarks()))){
                isChangeList.add("deliveryremarks");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    (!ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getLaboratoryTest()) &&
                            !baCheck.getLaboratoryTest()
                                    .equals(baCheckBefore.getLaboratoryTest()))){
                isChangeList.add("laboratoryTest");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    (!ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                            !baCheck.getRailwayTicket()
                                    .equals(baCheckBefore.getRailwayTicket()))){
                isChangeList.add("railwayTicket");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    (!ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getTrackScale()) &&
                            !baCheck.getTrackScale()
                                    .equals(baCheckBefore.getTrackScale()))){
                isChangeList.add("trackScale");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    (!ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getRailwayTicket()) &&
                            !baCheck.getRailwayTicket()
                                    .equals(baCheckBefore.getRailwayTicket()))){
                isChangeList.add("railwayTicket");
            } else if((ObjectUtils.isEmpty(baCheck) &&
                    !ObjectUtils.isEmpty(baCheckBefore)) ||
                    (!ObjectUtils.isEmpty(baCheck) &&
                            ObjectUtils.isEmpty(baCheckBefore)) || (
                    !ObjectUtils.isEmpty(baCheck.getEnclosure()) &&
                            !baCheck.getEnclosure()
                                    .equals(baCheckBefore.getEnclosure()))){
                isChangeList.add("enclosure");
            }

            //数质量报告
            BaQualityReport baQualityReport = new BaQualityReport();
            baQualityReport.setTransportId(baHiTransport.getId());
            baQualityReport.setStarId(baHiTransport.getStartId());
            List<BaQualityReport> baQualityReports = baQualityReportMapper.selectBaQualityReportList(baQualityReport);
            List<BaQualityReport> baQualityReportDeliveryList = baTransport.getBaQualityReportDeliveryList();
            Map<String,String> baQualityReportDeliveryMap = new HashMap<>();
            if(!CollectionUtils.isEmpty(baQualityReportDeliveryList)){
                baQualityReportDeliveryMap = baQualityReportDeliveryList.stream().collect(Collectors.toMap(k->
                        k.getTransportId() + "_" + k.getStarId() + "_" + k.getCirculationType() + "_" + k.getTransportWay()
                                + "_" + k.getAttachmentType(), h-> h.getAnnex(),(oldValue, newValue) -> newValue));
            }
            String annex = "";
            StringBuilder stringBuilder = new StringBuilder();
            if(!CollectionUtils.isEmpty(baQualityReports)){
                for(BaQualityReport baQualityReport1 : baQualityReports){
                    if(baTransport.getTransportType().equals("2")){
                        //汽运发货新增附件
                        if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                            //CirculationType = 2, TransportWay = 2, AttachmentType = 1
                            stringBuilder.append(baTransport.getId())
                                    .append("_")
                                    .append(baTransport.getStartId())
                                    .append("_2")
                                    .append("_2")
                                    .append("_1");
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString());
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                    }
                    if(baTransport.getTransportType().equals("1")){
                        stringBuilder = new StringBuilder();
                        //CirculationType = 2, TransportWay = 1, AttachmentType = 6
                        stringBuilder.append(baTransport.getId())
                                .append("_")
                                .append(baTransport.getStartId())
                                .append("_2")
                                .append("_1").toString();
                        //火运其他附件
                        if(StringUtils.isNotEmpty(baCheck.getEnclosure())){
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_6");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                        //火运随车化验单
                        if(StringUtils.isNotEmpty(baCheck.getLaboratoryTest())){
                            //AttachmentType = 2
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_2");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                        //火运铁路大票
                        if(StringUtils.isNotEmpty(baCheck.getRailwayTicket())){
                            //AttachmentType = 3
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_3");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                        //火运轨道衡
                        if(StringUtils.isNotEmpty(baCheck.getTrackScale())){
                            //AttachmentType = 4
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_4");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                        //火运随车附表
                        if(StringUtils.isNotEmpty(baCheck.getAccompanyingSchedule())){
                            //AttachmentType = 5
                            annex = baQualityReportDeliveryMap.get(stringBuilder.toString() + "_5");
                            if(StringUtils.isEmpty(baQualityReport1.getAnnex()) && StringUtils.isNotEmpty(annex) ||
                                    StringUtils.isNotEmpty(baQualityReport1.getAnnex()) && !baQualityReport1.getAnnex().equals(annex)){
                                isChangeList.add(baQualityReport1.getId() + "_"
                                        + baTransport.getStartId() + "_"
                                        + baQualityReport1.getCirculationType() + "_"
                                        + baQualityReport1.getTransportWay() + "_"
                                        + baQualityReport1.getAttachmentType() + "_"
                                        + "_annex");
                            }
                        }
                    }
                }
            }

            //验收
            if(StringUtils.isNotNull(baTransport.getReceipt())){
                baCheck = baTransport.getReceipt();
                baCheckBefore = baHiTransport.getReceipt();
                if((ObjectUtils.isEmpty(baCheck) &&
                        !ObjectUtils.isEmpty(baCheckBefore)) || (!ObjectUtils.isEmpty(baCheck) &&
                        ObjectUtils.isEmpty(baCheckBefore))){
                    isChangeList.add("checkTime");
                    isChangeList.add("carsNum");
                    isChangeList.add("checkCoalNum");
                    isChangeList.add("consumption");
                    isChangeList.add("receiptremarks");
                } else {
                    if(!DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baCheck.getCheckTime()).equals(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baCheckBefore.getCheckTime())) ||
                            !DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baCheck.getCheckEndTime()).equals(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS, baCheckBefore.getCheckEndTime()))){
                        isChangeList.add("checkTime");
                    }
                    if((!ObjectUtils.isEmpty(baCheck.getCarsNum()) && ObjectUtils.isEmpty(baCheckBefore.getCarsNum()) ||
                    (ObjectUtils.isEmpty(baCheck.getCarsNum()) && !ObjectUtils.isEmpty(baCheckBefore.getCarsNum()) ||
                            (!ObjectUtils.isEmpty(baCheck.getCarsNum()) && !ObjectUtils.isEmpty(baCheckBefore.getCarsNum()) &&
                                    baCheck.getCarsNum().longValue() != baCheckBefore.getCarsNum().longValue())))){
                        isChangeList.add("carsNum");
                    }
                    if((!ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) && ObjectUtils.isEmpty(baCheckBefore.getCheckCoalNum())) ||
                            (ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) && !ObjectUtils.isEmpty(baCheckBefore.getCheckCoalNum())) ||
                            (!ObjectUtils.isEmpty(baCheck.getCheckCoalNum()) && !ObjectUtils.isEmpty(baCheckBefore.getCheckCoalNum())
                                    && baCheck.getCheckCoalNum().longValue() != baCheckBefore.getCheckCoalNum().longValue())){
                        isChangeList.add("checkCoalNum");
                    }
                    if((!ObjectUtils.isEmpty(baCheck.getConsumption()) && ObjectUtils.isEmpty(baCheckBefore.getConsumption())) ||
                            (ObjectUtils.isEmpty(baCheck.getConsumption()) && !ObjectUtils.isEmpty(baCheckBefore.getConsumption())) ||
                            (!ObjectUtils.isEmpty(baCheck.getConsumption()) && !ObjectUtils.isEmpty(baCheckBefore.getConsumption())
                                    && baCheck.getConsumption().longValue() != baCheckBefore.getConsumption().longValue())){
                        isChangeList.add("consumption");
                    }
                    if(!StringUtils.isEmpty(baCheck.getRemarks()) && !baCheck.getRemarks().equals(baCheckBefore.getRemarks())){
                        isChangeList.add("receiptremarks");
                    }
                }
            }
        }
        return isChangeList;
    }
}
