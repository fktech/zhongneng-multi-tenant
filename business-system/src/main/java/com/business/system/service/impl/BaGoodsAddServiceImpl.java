package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaGoodsAddMapper;
import com.business.system.domain.BaGoodsAdd;
import com.business.system.service.IBaGoodsAddService;

/**
 * 商品附加Service业务层处理
 *
 * @author ljb
 * @date 2022-12-01
 */
@Service
public class BaGoodsAddServiceImpl extends ServiceImpl<BaGoodsAddMapper, BaGoodsAdd> implements IBaGoodsAddService
{
    @Autowired
    private BaGoodsAddMapper baGoodsAddMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询商品附加
     *
     * @param id 商品附加ID
     * @return 商品附加
     */
    @Override
    public BaGoodsAdd selectBaGoodsAddById(String id)
    {
        return baGoodsAddMapper.selectBaGoodsAddById(id);
    }

    /**
     * 查询商品附加列表
     *
     * @param baGoodsAdd 商品附加
     * @return 商品附加
     */
    @Override
    public List<BaGoodsAdd> selectBaGoodsAddList(BaGoodsAdd baGoodsAdd)
    {
        return baGoodsAddMapper.selectBaGoodsAddList(baGoodsAdd);
    }

    /**
     * 新增商品附加
     *
     * @param baGoodsAdd 商品附加
     * @return 结果
     */
    @Override
    public int insertBaGoodsAdd(BaGoodsAdd baGoodsAdd)
    {
        baGoodsAdd.setId(getRedisIncreID.getId());
        baGoodsAdd.setCreateTime(DateUtils.getNowDate());
        baGoodsAdd.setCreateBy(SecurityUtils.getUsername());
        return baGoodsAddMapper.insertBaGoodsAdd(baGoodsAdd);
    }

    /**
     * 修改商品附加
     *
     * @param baGoodsAdd 商品附加
     * @return 结果
     */
    @Override
    public int updateBaGoodsAdd(BaGoodsAdd baGoodsAdd)
    {
        baGoodsAdd.setUpdateTime(DateUtils.getNowDate());
        baGoodsAdd.setUpdateBy(SecurityUtils.getUsername());
        return baGoodsAddMapper.updateBaGoodsAdd(baGoodsAdd);
    }

    /**
     * 批量删除商品附加
     *
     * @param ids 需要删除的商品附加ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsAddByIds(String[] ids)
    {
        return baGoodsAddMapper.deleteBaGoodsAddByIds(ids);
    }

    /**
     * 删除商品附加信息
     *
     * @param id 商品附加ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsAddById(String id)
    {
        return baGoodsAddMapper.deleteBaGoodsAddById(id);
    }

}
