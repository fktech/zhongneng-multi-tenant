package com.business.system.service.impl;

import java.text.ParseException;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaProject;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaWeekday;
import com.business.system.mapper.OaAttendanceGroupMapper;
import com.business.system.mapper.OaWeekdayMapper;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.service.DingDingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaWorkTimesMapper;
import com.business.system.domain.OaWorkTimes;
import com.business.system.service.IOaWorkTimesService;

/**
 * 班次Service业务层处理
 *
 * @author single
 * @date 2023-11-24
 */
@Service
public class OaWorkTimesServiceImpl extends ServiceImpl<OaWorkTimesMapper, OaWorkTimes> implements IOaWorkTimesService
{
    @Autowired
    private OaWorkTimesMapper oaWorkTimesMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaWeekdayMapper oaWeekdayMapper;

    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private DingDingService dingDingService;

    /**
     * 查询班次
     *
     * @param id 班次ID
     * @return 班次
     */
    @Override
    public OaWorkTimes selectOaWorkTimesById(String id)
    {
        return oaWorkTimesMapper.selectOaWorkTimesById(id);
    }

    /**
     * 查询班次列表
     *
     * @param oaWorkTimes 班次
     * @return 班次
     */
    @Override
    public List<OaWorkTimes> selectOaWorkTimesList(OaWorkTimes oaWorkTimes)
    {
        List<OaWorkTimes> oaWorkTimesList = oaWorkTimesMapper.selectOaWorkTimesList(oaWorkTimes);
        oaWorkTimesList.stream().forEach(itm ->{
            QueryWrapper<OaWeekday> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("work_time_id",itm.getId());
            List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectList(queryWrapper);
            for (OaWeekday weekday:oaWeekdays) {
                //查询考勤班组信息
                OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(weekday.getRelationId());
                if(attendanceGroup.getFlag() == 0){
                    itm.setDeletionFlag("1");
                }
            }
        });
        return oaWorkTimesList;
    }

    /**
     * 新增班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    @Override
    public int insertOaWorkTimes(OaWorkTimes oaWorkTimes)
    {
        //班次名称不能重复
        QueryWrapper<OaWorkTimes> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", oaWorkTimes.getName());
        queryWrapper.eq("flag", 0);
        //租户ID
        queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        List<OaWorkTimes> workTimes = oaWorkTimesMapper.selectList(queryWrapper);
        if(workTimes.size() > 0){
            return -1;
        }
        //钉钉班次数据推送
        //钉钉修改用户
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentCreate = dingDingService.shiftAdd(oaWorkTimes);
                Object parse = JSONObject.parse(departmentCreate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    JSONObject result = jsonObject.getJSONObject("result");
                    String id = result.getString("id");
                    oaWorkTimes.setDingDing(id);
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                oaWorkTimes.setId(getRedisIncreID.getId());
                oaWorkTimes.setCreateTime(DateUtils.getNowDate());
                //租户ID
                oaWorkTimes.setTenantId(SecurityUtils.getCurrComId());
                result = oaWorkTimesMapper.insertOaWorkTimes(oaWorkTimes);
            }
        }
        return result;
    }

    /**
     * 修改班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    @Override
    public int updateOaWorkTimes(OaWorkTimes oaWorkTimes)
    {
        //查询原来数据
        OaWorkTimes workTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWorkTimes.getId());
        if(!workTimes.getName().equals(oaWorkTimes.getName())){
            //班次名称不能重复
            QueryWrapper<OaWorkTimes> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", oaWorkTimes.getName());
            queryWrapper.eq("flag", 0);
            List<OaWorkTimes> workTimesList = oaWorkTimesMapper.selectList(queryWrapper);
            if(workTimesList.size() > 0){
                return -1;
            }
        }
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentCreate = dingDingService.shiftAdd(oaWorkTimes);
                Object parse = JSONObject.parse(departmentCreate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    JSONObject result = jsonObject.getJSONObject("result");
                    String id = result.getString("id");
                    oaWorkTimes.setDingDing(id);
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                oaWorkTimes.setUpdateTime(DateUtils.getNowDate());
                result = oaWorkTimesMapper.updateOaWorkTimes(oaWorkTimes);
            }
        }
        return result;
    }

    /**
     * 批量删除班次
     *
     * @param ids 需要删除的班次ID
     * @return 结果
     */
    @Override
    public int deleteOaWorkTimesByIds(String[] ids)
    {
        //返回值
        int result = 0;
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                String msg = "";
                //查询班次信息
                OaWorkTimes workTimes = oaWorkTimesMapper.selectOaWorkTimesById(id);
                if(StringUtils.isNotEmpty(workTimes.getDingDing())){
                    try {
                        String shiftDelete = dingDingService.shiftDelete(Long.valueOf(workTimes.getDingDing()));
                        Object parse = JSONObject.parse(shiftDelete);
                        JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                        String errmsg = jsonObject.getString("errmsg");
                        if(errmsg.equals("ok")){
                            msg = "success";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    msg = "success";
                }
                if(StringUtils.isNotEmpty(msg)){
                    if(msg.equals("success")){
                        QueryWrapper<OaWeekday> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("work_time_id",id);
                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectList(queryWrapper);
                        for (OaWeekday weekday:oaWeekdays) {
                            //查询考勤班组信息
                            OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(weekday.getRelationId());
                            if(attendanceGroup.getFlag() == 0){
                                return -1;
                            }
                        }
                    }
                }else {
                    return 0;
                }
                result = oaWorkTimesMapper.deleteOaWorkTimesByIds(ids);
            }
        }
        return result;
    }

    /**
     * 删除班次信息
     *
     * @param id 班次ID
     * @return 结果
     */
    @Override
    public int deleteOaWorkTimesById(String id)
    {
        return oaWorkTimesMapper.deleteOaWorkTimesById(id);
    }

    @Override
    public OaWorkTimes dayWorkTime(String day) {

        //获取当前用户部门
        Long deptId = SecurityUtils.getDeptId();
        //获取考勤组信息
        SysDept dept = sysDeptMapper.selectDeptById(deptId);
        if(!"0".equals(dept.getAttendanceLock())){
            //查询考勤组
            OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
            OaWeekday weekday = new OaWeekday();
            weekday.setRelationId(oaAttendanceGroup.getId());
            weekday.setType("1");
            try {
                //前一天时间转周几
                weekday.setName(DateUtils.dateToWeek(day));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
            OaWeekday oaWeekday = oaWeekdays.get(0);
            if(!"0".equals(oaWeekday.getWorkTimeId())){
                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                return oaWorkTimes;
            }
        }
        return null;
    }

}
