package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMaterialStock;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.BaWashInventory;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 配煤出库审批接口 服务类
 * @date: 2023/1/5 11:10
 */
@Service
public class BlendOutputdepotApprovalService {

    @Autowired
    protected BaWashHeadMapper baWashHeadMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaWashInventoryMapper baWashInventoryMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaWashHead baWashHead = new BaWashHead();
        baWashHead.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baWashHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
        String userId = String.valueOf(baWashHead.getUserId());
        if(AdminCodeEnum.BLENDOUTPUTDEPOT_STATUS_PASS.getCode().equals(status)){
            baWashHead.setTypes("2"); //已完成
            //判断销售出库是否有出库清单
            QueryWrapper<BaWashInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id",baWashHead.getId());
            List<BaWashInventory> baWashInventories = baWashInventoryMapper.selectList(queryWrapper1);
            baWashHead = baWashHeadMapper.selectBaWashHeadById(baWashHead.getId());
            if(!CollectionUtils.isEmpty(baWashInventories)){
                for(BaWashInventory baWashInventory : baWashInventories){
                    //查询立项信息
                    BaMaterialStock materialStock = new BaMaterialStock();
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baWashInventory.getDepotId());
                    stockQueryWrapper.eq("location_id", baWashInventory.getLocationId());
                    stockQueryWrapper.eq("goods_id", baWashInventory.getGoodsId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    if (ObjectUtils.isEmpty(materialStock)) {
                        materialStock = new BaMaterialStock();
                        materialStock.setId(getRedisIncreID.getId());
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //仓库ID
                        materialStock.setCreateTime(DateUtils.getNowDate());
                        materialStock.setCreateBy(SecurityUtils.getUsername());
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock = this.calcBaMaterialStock(materialStock, baWashHead, baWashInventory);
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        baMaterialStockMapper.insertBaMaterialStock(materialStock);
                    } else {
                        materialStock.setDepotId(baWashInventory.getDepotId()); //仓库ID
                        materialStock.setGoodsId(baWashInventory.getGoodsId()); //商品ID
                        materialStock.setLocationId(baWashInventory.getLocationId()); //仓库ID
                        materialStock.setUpdateBy(SecurityUtils.getUsername());
                        materialStock.setUpdateTime(DateUtils.getNowDate());
                        materialStock = this.calcBaMaterialStock(materialStock, baWashHead, baWashInventory);
                        baMaterialStockMapper.updateBaMaterialStock(materialStock);
                    }

                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baWashHead, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.BLENDOUTPUTDEPOT_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baWashHead, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baWashHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baWashHead.setState(status);
        baWashHeadMapper.updateById(baWashHead);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaWashHead baWashHead, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baWashHead.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_BLENDIOUTPUTDEPOT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"配煤出库审批提醒",msgContent,baMessage.getType(),baWashHead.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaWashHead checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaWashHead baWashHead = baWashHeadMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baWashHead == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_BLENDOUTPUTDEPOT_001);
        }
        if (!AdminCodeEnum.BLENDOUTPUTDEPOT_STATUS_VERIFYING.getCode().equals(baWashHead.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_BLENDOUTPUTDEPOT_002);
        }
        return baWashHead;
    }

    /**
     * 出库申请审批通过计算更新库存数据
     * @param materialStock
     * @param baWashHead
     * @return
     */
    private BaMaterialStock calcBaMaterialStock(BaMaterialStock materialStock, BaWashHead baWashHead, BaWashInventory baWashInventory){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baWashInventory.getWashTotal() != null && baWashInventory.getWashTotal().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().subtract(baWashInventory.getWashTotal()));
        }
        return materialStock;
    }
}
