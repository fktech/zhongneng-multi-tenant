package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaBid;
import com.business.system.domain.BaSeal;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IBidApprovalService;
import com.business.system.service.workflow.ISealApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用印申请审批结果:审批通过
 */
@Service("sealApprovalContent:processApproveResult:pass")
@Slf4j
public class SealApprovalAgreeServiceImpl extends ISealApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSeal baSeal = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.SEAL_STATUS_PASS.getCode());
        return result;
    }
}
