package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaBid;

/**
 * 投标管理Service接口
 *
 * @author ljb
 * @date 2022-12-06
 */
public interface IBaBidService
{
    /**
     * 查询投标管理
     *
     * @param id 投标管理ID
     * @return 投标管理
     */
    public BaBid selectBaBidById(String id);

    /**
     * 查询投标管理列表
     *
     * @param baBid 投标管理
     * @return 投标管理集合
     */
    public List<BaBid> selectBaBidList(BaBid baBid);

    /**
     * 新增投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    public int insertBaBid(BaBid baBid);

    /**
     * 修改投标管理
     *
     * @param baBid 投标管理
     * @return 结果
     */
    public int updateBaBid(BaBid baBid);

    /**
     * 投标按钮
     * @param baBid
     * @return
     */
    public int bidSubmit(BaBid baBid);

    /**
     * 批量删除投标管理
     *
     * @param ids 需要删除的投标管理ID
     * @return 结果
     */
    public int deleteBaBidByIds(String[] ids);

    /**
     * 删除投标管理信息
     *
     * @param id 投标管理ID
     * @return 结果
     */
    public int deleteBaBidById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);



}
