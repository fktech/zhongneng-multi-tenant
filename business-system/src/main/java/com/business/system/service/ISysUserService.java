package com.business.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.BaUserVO;

import java.util.List;
import java.util.Map;

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
public interface ISysUserService extends IService<SysUser>
{
    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser user);


    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> userList(SysUser user);

    /**
     * 根据条件分页查询所有用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllUserList(SysUser user);

    /**
     * 根据条件分页查询树用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectTreeUserList(SysUser user);

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllocatedList(SysUser user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUnallocatedList(SysUser user);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);

    /**
     * 通过手机号重置密码
     *
     * @param phonenumber 手机号
     * @return 用户对象信息
     */
    public SysUser selectUserByPhone(String phonenumber);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long userId);

    /**
     * 根据用户ID查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserRoleGroup(String userName);

    /**
     * 根据用户ID查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserPostGroup(String userName);

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkUserNameUnique(SysUser user);

    /**
     * 信链云APP注册校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String registerCheckUserNameUnique(SysUser user);

    /**
     * 校验用户工号是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkJobNoUnique(SysUser user);

    /**
     * 校验用户岗位编码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkJobCodeUnique(SysUser user);

    /**
     * 校验用户副岗位编码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkDeputyPostCodeUnique(SysUser user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(SysUser user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkPhoneUnique1(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkEmailUnique(SysUser user);

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user);

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    public void checkUserDataScope(Long userId);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUserRegister(SysUser user);

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean registerUser(SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserAuth(Long userId, Long[] roleIds);

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserStatus(SysUser user);

    /**
     * 修改用户手机标识
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserPhoneCode(SysUser user);

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserProfile(SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateUserAvatar(String userName, String avatar);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    public int resetPwd(SysUser user);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(String userName, String password);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(Long userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] userIds);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUser(Long[] userIds);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName);

    /**
     * 封装当前登录人用户信息
     */
    public UserInfo getCurrentUserInfo();

    /**
     * 根据部门ID及角色ID查询用户列表
     *
     * @param sysUserRoleDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserByRoleId(SysUserRoleDTO sysUserRoleDTO);

    /**
     * 根据角色ID查询用户列表
     *
     * @param sysUserByRoleIdDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO);

    /**
     * 根据角色ID查询自定义数据权限部门列表
     * @param sysDeptRoleDTO
     * @return
     */
    public List<SysDept> selectDeptsByRoleId(SysDeptRoleDTO sysDeptRoleDTO);

    /**
     * 根据部门查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepId(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据部门及角色查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepIdAndRole(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据部门及角色查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepIdAndRoleName(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据运营查询部门经理列表
     * @param sysUserByDepRoleDTO
     * @return
     */
    public List<String> selectUserByRoleIdAndDepRole(SysUserByDepRoleDTO sysUserByDepRoleDTO);

    /**
     * 根据用户ID查询角色列表
     * @param sysRolesByUserDTO
     * @return
     */
    List<String> selectRolesByUser(SysRolesByUserDTO sysRolesByUserDTO);

    /**
     * 根据用户ID查询角色列表
     * @param sysRolesByUserDTO
     * @return
     */
    List<String> selectOperationRolesByUser(SysRolesByUserDTO sysRolesByUserDTO);

    /**
     * 根据openID获取用户信息
     */
    public SysUser selectOpenIdByUser(String openId);

    /**
     * 根据角色ID查询用户ID列表
     */
    List<String> selectUserIdsByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO);

    /**
     * 根据用户部门ID集合查询负责部门角色用户列表
     * @param sysRoleUserByDeptDTO
     * @return
     */
    List<SysUser> selectRoleUsersByUser(SysRoleUserByDeptDTO sysRoleUserByDeptDTO);

    /**
     * 通过用户名查询用户
     *
     * @return 用户对象信息
     */
    Map<String, SysUser> selectUserMapByUserName(SysUser sysUser);

    /**
     * 通过用户ID查询用户
     *
     * @return 用户对象信息
     */
    public Map<Long, SysUser> selectUserMapByUserId(SysUser sysUser);

    /**
     * 组织机构人数统计
     */
    public List<BaUserVO> countUser();

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserByUserId(Long userId);

    /**
     * 根据userId获取对应归属公司信息
     */
    public SysDept selectUserDeptByUserId(Long userId);

    /**
     * 根据部门ID集合查询用户
     * @param deptIds
     * @return
     */
    public List<SysUser> selectUsersByDeptIds(Long[] deptIds);

    public List<SysUser> operateUser(String comId);

    //新增考勤信息
    public int clockSummaryAdd (SysUser user);
}
