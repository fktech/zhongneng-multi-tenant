package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaLeave;
import com.business.system.domain.OaOvertime;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaLeaveApprovalService;
import com.business.system.service.workflow.IOaOvertimeApprovalService;
import org.springframework.stereotype.Service;


/**
 * 加班申请审批结果:审批拒绝
 */
@Service("oaOvertimeApprovalContent:processApproveResult:reject")
public class OaOvertimeApprovalRejectServiceImpl extends IOaOvertimeApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAOVERTIME_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaOvertime checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
