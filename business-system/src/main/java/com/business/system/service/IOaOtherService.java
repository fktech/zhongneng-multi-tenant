package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.OaOtherInstanceRelatedEditDTO;

/**
 * 其他申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaOtherService
{
    /**
     * 查询其他申请
     *
     * @param id 其他申请ID
     * @return 其他申请
     */
    public OaOther selectOaOtherById(String id);

    /**
     * 查询其他申请列表
     *
     * @param oaOther 其他申请
     * @return 其他申请集合
     */
    public List<OaOther> selectOaOtherList(OaOther oaOther);

    /**
     * 新增其他申请
     *
     * @param oaOther 其他申请
     * @return 结果
     */
    public int insertOaOther(OaOther oaOther);

    /**
     * 修改其他申请
     *
     * @param oaOther 其他申请
     * @return 结果
     */
    public int updateOaOther(OaOther oaOther);

    /**
     * 批量删除其他申请
     *
     * @param ids 需要删除的其他申请ID
     * @return 结果
     */
    public int deleteOaOtherByIds(String[] ids);

    /**
     * 删除其他申请信息
     *
     * @param id 其他申请ID
     * @return 结果
     */
    public int deleteOaOtherById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


    /**
     * 审批办理保存编辑业务数据
     * @param oaOtherInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(OaOtherInstanceRelatedEditDTO oaOtherInstanceRelatedEditDTO);
}
