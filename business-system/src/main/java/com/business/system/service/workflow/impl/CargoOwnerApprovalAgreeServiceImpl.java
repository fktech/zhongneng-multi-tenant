package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaCargoOwner;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.ICargoOwnerApprovalService;
import com.business.system.service.workflow.IEnterpriseApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 货主申请审批结果:审批通过
 */
@Service("cargoOwnerApprovalContent:processApproveResult:pass")
@Slf4j
public class CargoOwnerApprovalAgreeServiceImpl extends ICargoOwnerApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaCargoOwner baCargoOwner = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CARGOOWNER_STATUS_PASS.getCode());
        return result;
    }
}
