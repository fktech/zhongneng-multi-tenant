package com.business.system.service;

import com.business.system.domain.BaAttendance;
import com.business.system.domain.dto.ClockCountDTO;

import java.util.List;

/**
 * 离线消息推送Service接口
 *
 * @author single
 * @date 2024-8-28
 */
public interface IUniPushService
{
    /**
     * 推送消息
     *
     * @param cid 用户设备ID
     * @param title 标题
     * @param body 内容
     * @param payload
     * @return
     */
    public boolean pushMessage(String cid, String title, String body, String payload);
}
