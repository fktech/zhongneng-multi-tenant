package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaGoods;

/**
 * 商品Service接口
 *
 * @author ljb
 * @date 2022-12-01
 */
public interface IBaGoodsService
{
    /**
     * 查询商品
     *
     * @param id 商品ID
     * @return 商品
     */
    public BaGoods selectBaGoodsById(String id);

    /**
     * 查询商品列表
     *
     * @param baGoods 商品
     * @return 商品集合
     */
    public List<BaGoods> selectBaGoodsList(BaGoods baGoods);

    /**
     * 新增商品
     *
     * @param baGoods 商品
     * @return 结果
     */
    public int insertBaGoods(BaGoods baGoods);

    /**
     * 修改商品
     *
     * @param baGoods 商品
     * @return 结果
     */
    public int updateBaGoods(BaGoods baGoods);

    /**
     * 批量删除商品
     *
     * @param ids 需要删除的商品ID
     * @return 结果
     */
    public int deleteBaGoodsByIds(String[] ids);

    /**
     * 删除商品信息
     *
     * @param id 商品ID
     * @return 结果
     */
    public int deleteBaGoodsById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);



}
