package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaRenewMapper;
import com.business.system.domain.BaRenew;
import com.business.system.service.IBaRenewService;

/**
 * 市场动态更新Service业务层处理
 *
 * @author ljb
 * @date 2023-03-22
 */
@Service
public class BaRenewServiceImpl extends ServiceImpl<BaRenewMapper, BaRenew> implements IBaRenewService
{
    @Autowired
    private BaRenewMapper baRenewMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询市场动态更新
     *
     * @param id 市场动态更新ID
     * @return 市场动态更新
     */
    @Override
    public BaRenew selectBaRenewById(String id)
    {
        return baRenewMapper.selectBaRenewById(id);
    }

    /**
     * 查询市场动态更新列表
     *
     * @param baRenew 市场动态更新
     * @return 市场动态更新
     */
    @Override
    public List<BaRenew> selectBaRenewList(BaRenew baRenew)
    {
        return baRenewMapper.selectBaRenewList(baRenew);
    }

    /**
     * 新增市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    @Override
    public int insertBaRenew(BaRenew baRenew)
    {
        baRenew.setId(getRedisIncreID.getId());
        baRenew.setCreateTime(DateUtils.getNowDate());
        baRenew.setUserId(SecurityUtils.getUserId());
        baRenew.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baRenew.setTenantId(SecurityUtils.getCurrComId());
        return baRenewMapper.insertBaRenew(baRenew);
    }

    /**
     * 修改市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    @Override
    public int updateBaRenew(BaRenew baRenew)
    {
        baRenew.setUpdateTime(DateUtils.getNowDate());
        return baRenewMapper.updateBaRenew(baRenew);
    }

    /**
     * 批量删除市场动态更新
     *
     * @param ids 需要删除的市场动态更新ID
     * @return 结果
     */
    @Override
    public int deleteBaRenewByIds(String[] ids)
    {
        return baRenewMapper.deleteBaRenewByIds(ids);
    }

    /**
     * 删除市场动态更新信息
     *
     * @param id 市场动态更新ID
     * @return 结果
     */
    @Override
    public int deleteBaRenewById(String id)
    {
        return baRenewMapper.deleteBaRenewById(id);
    }

}
