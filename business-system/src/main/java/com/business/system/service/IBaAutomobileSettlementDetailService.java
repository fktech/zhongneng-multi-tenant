package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaAutomobileSettlementDetail;

/**
 * 付款明细Service接口
 *
 * @author ljb
 * @date 2023-04-12
 */
public interface IBaAutomobileSettlementDetailService
{
    /**
     * 查询付款明细
     *
     * @param id 付款明细ID
     * @return 付款明细
     */
    public BaAutomobileSettlementDetail selectBaAutomobileSettlementDetailById(String id);

    /**
     * 查询付款明细列表
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 付款明细集合
     */
    public List<BaAutomobileSettlementDetail> selectBaAutomobileSettlementDetailList(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 新增付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    public int insertBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 修改付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    public int updateBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail);

    /**
     * 批量删除付款明细
     *
     * @param ids 需要删除的付款明细ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementDetailByIds(String[] ids);

    /**
     * 删除付款明细信息
     *
     * @param id 付款明细ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementDetailById(String id);

    /**
     * 昕科推送付款状态
     */
    int updateDetail(JSONObject object);


}
