package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaChecked;
import com.business.system.domain.BaCheckedHead;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.ICheckedApprovalService;
import com.business.system.service.workflow.IIntodepotApprovalService;
import org.springframework.stereotype.Service;


/**
 * 盘点申请审批结果:审批拒绝
 */
@Service("checkedApprovalContent:processApproveResult:reject")
public class CheckedApprovalRejectServiceImpl extends ICheckedApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CHECKED_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaChecked checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
