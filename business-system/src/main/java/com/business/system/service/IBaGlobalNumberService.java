package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaGlobalNumber;

/**
 * 全局编号Service接口
 *
 * @author ljb
 * @date 2024-06-21
 */
public interface IBaGlobalNumberService
{
    /**
     * 查询全局编号
     *
     * @param id 全局编号ID
     * @return 全局编号
     */
    public BaGlobalNumber selectBaGlobalNumberById(String id);

    /**
     * 查询全局编号列表
     *
     * @param baGlobalNumber 全局编号
     * @return 全局编号集合
     */
    public List<BaGlobalNumber> selectBaGlobalNumberList(BaGlobalNumber baGlobalNumber);

    /**
     * 新增全局编号
     *
     * @param baGlobalNumber 全局编号
     * @return 结果
     */
    public int insertBaGlobalNumber(BaGlobalNumber baGlobalNumber);

    /**
     * 修改全局编号
     *
     * @param baGlobalNumber 全局编号
     * @return 结果
     */
    public int updateBaGlobalNumber(BaGlobalNumber baGlobalNumber);

    /**
     * 批量删除全局编号
     *
     * @param ids 需要删除的全局编号ID
     * @return 结果
     */
    public int deleteBaGlobalNumberByIds(String[] ids);

    /**
     * 删除全局编号信息
     *
     * @param id 全局编号ID
     * @return 结果
     */
    public int deleteBaGlobalNumberById(String id);


}
