package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.domain.vo.FlowElementVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 用印Service业务层处理
 *
 * @author ljb
 * @date 2023-03-14
 */
@Service
public class BaSealServiceImpl extends ServiceImpl<BaSealMapper, BaSeal> implements IBaSealService
{

    private static final Logger log = LoggerFactory.getLogger(BaSealServiceImpl.class);
    @Autowired
    private BaSealMapper baSealMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaFormworkMapper baFormworkMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询用印
     *
     * @param id 用印ID
     * @return 用印
     */
    @Override
    public BaSeal selectBaSealById(String id)
    {
        BaSeal seal = baSealMapper.selectBaSealById(id);
        //发起人
        SysUser user = sysUserMapper.selectUserById(seal.getUserId());
        user.setUserName(user.getNickName());
        //岗位名称
        String name = getName.getName(user.getUserId());
        if(name.equals("") == false){
            user.setPostName(name.substring(0,name.length()-1));
        }
        seal.setUser(user);
        //模板信息
        if(StringUtils.isNotEmpty(seal.getFormworkId())){
            BaFormwork baFormwork = baFormworkMapper.selectBaFormworkById(seal.getFormworkId());
            seal.setFormworkName(baFormwork.getName());
        }
        //合同启动信息
        if(StringUtils.isNotEmpty(seal.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(seal.getStartId());
            seal.setStartName(baContractStart.getName());
        }
        //创建人对应公司
        if (user.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        seal.setCompanyName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",seal.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        seal.setProcessInstanceId(processInstanceIds);
        return seal;
    }

    /**
     * 查询用印列表
     *
     * @param baSeal 用印
     * @return 用印
     */
    @Override
    public List<BaSeal> selectBaSealList(BaSeal baSeal)
    {
        List<BaSeal> baSeals = baSealMapper.selectBaSealList(baSeal);
        for (BaSeal seal:baSeals) {
            //岗位信息
            String name = getName.getName(seal.getUserId());
            //发起人名称
            if(name.equals("") == false){
                seal.setUserName(sysUserMapper.selectUserById(seal.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                seal.setUserName(sysUserMapper.selectUserById(seal.getUserId()).getNickName());
            }

            //模板名称
            if(StringUtils.isNotEmpty(seal.getFormworkId())){
                BaFormwork baFormwork = baFormworkMapper.selectBaFormworkById(seal.getFormworkId());
                seal.setFormworkName(baFormwork.getName());
            }
        }
        return baSeals;
    }

    /**
     * 新增用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    @Override
    public int insertBaSeal(BaSeal baSeal)
    {
        baSeal.setId(getRedisIncreID.getId());
        baSeal.setCreateTime(DateUtils.getNowDate());
        baSeal.setCreateBy(SecurityUtils.getUsername());
        baSeal.setUserId(SecurityUtils.getUserId());
        baSeal.setDeptId(SecurityUtils.getDeptId());
        //租户ID/system/notice
        baSeal.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baSeal.getWorkState())) {
            //流程发起状态
            baSeal.setWorkState("1");
            //默认审批流程已通过
            baSeal.setState(AdminCodeEnum.SEAL_STATUS_PASS.getCode());
        }
        //流程实例ID
        String flowId = "";
        String taskType = "";
        //普通用印
        if("1".equals(baSeal.getType())){
            taskType = AdminCodeEnum.TASK_TYPE_PRODUCT_GENERALSEAL.getCode();
        } else if("2".equals(baSeal.getType())){ //业务用印申请
            //用章类型是财务章
            if("1".equals(baSeal.getSealType())){
                taskType = AdminCodeEnum.TASK_TYPE_PRODUCT_FINANCIALSEAL.getCode();
            } else {
                taskType = AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode();
            }
        }
        flowId = iBaProcessDefinitionRelatedService.getFlowId(taskType,SecurityUtils.getCurrComId());
        baSeal.setFlowId(flowId);
        int result = baSealMapper.insertBaSeal(baSeal);
        //判断是否是业务用印
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baSeal.getWorkState())) {
                BaSeal seal = baSealMapper.selectBaSealById(baSeal.getId());
//                启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(seal, AdminCodeEnum.SEAL_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baSealMapper.deleteBaSealById(baSeal.getId());
                    return 0;
                } else {
                    seal.setState(AdminCodeEnum.SEAL_STATUS_VERIFYING.getCode());
                    baSealMapper.updateBaSeal(seal);
                }
            }
        }
        return result;
    }

    /**
     * 提交用印审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaSeal seal, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(seal.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(seal.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaSeal baSeal = this.selectBaSealById(seal.getId());
        SysUser sysUser = iSysUserService.selectUserById(baSeal.getUserId());
        baSeal.setUserName(sysUser.getUserName());
        baSeal.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baSeal, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);
        //设置任务处理人
        Map<String, Object> assigneeMap = new HashMap<>();
        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
//        BaProcessDefinitionDTO baProcessDefinitionDTO = new BaProcessDefinitionDTO();
//        baProcessDefinitionDTO.setProcessDefinitionId(processInstancesRuntime.getProcessDefinitionKey());
        //设置任务处理人数据
//        List<FlowElementVO> flowElementVOS = workFlowFeignClient.listDefinitionNode(baProcessDefinitionDTO);
//        String nodeId = "";
//        String[] assignee = new String[]{"4", "17"};
//        if(!ObjectUtils.isEmpty(flowElementVOS)){
//            if(!CollectionUtils.isEmpty(flowElementVOS)){
//                for(FlowElementVO flowElementVO : flowElementVOS){
//                    if("执行董事".equals(flowElementVO.getName())){
//                        nodeId = flowElementVO.getId()+"assigneeList";
//                    }
//                }
//            }
//        }
//        Map<String, Object> assigneeMap = new HashMap<>();
//        assigneeMap.put(nodeId, assignee);
//        startProcessInstanceDTO.setAssigneeMap(assigneeMap);
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.SEAL_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改用印
     *
     * @param baSeal 用印
     * @return 结果
     */
    @Override
    public int updateBaSeal(BaSeal baSeal)
    {
        //原数据
        BaSeal baSeal1 = baSealMapper.selectBaSealById(baSeal.getId());
        baSeal.setUpdateTime(DateUtils.getNowDate());
        baSeal.setUpdateBy(SecurityUtils.getUsername());
        baSeal.setCreateTime(baSeal.getUpdateTime());
        int result = baSealMapper.updateBaSeal(baSeal);
        if(result > 0 && !baSeal1.getState().equals(AdminCodeEnum.SEAL_STATUS_PASS.getCode())){
            BaSeal seal = baSealMapper.selectBaSealById(baSeal.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", seal.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(seal, AdminCodeEnum.SEAL_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baSealMapper.updateBaSeal(baSeal1);
                return 0;
            }else {
                seal.setState(AdminCodeEnum.SEAL_STATUS_VERIFYING.getCode());
                baSealMapper.updateBaSeal(seal);
            }
        }
        return result;
    }

    /**
     * 批量删除用印
     *
     * @param ids 需要删除的用印ID
     * @return 结果
     */
    @Override
    public int deleteBaSealByIds(String[] ids)
    {
        return baSealMapper.deleteBaSealByIds(ids);
    }

    /**
     * 删除用印信息
     *
     * @param id 用印ID
     * @return 结果
     */
    @Override
    public int deleteBaSealById(String id)
    {
        return baSealMapper.deleteBaSealById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaSeal baSeal = baSealMapper.selectBaSealById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baSeal.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.SEAL_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baSeal.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baSeal.setState(AdminCodeEnum.SEAL_STATUS_WITHDRAW.getCode());
                baSealMapper.updateBaSeal(baSeal);
                String userId = String.valueOf(baSeal.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baSeal, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public List<BaSeal> selectBaSealAuthorizedList(BaSeal baSeal) {
        List<BaSeal> baSeals = baSealMapper.selectBaSealAuthorizedList(baSeal);
        for (BaSeal seal:baSeals) {
            //岗位信息
            String name = getName.getName(seal.getUserId());
            //发起人名称
            if(name.equals("") == false){
                seal.setUserName(sysUserMapper.selectUserById(seal.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                seal.setUserName(sysUserMapper.selectUserById(seal.getUserId()).getNickName());
            }

            //模板名称
            if(StringUtils.isNotEmpty(seal.getFormworkId())){
                BaFormwork baFormwork = baFormworkMapper.selectBaFormworkById(seal.getFormworkId());
                seal.setFormworkName(baFormwork.getName());
            }
        }
        return baSeals;
    }

    @Override
    public int authorizedBaSeal(BaSeal baSeal) {
        return baSealMapper.updateBaSeal(baSeal);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSeal baSeal, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSeal.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SEAL.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"用印审批提醒",msgContent,baMessage.getType(),baSeal.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
