package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaFormworkMapper;
import com.business.system.domain.BaFormwork;
import com.business.system.service.IBaFormworkService;

/**
 * 模板Service业务层处理
 *
 * @author ljb
 * @date 2023-03-14
 */
@Service
public class BaFormworkServiceImpl extends ServiceImpl<BaFormworkMapper, BaFormwork> implements IBaFormworkService
{
    @Autowired
    private BaFormworkMapper baFormworkMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询模板
     *
     * @param id 模板ID
     * @return 模板
     */
    @Override
    public BaFormwork selectBaFormworkById(String id)
    {
        return baFormworkMapper.selectBaFormworkById(id);
    }

    /**
     * 查询模板列表
     *
     * @param baFormwork 模板
     * @return 模板
     */
    @Override
    public List<BaFormwork> selectBaFormworkList(BaFormwork baFormwork)
    {
        return baFormworkMapper.selectBaFormworkList(baFormwork);
    }

    /**
     * 新增模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    @Override
    public int insertBaFormwork(BaFormwork baFormwork)
    {
        baFormwork.setId(getRedisIncreID.getId());
        baFormwork.setCreateTime(DateUtils.getNowDate());
        baFormwork.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baFormwork.setTenantId(SecurityUtils.getCurrComId());
        return baFormworkMapper.insertBaFormwork(baFormwork);
    }

    /**
     * 修改模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    @Override
    public int updateBaFormwork(BaFormwork baFormwork)
    {
        baFormwork.setUpdateTime(DateUtils.getNowDate());
        return baFormworkMapper.updateBaFormwork(baFormwork);
    }

    /**
     * 批量删除模板
     *
     * @param ids 需要删除的模板ID
     * @return 结果
     */
    @Override
    public int deleteBaFormworkByIds(String[] ids)
    {
        return baFormworkMapper.deleteBaFormworkByIds(ids);
    }

    /**
     * 删除模板信息
     *
     * @param id 模板ID
     * @return 结果
     */
    @Override
    public int deleteBaFormworkById(String id)
    {
        return baFormworkMapper.deleteBaFormworkById(id);
    }

}
