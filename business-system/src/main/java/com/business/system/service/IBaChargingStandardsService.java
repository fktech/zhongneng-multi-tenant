package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaChargingStandards;

/**
 * 收费标准Service接口
 *
 * @author single
 * @date 2023-06-02
 */
public interface IBaChargingStandardsService
{
    /**
     * 查询收费标准
     *
     * @param id 收费标准ID
     * @return 收费标准
     */
    public BaChargingStandards selectBaChargingStandardsById(String id);

    /**
     * 查询收费标准列表
     *
     * @param baChargingStandards 收费标准
     * @return 收费标准集合
     */
    public List<BaChargingStandards> selectBaChargingStandardsList(BaChargingStandards baChargingStandards);

    /**
     * 新增收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    public int insertBaChargingStandards(BaChargingStandards baChargingStandards);

    /**
     * 修改收费标准
     *
     * @param baChargingStandards 收费标准
     * @return 结果
     */
    public int updateBaChargingStandards(BaChargingStandards baChargingStandards);

    /**
     * 批量删除收费标准
     *
     * @param ids 需要删除的收费标准ID
     * @return 结果
     */
    public int deleteBaChargingStandardsByIds(String[] ids);

    /**
     * 删除收费标准信息
     *
     * @param id 收费标准ID
     * @return 结果
     */
    public int deleteBaChargingStandardsById(String id);


}
