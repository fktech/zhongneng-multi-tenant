package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.system.domain.dto.NumsAndMoneyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaInvoiceDetailMapper;
import com.business.system.domain.BaInvoiceDetail;
import com.business.system.service.IBaInvoiceDetailService;

/**
 * 发票信息详情Service业务层处理
 *
 * @author single
 * @date 2023-03-14
 */
@Service
public class BaInvoiceDetailServiceImpl extends ServiceImpl<BaInvoiceDetailMapper, BaInvoiceDetail> implements IBaInvoiceDetailService
{
    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    /**
     * 查询发票信息详情
     *
     * @param id 发票信息详情ID
     * @return 发票信息详情
     */
    @Override
    public BaInvoiceDetail selectBaInvoiceDetailById(String id)
    {
        return baInvoiceDetailMapper.selectBaInvoiceDetailById(id);
    }

    /**
     * 查询发票信息详情列表
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 发票信息详情
     */
    @Override
    public List<BaInvoiceDetail> selectBaInvoiceDetailList(BaInvoiceDetail baInvoiceDetail)
    {
        return baInvoiceDetailMapper.selectBaInvoiceDetailList(baInvoiceDetail);
    }
    @Override
    public int selectBaInvoiceDetailByinviceId(String id) {
        return baInvoiceDetailMapper.selectBaInvoiceDetailByinviceId(id);
    }

    @Override
    public NumsAndMoneyDTO selectDetailByinviceIdMonth(String id) {
        return baInvoiceDetailMapper.selectDetailByinviceIdMonth(id) ;
    }

    /**
     * 新增发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    @Override
    public int insertBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail)
    {
        baInvoiceDetail.setCreateTime(DateUtils.getNowDate());
        return baInvoiceDetailMapper.insertBaInvoiceDetail(baInvoiceDetail);
    }

    /**
     * 修改发票信息详情
     *
     * @param baInvoiceDetail 发票信息详情
     * @return 结果
     */
    @Override
    public int updateBaInvoiceDetail(BaInvoiceDetail baInvoiceDetail)
    {
        baInvoiceDetail.setUpdateTime(DateUtils.getNowDate());
        return baInvoiceDetailMapper.updateBaInvoiceDetail(baInvoiceDetail);
    }

    /**
     * 批量删除发票信息详情
     *
     * @param ids 需要删除的发票信息详情ID
     * @return 结果
     */
    @Override
    public int deleteBaInvoiceDetailByIds(String[] ids)
    {
        return baInvoiceDetailMapper.deleteBaInvoiceDetailByIds(ids);
    }

    /**
     * 删除发票信息详情信息
     *
     * @param id 发票信息详情ID
     * @return 结果
     */
    @Override
    public int deleteBaInvoiceDetailById(String id)
    {
        return baInvoiceDetailMapper.deleteBaInvoiceDetailById(id);
    }

}
