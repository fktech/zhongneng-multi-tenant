package com.business.system.service;

import java.util.List;

import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaPoundRoom;

/**
 * 磅房数据Service接口
 *
 * @author ljb
 * @date 2023-09-04
 */
public interface IBaPoundRoomService
{
    /**
     * 查询磅房数据
     *
     * @param id 磅房数据ID
     * @return 磅房数据
     */
    public BaPoundRoom selectBaPoundRoomById(String id);

    /**
     * 查询磅房数据列表
     *
     * @param baPoundRoom 磅房数据
     * @return 磅房数据集合
     */
    public List<BaPoundRoom> selectBaPoundRoomList(BaPoundRoom baPoundRoom);

    /**
     * 新增磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    public int insertBaPoundRoom(BaPoundRoom baPoundRoom);

    /**
     * 批量新增
     */
    String importDepotHead(List<BaPoundRoom> poundRoomList);

    /**
     * 修改磅房数据
     *
     * @param baPoundRoom 磅房数据
     * @return 结果
     */
    public int updateBaPoundRoom(BaPoundRoom baPoundRoom);

    /**
     * 批量删除磅房数据
     *
     * @param ids 需要删除的磅房数据ID
     * @return 结果
     */
    public int deleteBaPoundRoomByIds(String[] ids);

    /**
     * 删除磅房数据信息
     *
     * @param id 磅房数据ID
     * @return 结果
     */
    public int deleteBaPoundRoomById(String id);

    public int association(BaPoundRoom baPoundRoom);


}
