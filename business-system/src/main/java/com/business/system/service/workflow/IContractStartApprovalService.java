package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaBidTransportService;
import com.business.system.service.IBaChargingStandardsService;
import com.business.system.service.IBaDeclareService;
import com.business.system.service.IBaMessageService;
import com.business.system.service.impl.BaShippingPlanServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 合同启动审批接口 服务类
 * @date: 2023/3/8 10:21
 */
@Service
public class IContractStartApprovalService {

    @Autowired
    protected BaContractStartMapper baContractStartMapper;

    @Autowired
    protected BaHiContractStartMapper baHiContractStartMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaEarlyWarningMapper baEarlyWarningMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaShippingPlanMapper baShippingPlanMapper;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;


    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaContractStart baContractStart = new BaContractStart();
        baContractStart.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        //变更状态
        BaHiContractStart baHiContractStart = baHiContractStartMapper.selectBaHiContractStartById(baContractStart.getId());

        String userId = "";
        if(AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode().equals(status)){
            BaContractStart baContractStart1 = new BaContractStart();
            if((ObjectUtils.isEmpty(baHiContractStart)) || (!ObjectUtils.isEmpty(baHiContractStart) && !"2".equals(baHiContractStart.getApproveFlag()))) {
                baContractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
                baContractStart.setState(status);
                userId = String.valueOf(baContractStart.getUserId());
                //设置合同启动面板业务阶段类型
                baContractStart.setStartType("1"); //供货中

                QueryWrapper<BaDeclare> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("start_id", approvalWorkflowDTO.getTargetId());
                queryWrapper.eq("flag", 1);
                List<BaDeclare> baDeclares = baDeclareMapper.selectList(queryWrapper);
                if (!CollectionUtils.isEmpty(baDeclares)) {
                    baDeclares.stream().forEach(item -> {
                        if (!ObjectUtils.isEmpty(item)) {
                            item.setSource(2); //合同启动审批通过计划申报
                            item.setFlag(new Long(0));
                            item.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode()); //审批通过
                            item.setUpdateTime(DateUtils.getNowDate());
                            baDeclareMapper.updateBaDeclare(item);
                        }
                    });
                }

                if ("5".equals(baContractStart.getType())) {
                    //合同启动序号
                    QueryWrapper<BaContractStart> baContractStartQueryWrapper = new QueryWrapper<>();
                    baContractStartQueryWrapper.eq("project_id", baContractStart.getProjectId());
                    baContractStartQueryWrapper.eq("type", "5");
                    //baContractStartQueryWrapper.eq("flag", 0);
                    //baContractStartQueryWrapper.eq("state", "contractStart:pass");
                    baContractStartQueryWrapper.orderByDesc("serial_number");
                    List<BaContractStart> contractStarts = baContractStartMapper.selectList(baContractStartQueryWrapper);
                    int num = 0;
                    if (!CollectionUtils.isEmpty(contractStarts)) {
                        if (contractStarts.get(0).getSerialNumber() != null) {
                            num = contractStarts.get(0).getSerialNumber();
                        }
                    } else {
                        num = 1;
                    }
                    baContractStart.setStartSerial(num);
                    //全局编号
                    if(StringUtils.isNotEmpty(baContractStart.getGlobalNumber())){
                        String[] s = baContractStart.getGlobalNumber().split("-");
                        if(num > 10){
                            baContractStart.setGlobalNumber(s[0]+"-"+num);
                        }else {
                            baContractStart.setGlobalNumber(s[0]+"-"+"0"+num);
                        }
                        //全局编号关系表新增数据
                        BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                        baGlobalNumber.setId(getRedisIncreID.getId());
                        baGlobalNumber.setBusinessId(baContractStart.getId());
                        baGlobalNumber.setCode(baContractStart.getGlobalNumber());
                        baGlobalNumber.setHierarchy("2");
                        baGlobalNumber.setType("2");
                        baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                        baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                        baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
                    }
                    //合同启动名称及简称修改
                    if (StringUtils.isNotEmpty(baContractStart.getName())) {
                        int lastIndex = baContractStart.getName().lastIndexOf("-");
                        String baContractStartName = baContractStart.getName().substring(0, lastIndex + 1);
                        if (num < 10) {
                            baContractStartName = baContractStartName + "0" + num;
                        } else {
                            baContractStartName = baContractStartName + num;
                        }
                        baContractStart.setName(baContractStartName);
                    }
                    if (StringUtils.isNotEmpty(baContractStart.getStartShorter())) {
                        int shortLastIndex = baContractStart.getStartShorter().lastIndexOf("-");
                        String startShorterName = baContractStart.getStartShorter().substring(0, shortLastIndex + 1);
                        if (num < 10) {
                            startShorterName = startShorterName + "0" + num;
                        } else {
                            startShorterName = startShorterName + num;
                        }
                        baContractStart.setStartShorter(startShorterName);
                    }
                    baContractStartMapper.updateBaContractStart(baContractStart);

                    //审批通过时新增预警
                    BaEarlyWarning baEarlyWarning = new BaEarlyWarning();
                    baEarlyWarning.setId(getRedisIncreID.getId());
                    baEarlyWarning.setRelevanceId(baContractStart.getId());
                    baEarlyWarning.setBusinessType("2");
                    baEarlyWarning.setAccountUpperLimit(baContractStart.getAccountUpperLimit());
                    baEarlyWarningMapper.insertBaEarlyWarning(baEarlyWarning);
                }
            } else {
                String id = baHiContractStart.getId();
                if(!ObjectUtils.isEmpty(baHiContractStart)){
                    userId = String.valueOf(baHiContractStart.getUserId());
                }
                baHiContractStart = baHiContractStartMapper.selectBaHiContractStartById(baHiContractStart.getId());
                baHiContractStart.setState(status);

                //设置合同启动面板业务阶段类型
                baHiContractStart.setStartType("1"); //供货中

                //新计划申报修改为原计划申报
                QueryWrapper<BaDeclare> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("start_id", baHiContractStart.getRelationId());
                queryWrapper.eq("flag", 0);
                List<BaDeclare> baDeclares = baDeclareMapper.selectList(queryWrapper);
                BaContractStart baContractStart2 = baContractStartMapper.selectBaContractStartById(baHiContractStart.getRelationId());

                //原计划申报修改为新计划申报
                queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("start_id", approvalWorkflowDTO.getTargetId());
                queryWrapper.eq("flag", 0);
                List<BaDeclare> baDeclaresHi = baDeclareMapper.selectList(queryWrapper);
                List<String> declareIds = null;
                if (!CollectionUtils.isEmpty(baDeclares)) {
                    declareIds = baDeclares.stream().map(BaDeclare::getId).collect(Collectors.toList());
                    baDeclareMapper.deleteBaDeclareByIds(declareIds.toArray(new String[0]));
                    for(BaDeclare item : baDeclares){
                        if (!ObjectUtils.isEmpty(item)) {
                            item.setSource(2); //合同启动审批通过计划申报
                            item.setFlag(new Long(0));
                            item.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode()); //审批通过
                            item.setStartId(approvalWorkflowDTO.getTargetId());
                            item.setUpdateTime(DateUtils.getNowDate());
                            baDeclareMapper.insertBaDeclare(item);
                        }
                    }
                }

                if (!CollectionUtils.isEmpty(baDeclaresHi)) {
                    declareIds = baDeclaresHi.stream().map(BaDeclare::getId).collect(Collectors.toList());
                    baDeclareMapper.deleteBaDeclareByIds(declareIds.toArray(new String[0]));
                    for(BaDeclare item : baDeclaresHi){
                        if (!ObjectUtils.isEmpty(item)) {
                            item.setSource(2); //合同启动审批通过计划申报
                            item.setFlag(new Long(0));
                            item.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode()); //审批通过
                            item.setStartId(baHiContractStart.getRelationId());
                            item.setUpdateTime(DateUtils.getNowDate());
                            baDeclareMapper.insertBaDeclare(item);
                        }
                    }
                }

                baHiContractStartMapper.updateBaHiContractStart(baHiContractStart);

                //查询原数据流程实例ID
                QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", baContractStart2.getId());

                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                BaProcessInstanceRelated baProcessInstanceRelatedBefore = new BaProcessInstanceRelated();
                BaProcessInstanceRelated baProcessInstanceRelatedBeforeTemp = new BaProcessInstanceRelated();
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelatedBefore = baProcessInstanceRelateds.get(0);
                    BeanUtils.copyBeanProp(baProcessInstanceRelatedBeforeTemp, baProcessInstanceRelatedBefore);
                }

                baContractStart1 = new BaContractStart();
                BeanUtils.copyBeanProp(baContractStart1, baHiContractStart);
                baContractStart1.setApproveFlag("0");
                baContractStart1.setId(baContractStart2.getId());
                baContractStart1.setState(status);
                baContractStartMapper.updateBaContractStartChange(baContractStart1);

                BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();

                relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", id);
                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                }

                BeanUtils.copyBeanProp(baHiContractStart, baContractStart2);
                baHiContractStart.setApproveFlag("0");
                baHiContractStart.setId(id);
                baHiContractStart.setState(status);
                baHiContractStartMapper.updateBaHiContractStartChange(baHiContractStart);

                baProcessInstanceRelatedBefore.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                baProcessInstanceRelatedBefore.setApproveType(baProcessInstanceRelated.getApproveType());
                baProcessInstanceRelatedBefore.setApproveResult(baProcessInstanceRelated.getApproveResult());
                baProcessInstanceRelatedBefore.setReason(baProcessInstanceRelated.getReason());
                baProcessInstanceRelatedBefore.setCreateBy(baProcessInstanceRelated.getCreateBy());
                baProcessInstanceRelatedBefore.setCreateTime(baProcessInstanceRelated.getCreateTime());
                baProcessInstanceRelatedBefore.setUpdateBy(baProcessInstanceRelated.getUpdateBy());
                baProcessInstanceRelatedBefore.setUpdateTime(baProcessInstanceRelated.getUpdateTime());
                baProcessInstanceRelatedBefore.setState(baProcessInstanceRelated.getState());
                baProcessInstanceRelatedBefore.setFlag(baProcessInstanceRelated.getFlag());
                baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelatedBefore); //对换流程实例ID

                baProcessInstanceRelated.setProcessInstanceId(baProcessInstanceRelatedBeforeTemp.getProcessInstanceId());
                baProcessInstanceRelated.setApproveType(baProcessInstanceRelatedBeforeTemp.getApproveType());
                baProcessInstanceRelated.setApproveResult(baProcessInstanceRelatedBeforeTemp.getApproveResult());
                baProcessInstanceRelated.setReason(baProcessInstanceRelatedBeforeTemp.getReason());
                baProcessInstanceRelated.setCreateBy(baProcessInstanceRelatedBeforeTemp.getCreateBy());
                baProcessInstanceRelated.setCreateTime(baProcessInstanceRelatedBeforeTemp.getCreateTime());
                baProcessInstanceRelated.setUpdateBy(baProcessInstanceRelatedBeforeTemp.getUpdateBy());
                baProcessInstanceRelated.setUpdateTime(baProcessInstanceRelatedBeforeTemp.getUpdateTime());
                baProcessInstanceRelated.setState(baProcessInstanceRelatedBeforeTemp.getState());
                baProcessInstanceRelated.setFlag(baProcessInstanceRelatedBeforeTemp.getFlag());
                baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelated); //对换流程实例ID

                //原运输计划修改为新运输计划
//                QueryWrapper<BaShippingPlan> shippingPlanQueryWrapper = new QueryWrapper<>();
//                shippingPlanQueryWrapper.eq("relevance_id", baContractStart2.getId());
//                List<BaShippingPlan> baShippingPlans = baShippingPlanMapper.selectList(shippingPlanQueryWrapper);
//
//                //新运输计划修改为原运输计划
//                shippingPlanQueryWrapper = new QueryWrapper<>();
//                shippingPlanQueryWrapper.eq("relevance_id", id);
//                List<BaShippingPlan> baShippingPlansHi = baShippingPlanMapper.selectList(shippingPlanQueryWrapper);
//                List<String> baShippingPlanIds = null;
//                if (!CollectionUtils.isEmpty(baShippingPlans)) {
//                    baShippingPlanIds = baShippingPlans.stream().map(BaShippingPlan::getId).collect(Collectors.toList());
//                    baShippingPlanMapper.deleteBaShippingPlanByIds(baShippingPlanIds.toArray(new String[0]));
//                    for (BaShippingPlan baShippingPlan : baShippingPlans) {
//                        baShippingPlan.setRelevanceId(baContractStart2.getId());
//                        baShippingPlanMapper.insertBaShippingPlan(baShippingPlan);
//                    }
//                }
//
//                if (!CollectionUtils.isEmpty(baShippingPlansHi)) {
//                    baShippingPlanIds = baShippingPlansHi.stream().map(BaShippingPlan::getId).collect(Collectors.toList());
//                    baShippingPlanMapper.deleteBaShippingPlanByIds(baShippingPlanIds.toArray(new String[0]));
//                    for (BaShippingPlan baShippingPlan : baShippingPlansHi) {
//                        baShippingPlan.setRelevanceId(id);
//                        baShippingPlanMapper.insertBaShippingPlan(baShippingPlan);
//                    }
//                }

                //原收费标准修改为新收费标准
                QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
                baChargingStandardsQueryWrapper.eq("relation_id", baContractStart2.getId());
                List<BaChargingStandards> baChargingStandards = baChargingStandardsMapper.selectList(baChargingStandardsQueryWrapper);

                //新收费标准修改为原收费标准
                baChargingStandardsQueryWrapper = new QueryWrapper<>();
                baChargingStandardsQueryWrapper.eq("relation_id", id);
                List<BaChargingStandards> baChargingStandardsHi = baChargingStandardsMapper.selectList(baChargingStandardsQueryWrapper);
                List<String> baChargingStandardIds = null;
                if(!CollectionUtils.isEmpty(baChargingStandards)){
                    baChargingStandardIds = baChargingStandards.stream().map(BaChargingStandards::getId).collect(Collectors.toList());
                    baChargingStandardsMapper.deleteBaChargingStandardsByIds(baChargingStandardIds.toArray(new String[0]));
                    for(BaChargingStandards baChargingStandards1 : baChargingStandards){
                        baChargingStandards1.setRelationId(id);
                        baChargingStandardsMapper.insertBaChargingStandards(baChargingStandards1);
                    }
                }

                if(!CollectionUtils.isEmpty(baChargingStandardsHi)){
                    baChargingStandardIds = baChargingStandardsHi.stream().map(BaChargingStandards::getId).collect(Collectors.toList());
                    baChargingStandardsMapper.deleteBaChargingStandardsByIds(baChargingStandardIds.toArray(new String[0]));
                    for(BaChargingStandards baChargingStandards2 : baChargingStandardsHi){
                        baChargingStandards2.setRelationId(baContractStart2.getId());
                        baChargingStandardsMapper.insertBaChargingStandards(baChargingStandards2);
                    }
                }

                //原合同启动投标信息运输信息修改为新合同启动投标信息运输信息
//                QueryWrapper<BaBidTransport> baBidTransportsQueryWrapper = new QueryWrapper<>();
//                baBidTransportsQueryWrapper.eq("start_id", baContractStart2.getId());
//                List<BaBidTransport> baBidTransports = baBidTransportMapper.selectList(baBidTransportsQueryWrapper);
//
//                //新合同启动投标信息运输信息修改为原合同启动投标信息运输信息
//                baBidTransportsQueryWrapper = new QueryWrapper<>();
//                baBidTransportsQueryWrapper.eq("start_id", id);
//                List<BaBidTransport> baBidTransportsHi = baBidTransportMapper.selectList(baBidTransportsQueryWrapper);
//                if (!CollectionUtils.isEmpty(baBidTransports)) {
//                    for(BaBidTransport baBidTransport : baBidTransports){
//                        baBidTransport.setStartId(baContractStart2.getId());
//                        baBidTransportMapper.updateBaBidTransportChange(baBidTransport);
//                    }
//                }
//
//                if (!CollectionUtils.isEmpty(baBidTransportsHi)) {
//                    for(BaBidTransport baBidTransport : baBidTransportsHi){
//                        baBidTransport.setStartId(id);
//                        baBidTransportMapper.updateBaBidTransportChange(baBidTransport);
//                    }
//                }
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baContractStart, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.CONTRACTSTART_STATUS_REJECT.getCode().equals(status)){
            baContractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
            userId = String.valueOf(baContractStart.getUserId());
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baContractStart, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baContractStart, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baContractStart = baContractStartMapper.selectBaContractStartById(baContractStart.getId());
            if(!ObjectUtils.isEmpty(baContractStart)){
                baContractStart.setState(status);
                baContractStartMapper.updateById(baContractStart);
            } else if(ObjectUtils.isEmpty(baHiContractStart)){
                baHiContractStart.setState(status);
                baHiContractStart.setApproveFlag("0");
                baHiContractStartMapper.updateById(baHiContractStart);
            }
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaContractStart baContractStart, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baContractStart.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTSTART.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"合同启动审批提醒",msgContent,baMessage.getType(),baContractStart.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaContractStart checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        //变更状态
        BaHiContractStart baHiContractStart = baHiContractStartMapper.selectBaHiContractStartById(approvalWorkflowDTO.getTargetId());
        BaContractStart baContractStart = new BaContractStart();
        if((ObjectUtils.isEmpty(baHiContractStart)) || (!ObjectUtils.isEmpty(baHiContractStart) && !"2".equals(baHiContractStart.getApproveFlag()))) {
            baContractStart = baContractStartMapper.selectById(approvalWorkflowDTO.getTargetId());
            if (ObjectUtils.isEmpty(baContractStart)) {
                throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CONTRACTSTART_001);
            }
            if (!AdminCodeEnum.CONTRACTSTART_STATUS_VERIFYING.getCode().equals(baContractStart.getState())) {
                throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CONTRACTSTART_002);
            }
        }
        return baContractStart;
    }
}
