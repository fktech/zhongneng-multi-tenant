package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 计划申报Service业务层处理
 *
 * @author js
 * @date 2023-03-22
 */
@Service
public class BaDeclareServiceImpl extends ServiceImpl<BaDeclareMapper, BaDeclare> implements IBaDeclareService
{
    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private BaDeclareDetailMapper baDeclareDetailMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    private static final Logger log = LoggerFactory.getLogger(BaDeclareServiceImpl.class);

    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    @Override
    public BaDeclare selectBaDeclareById(String id)
    {
//        baDeclareMapper.selectBaDeclareById(id);
        BaDeclare baDeclare = baDeclareMapper.selectBaDeclareById(id);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baDeclare.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baDeclare.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baDeclare.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }

        baDeclare.setUser(sysUser);

        //发票详情信息
        BaDeclareDetail baDeclareDetail = new BaDeclareDetail();
        baDeclareDetail.setDeclareId(baDeclare.getId());
        List<BaDeclareDetail> baDeclareDetailList = baDeclareDetailMapper.selectBaDeclareDetailList(baDeclareDetail);
        if(!CollectionUtils.isEmpty(baDeclareDetailList)){
            for(BaDeclareDetail baDeclareDetail1 : baDeclareDetailList){
                baDeclareDetailList.stream().forEach(item -> {
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
                    if(!ObjectUtils.isEmpty(baContractStart)){
                        //合同启动名称
                        item.setStartName(baContractStart.getName());
                    }
                });
            }
        }
        baDeclare.setStartName(baContractStartMapper.selectBaContractStartById(baDeclare.getStartId()).getName());
        baDeclare.setBaDeclareDetailList(baDeclareDetailList);
        return baDeclare;
    }

    /**
     * 查询计划申报列表
     *
     * @param baDeclare 计划申报
     * @return 计划申报
     */
    @Override
    @DataScope(deptAlias = "d",userAlias = "d")
    public List<BaDeclare> selectBaDeclareList(BaDeclare baDeclare)
    {
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        List<BaDeclare> baDeclares = baDeclareMapper.selectBaDeclareListByCondition(baDeclare);
        if(!CollectionUtils.isEmpty(baDeclares)){
            baDeclares.stream().forEach(item -> {
                BaDeclareDetail baDeclareDetail = new BaDeclareDetail();
                baDeclareDetail.setDeclareId(item.getId());
                List<BaDeclareDetail> baDeclareDetailList = baDeclareDetailMapper.selectBaDeclareDetailList(baDeclareDetail);
                if(!CollectionUtils.isEmpty(baDeclareDetailList)){
                    for(BaDeclareDetail baDeclareDetail1 : baDeclareDetailList){
                        if(!ObjectUtils.isEmpty(baDeclareDetail1)){
                            if(item.getDeliveryCount() == null){
                                item.setDeliveryCount(new BigDecimal(0));
                            }
                            //计划发货量
                            item.setDeliveryCount(item.getDeliveryCount().add(baDeclareDetail1.getDeliveryCount()));
                            if(item.getPlannedAmount() == null){
                                item.setPlannedAmount(new BigDecimal(0));
                            }
                            //计划资金
                            item.setPlannedAmount(item.getPlannedAmount().add(baDeclareDetail1.getPlannedAmount()));
                        }
                    }
                }
                //合同启动名称
                if(StringUtils.isNotEmpty(item.getStartId())){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
                    if(StringUtils.isNotNull(baContractStart)){
                        item.setStartName(baContractStart.getName());
                    }
                }
                SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
                if(!ObjectUtils.isEmpty(sysUser)){
                    SysDept dept = sysUser.getDept();
                    if(!ObjectUtils.isEmpty(dept)){
                        item.setDeptName(dept.getDeptName());
                    }
                }
                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("")==false){
                    item.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUser.getNickName());
                }

            });
        }
        return baDeclares;
    }

    /**
     * 新增计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    @Override
    public int insertBaDeclare(BaDeclare baDeclare)
    {
       /* baDeclare.setId(getRedisIncreID.getId());
        baDeclare.setCreateTime(DateUtils.getNowDate());
        baDeclare.setCreateBy(SecurityUtils.getUsername());
        baDeclare.setUpdateBy(SecurityUtils.getUsername());
        baDeclare.setUpdateTime(DateUtils.getNowDate());
        baDeclare.setUserId(SecurityUtils.getUserId());
        baDeclare.setDeptId(SecurityUtils.getDeptId());
        //合同启动名称拼接
        StringBuilder stringBuilder = new StringBuilder();
        //插入计划申报详情信息
        List<BaDeclareDetail> baDeclareDetailList = baDeclare.getBaDeclareDetailList();
        if(!CollectionUtils.isEmpty(baDeclareDetailList)){
            for(BaDeclareDetail baDeclareDetail : baDeclareDetailList){
                baDeclareDetail.setId(getRedisIncreID.getId());
                baDeclareDetail.setDeclareId(baDeclare.getId());
                baDeclareDetail.setCreateBy(SecurityUtils.getUsername());
                baDeclareDetail.setCreateTime(DateUtils.getNowDate());
                if(stringBuilder.length() > 0){
                    stringBuilder.append(", ").append(baDeclareDetail.getStartName());
                } else {
                    stringBuilder.append(baDeclareDetail.getStartName());
                }
            }
            baDeclareDetailMapper.batchInsertBaDeclareDetail(baDeclareDetailList);
            //合同启动名称
            baDeclare.setStartName(stringBuilder.toString());
        }
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode());
            baDeclare.setFlowId(flowId);
            Integer result = baDeclareMapper.insertBaDeclare(baDeclare);
            if (result > 0) {
                baDeclare = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDeclare, AdminCodeEnum.DECLARE_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baDeclareMapper.deleteBaDeclareById(baDeclare.getId());
                    return 0;
                } else {
                    BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
                    baDeclare1.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
                    baDeclareMapper.updateBaDeclare(baDeclare1);
                }
            }
        return 1;*/
        BaContractStart baContractStart = null;
        BaDeclare baDeclare1  = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
        if(StringUtils.isNotNull(baDeclare1)){
            return -1;
        }
        baDeclare.setId(getRedisIncreID.getId());
        baDeclare.setCreateTime(DateUtils.getNowDate());
        baDeclare.setCreateBy(SecurityUtils.getUsername());
        baDeclare.setUpdateBy(SecurityUtils.getUsername());
        baDeclare.setUpdateTime(DateUtils.getNowDate());
        baDeclare.setUserId(SecurityUtils.getUserId());
        baDeclare.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        if(StringUtils.isNotEmpty(baDeclare.getStartId())){
            baContractStart = baContractStartMapper.selectBaContractStartById(baDeclare.getStartId());
            baDeclare.setStartName(baContractStart.getName());
            //判断业务归属公司是否是上海子公司
            if(!"2".equals(baDeclare.getWorkState())) {
                //流程发起状态
                baDeclare.setWorkState("1");
                //默认审批流程已通过
                baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_PASS.getCode());
            }
        }

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode(),SecurityUtils.getCurrComId());
        baDeclare.setFlowId(flowId);
        Integer result = baDeclareMapper.insertBaDeclare(baDeclare);
        if (result > 0) {
            if("2".equals(baDeclare.getWorkState())){
                baDeclare = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
    //            启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDeclare, AdminCodeEnum.DECLARE_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baDeclareMapper.deleteBaDeclareById(baDeclare.getId());
                    return 0;
                } else {
                    BaDeclare baDeclare2 = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
                    baDeclare2.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
                    baDeclareMapper.updateBaDeclare(baDeclare2);
                }
            }
        }
        return 1;

    }

    /**
     * 计划申报提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaDeclare declare, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(declare.getFlowId());
        Map<String, Object> map = new HashMap<>();
        //计划申报
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(declare.getId());
        //计划申报
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode());
        //获取流程实例关联的业务对象
        BaDeclare baDeclare = baDeclareMapper.selectBaDeclareById(declare.getId());
        SysUser sysUser = iSysUserService.selectUserById(baDeclare.getUserId());
        baDeclare.setUserName(sysUser.getUserName());
        baDeclare.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baDeclare));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);
        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.DECLARE_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }

        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改计划申报
     *
     * @param baDeclare 计划申报
     * @return 结果
     */
    @Override
    public int updateBaDeclare(BaDeclare baDeclare)
    {
      /*  //插入发票详情信息
        List<BaDeclareDetail> baDeclareDetailList = baDeclare.getBaDeclareDetailList();
        //合同启动名称拼接
        StringBuilder stringBuilder = new StringBuilder();
        BaDeclare declare = baDeclareMapper.selectBaDeclareById(baDeclare.getId());
        if(!CollectionUtils.isEmpty(baDeclareDetailList)){
            for(BaDeclareDetail baDeclareDetail : baDeclareDetailList){
                baDeclareDetail.setId(getRedisIncreID.getId());
                baDeclareDetail.setDeclareId(baDeclare.getId());
                baDeclareDetail.setCreateBy(SecurityUtils.getUsername());
                baDeclareDetail.setCreateTime(DateUtils.getNowDate());
                if(stringBuilder.length() > 0){
                    stringBuilder.append(", ").append(baDeclareDetail.getStartName());
                } else {
                    stringBuilder.append(baDeclareDetail.getStartName());
                }
            }
            //合同启动名称
            baDeclare.setStartName(stringBuilder.toString());
        }
        baDeclare.setUpdateTime(DateUtils.getNowDate());
        Integer result = baDeclareMapper.updateBaDeclare(baDeclare);
        if(result > 0){
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baDeclare.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDeclare, AdminCodeEnum.DECLARE_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                declare.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
                declare.setCreateTime(baDeclare.getUpdateTime());
                baDeclareDetailMapper.deleteBaDeclareDetailByDeclareId(baDeclare.getId());
                baDeclareDetailMapper.batchInsertBaDeclareDetail(baDeclareDetailList);
                return 0;
            }else {
                baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
                baDeclare.setCreateTime(baDeclare.getUpdateTime());
                baDeclareDetailMapper.deleteBaDeclareDetailByDeclareId(baDeclare.getId());
                baDeclareDetailMapper.batchInsertBaDeclareDetail(baDeclareDetailList);
                baDeclareMapper.updateBaDeclare(baDeclare);
            }
        }
        return result;*/
       BaDeclare declare = baDeclareMapper.selectBaDeclareById(baDeclare.getId());

        if(declare.getStartId().equals (baDeclare.getStartId())){
             if(!declare.getPlanTime().equals(baDeclare.getPlanTime())){
                BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
                if (StringUtils.isNotNull(baDeclare1)) {
                    return -1;
                }
            }
         }
        if(!declare.getStartId().equals (baDeclare.getStartId())) {
            BaDeclare baDeclare1 = baDeclareMapper.selectBaDeclareBystartId(baDeclare);
            if (StringUtils.isNotNull(baDeclare1)) {
                return -1;
            }
        }
            //合同启动名称
        if(StringUtils.isNotEmpty(baDeclare.getStartId())){
            baDeclare.setStartName((baContractStartMapper.selectBaContractStartById(baDeclare.getStartId())).getName());
        }
        baDeclare.setUpdateTime(DateUtils.getNowDate());
        Integer result = baDeclareMapper.updateBaDeclare(baDeclare);
        if(result > 0){
    //查询流程与实例关系表
    QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("business_id",baDeclare.getId());
    List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
    if(baProcessInstanceRelateds.size() > 0){
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            baProcessInstanceRelated.setFlag(1);
            baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
        }
    }
    //启动流程实例
    BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDeclare, AdminCodeEnum.DECLARE_APPROVAL_CONTENT_UPDATE.getCode());
    if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
       /* declare.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
        declare.setCreateTime(baDeclare.getUpdateTime());
        baDeclareDetailMapper.deleteBaDeclareDetailByDeclareId(baDeclare.getId());
        baDeclareDetailMapper.batchInsertBaDeclareDetail(baDeclareDetailList);*/
        return 0;
    }else {
        baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_VERIFYING.getCode());
        baDeclare.setCreateTime(baDeclare.getUpdateTime());
        baDeclareMapper.updateBaDeclare(baDeclare);
    }
}
        return result;
    }

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的计划申报ID
     * @return 结果
     */
    @Override
    public int deleteBaDeclareByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaDeclare baDeclare = baDeclareMapper.selectBaDeclareById(id);
                baDeclare.setFlag(new Long(1));
                baDeclare.setSource(0);
                baDeclareMapper.updateBaDeclare(baDeclare);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除计划申报信息
     *
     * @param id 计划申报ID
     * @return 结果
     */
    @Override
    public int deleteBaDeclareById(String id)
    {
        return baDeclareMapper.deleteBaDeclareById(id);
    }

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询计划申报
            BaDeclare baDeclare = baDeclareMapper.selectBaDeclareById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baDeclare.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.DECLARE_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baDeclare.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //流程审批状态修改
                baDeclare.setState(AdminCodeEnum.DECLARE_STATUS_WITHDRAW.getCode());

                baDeclareMapper.updateBaDeclare(baDeclare);
                String userId = String.valueOf(baDeclare.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baDeclare, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public UR thisMonth() {
        Integer thisMonth = baDeclareMapper.thisMonth(SecurityUtils.getDeptId());
        return UR.ok().data("本月数据",thisMonth);
    }

    @Override
    @DataScope(deptAlias = "g",userAlias = "g")
    public UR thisMonth1(BaDeclare baDeclare) {
       /* baDeclare.setDeptId(SecurityUtils.getDeptId());*/
        //租户ID
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        Integer thisMonth1 = baDeclareMapper.thisMonth1(baDeclare);
        return UR.ok().data("本月数据",thisMonth1);
    }

    @Override
    public List<BaDeclare> selectBaDeclareAuthorizedList(BaDeclare baDeclare) {
        baDeclare.setTenantId(SecurityUtils.getCurrComId());
        List<BaDeclare> baDeclares = baDeclareMapper.selectBaDeclareAuthorizedListByCondition(baDeclare);
        if(!CollectionUtils.isEmpty(baDeclares)){
            baDeclares.stream().forEach(item -> {
                BaDeclareDetail baDeclareDetail = new BaDeclareDetail();
                baDeclareDetail.setDeclareId(item.getId());
                List<BaDeclareDetail> baDeclareDetailList = baDeclareDetailMapper.selectBaDeclareDetailList(baDeclareDetail);
                if(!CollectionUtils.isEmpty(baDeclareDetailList)){
                    for(BaDeclareDetail baDeclareDetail1 : baDeclareDetailList){
                        if(!ObjectUtils.isEmpty(baDeclareDetail1)){
                            if(item.getDeliveryCount() == null){
                                item.setDeliveryCount(new BigDecimal(0));
                            }
                            //计划发货量
                            item.setDeliveryCount(item.getDeliveryCount().add(baDeclareDetail1.getDeliveryCount()));
                            if(item.getPlannedAmount() == null){
                                item.setPlannedAmount(new BigDecimal(0));
                            }
                            //计划资金
                            item.setPlannedAmount(item.getPlannedAmount().add(baDeclareDetail1.getPlannedAmount()));
                        }
                    }
                }
                //合同启动名称
                if(StringUtils.isNotEmpty(item.getStartId())){
                    item.setStartName(baContractStartMapper.selectBaContractStartById(item.getStartId()).getName());
                }
                SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
                if(!ObjectUtils.isEmpty(sysUser)){
                    SysDept dept = sysUser.getDept();
                    if(!ObjectUtils.isEmpty(dept)){
                        item.setDeptName(dept.getDeptName());
                    }
                }
                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("")==false){
                    item.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUser.getNickName());
                }

            });
        }
        return baDeclares;
    }

    @Override
    public int authorizedBaDeclare(BaDeclare baDeclare) {
        return baDeclareMapper.updateBaDeclare(baDeclare);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaDeclare baDeclare, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baDeclare.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_DECLARE.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"计划申报审批提醒",msgContent,baMessage.getType(),baDeclare.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }
}
