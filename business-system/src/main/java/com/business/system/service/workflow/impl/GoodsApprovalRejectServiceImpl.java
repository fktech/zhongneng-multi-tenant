package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaGoods;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IDeclareApprovalService;
import com.business.system.service.workflow.IGoodsApprovalService;
import org.springframework.stereotype.Service;


/**
 * 商品审批结果:审批拒绝
 */
@Service("goodsApprovalContent:processApproveResult:reject")
public class GoodsApprovalRejectServiceImpl extends IGoodsApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.GOODS_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaGoods checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
