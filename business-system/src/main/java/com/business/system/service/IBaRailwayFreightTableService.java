package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaRailwayFreightTable;

/**
 * 铁路运费Service接口
 *
 * @author ljb
 * @date 2022-11-29
 */
public interface IBaRailwayFreightTableService
{
    /**
     * 查询铁路运费
     *
     * @param id 铁路运费ID
     * @return 铁路运费
     */
    public BaRailwayFreightTable selectBaRailwayFreightTableById(String id);

    /**
     * 查询铁路运费列表
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 铁路运费集合
     */
    public List<BaRailwayFreightTable> selectBaRailwayFreightTableList(BaRailwayFreightTable baRailwayFreightTable);

    /**
     * 新增铁路运费
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 结果
     */
    public int insertBaRailwayFreightTable(BaRailwayFreightTable baRailwayFreightTable);

    /**
     * 修改铁路运费
     *
     * @param baRailwayFreightTable 铁路运费
     * @return 结果
     */
    public int updateBaRailwayFreightTable(BaRailwayFreightTable baRailwayFreightTable);

    /**
     * 批量删除铁路运费
     *
     * @param ids 需要删除的铁路运费ID
     * @return 结果
     */
    public int deleteBaRailwayFreightTableByIds(String[] ids);

    /**
     * 删除铁路运费信息
     *
     * @param id 铁路运费ID
     * @return 结果
     */
    public int deleteBaRailwayFreightTableById(String id);


}
