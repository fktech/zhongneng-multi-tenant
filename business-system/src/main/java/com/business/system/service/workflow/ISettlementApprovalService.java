package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 结算管理审批接口 服务类
 * @date: 2023/3/10 15:35
 */
@Service
public class ISettlementApprovalService {

    @Autowired
    protected BaSettlementMapper baSettlementMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaSettlement baSettlement = new BaSettlement();
        baSettlement.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baSettlement = baSettlementMapper.selectBaSettlementById(baSettlement.getId());
        baSettlement.setState(status);
        String userId = String.valueOf(baSettlement.getUserId());
        if(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode().equals(status)){
            if(!ObjectUtils.isEmpty(baSettlement)){
                //下游结算单
                if(baSettlement.getSettlementType() == 2){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baSettlement.getContractId());
                    if(!ObjectUtils.isEmpty(baContractStart)){
                        //完结的合同启动不更改原有状态
                        if(baContractStart.getStartType().equals("4") == false) {
                            baContractStart.setStartType("3");
                        }
                    }
                    baContractStartMapper.updateBaContractStart(baContractStart);
                }
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //全局编号
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baSettlement.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baSettlement.getId());
                baGlobalNumber.setCode(baSettlement.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("5");
                baGlobalNumber.setStartId(baSettlement.getContractId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }

            //插入消息通知信息
            this.insertMessage(baSettlement, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.SETTLEMENT_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baSettlement, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baSettlement, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baSettlementMapper.updateById(baSettlement);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSettlement baSettlement, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSettlement.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"结算审批提醒",msgContent,baMessage.getType(),baSettlement.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaSettlement checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSettlement baSettlement = baSettlementMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baSettlement == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SETTLEMENT_001);
        }
        if (!AdminCodeEnum.SETTLEMENT_STATUS_VERIFYING.getCode().equals(baSettlement.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_SETTLEMENT_002);
        }
        return baSettlement;
    }
}
