package com.business.system.service.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaCargoOwnerService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 货主档案Service业务层处理
 *
 * @author ljb
 * @date 2023-12-22
 */
@Service
public class BaCargoOwnerServiceImpl extends ServiceImpl<BaCargoOwnerMapper, BaCargoOwner> implements IBaCargoOwnerService
{
    private static final Logger log = LoggerFactory.getLogger(BaCargoOwnerServiceImpl.class);
    @Autowired
    private BaCargoOwnerMapper baCargoOwnerMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private PostName getName;

    /**
     * 查询货主档案
     *
     * @param id 货主档案ID
     * @return 货主档案
     */
    @Override
    public BaCargoOwner selectBaCargoOwnerById(String id)
    {
        BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(id);
        //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baCargoOwner.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.isNull("state");
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        baCargoOwner.setBaBankList(baBanks);
        //仓库名称
        if(StringUtils.isNotEmpty(baCargoOwner.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baCargoOwner.getDepotId());
            baCargoOwner.setDepotName(baDepot.getName());
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baCargoOwner.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baCargoOwner.setProcessInstanceId(processInstanceIds);
        //开票信息
        QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
        informationQueryWrapper.eq("relevance_id",id);
        informationQueryWrapper.orderByDesc(id);
        List<BaBillingInformation> baBillingInformation = baBillingInformationMapper.selectList(informationQueryWrapper);
        if(baBillingInformation.size() > 0){
            baCargoOwner.setBillingInformation(baBillingInformation.get(0));
        }
        //发起人信息
        SysUser sysUser = sysUserMapper.selectUserById(baCargoOwner.getUserId());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baCargoOwner.setUser(sysUser);
        return baCargoOwner;
    }

    /**
     * 查询货主档案列表
     *
     * @param baCargoOwner 货主档案
     * @return 货主档案
     */
    @Override
    public List<BaCargoOwner> selectBaCargoOwnerList(BaCargoOwner baCargoOwner)
    {
        List<BaCargoOwner> baCargoOwners = baCargoOwnerMapper.selectBaCargoOwnerList(baCargoOwner);
        for (BaCargoOwner cargoOwner:baCargoOwners) {
            SysUser sysUser = sysUserMapper.selectUserById(cargoOwner.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位名称
                String name = getName.getName(sysUser.getUserId());
                //发起人
                if(name.equals("") == false){
                    cargoOwner.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    cargoOwner.setUserName(sysUser.getNickName());
                }

            }
        }
        return baCargoOwners;
    }

    /**
     * 新增货主档案
     *
     * @param baCargoOwner 货主档案
     * @return 结果
     */
    @Override
    public int insertBaCargoOwner(BaCargoOwner baCargoOwner)
    {
        //判断货主简称不能重复
        QueryWrapper<BaCargoOwner> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("shorter_form",baCargoOwner.getShorterForm());
        queryWrapper.eq("flag",0);
        BaCargoOwner selectOne = baCargoOwnerMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -1;
        }
        //判断名称不能重复
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baCargoOwner.getName());
        queryWrapper.eq("flag",0);
        selectOne = baCargoOwnerMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -2;
        }
        baCargoOwner.setId(getRedisIncreID.getId());
        baCargoOwner.setCreateTime(DateUtils.getNowDate());
        baCargoOwner.setCreateBy(SecurityUtils.getUsername());
        baCargoOwner.setUserId(SecurityUtils.getUserId());
        baCargoOwner.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baCargoOwner.setTenantId(SecurityUtils.getCurrComId());
        //新增开票信息
        if(StringUtils.isNotNull(baCargoOwner.getBillingInformation())){
            BaBillingInformation baBillingInformation = baCargoOwner.getBillingInformation();
            baBillingInformation.setId(getRedisIncreID.getId());
            baBillingInformation.setRelevanceId(baCargoOwner.getId());
            baBillingInformationMapper.insertBaBillingInformation(baBillingInformation);
        }
        //银行账户信息
        List<BaBank> baBankList = baCargoOwner.getBaBankList();
        if(!CollectionUtils.isEmpty(baBankList)){
            baBankList.stream().forEach(item -> {
                item.setId(getRedisIncreID.getId());
                item.setRelationId(baCargoOwner.getId());
                item.setCreateTime(DateUtils.getNowDate());
                item.setCreateBy(SecurityUtils.getUsername());
                baBankMapper.insertBaBank(item);
            });
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getCode(),SecurityUtils.getCurrComId());
        baCargoOwner.setFlowId(flowId);
        int result = baCargoOwnerMapper.insertBaCargoOwner(baCargoOwner);
        if(result > 0){
            BaCargoOwner cargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(baCargoOwner.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(cargoOwner, AdminCodeEnum.CARGOOWNER_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baCargoOwnerMapper.deleteBaCargoOwnerById(cargoOwner.getId());
                return 0;
            }else {
                cargoOwner.setState(AdminCodeEnum.CARGOOWNER_STATUS_VERIFYING.getCode());
                baCargoOwnerMapper.updateBaCargoOwner(cargoOwner);
            }
        }
        return result;
    }

    /**
     * 货主档案审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaCargoOwner cargoOwner, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(cargoOwner.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(cargoOwner.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaCargoOwner baCargoOwner = this.selectBaCargoOwnerById(cargoOwner.getId());
        SysUser sysUser = iSysUserService.selectUserById(cargoOwner.getUserId());
        baCargoOwner.setUserName(sysUser.getNickName());
        baCargoOwner.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baCargoOwner, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CARGOOWNER_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改货主档案
     *
     * @param baCargoOwner 货主档案
     * @return 结果
     */
    @Override
    public int updateBaCargoOwner(BaCargoOwner baCargoOwner)
    {
        //获取原货主信息
        BaCargoOwner cargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(baCargoOwner.getId());
        //判断简称是否一样
        if(cargoOwner.getShorterForm().equals(baCargoOwner.getShorterForm()) == false){
            QueryWrapper<BaCargoOwner> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("shorter_form",baCargoOwner.getShorterForm());
            queryWrapper.eq("flag",0);
            BaCargoOwner selectOne = baCargoOwnerMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        //判断名称是否一样
        if(cargoOwner.getName().equals(baCargoOwner.getName()) == false){
            QueryWrapper<BaCargoOwner> queryWrapper = new QueryWrapper<>();
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name",baCargoOwner.getName());
            queryWrapper.eq("flag",0);
            BaCargoOwner selectOne = baCargoOwnerMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
        //通过编辑
        if(baCargoOwner.getState().equals(AdminCodeEnum.CARGOOWNER_STATUS_PASS.getCode())){
            //收款账户
            List<BaBank> baBankList = baCargoOwner.getBaBankList();
            if(!CollectionUtils.isEmpty(baBankList)){
                baBankList.stream().forEach(item -> {
                    if(StringUtils.isNotEmpty(item.getId())){
                        item.setUpdateTime(DateUtils.getNowDate());
                        item.setUpdateBy(SecurityUtils.getUsername());
                        baBankMapper.updateBaBank(item);
                    }else {
                        item.setId(getRedisIncreID.getId());
                        item.setCreateTime(DateUtils.getNowDate());
                        item.setCreateBy(SecurityUtils.getUsername());
                        baBankMapper.insertBaBank(item);
                    }
                });
            }
            //修改开票信息
            if(StringUtils.isNotNull(baCargoOwner.getBillingInformation())){
                BaBillingInformation billingInformation = baCargoOwner.getBillingInformation();
                if(StringUtils.isNotEmpty(billingInformation.getId())){
                    baBillingInformationMapper.updateBaBillingInformation(baCargoOwner.getBillingInformation());
                }else {
                    billingInformation.setId(getRedisIncreID.getId());
                    billingInformation.setRelevanceId(baCargoOwner.getId());
                    baBillingInformationMapper.insertBaBillingInformation(billingInformation);
                }
            }
        }
        baCargoOwner.setUpdateTime(DateUtils.getNowDate());
        int result = baCargoOwnerMapper.updateBaCargoOwner(baCargoOwner);
        if(result > 0 && !baCargoOwner.getState().equals(AdminCodeEnum.CARGOOWNER_STATUS_PASS.getCode())){
            BaCargoOwner owner = baCargoOwnerMapper.selectBaCargoOwnerById(baCargoOwner.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",owner.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(owner, AdminCodeEnum.CARGOOWNER_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baCargoOwnerMapper.updateBaCargoOwner(cargoOwner);
                return 0;
            }else {
                //收款账户
                List<BaBank> baBankList = baCargoOwner.getBaBankList();
                if(!CollectionUtils.isEmpty(baBankList)){
                    baBankList.stream().forEach(item -> {
                        if(StringUtils.isNotEmpty(item.getId())){
                            item.setUpdateTime(DateUtils.getNowDate());
                            item.setUpdateBy(SecurityUtils.getUsername());
                            baBankMapper.updateBaBank(item);
                        }else {
                            item.setId(getRedisIncreID.getId());
                            item.setCreateTime(DateUtils.getNowDate());
                            item.setCreateBy(SecurityUtils.getUsername());
                            baBankMapper.insertBaBank(item);
                        }
                    });
                }
                //修改开票信息
                if(StringUtils.isNotNull(baCargoOwner.getBillingInformation())){
                    BaBillingInformation billingInformation = baCargoOwner.getBillingInformation();
                    if(StringUtils.isNotEmpty(billingInformation.getId())){
                        baBillingInformationMapper.updateBaBillingInformation(baCargoOwner.getBillingInformation());
                    }else {
                        billingInformation.setId(getRedisIncreID.getId());
                        billingInformation.setRelevanceId(baCargoOwner.getId());
                        baBillingInformationMapper.insertBaBillingInformation(billingInformation);
                    }
                }
                owner.setState(AdminCodeEnum.CARGOOWNER_STATUS_VERIFYING.getCode());
                baCargoOwnerMapper.updateBaCargoOwner(owner);
            }
        }
        return result;
    }

    /**
     * 批量删除货主档案
     *
     * @param ids 需要删除的货主档案ID
     * @return 结果
     */
    @Override
    public int deleteBaCargoOwnerByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaCargoOwner cargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(id);
                cargoOwner.setFlag(1L);
                baCargoOwnerMapper.updateBaCargoOwner(cargoOwner);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 500;
        }
        return 200;
    }

    /**
     * 删除货主档案信息
     *
     * @param id 货主档案ID
     * @return 结果
     */
    @Override
    public int deleteBaCargoOwnerById(String id)
    {
        return baCargoOwnerMapper.deleteBaCargoOwnerById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaCargoOwner cargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",cargoOwner.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CARGOOWNER_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(cargoOwner.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                cargoOwner.setState(AdminCodeEnum.CARGOOWNER_STATUS_WITHDRAW.getCode());
                baCargoOwnerMapper.updateBaCargoOwner(cargoOwner);
                String userId = String.valueOf(cargoOwner.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(cargoOwner, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public String coding() {
        //编码
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String formatDate = sdf.format(date);
        String idCode = formatDate+"001";
        QueryWrapper<BaCargoOwner> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id_code",idCode);
        queryWrapper.eq("flag",0);
        List<BaCargoOwner> baCargoOwners = baCargoOwnerMapper.selectList(queryWrapper);
        if(baCargoOwners.size() > 0){
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("flag",0);
            queryWrapper.like("id_code",formatDate);
            queryWrapper.orderByDesc("create_time");
            List<BaCargoOwner> cargoOwners = baCargoOwnerMapper.selectList(queryWrapper);
            //编号
            String idCode1 = cargoOwners.get(0).getIdCode();
            String substring = idCode1.substring(idCode1.length() - 3);
            Long code = Long.valueOf(substring)+1;
            if (code < 10){
                return formatDate+"00"+code;
            }else if(code > 10){
                return formatDate+"0"+code;
            }else if (code > 99){
                return formatDate+code;
            }
        }
        return idCode;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCargoOwner baCargoOwner, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baCargoOwner.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CARGOOWNER.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

}
