package com.business.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.system.service.MsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;


/**
 * 发送邮件短信实现类
 *
 * @author jiahe
 * @date 2020.7
 */
@Service
public class MsServiceImpl implements MsService {
    //redis
    @Autowired
    private RedisTemplate<String, String> redisTemlate;
    //用户
    /*@Autowired
    private IXMemberService memberService;*/
    //redis自动生成主键Id
    @Autowired
    private GetRedisIncreID getRedisIncreID;


    //发送短信验证码
    @Override
    public boolean send(String phone, Map<String, Object> param, String templateCode) {
        if (StringUtils.isEmpty(phone)) return false;

        DefaultProfile profile =
                DefaultProfile.getProfile("cn-hangzhou", "LTAI5t6vud5rdp72ycv1hUgd", "GDXnZk5RAw6YbodSaHUqeSZBKx0Fhh");
        IAcsClient client = new DefaultAcsClient(profile);

        //设置相关固定的参数
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phone);
        request.putQueryParameter("SignName", "中能信链大数据科技");
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param)); //验证码数据，转换json数据传递
        try {
            //最终发送
            CommonResponse response = client.getCommonResponse(request);
            return response.getHttpResponse().isSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    //验证短信验证码
    @Override
    public Boolean checkCode(String phone, String code) {
        String key = phone+"updatePassword";
        String redisCode = redisTemlate.opsForValue().get(key);
        assert redisCode != null;
        return redisCode.equals(code);
    }

//    //验证短信验证码并修改手机号
//    @Override
//    public Boolean checkCodeUpdatePhone(String phone, String code, HttpServletRequest request) {
//        String key = "updatePhone"+phone;
//        String redisCode = redisTemlate.opsForValue().get(key);
//        assert redisCode != null;
//        boolean flag = redisCode.equals(code);
//        if (flag){
//            memberService.updatePhone(phone,request);
//        }
//        return redisCode.equals(code);
//    }

    //发送登录验证短信
//    @Override
//    public boolean sendLoginCode(String phone) {
//        //生成随机值发送给阿里云
//        String code1 = RandomUtils.getFourBitRandom();
//        String phoneCode = "8"+code1;
//        Map<String, Object> param = new HashMap<>();
//        param.put("code",phoneCode);
//        String templateCode = "SMS_196195339";
//        //调用service发送短信的方法
//        boolean isSend = send(phone,param,templateCode);
//        if(isSend){
//            String key = "login"+phone;
//            redisTemlate.opsForValue().set(key,phoneCode,15, TimeUnit.MINUTES);
//            return true;
//        }else{
//            return false;
//        }
//    }

    //发送律师入驻通知短信
//    @Override
//    public boolean sendLawyerComeInMS(String phone) {
//        return send(phone,null,"SMS_205893476");
//    }


    //验证邮箱验证码
//    @Override
//    public Boolean checkEmailCode(String email, String code) {
//        String redisCode = redisTemlate.opsForValue().get(email);
//        assert redisCode != null;
//        return redisCode.equals(code);
//    }


    //发送非验证码短信
//    @Override
//    public boolean sendNoCode(String phone, String templateCode) {
//        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4GEqw7caEpRfwRVPFihL", "fMWA1mRYrpxKwUybqWRHPMKyFcwJwf");
//        IAcsClient client = new DefaultAcsClient(profile);
//
//        CommonRequest request = new CommonRequest();
//        request.setSysMethod(MethodType.GET);
//        request.setSysDomain("dysmsapi.aliyuncs.com");
//        request.setSysVersion("2017-05-25");
//        request.setSysAction("SendSms");
//        request.putQueryParameter("RegionId", "cn-hangzhou");
//        request.putQueryParameter("PhoneNumbers", phone);
//        request.putQueryParameter("SignName", "千纳美法律服务");
//        request.putQueryParameter("TemplateCode", templateCode);
//        try {
//            CommonResponse response = client.getCommonResponse(request);
////            System.out.println(response.getData());
//            return response.getHttpResponse().isSuccess();
//        } catch (ClientException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }


}
