package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaFinancingApproval;

/**
 * 融资审批Service接口
 *
 * @author single
 * @date 2023-05-03
 */
public interface IBaFinancingApprovalService
{
    /**
     * 查询融资审批
     *
     * @param id 融资审批ID
     * @return 融资审批
     */
    public BaFinancingApproval selectBaFinancingApprovalById(String id);

    /**
     * 查询融资审批列表
     *
     * @param baFinancingApproval 融资审批
     * @return 融资审批集合
     */
    public List<BaFinancingApproval> selectBaFinancingApprovalList(BaFinancingApproval baFinancingApproval);

    /**
     * 新增融资审批
     *
     * @param baFinancingApproval 融资审批
     * @return 结果
     */
    public int insertBaFinancingApproval(BaFinancingApproval baFinancingApproval);

    /**
     * 修改融资审批
     *
     * @param baFinancingApproval 融资审批
     * @return 结果
     */
    public int updateBaFinancingApproval(BaFinancingApproval baFinancingApproval);

    /**
     * 批量删除融资审批
     *
     * @param ids 需要删除的融资审批ID
     * @return 结果
     */
    public int deleteBaFinancingApprovalByIds(String[] ids);

    /**
     * 删除融资审批信息
     *
     * @param id 融资审批ID
     * @return 结果
     */
    public int deleteBaFinancingApprovalById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


}
