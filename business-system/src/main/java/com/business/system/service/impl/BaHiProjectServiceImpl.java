package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.domain.vo.BaProjectVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 立项管理Service业务层处理
 *
 * @author ljb
 * @date 2022-12-02
 */
@Service
public class BaHiProjectServiceImpl extends ServiceImpl<BaHiProjectMapper, BaHiProject> implements IBaHiProjectService {

    private static final Logger log = LoggerFactory.getLogger(BaHiProjectServiceImpl.class);

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaHiProjectMapper baHiProjectMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private IBaEnterpriseService baEnterpriseService;

    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private IBaContractService iBaContractService;

    @Autowired
    private IBaGoodsService iBaGoodsService;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaInvoiceDetailMapper baInvoiceDetailMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaUserMailMapper baUserMailMapper;

    @Autowired
    private BaProjectLinkMapper baProjectLinkMapper;

    @Autowired
    private SysPostMapper sysPostMapper;

    @Autowired
    private IBaProjectService baProjectService;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaBidTransportMapper baBidTransportMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private BaDeclareMapper baDeclareMapper;

    @Autowired
    private ISysMenuService iSysMenuService;

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private IBaProcessInstanceService iBaProcessInstanceService;

    @Autowired
    private BaCargoOwnerMapper baCargoOwnerMapper;

    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    @Override
    public BaHiProject selectBaHiProjectById(String id) {
        BaHiProject baHiProject = baHiProjectMapper.selectBaHiProjectById(id);
        StringBuilder stringBuilder = new StringBuilder();
        //供应商
        if (StringUtils.isNotEmpty(baHiProject.getSupplierId())) {
            String[] supplierIdArray = baHiProject.getSupplierId().split(",");
            for(String supplierId : supplierIdArray){
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                if(!ObjectUtils.isEmpty(baSupplier)){
                    if(stringBuilder.length() == 0){
                        stringBuilder.append(baSupplier.getName());
                    } else {
                        stringBuilder.append(",").append(baSupplier.getName());
                    }
                }
                baHiProject.setSupplierName(stringBuilder.toString());
            }
        }
        //用煤单位
        if (StringUtils.isNotEmpty(baHiProject.getEnterpriseId())) {
            BaEnterprise baEnterprise = baEnterpriseService.selectBaEnterpriseById(baHiProject.getEnterpriseId());
            baHiProject.setEnterpriseName(baEnterprise.getName());
            BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
            if(!ObjectUtils.isEmpty(baCompanyGroup)){
                baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
            }
            //父级ID为空
//          if(StringUtils.isEmpty(baEnterprise.getParentId())){
            BaEnterprise baEnterprise1 = baEnterpriseService.selectBaEnterpriseById(baHiProject.getEnterpriseId());
            if(!ObjectUtils.isEmpty(baEnterprise1)){
            //最新指标信息
                QueryWrapper<BaEnterprise> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id",baEnterprise1.getRelationId());
                queryWrapper.eq("state",AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
                queryWrapper.orderByDesc("create_time");
                List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(queryWrapper);
                if(baEnterpriseList.size() > 0){
                    BaEnterprise enterprise = baEnterpriseService.selectBaEnterpriseById(baEnterpriseList.get(0).getId());
                    baEnterprise.setEnterpriseRelevance(enterprise.getEnterpriseRelevance());
                    baEnterprise.setId(enterprise.getId());
                }else {
                    List<BaEnterpriseRelevance> baEnterpriseRelevances = baEnterprise1.getEnterpriseRelevance();
                    baEnterprise.setEnterpriseRelevance(baEnterpriseRelevances);
                }
//            }
            }
            baHiProject.setBaEnterprise(baEnterprise);
        }
        //下游贸易商
        if(StringUtils.isNotEmpty(baHiProject.getDownstreamTraders())){
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baHiProject.getDownstreamTraders());
            baHiProject.setDownstreamTradersName(baCompany.getName());
        }
        //实际发运人
        if (StringUtils.isNotEmpty(baHiProject.getCounterpartId())) {
            BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(baHiProject.getCounterpartId());
            if (StringUtils.isNotNull(baCounterpart))
                baHiProject.setCounterpartName(baCounterpart.getName());
        }
        //仓库信息
        if(StringUtils.isNotEmpty(baHiProject.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baHiProject.getDepotId());
            if(StringUtils.isNotNull(baDepot)){
                baHiProject.setDepotName(baDepot.getName());
                baHiProject.setDepotCompany(baDepot.getCompany());
            }
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", baHiProject.getId());
        queryWrapper.eq("flag", 0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baHiProject.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = iSysUserService.selectUserById(baHiProject.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        List<SysRole> roles = sysUser.getRoles();
        if(!CollectionUtils.isEmpty(roles)){
            for(SysRole sysRole : roles){
                if("yewuzhuli".equals(sysRole.getRoleKey())){
                    baHiProject.setIsYewuzhuli("true");
                    break;
                }
            }
        }
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }

        baHiProject.setUser(sysUser);
        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baHiProject.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }

        //品名
        if("4".equals(baHiProject.getProjectType())) { //判断是否是常规立项
            if (StringUtils.isNotEmpty(baHiProject.getGoodsId())) {
                QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                if (StringUtils.isNotEmpty(baHiProject.getGoodsId())) {
                    List<String> goodsIdList = Arrays.asList(baHiProject.getGoodsId().split(","));
                    baGoodsQueryWrapper.in("id", goodsIdList);
                    baGoodsQueryWrapper.eq("flag", 0);
                    List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                    //绑定商品对象
                    baHiProject.setGoodsList(goodsList);
                    if (!CollectionUtils.isEmpty(goodsList)) {
                        StringBuilder goodName = new StringBuilder();
                        for (BaGoods baGoods : goodsList) {
                            if (goodName.length() > 0) {
                                goodName.append(",").append(baGoods.getName());
                            } else {
                                goodName.append(baGoods.getName());
                            }
                        }
                        baHiProject.setGoodsName(goodName.toString());
                    }
                }
            }
        } else {
            if (StringUtils.isNotEmpty(baHiProject.getGoodsType())) {
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baHiProject.getGoodsType());
                if (!ObjectUtils.isEmpty(baGoods)) {
                    baHiProject.setGoodsTypeName(baGoods.getName());
                    baHiProject.setGoodsName(baGoods.getName());
                } else {
                    if(StringUtils.isNotEmpty(baHiProject.getGoodsType())){
                        BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baHiProject.getGoodsType());
                        if(!ObjectUtils.isEmpty(baGoodsType)){
                            baHiProject.setGoodsTypeName(baGoodsType.getName());
                            baHiProject.setGoodsName(baGoodsType.getName());
                        }
                    }
                }
            }
        }

        //商品分类
        if (StringUtils.isNotEmpty(baHiProject.getGoodsType())) {
            BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baHiProject.getGoodsType());
            if(!ObjectUtils.isEmpty(baGoodsType)){
                baHiProject.setGoodsTypeName(baGoodsType.getName());
            }
        }

        //业务经理名称
       if(StringUtils.isNotEmpty(baHiProject.getServiceManager())){
           if(baHiProject.getServiceManager().indexOf(",") != -1){
               String[] serviceManagerArray = baHiProject.getServiceManager().split(",");
               String serviceManagerStr = serviceManagerArray[serviceManagerArray.length - 1];
               if(StringUtils.isNotEmpty(serviceManagerStr)){
                   SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(serviceManagerStr));
                  /* if(!ObjectUtils.isEmpty(sysUser1)){
                       List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(sysUser1.getUserName());
                       StringBuilder sysPostName = new StringBuilder();
                       if(!CollectionUtils.isEmpty(sysPosts)){
                           for(SysPost sysPost : sysPosts){
                               if(sysPostName.length() > 0){
                                   sysPostName.append(",").append(sysPost.getPostName());
                               } else {
                                   sysPostName.append(sysPost.getPostName());
                               }
                           }
                       }
                       if(StringUtils.isNotEmpty(sysPostName)){
                           sysUser1.setNickName(sysUser1.getNickName() + "-" + sysPostName);
                       }
                   }*/
                   baHiProject.setServiceManagerName(sysUser1.getNickName());
               }
           }
       }

        //现场人员名称
        /*StringBuilder stringBuilder = new StringBuilder();
        String personScene = baProject.getPersonScene();
        if(StringUtils.isNotEmpty(personScene)){
            String[] personSceneArray = personScene.trim().split(",");
            if(!ObjectUtils.isEmpty(personSceneArray)){
                for(String ps : personSceneArray){
                    if(stringBuilder.length() > 0){
                        stringBuilder.append(",").append(sysUserMapper.selectUserById(Long.valueOf(ps)).getNickName());
                    } else {
                        stringBuilder.append(sysUserMapper.selectUserById(Long.valueOf(ps)).getNickName());
                    }
                }
            }
        }
        baProject.setPersonSceneName(stringBuilder.toString());*/
        //运营责任人名称
        if(StringUtils.isNotEmpty(baHiProject.getProjectType())){
            if(baHiProject.getProjectType().equals("4") == false && baHiProject.getProjectType().equals("2") == false){
                if(StringUtils.isNotEmpty(baHiProject.getPersonCharge())) {
                    if(StringUtils.isNotEmpty(baHiProject.getPersonCharge())){
                        String[] personChargeArray = baHiProject.getPersonCharge().split(",");
                        String personChargeStr = personChargeArray[personChargeArray.length - 1];
                        if(StringUtils.isNotEmpty(personChargeStr)){
                            baHiProject.setPersonChargeName(sysUserMapper.selectUserById(Long.valueOf(personChargeStr)).getNickName());
                        }
                    }
                }
            }
        }
        //终端内部对接人
        if(StringUtils.isNotEmpty(baHiProject.getInternalContactPerson())){
            BaUserMail baUserMail = baUserMailMapper.selectBaUserMailById(baHiProject.getInternalContactPerson());
            baHiProject.setInternalContactPersonName(baUserMail.getName());
        }
        //下游内部对接人名称
        if(StringUtils.isNotEmpty(baHiProject.getLowInternalContactPerson())){
            BaUserMail baUser = baUserMailMapper.selectBaUserMailById(baHiProject.getLowInternalContactPerson());
            baHiProject.setLowInternalContactPersonName(baUser.getName());
        }
        //收费标准
        if(StringUtils.isNotEmpty(baHiProject.getStandardsId())){
            BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(baHiProject.getStandardsId());
            baHiProject.setStandards(baChargingStandards);
        }
        //资金计划
        QueryWrapper<BaDeclare> declareQueryWrapper = new QueryWrapper<>();
        declareQueryWrapper.eq("start_id",baHiProject.getId());
        List<BaDeclare> baDeclares = baDeclareMapper.selectList(declareQueryWrapper);
        baHiProject.setBaDeclares(baDeclares);
        //现场负责人名称
        /*if(StringUtils.isNotEmpty(baProject.getPersonSite())) {
            baProject.setPersonSiteName(sysUserMapper.selectUserById(Long.valueOf(baProject.getPersonSite())).getNickName());
        }*/
        //业务专员名称
        if (StringUtils.isNotEmpty(baHiProject.getSalesman())) {
            if (StringUtils.isNotEmpty(baHiProject.getSalesman())) {
                String salesmanStr = "";
                StringBuilder salesmanStringBuilder = new StringBuilder();
                String[] salesmanArray = baHiProject.getSalesman().split(";");
                for (String s : salesmanArray) {
                    if (StringUtils.isNotEmpty(s)) {
                        String[] salesmanSplit = s.split(",");
                        salesmanStr = salesmanSplit[salesmanSplit.length - 1];
                        if (StringUtils.isNotEmpty(salesmanStr)) {
                            SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(salesmanStr));
                            List<SysPost> sysPosts = sysPostMapper.selectPostsByUserIdAndTenantId(String.valueOf(sysUser1.getUserId()), SecurityUtils.getCurrComId());
                            StringBuilder sysPostName = new StringBuilder();
                            if (!CollectionUtils.isEmpty(sysPosts)) {
                                for (SysPost sysPost : sysPosts) {
                                    if (sysPostName.length() > 0) {
                                        sysPostName.append(",").append(sysPost.getPostName());
                                    } else {
                                        sysPostName.append(sysPost.getPostName());
                                    }
                                }
                            }
                            if (StringUtils.isNotEmpty(sysPostName)) {
                                sysUser1.setNickName(sysUser1.getNickName() + "-" + sysPostName);
                                if (salesmanStringBuilder.length() > 0) {
                                    salesmanStringBuilder.append(",").append(sysUser1.getNickName());
                                } else {
                                    salesmanStringBuilder.append(sysUser1.getNickName());
                                }
                            }
                        }
                    }
                }
                baHiProject.setSalesmanName(salesmanStringBuilder.toString());
            }
        }
        //子公司
        if(StringUtils.isNotEmpty(baHiProject.getSubsidiary())){
            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baHiProject.getSubsidiary()));
            baHiProject.setSubsidiaryName(dept.getDeptName());
            baHiProject.setSubsidiaryAbbreviation(dept.getAbbreviation());
        }
        //运营专员
        if(StringUtils.isNotEmpty(baHiProject.getPersonCharge())){
            String[] split = baHiProject.getPersonCharge().split(",");
            String userName = "";
            for (String userId:split) {
                if(this.isNumeric(userId) == true){
                    SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                    if(StringUtils.isNotNull(user)){
                        userName = user.getNickName() + "," + userName;
                    }
                }
            }
            if(StringUtils.isNotEmpty(userName) && !"".equals(userName)){
                baHiProject.setPersonChargeName(userName.substring(0,userName.length()-1));
            }else {
                baHiProject.setPersonChargeName(baHiProject.getPersonCharge());
            }
        }
        //上游公司
        if(StringUtils.isNotEmpty(baHiProject.getUpstreamCompany())){
            //查询供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baHiProject.getUpstreamCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baHiProject.setUpstreamCompanyName(baSupplier.getName());
            }
        }
        //主体公司
        if(StringUtils.isNotEmpty(baHiProject.getSubjectCompany())){
            //子公司
            SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baHiProject.getSubjectCompany()));
            if(!ObjectUtils.isEmpty(sysDept)){
                baHiProject.setSubjectCompanyName(sysDept.getDeptName());
            }
        }
        //终端公司
        if(StringUtils.isNotEmpty(baHiProject.getTerminalCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baHiProject.getTerminalCompany());
            if(StringUtils.isNotNull(enterprise)){
                baHiProject.setTerminalCompanyName(enterprise.getName());
            }
        }
        //下游公司
        if(StringUtils.isNotEmpty(baHiProject.getDownstreamCompany())){
            //下游公司名称
            String downstreamCompanyName = "";
            String[] split = baHiProject.getDownstreamCompany().split(",");
            for (String downstream:split) {
                String[] split1 = downstream.split(";");
                //供应商
                if("1".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(key);
                    downstreamCompanyName = split1[0]+";"+baSupplier.getName()+","+downstreamCompanyName;
                }
                //终端企业
                if("2".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(key);
                    downstreamCompanyName = split1[0]+";"+enterprise.getName()+","+downstreamCompanyName;
                }
                //公司
                if(!"1".equals(split1[0]) && !"2".equals(split1[0]) && !"6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(key);
                    downstreamCompanyName = split1[0]+";"+baCompany.getName()+","+downstreamCompanyName;
                }
                //集团公司
                if("6".equals(split1[0])){
                    String key = split1[split1.length - 1];
                    BaCompanyGroup companyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(key);
                    downstreamCompanyName = split1[0]+";"+companyGroup.getCompanyName()+","+downstreamCompanyName;
                }
            }
            if(",".equals(downstreamCompanyName.substring(downstreamCompanyName.length()-1))){
                baHiProject.setDownstreamCompanyName(downstreamCompanyName.substring(0,downstreamCompanyName.length()-1));
            }else {
                baHiProject.setDownstreamCompanyName(downstreamCompanyName);
            }
        }
        //货主
        if(StringUtils.isNotEmpty(baHiProject.getCustomId())){
            BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(baHiProject.getCustomId());
            if(!ObjectUtils.isEmpty(baCargoOwner)){
                baHiProject.setCustomName(baCargoOwner.getName());
            }
        }
        //合同信息
        BaContract baContract = new BaContract();
        baContract.setStorageId(id);
        baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
        baHiProject.setBaContractList(baContracts);
        return baHiProject;
    }

    //判断String是否能转数字
    public static boolean isNumeric(String str) {
        try {
            Long.valueOf(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 查询立项管理列表
     *
     * @param baHiProject 立项管理
     * @return 立项管理
     */
    @Override
    @DataScope(deptAlias = "pj", userAlias = "pj")
    public List<BaHiProject>  selectBaHiProjectList(BaHiProject baHiProject) {
        //查询业务状态为未立项
        //baProject.setProjectState(AdminCodeEnum.PROJECT_NOT_APPROVED.getCode());
        //baProject.setDeptId(SecurityUtils.getDeptId());
        List<BaHiProject> baHiProjects = baHiProjectMapper.selectHiProject(baHiProject);

        if (baHiProjects.size() > 0){
            baHiProjects.stream().forEach(item ->{
                //供应商
                StringBuilder stringBuilder = new StringBuilder();
                //供应商
                if (StringUtils.isNotEmpty(item.getSupplierId())) {
                    String[] supplierIdArray = item.getSupplierId().split(",");
                    for(String supplierId : supplierIdArray){
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(supplierId);
                        if(!ObjectUtils.isEmpty(baSupplier)){
                            if(stringBuilder.length() == 0){
                                stringBuilder.append(baSupplier.getName());
                            } else {
                                stringBuilder.append(",").append(baSupplier.getName());
                            }
                        }
                        item.setSupplierName(stringBuilder.toString());
                    }
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baHiProject.getSupplierId());
                    item.setSupplierName(baSupplier.getName());
                }
                //用煤单位
                if (StringUtils.isNotEmpty(item.getEnterpriseId())) {
                    BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getEnterpriseId());
                    item.setEnterpriseName(baEnterprise.getName());
                    BaCompanyGroup baCompanyGroup = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                    if(!ObjectUtils.isEmpty(baCompanyGroup)){
                        baEnterprise.setCompanyName(baCompanyGroup.getCompanyName());
                        baEnterprise.setBaCompanyGroup(baCompanyGroup);
                    }
                    item.setBaEnterprise(baEnterprise);
                    item.setSpliceName(baEnterprise.getName());
                }
                //实际发起人
                if (StringUtils.isNotEmpty(item.getCounterpartId())) {
                    BaCounterpart baCounterpart = baCounterpartMapper.selectBaCounterpartById(item.getCounterpartId());
                    item.setCounterpartName(baCounterpart.getName());
                    item.setSpliceName(baCounterpart.getName());
                }
                //发起人
                SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());


                //拼接归属部门
                if (sysUser.getDeptId() != null) {
                    SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
                    //查询祖籍
                    String[] split = dept.getAncestors().split(",");
                    List<Long> integerList = new ArrayList<>();
                    for (String deptId:split) {
                        integerList.add(Long.valueOf(deptId));
                    }
                    //倒叙拿到第一个公司
                    Collections.reverse(integerList);
                    for (Long itm:integerList) {
                        SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                        if(StringUtils.isNotNull(dept1)){
                            if(dept1.getDeptType().equals("1")){
                                sysUser.setPartDeptCompany(dept1.getDeptName());
                                break;
                            }
                        }
                    }
                    sysUser.setPartDeptName(sysUser.getPartDeptCompany()+"-"+dept.getDeptName());
                }
                item.setUser(sysUser);

                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUser.getNickName());
                }
                //仓库信息
                if(StringUtils.isNotEmpty(item.getDepotId())){
                    BaDepot baDepot = baDepotMapper.selectBaDepotById(item.getDepotId());
                    if(StringUtils.isNotNull(baDepot)){
                        item.setDepotName(baDepot.getName());
                    }
                }

                //商品信息
                /*BaGoods baGoods = baGoodsMapper.selectBaGoodsById(item.getGoodsId());
                if (!ObjectUtils.isEmpty(baGoods)) {
                    item.setGoodsName(baGoods.getName());
                }*/
                if("4".equals(item.getProjectType())) { //判断是否是常规立项
                    if (StringUtils.isNotEmpty(item.getGoodsId())) {
                        QueryWrapper<BaGoods> baGoodsQueryWrapper = new QueryWrapper<>();
                        if(StringUtils.isNotEmpty(item.getGoodsId())){
                            List<String> goodsIdList = Arrays.asList(item.getGoodsId().split(","));
                            baGoodsQueryWrapper.in("id", goodsIdList);
                            baGoodsQueryWrapper.eq("flag", 0);
                            List<BaGoods> goodsList = baGoodsMapper.selectList(baGoodsQueryWrapper);
                            //绑定商品对象
                            item.setGoodsList(goodsList);
                            if(!CollectionUtils.isEmpty(goodsList)){
                                StringBuilder goodName = new StringBuilder();
                                for(BaGoods baGoods : goodsList){
                                    if(goodName.length() > 0){
                                        goodName.append(",").append(baGoods.getName());
                                    } else {
                                        goodName.append(baGoods.getName());
                                    }
                                }
                                item.setGoodsName(goodName.toString());
                            }
                        }
                    }
                } else {
                    if (StringUtils.isNotEmpty(item.getGoodsType())) {
                        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(item.getGoodsType());
                        if (!ObjectUtils.isEmpty(baGoods)) {
                            item.setGoodsName(baGoods.getName());
                        } else {
                            if(StringUtils.isNotEmpty(item.getGoodsType())){
                                BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(item.getGoodsType());
                                if(!ObjectUtils.isEmpty(baGoodsType)){
                                    item.setGoodsName(baGoodsType.getName());
                                }
                            }
                        }
                    }
                }
                //收费标准
                if(StringUtils.isNotEmpty(item.getStandardsId())){
                    BaChargingStandards baChargingStandards = baChargingStandardsMapper.selectBaChargingStandardsById(item.getStandardsId());
                    item.setStandards(baChargingStandards);
                }
                //子公司
                if(StringUtils.isNotEmpty(item.getSubsidiary())){
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubsidiary()));
                    item.setSubsidiaryName(dept.getDeptName());
                    item.setSubsidiaryAbbreviation(dept.getAbbreviation());
                }
                List<String> processInstanceIds = new ArrayList<>();
                //流程实例ID
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", item.getId());
                queryWrapper.eq("flag", 0);
                //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
                queryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                }
                item.setProcessInstanceId(processInstanceIds);

                //主体公司
                if(StringUtils.isNotEmpty(item.getSubjectCompany())){
                    //子公司
                    SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(item.getSubjectCompany()));
                    if(!ObjectUtils.isEmpty(sysDept)){
                        item.setSubjectCompanyName(sysDept.getDeptName());
                    }
                }
                //货主
                if(StringUtils.isNotEmpty(item.getCustomId())){
                    BaCargoOwner baCargoOwner = baCargoOwnerMapper.selectBaCargoOwnerById(item.getCustomId());
                    if(!ObjectUtils.isEmpty(baCargoOwner)){
                        item.setCustomName(baCargoOwner.getName());
                    }
                }
            });
        }

        return baHiProjects;
    }

    @Override
    public List<BaHiProject> selectChangeDataBaHiProjectList(BaHiProject baHiProject) {
        //查询变更记录
        baHiProject.setTenantId(SecurityUtils.getCurrComId());
        List<BaHiProject> baHiProjects = baHiProjectMapper.selectChangeBaHiProjectList(baHiProject);
        if(!CollectionUtils.isEmpty(baHiProjects)){
            baHiProjects.stream().forEach(item -> {
                SysUser sysUser = sysUserMapper.selectUserByUserName(item.getChangeBy());
                if(!ObjectUtils.isEmpty(sysUser)){
                    //岗位名称
                    String name = getName.getName(sysUser.getUserId());
                    if(StringUtils.isNotEmpty(name)){
                        sysUser.setPostName(name.substring(0,name.length()-1));
                    }
                    item.setChangeUser(sysUser);
                }
            });
        }
        return baHiProjects;
    }

}
