package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.BaShippingOrderCmstMapper;
import com.business.system.mapper.BaTransportAutomobileMapper;
import com.business.system.mapper.BaTransportMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaTransportCmstMapper;
import com.business.system.service.IBaTransportCmstService;

/**
 * 中储智运Service业务层处理
 *
 * @author single
 * @date 2023-08-21
 */
@Service
public class BaTransportCmstServiceImpl extends ServiceImpl<BaTransportCmstMapper, BaTransportCmst> implements IBaTransportCmstService
{
    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;



    /**
     * 查询中储智运
     *
     * @param id 中储智运ID
     * @return 中储智运
     */
    @Override
    public BaTransportCmst selectBaTransportCmstById(String id)
    {
        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(id);
        //运单信息
        BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
        baShippingOrderCmst1.setYardId(transportCmst.getYardld());
        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
        if(baShippingOrderCmsts != null && baShippingOrderCmsts.size() > 0){
            //货主名称
            transportCmst.setConsignorUserName(baShippingOrderCmsts.get(0).getConsignorUserName());
        }
        transportCmst.setBaShippingOrderCmsts(baShippingOrderCmsts);
        return transportCmst;
    }

    /**
     * 查询中储智运列表
     *
     * @param baTransportCmst 中储智运
     * @return 中储智运
     */
    @Override
    public List<BaTransportCmst> selectBaTransportCmstList(BaTransportCmst baTransportCmst)
    {
        List<BaTransportCmst> baTransportCmsts = baTransportCmstMapper.selectBaTransportCmstList(baTransportCmst);
        for (BaTransportCmst transportCmst:baTransportCmsts) {
            BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
            baShippingOrderCmst1.setYardId(transportCmst.getYardld());
            List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
            //车数
            transportCmst.setCarNum(baShippingOrderCmsts.size());
            //实际发运量
            BigDecimal saleNum = new BigDecimal(0);
            for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                if(baShippingOrderCmst.getTonnage() != null){
                    saleNum = saleNum.add(baShippingOrderCmst.getTonnage());
                }
            }
            baTransportCmst.setSaleNum(saleNum);
        }
        return baTransportCmsts;
    }

    @Override
    public List<BaTransportCmst> baTransportCmstList(BaTransportCmst baTransportCmst) {
        List<BaTransportCmst> baTransportCmsts = baTransportCmstMapper.baTransportCmstList(baTransportCmst);
        for (BaTransportCmst transportCmst:baTransportCmsts) {
            BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
            baShippingOrderCmst1.setYardId(transportCmst.getYardld());
            List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
            //车数
            transportCmst.setCarNum(baShippingOrderCmsts.size());
            //实际发运量
            BigDecimal saleNum = new BigDecimal(0);
            for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                if(baShippingOrderCmst.getTonnage() != null){
                    saleNum = saleNum.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                }
            }
            transportCmst.setSaleNum(saleNum);
            if(baShippingOrderCmsts != null && baShippingOrderCmsts.size() > 0){
                //货主名称
                transportCmst.setConsignorUserName(baShippingOrderCmsts.get(0).getConsignorUserName());
            }
        }
        return baTransportCmsts;
    }

    /**
     * 新增中储智运
     *
     * @param baTransportCmst 中储智运
     * @return 结果
     */
    @Override
    public int insertBaTransportCmst(BaTransportCmst baTransportCmst)
    {
        baTransportCmst.setCreateTime(DateUtils.getNowDate());
        return baTransportCmstMapper.insertBaTransportCmst(baTransportCmst);
    }

    /**
     * 修改中储智运
     *
     * @param baTransportCmst 中储智运
     * @return 结果
     */
    @Override
    public int updateBaTransportCmst(BaTransportCmst baTransportCmst)
    {
        baTransportCmst.setUpdateTime(DateUtils.getNowDate());
        return baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
    }

    /**
     * 批量删除中储智运
     *
     * @param ids 需要删除的中储智运ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportCmstByIds(String[] ids)
    {
        return baTransportCmstMapper.deleteBaTransportCmstByIds(ids);
    }

    /**
     * 删除中储智运信息
     *
     * @param id 中储智运ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportCmstById(String id)
    {
        return baTransportCmstMapper.deleteBaTransportCmstById(id);
    }

    @Override
    public int transport(BaTransport baTransport) {
        //修改运输信息发货状态
        baTransport.setConfirmStatus("2");
        baTransport.setTransportStatus("1");
        if(StringUtils.isNotEmpty(baTransport.getAutomobileType())){
            //中储货源绑定运输单
            if(baTransport.getAutomobileType().equals("2")){
                if(StringUtils.isNotEmpty(baTransport.getAutomobileId())){
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(baTransport.getAutomobileId());
                    transportCmst.setRelationId(baTransport.getId());
                    baTransportCmstMapper.updateBaTransportCmst(transportCmst);

                }
            }
            //昕科货源绑定运输单
            if(baTransport.getAutomobileType().equals("1")){
                if(StringUtils.isNotEmpty(baTransport.getAutomobileId())){
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransport.getAutomobileId());
                    baTransportAutomobile.setOrderId(baTransport.getId());
                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                }
            }
        }
        return baTransportMapper.updateBaTransport(baTransport);
    }

}
