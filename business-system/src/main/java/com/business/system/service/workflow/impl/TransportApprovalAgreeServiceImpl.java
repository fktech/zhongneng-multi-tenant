package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaBid;
import com.business.system.domain.BaTransport;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.mapper.BaBidMapper;
import com.business.system.mapper.BaTransportMapper;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IBidApprovalService;
import com.business.system.service.workflow.ITransportApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 运输申请审批结果:审批通过
 */
@Service("transportApprovalContent:processApproveResult:pass")
@Slf4j
public class TransportApprovalAgreeServiceImpl extends ITransportApprovalService implements IWorkflowUpdateStatusService {
    @Autowired
    private BaTransportMapper baTransportMapper;

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaTransport baTransport = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode());
        return result;
    }
}
