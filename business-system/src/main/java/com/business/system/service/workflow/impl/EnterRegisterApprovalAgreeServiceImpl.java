package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaOther;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IEnterRegisterApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 企业注册申请审批结果:审批通过
 */
@Service("enterRegisterApprovalContent:processApproveResult:pass")
@Slf4j
public class EnterRegisterApprovalAgreeServiceImpl extends IEnterRegisterApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        SysCompany sysCompany = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ENTERREGISTER_STATUS_PASS.getCode());
        return result;
    }
}
