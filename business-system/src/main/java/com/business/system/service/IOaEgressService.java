package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaEgress;

/**
 * 外出申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaEgressService
{
    /**
     * 查询外出申请
     *
     * @param id 外出申请ID
     * @return 外出申请
     */
    public OaEgress selectOaEgressById(String id);

    /**
     * 查询外出申请列表
     *
     * @param oaEgress 外出申请
     * @return 外出申请集合
     */
    public List<OaEgress> selectOaEgressList(OaEgress oaEgress);

    /**
     * 新增外出申请
     *
     * @param oaEgress 外出申请
     * @return 结果
     */
    public int insertOaEgress(OaEgress oaEgress);

    /**
     * 修改外出申请
     *
     * @param oaEgress 外出申请
     * @return 结果
     */
    public int updateOaEgress(OaEgress oaEgress);

    /**
     * 批量删除外出申请
     *
     * @param ids 需要删除的外出申请ID
     * @return 结果
     */
    public int deleteOaEgressByIds(String[] ids);

    /**
     * 删除外出申请信息
     *
     * @param id 外出申请ID
     * @return 结果
     */
    public int deleteOaEgressById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);




}
