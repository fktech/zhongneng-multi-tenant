package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 业务维护/拓展Service业务层处理
 *
 * @author ljb
 * @date 2023-11-13
 */
@Service
public class OaMaintenanceExpansionServiceImpl extends ServiceImpl<OaMaintenanceExpansionMapper, OaMaintenanceExpansion> implements IOaMaintenanceExpansionService
{
    private static final Logger log = LoggerFactory.getLogger(OaMaintenanceExpansionServiceImpl.class);
    @Autowired
    private OaMaintenanceExpansionMapper oaMaintenanceExpansionMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaCarMapper baCarMapper;

    /**
     * 查询业务维护/拓展
     *
     * @param id 业务维护/拓展ID
     * @return 业务维护/拓展
     */
    @Override
    public OaMaintenanceExpansion selectOaMaintenanceExpansionById(String id)
    {
        OaMaintenanceExpansion oaMaintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",oaMaintenanceExpansion.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        oaMaintenanceExpansion.setProcessInstanceId(processInstanceIds);
        //申请人名称
        if(oaMaintenanceExpansion.getUserId() != null){
            oaMaintenanceExpansion.setUserName(sysUserMapper.selectUserById(oaMaintenanceExpansion.getUserId()).getNickName());
        }
        //申请人部门
        if(oaMaintenanceExpansion.getDeptId() != null){
            oaMaintenanceExpansion.setDeptName(sysDeptMapper.selectDeptById(oaMaintenanceExpansion.getDeptId()).getDeptName());
        }
        return oaMaintenanceExpansion;
    }

    /**
     * 查询业务维护/拓展列表
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 业务维护/拓展
     */
    @Override
    public List<OaMaintenanceExpansion> selectOaMaintenanceExpansionList(OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        List<OaMaintenanceExpansion> oaMaintenanceExpansions = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionList(oaMaintenanceExpansion);
        if(!CollectionUtils.isEmpty(oaMaintenanceExpansions)){
            for(OaMaintenanceExpansion maintenanceExpansion : oaMaintenanceExpansions){
                List<String> processInstanceIds = new ArrayList<>();
                //流程实例ID
                QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
                sealQueryWrapper.eq("business_id",maintenanceExpansion.getId());
                sealQueryWrapper.eq("flag",0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                sealQueryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
                }
                maintenanceExpansion.setProcessInstanceId(processInstanceIds);

                //申请人名称
                if(maintenanceExpansion.getUserId() != null){
                    maintenanceExpansion.setUserName(sysUserMapper.selectUserById(maintenanceExpansion.getUserId()).getNickName());
                }
                //申请人部门
                if(maintenanceExpansion.getDeptId() != null){
                    maintenanceExpansion.setDeptName(sysDeptMapper.selectDeptById(maintenanceExpansion.getDeptId()).getDeptName());
                }
                maintenanceExpansion.setBusinessType("taskType:oaMaintenanceExpansion");
            }
        }
        return oaMaintenanceExpansions;
    }

    /**
     * 新增业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    @Override
    public int insertOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        //判断是否为保存数据
        if(StringUtils.isNotEmpty(oaMaintenanceExpansion.getId()) && "0".equals(oaMaintenanceExpansion.getSubmitState())){
            return oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(oaMaintenanceExpansion);
        }
        //判断保存数据提交
        if(StringUtils.isNotEmpty(oaMaintenanceExpansion.getId()) && "1".equals(oaMaintenanceExpansion.getSubmitState())){
            OaMaintenanceExpansion maintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(oaMaintenanceExpansion.getId());
            maintenanceExpansion.setFlag(1L);
            oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(maintenanceExpansion);
        }
        oaMaintenanceExpansion.setId(getRedisIncreID.getId());
        oaMaintenanceExpansion.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaMaintenanceExpansion.setTenantId(SecurityUtils.getCurrComId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode(),SecurityUtils.getCurrComId());
        oaMaintenanceExpansion.setFlowId(flowId);
        int result = oaMaintenanceExpansionMapper.insertOaMaintenanceExpansion(oaMaintenanceExpansion);
        if(result > 0 && "1".equals(oaMaintenanceExpansion.getSubmitState())){
            OaMaintenanceExpansion maintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(oaMaintenanceExpansion.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(maintenanceExpansion, AdminCodeEnum.OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaMaintenanceExpansionMapper.deleteOaMaintenanceExpansionById(maintenanceExpansion.getId());
                return 0;
            }else {
                maintenanceExpansion.setState(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_VERIFYING.getCode());
                oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(maintenanceExpansion);
            }
        }
        return result;
    }

    /**
     * 提交请假申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaMaintenanceExpansion maintenanceExpansion, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(maintenanceExpansion.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode(), AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(maintenanceExpansion.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        OaMaintenanceExpansion oaMaintenanceExpansion = this.selectOaMaintenanceExpansionById(maintenanceExpansion.getId());
        SysUser sysUser = iSysUserService.selectUserById(maintenanceExpansion.getUserId());
        oaMaintenanceExpansion.setUserName(sysUser.getNickName());
        oaMaintenanceExpansion.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaMaintenanceExpansion, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    @Override
    public int updateOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        //原数据
        OaMaintenanceExpansion oaMaintenanceExpansion1 = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(oaMaintenanceExpansion.getId());

        oaMaintenanceExpansion.setUpdateTime(DateUtils.getNowDate());
        int result = oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(oaMaintenanceExpansion);
        if(result > 0 && "1".equals(oaMaintenanceExpansion.getSubmitState())){
            OaMaintenanceExpansion maintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(oaMaintenanceExpansion.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", maintenanceExpansion.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(maintenanceExpansion, AdminCodeEnum.OAMAINTENANCEEXPANSION_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(oaMaintenanceExpansion1);
                return 0;
            }else {
                maintenanceExpansion.setState(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_VERIFYING.getCode());
                oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(maintenanceExpansion);
            }
        }
        return result;
    }

    /**
     * 批量删除业务维护/拓展
     *
     * @param ids 需要删除的业务维护/拓展ID
     * @return 结果
     */
    @Override
    public int deleteOaMaintenanceExpansionByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaMaintenanceExpansion maintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(id);
                maintenanceExpansion.setFlag(1L);
                oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(maintenanceExpansion);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除业务维护/拓展信息
     *
     * @param id 业务维护/拓展ID
     * @return 结果
     */
    @Override
    public int deleteOaMaintenanceExpansionById(String id)
    {
        return oaMaintenanceExpansionMapper.deleteOaMaintenanceExpansionById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaMaintenanceExpansion maintenanceExpansion = oaMaintenanceExpansionMapper.selectOaMaintenanceExpansionById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",maintenanceExpansion.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(maintenanceExpansion.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                maintenanceExpansion.setState(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_WITHDRAW.getCode());
                oaMaintenanceExpansionMapper.updateOaMaintenanceExpansion(maintenanceExpansion);
                String userId = String.valueOf(maintenanceExpansion.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(maintenanceExpansion, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaMaintenanceExpansion oaMaintenanceExpansion, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaMaintenanceExpansion.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_MAINTENANCEEXPANSION.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"业务拓展维护审批提醒",msgContent,"oa",oaMaintenanceExpansion.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 审批办理保存编辑业务数据
     * @param oaMaintenanceExpansionInstanceRelatedEditDTO
     * @return
     */
    public int updateProcessBusinessData(OaMaintenanceExpansionInstanceRelatedEditDTO oaMaintenanceExpansionInstanceRelatedEditDTO) {
        OaMaintenanceExpansion oaMaintenanceExpansion = oaMaintenanceExpansionInstanceRelatedEditDTO.getOaMaintenanceExpansion();
        oaMaintenanceExpansion.setUpdateTime(DateUtils.getNowDate());
        oaMaintenanceExpansion.setUpdateBy(SecurityUtils.getUsername());
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        baProcessInstanceRelated.setProcessInstanceId(oaMaintenanceExpansionInstanceRelatedEditDTO.getProcessInstanceId());
        baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(oaMaintenanceExpansion));
        try {
            baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
        } catch (Exception e) {
            return 0;
        }
        return 1;
    }

    @Override
    public List<UserMaintenanceExpansionDTO> submitUser(OaMaintenanceExpansion oaMaintenanceExpansion) {
        List<UserMaintenanceExpansionDTO> userMaintenanceExpansionDTOArrayList = new ArrayList<>();

        List<String> list = oaMaintenanceExpansionMapper.submitUser(oaMaintenanceExpansion);

        for (String userId:list) {
            UserMaintenanceExpansionDTO userMaintenanceExpansionDTO = new UserMaintenanceExpansionDTO();
            //总公里数
            BigDecimal realityKilometersTotal = new BigDecimal(0);
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
            //用户ID
            userMaintenanceExpansionDTO.setUserId(user.getUserId().toString());
            //用户名称
            userMaintenanceExpansionDTO.setUserName(user.getNickName());
            //用户头像
            userMaintenanceExpansionDTO.setAvatar(user.getAvatar());
            //部门
            if(user.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                userMaintenanceExpansionDTO.setDeptName(dept.getDeptName());
            }
            //查询
            QueryWrapper<OaMaintenanceExpansion> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id",user.getUserId());
            queryWrapper.eq("flag",0);
            queryWrapper.eq("state","oaMaintenanceExpansion:pass");
            if(oaMaintenanceExpansion.getApplicationTime() != null){
                //日期格式化
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
                queryWrapper.apply("DATE_FORMAT(application_time,'%Y-%m') = '"+format.format(oaMaintenanceExpansion.getApplicationTime())+"'");
            }
            List<OaMaintenanceExpansion> oaMaintenanceExpansions = oaMaintenanceExpansionMapper.selectList(queryWrapper);
            for (OaMaintenanceExpansion maintenanceExpansion:oaMaintenanceExpansions) {
                if(maintenanceExpansion.getRealityKilometers() != null){
                    realityKilometersTotal = realityKilometersTotal.add(maintenanceExpansion.getRealityKilometers());
                }
            }
            userMaintenanceExpansionDTO.setRealityKilometersTotal(realityKilometersTotal);
            userMaintenanceExpansionDTO.setMaintenanceExpansionList(oaMaintenanceExpansions);
            userMaintenanceExpansionDTOArrayList.add(userMaintenanceExpansionDTO);
        }
        return userMaintenanceExpansionDTOArrayList;
    }

}
