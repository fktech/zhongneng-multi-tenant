package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaAutomobileSettlementDetail;
import com.business.system.domain.BaShippingOrder;
import com.business.system.mapper.BaAutomobileSettlementDetailMapper;
import com.business.system.mapper.BaShippingOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAutomobileSettlementMapper;
import com.business.system.domain.BaAutomobileSettlement;
import com.business.system.service.IBaAutomobileSettlementService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 汽运结算Service业务层处理
 *
 * @author single
 * @date 2023-02-17
 */
@Service
public class BaAutomobileSettlementServiceImpl extends ServiceImpl<BaAutomobileSettlementMapper, BaAutomobileSettlement> implements IBaAutomobileSettlementService
{
    @Autowired
    private BaAutomobileSettlementMapper baAutomobileSettlementMapper;

    @Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    @Override
    public BaAutomobileSettlement selectBaAutomobileSettlementById(String id)
    {
        BaAutomobileSettlement baAutomobileSettlement = baAutomobileSettlementMapper.selectBaAutomobileSettlementById(id);
        //付款明细
        BaAutomobileSettlementDetail baAutomobileSettlementDetail = new BaAutomobileSettlementDetail();
        baAutomobileSettlementDetail.setPaymentOrderNum(baAutomobileSettlement.getPaymentOrderNum());
        List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.automobileSettlementDetailList(baAutomobileSettlementDetail);
        baAutomobileSettlement.setBaAutomobileSettlementDetail(baAutomobileSettlementDetails);

        return baAutomobileSettlement;
    }

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 汽运结算
     */
    @Override
    public List<BaAutomobileSettlement> selectBaAutomobileSettlementList(BaAutomobileSettlement baAutomobileSettlement)
    {
        List<BaAutomobileSettlement> baAutomobileSettlements = baAutomobileSettlementMapper.selectBaAutomobileSettlementList(baAutomobileSettlement);
        for (BaAutomobileSettlement baAutomobileSettlement1:baAutomobileSettlements) {
            QueryWrapper<BaAutomobileSettlementDetail> detailQueryWrapper = new QueryWrapper<>();
            detailQueryWrapper.eq("payment_order_num",baAutomobileSettlement1.getPaymentOrderNum());
            List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(detailQueryWrapper);
            baAutomobileSettlement1.setWaybill(baAutomobileSettlementDetails.size());
        }
        return baAutomobileSettlements;
    }

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement)
    {
        baAutomobileSettlement.setCreateTime(DateUtils.getNowDate());
        return baAutomobileSettlementMapper.insertBaAutomobileSettlement(baAutomobileSettlement);
    }

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlement 汽运结算
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlement(BaAutomobileSettlement baAutomobileSettlement)
    {
        baAutomobileSettlement.setUpdateTime(DateUtils.getNowDate());
        return baAutomobileSettlementMapper.updateBaAutomobileSettlement(baAutomobileSettlement);
    }

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaAutomobileSettlement baAutomobileSettlement = baAutomobileSettlementMapper.selectBaAutomobileSettlementByPaymentOrderNum(id);
                baAutomobileSettlement.setFlag(1L);
                baAutomobileSettlementMapper.updateBaAutomobileSettlement(baAutomobileSettlement);
            }
        }else {
        return 0;
       }
        return 1;
    }

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementById(String id)
    {
        return baAutomobileSettlementMapper.deleteBaAutomobileSettlementById(id);
    }

    /**
     * 昕科推送汽运结算（付款单）
     */
    @Override
    @Transactional
    public int addAutomobileSettlement(JSONObject object) {
        BaAutomobileSettlement baAutomobileSettlement = new BaAutomobileSettlement();
        baAutomobileSettlement.setId(getRedisIncreID.getId());
        //单据编号（付款单号）
        baAutomobileSettlement.setPaymentOrderNum(object.getString("paymentOrderNum"));
        //单据名称
        baAutomobileSettlement.setPaymentOrderName(object.getString("paymentOrderName"));
        //归属货主（公司）
        baAutomobileSettlement.setCompanyName(object.getString("companyName"));
        //金额总计
        baAutomobileSettlement.setFreightAmount(object.getBigDecimal("freightAmount"));
        //付款单审核状态（0审批中 1审核通过 2审核拒绝 ）
        baAutomobileSettlement.setCheckFlag(Long.valueOf(object.getInteger("checkFlag")));
        //申请时间
        baAutomobileSettlement.setApplyTime(object.getDate("applyTime"));
        //新增时间
        baAutomobileSettlement.setCreateTime(DateUtils.getNowDate());
        //获取结算详情本地环境
//        String url = "https://4942h442k4.yicp.fun/js/js/PaymentOrderDetail";
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/js/js/PaymentOrderDetail";
        Map<String, String> map = new HashMap<>();
        map.put("paymentOrderNum", baAutomobileSettlement.getPaymentOrderNum());
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(map));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        if(obj.getString("code").equals("200")){
            //获取data对象集合
            JSONArray data = obj.getJSONArray("data");
            for (Object userObj : data) {
                BaAutomobileSettlementDetail baAutomobileSettlementDetail = JSONObject.toJavaObject((JSONObject) JSON.toJSON(userObj), BaAutomobileSettlementDetail.class);
                baAutomobileSettlementDetail.setPaymentOrderNum(baAutomobileSettlement.getPaymentOrderNum());
                baAutomobileSettlementDetail.setId(getRedisIncreID.getId());
                baAutomobileSettlementDetail.setState("1");
                baAutomobileSettlementDetail.setCreateTime(DateUtils.getNowDate());
                baAutomobileSettlementDetailMapper.insertBaAutomobileSettlementDetail(baAutomobileSettlementDetail);
            }
        }else {
            return 0;
        }

        return baAutomobileSettlementMapper.insert(baAutomobileSettlement);
    }

    @Override
    public int selectSettlement(String paymentOrderNum) {
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/js/js/PaymentOrderDetail";
        Map<String, String> map = new HashMap<>();
        map.put("paymentOrderNum", paymentOrderNum);
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(map));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        if(obj.getString("code").equals("200")){
            //获取data对象集合
            JSONArray data = obj.getJSONArray("data");
            for (Object userObj : data) {
                BaAutomobileSettlementDetail baAutomobileSettlementDetail = JSONObject.toJavaObject((JSONObject) JSON.toJSON(userObj), BaAutomobileSettlementDetail.class);
                baAutomobileSettlementDetail.setPaymentOrderNum(paymentOrderNum);
                baAutomobileSettlementDetail.setId(getRedisIncreID.getId());
                baAutomobileSettlementDetail.setState("1");
                baAutomobileSettlementDetail.setCreateTime(DateUtils.getNowDate());
                baAutomobileSettlementDetailMapper.insertBaAutomobileSettlementDetail(baAutomobileSettlementDetail);
            }
            return 1;
        }
        return 0;
    }

}
