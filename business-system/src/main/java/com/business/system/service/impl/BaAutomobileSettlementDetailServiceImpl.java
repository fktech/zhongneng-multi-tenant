package com.business.system.service.impl;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAutomobileSettlementDetailMapper;
import com.business.system.domain.BaAutomobileSettlementDetail;
import com.business.system.service.IBaAutomobileSettlementDetailService;

/**
 * 付款明细Service业务层处理
 *
 * @author ljb
 * @date 2023-04-12
 */
@Service
public class BaAutomobileSettlementDetailServiceImpl extends ServiceImpl<BaAutomobileSettlementDetailMapper, BaAutomobileSettlementDetail> implements IBaAutomobileSettlementDetailService
{
    @Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;
    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询付款明细
     *
     * @param id 付款明细ID
     * @return 付款明细
     */
    @Override
    public BaAutomobileSettlementDetail selectBaAutomobileSettlementDetailById(String id)
    {
        return baAutomobileSettlementDetailMapper.selectBaAutomobileSettlementDetailById(id);
    }

    /**
     * 查询付款明细列表
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 付款明细
     */
    @Override
    public List<BaAutomobileSettlementDetail> selectBaAutomobileSettlementDetailList(BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        return baAutomobileSettlementDetailMapper.automobileSettlementDetailList(baAutomobileSettlementDetail);
    }

    /**
     * 新增付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        return baAutomobileSettlementDetailMapper.insertBaAutomobileSettlementDetail(baAutomobileSettlementDetail);
    }

    /**
     * 修改付款明细
     *
     * @param baAutomobileSettlementDetail 付款明细
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlementDetail(BaAutomobileSettlementDetail baAutomobileSettlementDetail)
    {
        return baAutomobileSettlementDetailMapper.updateBaAutomobileSettlementDetail(baAutomobileSettlementDetail);
    }

    /**
     * 批量删除付款明细
     *
     * @param ids 需要删除的付款明细ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementDetailByIds(String[] ids)
    {
        return baAutomobileSettlementDetailMapper.deleteBaAutomobileSettlementDetailByIds(ids);
    }

    /**
     * 删除付款明细信息
     *
     * @param id 付款明细ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementDetailById(String id)
    {
        return baAutomobileSettlementDetailMapper.deleteBaAutomobileSettlementDetailById(id);
    }

    @Override
    public int updateDetail(JSONObject object) {
        QueryWrapper<BaAutomobileSettlementDetail> detailQueryWrapper = new QueryWrapper<>();
        detailQueryWrapper.eq("external_order",object.getString("thdno"));
        BaAutomobileSettlementDetail baAutomobileSettlementDetail = baAutomobileSettlementDetailMapper.selectOne(detailQueryWrapper);
        baAutomobileSettlementDetail.setRemark(object.getString("payFailReturn"));
        baAutomobileSettlementDetail.setState(object.getInteger("settlementStatus").toString());
        baAutomobileSettlementDetail.setJsTime(object.getDate("jsTime"));
        int result = baAutomobileSettlementDetailMapper.updateBaAutomobileSettlementDetail(baAutomobileSettlementDetail);
        return result;
    }

}
