package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaFormwork;

/**
 * 模板Service接口
 *
 * @author ljb
 * @date 2023-03-14
 */
public interface IBaFormworkService
{
    /**
     * 查询模板
     *
     * @param id 模板ID
     * @return 模板
     */
    public BaFormwork selectBaFormworkById(String id);

    /**
     * 查询模板列表
     *
     * @param baFormwork 模板
     * @return 模板集合
     */
    public List<BaFormwork> selectBaFormworkList(BaFormwork baFormwork);

    /**
     * 新增模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    public int insertBaFormwork(BaFormwork baFormwork);

    /**
     * 修改模板
     *
     * @param baFormwork 模板
     * @return 结果
     */
    public int updateBaFormwork(BaFormwork baFormwork);

    /**
     * 批量删除模板
     *
     * @param ids 需要删除的模板ID
     * @return 结果
     */
    public int deleteBaFormworkByIds(String[] ids);

    /**
     * 删除模板信息
     *
     * @param id 模板ID
     * @return 结果
     */
    public int deleteBaFormworkById(String id);


}
