package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.OaContractSeal;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 企业注册申请审批接口 服务类
 * @date: 2024/5/27 16:38
 */
@Service
public class IContractSealApprovalService {

    @Autowired
    protected OaContractSealMapper oaContractSealMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaContractSeal oaContractSeal = new OaContractSeal();
        oaContractSeal.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaContractSeal = oaContractSealMapper.selectOaContractSealById(oaContractSeal.getId());
        oaContractSeal.setState(status);
        String userId = String.valueOf(oaContractSeal.getUserId());
        if(AdminCodeEnum.CONTRACTSEAL_STATUS_PASS.getCode().equals(status)){
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            OaContractSeal contractSeal = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), OaContractSeal.class);

            //申请人名称
            SysUser sysUser = sysUserMapper.selectUserById(contractSeal.getUserId());

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            contractSeal.setState(status);
            oaContractSealMapper.updateById(contractSeal);
            //插入消息通知信息
            this.insertMessage(oaContractSeal, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.CONTRACTSEAL_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaContractSeal, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaContractSeal, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            OaContractSeal contractSeal = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), OaContractSeal.class);
            contractSeal.setState(AdminCodeEnum.CONTRACTSEAL_STATUS_REJECT.getCode());
            oaContractSealMapper.updateById(oaContractSeal);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaContractSeal oaContractSeal, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaContractSeal.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CONTRACTSEAL.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"oa合同用印审批提醒",msgContent,baMessage.getType(),oaContractSeal.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaContractSeal checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaContractSeal oaContractSeal = oaContractSealMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaContractSeal == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_ENTERREGISTER_001);
        }
        if (!AdminCodeEnum.CONTRACTSEAL_STATUS_VERIFYING.getCode().equals(oaContractSeal.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_ENTERREGISTER_002);
        }
        return oaContractSeal;
    }
}
