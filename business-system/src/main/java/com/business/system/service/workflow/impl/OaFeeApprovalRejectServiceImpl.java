package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaFee;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEgressApprovalService;
import com.business.system.service.workflow.IOaFeeApprovalService;
import org.springframework.stereotype.Service;


/**
 * 费用申请审批结果:审批拒绝
 */
@Service("oaFeeApprovalContent:processApproveResult:reject")
public class OaFeeApprovalRejectServiceImpl extends IOaFeeApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAFEE_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaFee checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
