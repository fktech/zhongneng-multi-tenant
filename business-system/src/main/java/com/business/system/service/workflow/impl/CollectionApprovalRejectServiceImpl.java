package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaCollection;
import com.business.system.domain.BaPayment;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.ICollectionApprovalService;
import com.business.system.service.workflow.IPaymentApprovalService;
import org.springframework.stereotype.Service;


/**
 * 收款申请审批结果:审批拒绝
 */
@Service("collectionApprovalContent:processApproveResult:reject")
public class CollectionApprovalRejectServiceImpl extends ICollectionApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.COLLECTION_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaCollection checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}