package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaProgressInformation;
import com.business.system.domain.BaProject;
import com.business.system.domain.BaProjectBeforehand;
import com.business.system.domain.dto.BaProjectInstanceRelatedEditDTO;
import com.business.system.domain.dto.CountBaProjectDTO;

/**
 * 预立项Service接口
 *
 * @author ljb
 * @date 2023-09-20
 */
public interface IBaProjectBeforehandService
{
    /**
     * 查询预立项
     *
     * @param id 预立项ID
     * @return 预立项
     */
    public BaProjectBeforehand selectBaProjectBeforehandById(String id);

    /**
     * 查询预立项列表
     *
     * @param baProjectBeforehand 预立项
     * @return 预立项集合
     */
    public List<BaProjectBeforehand> selectBaProjectBeforehandList(BaProjectBeforehand baProjectBeforehand);

    /**
     * 新增预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    public int insertBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    /**
     * 修改预立项
     *
     * @param baProjectBeforehand 预立项
     * @return 结果
     */
    public int updateBaProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    public int updateProjectBeforehand(BaProjectBeforehand baProjectBeforehand);

    /**
     * 批量删除预立项
     *
     * @param ids 需要删除的预立项ID
     * @return 结果
     */
    public int deleteBaProjectBeforehandByIds(String[] ids);

    /**
     * 删除预立项信息
     *
     * @param id 预立项ID
     * @return 结果
     */
    public int deleteBaProjectBeforehandById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 立项序号
     * @return
     */
    String projectSerial();

    /**
     * 审批办理保存编辑业务数据
     * @param baProjectInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(BaProjectInstanceRelatedEditDTO baProjectInstanceRelatedEditDTO);

    /**
     * APP业务首页项目立项
     */
    public UR appHomeProjectBeforehandStat(BaProjectBeforehand baProjectBeforehand);

    /**
     * 新增进度信息
     *
     * @param baProgressInformation 进度信息
     * @return 结果
     */
    public int insertBaProgressInformation(BaProgressInformation baProgressInformation);




}
