package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.ClockCountDTO;
import com.business.system.mapper.SysUserMapper;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAttendanceMapper;
import com.business.system.domain.BaAttendance;
import com.business.system.service.IBaAttendanceService;

/**
 * 打卡Service业务层处理
 *
 * @author single
 * @date 2023-10-11
 */
@Service
public class BaAttendanceServiceImpl extends ServiceImpl<BaAttendanceMapper, BaAttendance> implements IBaAttendanceService
{
    @Autowired
    private BaAttendanceMapper baAttendanceMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PostName getName;

    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    @Override
    public BaAttendance selectBaAttendanceById(String id)
    {
        return baAttendanceMapper.selectBaAttendanceById(id);
    }

    /**
     * 查询打卡列表
     *
     * @param baAttendance 打卡
     * @return 打卡
     */
    @Override
    public List<BaAttendance> selectBaAttendanceList(BaAttendance baAttendance)
    {
        List<BaAttendance> baAttendances = baAttendanceMapper.selectBaAttendanceList(baAttendance);
        baAttendances.stream().forEach(itm ->{
            //标记人
            SysUser user = sysUserMapper.selectUserById(itm.getEmployeeId());
            //打卡人
            itm.setEmployeeName(user.getNickName());
            //岗位信息
            String name = getName.getName(itm.getEmployeeId());
            //打卡人
            if (name.equals("") == false) {
                itm.setPostName(name);
            }
        });
        return baAttendances;
    }

    /**
     * 新增打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    @Override
    public int insertBaAttendance(BaAttendance baAttendance)
    {
        baAttendance.setId(getRedisIncreID.getId());
        baAttendance.setUserId(SecurityUtils.getUserId());
        baAttendance.setDeptId(SecurityUtils.getDeptId());
        baAttendance.setCreateTime(DateUtils.getNowDate());
        return baAttendanceMapper.insertBaAttendance(baAttendance);
    }

    /**
     * 修改打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    @Override
    public int updateBaAttendance(BaAttendance baAttendance)
    {
        baAttendance.setUpdateTime(DateUtils.getNowDate());
        return baAttendanceMapper.updateBaAttendance(baAttendance);
    }

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的打卡ID
     * @return 结果
     */
    @Override
    public int deleteBaAttendanceByIds(String[] ids)
    {
        return baAttendanceMapper.deleteBaAttendanceByIds(ids);
    }

    /**
     * 删除打卡信息
     *
     * @param id 打卡ID
     * @return 结果
     */
    @Override
    public int deleteBaAttendanceById(String id)
    {
        return baAttendanceMapper.deleteBaAttendanceById(id);
    }

    @Override
    public ClockCountDTO clockCount(BaAttendance baAttendance) {
        //打卡统计对象
        ClockCountDTO clockCountDTO = new ClockCountDTO();
        //查看所有二类人
        //List<String> list = sysUserMapper.selectUserIdByAssort("2");
        SysUser user = new SysUser();
        user.setHumanoid("2");
        if(baAttendance.getDeptId() != null){
            user.setDeptId(baAttendance.getDeptId());
        }
        List<SysUser> sysUsers = sysUserMapper.userList(user);
        //应出勤人数
        clockCountDTO.setPeopleTotal((long)sysUsers.size());
        //查看打卡记录
        //baAttendance.setCheckType("1");
        QueryWrapper<BaAttendance> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("flag",0);
        List<BaAttendance> baAttendances1 = baAttendanceMapper.selectList(queryWrapper);
        //打卡数据去重
        ArrayList<BaAttendance> collect = baAttendances1.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaAttendance::getEmployeeId))), ArrayList::new));
        //实际打卡人数
        clockCountDTO.setPeopleReality((long)collect.size());
        //迟到
        int beLate = 0;
        //早退
        int leaveEarly = 0;
        //缺卡第一种情况（未打卡）应出勤人数减去实际出勤人数
        int lackCards = Integer.valueOf(clockCountDTO.getPeopleTotal().toString()) - Integer.valueOf(clockCountDTO.getPeopleReality().toString());;
        List<BaAttendance> baAttendances = baAttendanceMapper.selectBaAttendanceList(baAttendance);
        for (BaAttendance attendance:baAttendances) {
            //上班打卡情况
            if("1".equals(attendance.getCheckType())){
                if("2".equals(attendance.getCheckState())){
                    beLate = beLate+1;
                }
            }
            //下班打卡情况
            if("2".equals(attendance.getCheckType())){
                if("3".equals(attendance.getCheckState())){
                    leaveEarly = leaveEarly +1;
                }
            }
        }
        //迟到人数
        clockCountDTO.setBeLate((long)beLate);
        //早退人数
        clockCountDTO.setLeaveEarly((long)leaveEarly);

        return clockCountDTO;
    }

}
