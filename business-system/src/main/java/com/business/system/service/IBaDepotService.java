package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaDepot;
import com.business.system.domain.dto.DepotDTO;

/**
 * 仓库信息Service接口
 *
 * @author ljb
 * @date 2022-11-30
 */
public interface IBaDepotService
{
    /**
     * 查询仓库信息
     *
     * @param id 仓库信息ID
     * @return 仓库信息
     */
    public BaDepot selectBaDepotById(String id);

    /**
     * 查询仓库信息列表
     *
     * @param baDepot 仓库信息
     * @return 仓库信息集合
     */
    public List<BaDepot> selectBaDepotList(BaDepot baDepot);

    /**
     * app查询仓库统计数据
     *
     *
     */
    public DepotDTO selectBaDepotAppList(BaDepot baDepot);

    /**
     * 新增仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    public int insertBaDepot(BaDepot baDepot);

    /**
     * 修改仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    public int updateBaDepot(BaDepot baDepot);

    /**
     * 批量删除仓库信息
     *
     * @param ids 需要删除的仓库信息ID
     * @return 结果
     */
    public int deleteBaDepotByIds(String[] ids);

    /**
     * 删除仓库信息信息
     *
     * @param id 仓库信息ID
     * @return 结果
     */
    public int deleteBaDepotById(String id);

    /**
     * 撤销
     */
    public int revoke(String id);

    /**
     * 修改仓库信息不启动流程
     */
    int editNoProcess(BaDepot baDepot);

    /**
     * 判断仓位是否可删除
     */
    public int deleteJudge(String locationId);
}
