package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaGoodsType;
import com.business.system.domain.vo.MaterialTypeVo;

/**
 * 商品分类Service接口
 *
 * @author ljb
 * @date 2022-11-30
 */
public interface IBaGoodsTypeService
{
    /**
     * 查询商品分类
     *
     * @param id 商品分类ID
     * @return 商品分类
     */
    public BaGoodsType selectBaGoodsTypeById(String id);

    /**
     * 查询商品分类列表
     *
     * @param baGoodsType 商品分类
     * @return 商品分类集合
     */
    public List<BaGoodsType> selectBaGoodsTypeList(BaGoodsType baGoodsType);

    /**
     * 查询下拉框中的type数据
     */
    List<MaterialTypeVo> selectBaGoodsTypeTypeList();

    /**
     * 新增商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    public int insertBaGoodsType(BaGoodsType baGoodsType);

    /**
     * 修改商品分类
     *
     * @param baGoodsType 商品分类
     * @return 结果
     */
    public int updateBaGoodsType(BaGoodsType baGoodsType);

    /**
     * 批量删除商品分类
     *
     * @param ids 需要删除的商品分类ID
     * @return 结果
     */
    public int deleteBaGoodsTypeByIds(String[] ids);

    /**
     * 删除商品分类信息
     *
     * @param id 商品分类ID
     * @return 结果
     */
    public int deleteBaGoodsTypeById(String id);

    /**
     * 查询下拉框中的type数据及商品数据
     */
    List<MaterialTypeVo> selectTypeAndGoodsList();
}
