package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaShippingOrderCmst;

/**
 * 中储智运发运单Service接口
 *
 * @author single
 * @date 2023-08-21
 */
public interface IBaShippingOrderCmstService
{
    /**
     * 查询中储智运发运单
     *
     * @param id 中储智运发运单ID
     * @return 中储智运发运单
     */
    public BaShippingOrderCmst selectBaShippingOrderCmstById(String id);

    /**
     * 查询中储智运发运单列表
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 中储智运发运单集合
     */
    public List<BaShippingOrderCmst> selectBaShippingOrderCmstList(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 新增中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    public int insertBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 修改中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    public int updateBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst);

    /**
     * 修改中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    public int updateShippingOrderCmst(JSONObject baShippingOrderCmst);

    /**
     * 批量删除中储智运发运单
     *
     * @param ids 需要删除的中储智运发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderCmstByIds(String[] ids);

    /**
     * 删除中储智运发运单信息
     *
     * @param id 中储智运发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderCmstById(String id);

    /**
     * 中储回单确认通知
     */
    int receiptShippingOrder(JSONObject object);
}
