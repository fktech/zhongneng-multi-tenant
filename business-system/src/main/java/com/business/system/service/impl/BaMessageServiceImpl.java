package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.*;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.mapper.BaCompanyMapper;
import com.business.system.mapper.BaEnterpriseMapper;
import com.business.system.mapper.BaProcessInstanceRelatedMapper;
import com.business.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.service.IBaMessageService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 系统消息记录Service业务层处理
 *
 * @author single
 * @date 2023-01-10
 */
@Service
public class BaMessageServiceImpl extends ServiceImpl<BaMessageMapper, BaMessage> implements IBaMessageService
{
    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    /**
     * 查询系统消息记录
     *
     * @param msId 系统消息记录ID
     * @return 系统消息记录
     */
    @Override
    public BaMessage selectBaMessageById(String msId)
    {
        BaMessage baMessage = baMessageMapper.selectBaMessageById(msId);

        return baMessage;
    }

    /**
     * 查询系统消息记录列表
     *
     * @param baMessage 系统消息记录
     * @return 系统消息记录
     */
    @Override
    public List<BaMessage> selectBaMessageList(BaMessage baMessage)
    {
        baMessage.setMId(SecurityUtils.getUserId().toString());
        List<BaMessage> baMessages = baMessageMapper.selectBaMessageList(baMessage);
        for (BaMessage message:baMessages) {
            //查询流程实例
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",message.getBusinessId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(!ObjectUtils.isEmpty(baProcessInstanceRelateds)){
                //区分客商管理
                if(AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode().equals(baProcessInstanceRelateds.get(0).getBusinessType())){
                    message.setCompanyType(baProcessInstanceRelateds.get(0).getApproveType().split(":")[0]);
                }
            }
            if(StringUtils.isEmpty(message.getCompanyType())){
                BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(message.getBusinessId());
                if(!ObjectUtils.isEmpty(baEnterprise)){
                    message.setCompanyType(baEnterprise.getState());
                }
            }
            //公司类型
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(message.getBusinessId());
            if(StringUtils.isNotNull(baCompany)){
                message.setCompanyType(baProcessInstanceRelateds.get(0).getApproveType().split(":")[0]);
                message.setCorpType(baCompany.getCompanyType());
            }
            //用户信息
            SysUser user = sysUserService.selectUserById(Long.valueOf(message.getMId()));
            message.setUser(user);
        }
        return baMessages;
    }

    /**
     * 新增系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    @Override
    public int insertBaMessage(BaMessage baMessage)
    {
        baMessage.setMId(getRedisIncreID.getId());
        baMessage.setCreateTime(DateUtils.getNowDate());
        return baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 修改系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    @Override
    public int updateBaMessage(BaMessage baMessage)
    {
        baMessage.setUpdateTime(DateUtils.getNowDate());
        return baMessageMapper.updateBaMessage(baMessage);
    }

    /**
     * 批量删除系统消息记录
     *
     * @param msIds 需要删除的系统消息记录ID
     * @return 结果
     */
    @Override
    public int deleteBaMessageByIds(String[] msIds)
    {
        if(StringUtils.isNotEmpty(msIds)){
            for (String id:msIds) {
                BaMessage baMessage = baMessageMapper.selectBaMessageById(id);
                baMessage.setFlag(1L);
                baMessageMapper.updateBaMessage(baMessage);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除系统消息记录信息
     *
     * @param msId 系统消息记录ID
     * @return 结果
     */
    @Override
    public int deleteBaMessageById(String msId)
    {
        return baMessageMapper.deleteBaMessageById(msId);
    }

    @Override
    public int empty() {
        QueryWrapper<BaMessage> messageQueryWrapper = new QueryWrapper<>();
        messageQueryWrapper.eq("m_id",String.valueOf(SecurityUtils.getUserId()));
        messageQueryWrapper.eq("states","1");
        List<BaMessage> baMessageList = baMessageMapper.selectList(messageQueryWrapper);
        for (BaMessage baMessage:baMessageList) {
            BaMessage message = baMessageMapper.selectBaMessageById(baMessage.getMsId());
            message.setFlag((long) 1);
            baMessageMapper.updateBaMessage(message);
        }
        return 1;
    }

    @Override
    public int read() {
            QueryWrapper<BaMessage> messageQueryWrapper = new QueryWrapper<>();
            messageQueryWrapper.eq("m_id", String.valueOf(SecurityUtils.getUserId()));
            messageQueryWrapper.eq("states","2");
            List<BaMessage> baMessageList = baMessageMapper.selectList(messageQueryWrapper);
            for (BaMessage baMessage:baMessageList) {
                baMessage.setStates("1");
                baMessageMapper.updateBaMessage(baMessage);
            }
        return 1;
    }

    @Override
    public int batchRead(String[] msIds) {
        if(StringUtils.isNotEmpty(msIds)){
            for (String id:msIds) {
                BaMessage baMessage = baMessageMapper.selectBaMessageById(id);
                baMessage.setStates("1");
                baMessageMapper.updateBaMessage(baMessage);
            }
        }else {
            return -1;
        }
        return 1;
    }

    @Override
    public BaMessage selectBaMessage() {
        QueryWrapper<BaMessage> messageQueryWrapper = new QueryWrapper<>();
        messageQueryWrapper.eq("m_id",String.valueOf(SecurityUtils.getUserId()));
        messageQueryWrapper.eq("states","2");
        messageQueryWrapper.eq("box_state",0);
        messageQueryWrapper.eq("type","1");
        messageQueryWrapper.orderByDesc("create_time");
        List<BaMessage> baMessages = baMessageMapper.selectList(messageQueryWrapper);
        for (BaMessage message:baMessages) {
            //查询流程实例
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",message.getBusinessId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(baProcessInstanceRelateds) && AdminCodeEnum.TASK_TYPE_PRODUCT_TRADER.getCode().equals(baProcessInstanceRelateds.get(0).getBusinessType())){
                message.setCompanyType(baProcessInstanceRelateds.get(0).getApproveType().split(":")[0]);
            }
            //公司类型
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(message.getBusinessId());
            if(StringUtils.isNotNull(baCompany)){
                message.setCorpType(baCompany.getCompanyType());
            }
        }
        BaMessage baMessage = new BaMessage();

        if(baMessages.size() > 0){
            //取最新数据放入新对象
            BeanUtils.copyBeanProp(baMessage,baMessages.get(0));
        }else {
            return null;
        }

        return baMessage;
    }

    @Override
    public int phoneCode(BaMessage baMessage) {
        List<BaMessage> baMessages = baMessageMapper.selectBaMessageList(baMessage);
        if(baMessages.size() > 0){
            for (BaMessage message:baMessages) {
                if(null != baMessage.getEliminate()){
                    if(baMessage.getEliminate().equals("1")){
                        message.setPhoneCode("");
                    }
                }else {
                    message.setPhoneCode(baMessage.getPhoneCode());
                }
                baMessageMapper.updateBaMessage(message);
            }
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public List<BaMessage> selectPhoneCode(String phoneCode) {
        return baMessageMapper.selectPhoneCode(phoneCode);
    }

    @Override
    public String messagePush(String cid,String title,String content,String type, String id,String businessType) {
        String url = "https://fc-mp-8693c044-d244-44b2-9507-010fb42b18b2.next.bspapp.com/requsetPush/pushApi";
        //Map<String, String> map = new HashMap<>();
        JSONObject paramMap = new JSONObject();
        Map<String, String> map = new HashMap<>();
        map.put("type",type);
        map.put("id",id);
        map.put("businessType",businessType);
        paramMap.put("cid", cid);
        paramMap.put("title", title);
        paramMap.put("content", content);
        paramMap.put("payload",map);
        String postMethod = restTemplateUtil.postMethod(url, JSONObject.toJSONString(paramMap));
        Object parse = JSONObject.parse(postMethod);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        String result = obj.getString("errMsg");
        return result;
    }
}
