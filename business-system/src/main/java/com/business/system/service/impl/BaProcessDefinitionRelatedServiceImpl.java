package com.business.system.service.impl;

import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.exception.ServiceException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaProcessDefinitionRelated;
import com.business.system.domain.dto.BaProcessDefinitionRelatedDeleteDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedQueryDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedSaveDTO;
import com.business.system.domain.vo.BaProcessDefinitionRelatedVO;
import com.business.system.mapper.BaProcessDefinitionRelatedMapper;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程定义关联关系接口
 * @date: 2022/12/1 17:46
 */
@Service
public class BaProcessDefinitionRelatedServiceImpl implements IBaProcessDefinitionRelatedService {

    @Autowired
    private BaProcessDefinitionRelatedMapper baProcessDefinitionRelatedMapper;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 按条件查询业务与流程定义关联关系列表
     *
     * @param businessType 业务类型
     * @return 业务与流程定义关联关系集合
     */
    @Override
    public List<BaProcessDefinitionRelated> listProcessBusinessDefinitionByType(String businessType, String tenantId) {
        return baProcessDefinitionRelatedMapper.listProcessBusinessDefinitionByType(businessType, tenantId);
    }

    @Override
    public List<BaProcessDefinitionRelatedVO> listProcessDefinitionPage(BaProcessDefinitionRelatedQueryDTO queryDTO){
        BaProcessDefinitionRelated queryDO = beanUtil.beanConvert(queryDTO,BaProcessDefinitionRelated.class);
        List<BaProcessDefinitionRelated> relatedDOList = baProcessDefinitionRelatedMapper.listProcessDefinitionPage(queryDO);
        return beanUtil.listConvert(relatedDOList, BaProcessDefinitionRelatedVO.class);
    }

    @Override
    public int saveProcessDefinition(BaProcessDefinitionRelatedSaveDTO saveDTO) {
        saveDTO.setTenantId(SecurityUtils.getCurrComId());
        checkSaveInfo(saveDTO);
        List<BaProcessDefinitionRelated> relatedDOS = baProcessDefinitionRelatedMapper.listProcessBusinessDefinitionByType(saveDTO.getBusinessType(), saveDTO.getTenantId());
        BaProcessDefinitionRelated baProcessDefinitionRelated = null;
        if(!CollectionUtils.isEmpty(relatedDOS)){
            baProcessDefinitionRelated = relatedDOS.get(0);
            baProcessDefinitionRelatedMapper.deleteById(baProcessDefinitionRelated.getId());
        }
        BaProcessDefinitionRelated relatedDO = beanUtil.beanConvert(saveDTO,BaProcessDefinitionRelated.class);
        relatedDO.setId(getRedisIncreID.getId());
        relatedDO.setProcessKey("Flowable" + relatedDO.getTemplateId());
        relatedDO.setCreateBy(String.valueOf(SecurityUtils.getUsername()));
        relatedDO.setUpdateBy(String.valueOf(SecurityUtils.getUsername()));
        relatedDO.setCreateTime(DateUtils.getNowDate());
        relatedDO.setUpdateTime(DateUtils.getNowDate());
        relatedDO.setTenantId(saveDTO.getTenantId());
        return baProcessDefinitionRelatedMapper.insert(relatedDO);
    }

    @Override
    public int deleteProcessDefinition(BaProcessDefinitionRelatedDeleteDTO deleteDTO) {
        return baProcessDefinitionRelatedMapper.deleteProcessDefinition(deleteDTO.getIds());
    }

    @Override
    public int deleteProcessDefinitionById(String id) {
        return baProcessDefinitionRelatedMapper.deleteProcessDefinitionById(id);
    }

    @Override
    public String getFlowId(String businessType, String tenantId) {
        List<BaProcessDefinitionRelated> relatedDOS = baProcessDefinitionRelatedMapper.listProcessBusinessDefinitionByType(businessType, tenantId);
        if(CollectionUtils.isEmpty(relatedDOS)){
            throw new ServiceException("系统异常，不存在审批流程！");
        }
        return relatedDOS.get(0).getProcessKey();
    }

    @Override
    public BaProcessDefinitionRelatedVO getProcessDefinitionRelated(String templateId) {
        BaProcessDefinitionRelated baProcessDefinitionRelated = baProcessDefinitionRelatedMapper.selectBaProcessDefinitionRelated(templateId, SecurityUtils.getCurrComId());
        return beanUtil.beanConvert(baProcessDefinitionRelated, BaProcessDefinitionRelatedVO.class);
    }

    /**
     * 新增校验：同类型下只能存在一个审批流程
     * @param saveDTO
     */
    private void checkSaveInfo(BaProcessDefinitionRelatedSaveDTO saveDTO){
        List<BaProcessDefinitionRelated> relatedDOS = baProcessDefinitionRelatedMapper.listProcessBusinessDefinitionByType(saveDTO.getBusinessType(), saveDTO.getTenantId());
        if(!CollectionUtils.isEmpty(relatedDOS)){
            throw new ServiceException("该流程类型已存在一条审批流程！");
        }
    }
}
