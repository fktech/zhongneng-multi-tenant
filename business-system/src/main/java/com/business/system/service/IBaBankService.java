package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaBank;

/**
 * 开户行Service接口
 *
 * @author ljb
 * @date 2022-12-05
 */
public interface IBaBankService
{
    /**
     * 查询开户行
     *
     * @param id 开户行ID
     * @return 开户行
     */
    public BaBank selectBaBankById(String id);

    /**
     * 查询开户行列表
     *
     * @param baBank 开户行
     * @return 开户行集合
     */
    public List<BaBank> selectBaBankList(BaBank baBank);

    /**
     * 新增开户行
     *
     * @param baBank 开户行
     * @return 结果
     */
    public int insertBaBank(BaBank baBank);

    /**
     * 修改开户行
     *
     * @param baBank 开户行
     * @return 结果
     */
    public int updateBaBank(BaBank baBank);

    /**
     * 批量删除开户行
     *
     * @param ids 需要删除的开户行ID
     * @return 结果
     */
    public int deleteBaBankByIds(String[] ids);

    /**
     * 删除开户行信息
     *
     * @param id 开户行ID
     * @return 结果
     */
    public int deleteBaBankById(String id);


}
