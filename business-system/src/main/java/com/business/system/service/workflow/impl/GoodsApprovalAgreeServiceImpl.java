package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaGoods;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IDeclareApprovalService;
import com.business.system.service.workflow.IGoodsApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商品审批结果:审批通过
 */
@Service("goodsApprovalContent:processApproveResult:pass")
@Slf4j
public class GoodsApprovalAgreeServiceImpl extends IGoodsApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaGoods baGoods = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.GOODS_STATUS_PASS.getCode());
        return result;
    }
}
