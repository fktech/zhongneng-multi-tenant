package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaMaintenanceExpansion;
import com.business.system.domain.dto.BaProjectInstanceRelatedEditDTO;
import com.business.system.domain.dto.OaMaintenanceExpansionInstanceRelatedEditDTO;
import com.business.system.domain.dto.UserMaintenanceExpansionDTO;

/**
 * 业务维护/拓展Service接口
 *
 * @author ljb
 * @date 2023-11-13
 */
public interface IOaMaintenanceExpansionService
{
    /**
     * 查询业务维护/拓展
     *
     * @param id 业务维护/拓展ID
     * @return 业务维护/拓展
     */
    public OaMaintenanceExpansion selectOaMaintenanceExpansionById(String id);

    /**
     * 查询业务维护/拓展列表
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 业务维护/拓展集合
     */
    public List<OaMaintenanceExpansion> selectOaMaintenanceExpansionList(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 新增业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    public int insertOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 修改业务维护/拓展
     *
     * @param oaMaintenanceExpansion 业务维护/拓展
     * @return 结果
     */
    public int updateOaMaintenanceExpansion(OaMaintenanceExpansion oaMaintenanceExpansion);

    /**
     * 批量删除业务维护/拓展
     *
     * @param ids 需要删除的业务维护/拓展ID
     * @return 结果
     */
    public int deleteOaMaintenanceExpansionByIds(String[] ids);

    /**
     * 删除业务维护/拓展信息
     *
     * @param id 业务维护/拓展ID
     * @return 结果
     */
    public int deleteOaMaintenanceExpansionById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 审批办理保存编辑业务数据
     * @param oaMaintenanceExpansionInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(OaMaintenanceExpansionInstanceRelatedEditDTO oaMaintenanceExpansionInstanceRelatedEditDTO);

    /**
     * 获取所有提交人员
     */
     public List<UserMaintenanceExpansionDTO> submitUser(OaMaintenanceExpansion oaMaintenanceExpansion);

}
