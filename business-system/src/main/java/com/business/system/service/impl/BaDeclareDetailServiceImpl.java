package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaDeclareDetail;
import com.business.system.mapper.BaDeclareDetailMapper;
import com.business.system.service.IBaDeclareDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 计划申报Service业务层处理
 *
 * @author single
 * @date 2023-03-21
 */
@Service
public class BaDeclareDetailServiceImpl extends ServiceImpl<BaDeclareDetailMapper, BaDeclareDetail> implements IBaDeclareDetailService
{
    @Autowired
    private BaDeclareDetailMapper baDeclareDetailMapper;

    /**
     * 查询计划申报
     *
     * @param id 计划申报ID
     * @return 计划申报
     */
    @Override
    public BaDeclareDetail selectBaDeclareDetailById(String id)
    {
        return baDeclareDetailMapper.selectBaDeclareDetailById(id);
    }

    /**
     * 查询计划申报列表
     *
     * @param baDeclareDetail 计划申报
     * @return 计划申报
     */
    @Override
    public List<BaDeclareDetail> selectBaDeclareDetailList(BaDeclareDetail baDeclareDetail)
    {
        return baDeclareDetailMapper.selectBaDeclareDetailList(baDeclareDetail);
    }

    /**
     * 新增计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    @Override
    public int insertBaDeclareDetail(BaDeclareDetail baDeclareDetail)
    {
        baDeclareDetail.setCreateTime(DateUtils.getNowDate());
        return baDeclareDetailMapper.insertBaDeclareDetail(baDeclareDetail);
    }

    /**
     * 修改计划申报
     *
     * @param baDeclareDetail 计划申报
     * @return 结果
     */
    @Override
    public int updateBaDeclareDetail(BaDeclareDetail baDeclareDetail)
    {
        baDeclareDetail.setUpdateTime(DateUtils.getNowDate());
        return baDeclareDetailMapper.updateBaDeclareDetail(baDeclareDetail);
    }

    /**
     * 批量删除计划申报
     *
     * @param ids 需要删除的计划申报ID
     * @return 结果
     */
    @Override
    public int deleteBaDeclareDetailByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                if(StringUtils.isNotEmpty(id)){
                    baDeclareDetailMapper.deleteBaDeclareDetailById(id);
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除计划申报信息
     *
     * @param id 计划申报ID
     * @return 结果
     */
    @Override
    public int deleteBaDeclareDetailById(String id)
    {
        return baDeclareDetailMapper.deleteBaDeclareDetailById(id);
    }

}
