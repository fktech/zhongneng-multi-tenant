package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaCoalSourceStorage;

/**
 * 煤源库Service接口
 *
 * @author single
 * @date 2024-01-25
 */
public interface IBaCoalSourceStorageService
{
    /**
     * 查询煤源库
     *
     * @param id 煤源库ID
     * @return 煤源库
     */
    public BaCoalSourceStorage selectBaCoalSourceStorageById(String id);

    /**
     * 查询煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库集合
     */
    public List<BaCoalSourceStorage> selectBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 新增煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    public int insertBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 修改煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    public int updateBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage);

    /**
     * 批量删除煤源库
     *
     * @param ids 需要删除的煤源库ID
     * @return 结果
     */
    public int deleteBaCoalSourceStorageByIds(String[] ids);

    /**
     * 删除煤源库信息
     *
     * @param id 煤源库ID
     * @return 结果
     */
    public int deleteBaCoalSourceStorageById(String id);

    /**
     * 统计煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库集合
     */
    public List<BaCoalSourceStorage> countBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage);


}
