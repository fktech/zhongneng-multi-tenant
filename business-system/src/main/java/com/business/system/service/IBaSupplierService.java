package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.BaSupplierInstanceRelatedEditDTO;
import com.business.system.domain.dto.DropDownBox;

/**
 * 供应商Service接口
 *
 * @author ljb
 * @date 2022-11-28
 */
public interface IBaSupplierService
{
    /**
     * 查询供应商
     *
     * @param id 供应商ID
     * @return 供应商
     */
    public BaSupplier selectBaSupplierById(String id);

    /**
     * 查询供应商列表
     *
     * @param baSupplier 供应商
     * @return 供应商集合
     */
    public List<BaSupplier> selectBaSupplierList(BaSupplier baSupplier);

    /**
     * 新增供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    public int insertBaSupplier(BaSupplier baSupplier);

    /**
     * 修改供应商
     *
     * @param baSupplier 供应商
     * @return 结果
     */
    public int updateBaSupplier(BaSupplier baSupplier);

    /**
     * 修改账户信息
     */
    public int updateSupplier(BaSupplier baSupplier);

    /**
     * 批量删除供应商
     *
     * @param ids 需要删除的供应商ID
     * @return 结果
     */
    public int deleteBaSupplierByIds(String[] ids);

    /**
     * 删除供应商信息
     *
     * @param id 供应商ID
     * @return 结果
     */
    public int deleteBaSupplierById(String[] id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


    /**
     * 审批办理保存编辑业务数据
     * @param baSupplierInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(BaSupplierInstanceRelatedEditDTO baSupplierInstanceRelatedEditDTO);

    /**
     * 供应商下拉框
     * @return
     */
    public List<BaSupplier> baSupplierList();

    /**
     * 供应商下拉组合（供应商、装卸服务公司）
     */
    public List<DropDownBox> dropDownBox(String name);

    /**
     * 分类统计客商管理信息
     * @return
     */
    UR statTraderInfo();
}
