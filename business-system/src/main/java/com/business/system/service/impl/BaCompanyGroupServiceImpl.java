package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.UR;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.BaEnterpriseDTO;
import com.business.system.domain.dto.TerminalDTO;
import com.business.system.mapper.*;
import com.business.system.service.IBaShippingOrderCmstService;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaCompanyGroupService;
import org.springframework.util.ObjectUtils;

/**
 * 集团公司Service业务层处理
 *
 * @author ljb
 * @date 2023-03-01
 */
@Service
public class BaCompanyGroupServiceImpl extends ServiceImpl<BaCompanyGroupMapper, BaCompanyGroup> implements IBaCompanyGroupService
{
    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private IBaShippingOrderCmstService baShippingOrderCmstService;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询集团公司
     *
     * @param id 集团公司ID
     * @return 集团公司
     */
    @Override
    public BaCompanyGroup selectBaCompanyGroupById(String id)
    {
        return baCompanyGroupMapper.selectBaCompanyGroupById(id);
    }

    /**
     * 查询集团公司列表
     *
     * @param baCompanyGroup 集团公司
     * @return 集团公司
     */
    @Override
    public List<BaCompanyGroup> selectBaCompanyGroupList(BaCompanyGroup baCompanyGroup)
    {
        List<BaCompanyGroup> baCompanyGroups = baCompanyGroupMapper.selectBaCompanyGroupList(baCompanyGroup);
        for (BaCompanyGroup baCompanyGroup1:baCompanyGroups) {
            //岗位信息
            String name = getName.getName(baCompanyGroup1.getUserId());
            //发起人
            if (name.equals("") == false) {
                baCompanyGroup1.setUserName(sysUserMapper.selectUserById(baCompanyGroup1.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                baCompanyGroup1.setUserName(sysUserMapper.selectUserById(baCompanyGroup1.getUserId()).getNickName());
            }
        }
        return baCompanyGroups;
    }

    @Override
    public List<BaCompanyGroup> subsidiaryList(String depId) {
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("type","2");
        List<BaCompanyGroup> baCompanyGroups = baCompanyGroupMapper.selectList(groupQueryWrapper);
        for (BaCompanyGroup companyGroup:baCompanyGroups) {
            //查看子公司下所有合同启动
            QueryWrapper<BaContractStart> contractStartQueryWrapper = new QueryWrapper<>();
            contractStartQueryWrapper.eq("state", AdminCodeEnum.CONTRACTSTART_STATUS_PASS.getCode());
            if(StringUtils.isNotEmpty(depId)){
                contractStartQueryWrapper.eq("dept_id",Long.valueOf(depId));
            }
            contractStartQueryWrapper.eq("flag",0);
            List<BaContractStart> contractStarts = baContractStartMapper.selectList(contractStartQueryWrapper);
            //累计合同额
            BigDecimal contractAmount = new BigDecimal(0);
            //累计贸易额
            BigDecimal tradeAmount = new BigDecimal(0);
            //累计运输量
            BigDecimal checkCoalNum = new BigDecimal(0);
            //昕科累计运单量
            BigDecimal ccjweightTotal = new BigDecimal(0);
            //中储累计运单量
            BigDecimal weightTotal = new BigDecimal(0);
            for (BaContractStart baContractStart:contractStarts) {
              //合同额
                if(baContractStart.getPrice() != null && baContractStart.getTonnage() != null){
                    contractAmount = contractAmount.add(baContractStart.getPrice().multiply(baContractStart.getTonnage()));
                }
                //下游结算单金额
                QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                settlementQueryWrapper.eq("contract_id", baContractStart.getId());
                settlementQueryWrapper.eq("state", "settlement:pass");
                settlementQueryWrapper.eq("flag", 0);
                settlementQueryWrapper.eq("settlement_type", 2);
                List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                for (BaSettlement baSettlement : baSettlementList) {
                    if(baSettlement.getSettlementAmount() != null){
                        tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                    }
                }
                //累计运输量
                QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                transportQueryWrapper.eq("start_id", baContractStart.getId());
                transportQueryWrapper.eq("flag", 0);
                List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                for (BaTransport baTransport : baTransports) {
                    //验收信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id", baTransport.getId());
                    checkQueryWrapper.eq("type", 1);
                    List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                    for (BaCheck baCheck : baChecks) {
                        checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                    }
                    //无车承运货源数据（昕科）
                    QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
                    automobileQueryWrapper.eq("order_id",baTransport.getId());
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
                    if(StringUtils.isNotNull(baTransportAutomobile)){
                        //查询运单(昕科)
                        QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
                        orderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                        List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
                        for (BaShippingOrder baShippingOrder:shippingOrders) {
                            if(baShippingOrder.getCcjweight() != null){
                                //昕科装车重量累计
                                ccjweightTotal = ccjweightTotal.add(baShippingOrder.getCcjweight());
                            }
                        }
                    }
                    //无车承运货源数据（中储）
                    QueryWrapper<BaTransportCmst> cmstQueryWrapper = new QueryWrapper<>();
                    cmstQueryWrapper.eq("relation_id",baTransport.getId());
                    BaTransportCmst transportCmst = baTransportCmstMapper.selectOne(cmstQueryWrapper);
                    if(StringUtils.isNotNull(transportCmst)){
                        //查询运单（中储）
                        BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                        baShippingOrderCmst1.setYardId(transportCmst.getId());
                        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    /*QueryWrapper<BaShippingOrderCmst> orderCmstQueryWrapper = new QueryWrapper<>();
                    orderCmstQueryWrapper.eq("yard_id",transportCmst.getId());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(orderCmstQueryWrapper);*/
                        for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                            if(baShippingOrderCmst.getConfirmedDeliveryWeight() != null){
                                //中储装车累计重量
                                weightTotal = weightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                            }
                        }
                    }

                }
            }
            companyGroup.setContractAmount(contractAmount);
            companyGroup.setTradeAmount(tradeAmount);
            companyGroup.setCheckCoalNum(checkCoalNum.add(ccjweightTotal.add(weightTotal)));
        }
        return baCompanyGroups;
    }

    /**
     * 新增集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    @Override
    public int insertBaCompanyGroup(BaCompanyGroup baCompanyGroup)
    {
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("company_name",baCompanyGroup.getCompanyName());
        BaCompanyGroup selectOne = baCompanyGroupMapper.selectOne(groupQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -2;
        }
        groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("company_abbreviation",baCompanyGroup.getCompanyName());
        selectOne = baCompanyGroupMapper.selectOne(groupQueryWrapper);
        if(StringUtils.isNotNull(selectOne)){
            return -1;
        }
        baCompanyGroup.setId(getRedisIncreID.getId());
        baCompanyGroup.setCreateTime(DateUtils.getNowDate());
        baCompanyGroup.setCreateBy(SecurityUtils.getUsername());
        baCompanyGroup.setUserId(SecurityUtils.getUserId());
        baCompanyGroup.setDeptId(SecurityUtils.getDeptId());
        baCompanyGroup.setTenantId(SecurityUtils.getCurrComId());
        return baCompanyGroupMapper.insertBaCompanyGroup(baCompanyGroup);
    }

    /**
     * 修改集团公司
     *
     * @param baCompanyGroup 集团公司
     * @return 结果
     */
    @Override
    public int updateBaCompanyGroup(BaCompanyGroup baCompanyGroup)
    {
        BaCompanyGroup baCompanyGroup1 = baCompanyGroupMapper.selectBaCompanyGroupById(baCompanyGroup.getId());
        if(baCompanyGroup1.getCompanyAbbreviation().equals(baCompanyGroup.getCompanyAbbreviation()) == false){
            QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
            groupQueryWrapper.eq("company_abbreviation",baCompanyGroup.getCompanyName());
            BaCompanyGroup selectOne = baCompanyGroupMapper.selectOne(groupQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -1;
            }
        }
        if(baCompanyGroup1.getCompanyName().equals(baCompanyGroup.getCompanyName()) == false){
            QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
            groupQueryWrapper.eq("company_name",baCompanyGroup.getCompanyName());
            BaCompanyGroup selectOne = baCompanyGroupMapper.selectOne(groupQueryWrapper);
            if(StringUtils.isNotNull(selectOne)){
                return -2;
            }
        }
        baCompanyGroup.setUpdateTime(DateUtils.getNowDate());
        baCompanyGroup.setUpdateBy(SecurityUtils.getUsername());
        return baCompanyGroupMapper.updateBaCompanyGroup(baCompanyGroup);
    }

    /**
     * 批量删除集团公司
     *
     * @param ids 需要删除的集团公司ID
     * @return 结果
     */
    @Override
    public int deleteBaCompanyGroupByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                QueryWrapper<BaEnterprise> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("company_nature",id);
                queryWrapper.eq("flag",0);
                List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(queryWrapper);
                if(baEnterpriseList.size() > 0){
                    return -1;
                }
                 baCompanyGroupMapper.deleteBaCompanyGroupById(id);
            }
        }
        return 1;
    }

    /**
     * 删除集团公司信息
     *
     * @param id 集团公司ID
     * @return 结果
     */
    @Override
    public int deleteBaCompanyGroupById(String id)
    {
        return baCompanyGroupMapper.deleteBaCompanyGroupById(id);
    }

    /**
     * 首页查询终端面板统计
     */
    @Override
    public BaEnterpriseDTO companyGroupListStat(BaCompanyGroup baCompanyGroup) {

        BaEnterpriseDTO baEnterpriseDTO = new BaEnterpriseDTO();

        List<BaEnterprise> list = new ArrayList<>();

        //集团公司
        QueryWrapper<BaCompanyGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("type","1");
        //租户ID
        groupQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        List<BaCompanyGroup> baCompanyGroups = baCompanyGroupMapper.selectList(groupQueryWrapper);
        baEnterpriseDTO.setGroup(baCompanyGroups.size());


        //累计贸易额
        BigDecimal tradeAmountTotal = new BigDecimal(0);
        //累计运输量
        BigDecimal checkCoalNumTotal = new BigDecimal(0);

            //查询终端
            QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
            enterpriseQueryWrapper.eq("state",AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
            enterpriseQueryWrapper.eq("flag",0);
            //租户ID
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            if(StringUtils.isNotEmpty(baCompanyGroup.getId())){
                if(!"screen".equals(baCompanyGroup.getId())){
                    enterpriseQueryWrapper.eq("company_nature",baCompanyGroup.getId());
                }
                if("screen".equals(baCompanyGroup.getId())){
                    enterpriseQueryWrapper.eq("company_nature","");
                }
            }
            List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(enterpriseQueryWrapper);
            baEnterpriseDTO.setPowerPlant(baEnterpriseList.size());

            for (BaEnterprise baEnterprise:baEnterpriseList) {
                //累计贸易额
                BigDecimal tradeAmount = new BigDecimal(0);
                //累计运输量
                BigDecimal checkCoalNum = new BigDecimal(0);
                //所属集团
                if(StringUtils.isNotEmpty(baEnterprise.getCompanyNature())){
                    BaCompanyGroup baCompanyGroup1 = baCompanyGroupMapper.selectBaCompanyGroupById(baEnterprise.getCompanyNature());
                    baEnterprise.setCompanyName(baCompanyGroup1.getCompanyName());
                }
                //查询所有关联合同启动
                TerminalDTO terminalDTO = new TerminalDTO();
                terminalDTO.setEnterpriseId(baEnterprise.getId());
                List<TerminalDTO> terminalDTOS = baContractStartMapper.terminalStatistics(terminalDTO);

                    for (TerminalDTO terminalDTO1:terminalDTOS) {
                        //下游结算单金额
                       /* QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
                        settlementQueryWrapper.eq("contract_id", baContractStart.getId());
                        settlementQueryWrapper.eq("state", "settlement:pass");
                        settlementQueryWrapper.eq("flag", 0);
                        settlementQueryWrapper.eq("settlement_type", 2);
                        List<BaSettlement> baSettlementList = baSettlementMapper.selectList(settlementQueryWrapper);
                        for (BaSettlement baSettlement : baSettlementList) {
                            if(baSettlement.getSettlementAmount() != null){
                                tradeAmount = tradeAmount.add(baSettlement.getSettlementAmount());
                            }
                        }*/
                        //下游结算单金额
                        BaSettlement baSettlement = new BaSettlement();
                        baSettlement.setFlag(0);
                        baSettlement.setSettlementType(new Long(2)); //销售结算单
                        baSettlement.setContractId(terminalDTO1.getStartId());
                        baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
                        //租户ID
                        baSettlement.setTenantId(SecurityUtils.getCurrComId());
                        BigDecimal tempTradeAmount = baSettlementMapper.sumBaSettlementAndCollection(baSettlement);
                        if (tempTradeAmount != null) {
                            tradeAmount = tradeAmount.add(tempTradeAmount);
                        }
                        /*//累计运输量
                        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
                        transportQueryWrapper.eq("start_id", baContractStart.getId());
                        transportQueryWrapper.eq("flag", 0);
                        List<BaTransport> baTransports = baTransportMapper.selectList(transportQueryWrapper);
                        for (BaTransport baTransport : baTransports) {
                            //验收信息
                            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                            checkQueryWrapper.eq("relation_id", baTransport.getId());
                            checkQueryWrapper.eq("type", 1);
                            List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                            for (BaCheck baCheck : baChecks) {
                                checkCoalNum = checkCoalNum.add(baCheck.getCheckCoalNum());
                            }
                            //无车承运货源数据（昕科）
                            QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
                            automobileQueryWrapper.eq("order_id",baTransport.getId());
                            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
                            if(StringUtils.isNotNull(baTransportAutomobile)){
                                //查询运单(昕科)
                                QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
                                orderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                                List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
                                for (BaShippingOrder baShippingOrder:shippingOrders) {
                                    if(baShippingOrder.getCcjweight() != null){
                                        //昕科装车重量累计
                                        ccjweightTotal = ccjweightTotal.add(baShippingOrder.getCcjweight());
                                    }
                                }
                            }
                            //无车承运货源数据（中储）
                            QueryWrapper<BaTransportCmst> cmstQueryWrapper = new QueryWrapper<>();
                            cmstQueryWrapper.eq("relation_id",baTransport.getId());
                            BaTransportCmst transportCmst = baTransportCmstMapper.selectOne(cmstQueryWrapper);
                            if(StringUtils.isNotNull(transportCmst)){
                                //查询运单（中储）
                                BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                                baShippingOrderCmst1.setYardId(transportCmst.getId());
                                List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstService.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    *//*QueryWrapper<BaShippingOrderCmst> orderCmstQueryWrapper = new QueryWrapper<>();
                    orderCmstQueryWrapper.eq("yard_id",transportCmst.getId());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(orderCmstQueryWrapper);*//*
                                for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                                    if(baShippingOrderCmst.getConfirmedDeliveryWeight() != null){
                                        //中储装车累计重量
                                        weightTotal = weightTotal.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                                    }
                                }
                            }

                        }*/
                        BaTransport baTransport = new BaTransport();
                        baTransport.setStartId(terminalDTO1.getStartId());
                        baTransport.setTenantId(SecurityUtils.getCurrComId());
                        baTransport.setType("1");
                        BaTransport baTransport1 = baTransportMapper.selectTransportSum(baTransport);
                        if(!ObjectUtils.isEmpty(baTransport1)){
                            if(baTransport1.getCheckCoalNum() != null){
                                checkCoalNum = checkCoalNum.add(baTransport1.getCheckCoalNum());
                            }
                            if(baTransport1.getCcjweight() != null){
                                checkCoalNum = checkCoalNum.add(baTransport1.getCcjweight());
                            }
                            if(baTransport1.getConfirmedDeliveryWeight() != null){
                                checkCoalNum = checkCoalNum.add(baTransport1.getConfirmedDeliveryWeight());
                            }
                        }

                }
                baEnterprise.setTradeAmount(tradeAmount);
                baEnterprise.setCheckCoalNum(checkCoalNum);
                list.add(baEnterprise);
                tradeAmountTotal = tradeAmountTotal.add(tradeAmount);
                checkCoalNumTotal = checkCoalNumTotal.add(checkCoalNum);
            }
        baEnterpriseDTO.setTradeAmount(tradeAmountTotal);
        baEnterpriseDTO.setCheckCoalNum(checkCoalNumTotal);
        baEnterpriseDTO.setBaEnterprises(list);

        return baEnterpriseDTO;
    }

}
