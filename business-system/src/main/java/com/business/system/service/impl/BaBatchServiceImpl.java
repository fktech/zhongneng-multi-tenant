package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import com.business.system.service.IBaGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaBatchService;
import org.springframework.util.ObjectUtils;

/**
 * 批次Service业务层处理
 *
 * @author single
 * @date 2023-01-05
 */
@Service
public class BaBatchServiceImpl extends ServiceImpl<BaBatchMapper, BaBatch> implements IBaBatchService
{
    @Autowired
    private BaBatchMapper baBatchMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaDepotHeadMapper baDepotHeadMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private IBaGoodsService iBaGoodsService;

    /**
     * 查询批次
     *
     * @param id 批次ID
     * @return 批次
     */
    @Override
    public BaBatch selectBaBatchById(String id)
    {
        //获取批次信息
        BaBatch baBatch = baBatchMapper.selectBaBatchById(id);
        if(!ObjectUtils.isEmpty(baBatch)){
            //设置订单名称
            if(StringUtils.isNotEmpty(baBatch.getOrderId())){
                baBatch.setOrderName(baOrderMapper.selectBaOrderById(baBatch.getOrderId()).getOrderName());
            }
            //设置供应商名称
            baBatch.setSupplierName(baSupplierMapper.selectBaSupplierById(baBatch.getSupplierId()).getName());
        }
        //设置入库列表信息
        QueryWrapper<BaDepotHead> queryWrapper = new QueryWrapper();
        queryWrapper.eq("batch", baBatch.getId()); //批次
        queryWrapper.eq("type", "1");       //入库类型
        //queryWrapper.eq("order_id", baBatch.getOrderId()); //订单编号
        queryWrapper.eq("flag",0);
        queryWrapper.eq("head_status","3");
        queryWrapper.orderByDesc("create_time");
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.selectList(queryWrapper);
        for (BaDepotHead depotHead:baDepotHeads){
            //商品
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(depotHead.getMaterialId());
            depotHead.setMaterialName(baGoods.getName());
            depotHead.setMaterialCode(baGoods.getCode());
            if(depotHead.getType() == 1){
                //总量
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",depotHead.getId());
                BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
                if(StringUtils.isNotNull(check)){
                    depotHead.setTotals(check.getCheckCoalNum());
                }
            }
            //批次号
            depotHead.setBatchName(baBatchMapper.selectBaBatchById(depotHead.getBatch()).getBatch());
            //仓库名称
            depotHead.setDepotName(baDepotMapper.selectBaDepotById(depotHead.getDepotId()).getName());
        }
        baBatch.setIntoBaDepotHeads(baDepotHeads);

        //设置出库列表信息
        queryWrapper = new QueryWrapper();
        queryWrapper.eq("batch", baBatch.getId()); //批次
        queryWrapper.eq("type", "2");       //出入库类型
        //queryWrapper.eq("order_id", baBatch.getOrderId()); //订单编号
        queryWrapper.eq("flag",0);
        queryWrapper.eq("head_status","2");
        queryWrapper.orderByDesc("create_time");
        List<BaDepotHead> baDepotHeadList = baDepotHeadMapper.selectList(queryWrapper);
        for (BaDepotHead depotHead:baDepotHeadList){
            //商品
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(depotHead.getMaterialId());
            depotHead.setMaterialName(baGoods.getName());
            depotHead.setMaterialCode(baGoods.getCode());
            //仓库名称
            depotHead.setDepotName(baDepotMapper.selectBaDepotById(depotHead.getDepotId()).getName());
        }
        baBatch.setOutputBaDepotHeads(baDepotHeadList);

        //设置存货信息
        QueryWrapper<BaMaterialStock> baMaterialStockQueryWrapper = new QueryWrapper();
        baMaterialStockQueryWrapper.eq("batch", baBatch.getId()); //批次
        //baMaterialStockQueryWrapper.eq("flag",0);
        baMaterialStockQueryWrapper.orderByDesc("create_time");
        List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectList(baMaterialStockQueryWrapper);
        for (BaMaterialStock materialStock:baMaterialStocks) {
            //商品信息
            BaGoods baGoods = iBaGoodsService.selectBaGoodsById(materialStock.getGoodsId());
            materialStock.setBaGoods(baGoods);
            //供应商
            materialStock.setSupplierName(baSupplierMapper.selectBaSupplierById(baBatch.getSupplierId()).getName());
            //批次号
            materialStock.setBatchNum(baBatchMapper.selectBaBatchById(materialStock.getBatch()).getBatch());
            //仓库名称
            materialStock.setDepName(baDepotMapper.selectBaDepotById(materialStock.getDepotId()).getName());
        }
        baBatch.setBaMaterialStocks(baMaterialStocks);
        return baBatch;
    }

    /**
     * 查询批次列表
     *
     * @param baBatch 批次
     * @return 批次
     */
    @Override
    public List<BaBatch> selectBaBatchList(BaBatch baBatch)
    {
        List<BaBatch> baBatches = baBatchMapper.selectBaBatchList(baBatch);
        baBatches.stream().forEach(item -> {
            //设置供应商名称
            item.setSupplierName(baSupplierMapper.selectBaSupplierById(item.getSupplierId()).getName());
        });
        return baBatches;
    }

    /**
     * 新增批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    @Override
    public int insertBaBatch(BaBatch baBatch)
    {
        QueryWrapper<BaBatch> batchQueryWrapper = new QueryWrapper<>();
        batchQueryWrapper.eq("batch",baBatch.getBatch());
        BaBatch baBatch1 = baBatchMapper.selectOne(batchQueryWrapper);
        if(StringUtils.isNotNull(baBatch1)){
            return 0;
        }
        baBatch.setId("PC"+getRedisIncreID.getId());
        baBatch.setCreateTime(DateUtils.getNowDate());
        baBatch.setState("0");
        return baBatchMapper.insertBaBatch(baBatch);
    }

    /**
     * 修改批次
     *
     * @param baBatch 批次
     * @return 结果
     */
    @Override
    public int updateBaBatch(BaBatch baBatch)
    {
        baBatch.setUpdateTime(DateUtils.getNowDate());
        baBatch.setUpdateBy(SecurityUtils.getUsername());
        return baBatchMapper.updateBaBatch(baBatch);
    }

    /**
     * 批量删除批次
     *
     * @param ids 需要删除的批次ID
     * @return 结果
     */
    @Override
    public int deleteBaBatchByIds(String[] ids)
    {
        return baBatchMapper.deleteBaBatchByIds(ids);
    }

    /**
     * 删除批次信息
     *
     * @param id 批次ID
     * @return 结果
     */
    @Override
    public int deleteBaBatchById(String id)
    {
        return baBatchMapper.deleteBaBatchById(id);
    }

    @Override
    public int selectBaBatch(String batch) {
        //判断传入的批次是否为空
        if(StringUtils.isNotEmpty(batch)){
            //下拉框选择的批次
           BaBatch baBatch1 = baBatchMapper.selectBaBatchById(batch);
           if(StringUtils.isNotNull(baBatch1)){
               return 1;
           }else {
               //手动输入批次号
               QueryWrapper<BaBatch> batchQueryWrapper = new QueryWrapper<>();
               batchQueryWrapper.eq("batch",batch);
               batchQueryWrapper.eq("state",1);
               BaBatch baBatch = baBatchMapper.selectOne(batchQueryWrapper);
               if(StringUtils.isNotNull(baBatch)){
                   return 0;
               }else {
                   return 1;
               }
           }
        }else {
            return -1;
        }
    }

}
