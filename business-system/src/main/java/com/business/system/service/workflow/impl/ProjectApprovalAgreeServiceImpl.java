package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaProject;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.mapper.BaProjectMapper;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IProjectApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 立项申请审批结果:审批通过
 */
@Service("projectApprovalContent:processApproveResult:pass")
@Slf4j
public class ProjectApprovalAgreeServiceImpl extends IProjectApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
        return result;
    }
}
