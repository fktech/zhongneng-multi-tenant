package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.BaHiEnterprise;
import com.business.system.domain.dto.BaEnterpriseInstanceRelatedEditDTO;
import com.business.system.domain.vo.BaEnterpriseVO;

/**
 * 用煤企业Service接口
 *
 * @author ljb
 * @date 2022-11-29
 */
public interface IBaEnterpriseService
{
    /**
     * 查询用煤企业
     *
     * @param id 用煤企业ID
     * @return 用煤企业
     */
    public BaEnterprise selectBaEnterpriseById(String id);

    /**
     * 查询用煤企业列表
     *
     * @param baEnterprise 用煤企业
     * @return 用煤企业集合
     */
    public List<BaEnterprise> selectBaEnterpriseList(BaEnterprise baEnterprise);

    /**
     * 新增用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    public int insertBaEnterprise(BaEnterprise baEnterprise);

    /**
     * 修改用煤企业
     *
     * @param baEnterprise 用煤企业
     * @return 结果
     */
    public int updateBaEnterprise(BaEnterprise baEnterprise);

    /**
     * 修改账户信息
     */
    public int updateEnterprise(BaEnterprise baEnterprise);

    /**
     * 批量删除用煤企业
     *
     * @param ids 需要删除的用煤企业ID
     * @return 结果
     */
    public int deleteBaEnterpriseByIds(String[] ids);

    /**
     * 删除用煤企业信息
     *
     * @param id 用煤企业ID
     * @return 结果
     */
    public int deleteBaEnterpriseById(String id);

    /**
     * 封装终端企业下拉列表
     */
    List<BaEnterprise> selectEnterpriseList();

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 审批办理保存编辑业务数据
     * @param baEnterpriseInstanceRelatedEditDTO
     * @return
     */
    int updateProcessBusinessData(BaEnterpriseInstanceRelatedEditDTO baEnterpriseInstanceRelatedEditDTO);

    /**
     * 删除所需煤种
     */
    public int delete(String id);

    public List<BaEnterprise> enterpriseList();
    /**
     * 变更终端企业
     */
    int changeBaEnterprise(BaEnterprise baEnterprise) throws Exception;

    /**
     * 查询用煤企业变更历史列表
     */
    List<BaHiEnterprise> selectChangeBaHiEnterpriseList(BaHiEnterprise baHiEnterprise);

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    List<String> selectChangeDataList(String id);

    /**
     * 查询用煤企业变更记录列表
     */
    List<BaHiEnterprise> selectChangeDataBaHiEnterpriseList(BaHiEnterprise baHiEnterprise);

    /**
     * 获取客商类型
     */
    String getCompanyType(String id);

    /**
     * 终端发运排名
     */
    List<BaEnterpriseVO> countRanking();
}
