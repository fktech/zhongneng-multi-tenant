package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaRailwayBureauMapper;
import com.business.system.domain.BaRailwayBureau;
import com.business.system.service.IBaRailwayBureauService;

/**
 * 铁路局Service业务层处理
 *
 * @author ljb
 * @date 2023-01-16
 */
@Service
public class BaRailwayBureauServiceImpl extends ServiceImpl<BaRailwayBureauMapper, BaRailwayBureau> implements IBaRailwayBureauService
{
    @Autowired
    private BaRailwayBureauMapper baRailwayBureauMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询铁路局
     *
     * @param id 铁路局ID
     * @return 铁路局
     */
    @Override
    public BaRailwayBureau selectBaRailwayBureauById(String id)
    {
        return baRailwayBureauMapper.selectBaRailwayBureauById(id);
    }

    /**
     * 查询铁路局列表
     *
     * @param baRailwayBureau 铁路局
     * @return 铁路局
     */
    @Override
    public List<BaRailwayBureau> selectBaRailwayBureauList(BaRailwayBureau baRailwayBureau)
    {
        return baRailwayBureauMapper.selectBaRailwayBureauList(baRailwayBureau);
    }

    /**
     * 新增铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    @Override
    public int insertBaRailwayBureau(BaRailwayBureau baRailwayBureau)
    {
        baRailwayBureau.setId(getRedisIncreID.getId());
        baRailwayBureau.setCreateTime(DateUtils.getNowDate());
        return baRailwayBureauMapper.insertBaRailwayBureau(baRailwayBureau);
    }

    /**
     * 修改铁路局
     *
     * @param baRailwayBureau 铁路局
     * @return 结果
     */
    @Override
    public int updateBaRailwayBureau(BaRailwayBureau baRailwayBureau)
    {
        baRailwayBureau.setUpdateTime(DateUtils.getNowDate());
        return baRailwayBureauMapper.updateBaRailwayBureau(baRailwayBureau);
    }

    /**
     * 批量删除铁路局
     *
     * @param ids 需要删除的铁路局ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayBureauByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaRailwayBureau baRailwayBureau = baRailwayBureauMapper.selectBaRailwayBureauById(id);
                baRailwayBureau.setFlag(1);
                baRailwayBureauMapper.updateBaRailwayBureau(baRailwayBureau);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除铁路局信息
     *
     * @param id 铁路局ID
     * @return 结果
     */
    @Override
    public int deleteBaRailwayBureauById(String id)
    {
        return baRailwayBureauMapper.deleteBaRailwayBureauById(id);
    }

}
