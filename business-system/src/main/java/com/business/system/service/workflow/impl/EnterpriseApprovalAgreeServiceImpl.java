package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IEnterpriseApprovalService;
import com.business.system.service.workflow.ISupplierApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 终端企业申请审批结果:审批通过
 */
@Service("enterpriseApprovalContent:processApproveResult:pass")
@Slf4j
public class EnterpriseApprovalAgreeServiceImpl extends IEnterpriseApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaEnterprise baEnterprise = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        return result;
    }
}
