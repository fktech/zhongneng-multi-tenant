package com.business.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.PercentUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.DepotHeadDTO;
import com.business.system.domain.vo.BaDepotHeadChartVO;
import com.business.system.domain.vo.BaDepotVO;
import com.business.system.domain.vo.BaMasterialStockVO;
import com.business.system.mapper.*;
import com.business.system.service.IBaGoodsService;
import com.business.system.service.IBaMaterialStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 库存Service业务层处理
 *
 * @author single
 * @date 2023-01-05
 */
@Service
public class BaMaterialStockServiceImpl extends ServiceImpl<BaMaterialStockMapper, BaMaterialStock> implements IBaMaterialStockService {
    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;

    @Autowired
    private IBaGoodsService iBaGoodsService;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaDepotHeadMapper baDepotHeadMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaWashHeadMapper baWashHeadMapper;

    /**
     * 查询库存
     *
     * @param id 库存ID
     * @return 库存
     */
    @Override
    public BaMaterialStock selectBaMaterialStockById(String id) {
        return baMaterialStockMapper.selectBaMaterialStockById(id);
    }

    /**
     * 查询库存列表
     *
     * @param baMaterialStock 库存
     * @return 库存
     */
    @Override
    public List<BaMaterialStock> selectBaMaterialStockList(BaMaterialStock baMaterialStock) {
        baMaterialStock.setTenantId(SecurityUtils.getCurrComId());
        List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectBaMaterialStockListPage(baMaterialStock);
        for (BaMaterialStock materialStock:baMaterialStocks) {
            if(StringUtils.isNotEmpty(materialStock.getGoodsId())){
                BaGoods baGoods = iBaGoodsService.selectBaGoodsById(materialStock.getGoodsId());
                materialStock.setBaGoods(baGoods);
            }
            if(StringUtils.isNotEmpty(materialStock.getLocationId())){
                BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(materialStock.getLocationId());
                if(!ObjectUtils.isEmpty(baDepotLocation)){
                    materialStock.setLocationName(baDepotLocation.getName());
                }
            }
            //仓库名称
            if(StringUtils.isNotEmpty(materialStock.getDepotId())) {
                BaDepot baDepot = baDepotMapper.selectBaDepotById(materialStock.getDepotId());
                if(!ObjectUtils.isEmpty(baDepot)){
                    materialStock.setDepotName(baDepot.getName());
                }
            }
        }
        return baMaterialStocks;
    }

    /**
     * 新增库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    @Override
    public int insertBaMaterialStock(BaMaterialStock baMaterialStock) {
        baMaterialStock.setId("KC" + getRedisIncreID);
        baMaterialStock.setCreateTime(DateUtils.getNowDate());
        baMaterialStock.setTenantId(SecurityUtils.getCurrComId());
        return baMaterialStockMapper.insertBaMaterialStock(baMaterialStock);
    }

    /**
     * 修改库存
     *
     * @param baMaterialStock 库存
     * @return 结果
     */
    @Override
    public int updateBaMaterialStock(BaMaterialStock baMaterialStock) {
        baMaterialStock.setUpdateTime(DateUtils.getNowDate());
        return baMaterialStockMapper.updateBaMaterialStock(baMaterialStock);
    }

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存ID
     * @return 结果
     */
    @Override
    public int deleteBaMaterialStockByIds(String[] ids) {
        return baMaterialStockMapper.deleteBaMaterialStockByIds(ids);
    }

    /**
     * 删除库存信息
     *
     * @param id 库存ID
     * @return 结果
     */
    @Override
    public int deleteBaMaterialStockById(String id) {
        return baMaterialStockMapper.deleteBaMaterialStockById(id);
    }

    @Override
    public BigDecimal totals() {
        return baMaterialStockMapper.totals();
    }

    /**
     * 根据商品查询库存仓库及货位列表
     */
    @Override
    public List<BaMaterialStock> selectDepotList(BaMaterialStock baMaterialStock) {
        List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectBaMaterialStockListPage(baMaterialStock);
        Map<String, BaMaterialStock> depotMap = new HashMap<>();
        BaMaterialStock baMaterialStock1 = null;
        BaDepotLocation baDepotLocation = null;
        List<BaDepotLocation> baDepotLocationList = null;
        if(!CollectionUtils.isEmpty(baMaterialStocks)){
            for(BaMaterialStock materialStock : baMaterialStocks){
                if(StringUtils.isNotEmpty(materialStock.getLocationId())){
                    if(depotMap.containsKey(materialStock.getDepotId())){
                        baMaterialStock1 = depotMap.get(materialStock.getDepotId());
                        baDepotLocation = new BaDepotLocation();
                        baDepotLocation.setId(materialStock.getLocationId()); //库位ID
                        baDepotLocation.setName(materialStock.getLocationName()); //库位名称
                        baDepotLocation.setInventoryTotal(materialStock.getInventoryTotal()); //库存总量
                        if(!CollectionUtils.isEmpty(baMaterialStock1.getBaDepotLocationList())){
                            baMaterialStock1.getBaDepotLocationList().add(baDepotLocation);
                        } else {
                            baDepotLocationList = new ArrayList<>();
                            baDepotLocationList.add(baDepotLocation);
                            baMaterialStock1.setBaDepotLocationList(baDepotLocationList);
                        }
                        depotMap.put(materialStock.getDepotId(), baMaterialStock1);
                    } else {
                        baDepotLocation = new BaDepotLocation();
                        baDepotLocation.setId(materialStock.getLocationId());//库位ID
                        baDepotLocation.setName(materialStock.getLocationName());//库位名称
                        baDepotLocation.setInventoryTotal(materialStock.getInventoryTotal()); //库存总量
                        if(!CollectionUtils.isEmpty(materialStock.getBaDepotLocationList())){
                            materialStock.getBaDepotLocationList().add(baDepotLocation);
                        } else {
                            baDepotLocationList = new ArrayList<>();
                            baDepotLocationList.add(baDepotLocation);
                            materialStock.setBaDepotLocationList(baDepotLocationList);
                        }
                        depotMap.put(materialStock.getDepotId(), materialStock);
                    }
                }else {
                        depotMap.put(materialStock.getDepotId(), materialStock);
                }

            }
        }
        return new ArrayList<>(depotMap.values());
    }

    @Override
    public BaMaterialStock getListByDepotAndLocation(BaMaterialStock materialStock) {
        QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
        stockQueryWrapper.eq("depot_id", materialStock.getDepotId());
        stockQueryWrapper.eq("goods_id", materialStock.getGoodsId());
        if(StringUtils.isNotEmpty(materialStock.getLocationId())){
            stockQueryWrapper.eq("location_id", materialStock.getLocationId());
        } else {
            stockQueryWrapper.isNull("location_id");
        }
        List<BaMaterialStock> baMaterialStocksList = baMaterialStockMapper.selectList(stockQueryWrapper);
        if(!CollectionUtils.isEmpty(baMaterialStocksList)){
            return baMaterialStocksList.get(0);
        }
        return null;
    }

    @Override
    public int selectBaMaterialStockGoodsList(BaMaterialStock baMaterialStock) {
        QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
        stockQueryWrapper.select("distinct goods_id");
        stockQueryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        if(StringUtils.isNotEmpty(baMaterialStock.getDepotId())){
            stockQueryWrapper.eq("depot_id", baMaterialStock.getDepotId());
        }
        Integer integer = baMaterialStockMapper.selectCount(stockQueryWrapper);
        return integer;
    }

    /**
     * 库存统计
     */
    @Override
    public List<BaMasterialStockVO> sumList(BaMaterialStock baMaterialStock) {
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //统计入库
                    depotHeadDTO.setType(new Long(1));
                    depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤入库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(1));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washIntodepotTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
                    if(totals == null){
                        totals = new BigDecimal(0);
                    }
                    if(washIntodepotTotals != null){
                        totals = totals.add(washIntodepotTotals);
                    }
                    baMasterialStockVO.setTodayIntoTotal(totals);

                    //统计出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setType(new Long(2));
                    depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
                    if(totals1 == null){
                        totals1 = new BigDecimal(0);
                    }
                    if(washOutputdepotTotals != null){
                        totals1 = totals1.add(washOutputdepotTotals);
                    }
                    baMasterialStockVO.setTodayOutputTotal(totals1);


                    if(StringUtils.isNotEmpty(baMasterialStockVO.getDepotId())){
                        BaDepotHead baDepotHead = new BaDepotHead();
                        baDepotHead.setDepotId(baMasterialStockVO.getDepotId());
                        //租户ID
                        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
                        baMasterialStockVO.setProjectCount(baDepotHeadMapper.countProjectList(baDepotHead));
                        BaMaterialStock baMaterialStock1 = new BaMaterialStock();
                        baMaterialStock1.setDepotId(baMasterialStockVO.getDepotId());
                        baMasterialStockVO.setInventoryTotal(baMaterialStockMapper.sumTotalByCon(baMaterialStock1));
                    }


                }
            }
        }
        return baMasterialStockVOS;
    }

    @Override
    public List<BaMasterialStockVO> sumProjectList(BaMaterialStock baMaterialStock) {
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumProjectList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setProjectId(baMasterialStockVO.getProjectId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //统计入库
                    depotHeadDTO.setType(new Long(1));
                    depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());

                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                    baMasterialStockVO.setTodayIntoTotal(totals);

                    //统计出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setProjectId(baMasterialStockVO.getProjectId());
                    depotHeadDTO.setType(new Long(2));
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                    baMasterialStockVO.setTodayOutputTotal(totals1);
                    DepotHeadDTO baDepotHead = new DepotHeadDTO();
                    baDepotHead.setType(new Long(1));
                    baDepotHead.setDepotId(baMasterialStockVO.getDepotId());
                    baDepotHead.setProjectId(baMasterialStockVO.getProjectId());
                    BigDecimal intoTotals = baDepotHeadMapper.totals(baDepotHead);
                    if(intoTotals == null){
                        intoTotals = new BigDecimal(0);
                    }
                    baDepotHead.setType(new Long(2));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal outputTotals = baDepotHeadMapper.totals(baDepotHead);
                    if(outputTotals == null){
                        outputTotals = new BigDecimal(0);
                    }

                    //查询立项库存信息
                    BaMaterialStock baMaterialStock1 = new BaMaterialStock();
                    baMaterialStock1.setDepotId(baMasterialStockVO.getDepotId());
                    BigDecimal inventoryTotal = baMaterialStockMapper.sumTotalByCon(baMaterialStock1);
                    if(inventoryTotal != null){
                        baMasterialStockVO.setInventoryTotal(inventoryTotal);
                    } else {
                        baMasterialStockVO.setInventoryTotal(new BigDecimal(0));
                    }
//                    baMasterialStockVO.setInventoryTotal(intoTotals.subtract(outputTotals));
                }
            }
        }
        return baMasterialStockVOS;
    }

    @Override
    public List<BaMasterialStockVO> sumLocationList(BaMaterialStock baMaterialStock) {
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumLocationList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setLocationId(baMasterialStockVO.getLocationId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //统计入库
                    depotHeadDTO.setType(new Long(1));
                    depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤入库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setLocationId(baMasterialStockVO.getLocationId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(1));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washIntodepotTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
                    if(totals == null){
                        totals = new BigDecimal(0);
                    }
                    if(washIntodepotTotals != null){
                        totals = totals.add(washIntodepotTotals);
                    }
                    baMasterialStockVO.setTodayIntoTotal(totals);

                    //统计出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setLocationId(baMasterialStockVO.getLocationId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setLocationId(baMasterialStockVO.getLocationId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
                    if(totals1 == null){
                        totals1 = new BigDecimal(0);
                    }
                    if(washOutputdepotTotals != null){
                        totals1 = totals1.add(washOutputdepotTotals);
                    }
                    baMasterialStockVO.setTodayOutputTotal(totals1);

                    BaMaterialStock baMaterialStock1 = new BaMaterialStock();
                    baMaterialStock1.setDepotId(baMasterialStockVO.getDepotId());
                    baMaterialStock1.setLocationId(baMasterialStockVO.getLocationId());
                    baMasterialStockVO.setInventoryTotal(baMaterialStockMapper.sumTotalByCon(baMaterialStock1));
                }
            }
        }
        return baMasterialStockVOS;
    }

    @Override
    public List<BaMasterialStockVO> sumGoodsList(BaMaterialStock baMaterialStock) {
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumGoodsList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //统计入库
                    depotHeadDTO.setType(new Long(1));
                    depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤入库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(1));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washIntodepotTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
                    if(totals == null){
                        totals = new BigDecimal(0);
                    }
                    if(washIntodepotTotals != null){
                        totals = totals.add(washIntodepotTotals);
                    }
                    baMasterialStockVO.setTodayIntoTotal(totals);

                    //统计出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
                    if(totals1 == null){
                        totals1 = new BigDecimal(0);
                    }
                    if(washOutputdepotTotals != null){
                        totals1 = totals1.add(washOutputdepotTotals);
                    }
                    baMasterialStockVO.setTodayOutputTotal(totals1);

                    BaMaterialStock baMaterialStock1 = new BaMaterialStock();
                    baMaterialStock1.setDepotId(baMasterialStockVO.getDepotId());
                    baMaterialStock1.setGoodsId(baMasterialStockVO.getGoodsId());
                    baMasterialStockVO.setInventoryTotal(baMaterialStockMapper.sumTotalByCon(baMaterialStock1));
                }
            }
        }
        return baMasterialStockVOS;
    }

    @Override
    public List<BaMasterialStockVO> sumGoodsLocationList(BaMaterialStock baMaterialStock) {
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumGoodsLocationList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    //统计入库
                    depotHeadDTO.setType(new Long(1));
                    depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤入库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(1));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washIntodepotTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
                    if(totals == null){
                        totals = new BigDecimal(0);
                    }
                    if(washIntodepotTotals != null){
                        totals = totals.add(washIntodepotTotals);
                    }
                    baMasterialStockVO.setTodayIntoTotal(totals);

                    //统计出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                    //统计洗煤、配煤出库
                    depotHeadDTO = new DepotHeadDTO();
                    depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                    depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                    depotHeadDTO.setCurrentFlag("currentFlag");
                    depotHeadDTO.setType(new Long(2));
                    //租户ID
                    depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                    BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
                    if(totals1 == null){
                        totals1 = new BigDecimal(0);
                    }
                    if(washOutputdepotTotals != null){
                        totals1 = totals1.add(washOutputdepotTotals);
                    }
                    baMasterialStockVO.setTodayOutputTotal(totals1);

                    BaMaterialStock baMaterialStock1 = new BaMaterialStock();
                    baMaterialStock1.setDepotId(baMasterialStockVO.getDepotId());
                    baMaterialStock1.setGoodsId(baMasterialStockVO.getGoodsId());
                    baMasterialStockVO.setInventoryTotal(baMaterialStockMapper.sumTotalByCon(baMaterialStock1));
                }
            }
        }
        return baMasterialStockVOS;
    }

    @Override
    public List<BaDepotHeadChartVO> sumGoodsDepotList(BaMaterialStock baMaterialStock) {
        List<BaDepotHeadChartVO> baDepotHeadChartVOS = new ArrayList<>();
        List<BaMasterialStockVO> baMasterialStockVOS = baMaterialStockMapper.sumGoodsDepotList(baMaterialStock);
        DepotHeadDTO depotHeadDTO = null;
        //商品占比map
        Map<String, BigDecimal> goodsProportionMap = new LinkedHashMap<>();
        BigDecimal depotTotals = new BigDecimal(0);
        BigDecimal sumTotals = new BigDecimal(0);
        if(!CollectionUtils.isEmpty(baMasterialStockVOS)){
            for(BaMasterialStockVO baMasterialStockVO : baMasterialStockVOS){
                if(!ObjectUtils.isEmpty(baMasterialStockVO)){
                    if("1".equals(baMaterialStock.getDepotType())){
                        depotHeadDTO = new DepotHeadDTO();
                        depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                        depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                        //统计入库
                        depotHeadDTO.setType(new Long(1));
                        depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
                        //租户ID
                        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                        if(StringUtils.isNotEmpty(baMaterialStock.getYearDepotTime())){
                            depotHeadDTO.setYearDepotTime(baMaterialStock.getYearDepotTime());
                        }
                        if(StringUtils.isNotEmpty(baMaterialStock.getMonthDepotTime())){
                            depotHeadDTO.setMonthDepotTime(baMaterialStock.getMonthDepotTime());
                        }
                        BigDecimal totals = baDepotHeadMapper.totals(depotHeadDTO);

                        //统计洗煤、配煤入库
                        depotHeadDTO = new DepotHeadDTO();
                        depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                        depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                        depotHeadDTO.setType(new Long(1));
                        //租户ID
                        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                        if(StringUtils.isNotEmpty(baMaterialStock.getYearDepotTime())){
                            depotHeadDTO.setYearDepotTime(baMaterialStock.getYearDepotTime());
                        }
                        if(StringUtils.isNotEmpty(baMaterialStock.getMonthDepotTime())){
                            depotHeadDTO.setMonthDepotTime(baMaterialStock.getMonthDepotTime());
                        }
                        BigDecimal washIntodepotTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
                        if(totals == null){
                            totals = new BigDecimal(0);
                        }
                        if(washIntodepotTotals != null){
                            totals = totals.add(washIntodepotTotals);
                        }
                        //判断是否已保存在map中
//                        if(goodsProportionMap.size() < 5){
                            if(!ObjectUtils.isEmpty(goodsProportionMap.get(baMasterialStockVO.getGoodsName()))){
                                depotTotals = goodsProportionMap.get(baMasterialStockVO.getGoodsName());
                                goodsProportionMap.put(baMasterialStockVO.getGoodsName(), depotTotals.add(totals));
                            } else {
                                goodsProportionMap.put(baMasterialStockVO.getGoodsName(), totals);
                            }
//                        }
//                        else {
//                            if(!ObjectUtils.isEmpty(goodsProportionMap.get("其他"))){
//                                depotTotals = goodsProportionMap.get("其他");
//                                goodsProportionMap.put("其他", depotTotals.add(totals));
//                            } else {
//                                goodsProportionMap.put("其他", totals);
//                            }
//                        }
                        sumTotals = sumTotals.add(totals);
                    } else {
                        //统计出库
                        depotHeadDTO = new DepotHeadDTO();
                        depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                        depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                        depotHeadDTO.setType(new Long(2));
                        depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
                        //租户ID
                        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                        if(StringUtils.isNotEmpty(baMaterialStock.getYearDepotTime())){
                            depotHeadDTO.setYearDepotTime(baMaterialStock.getYearDepotTime());
                        }
                        if(StringUtils.isNotEmpty(baMaterialStock.getMonthDepotTime())){
                            depotHeadDTO.setMonthDepotTime(baMaterialStock.getMonthDepotTime());
                        }
                        BigDecimal totals1 = baDepotHeadMapper.totals(depotHeadDTO);

                        //统计洗煤、配煤出库
                        depotHeadDTO = new DepotHeadDTO();
                        depotHeadDTO.setDepotId(baMasterialStockVO.getDepotId());
                        depotHeadDTO.setGoodsId(baMasterialStockVO.getGoodsId());
                        depotHeadDTO.setType(new Long(2));
                        //租户ID
                        depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
                        if(StringUtils.isNotEmpty(baMaterialStock.getYearDepotTime())){
                            depotHeadDTO.setYearDepotTime(baMaterialStock.getYearDepotTime());
                        }
                        if(StringUtils.isNotEmpty(baMaterialStock.getMonthDepotTime())){
                            depotHeadDTO.setMonthDepotTime(baMaterialStock.getMonthDepotTime());
                        }
                        BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
                        if (totals1 == null) {
                            totals1 = new BigDecimal(0);
                        }
                        if (washOutputdepotTotals != null) {
                            totals1 = totals1.add(washOutputdepotTotals);
                        }
                        //判断是否已保存在map中
                        if(!ObjectUtils.isEmpty(goodsProportionMap.get(baMasterialStockVO.getGoodsName()))){
                            depotTotals = goodsProportionMap.get(baMasterialStockVO.getGoodsName());
                            goodsProportionMap.put(baMasterialStockVO.getGoodsName(), depotTotals.add(totals1));
                        } else {
                            goodsProportionMap.put(baMasterialStockVO.getGoodsName(), totals1);
                        }
//                        else {
//                            if(!ObjectUtils.isEmpty(goodsProportionMap.get("其他"))){
//                                depotTotals = goodsProportionMap.get("其他");
//                                goodsProportionMap.put("其他", depotTotals.add(totals1));
//                            } else {
//                                goodsProportionMap.put("其他", totals1);
//                            }
//                        }
                        sumTotals = sumTotals.add(totals1);
                    }
                }
            }

            // 对LinkedHashMap进行排序，同时排除特定键
            LinkedHashMap<String, BigDecimal> sortedMap = goodsProportionMap.entrySet().stream()
                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue())) // 按值排序
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (e1, e2) -> e1,
                            LinkedHashMap::new)); // 保留排序

            Set<Map.Entry<String, BigDecimal>> entrySet = sortedMap.entrySet();
            int count = 1;
            BigDecimal otherValue = new BigDecimal(0);
            for(Map.Entry<String, BigDecimal> goods : entrySet) {
                if(count < 6){
                    if(!ObjectUtils.isEmpty(goods.getValue())){
                        BaDepotHeadChartVO baDepotHeadChartVO = new BaDepotHeadChartVO();
                        baDepotHeadChartVO.setGoodsName(goods.getKey());
                        baDepotHeadChartVO.setProportion(goods.getValue());
                        baDepotHeadChartVOS.add(baDepotHeadChartVO);
                    }
                } else {
                    otherValue = otherValue.add(goods.getValue());
                }
                count++;
            }
            if(!ObjectUtils.isEmpty(otherValue)) {
                BaDepotHeadChartVO baDepotHeadChartVO = new BaDepotHeadChartVO();
                baDepotHeadChartVO.setGoodsName("其他");
                baDepotHeadChartVO.setProportion(otherValue);
                baDepotHeadChartVOS.add(baDepotHeadChartVO);
            }
        }
        return baDepotHeadChartVOS;
    }

    @Override
    public List<BaDepotVO> depotList(BaMaterialStock baMaterialStock) {
        return baMaterialStockMapper.depotList(baMaterialStock);
    }

    @Override
    public int countGoods(BaMaterialStock baMaterialStock) {
        return baMaterialStockMapper.countGoods(baMaterialStock);
    }
}
