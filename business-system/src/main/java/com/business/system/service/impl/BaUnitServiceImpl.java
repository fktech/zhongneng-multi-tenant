package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaUnitMapper;
import com.business.system.domain.BaUnit;
import com.business.system.service.IBaUnitService;

/**
 * 单位Service业务层处理
 *
 * @author ljb
 * @date 2022-11-30
 */
@Service
public class BaUnitServiceImpl extends ServiceImpl<BaUnitMapper, BaUnit> implements IBaUnitService
{
    @Autowired
    private BaUnitMapper baUnitMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询单位
     *
     * @param id 单位ID
     * @return 单位
     */
    @Override
    public BaUnit selectBaUnitById(String id)
    {
        return baUnitMapper.selectBaUnitById(id);
    }

    /**
     * 查询单位列表
     *
     * @param baUnit 单位
     * @return 单位
     */
    @Override
    public List<BaUnit> selectBaUnitList(BaUnit baUnit)
    {
        return baUnitMapper.selectBaUnitList(baUnit);
    }

    /**
     * 新增单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    @Override
    public int insertBaUnit(BaUnit baUnit)
    {
        QueryWrapper<BaUnit> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baUnit.getName());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        BaUnit result = baUnitMapper.selectOne(queryWrapper);
        if (StringUtils.isNotNull(result))
            return -1;
        baUnit.setId(getRedisIncreID.getId());
        baUnit.setCreateTime(DateUtils.getNowDate());
        baUnit.setCreateBy(SecurityUtils.getUsername());
        baUnit.setTenantId(SecurityUtils.getCurrComId());
        return baUnitMapper.insertBaUnit(baUnit);
    }

    /**
     * 修改单位
     *
     * @param baUnit 单位
     * @return 结果
     */
    @Override
    public int updateBaUnit(BaUnit baUnit)
    {
        baUnit.setUpdateTime(DateUtils.getNowDate());
        baUnit.setUpdateBy(SecurityUtils.getUsername());
        return baUnitMapper.updateBaUnit(baUnit);
    }

    /**
     * 批量删除单位
     *
     * @param ids 需要删除的单位ID
     * @return 结果
     */
    @Override
    public int deleteBaUnitByIds(String[] ids)
    {
        //伪删除
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaUnit baUnit = baUnitMapper.selectBaUnitById(id);
                baUnit.setFlag(1);
                baUnitMapper.updateBaUnit(baUnit);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除单位信息
     *
     * @param id 单位ID
     * @return 结果
     */
    @Override
    public int deleteBaUnitById(String id)
    {
        return baUnitMapper.deleteBaUnitById(id);
    }

}
