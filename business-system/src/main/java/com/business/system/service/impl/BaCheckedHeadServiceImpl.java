package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.Excel;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaMaterialStock;
import com.business.system.mapper.BaMaterialStockMapper;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaCheckedHeadMapper;
import com.business.system.domain.BaCheckedHead;
import com.business.system.service.IBaCheckedHeadService;

/**
 * 盘点Service业务层处理
 *
 * @author single
 * @date 2023-01-05
 */
@Service
public class BaCheckedHeadServiceImpl extends ServiceImpl<BaCheckedHeadMapper, BaCheckedHead> implements IBaCheckedHeadService
{
    @Autowired
    private BaCheckedHeadMapper baCheckedHeadMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    /**
     * 查询盘点
     *
     * @param id 盘点ID
     * @return 盘点
     */
    @Override
    public BaCheckedHead selectBaCheckedHeadById(String id)
    {
        return baCheckedHeadMapper.selectBaCheckedHeadById(id);
    }

    /**
     * 查询盘点列表
     *
     * @param baCheckedHead 盘点
     * @return 盘点
     */
    @Override
    public List<BaCheckedHead> selectBaCheckedHeadList(BaCheckedHead baCheckedHead)
    {
        return baCheckedHeadMapper.selectBaCheckedHeadList(baCheckedHead);
    }

    /**
     * 新增盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    @Override
    public int insertBaCheckedHead(BaCheckedHead baCheckedHead)
    {
        baCheckedHead.setId(getRedisIncreID.getId());
        baCheckedHead.setCreateTime(DateUtils.getNowDate());
        baCheckedHead.setCreateBy(SecurityUtils.getUsername());
        baCheckedHead.setUserId(SecurityUtils.getUserId());
        baCheckedHead.setDeptId(SecurityUtils.getDeptId());
        return baCheckedHeadMapper.insertBaCheckedHead(baCheckedHead);
    }

    /**
     * 修改盘点
     *
     * @param baCheckedHead 盘点
     * @return 结果
     */
    @Override
    public int updateBaCheckedHead(BaCheckedHead baCheckedHead)
    {
        baCheckedHead.setUpdateBy(SecurityUtils.getUsername());
        baCheckedHead.setUpdateTime(DateUtils.getNowDate());
        baCheckedHead.setUpdateTime(DateUtils.getNowDate());
        baCheckedHead.setUpdateBy(SecurityUtils.getUsername());
        baCheckedHead.setUpdateTime(DateUtils.getNowDate());
        //插入盘点信息
        return baCheckedHeadMapper.updateBaCheckedHead(baCheckedHead);
    }

    /**
     * 批量删除盘点
     *
     * @param ids 需要删除的盘点ID
     * @return 结果
     */
    @Override
    public int deleteBaCheckedHeadByIds(String[] ids)
    {
        return baCheckedHeadMapper.deleteBaCheckedHeadByIds(ids);
    }

    /**
     * 删除盘点信息
     *
     * @param id 盘点ID
     * @return 结果
     */
    @Override
    public int deleteBaCheckedHeadById(String id)
    {
        return baCheckedHeadMapper.deleteBaCheckedHeadById(id);
    }

    @Override
    public List<BaCheckedHead> selectBaCheckedHeadListByCheckId(BaCheckedHead baCheckedHead) {
        List<BaCheckedHead> baCheckedHeadList = baCheckedHeadMapper.selectBaCheckedHeadListByCheckId(baCheckedHead);
        baCheckedHeadList.stream().forEach(item -> {
            if(StringUtils.isEmpty(item.getCheckedState())){
                item.setCheckedState("1"); //设置未盘点
            }
        });
        return baCheckedHeadList;
    }

    @Override
    public BaCheckedHead selectBaCheckedHead(BaCheckedHead baCheckedHead) {
        QueryWrapper<BaCheckedHead> queryWrapper = new QueryWrapper();
        queryWrapper.eq("material_id", baCheckedHead.getMaterialId());
        queryWrapper.eq("checked_id", baCheckedHead.getCheckedId());
        return baCheckedHeadMapper.selectOne(queryWrapper);
    }

    @Override
    public int deleteBaCheckedHeadByCheckedId(String checkedId) {
        return baCheckedHeadMapper.deleteBaCheckedHeadByCheckId(checkedId);
    }

    /**
     * 统计库存盘点
     * @param baCheckedHead
     * @return
     */
    @Override
    public int countBaCheckedHead(BaCheckedHead baCheckedHead) {
        baCheckedHead.setTenantId(SecurityUtils.getCurrComId());
        return baCheckedHeadMapper.countBaCheckedHead(baCheckedHead);
    }
}
