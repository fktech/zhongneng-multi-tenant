package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.RedisService;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.DistrictsMapper;
import com.business.system.domain.Districts;
import com.business.system.service.IDistrictsService;
import com.alibaba.fastjson.JSON;

/**
 * 地区Service业务层处理
 *
 * @author ljb
 * @date 2022-11-28
 */
@Service
public class DistrictsServiceImpl extends ServiceImpl<DistrictsMapper, Districts> implements IDistrictsService
{
    @Autowired
    private DistrictsMapper districtsMapper;

    @Autowired
    private RedisService redisService;

    /**
     * 查询地区
     *
     * @param id 地区ID
     * @return 地区
     */
    @Override
    public Districts selectDistrictsById(Integer id)
    {
        return districtsMapper.selectDistrictsById(id);
    }

    /**
     * 查询地区列表
     *
     * @param districts 地区
     * @return 地区
     */
    @Override
    public List<Districts> selectDistrictsList(Districts districts)
    {
        return districtsMapper.selectDistrictsList(districts);
    }

    @Override
    public List<Districts> getAllList() {
        Object jsonResult = redisService.getCacheObject("districtsList");
        List<Districts> result ;

        if (StringUtils.isNotNull(jsonResult)) {
            result = JSON.parseArray(jsonResult.toString(),Districts.class);
            return result;
        }

        result = getAllDistricts();
        redisService.setCacheObject("districtsList", JSON.toJSON(result));

        return result;
    }

    private List<Districts> getAllDistricts(){
        //1.先查询所有顶级地区
        QueryWrapper<Districts> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pid",0);
        List<Districts> result = districtsMapper.selectList(queryWrapper);

        //2.循环查询该顶级地区是否有二级地区
        for (Districts secondFool : result ){
            queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("pid",secondFool.getId());
            List<Districts> secondResult = districtsMapper.selectList(queryWrapper);

            //添加二级地区
            if (secondResult != null && secondResult.size() > 0)
                secondFool.setChildren(secondResult);

            for (Districts thirdFool : secondResult){

                queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("pid", thirdFool.getId());
                List<Districts> thirdResult = districtsMapper.selectList(queryWrapper);

                if (thirdResult != null && thirdResult.size() > 0)
                    thirdFool.setChildren(thirdResult);

            }

        }
        return result;
    }

    /**
     * 新增地区
     *
     * @param districts 地区
     * @return 结果
     */
    @Override
    public int insertDistricts(Districts districts)
    {
        districts.setCreateTime(DateUtils.getNowDate());
        return districtsMapper.insertDistricts(districts);
    }

    /**
     * 修改地区
     *
     * @param districts 地区
     * @return 结果
     */
    @Override
    public int updateDistricts(Districts districts)
    {
        districts.setUpdateTime(DateUtils.getNowDate());
        return districtsMapper.updateDistricts(districts);
    }

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的地区ID
     * @return 结果
     */
    @Override
    public int deleteDistrictsByIds(Integer[] ids)
    {
        return districtsMapper.deleteDistrictsByIds(ids);
    }

    /**
     * 删除地区信息
     *
     * @param id 地区ID
     * @return 结果
     */
    @Override
    public int deleteDistrictsById(Integer id)
    {
        return districtsMapper.deleteDistrictsById(id);
    }

}
