package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaWashInventoryMapper;
import com.business.system.domain.BaWashInventory;
import com.business.system.service.IBaWashInventoryService;

/**
 * 洗煤配煤入库清单Service业务层处理
 *
 * @author single
 * @date 2024-01-05
 */
@Service
public class BaWashInventoryServiceImpl extends ServiceImpl<BaWashInventoryMapper, BaWashInventory> implements IBaWashInventoryService
{
    @Autowired
    private BaWashInventoryMapper baWashInventoryMapper;

    /**
     * 查询洗煤配煤入库清单
     *
     * @param id 洗煤配煤入库清单ID
     * @return 洗煤配煤入库清单
     */
    @Override
    public BaWashInventory selectBaWashInventoryById(String id)
    {
        return baWashInventoryMapper.selectBaWashInventoryById(id);
    }

    /**
     * 查询洗煤配煤入库清单列表
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 洗煤配煤入库清单
     */
    @Override
    public List<BaWashInventory> selectBaWashInventoryList(BaWashInventory baWashInventory)
    {
        return baWashInventoryMapper.selectBaWashInventoryList(baWashInventory);
    }

    /**
     * 新增洗煤配煤入库清单
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 结果
     */
    @Override
    public int insertBaWashInventory(BaWashInventory baWashInventory)
    {
        baWashInventory.setCreateTime(DateUtils.getNowDate());
        return baWashInventoryMapper.insertBaWashInventory(baWashInventory);
    }

    /**
     * 修改洗煤配煤入库清单
     *
     * @param baWashInventory 洗煤配煤入库清单
     * @return 结果
     */
    @Override
    public int updateBaWashInventory(BaWashInventory baWashInventory)
    {
        baWashInventory.setUpdateTime(DateUtils.getNowDate());
        return baWashInventoryMapper.updateBaWashInventory(baWashInventory);
    }

    /**
     * 批量删除洗煤配煤入库清单
     *
     * @param ids 需要删除的洗煤配煤入库清单ID
     * @return 结果
     */
    @Override
    public int deleteBaWashInventoryByIds(String[] ids)
    {
        return baWashInventoryMapper.deleteBaWashInventoryByIds(ids);
    }

    /**
     * 删除洗煤配煤入库清单信息
     *
     * @param id 洗煤配煤入库清单ID
     * @return 结果
     */
    @Override
    public int deleteBaWashInventoryById(String id)
    {
        return baWashInventoryMapper.deleteBaWashInventoryById(id);
    }

}
