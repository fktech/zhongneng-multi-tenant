package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaServiceVoucherService;

/**
 * 进厂确认单Service业务层处理
 *
 * @author single
 * @date 2023-09-19
 */
@Service
public class BaServiceVoucherServiceImpl extends ServiceImpl<BaServiceVoucherMapper, BaServiceVoucher> implements IBaServiceVoucherService
{
    @Autowired
    private BaServiceVoucherMapper baServiceVoucherMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaServiceVoucherRecordMapper baServiceVoucherRecordMapper;

    @Autowired
    private BaPoundRoomMapper baPoundRoomMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询进厂确认单
     *
     * @param id 进厂确认单ID
     * @return 进厂确认单
     */
    @Override
    public BaServiceVoucher selectBaServiceVoucherById(String id)
    {
        BaServiceVoucher baServiceVoucher = baServiceVoucherMapper.selectBaServiceVoucherById(id);

        //查看附表信息
        QueryWrapper<BaServiceVoucherRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("association_id",baServiceVoucher.getId());
        List<BaServiceVoucherRecord> baServiceVoucherRecords = baServiceVoucherRecordMapper.selectList(queryWrapper);
        baServiceVoucher.setBaServiceVoucherRecords(baServiceVoucherRecords);

        //采购计划
        if(StringUtils.isNotEmpty(baServiceVoucher.getPlanId())){
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baServiceVoucher.getPlanId());
            if(StringUtils.isNotEmpty(baProcurementPlan.getDepotId())){
                BaDepot baDepot = baDepotMapper.selectBaDepotById(baProcurementPlan.getDepotId());
                baServiceVoucher.setDepotName(baDepot.getName());
            }
            if(StringUtils.isNotEmpty(baProcurementPlan.getDepotHead())){
                baServiceVoucher.setDepotHead(baProcurementPlan.getDepotHead());
            }
        }

        return baServiceVoucher;
    }

    /**
     * 查询进厂确认单列表
     *
     * @param baServiceVoucher 进厂确认单
     * @return 进厂确认单
     */
    @Override
    public List<BaServiceVoucher> selectBaServiceVoucherList(BaServiceVoucher baServiceVoucher)
    {
        List<BaServiceVoucher> baServiceVouchers = baServiceVoucherMapper.selectBaServiceVoucher(baServiceVoucher);
        for (BaServiceVoucher serviceVoucher:baServiceVouchers) {
            //发起人名称
            serviceVoucher.setUserName(sysUserMapper.selectUserById(serviceVoucher.getUserId()).getNickName());
        }
        return baServiceVouchers;
    }

    /**
     * 新增进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    @Override
    public int insertBaServiceVoucher(BaServiceVoucher baServiceVoucher)
    {
        baServiceVoucher.setId(getRedisIncreID.getId());
        baServiceVoucher.setCreateTime(DateUtils.getNowDate());
        baServiceVoucher.setUserId(SecurityUtils.getUserId());
        baServiceVoucher.setDeptId(SecurityUtils.getDeptId());
        //批次进厂车数
        Long batchCars = 0L;
        //批次进厂数量
        BigDecimal batchNumber = new BigDecimal(0);
        //需付款
        BigDecimal paymentRequired = new BigDecimal(0);
        //货值
        BigDecimal goodsValue = new BigDecimal(0);
        if(baServiceVoucher.getBaServiceVoucherRecords().size() > 0){
            for (BaServiceVoucherRecord baServiceVoucherRecord:baServiceVoucher.getBaServiceVoucherRecords()) {
                baServiceVoucherRecord.setId(getRedisIncreID.getId());
                baServiceVoucherRecord.setAssociationId(baServiceVoucher.getId());
                baServiceVoucherRecord.setCreateTime(DateUtils.getNowDate());
                baServiceVoucherRecordMapper.insertBaServiceVoucherRecord(baServiceVoucherRecord);
                //进厂确认单（批次进厂车数）
                batchCars = baServiceVoucherRecord.getBatchCars() + batchCars;
                //进厂确认单（批次进厂数量）
                batchNumber = baServiceVoucherRecord.getBatchNumber().add(batchNumber);
                //进厂确认单（需付款）
                if(baServiceVoucherRecord.getPaymentRequired() != null){
                    paymentRequired = baServiceVoucherRecord.getPaymentRequired().add(paymentRequired);
                }else {
                    baServiceVoucherRecord.setPaymentRequired(new BigDecimal(0));
                    paymentRequired = baServiceVoucherRecord.getPaymentRequired().add(paymentRequired);
                }

                //进厂确认单（货值）
                goodsValue = baServiceVoucherRecord.getGoodsValue().add(goodsValue);
            }
        }
        baServiceVoucher.setBatchCars(batchCars);
        baServiceVoucher.setBatchNumber(batchNumber);
        baServiceVoucher.setPaymentRequired(paymentRequired);
        baServiceVoucher.setGoodsValue(goodsValue);
        int result = baServiceVoucherMapper.insertBaServiceVoucher(baServiceVoucher);
        if(result > 0){
            //更改磅房进厂确认状态
            if(StringUtils.isNotEmpty(baServiceVoucher.getRoomId())){
                String[] split = baServiceVoucher.getRoomId().split(",");
                for (String id:split) {
                    BaPoundRoom baPoundRoom = baPoundRoomMapper.selectBaPoundRoomById(id);
                    baPoundRoom.setEnteringState("1");
                    baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);
                }
            }
        }
        return result;
    }

    @Override
    public int insertServiceVoucher(List<BaServiceVoucherRecord> baServiceVoucherRecords) {
        BaServiceVoucher baServiceVoucher = new BaServiceVoucher();
        baServiceVoucher.setId(getRedisIncreID.getId());
        //新增进厂附表数据
        if(baServiceVoucherRecords.size() > 0){
            for (BaServiceVoucherRecord baServiceVoucherRecord:baServiceVoucherRecords) {
                baServiceVoucherRecord.setId(getRedisIncreID.getId());
                baServiceVoucherRecord.setAssociationId(baServiceVoucher.getId());
                baServiceVoucherRecord.setCreateTime(DateUtils.getNowDate());
                baServiceVoucherRecordMapper.insertBaServiceVoucherRecord(baServiceVoucherRecord);
            }
        }

        return 0;
    }


    /**
     * 修改进厂确认单
     *
     * @param baServiceVoucher 进厂确认单
     * @return 结果
     */
    @Override
    public int updateBaServiceVoucher(BaServiceVoucher baServiceVoucher)
    {
        baServiceVoucher.setUpdateTime(DateUtils.getNowDate());
        return baServiceVoucherMapper.updateBaServiceVoucher(baServiceVoucher);
    }

    /**
     * 批量删除进厂确认单
     *
     * @param ids 需要删除的进厂确认单ID
     * @return 结果
     */
    @Override
    public int deleteBaServiceVoucherByIds(String[] ids)
    {
        return baServiceVoucherMapper.deleteBaServiceVoucherByIds(ids);
    }

    /**
     * 删除进厂确认单信息
     *
     * @param id 进厂确认单ID
     * @return 结果
     */
    @Override
    public int deleteBaServiceVoucherById(String id)
    {
        return baServiceVoucherMapper.deleteBaServiceVoucherById(id);
    }

}
