package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaRenew;

/**
 * 市场动态更新Service接口
 *
 * @author ljb
 * @date 2023-03-22
 */
public interface IBaRenewService
{
    /**
     * 查询市场动态更新
     *
     * @param id 市场动态更新ID
     * @return 市场动态更新
     */
    public BaRenew selectBaRenewById(String id);

    /**
     * 查询市场动态更新列表
     *
     * @param baRenew 市场动态更新
     * @return 市场动态更新集合
     */
    public List<BaRenew> selectBaRenewList(BaRenew baRenew);

    /**
     * 新增市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    public int insertBaRenew(BaRenew baRenew);

    /**
     * 修改市场动态更新
     *
     * @param baRenew 市场动态更新
     * @return 结果
     */
    public int updateBaRenew(BaRenew baRenew);

    /**
     * 批量删除市场动态更新
     *
     * @param ids 需要删除的市场动态更新ID
     * @return 结果
     */
    public int deleteBaRenewByIds(String[] ids);

    /**
     * 删除市场动态更新信息
     *
     * @param id 市场动态更新ID
     * @return 结果
     */
    public int deleteBaRenewById(String id);


}
