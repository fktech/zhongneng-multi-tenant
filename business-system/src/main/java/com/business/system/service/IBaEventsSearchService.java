package com.business.system.service;

import com.business.system.domain.BaEventsSearch;
import com.business.system.domain.dto.SysEventsSearchDTO;

import java.util.List;

/**
 * 联动事件Service接口
 *
 * @author single
 * @date 2023-07-05
 */
public interface IBaEventsSearchService
{
    /**
     * 查询联动事件
     *
     * @param id 联动事件ID
     * @return 联动事件
     */
    public BaEventsSearch selectBaEventsSearchById(String id);

    /**
     * 查询联动事件列表
     *
     * @param baEventsSearch 联动事件
     * @return 联动事件集合
     */
    public List<BaEventsSearch> selectBaEventsSearchList(BaEventsSearch baEventsSearch);

    /**
     * 新增联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    public int insertBaEventsSearch(BaEventsSearch baEventsSearch);

    /**
     * 修改联动事件
     *
     * @param baEventsSearch 联动事件
     * @return 结果
     */
    public int updateBaEventsSearch(BaEventsSearch baEventsSearch);

    /**
     * 批量删除联动事件
     *
     * @param ids 需要删除的联动事件ID
     * @return 结果
     */
    public int deleteBaEventsSearchByIds(String[] ids);

    /**
     * 删除联动事件信息
     *
     * @param id 联动事件ID
     * @return 结果
     */
    public int deleteBaEventsSearchById(String id);

    /**
     * 获取联动事件列表
     * @param sysEventsSearchDTO
     * @return
     */
    public String eventsSearchList(SysEventsSearchDTO sysEventsSearchDTO);

    /**
     * 获取监控点在线状态
     * @param status
     * @param indexCodes
     * @return
     */
    public String cameraList(String status,String[] indexCodes);


}
