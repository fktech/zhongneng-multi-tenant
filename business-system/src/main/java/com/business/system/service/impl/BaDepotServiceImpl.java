package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaDepotService;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 仓库信息Service业务层处理
 *
 * @author ljb
 * @date 2022-11-30
 */
@Service
public class BaDepotServiceImpl extends ServiceImpl<BaDepotMapper, BaDepot> implements IBaDepotService {
    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;
    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;
    @Autowired
    private ISysUserService iSysUserService;
    @Value(value = "${user.login.lockTime}")
    private int lockTime;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;

    @Autowired
    private BaDepotMapMapper baDepotMapMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    private static final Logger log = LoggerFactory.getLogger(BaDeclareServiceImpl.class);

    @Autowired
    private BaDepotHeadMapper baDepotHeadMapper;

    /**
     * 查询仓库信息
     *
     * @param id 仓库信息ID
     * @return 仓库信息
     */
    @Override
    public BaDepot selectBaDepotById(String id) {
        BaDepot baDepot = baDepotMapper.selectBaDepotById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", baDepot.getId());
        queryWrapper.eq("flag", 0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baDepot.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baDepot.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位信息
        String name = getName.getName(sysUser.getUserId());
        if (name.equals("") == false) {
            sysUser.setPostName(name.substring(0, name.length() - 1));
        }
        baDepot.setUser(sysUser);

        //获取最新货位图
        BaDepotMap baDepotMap = new BaDepotMap();
        baDepotMap.setRelevanceId(baDepot.getId());
        List<BaDepotMap> baDepotMaps = baDepotMapMapper.selectBaDepotMapList(baDepotMap);
        if (!CollectionUtils.isEmpty(baDepotMaps)) {
            baDepot.setBaDepotMap(baDepotMaps.get(0));
        }
        //仓库名称
        if(StringUtils.isNotEmpty(baDepot.getCompany())){
            if(isInteger(baDepot.getCompany())){
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baDepot.getCompany()));
                if(StringUtils.isNotNull(dept)){
                    baDepot.setCompanyName(dept.getDeptName());
                }else {
                    baDepot.setCompanyName(baDepot.getCompany());
                }
            }else {
                baDepot.setCompanyName(baDepot.getCompany());
            }

        }
        //查询库位信息列表
        BaDepotLocation baDepotLocation = new BaDepotLocation();
        baDepotLocation.setRelevanceId(baDepot.getId());
        baDepot.setBaDepotLocationList(baDepotLocationMapper.selectBaDepotLocationList(baDepotLocation));
        return baDepot;
    }

    /**
     * 查询仓库信息列表
     *
     * @param baDepot 仓库信息
     * @return 仓库信息
     */
    @Override
    public List<BaDepot> selectBaDepotList(BaDepot baDepot) {
        List<BaDepot> baDepots = baDepotMapper.selectBaDepotList(baDepot);
        baDepots.stream().forEach(item ->{
            if(StringUtils.isNotEmpty(item.getCompany())){
                if(isInteger(item.getCompany())){
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(item.getCompany()));
                    if(StringUtils.isNotNull(dept)){
                        item.setCompanyName(dept.getDeptName());
                    }else {
                        item.setCompanyName(item.getCompany());
                    }
                }else {
                    item.setCompanyName(item.getCompany());
                }
            }
        });
        return baDepots;
    }

    /**
     * app查询仓库统计数据
     */
    @Override
    public DepotDTO selectBaDepotAppList(BaDepot baDepot) {
        //BaDepot baDepot=new BaDepot();
        baDepot.setState(AdminCodeEnum.DEPOT_STATUS_PASS.getCode());
        //租户ID
        baDepot.setTenantId(SecurityUtils.getCurrComId());
         DepotDTO depotDTO = new DepotDTO();
         //库存面积
        BigDecimal InventoryArea=new BigDecimal(0.00);
        //库存总量
        BigDecimal inventoryTotals=new BigDecimal(0.00);
        List<BaDepot> baDepots = baDepotMapper.selectBaDepotList(baDepot);
        //审核通过的仓库个数
        depotDTO.setDepotNumber(new Long(baDepots.size()));
        for (BaDepot baDepot1:baDepots){
            if(StringUtils.isNotNull(baDepot1.getDepotArea())){
                InventoryArea = InventoryArea.add(baDepot1.getDepotArea());
            }

            //根据仓库查询库存
            BaMaterialStock baMaterialStock=new BaMaterialStock();
            baMaterialStock.setDepotId(baDepot1.getId());
            List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectBaMaterialStockList(baMaterialStock);
            //单个库存总量
            BigDecimal oneArea=new BigDecimal(0.00);
            //库存货值
            BigDecimal depotCargoValue=new BigDecimal(0.00);
            for(BaMaterialStock baMaterialStock1:baMaterialStocks){
                if(StringUtils.isNotNull(baMaterialStock1.getInventoryTotal())){
                    oneArea=oneArea.add(baMaterialStock1.getInventoryTotal());
                    if(baMaterialStock1.getInventoryTotal() != null && baMaterialStock1.getInventoryAverage() != null){
                        depotCargoValue = depotCargoValue.add(baMaterialStock1.getInventoryTotal().multiply(baMaterialStock1.getInventoryAverage()));
                    }
                }
            }
            baDepot1.setDepotCargoValue(depotCargoValue);
            baDepot1.setInventoryTotal(oneArea);
            inventoryTotals=inventoryTotals.add(oneArea);
        }
        depotDTO.setDepotList(baDepots);
        depotDTO.setInventoryArea(InventoryArea);
        depotDTO.setInventoryTotals(inventoryTotals);
        return depotDTO;
    }

    /**
     * 新增仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    @Override
    public int insertBaDepot(BaDepot baDepot) {
        //判断仓库名称是否存在
        QueryWrapper<BaDepot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", baDepot.getName());
        queryWrapper.eq("flag", 0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaDepot baDepotResult = baDepotMapper.selectOne(queryWrapper);
        if (StringUtils.isNotNull(baDepotResult)) {
            return -1;
        }
        //判断库位编号是否存在
        List<BaDepotLocation> baDepotLocationList = baDepot.getBaDepotLocationList();
        //判断页面传过来的库位编号是否有重复
       /* List<BaDepotLocation> baDepotLocationCodeListBefore = baDepotLocationList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDepotLocation::getCode))), ArrayList::new));
        if (!CollectionUtils.isEmpty(baDepotLocationCodeListBefore) && !CollectionUtils.isEmpty(baDepotLocationList)) {
            if (baDepotLocationCodeListBefore.size() != baDepotLocationList.size()) {
                return -2;
            }
        }*/
        //判断库位编码是否已存在
        QueryWrapper<BaDepotLocation> baDepotLocationCodeQueryWrapper = null;
        BaDepotLocation depotLocation = null;
        if (!CollectionUtils.isEmpty(baDepotLocationList)) {
            for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                baDepotLocationCodeQueryWrapper = new QueryWrapper<BaDepotLocation>();
                baDepotLocationCodeQueryWrapper.eq("code", baDepotLocation.getCode());
                baDepotLocationCodeQueryWrapper.eq("flag", 0);
                depotLocation = baDepotLocationMapper.selectOne(baDepotLocationCodeQueryWrapper);
                if (!ObjectUtils.isEmpty(depotLocation)) {
                    return -2;
                }
            }
        }

        //判断页面传过来的库位名称是否有重复
        /*List<BaDepotLocation> baDepotLocationListNameBefore = baDepotLocationList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDepotLocation::getName))), ArrayList::new));
        if (!CollectionUtils.isEmpty(baDepotLocationListNameBefore) && !CollectionUtils.isEmpty(baDepotLocationList)) {
            if (baDepotLocationListNameBefore.size() != baDepotLocationList.size()) {
                return -3;
            }
        }*/

        //判断库位名称是否已存在
       /* QueryWrapper<BaDepotLocation> baDepotLocationNameQueryWrapper = null;
        if (!CollectionUtils.isEmpty(baDepotLocationList)) {
            for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                baDepotLocationNameQueryWrapper = new QueryWrapper<BaDepotLocation>();
                baDepotLocationNameQueryWrapper.eq("name", baDepotLocation.getName());
                baDepotLocationNameQueryWrapper.eq("flag", 0);
                depotLocation = baDepotLocationMapper.selectOne(baDepotLocationNameQueryWrapper);
                if (!ObjectUtils.isEmpty(depotLocation)) {
                    return -3;
                }
            }
        }*/

        //设置业务仓库的相关数据
        baDepot.setId(getRedisIncreID.getId());
        baDepot.setCreateTime(DateUtils.getNowDate());
        baDepot.setCreateBy(SecurityUtils.getUsername());
        baDepot.setUpdateTime(DateUtils.getNowDate());
        baDepot.setUpdateBy(SecurityUtils.getUsername());
        baDepot.setUserId(SecurityUtils.getUserId());
        baDepot.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            baDepot.setTenantId(SecurityUtils.getCurrComId());
        }

        //根据业务类型查询业务流程关联对象中的流程定义key
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode(),SecurityUtils.getCurrComId());
        baDepot.setFlowId(flowId);
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baDepot.getWorkState())) {
            baDepot.setWorkState("1");
            //默认审批流程已通过
            baDepot.setState(AdminCodeEnum.DEPOT_STATUS_PASS.getCode());
        }
        int i = baDepotMapper.insertBaDepot(baDepot);
        int baDepotLocationResult = 0;
        int baDepotLocationResultTemp = 0;
        //新增库位信息
        if (i > 0) {
            if (StringUtils.isNotNull(baDepotLocationList))
                for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                    baDepotLocation.setId(getRedisIncreID.getId());
                    baDepotLocation.setCreateTime(DateUtils.getNowDate());
                    baDepotLocation.setCreateBy(SecurityUtils.getUsername());
                    baDepotLocation.setRelevanceId(baDepot.getId()); //仓库ID
                    baDepotLocation.setUserId(SecurityUtils.getUserId());
                    baDepotLocation.setDeptId(SecurityUtils.getDeptId());
                    baDepotLocationResultTemp = baDepotLocationMapper.insertBaDepotLocation(baDepotLocation);
                    baDepotLocationResult = baDepotLocationResult + baDepotLocationResultTemp;
                }
        }
        //判断业务数据是否添加成功
        if (i > 0 && baDepotLocationResult > 0) {
            //判断业务归属公司是否是上海子公司
            if("2".equals(baDepot.getWorkState())) {
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDepot, AdminCodeEnum.DEPOT_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baDepotMapper.deleteBaDepotById(baDepot.getId());
                    return 0;
                } else {
                    BaDepot baDepot1 = baDepotMapper.selectBaDepotById(baDepot.getId());
                    baDepot1.setState(AdminCodeEnum.DEPOT_STATUS_VERIFYING.getCode());
                    baDepotMapper.updateBaDepot(baDepot1);
                }
            }
        }
        return 1;

    }

    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaDepot baDepot, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO processInstancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        //将流程id封装进流程实例启动对象
        processInstancesRuntimeDTO.setProcessDefinitionKey(baDepot.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode());
        processInstancesRuntimeDTO.setVariables(map);
        //业务流程实例关联对象
        BaProcessInstanceRelated related = new BaProcessInstanceRelated();
        related.setBusinessId(baDepot.getId());
        //业务类型
        related.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode());
        //获取流程实例关联的业务对象
        //BaDepot baDepot1 = baDepotMapper.selectBaDepotById(baDepot.getId());
        SysUser sysUser = iSysUserService.selectUserById(baDepot.getUserId());
        baDepot.setUserName(sysUser.getUserName());
        baDepot.setDeptName(sysUser.getDept().getDeptName());
        related.setBusinessData(GsonUtil.toJsonStringValue(baDepot));
        related.setApproveType(taskContent);
        processInstancesRuntimeDTO.setBaProcessInstanceRelated(related);
        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(processInstancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;

    }

    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO instancesRuntimeDTO) {
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(instancesRuntimeDTO.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(instancesRuntimeDTO.getVariables())));
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if (!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)) {
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if (data != null) {
            baProcessInstanceRelated = instancesRuntimeDTO.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if (instancesRuntimeDTO.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.DEPOT_APPROVAL_CONTENT_UPDATE.getCode())) {
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            } else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(instancesRuntimeDTO.getBaProcessInstanceRelated().getApproveType());
        }

        // 保存业务数据与流程实例关系
        if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }


    /**
     * 修改仓库信息
     *
     * @param baDepot 仓库信息
     * @return 结果
     */
    @Override
    public int updateBaDepot(BaDepot baDepot) {
        //判断仓库名称是否存在
        QueryWrapper<BaDepot> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", baDepot.getName());
        queryWrapper.eq("flag", 0);
        BaDepot baDepotResult = baDepotMapper.selectOne(queryWrapper);
        if (StringUtils.isNotNull(baDepotResult)) {
            if(!baDepot.getId().equals(baDepotResult.getId())){
                return -1;
            }
        }
        //判断库位编号是否存在
        List<BaDepotLocation> baDepotLocationList = baDepot.getBaDepotLocationList();
        //判断页面传过来的库位编号是否有重复
        /*List<BaDepotLocation> baDepotLocationCodeListBefore = baDepotLocationList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDepotLocation::getCode))), ArrayList::new));
        if (!CollectionUtils.isEmpty(baDepotLocationCodeListBefore) && !CollectionUtils.isEmpty(baDepotLocationList)) {
            if (baDepotLocationCodeListBefore.size() != baDepotLocationList.size()) {
                return -2;
            }
        }*/
        //判断库位编码是否已存在
        QueryWrapper<BaDepotLocation> baDepotLocationCodeQueryWrapper = null;
        BaDepotLocation depotLocation = null;
        if (!CollectionUtils.isEmpty(baDepotLocationList)) {
            for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                baDepotLocationCodeQueryWrapper = new QueryWrapper<BaDepotLocation>();
                baDepotLocationCodeQueryWrapper.eq("code", baDepotLocation.getCode());
                baDepotLocationCodeQueryWrapper.eq("flag", 0);
                depotLocation = baDepotLocationMapper.selectOne(baDepotLocationCodeQueryWrapper);
                if(!ObjectUtils.isEmpty(depotLocation) && !depotLocation.getId().equals(baDepotLocation.getId())){
                    if (!ObjectUtils.isEmpty(depotLocation) && depotLocation.getCode().equals(baDepotLocation.getCode())) {
                        return -2;
                    }
                }

            }
        }

        //判断页面传过来的库位名称是否有重复
       /* List<BaDepotLocation> baDepotLocationListNameBefore = baDepotLocationList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BaDepotLocation::getName))), ArrayList::new));
        if (!CollectionUtils.isEmpty(baDepotLocationListNameBefore) && !CollectionUtils.isEmpty(baDepotLocationList)) {
            if (baDepotLocationListNameBefore.size() != baDepotLocationList.size()) {
                return -3;
            }
        }*/

        //判断库位名称是否已存在
        /*QueryWrapper<BaDepotLocation> baDepotLocationNameQueryWrapper = null;
        if (!CollectionUtils.isEmpty(baDepotLocationList)) {
            for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                baDepotLocationNameQueryWrapper = new QueryWrapper<BaDepotLocation>();
                baDepotLocationNameQueryWrapper.eq("name", baDepotLocation.getName());
                baDepotLocationNameQueryWrapper.eq("flag", 0);
                depotLocation = baDepotLocationMapper.selectOne(baDepotLocationNameQueryWrapper);
                if(!ObjectUtils.isEmpty(depotLocation) && !depotLocation.getId().equals(baDepotLocation.getId())){
                    if (!ObjectUtils.isEmpty(depotLocation) && depotLocation.getName().equals(baDepotLocation.getName())) {
                        return -3;
                    }
                }
            }
        }*/

        baDepot.setUpdateTime(DateUtils.getNowDate());
        baDepot.setUpdateBy(SecurityUtils.getUsername());
        int i = baDepotMapper.updateBaDepot(baDepot);
        int baDepotLocationResult = 0;
        int baDepotLocationResultTemp = 0;
        if (i > 0) {
            if (StringUtils.isNotNull(baDepotLocationList)) {
                //删除仓库库位信息
                baDepotLocationMapper.deleteBaDepotLocationByRelevanceId(baDepot.getId());
                for (BaDepotLocation baDepotLocation : baDepotLocationList) {
                    if(StringUtils.isEmpty(baDepotLocation.getId())){
                        baDepotLocation.setId(getRedisIncreID.getId());
                    }
                    baDepotLocation.setUserId(SecurityUtils.getUserId());
                    baDepotLocation.setDeptId(SecurityUtils.getDeptId());
                    baDepotLocation.setCreateBy(SecurityUtils.getUsername());
                    baDepotLocation.setCreateTime(DateUtils.getNowDate());
                    baDepotLocation.setUserId(SecurityUtils.getUserId());
                    baDepotLocation.setDeptId(SecurityUtils.getDeptId());
                    baDepotLocationResultTemp = baDepotLocationResultTemp + baDepotLocationMapper.insertBaDepotLocation(baDepotLocation);
                    baDepotLocationResult = baDepotLocationResult + baDepotLocationResultTemp;
                }
            }
            if (i > 0 && baDepotLocationResult > 0 && !baDepot.getState().equals(AdminCodeEnum.DEPOT_STATUS_PASS.getCode())) {
                BaDepot depot = baDepotMapper.selectBaDepotById(baDepot.getId());
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> baProcessInstanceRelatedQueryWrapper = new QueryWrapper<>();
                baProcessInstanceRelatedQueryWrapper.eq("business_id",depot.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(baProcessInstanceRelatedQueryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baDepot, AdminCodeEnum.DEPOT_APPROVAL_CONTENT_UPDATE.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    return 0;
                } else {
                    baDepot.setState(AdminCodeEnum.DEPOT_STATUS_VERIFYING.getCode());
                    baDepot.setCreateTime(baDepot.getUpdateTime());
                    baDepotMapper.updateBaDepot(baDepot);
                }
            }
        }
        return i;
    }

    /**
     * 批量删除仓库信息
     *
     * @param ids 需要删除的仓库信息ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteBaDepotByIds(String[] ids) {
        if (StringUtils.isNotEmpty(ids)) {
            QueryWrapper<BaDepotLocation> baDepotLocationCodeQueryWrapper = null;
            QueryWrapper<BaDepotMap> baDepotMapQueryWrapper = null;
            for (String id : ids) {
                BaDepot baDepot = baDepotMapper.selectBaDepotById(id);
                baDepot.setFlag(1);
                baDepotMapper.updateBaDepot(baDepot);

                //删除库位
                baDepotLocationCodeQueryWrapper = new QueryWrapper<BaDepotLocation>();
                baDepotLocationCodeQueryWrapper.eq("relevance_id", baDepot.getId());
                baDepotLocationCodeQueryWrapper.eq("flag", 0);
                List<BaDepotLocation> baDepotLocations = baDepotLocationMapper.selectList(baDepotLocationCodeQueryWrapper);
                if (!CollectionUtils.isEmpty(baDepotLocations)) {
                    for (BaDepotLocation baDepotLocation : baDepotLocations) {
                        baDepotLocation.setFlag(1);
                        baDepotLocationMapper.updateBaDepotLocation(baDepotLocation);
                    }
                }
                //删除库位图
                baDepotMapQueryWrapper = new QueryWrapper<BaDepotMap>();
                baDepotMapQueryWrapper.eq("relevance_id", baDepot.getId());
                baDepotMapQueryWrapper.eq("flag", 0);
                List<BaDepotMap> baDepotMaps = baDepotMapMapper.selectList(baDepotMapQueryWrapper);
                if (!CollectionUtils.isEmpty(baDepotMaps)) {
                    for (BaDepotMap baDepotMap : baDepotMaps) {
                        baDepotMap.setFlag(1);
                        baDepotMapMapper.updateBaDepotMap(baDepotMap);
                    }
                }

                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        } else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除仓库信息信息
     *
     * @param id 仓库信息ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotById(String id) {
        return baDepotMapper.deleteBaDepotById(id);
    }

    /**
     * 撤销
     *
     * @param id
     * @return
     */
    @Override
    public int revoke(String id) {
        //判断id不为空
        if (StringUtils.isNotEmpty(id)) {
            BaDepot baDepot = baDepotMapper.selectBaDepotById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", baDepot.getId());
            queryWrapper.eq("flag", 0);
            queryWrapper.ne("approve_result", AdminCodeEnum.DEPOT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baDepot.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result = workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if (result.get("code").toString().equals("200")) {
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baDepot.setState(AdminCodeEnum.DEPOT_STATUS_WITHDRAW.getCode());
                baDepotMapper.updateBaDepot(baDepot);
                String userId = String.valueOf(baDepot.getUserId());
                //给所有已审批的用户发消息
                if (!CollectionUtils.isEmpty(baProcessInstanceRelateds)) {
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if (!ObjectUtils.isEmpty(baProcessInstanceRelated)) {
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if (!ObjectUtils.isEmpty(ajaxResult)) {
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for (LinkedHashMap map : data) {
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                                        if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                            this.insertMessage(baDepot, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            } else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int editNoProcess(BaDepot baDepot) {
        baDepot.setUpdateTime(DateUtils.getNowDate());
        baDepot.setUpdateBy(SecurityUtils.getUsername());
        return baDepotMapper.updateBaDepot(baDepot);
    }

    @Override
    public int deleteJudge(String locationId) {
        //查询仓位是否存在出入库信息
        QueryWrapper<BaDepotHead> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("location_id",locationId);
        queryWrapper.eq("flag",0);
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.selectList(queryWrapper);
        if(baDepotHeads.size() > 0){
            return 0;
        }
        //查询仓位库存信息
        QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
        stockQueryWrapper.eq("location_id",locationId);
        List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectList(stockQueryWrapper);
        if(baMaterialStocks.size() > 0){
            return 0;
        }
        return 1;
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(BaDepot baDepot, String mId, String msgContent) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baDepot.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_DEPOT.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }
}
