package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaHiTransportService;
import com.business.system.service.IBaQualityReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 运输管理审批接口 服务类
 * @date: 2022/12/14 16:9
 */
@Service
public class ITransportApprovalService {

    @Autowired
    protected BaTransportMapper baTransportMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    protected BaProjectMapper baProjectMapper;

    @Autowired
    protected BaOrderMapper baOrderMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private IBaQualityReportService qualityReportService;

    @Autowired
    private IBaHiTransportService baHiTransportService;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaTransport baTransport = new BaTransport();
        baTransport.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baTransport = baTransportMapper.selectBaTransportById(baTransport.getId());
        baTransport.setState(status);
        String userId = String.valueOf(baTransport.getUserId());

        if(AdminCodeEnum.TRANSPORT_STATUS_PASS.getCode().equals(status)) {
            if(!"2".equals(baTransport.getApproveFlag())) {
                BaOrder baOrder = baOrderMapper.selectBaOrderById(baTransport.getOrderId());
                if (!ObjectUtils.isEmpty(baOrder)) {
                    BaContract baContract = baContractMapper.selectBaContractById(baOrder.getCompanyId());//查询合同
                    if (!ObjectUtils.isEmpty(baContract)) {
                        BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
                        //更新项目阶段
                        baProject.setProjectStage(7); //订单发运
                        baProjectMapper.updateBaProject(baProject);
                    }
                }
            } else {
            //变更状态
                BaHiTransport baHiTransport = baHiTransportService.selectBaHiTransportById(baTransport.getId());
                //当前发货
                if (StringUtils.isNotNull(baHiTransport.getDelivery())) {
                    BaCheck baCheck = baHiTransport.getDelivery();
                    //过滤无车承运
                    if (baTransport.getTransportType().equals("4") == false) {
                        if (StringUtils.isNotEmpty(baCheck.getId())) {
                            baCheck.setRelationId(baTransport.getId());
                            baCheckMapper.updateBaCheck(baCheck);
                        }
                    }
                    //删除原数据
                    qualityReportService.deleteBaQualityReportById(baTransport.getId());

                    //数质量报告
                    BaQualityReport baQualityReport = new BaQualityReport();
                    if(!StringUtils.isNotEmpty(baHiTransport.getId())) {
                        baQualityReport.setTransportId(baHiTransport.getId());
                        baQualityReport.setStarId(baHiTransport.getStartId());
                        List<BaQualityReport> baQualityReports = qualityReportService.selectBaQualityReportList(baQualityReport);
                        if (!CollectionUtils.isEmpty(baQualityReports)) {
                            for (BaQualityReport baQualityReport1 : baQualityReports) {
                                baQualityReport1.setTransportId(baTransport.getId());
                                qualityReportService.insertBaQualityReport(baQualityReport);
                            }
                        }
                    }
                }

                //当前验收
                if (StringUtils.isNotNull(baHiTransport.getReceipt())) {
                    BaCheck baCheck = baTransport.getReceipt();
                    if (StringUtils.isNotEmpty(baCheck.getId())) {
                        baCheck.setRelationId(baTransport.getId());
                        baCheckMapper.updateBaCheck(baCheck);
                    }
                }

                //全局编号
                //全局编号关系表新增数据
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baTransport.getId());
                baGlobalNumber.setCode(baTransport.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);

                BaTransport baTransport1 = new BaTransport();
                BeanUtils.copyBeanProp(baTransport1, baHiTransport);
                baTransport1.setApproveFlag("0");
                baTransport1.setId(baTransport.getId());
                baTransportMapper.updateBaTransport(baTransport1);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baTransport, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.TRANSPORT_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baTransport, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baTransport, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baTransportMapper.updateById(baTransport);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaTransport baTransport, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baTransport.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_TRANSPORT.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaTransport checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaTransport baTransport = baTransportMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baTransport == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_TRANSPORT_001);
        }
        if (!AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode().equals(baTransport.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_TRANSPORT_002);
        }
        return baTransport;
    }
}
