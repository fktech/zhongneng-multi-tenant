package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaDepotInventory;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.dto.DepotHeadDTO;
import com.business.system.domain.vo.BaDepotHeadStatVO;

/**
 * 出入库记录Service接口
 *
 * @author single
 * @date 2023-01-05
 */
public interface IBaDepotHeadService
{
    /**
     * 查询出入库记录
     *
     * @param id 出入库记录ID
     * @return 出入库记录
     */
    public BaDepotHead selectBaDepotHeadById(String id);

    /**
     * 查询出入库记录列表
     *
     * @param baDepotHead 出入库记录
     * @return 出入库记录集合
     */
    public List<BaDepotHead> selectBaDepotHeadList(BaDepotHead baDepotHead);

    public List<BaDepotHead> selectBaDepotHead(BaDepotHead baDepotHead);

    /**
     * 新增入库记录
     *
     * @param baDepotHead 入库记录
     * @return 结果
     */
    public int insertBaDepotHead(BaDepotHead baDepotHead);

    /**
     * 新增出库记录
     * @param baDepotHead
     * @return
     */
    public int insertRetrieval(BaDepotHead baDepotHead);


    /**
     * 修改出入库记录
     *
     * @param baDepotHead 出入库记录
     * @return 结果
     */
    public int updateBaDepotHead(BaDepotHead baDepotHead);

    /**
     * 修改入库记录
     *
     * @param baDepotHead 入库记录
     * @return 结果
     */
    public int updateDepotHead(BaDepotHead baDepotHead);

    /**
     * 批量删除出入库记录
     *
     * @param ids 需要删除的出入库记录ID
     * @return 结果
     */
    public int deleteBaDepotHeadByIds(String[] ids);

    /**
     * 删除出入库记录信息
     *
     * @param id 出入库记录ID
     * @return 结果
     */
    public int deleteBaDepotHeadById(String id);

    public int inspection(BaDepotHead baDepotHead);

    /**
     * 入库撤销
     */
    Integer intoRevoke(String id);

    /**
     * 出库撤销
     */
    Integer outputRevoke(String id);

    /**
     * 出库按钮
     */
    public Integer output(BaDepotHead baDepotHead);

    /**
     * 加工信息延期预警
     */
    public List<DepotHeadDTO> outputBaDepotHead(DepotHeadDTO depotHeadDTO);

    /**
     * 加工信息损耗预警
     */
    public List<DepotHeadDTO> intoBaDepotHead(DepotHeadDTO depotHeadDTO);

    /**
     * 入库清单
     */
    public int inventory(BaDepotHead baDepotHead);

    /**
     * 统计出入库
     */
    public int countBaDepotHeadList(BaDepotHead baDepotHead);

    /**
     * 出入库明细
     * @param baDepotHead
     * @return
     */
    public List<BaDepotHead> depotHeadDetail(BaDepotHead baDepotHead);

    /**
     * 智慧驾驶舱出入库记录
     *
     * @param baDepotInventory 入库清单
     * @return 智慧驾驶舱出入库记录
     */
    public List<BaDepotInventory> selectBaDepotInventory(BaDepotInventory baDepotInventory);

    /**
     * 今日入库、出库量统计
     */
    public BaDepotHeadStatVO depotHeadStat(DepotHeadDTO depotHeadDTO);

    /**
     * 查询多租户授权信息出入库列表
     */
    public List<BaDepotHead> selectBaDepotHeadAuthorizedList(BaDepotHead baDepotHead);

    public int authorizedBaDepotHead(BaDepotHead baDepotHead);
}
