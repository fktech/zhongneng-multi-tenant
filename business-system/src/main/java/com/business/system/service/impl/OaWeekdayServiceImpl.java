package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaWeekdayMapper;
import com.business.system.domain.OaWeekday;
import com.business.system.service.IOaWeekdayService;

/**
 * 工作日Service业务层处理
 *
 * @author single
 * @date 2023-11-24
 */
@Service
public class OaWeekdayServiceImpl extends ServiceImpl<OaWeekdayMapper, OaWeekday> implements IOaWeekdayService
{
    @Autowired
    private OaWeekdayMapper oaWeekdayMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询工作日
     *
     * @param id 工作日ID
     * @return 工作日
     */
    @Override
    public OaWeekday selectOaWeekdayById(String id)
    {
        return oaWeekdayMapper.selectOaWeekdayById(id);
    }

    /**
     * 查询工作日列表
     *
     * @param oaWeekday 工作日
     * @return 工作日
     */
    @Override
    public List<OaWeekday> selectOaWeekdayList(OaWeekday oaWeekday)
    {
        return oaWeekdayMapper.selectOaWeekdayList(oaWeekday);
    }

    /**
     * 新增工作日
     *
     * @param oaWeekday 工作日
     * @return 结果
     */
    @Override
    public int insertOaWeekday(OaWeekday oaWeekday)
    {
        oaWeekday.setId(getRedisIncreID.getId());
        oaWeekday.setCreateTime(DateUtils.getNowDate());
        return oaWeekdayMapper.insertOaWeekday(oaWeekday);
    }

    /**
     * 修改工作日
     *
     * @param oaWeekday 工作日
     * @return 结果
     */
    @Override
    public int updateOaWeekday(OaWeekday oaWeekday)
    {
        oaWeekday.setUpdateTime(DateUtils.getNowDate());
        return oaWeekdayMapper.updateOaWeekday(oaWeekday);
    }

    /**
     * 批量删除工作日
     *
     * @param ids 需要删除的工作日ID
     * @return 结果
     */
    @Override
    public int deleteOaWeekdayByIds(String[] ids)
    {
        return oaWeekdayMapper.deleteOaWeekdayByIds(ids);
    }

    /**
     * 删除工作日信息
     *
     * @param id 工作日ID
     * @return 结果
     */
    @Override
    public int deleteOaWeekdayById(String id)
    {
        return oaWeekdayMapper.deleteOaWeekdayById(id);
    }

}
