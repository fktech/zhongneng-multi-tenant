package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaMaintenanceExpansion;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEgressApprovalService;
import com.business.system.service.workflow.IOaMaintenanceExpansionApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务维护/拓展申请审批结果:审批通过
 */
@Service("oaMaintenanceExpansionApprovalContent:processApproveResult:pass")
@Slf4j
public class OaMaintenanceExpansionApprovalAgreeServiceImpl extends IOaMaintenanceExpansionApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaMaintenanceExpansion oaMaintenanceExpansion = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_PASS.getCode());
        return result;
    }
}
