package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.BaSupplier;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IEnterpriseApprovalService;
import com.business.system.service.workflow.ISupplierApprovalService;
import org.springframework.stereotype.Service;


/**
 * 终端企业管理申请审批结果:审批拒绝
 */
@Service("enterpriseApprovalContent:processApproveResult:reject")
public class EnterpriseApprovalRejectServiceImpl extends IEnterpriseApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ENTERPRISE_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaEnterprise checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}