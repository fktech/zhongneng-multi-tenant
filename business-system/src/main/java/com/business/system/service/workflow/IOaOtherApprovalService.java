package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.OaClaimRelation;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 其他申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaOtherApprovalService {

    @Autowired
    protected OaOtherMapper oaOtherMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OaClaimRelationMapper oaClaimRelationMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaOther oaOther = new OaOther();
        oaOther.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaOther = oaOtherMapper.selectOaOtherById(oaOther.getId());
        oaOther.setState(status);
        String userId = String.valueOf(oaOther.getUserId());
        if(AdminCodeEnum.OAOTHER_STATUS_PASS.getCode().equals(status)){
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            OaOther oaOther1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), OaOther.class);
            //更新项目阶段
            oaOther1.setState(status);
            oaOtherMapper.updateById(oaOther1);

            OaClaimRelation oaClaimRelation = new OaClaimRelation();
            oaClaimRelation.setId(getRedisIncreID.getId());
            oaClaimRelation.setClaimState(1); //未报销
            oaClaimRelation.setCreateTime(DateUtils.getNowDate());
            oaClaimRelation.setCreateBy(String.valueOf(oaOther1.getUserId()));
            oaClaimRelation.setUserId(oaOther1.getUserId());
            oaClaimRelation.setDeptId(oaOther1.getDeptId());
            oaClaimRelation.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_OTHER.getCode()); //招待申请
            oaClaimRelation.setApplyTime(oaOther1.getCreateTime()); //申请时间
            oaClaimRelation.setApplyAmount(new BigDecimal(oaOther1.getSpendAmount()));
            oaClaimRelation.setBusinessId(oaOther1.getId());
            oaClaimRelation.setInvoice(oaOther1.getInvoice());
            oaClaimRelation.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            //申请人名称
            SysUser sysUser = sysUserMapper.selectUserById(oaOther1.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                oaClaimRelation.setProcessName(sysUser.getNickName() + "的" + AdminCodeEnum.TASK_TYPE_OA_OTHER.getDescription());
            }
            oaClaimRelation.setRelationId(oaClaimRelation.getId());
            oaClaimRelationMapper.insertOaClaimRelation(oaClaimRelation);

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaOther, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_OTHER.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OAOTHER_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaOther, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_OTHER.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaOther, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_OTHER.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            oaOtherMapper.updateById(oaOther);
        }
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaOther oaOther, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaOther.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_OTHER.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"其他申请审批提醒",msgContent,"oa",oaOther.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaOther checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaOther oaOther = oaOtherMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaOther == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAOTHER_001);
        }
        if (!AdminCodeEnum.OAOTHER_STATUS_VERIFYING.getCode().equals(oaOther.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAOTHER_002);
        }
        return oaOther;
    }
}
