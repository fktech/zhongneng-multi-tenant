package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IProjectApprovalService;
import com.business.system.service.workflow.IProjectBeforehandApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 预立项申请审批结果:审批通过
 */
@Service("projectBeforehandApprovalContent:processApproveResult:pass")
@Slf4j
public class ProjectBeforehandApprovalAgreeServiceImpl extends IProjectBeforehandApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.PROJECTBEFOREHAND_STATUS_PASS.getCode());
        return result;
    }
}
