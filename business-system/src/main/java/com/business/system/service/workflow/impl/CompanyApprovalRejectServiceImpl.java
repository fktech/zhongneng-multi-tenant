package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaCompany;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.ICompanyApprovalService;
import org.springframework.stereotype.Service;


/**
 * 公司管理申请审批结果:审批拒绝
 */
@Service("companyApprovalContent:processApproveResult:reject")
public class CompanyApprovalRejectServiceImpl extends ICompanyApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.COMPANY_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaCompany checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}