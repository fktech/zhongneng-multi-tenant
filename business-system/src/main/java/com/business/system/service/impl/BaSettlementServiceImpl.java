package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 结算信息Service业务层处理
 *
 * @author ljb
 * @date 2022-12-26
 */
@Service
public class BaSettlementServiceImpl extends ServiceImpl<BaSettlementMapper, BaSettlement> implements IBaSettlementService
{
    private static final Logger log = LoggerFactory.getLogger(BaSettlementServiceImpl.class);
    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private IBaOrderService baOrderService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private IBaContractService baContractService;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private IBaPaymentService baPaymentService;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private IBaCollectionService baCollectionService;

    @Autowired
    private BaInvoiceMapper baInvoiceMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;



    /**
     * 查询结算信息
     *
     * @param id 结算信息ID
     * @return 结算信息
     */
    @Override
    public BaSettlement selectBaSettlementById(String id)
    {
        BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);
        if(StringUtils.isNotEmpty(baSettlement.getContractId())){
            //关联合同启动信息
          BaContractStart baContract =  baContractStartMapper.selectBaContractStartById(baSettlement.getContractId());
          baSettlement.setContractName(baContract.getName());
          if(StringUtils.isNotEmpty(baContract.getProjectId())){
              //项目类型
             BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
             baSettlement.setBusinessType(baProject.getBusinessType());
          }
        }
        //结算方
        if(StringUtils.isNotEmpty(baSettlement.getSettlementParty())){
            if(baSettlement.getSettlementType() == 1){
                //供应商名称
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baSettlement.getSettlementParty());
                baSettlement.setSettlementPartyName(baSupplier.getName());
            }else if(baSettlement.getSettlementType() == 2){
                //用煤单位
                BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baSettlement.getSettlementParty());
                baSettlement.setSettlementPartyName(baEnterprise.getName());
            }else if(baSettlement.getSettlementType() == 3 || baSettlement.getSettlementType() == 4){
                //公司名称
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baSettlement.getSettlementParty());
                baSettlement.setSettlementPartyName(baCompany.getName());
            }
        }

        if(baSettlement.getSettlementType() == 2){
            //收款信息
            QueryWrapper<BaCollection> collectionQueryWrapper = new QueryWrapper<>();
            collectionQueryWrapper.eq("settlement_id",baSettlement.getId());
            collectionQueryWrapper.eq("flag",0);
            List<BaCollection> baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
            for (BaCollection baCollection:baCollectionList) {

                List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
                //发起人
                baCollection.setUserName(sysUserMapper.selectUserById(baCollection.getUserId()).getNickName());
                //订单历史信息
                QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("settlement_id",id);
                queryWrapper.eq("relation_id",baCollection.getId());
                List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper);
                for (BaSettlementDetail baSettlementDetail:baSettlementDetailList) {
                    //订单信息
                    BaOrderDTO baOrderDTO = new BaOrderDTO();
                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("settlement_id",baSettlement.getId());
                    queryWrapper.eq("order_id",baSettlementDetail.getOrderId());
                    queryWrapper.isNull("relation_id");
                    BaSettlementDetail baSettlementDetail1 = baSettlementDetailMapper.selectOne(queryWrapper);
                    BaOrder baOrder = baOrderMapper.selectBaOrderById(baSettlementDetail.getOrderId());
                    baOrderDTO = baOrderDTOList(baOrder, baSettlementDetail.getAmount());
                    baOrderDTO.setActualPayment(baSettlementDetail.getAmount());
                    baOrderDTO.setCurrentCollection(baSettlementDetail1.getAmount());
                    baOrderDTO.setDetailId(baSettlementDetail.getId());
                    //先查出付款的历史信息计算累计实收金额
                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("settlement_id", id);
                    //queryWrapper.eq("relation_id",baPayment.getId());
                    queryWrapper.eq("type", "2");
                    queryWrapper.eq("order_id", baOrderDTO.getId());
                    //定义累计金额
                    BigDecimal amountPaid = new BigDecimal(0);
                    List<BaSettlementDetail> baSettlementDetailList1 = baSettlementDetailMapper.selectList(queryWrapper);
                    if (StringUtils.isNotNull(baSettlementDetailList1)) {
                        for (BaSettlementDetail baSettlementDetail2 : baSettlementDetailList1) {
                            //累计付款历史信息
                            amountPaid = amountPaid.add(baSettlementDetail2.getAmount());
                        }
                        baOrderDTO.setAmountPaid(amountPaid);
                    } else {
                        baOrderDTO.setAmountPaid(amountPaid);
                    }
                    //已收金额
                    baOrderDTOList.add(baOrderDTO);
                }
                baCollection.setBaOrderDTOs(baOrderDTOList);
            }

            baSettlement.setCollectionList(baCollectionList);
            //已收金额
            BigDecimal decimal = new BigDecimal(0);
            if(StringUtils.isNotNull(baCollectionList)){
                for (BaCollection baCollection:baCollectionList) {
                    if(baCollection.getCollectionState().equals(AdminCodeEnum.COLLECTION_RECEIVED.getCode())){
                        decimal = decimal.add(baCollection.getActualCollectionAmount());
                    }
                }
                baSettlement.setDecimal(decimal);
            }else {
                baSettlement.setDecimal(new BigDecimal(0));
            }
        }else if(baSettlement.getSettlementType() == 1 || baSettlement.getSettlementType() == 3){
            //付款信息
            QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
            paymentQueryWrapper.eq("settlement_id",baSettlement.getId());
            paymentQueryWrapper.eq("flag",0);
            List<BaPayment> list = baPaymentMapper.selectList(paymentQueryWrapper);
            for (BaPayment baPayment:list) {
                List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
                //发起人
                baPayment.setUserName(sysUserMapper.selectUserById(baPayment.getUserId()).getNickName());
                //订单历史信息
                QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("settlement_id",id);
                queryWrapper.eq("relation_id",baPayment.getId());
                List<BaSettlementDetail> baSettlementDetailList1 = baSettlementDetailMapper.selectList(queryWrapper);
                for (BaSettlementDetail baSettlementDetail:baSettlementDetailList1) {
                    //订单信息
                    BaOrderDTO baOrderDTO = new BaOrderDTO();
                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("settlement_id",baSettlement.getId());
                    queryWrapper.eq("order_id",baSettlementDetail.getOrderId());
                    queryWrapper.isNull("relation_id");
                    BaSettlementDetail baSettlementDetail1 = baSettlementDetailMapper.selectOne(queryWrapper);
                    BaOrder baOrder = baOrderMapper.selectBaOrderById(baSettlementDetail.getOrderId());
                    baOrderDTO = baOrderDTOList(baOrder, baSettlementDetail.getAmount());
                    baOrderDTO.setCurrentCollection(baSettlementDetail1.getAmount());
                    baOrderDTO.setActualPayment(baSettlementDetail.getAmount());
                    baOrderDTO.setDetailId(baSettlementDetail.getId());
                    //先查出付款的历史信息计算累计实付金额
                    queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("settlement_id", id);
                    //queryWrapper.eq("relation_id",baPayment.getId());
                    queryWrapper.eq("type", "2");
                    queryWrapper.eq("order_id", baOrderDTO.getId());
                    BigDecimal amountPaid = new BigDecimal(0);
                    List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper);
                    if (StringUtils.isNotNull(baSettlementDetailList)) {
                        for (BaSettlementDetail baSettlementDetail2 : baSettlementDetailList) {
                            //累计付款历史信息
                            amountPaid = amountPaid.add(baSettlementDetail2.getAmount());
                        }
                        baOrderDTO.setAmountPaid(amountPaid);
                    } else {
                        baOrderDTO.setAmountPaid(amountPaid);
                    }
                    //已付金额
                    baOrderDTOList.add(baOrderDTO);
                }
                baPayment.setBaOrderDTOs(baOrderDTOList);
            }
            baSettlement.setPaymentList(list);

            //已付款金额
            BigDecimal decimal = new BigDecimal(0);
            if(StringUtils.isNotNull(list)){
                for (BaPayment baPayment:list) {
                    if(baPayment.getPaymentState().equals(AdminCodeEnum.PAYMENT_PAID.getCode())){
                        decimal = decimal.add(baPayment.getActualPaymentAmount());
                    }
                }
                baSettlement.setDecimal(decimal);
            }else {
                baSettlement.setDecimal(new BigDecimal(0));
            }
        }
        //结算订单的历史数据
        QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("settlement_id",baSettlement.getId());
        queryWrapper.eq("type","1");
        List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper);
        List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
        for (BaSettlementDetail baSettlementDetail:baSettlementDetails) {
            BaOrder baOrder = baOrderMapper.selectBaOrderById(baSettlementDetail.getOrderId());
            //查询订单对应的信息
            BaOrderDTO baOrderDTO = baOrderDTOList(baOrder, baSettlementDetail.getAmount());
            //订单已付金额
            if(baSettlement.getSettlementType() == 1) {
                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                paymentQueryWrapper.eq("settlement_id", baSettlement.getId());
                paymentQueryWrapper.eq("flag", 0);
                List<BaPayment> list = baPaymentMapper.selectList(paymentQueryWrapper);
                //判断是否有付款信息
                if(list.size() > 0){
                    //订单已付金额
                    BigDecimal amountPaid = new BigDecimal(0);
                    for (BaPayment baPayment : list) {
                        //先查出付款的历史信息
                        QueryWrapper<BaSettlementDetail> queryWrapper1 = new QueryWrapper<>();
                        queryWrapper1.eq("settlement_id", id);
                        queryWrapper1.eq("relation_id",baPayment.getId());
                        queryWrapper1.eq("type", "2");
                        queryWrapper1.eq("order_id", baOrderDTO.getId());
                        List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper1);
                        if (StringUtils.isNotNull(baSettlementDetailList)) {
                            for (BaSettlementDetail baSettlementDetail1 : baSettlementDetailList) {
                                //累计付款历史信息
                                amountPaid = amountPaid.add(baSettlementDetail1.getAmount());
                            }
                            baOrderDTO.setAmountPaid(amountPaid);
                        } else {
                            baOrderDTO.setAmountPaid(amountPaid);
                        }
                    }
                }
            }else if(baSettlement.getSettlementType() == 2){
                QueryWrapper<BaCollection> collectionQueryWrapper = new QueryWrapper<>();
                collectionQueryWrapper.eq("settlement_id",baSettlement.getId());
                collectionQueryWrapper.eq("flag",0);
                List<BaCollection> baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
                //判断是否有收款信息
                if(baCollectionList.size() > 0){
                    //订单已收金额
                    BigDecimal amountPaid = new BigDecimal(0);
                    for (BaCollection baCollection:baCollectionList) {
                        //先查出收款的历史信息
                        QueryWrapper<BaSettlementDetail> queryWrapper1 = new QueryWrapper<>();
                        queryWrapper1.eq("settlement_id", id);
                        queryWrapper1.eq("relation_id",baCollection.getId());
                        queryWrapper1.eq("type", "2");
                        queryWrapper1.eq("order_id", baOrderDTO.getId());
                        List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper1);
                        if (StringUtils.isNotNull(baSettlementDetailList)) {
                            for (BaSettlementDetail baSettlementDetail1 : baSettlementDetailList) {
                                //累计收款历史信息
                                amountPaid = amountPaid.add(baSettlementDetail1.getAmount());
                            }
                            baOrderDTO.setAmountPaid(amountPaid);
                        } else {
                            baOrderDTO.setAmountPaid(amountPaid);
                        }
                    }
                }
            }
            baOrderDTOList.add(baOrderDTO);
        }
        baSettlement.setBaOrderDTOs(baOrderDTOList);
        return baSettlement;
    }

    @Override
    public BaSettlement selectSettlementById(String id) {
        BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);
        if(StringUtils.isNotEmpty(baSettlement.getContractId())){
            //关联合同启动信息
            BaContractStart baContract =  baContractStartMapper.selectBaContractStartById(baSettlement.getContractId());
            baSettlement.setContractName(baContract.getName());
            if(StringUtils.isNotEmpty(baContract.getProjectId())){
                //项目类型
                BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
                baSettlement.setBusinessType(baProject.getBusinessType());
            }
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baSettlement.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baSettlement.setProcessInstanceId(processInstanceIds);
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baSettlement.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baSettlement.setUser(sysUser);
        //仓储里立项名称
        if(StringUtils.isNotEmpty(baSettlement.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baSettlement.getProjectId());
            baSettlement.setProjectName(baProject.getName());
        }
        //采购计划名称
        if(StringUtils.isNotEmpty(baSettlement.getPlanId())){
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baSettlement.getPlanId());
            baSettlement.setPlanName(baProcurementPlan.getPlanName());
        }
        return baSettlement;
    }

    //查询订单信息
    public BaOrderDTO baOrderDTOList(BaOrder baOrder,BigDecimal amount){
        BaOrderDTO baOrderDTO = new BaOrderDTO();
        //BaOrder baOrder = baOrderService.selectBaOrderById(baorder.getId());
        baOrderDTO.setId(baOrder.getId());
        baOrderDTO.setOrderName(baOrder.getOrderName());
        baOrderDTO.setGoodsName(baOrder.getGoodsName());
        baOrderDTO.setTransportType(baOrder.getTransportType());
        baOrderDTO.setCurrentCollection(amount);
        //订单对应的运输信息
        QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
        transportQueryWrapper.eq("order_id",baOrder.getId());
        transportQueryWrapper.eq("flag",0);
        BaTransport baTransport = baTransportMapper.selectOne(transportQueryWrapper);
        if(StringUtils.isNotNull(baTransport)){
            //运输编号
            baOrderDTO.setTransportId(baTransport.getId());
            //以运输信息查询发运信息
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baTransport.getId());
            List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
            for (BaCheck baCheck:baChecks) {
                if(baCheck.getType() == 1){
                    //矿发煤量
                    baOrderDTO.setCoalOutput(baCheck.getCheckCoalNum());
                    //发货日期
                    baOrderDTO.setCheckTime(baCheck.getCheckTime());
                    baOrderDTO.setCarsNum(baCheck.getCarsNum());
                }else {
                    //到货煤量
                    baOrderDTO.setCheckCoalNum(baCheck.getCheckCoalNum());
                    baOrderDTO.setArrivalTime(baCheck.getArrivalTime());
                    baOrderDTO.setCheckDate(baCheck.getCheckTime());
                }
            }
        }

        return baOrderDTO;
    }
    /**
     * 查询结算信息列表
     *
     * @param baSettlement 结算信息
     * @return 结算信息
     */
    @Override
    public List<BaSettlement> selectBaSettlementList(BaSettlement baSettlement)
    {
        List<BaSettlement> baSettlementList = baSettlementMapper.selectBaSettlement(baSettlement);
        for (BaSettlement baSettlements:baSettlementList) {
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(baSettlements.getUserId());
            //岗位
            String name = getName.getName(sysUser.getUserId());
            if(name.equals("") == false){
                baSettlements.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baSettlements.setUserName(sysUser.getNickName());
            }


            //结算方
            if(StringUtils.isNotEmpty(baSettlements.getSettlementParty())){
                if(baSettlements.getSettlementType() == 1){
                    //供应商名称
                 BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baSettlements.getSettlementParty());
                 baSettlements.setSettlementParty(baSupplier.getName());
                }else if(baSettlements.getSettlementType() == 2){
                    //用煤单位
                   BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baSettlements.getSettlementParty());
                   baSettlements.setSettlementParty(baEnterprise.getName());
                }else if(baSettlements.getSettlementType() == 3 || baSettlements.getSettlementType() == 4){
                    //公司名称
                   BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baSettlements.getSettlementParty());
                   baSettlements.setSettlementParty(baCompany.getName());
                }
            }
            //仓储里立项名称
            if(StringUtils.isNotEmpty(baSettlement.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(baSettlement.getProjectId());
                baSettlement.setProjectName(baProject.getName());
            }
            //采购计划名称
            if(StringUtils.isNotEmpty(baSettlement.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baSettlement.getPlanId());
                baSettlement.setPlanName(baProcurementPlan.getPlanName());
            }
        }
        return baSettlementList;
    }

    /**
     * 新增结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    @Override
    public int insertBaSettlement(BaSettlement baSettlement)
    {
        baSettlement.setId(getRedisIncreID.getId());
        baSettlement.setCreateTime(DateUtils.getNowDate());
        baSettlement.setCreateBy(SecurityUtils.getUsername());
        baSettlement.setUserId(SecurityUtils.getUserId());
        baSettlement.setDeptId(SecurityUtils.getDeptId());
        baSettlement.setSettlementState("1");
        if(StringUtils.isNotNull(baSettlement.getBaOrderDTOs())){
            BaSettlementDetail baSettlementDetail = new BaSettlementDetail();
            for (BaOrderDTO baOrder:baSettlement.getBaOrderDTOs()) {
                //订单添加历史数据
                baSettlementDetail.setId(getRedisIncreID.getId());
                baSettlementDetail.setSettlementId(baSettlement.getId());
                baSettlementDetail.setOrderId(baOrder.getId());
                baSettlementDetail.setAmount(baOrder.getCurrentCollection());
                //baSettlementDetail.setRelationId(baSettlement.getId());
                baSettlementDetail.setType("1");
                baSettlementDetailMapper.insertBaSettlementDetail(baSettlementDetail);
                //修改订单标识
                BaOrder baOrder1 = baOrderMapper.selectBaOrderById(baOrder.getId());
                if(baSettlement.getSettlementType() == 1){
                    if(StringUtils.isNotNull(baOrder1)){
                        //判断订单是否即使销售又是采购订单
                        if(baOrder1.getOrderType().equals("4") && baOrder1.getSettlementMark().equals("0") && baOrder1.getSettlementSign().equals("0")){
                            baOrder1.setSettlementMark(baSettlement.getSettlementType().toString());
                        }else if(baOrder1.getOrderType().equals("4") && baOrder1.getSettlementSign().equals("2")){
                            baOrder1.setSettlementMark(baSettlement.getSettlementType().toString());
                        }else {
                            baOrder1.setSettlementMark(baSettlement.getSettlementType().toString());
                            baOrder1.setSettlementSign(baSettlement.getSettlementType().toString());
                        }
                        baOrderMapper.updateBaOrder(baOrder1);
                    }
                }else if(baSettlement.getSettlementType() == 2){
                    if(StringUtils.isNotNull(baOrder1)){
                        //判断订单是否即使销售又是采购订单
                        if(baOrder1.getOrderType().equals("4") && baOrder1.getSettlementMark().equals("0") && baOrder1.getSettlementSign().equals("0")){
                            baOrder1.setSettlementSign(baSettlement.getSettlementType().toString());
                        }else if(baOrder1.getOrderType().equals("4") && baOrder1.getSettlementMark().equals("0")){
                            baOrder1.setSettlementSign(baSettlement.getSettlementType().toString());
                        }else if(baOrder1.getOrderType().equals("4") && baOrder1.getSettlementMark().equals("1")){
                            baOrder1.setSettlementSign(baSettlement.getSettlementType().toString());
                        }else {
                            baOrder1.setSettlementMark(baSettlement.getSettlementType().toString());
                            baOrder1.setSettlementSign(baSettlement.getSettlementType().toString());
                        }
                        baOrderMapper.updateBaOrder(baOrder1);
                    }
                }else if(baSettlement.getSettlementType() == 3){
                    if(StringUtils.isNotNull(baOrder1)){
                        baOrder1.setTransportMark(baSettlement.getSettlementType().toString());
                        baOrderMapper.updateBaOrder(baOrder1);
                    }
                }
            }
        }
        Integer result = baSettlementMapper.insertBaSettlement(baSettlement);
        if(result > 0){
            //更新项目阶段
            if(StringUtils.isNotEmpty(baSettlement.getContractId())){
                //关联合同信息
                BaContract baContract =  baContractService.selectBaContractById(baSettlement.getContractId());
                if(StringUtils.isNotEmpty(baContract.getProjectId())){
                    //项目阶段
                    BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
                    baProject.setProjectStage(8);
                    baProjectMapper.updateBaProject(baProject);
                }
            }
        }
        return result;
    }

    @Override
    public int insertSettlement(BaSettlement baSettlement) {
        //查询合同启动下是否存在最终结算单
        if(StringUtils.isNotEmpty(baSettlement.getContractId())){
            QueryWrapper<BaSettlement> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("contract_id", baSettlement.getContractId()).and(itm -> itm.eq("state", "settlement:pass").or().eq("state", "settlement:verifying"));
            queryWrapper.eq("flag", 0);
            queryWrapper.eq("final_mark",1);
            BaSettlement settlement = baSettlementMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(settlement)){
                return -1;
            }
        }
        baSettlement.setId(getRedisIncreID.getId());
        baSettlement.setCreateTime(DateUtils.getNowDate());
        baSettlement.setCreateBy(SecurityUtils.getUsername());
        baSettlement.setUserId(SecurityUtils.getUserId());
        baSettlement.setDeptId(SecurityUtils.getDeptId());
        baSettlement.setSettlementState("1");
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        //判断合同启动全局编号
        if(StringUtils.isNotEmpty(baSettlement.getStartGlobalNumber())){
            //全局编号
            QueryWrapper<BaSettlement> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.eq("flag",0);
            queryWrapper.isNotNull("serial_number");
            queryWrapper.orderByDesc("serial_number");
            List<BaSettlement> baSettlements = baSettlementMapper.selectList(queryWrapper);
            if(baSettlements.size() > 0){
                Integer serialNumber = baSettlements.get(0).getSerialNumber();
                baSettlement.setSerialNumber(serialNumber+1);
                if(serialNumber < 10){
                    baSettlement.setGlobalNumber(baSettlement.getStartGlobalNumber()+"-"+"JS"+"-"+"000"+baSettlement.getSerialNumber());
                }else if(serialNumber >= 10){
                    baSettlement.setGlobalNumber(baSettlement.getStartGlobalNumber()+"-"+"JS"+"-"+"00"+baSettlement.getSerialNumber());
                }else if(serialNumber >= 100){
                    baSettlement.setGlobalNumber(baSettlement.getStartGlobalNumber()+"-"+"JS"+"-"+"0"+baSettlement.getSerialNumber());
                }else if(serialNumber >= 1000){
                    baSettlement.setGlobalNumber(baSettlement.getStartGlobalNumber()+"-"+"JS"+"-"+baSettlement.getSerialNumber());
                }
            }else {
                baSettlement.setSerialNumber(1);
                baSettlement.setGlobalNumber(baSettlement.getStartGlobalNumber()+"-"+"JS"+"-"+"000"+baSettlement.getSerialNumber());
            }
        }
        BaContractStart baContract = null;
        if(StringUtils.isNotEmpty(baSettlement.getContractId())){
            //关联合同启动信息
            baContract =  baContractStartMapper.selectBaContractStartById(baSettlement.getContractId());
        }
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baSettlement.getWorkState()) || baSettlement.getSettlementType() == 2 || baSettlement.getSettlementType() == 3) {
            //流程发起状态
            baSettlement.setWorkState("1");
            //默认审批流程已通过
            baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode());
            //全局编号
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baSettlement.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baSettlement.getId());
                baGlobalNumber.setCode(baSettlement.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("5");
                baGlobalNumber.setStartId(baSettlement.getContractId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }
        }

        //审批流程关闭开始
        if(!ObjectUtils.isEmpty(baSettlement)){
            //下游结算单
            if(baSettlement.getSettlementType() == 2){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baSettlement.getContractId());
                if(!ObjectUtils.isEmpty(baContractStart)){
                    //完结的合同启动不更改原有状态
                    if(baContractStart.getStartType().equals("4") == false) {
                        baContractStart.setStartType("3");
                    }
                }
                baContractStartMapper.updateBaContractStart(baContractStart);
            }
        }
        //审批流程关闭结束
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode(),SecurityUtils.getCurrComId());
        baSettlement.setFlowId(flowId);
        Integer result = baSettlementMapper.insertBaSettlement(baSettlement);
        if(result > 0){
            if("2".equals(baSettlement.getWorkState()) && (baSettlement.getSettlementType() != 2 || baSettlement.getSettlementType() != 3)) {
                BaSettlement settlement = baSettlementMapper.selectBaSettlementById(baSettlement.getId());
//                启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(settlement, AdminCodeEnum.SETTLEMENT_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    deleteBaSettlementById(settlement.getId());
                    return 0;
                } else {
                    settlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_VERIFYING.getCode());
                    baSettlementMapper.updateBaSettlement(settlement);
                }
            }
        }
        return result;
    }

    /**
     * 提交结算审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaSettlement settlement, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(settlement.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(settlement.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaSettlement baSettlement = this.selectSettlementById(settlement.getId());
        SysUser sysUser = iSysUserService.selectUserById(baSettlement.getUserId());
        baSettlement.setUserName(sysUser.getUserName());
        baSettlement.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baSettlement, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.SETTLEMENT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改结算信息
     *
     * @param baSettlement 结算信息
     * @return 结果
     */
    @Override
    public int updateBaSettlement(BaSettlement baSettlement)
    {
        //查询合同启动下是否存在最终结算单
        if(StringUtils.isNotEmpty(baSettlement.getContractId())){
            QueryWrapper<BaSettlement> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("contract_id", baSettlement.getContractId()).and(itm -> itm.eq("state", "settlement:pass").or().eq("state", "settlement:verifying"));
            queryWrapper.eq("flag", 0);
            queryWrapper.eq("final_mark",1);
            BaSettlement settlement = baSettlementMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(settlement)){
                return -1;
            }
        }
        //原数据
        BaSettlement baSettlement1 = baSettlementMapper.selectBaSettlementById(baSettlement.getId());
        baSettlement.setUpdateTime(DateUtils.getNowDate());
        baSettlement.setUpdateBy(SecurityUtils.getUsername());
        baSettlement.setCreateTime(baSettlement.getUpdateTime());
        /*if(StringUtils.isNotNull(baSettlement.getBaOrderDTOs())) {
            for (BaOrderDTO baOrder : baSettlement.getBaOrderDTOs()) {
                //查询对应的历史数据
               QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
               queryWrapper.eq("settlement_id",baSettlement.getId());
               queryWrapper.eq("order_id",baOrder.getId());
                List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper);
                for (BaSettlementDetail baSettlementDetail:baSettlementDetails) {
                    baSettlementDetail.setAmount(baOrder.getCurrentCollection());
                    baSettlementDetailMapper.updateBaSettlementDetail(baSettlementDetail);
                }
            }
        }*/
        int result = baSettlementMapper.updateBaSettlement(baSettlement);
        if(result > 0 && !baSettlement1.getState().equals(AdminCodeEnum.SETTLEMENT_STATUS_PASS.getCode())){
            BaSettlement settlement = baSettlementMapper.selectBaSettlementById(baSettlement.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",settlement.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(settlement, AdminCodeEnum.SETTLEMENT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baSettlementMapper.updateBaSettlement(baSettlement1);
                return 0;
            }else {
                settlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_VERIFYING.getCode());
                baSettlementMapper.updateBaSettlement(settlement);
            }
        }
        return result;
    }

    /**
     * 批量删除结算信息
     *
     * @param ids 需要删除的结算信息ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapperRelated = new QueryWrapper<>();
                queryWrapperRelated.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapperRelated);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
               baSettlement.setFlag(1);
               baSettlementMapper.updateBaSettlement(baSettlement);
               /*if(baSettlement.getSettlementType() == 1){
                   //结算对应的付款信息
                   QueryWrapper<BaPayment> queryWrapper = new QueryWrapper<>();
                   queryWrapper.eq("settlement_id",id);
                   queryWrapper.eq("flag",0);
                   List<BaPayment> list = baPaymentMapper.selectList(queryWrapper);
                   String paymentId = "";
                   for (BaPayment baPayment:list) {
                       paymentId = baPayment.getId()+","+paymentId;
                   }
                   baPaymentService.deleteBaPaymentByIds(paymentId.split(","));
               }else if(baSettlement.getSettlementType() == 2){
                   //结算对应的收款信息
                   QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
                   queryWrapper.eq("settlement_id",id);
                   queryWrapper.eq("flag",0);
                   List<BaCollection> list = baCollectionMapper.selectList(queryWrapper);
                   String collectionId = "";
                   for (BaCollection baCollection:list) {
                       collectionId = baCollection.getId()+","+collectionId;
                   }
                   baCollectionService.deleteBaCollectionByIds(collectionId.split(","));
               }
                   //查询结算历史记录
                   QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
                   queryWrapper.eq("settlement_id",id);
                   List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper);
                   for (BaSettlementDetail baSettlementDetail:baSettlementDetails) {
                       //查询订单
                       BaOrder baOrder = baOrderMapper.selectBaOrderById(baSettlementDetail.getOrderId());
                       if(baSettlement.getSettlementType() == 1 && baOrder.getOrderType().equals("4")){
                         baOrder.setSettlementMark("0");
                       }else if(baSettlement.getSettlementType() == 2 && baOrder.getOrderType().equals("4")){
                           baOrder.setSettlementSign("0");
                       }else if(baSettlement.getSettlementType() == 3){
                           baOrder.setTransportMark("0");
                       } else {
                           baOrder.setSettlementMark("0");
                           baOrder.setSettlementSign("0");
                       }
                       baOrderMapper.updateBaOrder(baOrder);
                       //删除历史记录
                       baSettlementDetailMapper.deleteBaSettlementDetailById(baSettlementDetail.getId());
                   }*/

                //全局联查相关删除
                QueryWrapper<BaGlobalNumber> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                BaGlobalNumber baGlobalNumber = baGlobalNumberMapper.selectOne(queryWrapper);
                if(StringUtils.isNotNull(baGlobalNumber)){
                    baGlobalNumberMapper.deleteBaGlobalNumberById(baGlobalNumber.getId());
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除结算信息信息
     *
     * @param id 结算信息ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementById(String id)
    {
        return baSettlementMapper.deleteBaSettlementById(id);
    }

    @Override
    public List<BaOrderDTO> orderDto(String contractId,String type) {
        List<BaOrderDTO> baOrderDTOList = new ArrayList<>();
        //查询合同相关订单
        QueryWrapper<BaOrder> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(type)){
           if(type.equals("2")){
               //判断即使销售又是采购订单
               queryWrapper.eq("company_id",contractId).and(query ->query.eq("order_type","1")
                       .or().eq("order_type","4").or().isNull("settlement_mark"));
               queryWrapper.eq("settlement_sign","0");
           }else if(type.equals("1")){
               //判断即使销售又是采购订单
               queryWrapper.eq("purchase_company_id",contractId).and(query ->query.eq("order_type","2")
                       .or().eq("order_type","4").or().isNull("settlement_sign"));
               queryWrapper.eq("settlement_mark","0");
           }else if(type.equals("3") || type.equals("4")){
               queryWrapper.eq("purchase_company_id",contractId).and(query ->query.eq("order_type","4")).
                       or().eq("company_id",contractId).and(query ->query.eq("order_type","2")).or().
               eq("purchase_company_id",contractId).and(query ->query.eq("order_type","1"));
               //queryWrapper.eq("order_type",3);
               queryWrapper.eq("transport_mark","0");
           }
        }
        queryWrapper.eq("order_state","1");
        //queryWrapper.eq("order_type","2").or().eq("order_type","4");
        /*if(StringUtils.isNotEmpty(type)){
            queryWrapper.eq("transport_type",type);
        }*/
        queryWrapper.eq("flag",0);
        List<BaOrder> orderList  = baOrderMapper.selectList(queryWrapper);
        for (BaOrder baorder:orderList) {
            BaOrderDTO baOrderDTO = new BaOrderDTO();
            BaOrder baOrder = baOrderService.selectBaOrderById(baorder.getId());
            baOrderDTO.setId(baOrder.getId());
            baOrderDTO.setOrderName(baOrder.getOrderName());
            baOrderDTO.setGoodsName(baOrder.getGoodsName());
            baOrderDTO.setTransportType(baOrder.getTransportType());
            //订单对应的运输信息
            QueryWrapper<BaTransport> transportQueryWrapper = new QueryWrapper<>();
            transportQueryWrapper.eq("order_id",baOrder.getId());
            transportQueryWrapper.eq("flag",0);
            BaTransport baTransport = baTransportMapper.selectOne(transportQueryWrapper);
            //汽车运输
            QueryWrapper<BaTransportAutomobile> automobileQueryWrapper = new QueryWrapper<>();
            automobileQueryWrapper.eq("order_id",baOrder.getId());
            transportQueryWrapper.eq("is_delete",0);
            BaTransportAutomobile automobile = baTransportAutomobileMapper.selectOne(automobileQueryWrapper);
            if(StringUtils.isNotNull(baTransport) || StringUtils.isNotNull(automobile)){

                if(StringUtils.isNotNull(baTransport)){
                    //运输编号
                    baOrderDTO.setTransportId(baTransport.getId());
                }else if(StringUtils.isNotNull(automobile)){
                    //汽车运输编号
                    baOrderDTO.setTransportId(automobile.getId());
                }

                //以运输信息查询发运信息
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                if(StringUtils.isNotNull(automobile) && StringUtils.isNotNull(baTransport)){
                    checkQueryWrapper.eq("relation_id",baTransport.getId()).or().eq("relation_id",automobile.getId());
                }else if(StringUtils.isNotNull(baTransport)){
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                }else if(StringUtils.isNotNull(automobile)){
                    checkQueryWrapper.eq("relation_id",automobile.getId());
                }

                List<BaCheck> baChecks = baCheckMapper.selectList(checkQueryWrapper);
                for (BaCheck baCheck:baChecks) {
                    if(baCheck.getType() == 1){
                        //矿发煤量
                        baOrderDTO.setCoalOutput(baCheck.getCheckCoalNum());
                        //发货日期
                        baOrderDTO.setCheckTime(baCheck.getCheckTime());
                        baOrderDTO.setCarsNum(baCheck.getCarsNum());
                    }else {
                        //到货煤量
                        baOrderDTO.setCheckCoalNum(baCheck.getCheckCoalNum());
                        baOrderDTO.setArrivalTime(baCheck.getArrivalTime());
                        baOrderDTO.setCheckDate(baCheck.getCheckTime());
                    }
                }
            }
            //判断是否有验收信息
            if(baOrderDTO.getArrivalTime() != null){
                baOrderDTOList.add(baOrderDTO);
            }
        }
        return baOrderDTOList;
    }

    @Override
    public List<BaSettlement> selectBaSettlement(BaSettlement baSettlement) {
        QueryWrapper<BaSettlement> settlementQueryWrapper = new QueryWrapper<>();
        settlementQueryWrapper.eq("flag",0);
        if(!baSettlement.getSettlementState().equals("all")){
            if(StringUtils.isNotEmpty(baSettlement.getSettlementState())){
                settlementQueryWrapper.eq("settlement_state",baSettlement.getSettlementState());
            }
        }
        if(StringUtils.isNotEmpty(baSettlement.getSettlementName())){
            settlementQueryWrapper.like("settlement_name",baSettlement.getSettlementName());
        }

        if(baSettlement.getSettlementType() != null){
            settlementQueryWrapper.eq("settlement_type",baSettlement.getSettlementType());
        }
        settlementQueryWrapper.orderByDesc("create_time");
        List<BaSettlement> settlementList = baSettlementMapper.selectList(settlementQueryWrapper);
        for (BaSettlement baSettlements:settlementList) {
            if(StringUtils.isNotEmpty(baSettlements.getSettlementParty())){
             //结算方
                if(baSettlements.getSettlementType() == 1){
                    if(StringUtils.isNotEmpty(baSettlements.getSettlementParty())){
                        //供应商名称
                        BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baSettlements.getSettlementParty());
                        baSettlements.setSettlementParty(baSupplier.getName());
                    }
                }else if(baSettlements.getSettlementType() == 2){
                    if(StringUtils.isNotEmpty(baSettlements.getSettlementParty())) {
                        //用煤单位
                        BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baSettlements.getSettlementParty());
                        baSettlements.setSettlementParty(baEnterprise.getName());
                    }
                }else if(baSettlements.getSettlementType() == 3 || baSettlements.getSettlementType() == 4){
                    //公司名称
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baSettlements.getSettlementParty());
                    baSettlements.setSettlementParty(baCompany.getName());
                }
            }
            //已结金额
            if(baSettlements.getSettlementType() == 2){
                //收款信息
                QueryWrapper<BaCollection> collectionQueryWrapper = new QueryWrapper<>();
                collectionQueryWrapper.eq("settlement_id",baSettlements.getId());
                collectionQueryWrapper.eq("flag",0);
                List<BaCollection> baCollectionList = baCollectionMapper.selectList(collectionQueryWrapper);
                for (BaCollection baCollection:baCollectionList) {
                    //发起人
                    baSettlements.setUserName(sysUserMapper.selectUserById(baCollection.getUserId()).getNickName());
                }
                baSettlements.setCollectionList(baCollectionList);
                //已收金额
                BigDecimal decimal = new BigDecimal(0);
                if(StringUtils.isNotNull(baCollectionList)){
                    for (BaCollection baCollection:baCollectionList) {
                        if(baCollection.getCollectionState().equals(AdminCodeEnum.COLLECTION_RECEIVED.getCode())){
                            decimal = decimal.add(baCollection.getActualCollectionAmount());
                        }
                    }
                    baSettlements.setDecimal(decimal);
                }else {
                    baSettlements.setDecimal(new BigDecimal(0));
                }
            }else {
                //付款信息
                QueryWrapper<BaPayment> paymentQueryWrapper = new QueryWrapper<>();
                paymentQueryWrapper.eq("settlement_id", baSettlements.getId());
                paymentQueryWrapper.eq("flag", 0);
                List<BaPayment> list = baPaymentMapper.selectList(paymentQueryWrapper);
                for (BaPayment baPayment : list) {
                    baSettlements.setUserName(sysUserMapper.selectUserById(baPayment.getUserId()).getNickName());
                }
                baSettlements.setPaymentList(list);
                //已付款金额
                BigDecimal decimal = new BigDecimal(0);
                if (StringUtils.isNotNull(list)) {
                    for (BaPayment baPayment : list) {
                        if(baPayment.getPaymentState().equals(AdminCodeEnum.PAYMENT_PAID.getCode())){
                            decimal = decimal.add(baPayment.getActualPaymentAmount());
                        }
                    }
                    baSettlements.setDecimal(decimal);
                } else {
                    baSettlements.setDecimal(new BigDecimal(0));
                }
            }
            //发票数量
            QueryWrapper<BaInvoice> invoiceQueryWrapper = new QueryWrapper<>();
            invoiceQueryWrapper.eq("settlement_id",baSettlements.getId());
            invoiceQueryWrapper.eq("flag", 0);
            List<BaInvoice> baInvoices = baInvoiceMapper.selectList(invoiceQueryWrapper);
            if(StringUtils.isNotNull(baInvoices)){
                baSettlements.setInvoiceNum(baInvoices.size());
            }
        }
        return settlementList;
    }

    @Override
    public NumsAndMoneyDTO selectBaSettlementforup(BaSettlement baSettlement) {
        return baSettlementMapper.selectBaSettlementforup(baSettlement);
    }

    @Override
    public NumsAndMoneyDTO selectBaSettlementforMonth(BaSettlement baSettlement) {
        return baSettlementMapper.selectBaSettlementforMonth(baSettlement);
    }

    @Override
    public NumsAndMoneyDTO selectBaSettlementforYear(BaSettlement baSettlement) {
        return baSettlementMapper.selectBaSettlementforYear(baSettlement) ;
    }

    @Override
    public int settlementEnd(String id) {
        if(StringUtils.isNotEmpty(id)){
           BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);
           baSettlement.setSettlementState("3");
           baSettlementMapper.updateBaSettlement(baSettlement);
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baSettlement.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.SETTLEMENT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baSettlement.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baSettlement.setState(AdminCodeEnum.SETTLEMENT_STATUS_WITHDRAW.getCode());
                baSettlementMapper.updateBaSettlement(baSettlement);
                String userId = String.valueOf(baSettlement.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baSettlement, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int countBaSettlementList(BaSettlement baSettlement) {
        //租户ID
        baSettlement.setTenantId(SecurityUtils.getCurrComId());
        return baSettlementMapper.countBaSettlementList(baSettlement);
    }

    @Override
    public List<BaSettlement> selectBaSettlementAuthorizedList(BaSettlement baSettlement) {
        List<BaSettlement> baSettlementList = baSettlementMapper.selectBaSettlementAuthorized(baSettlement);
        for (BaSettlement baSettlements:baSettlementList) {
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(baSettlements.getUserId());
            //岗位
            String name = getName.getName(sysUser.getUserId());
            if(name.equals("") == false){
                baSettlements.setUserName(sysUser.getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                baSettlements.setUserName(sysUser.getNickName());
            }


            //结算方
            if(StringUtils.isNotEmpty(baSettlements.getSettlementParty())){
                if(baSettlements.getSettlementType() == 1){
                    //供应商名称
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baSettlements.getSettlementParty());
                    baSettlements.setSettlementParty(baSupplier.getName());
                }else if(baSettlements.getSettlementType() == 2){
                    //用煤单位
                    BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baSettlements.getSettlementParty());
                    baSettlements.setSettlementParty(baEnterprise.getName());
                }else if(baSettlements.getSettlementType() == 3 || baSettlements.getSettlementType() == 4){
                    //公司名称
                    BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baSettlements.getSettlementParty());
                    baSettlements.setSettlementParty(baCompany.getName());
                }
            }
            //仓储里立项名称
            if(StringUtils.isNotEmpty(baSettlement.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(baSettlement.getProjectId());
                baSettlement.setProjectName(baProject.getName());
            }
            //采购计划名称
            if(StringUtils.isNotEmpty(baSettlement.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baSettlement.getPlanId());
                baSettlement.setPlanName(baProcurementPlan.getPlanName());
            }
        }
        return baSettlementList;
    }

    @Override
    public int authorizedBaSettlement(BaSettlement baSettlement) {
        return baSettlementMapper.updateBaSettlement(baSettlement);
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSettlement baSettlement, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSettlement.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SETTLEMENT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"结算审批提醒",msgContent,baMessage.getType(),baSettlement.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
