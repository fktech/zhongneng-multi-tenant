package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaMarketInformation;

/**
 * 市场信息管理Service接口
 *
 * @author ljb
 * @date 2023-03-22
 */
public interface IBaMarketInformationService
{
    /**
     * 查询市场信息管理
     *
     * @param id 市场信息管理ID
     * @return 市场信息管理
     */
    public BaMarketInformation selectBaMarketInformationById(String id);

    /**
     * 查询市场信息管理列表
     *
     * @param baMarketInformation 市场信息管理
     * @return 市场信息管理集合
     */
    public List<BaMarketInformation> selectBaMarketInformationList(BaMarketInformation baMarketInformation);

    /**
     * 移动端业务经理首页
     *
     * @param baMarketInformation 市场信息管理
     * @return 市场信息管理集合
     */
    public List<BaMarketInformation> baMarketInformationList(BaMarketInformation baMarketInformation);

    /**
     * 新增市场信息管理
     *
     * @param baMarketInformation 市场信息管理
     * @return 结果
     */
    public int insertBaMarketInformation(BaMarketInformation baMarketInformation);

    /**
     * 修改市场信息管理
     *
     * @param baMarketInformation 市场信息管理
     * @return 结果
     */
    public int updateBaMarketInformation(BaMarketInformation baMarketInformation);

    /**
     * 批量删除市场信息管理
     *
     * @param ids 需要删除的市场信息管理ID
     * @return 结果
     */
    public int deleteBaMarketInformationByIds(String[] ids);

    /**
     * 删除市场信息管理信息
     *
     * @param id 市场信息管理ID
     * @return 结果
     */
    public int deleteBaMarketInformationById(String id);


}
