package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 入库审批接口 服务类
 * @date: 2023/1/5 11:10
 */
@Service
public class IIntodepotApprovalService {

    @Autowired
    protected BaDepotHeadMapper baDepotHeadMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaBatchMapper baBatchMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaDepotInventoryMapper baDepotInventoryMapper;

    @Autowired
    private BaPurchaseOrderMapper baPurchaseOrderMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaPoundRoomMapper baPoundRoomMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaDepotHead baDepotHead = new BaDepotHead();
        baDepotHead.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
        String userId = String.valueOf(baDepotHead.getUserId());
        if(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode().equals(status)){
            //根据出入库记录查询质检数据
            QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id", baDepotHead.getId());
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
            baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            //设置业务状态已入库
            //判断采购入库是否有入库清单
            QueryWrapper<BaDepotInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id",baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(queryWrapper1);
            //根据货源单ID查询货源
            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                    baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                    baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                    baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                }
            }
            if(!CollectionUtils.isEmpty(baDepotInventories)){
                for(BaDepotInventory baDepotInventory : baDepotInventories){
                    //昕科运单入库状态修改
                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                    baShippingOrder.setState("2"); //运单绑定状态修改
                    baShippingOrder.setDepotId(baDepotHead.getDepotId());
                    baShippingOrder.setPositionId(baDepotHead.getLocationId());
                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                }
            }
            //更新入库时间
            baDepotHead.setOperTime(DateUtils.getNowDate());

            //查询采购计划下的清单
            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
            //采购计划绑定项目ID
//            String procurementPlanProjectId = "";
//            if(!ObjectUtils.isEmpty(baPurchaseOrder)){
                //采购清单品名
//                purchaseGoodsId = baPurchaseOrder.getGoodsId();
//                if(StringUtils.isNotEmpty(baPurchaseOrder.getPlanId())){
//                    BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
//                    if(!ObjectUtils.isEmpty(baProcurementPlan)){
//                        procurementPlanProjectId = baProcurementPlan.getProjectId();
//                    }
//                }
//            }

//            BaBatch baBatch = baBatchMapper.selectBaBatchById(baDepotHead.getBatch());
            //更新批次初始库存
//            if(ObjectUtils.isEmpty(baBatch.getInitialStock())){
//                if(!ObjectUtils.isEmpty(baCheck)){
//                    baBatch.setInitialStock(baCheck.getCheckCoalNum());
//                }
//                baBatchMapper.updateBaBatch(baBatch);
//            }

           /* BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getStartId());

            //更新出库量
            if(baContractStart.getScheduledReceipt() != null && baContractStart.getScheduledReceipt().compareTo(BigDecimal.ZERO) != 0 && baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0){
                baContractStart.setScheduledReceipt(baContractStart.getScheduledReceipt().add(baDepotHead.getTotals()));
            } else {
                if(baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0){
                    baContractStart.setScheduledReceipt(baDepotHead.getTotals());
                }
            }
            //查询立项信息
            if (StringUtils.isNotEmpty(baContractStart.getProjectId())) {
                BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
                if(StringUtils.isNotNull(baProject.getDepotId())) {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baProject.getDepotId());
                    BaMaterialStock materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    //更新库存总量
                    if (materialStock.getInventoryTotal() == null) {
                        materialStock.setInventoryTotal(new BigDecimal(0));
                    }
                    if (baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0) {
                        materialStock.setInventoryTotal(materialStock.getInventoryTotal().add(baDepotHead.getTotals()));
                    }
                    //更新库存总热值
                    if (materialStock.getAllAverageCalorific() == null) {
                        materialStock.setAllAverageCalorific(new BigDecimal(0));
                    }
                    if (baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null) {
                        materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific()
                                .add(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP)));
                    }
                    //库存均价
                    if (materialStock.getInventoryAverage() == null) {
                        materialStock.setInventoryAverage(baDepotHead.getProcurePrice());
                    }
                    //库存总量（吨）
                    if (materialStock.getInventoryTotal() != null) {
                        if (materialStock.getInventoryTotal().intValue() == 0) {
                            materialStock.setAverageCalorific(new BigDecimal(0));
                        } else if (materialStock.getAllAverageCalorific() != null) {
                            //平均热值等于库存总热值除以库存总量
                            materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
                        }
                    }
                    materialStock.setUpdateBy(SecurityUtils.getUsername());
                    materialStock.setUpdateTime(DateUtils.getNowDate());
                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
                baContractStartMapper.updateBaContractStart(baContractStart);
            }*/


            //更新出库量
//            if(baContractStart.getScheduledReceipt() != null && baContractStart.getScheduledReceipt().compareTo(BigDecimal.ZERO) != 0 && baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0){
//                baContractStart.setScheduledReceipt(baContractStart.getScheduledReceipt().add(baDepotHead.getTotals()));
//            } else {
//                if(baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0){
//                    baContractStart.setScheduledReceipt(baDepotHead.getTotals());
//                }
//            }
            //查询立项信息
//            BaProject baProject = baProjectMapper.selectBaProjectById(procurementPlanProjectId);
//            if(StringUtils.isNotNull(baProject.getDepotId())) {
                BaMaterialStock materialStock = null;
                //判断是否选择了库位
                if(StringUtils.isNotEmpty(baDepotHead.getLocationId())){
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                    stockQueryWrapper.eq("location_id", baDepotHead.getLocationId());
                    stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    //判断是否已存在库存, 不存在则新增
                    if(ObjectUtils.isEmpty(materialStock)){
                        materialStock = new BaMaterialStock();
                        materialStock.setId(getRedisIncreID.getId());
                        materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                        materialStock.setCreateTime(DateUtils.getNowDate());
                        materialStock.setCreateBy(SecurityUtils.getUsername());
                        materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                        materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        baMaterialStockMapper.insertBaMaterialStock(materialStock);
                    } else {
                        materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                        materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                        materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                        materialStock.setUpdateBy(SecurityUtils.getUsername());
                        materialStock.setUpdateTime(DateUtils.getNowDate());
                        materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                        baMaterialStockMapper.updateBaMaterialStock(materialStock);
                    }

                } else {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                    stockQueryWrapper.isNull("location_id");
                    stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    //判断是否已存在库存, 不存在则新增
                    if(ObjectUtils.isEmpty(materialStock)){
                        materialStock = new BaMaterialStock();
                        materialStock.setId(getRedisIncreID.getId());
                        materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                        materialStock.setCreateTime(DateUtils.getNowDate());
                        materialStock.setCreateBy(SecurityUtils.getUsername());
                        materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                        materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                        materialStock.setTenantId(SecurityUtils.getCurrComId());
                        baMaterialStockMapper.insertBaMaterialStock(materialStock);
                    } else {
                        materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                        materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                        materialStock.setUpdateBy(SecurityUtils.getUsername());
                        materialStock.setUpdateTime(DateUtils.getNowDate());
                        materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                        baMaterialStockMapper.updateBaMaterialStock(materialStock);
                    }
                }
                //入库计算货值均价
                if(materialStock.getIntoDepotCargoValue() == null){
                    materialStock.setIntoDepotCargoValue(new BigDecimal(0));
                }
                if(baDepotHead.getCargoValue() != null){
                    //库存入库货值累计 + 本次入库货值
                    materialStock.setIntoDepotCargoValue(materialStock.getIntoDepotCargoValue().add(baDepotHead.getCargoValue()));
                }

                if(materialStock.getInventoryAverage() == null){
                    materialStock.setInventoryAverage(new BigDecimal(0));
                }
                if(materialStock.getIntoDepotCargoValue() != null && materialStock.getInventoryTotal() != null){
                    //计算库存均价
                    materialStock.setInventoryAverage(materialStock.getIntoDepotCargoValue().divide(materialStock.getInventoryTotal(),
                        2, BigDecimal.ROUND_HALF_UP));
                }
                baMaterialStockMapper.updateBaMaterialStock(materialStock);
                baDepotHead.setHeadStatus("3");

//            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());

            //插入消息通知信息
            this.insertMessage(baDepotHead, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.INTODEPOT_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baDepotHead, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            //设置业务状态未入库
            baDepotHead.setHeadStatus("2");

            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            //查询所有入库单
            for (BaDepotInventory baDepotInventory:baDepotInventories) {
                if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                    if(baDepotInventory.getSource().equals("1")){
                        //改变磅房数据入库状态
                        QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                        queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                        BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                        baPoundRoom.setStatus("0");
                        baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                        //根据货源单ID查询货源
                        if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                            if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                baTransportAutomobile.setWarehousing("0"); //货源绑定入库
                                baTransportAutomobile.setProjectId("");
                                baTransportAutomobile.setProcureId("");
                                baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                            }
                        }
                        //昕科运单入库状态修改
                        BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                        baShippingOrder.setStatus("0"); //运单绑定状态修改
                        baShippingOrder.setState("0"); //运单绑定状态修改
                        baShippingOrder.setDepotId("");
                        baShippingOrder.setPositionId("");
                        baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                    }else if(baDepotInventory.getSource().equals("2")){
                        //改变无车承运数据入库状态
                        if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                            if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                //昕科运单入库状态修改
                                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                baShippingOrder.setStatus("0");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);

                                //根据货源单ID查询货源
                                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                    if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                        baTransportAutomobile.setWarehousing("0"); //货源绑定入库
                                        baTransportAutomobile.setProjectId("");
                                        baTransportAutomobile.setProcureId("");
                                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                    }
                                }
                                //昕科运单入库状态修改
                                baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }else if(baDepotInventory.getPlatformType().equals("2")){
                                //中储摘单入库状态修改
                                BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                baShippingOrderCmst.setStatus("0");
                                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);

                                //根据货源单ID查询货源
                                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                    BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                    if(!ObjectUtils.isEmpty(baTransportCmst)){
                                        baTransportCmst.setWarehousing("0"); //货源绑定入库
                                        baTransportCmst.setProjectId("");
                                        baTransportCmst.setProcureId("");
                                        baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                    }
                                }
                                //昕科运单入库状态修改
                                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }
                    }
                }
            }

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baDepotHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baDepotHead.setState(status);
        baDepotHeadMapper.updateById(baDepotHead);
        return true;
    }

    /**
     * 入库申请审批通过计算更新库存数据
     * @param materialStock
     * @param baDepotHead
     * @param baCheck
     * @return
     */
    private BaMaterialStock calcBaMaterialStock(BaMaterialStock materialStock, BaDepotHead baDepotHead,BaCheck baCheck){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().add(baDepotHead.getTotals()));
        }
        //更新库存总热值
        if (materialStock.getAllAverageCalorific() == null) {
            materialStock.setAllAverageCalorific(new BigDecimal(0));
        }
        if (baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null) {
            materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific()
                    .add(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP)));
        }
        //库存均价
        if (materialStock.getInventoryAverage() == null) {
            materialStock.setInventoryAverage(baDepotHead.getProcurePrice());
        }
        //库存总量（吨）
        if (materialStock.getInventoryTotal() != null) {
            if (materialStock.getInventoryTotal().intValue() == 0) {
                materialStock.setAverageCalorific(new BigDecimal(0));
            } else if (materialStock.getAllAverageCalorific() != null) {
                //平均热值等于库存总热值除以库存总量
                materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
            }
        }
        return materialStock;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaDepotHead baDepotHead, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baDepotHead.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"入库审批提醒",msgContent,baMessage.getType(),baDepotHead.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaDepotHead checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaDepotHead baDepotHead = baDepotHeadMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baDepotHead == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_INTODEPOT_001);
        }
        if (!AdminCodeEnum.INTODEPOT_STATUS_VERIFYING.getCode().equals(baDepotHead.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_INTODEPOT_002);
        }
        return baDepotHead;
    }
}
