package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaOvertime;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEgressApprovalService;
import com.business.system.service.workflow.IOaOvertimeApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 外出申请审批结果:审批通过
 */
@Service("oaEgressApprovalContent:processApproveResult:pass")
@Slf4j
public class OaEgressApprovalAgreeServiceImpl extends IOaEgressApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaEgress oaEgress = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
        return result;
    }
}
