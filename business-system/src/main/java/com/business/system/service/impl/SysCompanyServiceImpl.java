package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.UserConstants;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.CustomException;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.SysCompany;
import com.business.system.domain.SysTemplate;
import com.business.system.domain.dto.CopyProcessTemplatesDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.SysCompanyInstanceRelatedEditDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.KeyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * 公司信息Service业务层处理
 *
 * @author leslie1015
 * @date 2020-07-11
 */
@Service
public class SysCompanyServiceImpl implements ISysCompanyService
{
    private static final Logger log = LoggerFactory.getLogger(SysCompanyServiceImpl.class);
    @Autowired
    private SysCompanyMapper sysCompanyMapper;


    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysTemplateMapper sysTemplateMapper;

    @Value("${sys.default.pwd}")
    private String defaultPwd;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaBankMapper baBankMapper;


    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     *  0否1是
     */
    private static final Integer FLAG_NO = 0, FLAG_YES = 1;

    /**
     * 查询公司信息
     *
     * @param id 公司信息ID
     * @return 公司信息
     */
    @Override
    public SysCompany selectSysCompanyById(String id)
    {
        SysCompany sysCompany = sysCompanyMapper.selectSysCompanyById(id);
        if(StringUtils.isNotEmpty(sysCompany.getTempId())){
            SysTemplate sysTemplate = sysTemplateMapper.selectSysTemplateById(sysCompany.getTempId());
            sysCompany.setTempName(sysTemplate.getName());
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",sysCompany.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        sysCompany.setProcessInstanceId(processInstanceIds);
        return sysCompany;
    }

    /**
     * 查询公司信息列表
     *
     * @param sysCompany 公司信息
     * @return 公司信息
     */
    @Override
    public List<SysCompany> selectSysCompanyList(SysCompany sysCompany)
    {
        return sysCompanyMapper.selectSysCompanyList(sysCompany);
    }

    /**
     * 新增公司信息
     *
     * @param sysCompany 公司信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSysCompany(SysCompany sysCompany)
    {
        //简称不能重复
        SysCompany sysCompany1 = new SysCompany();
        sysCompany1.setCompanyAbbreviation(sysCompany.getCompanyAbbreviation());
        List<SysCompany> sysCompanies = sysCompanyMapper.selectSysCompanyList(sysCompany1);
        if(sysCompanies.size() > 0){
            return -1;
        }
        String companyId = KeyUtils.genUniqueKey();
        sysCompany.setId(companyId);
        sysCompany.setComCode(getComCode());
        sysCompany.setCreateTime(DateUtils.getNowDate());
        if (null == sysCompany.getActiveFlag()) {
            sysCompany.setActiveFlag(FLAG_YES);
        }
        if (StringUtils.isBlank(sysCompany.getPhone())) {
            throw new CustomException("电话不能为空");
        }
        if (null == sysCompany.getTempId()) {
            throw new CustomException("请选择权限模板");
        }
        if (sysCompanyMapper.insertSysCompany(sysCompany) <= 0) {
            throw new CustomException("添加公司失败");
        }
        //初始化流程定义模板
        CopyProcessTemplatesDTO copyProcessTemplatesDTO = new CopyProcessTemplatesDTO();
        copyProcessTemplatesDTO.setTenantId("1706366025829490414");//默认流程模板
        copyProcessTemplatesDTO.setNewTenantId(companyId);
        AjaxResult ajaxResult = workFlowFeignClient.copyTemplates(copyProcessTemplatesDTO);
        String result = "";
        if(!ObjectUtils.isEmpty(ajaxResult)){
            result = String.valueOf(ajaxResult.get("msg"));
            if(!result.equals(Constants.SUCCESS)){
                throw new CustomException("初始化流程定义异常");
            }
        }
        return addDefaultUser(sysCompany);
    }

    /**
     * 认证公司信息
     *
     * @param sysCompany 公司信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addSysCompany(SysCompany sysCompany)
    {
        if(StringUtils.isNotEmpty(sysCompany.getCompanyAbbreviation())){
            //简称不能重复
            SysCompany sysCompany1 = new SysCompany();
            sysCompany1.setCompanyAbbreviation(sysCompany.getCompanyAbbreviation());
            List<SysCompany> sysCompanies = sysCompanyMapper.selectSysCompanyList(sysCompany1);
            if(sysCompanies.size() > 0){
                return -1;
            }
        }
        String companyId = KeyUtils.genUniqueKey();
        sysCompany.setId(companyId);
        sysCompany.setComCode(getComCode());
        sysCompany.setCreateTime(DateUtils.getNowDate());
        sysCompany.setUserName(sysCompany.getCompanyName());
        if (null == sysCompany.getActiveFlag()) {
            sysCompany.setActiveFlag(FLAG_YES);
        }
        //新增对应部门
        SysDept sysDept = new SysDept();
        sysDept.setDeptId(Long.valueOf(getRedisIncreID.getId().substring(getRedisIncreID.getId().length() - 12)));
        sysDept.setParentId(0L);
        //公司名称
        sysDept.setDeptName(sysCompany.getCompanyName());
        sysDept.setDeptType("2");
        sysDept.setStatus("0");
        sysDept.setTenantId(sysCompany.getId());
        int i = iSysDeptService.insertDept(sysDept);
        if(i > 0){
            //更新用户信息
            SysUser user = sysUserMapper.selectUserById(sysCompany.getUserId());
            user.setDeptId(sysDept.getDeptId());
            user.setComId(sysCompany.getId());
            //法人名称
            user.setNickName(sysCompany.getLegalPerson());
            sysUserMapper.updateUser(user);
            sysCompany.setDeptId(sysDept.getDeptId());
            //企业用户名
            sysCompany.setUserName(user.getUserName());
            //企业手机号
            sysCompany.setPhone(user.getPhonenumber());
        }
        //流程ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_ENTERREGISTER.getCode(),"1706366025829490414");
        sysCompany.setFlowId(flowId);
        int company = sysCompanyMapper.insertSysCompany(sysCompany);
        if(company > 0){
            SysCompany company1 = sysCompanyMapper.selectSysCompanyById(sysCompany.getId());
            company1.setTenantId("1706366025829490414");
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(company1, AdminCodeEnum.ENTERREGISTER_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                //用户清空租户ID
                SysCompany company2 = sysCompanyMapper.selectSysCompanyById(sysCompany.getId());
                //用户清空对应租户ID
                SysUser user = sysUserMapper.selectUserById(company2.getUserId());
                user.setComId("");
                sysUserMapper.updateUser(user);
                //删除对应部门
                sysDeptMapper.deleteDeptById(company2.getDeptId());
                sysCompanyMapper.deleteSysCompanyById(sysCompany.getId());
                return 0;
            } else {
                company1.setState(AdminCodeEnum.ENTERREGISTER_STATUS_VERIFYING.getCode());
                sysCompanyMapper.updateSysCompany(company1);
            }
        }
        return company;
    }

    /**
     * 提交企业注册审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(SysCompany company, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(company.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ENTERREGISTER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_ENTERREGISTER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(company.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ENTERREGISTER.getCode());
        //获取流程实例关联的业务对象
        SysCompany sysCompany = this.selectSysCompanyById(company.getId());
        SysUser sysUser = iSysUserService.selectUserById(sysCompany.getUserId());
        sysCompany.setUserName(sysUser.getUserName());
        sysCompany.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(sysCompany, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO,sysCompany);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime,SysCompany company) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        //UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        SysUser user = sysUserMapper.selectUserById(company.getUserId());
        UserInfo userInfo = new UserInfo();
        userInfo.setId(String.valueOf(user.getUserId()));
        userInfo.setName(user.getUserName());
        userInfo.setNickName(user.getNickName());
        //根据部门ID获取部门信息
        SysDept sysDept = iSysDeptService.selectDeptById(user.getDeptId());
        if(!ObjectUtils.isEmpty(sysDept)){
            userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
        }
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId("1706366025829490414");
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        LoginUser loginUser1 = new LoginUser();
        loginUser1.setDept(user.getDeptId());
        loginUser1.setDeptId(user.getDeptId());
        loginUser1.setUserId(user.getUserId());
        user.setComId("1706366025829490414");
        loginUser1.setUser(user);
        try {
            redisCache.lock(Constants.LOGIN_USER, loginUser1, lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.ENTERREGISTER_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(user.getUserName());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(user.getUserName());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }


    /**
     * 添加默认用户
     * @param sysCompany
     */
    private Integer addDefaultUser(SysCompany sysCompany) {
        SysUser user = new SysUser();
        user.setUserName(sysCompany.getUserName());
        user.setPhonenumber(sysCompany.getPhone());
        //user.setNickName("管理员" + sysCompany.getComCode());
        user.setNickName(sysCompany.getLegalPerson());
        user.setEmail(sysCompany.getEmail());
        if(sysCompany.getActiveFlag() == 1){
            user.setStatus("0");//0正常1停用
        }else if(sysCompany.getActiveFlag() == 0){
            user.setStatus("1");//0正常1停用
        }
        user.setCreateBy("superAdmin");
        user.setCreateTime(new Date());
        user.setDeptId(new Long(103));
        user.setRemark("公司管理员账号");
        user.setComId(sysCompany.getId());
        user.setAdminFlag(FLAG_YES);//是否管理员0否1是
        user.setSuperAdminFlag(FLAG_NO);//是否平台超级管理员
        user.setPassword(SecurityUtils.encryptPassword(defaultPwd));
        checkUser(user);
        return userMapper.insertUser(user);
    }

    /**
     * 校验用户信息
     * @param user
     */
    private void checkUser(SysUser user) {
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，该登录账号已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }else if(StringUtils.isNotEmpty(user.getPhonenumber()) && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))){
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，手机号已存在");
        }
    }

    private synchronized Integer getComCode() {
        Integer comCode = KeyUtils.genComCode();
        Integer count = sysCompanyMapper.countByComCode(comCode);
        if ( null != count && count > 0) {
            return getComCode();
        }
        return comCode;
    }

    /**
     * 修改公司信息
     *
     * @param sysCompany 公司信息
     * @return 结果
     */
    @Override
    public int updateSysCompany(SysCompany sysCompany)
    {
            //禁用租户下所有用户
            SysUser user = new SysUser();
            user.setComId(sysCompany.getId());
            List<SysUser> sysUsers = sysUserMapper.selectUserList(user);
            for (SysUser sysUser:sysUsers) {
                //判断租户状态
                if(null != sysCompany.getActiveFlag()){
                    if(sysCompany.getActiveFlag() == 1){
                        sysUser.setStatus("0");//0正常1停用
                    }else if(sysCompany.getActiveFlag() == 0){
                        sysUser.setStatus("1");//0正常1停用
                    }
                    sysUserMapper.updateUser(sysUser);
                }
            }
            //查看简称是否重复
        SysCompany sysCompany1 = sysCompanyMapper.selectSysCompanyById(sysCompany.getId());
        if(StringUtils.isNotEmpty(sysCompany.getCompanyAbbreviation()) && StringUtils.isNotEmpty(sysCompany1.getCompanyAbbreviation())){
                if(!sysCompany1.getCompanyAbbreviation().equals(sysCompany.getCompanyAbbreviation())){
                    //简称不能重复
                    SysCompany company = new SysCompany();
                    company.setCompanyAbbreviation(sysCompany.getCompanyAbbreviation());
                    List<SysCompany> sysCompanies = sysCompanyMapper.selectSysCompanyList(company);
                    if(sysCompanies.size() > 0){
                        return -1;
                    }
                }
            }else if(StringUtils.isNotEmpty(sysCompany1.getCompanyAbbreviation())){
                    //简称不能重复
                    SysCompany company = new SysCompany();
                    company.setCompanyAbbreviation(sysCompany.getCompanyAbbreviation());
                    List<SysCompany> sysCompanies = sysCompanyMapper.selectSysCompanyList(company);
                    if(sysCompanies.size() > 0){
                        return -1;
                    }
        }
        return sysCompanyMapper.updateSysCompany(sysCompany);
    }

    /**
     * 批量删除公司信息
     *
     * @param ids 需要删除的公司信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                //查询租户下所有用户
                SysUser user = new SysUser();
                user.setComId(id);
                List<SysUser> sysUsers = sysUserMapper.selectUserList(user);
                if(sysUsers.size() > 0){
                    for (SysUser sysUser:sysUsers) {
                        if(null == sysUser.getAdminFlag()){
                            return -1;
                        }else {
                            sysUserMapper.deleteUserById(sysUser.getUserId());
                        }
                    }
                }
            }
        }
        return sysCompanyMapper.deleteSysCompanyByIds(ids);
    }

    /**
     * 删除公司信息信息
     *
     * @param id 公司信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyById(String id)
    {
        return sysCompanyMapper.deleteSysCompanyById(id);
    }

    @Override
    public int updateProcessBusinessData(SysCompanyInstanceRelatedEditDTO sysCompanyInstanceRelatedEditDTO) {
        SysCompany sysCompany = sysCompanyInstanceRelatedEditDTO.getSysCompany();
        if (!ObjectUtils.isEmpty(sysCompanyInstanceRelatedEditDTO.getSysCompany())) {
            //获取企业信息
            SysCompany sysCompany1 = sysCompanyMapper.selectSysCompanyById(sysCompany.getId());
            BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(sysCompanyInstanceRelatedEditDTO.getProcessInstanceId());
            baProcessInstanceRelated.setBusinessData(JSONObject.toJSONString(sysCompany));
            try {
                baProcessInstanceRelatedMapper.updateBaProcessInstanceRelated(baProcessInstanceRelated);
            } catch (Exception e) {
                return 0;
            }
            return 1;
        }
        return 0;
    }

    @Override
    public List<SysCompany> getCompanyList(String tenantId) {
        return sysCompanyMapper.getCompanyList(tenantId);
    }
}
