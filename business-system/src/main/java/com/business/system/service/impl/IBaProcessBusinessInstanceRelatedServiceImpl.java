package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.BeanUtil;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;
import com.business.system.mapper.BaBankMapper;
import com.business.system.mapper.BaBillingInformationMapper;
import com.business.system.mapper.BaProcessInstanceRelatedMapper;
import com.business.system.mapper.BaSupplierMapper;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系接口 服务实现类
 * @date: 2022/12/5 15:12
 */
@Service
public class IBaProcessBusinessInstanceRelatedServiceImpl extends ServiceImpl<BaProcessInstanceRelatedMapper, BaProcessInstanceRelated> implements IBaProcessBusinessInstanceRelatedService {

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Override
    public Map<String, BaProcessBusinessInstanceRelatedVO> packageBusinessProcessInstanceRelatedMap(List<String> businessKeys) {
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listApplyBusinessDataByBusinessKeys(businessKeys);
        List<BaProcessBusinessInstanceRelatedVO> processBusinessInstanceRelatedVOs = beanUtil.listConvert(baProcessInstanceRelateds, BaProcessBusinessInstanceRelatedVO.class);
        return processBusinessInstanceRelatedVOs.stream().collect(Collectors.groupingBy(BaProcessBusinessInstanceRelatedVO::getBusinessKey,
                Collectors.collectingAndThen(Collectors.reducing((o1, o2) -> o1.getCreatedTime().isAfter(o2.getCreatedTime()) ? o1 : o2), Optional::get)));
    }

    @Override
    public BaProcessInstanceRelated getApplyBusinessDataByProcessInstanceId(String processInstanceId) {
        return baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(processInstanceId);
    }
}
