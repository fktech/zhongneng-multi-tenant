package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaSampleAssay;

/**
 * 取样化验Service接口
 *
 * @author ljb
 * @date 2024-01-06
 */
public interface IBaSampleAssayService
{
    /**
     * 查询取样化验
     *
     * @param id 取样化验ID
     * @return 取样化验
     */
    public BaSampleAssay selectBaSampleAssayById(String id);

    /**
     * 查询取样化验列表
     *
     * @param baSampleAssay 取样化验
     * @return 取样化验集合
     */
    public List<BaSampleAssay> selectBaSampleAssayList(BaSampleAssay baSampleAssay);

    /**
     * 新增取样
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int insertBaSampleAssay(BaSampleAssay baSampleAssay);

    /**
     * 新增化验
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int insertAssay(BaSampleAssay baSampleAssay);

    /**
     * 修改取样化验
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int updateBaSampleAssay(BaSampleAssay baSampleAssay);

    /**
     * 修改化验
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    public int updateAssay(BaSampleAssay baSampleAssay);

    /**
     * 批量删除取样化验
     *
     * @param ids 需要删除的取样化验ID
     * @return 结果
     */
    public int deleteBaSampleAssayByIds(String[] ids);

    /**
     * 删除取样化验信息
     *
     * @param id 取样化验ID
     * @return 结果
     */
    public int deleteBaSampleAssayById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revokeAssay(String id);


}
