package com.business.system.service;

import java.util.List;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.BaProject;
import com.business.system.domain.BaPurchaseOrder;

/**
 * 采购计划Service接口
 *
 * @author ljb
 * @date 2023-08-31
 */
public interface IBaProcurementPlanService
{
    /**
     * 查询采购计划
     *
     * @param id 采购计划ID
     * @return 采购计划
     */
    public BaProcurementPlan selectBaProcurementPlanById(String id);

    /**
     * 查询采购计划列表
     *
     * @param baProcurementPlan 采购计划
     * @return 采购计划集合
     */
    public List<BaProcurementPlan> selectBaProcurementPlanList(BaProcurementPlan baProcurementPlan);

    /**
     * 新增采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    public int insertBaProcurementPlan(BaProcurementPlan baProcurementPlan);

    /**
     * 修改采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    public int updateBaProcurementPlan(BaProcurementPlan baProcurementPlan);

    /**
     * 批量删除采购计划
     *
     * @param ids 需要删除的采购计划ID
     * @return 结果
     */
    public int deleteBaProcurementPlanByIds(String[] ids);

    /**
     * 删除采购计划信息
     *
     * @param id 采购计划ID
     * @return 结果
     */
    public int deleteBaProcurementPlanById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 关联项目
     */
    public int association(BaProcurementPlan baProcurementPlan);

    /**
     * 采购单下拉
     */
    public List<BaPurchaseOrder> dropDown(String projectId,String goodsId);

    /**
     * 采购清单
     */
    public List<BaPurchaseOrder> selectBaPurchaseOrderList(String projectId,String planId);

    /**
     * 采购申请名称
     */
    public UR purchaseRequestName(String projectId, String type);

    /**
     * 统计采购计划
     * @param baProcurementPlan
     * @return
     */
    public int countBaProcurementPlanList(BaProcurementPlan baProcurementPlan);

    /**
     * 查询多租户授权信息采购计划列表
     */
    public List<BaProcurementPlan> selectBaProcurementPlanAuthorizedList(BaProcurementPlan baProcurementPlan);

    /**
     * 授权采购计划
     */
    public int authorizedBaProcurementPlan(BaProcurementPlan baProcurementPlan);
}
