package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 盘点审批接口 服务类
 * @date: 2023/1/5 11:10
 */
@Service
public class ICheckedApprovalService {

    @Autowired
    protected BaCheckedHeadMapper baCheckedHeadMapper;

    @Autowired
    protected BaCheckedMapper baCheckedMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaChecked baChecked = new BaChecked();
        baChecked.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baChecked = baCheckedMapper.selectBaCheckedById(baChecked.getId());
        String userId = String.valueOf(baChecked.getUserId());
        if(AdminCodeEnum.CHECKED_STATUS_PASS.getCode().equals(status)){
            BaMaterialStock materialStock = null;
                    //获取盘点详情
            QueryWrapper<BaCheckedHead> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("checked_id", baChecked.getId());
            queryWrapper.eq("flag", "0");
            List<BaCheckedHead> baCheckedHeadList = baCheckedHeadMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(baCheckedHeadList)){
                for(BaCheckedHead baCheckedHead : baCheckedHeadList){
                    //根据盘点库存ID查询库存数据
                    materialStock = baMaterialStockMapper.selectBaMaterialStockById(baCheckedHead.getMaterialId());
                    if(!ObjectUtils.isEmpty(materialStock)){
                        //审批通过后更新盘点量为此总量
                        materialStock.setInventoryTotal(baCheckedHead.getCheckedCount());
                        //审批通过后更新库存均价为此盘点均价
                        materialStock.setInventoryAverage(baCheckedHead.getCheckedAverage());
                        //库存总量（吨）
                        if(materialStock.getInventoryTotal() != null){
                            if(materialStock.getInventoryTotal().intValue() == 0){
                                materialStock.setAverageCalorific(new BigDecimal(0));
                            } else if(materialStock.getAllAverageCalorific() != null){
                                //平均热值等于库存总热值除以库存总量
                                materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(),2,BigDecimal.ROUND_HALF_UP));
                            }
                        }
                        //计算盘点货值累计
                        if(baCheckedHead.getCheckedAverage() == null){
                            baCheckedHead.setCheckedAverage(new BigDecimal(0));
                        }
                        BigDecimal multiply = baCheckedHead.getCheckedAverage().multiply(baCheckedHead.getDifference().setScale(2, BigDecimal.ROUND_HALF_UP));
                        if(materialStock.getCheckedCargoValue() == null){
                            materialStock.setCheckedCargoValue(new BigDecimal(0));
                        }
                        //库存入库货值累计 + 本次入库货值
                        materialStock.setCheckedCargoValue(materialStock.getCheckedCargoValue().add(multiply));
                    }
                }

                //盘点计算货值均价
                if(materialStock.getCheckedCargoValue() == null){
                    materialStock.setCheckedCargoValue(new BigDecimal(0));
                }

                if(materialStock.getInventoryAverage() == null){
                    materialStock.setInventoryAverage(new BigDecimal(0));
                }
                if(materialStock.getInventoryTotal() == null){
                    materialStock.setInventoryTotal(new BigDecimal(0));
                }
                BigDecimal subtract = materialStock.getIntoDepotCargoValue()
                        .subtract(materialStock.getOutputDepotCargoValue())
                        .subtract(materialStock.getCheckedCargoValue());
                //计算库存均价
                if(subtract != null && materialStock.getInventoryTotal() != null && subtract.intValue() > 0 && materialStock.getInventoryTotal().intValue() > 0) {
                    materialStock.setInventoryAverage(subtract
                            .divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
                }
                baMaterialStockMapper.updateBaMaterialStock(materialStock);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baChecked, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.CHECKED_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage( baChecked, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baChecked, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baChecked.setState(status);
        baCheckedMapper.updateById(baChecked);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaChecked baChecked, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baChecked.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CHECKED.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"盘点审批提醒",msgContent,baMessage.getType(),baChecked.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaChecked checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaChecked baChecked = baCheckedMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baChecked == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CHECKED_001);
        }
        if (!AdminCodeEnum.CHECKED_STATUS_VERIFYING.getCode().equals(baChecked.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CHECKED_002);
        }
        return baChecked;
    }
}
