package com.business.system.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaAutomobileSettlementInvoice;

/**
 * 无车承运开票Service接口
 *
 * @author single
 * @date 2023-04-12
 */
public interface IBaAutomobileSettlementInvoiceService
{
    /**
     * 查询无车承运开票
     *
     * @param id 无车承运开票ID
     * @return 无车承运开票
     */
    public BaAutomobileSettlementInvoice selectBaAutomobileSettlementInvoiceById(String id);

    /**
     * 查询无车承运开票列表
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 无车承运开票集合
     */
    public List<BaAutomobileSettlementInvoice> selectBaAutomobileSettlementInvoiceList(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 新增无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    public int insertBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 修改无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    public int updateBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice);

    /**
     * 批量删除无车承运开票
     *
     * @param ids 需要删除的无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceByIds(String[] ids);

    /**
     * 删除无车承运开票信息
     *
     * @param id 无车承运开票ID
     * @return 结果
     */
    public int deleteBaAutomobileSettlementInvoiceById(String id);

    /**
     * 昕科推送发票（发票信息）
     */
    int addAutomobileSettlementInvoice(JSONObject automobileSettlementInvoice);

    /**
     * 中储推送发票（发票信息）
     */
    int added(JSONObject automobileSettlementInvoice);

    /**
     * 修改无车承运开票
     *
     * @param object 无车承运开票
     * @return 结果
     */
    public int updateInvoice(JSONObject object);

    /**
     * 查询开票明细
     */
    public int selectInvoice(String invoiceNo);
}
