package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaGoodsService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 商品Service业务层处理
 *
 * @author ljb
 * @date 2022-12-01
 */
@Service
public class BaGoodsServiceImpl extends ServiceImpl<BaGoodsMapper, BaGoods> implements IBaGoodsService
{
    private static final Logger log = LoggerFactory.getLogger(BaGoodsServiceImpl.class);
    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private BaUnitMapper baUnitMapper;

    @Autowired
    private BaGoodsAddMapper baGoodsAddMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;


    /**
     * 查询商品
     *
     * @param id 商品ID
     * @return 商品
     */
    @Override
    public BaGoods selectBaGoodsById(String id)
    {
        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(id);
        //发起人
        SysUser user = sysUserMapper.selectUserById(baGoods.getUserId());
        user.setUserName(user.getNickName());
        //岗位名称
        String name = getName.getName(user.getUserId());
        if(name.equals("") == false){
            user.setPostName(name.substring(0,name.length()-1));
        }
        baGoods.setUser(user);
        //商品计量单位
        if(StringUtils.isNotEmpty(baGoods.getUnitId())){
           BaUnit baUnit = baUnitMapper.selectBaUnitById(baGoods.getUnitId());
           baGoods.setUnit(baUnit.getName());
        }
        //商品分类
        if(StringUtils.isNotEmpty(baGoods.getGoodsType())){
           BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baGoods.getGoodsType());
           if(StringUtils.isNotNull(baGoodsType)){
               baGoods.setGoodsTypeName(baGoodsType.getName());
           }
        }
        //查询商品所有的附加信息
        QueryWrapper<BaGoodsAdd> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("goods_id",baGoods.getId());
        queryWrapper.eq("flag",0);
        List<BaGoodsAdd> baGoodsAddList = baGoodsAddMapper.selectList(queryWrapper);
        baGoods.setGoodsAddList(baGoodsAddList);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",baGoods.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baGoods.setProcessInstanceId(processInstanceIds);
        return baGoods;
    }

    /**
     * 查询商品列表
     *
     * @param baGoods 商品
     * @return 商品
     */
    @Override
    public List<BaGoods> selectBaGoodsList(BaGoods baGoods)
    {
        QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
        //商品编码查询
        if(StringUtils.isNotEmpty(baGoods.getCode())){
            queryWrapper.like("code",baGoods.getCode());
        }
        //商品名称查询
        if(StringUtils.isNotEmpty(baGoods.getName())){
            queryWrapper.like("name",baGoods.getName());
        }
        //商品型号查询
        if(StringUtils.isNotEmpty(baGoods.getModel())){
            queryWrapper.eq("model",baGoods.getModel());
        }
        //商品类型查询
        if(StringUtils.isNotEmpty(baGoods.getGoodsType())){
            BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baGoods.getGoodsType());
            //判断是否是一级分类
            if(baGoodsType.getParentId().equals("0")){
             QueryWrapper<BaGoodsType> typeQueryWrapper = new QueryWrapper<>();
             typeQueryWrapper.eq("parent_id",baGoodsType.getId());
             //查询所有的二级分类
            List<BaGoodsType> baGoodsTypeList = baGoodsTypeMapper.selectList(typeQueryWrapper);

                if(StringUtils.isNotEmpty(baGoodsTypeList)){
                  List<String> ids = new ArrayList<>();
                    for (BaGoodsType type:baGoodsTypeList) {
                        ids.add(type.getId());
                    }
                        ids.add(baGoodsType.getId());
                    queryWrapper.in("goods_type",ids);
                }else {
                    queryWrapper.eq("goods_type",baGoodsType.getId());
                }

            }else {
                queryWrapper.eq("goods_type",baGoods.getGoodsType());
            }
        }
        //供应商查询
        if(StringUtils.isNotEmpty(baGoods.getSupplierId())){
            queryWrapper.eq("supplier_id",baGoods.getSupplierId());
        }
        //审批状态查询
        if(!"all".equals(baGoods.getState())){
            queryWrapper.eq("state",baGoods.getState());
        }
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        queryWrapper.eq("flag","0");
        queryWrapper.orderByDesc("goods_type","initiation_time");
       List<BaGoods> baGoodsList = baGoodsMapper.selectList(queryWrapper);
        for (BaGoods goods:baGoodsList) {
            //岗位信息
            String name = getName.getName(goods.getUserId());
            //发起人名称
            if(name.equals("") == false){
                goods.setUserName(sysUserMapper.selectUserById(goods.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                goods.setUserName(sysUserMapper.selectUserById(goods.getUserId()).getNickName());
            }
           BaUnit baUnit = baUnitMapper.selectBaUnitById(goods.getUnitId());
           //计量单位
           if(StringUtils.isNotNull(baUnit)){
               goods.setUnit(baUnit.getName());
           }
           BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(goods.getGoodsType());
           if(StringUtils.isNotNull(baGoodsType)){
               goods.setGoodsType(baGoodsType.getName());
           }else {
               goods.setGoodsType("");
           }
        }
        return baGoodsList;
    }

    /**
     * 新增商品
     *
     * @param baGoods 商品
     * @return 结果
     */
    @Override
    public int insertBaGoods(BaGoods baGoods)
    {
        //判断商品编码是否存在
        /*QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("code",baGoods.getCode());
        queryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaGoods codeResult = baGoodsMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(codeResult)){
            return -2;
        }*/
        //判断商品名称是否重复
        QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name",baGoods.getName());
        queryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
        }
        BaGoods nameResult = baGoodsMapper.selectOne(queryWrapper);
        if(StringUtils.isNotNull(nameResult)){
            return -1;
        }

        //没有选择分类
        if (StringUtils.isEmpty(baGoods.getGoodsType()))
            baGoods.setGoodsType("未选择");

        baGoods.setId(getRedisIncreID.getId());
        baGoods.setCreateTime(DateUtils.getNowDate());
        baGoods.setCreateBy(SecurityUtils.getUsername());
        baGoods.setUserId(SecurityUtils.getUserId());
        baGoods.setDeptId(SecurityUtils.getDeptId());
        baGoods.setCode(this.goodsSerial());
        //租户ID
        baGoods.setTenantId(SecurityUtils.getCurrComId());
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baGoods.getWorkState())) {
            //流程发起状态
            baGoods.setWorkState("1");
            //默认审批流程已通过
            baGoods.setState(AdminCodeEnum.GOODS_STATUS_PASS.getCode());
            //新增规格
            if (StringUtils.isNotNull(baGoods.getGoodsAddList()))
                for (BaGoodsAdd baGoodsAdd:baGoods.getGoodsAddList()) {
                    baGoodsAdd.setId(getRedisIncreID.getId());
                    baGoodsAdd.setGoodsId(baGoods.getId());
                               /* baGoodsAdd.setInformationName(baGoods.getInformationName());
                                baGoodsAdd.setInformationValue(baGoods.getInformationValue());
                                baGoodsAdd.setRemarks(baGoods.getAddRemarks());*/
                    baGoodsAdd.setCreateTime(DateUtils.getNowDate());
                    baGoodsAdd.setCreateBy(SecurityUtils.getUsername());
                    baGoodsAddMapper.insertBaGoodsAdd(baGoodsAdd);
                }
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode(),SecurityUtils.getCurrComId());
        baGoods.setFlowId(flowId);
        Integer result = baGoodsMapper.insertBaGoods(baGoods);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baGoods.getWorkState())) {
                BaGoods goods = baGoodsMapper.selectBaGoodsById(baGoods.getId());
                //启动流程实例
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(goods, AdminCodeEnum.GOODS_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    baGoodsMapper.deleteBaGoodsById(goods.getId());
                    return 0;
                }else {
                    goods.setState(AdminCodeEnum.GOODS_STATUS_VERIFYING.getCode());
                    if (StringUtils.isNotNull(baGoods.getGoodsAddList()))
                        for (BaGoodsAdd baGoodsAdd:baGoods.getGoodsAddList()) {
                            baGoodsAdd.setId(getRedisIncreID.getId());
                            baGoodsAdd.setGoodsId(baGoods.getId());
                               /* baGoodsAdd.setInformationName(baGoods.getInformationName());
                                baGoodsAdd.setInformationValue(baGoods.getInformationValue());
                                baGoodsAdd.setRemarks(baGoods.getAddRemarks());*/
                            baGoodsAdd.setCreateTime(DateUtils.getNowDate());
                            baGoodsAdd.setCreateBy(SecurityUtils.getUsername());
                            baGoodsAddMapper.insertBaGoodsAdd(baGoodsAdd);
                        }
                    baGoodsMapper.updateBaGoods(goods);
                }
            }
        }
        return result;
    }

    /**
     * 提交用印审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaGoods goods, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(goods.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(goods.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaGoods baGoods = this.selectBaGoodsById(goods.getId());
        SysUser sysUser = iSysUserService.selectUserById(baGoods.getUserId());
        baGoods.setUserName(sysUser.getUserName());
        baGoods.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baGoods, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.GOODS_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改商品
     *
     * @param baGoods 商品
     * @return 结果
     */
    @Override
    public int updateBaGoods(BaGoods baGoods)
    {
        //原数据
        BaGoods baGoods1 = baGoodsMapper.selectBaGoodsById(baGoods.getId());
        //判断商品编码是否相等
        if(StringUtils.isNotEmpty(baGoods.getCode())){
            if(baGoods1.getCode().equals(baGoods.getCode()) == false){
                QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("code",baGoods.getCode());
                queryWrapper.eq("flag",0);
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
                }
                BaGoods codeResult = baGoodsMapper.selectOne(queryWrapper);
                if(StringUtils.isNotNull(codeResult)){
                    return -2;
                }
            }
        }
        //判断商品名称是否相等
        if(StringUtils.isNotEmpty(baGoods.getName())){
            if(baGoods1.getName().equals(baGoods.getCode()) == false){
                QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("name",baGoods.getCode());
                queryWrapper.eq("flag",0);
                //租户ID
                if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
                    queryWrapper.eq("tenant_id", SecurityUtils.getCurrComId());
                }
                BaGoods nameResult = baGoodsMapper.selectOne(queryWrapper);
                if(StringUtils.isNotNull(nameResult)){
                    return -1;
                }
            }
        }
        if(baGoods1.getState().equals(AdminCodeEnum.GOODS_STATUS_PASS.getCode())){
            //删除商品下所有的规格
            QueryWrapper<BaGoodsAdd> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("goods_id",baGoods.getId());
            List<BaGoodsAdd> baGoodsAddList = baGoodsAddMapper.selectList(queryWrapper);
            for (BaGoodsAdd baGoodsAdd:baGoodsAddList) {
                baGoodsAddMapper.deleteBaGoodsAddById(baGoodsAdd.getId());
            }
            baGoods.setUpdateTime(DateUtils.getNowDate());
            baGoods.setUpdateBy(SecurityUtils.getUsername());
            if(StringUtils.isNotNull(baGoods.getGoodsAddList())){
                for (BaGoodsAdd baGoodsAdd:baGoods.getGoodsAddList()) {
                    baGoodsAdd.setId(getRedisIncreID.getId());
                    baGoodsAdd.setGoodsId(baGoods.getId());
                    baGoodsAdd.setCreateTime(DateUtils.getNowDate());
                    baGoodsAdd.setCreateBy(SecurityUtils.getUsername());
                    baGoodsAddMapper.insertBaGoodsAdd(baGoodsAdd);
                }
            }
        }
        int result = baGoodsMapper.updateBaGoods(baGoods);
        if(result > 0 && !baGoods1.getState().equals(AdminCodeEnum.GOODS_STATUS_PASS.getCode())){
            BaGoods goods = baGoodsMapper.selectBaGoodsById(baGoods.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("business_id",goods.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper2);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(goods, AdminCodeEnum.GOODS_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baGoodsMapper.updateBaGoods(baGoods1);
                return 0;
            }else {
                goods.setState(AdminCodeEnum.GOODS_STATUS_VERIFYING.getCode());

                //删除商品下所有的规格
                QueryWrapper<BaGoodsAdd> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("goods_id",baGoods.getId());
                List<BaGoodsAdd> baGoodsAddList = baGoodsAddMapper.selectList(queryWrapper);
                for (BaGoodsAdd baGoodsAdd:baGoodsAddList) {
                    baGoodsAddMapper.deleteBaGoodsAddById(baGoodsAdd.getId());
                }
                baGoods.setUpdateTime(DateUtils.getNowDate());
                baGoods.setUpdateBy(SecurityUtils.getUsername());
                if(StringUtils.isNotNull(baGoods.getGoodsAddList())){
                    for (BaGoodsAdd baGoodsAdd:baGoods.getGoodsAddList()) {
                        baGoodsAdd.setId(getRedisIncreID.getId());
                        baGoodsAdd.setGoodsId(baGoods.getId());
                        baGoodsAdd.setCreateTime(DateUtils.getNowDate());
                        baGoodsAdd.setCreateBy(SecurityUtils.getUsername());
                        baGoodsAddMapper.insertBaGoodsAdd(baGoodsAdd);
                    }
                }

                baGoodsMapper.updateBaGoods(goods);
            }
        }
        return result;
    }

    /**
     * 批量删除商品
     *
     * @param ids 需要删除的商品ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsByIds(String[] ids)
    {
        //伪删除
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaGoods baGoods = baGoodsMapper.selectBaGoodsById(id);
               baGoods.setFlag(1);
               baGoodsMapper.updateBaGoods(baGoods);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除商品信息
     *
     * @param id 商品ID
     * @return 结果
     */
    @Override
    public int deleteBaGoodsById(String id)
    {
        return baGoodsMapper.deleteBaGoodsById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baGoods.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.GOODS_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baGoods.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baGoods.setState(AdminCodeEnum.GOODS_STATUS_WITHDRAW.getCode());
                baGoodsMapper.updateBaGoods(baGoods);
                String userId = String.valueOf(baGoods.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baGoods, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaGoods baGoods, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baGoods.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_GOODS.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    public String goodsSerial() {
        QueryWrapper<BaGoods> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq("project_type","4");
        queryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        queryWrapper.orderByDesc("create_time");
        List<BaGoods> baGoodsList = baGoodsMapper.selectList(queryWrapper);
        //序号
        String num = "";
        if(baGoodsList.size() > 0){
            //判断立项序号是否为空或
            if(StringUtils.isNotEmpty(baGoodsList.get(0).getCode())){
                int number = Integer.parseInt(baGoodsList.get(0).getCode());
                int serial = number + 1;
                if(String.valueOf(serial).length() == 1){
                    num = "000"+ serial;
                }else if(String.valueOf(serial).length() == 2){
                    num = "00"+ serial;
                }else if(String.valueOf(serial).length() == 3){
                    num = "0"+ serial;
                }else if(String.valueOf(serial).length() == 4){
                    num =  String.valueOf(serial);
                }
            }
        }else {
            return "0001";
        }
        return num;
    }

}
