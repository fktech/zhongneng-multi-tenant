package com.business.system.service.impl;

import cn.hutool.core.io.unit.DataUnit;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaCollectionStatVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 收款信息Service业务层处理
 *
 * @author single
 * @date 2022-12-26
 */
@Service
public class BaCollectionServiceImpl extends ServiceImpl<BaCollectionMapper, BaCollection> implements IBaCollectionService
{
    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaProjectMapper projectMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaClaimMapper baClaimMapper;

    @Autowired
    private BaClaimHistoryMapper baClaimHistoryMapper;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private PostName getName;

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    private static final Logger log = LoggerFactory.getLogger(BaCollectionServiceImpl.class);

    /**
     * 查询收款信息
     *
     * @param id 收款信息ID
     * @return 收款信息
     */
    @Override
    public BaCollection selectBaCollectionById(String id)
    {
        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
        //查询结算单信息
        /*if(StringUtils.isNotEmpty(baCollection.getSettlementId())){
            baCollection.setBaSettlement(baSettlementMapper.selectBaSettlementById(baCollection.getSettlementId()));
        }
        //账户信息
        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baCollection.getContractId());
        queryWrapper.eq("flag",0);
        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
        baCollection.setBankList(baBanks);

        //终端企业（用煤单位）
        if(StringUtils.isNotEmpty(baCollection.getPaymentCompany())){
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baCollection.getPaymentCompany());
            if(StringUtils.isNotNull(enterprise)){
                baCollection.setPaymentCompanyName(enterprise.getName());
            }
        }
        //供应商
        if(StringUtils.isNotEmpty(baCollection.getCollectionCompany())){
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baCollection.getCollectionCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baCollection.setCollectionCompanyName(baSupplier.getName());
            }
        }
        //转company
        }*/
        //付款单位
        if(!ObjectUtils.isEmpty(baCollection)){
            if(StringUtils.isNotEmpty(baCollection.getPaymentCompany())) {
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baCollection.getPaymentCompany());
                if (StringUtils.isNotNull(enterprise)) {
                    //税号查询
                    QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
                    informationQueryWrapper.eq("relevance_id",enterprise.getId());
                    BaBillingInformation baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
                    baCollection.setPaymentCompanyName(enterprise.getName());
                    baCollection.setTaxNum(baBillingInformation.getDutyParagraph());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baCollection.getPaymentCompany());
                if (StringUtils.isNotNull(baSupplier)) {
                    baCollection.setPaymentCompanyName(baSupplier.getName());
                    baCollection.setTaxNum(baSupplier.getTaxNum());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baCollection.getPaymentCompany());
                if (StringUtils.isNotNull(baCompany)) {
                    baCollection.setPaymentCompanyName(baCompany.getName());
                    baCollection.setTaxNum(baCompany.getTaxNum());
                }
            }
            //开户行信息
            if(StringUtils.isNotEmpty(baCollection.getBankId())){
                BaBank baBank = baBankMapper.selectBaBankById(baCollection.getBankId());
                baCollection.setBank(baBank);
            }
    }

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baCollection.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baCollection.setUser(sysUser);
        //发起人
        baCollection.setUserName(sysUser.getNickName());
        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baCollection.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }

        //认领记录
        List<BaClaim> baClaims = baClaimMapper.selectBaClaimList(new BaClaim().setRelationId(id));
        if(StringUtils.isNotNull(baClaims)){
            for (BaClaim baClaim:baClaims) {
                baClaim.setUserName(sysUserMapper.selectUserById(baClaim.getUserId()).getNickName());
                //查询对应历史数据
                QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                baClaimHistoryQueryWrapper.eq("claim_id",baClaim.getId());
                List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                //认领记录明细
                List<BaContractStart> baContractStartList = new ArrayList<>();
                for (BaClaimHistory baClaimHistory:baClaimHistories) {
                    //合同启动信息
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baClaimHistory.getOrderId());
                    baContractStart.setCurrentCollection(baClaimHistory.getCurrentCollection());
                    baContractStart.setHistoryId(baClaimHistory.getId());
                    baContractStartList.add(baContractStart);
                }
                baClaim.setBaContractStartList(baContractStartList);
            }
            baCollection.setBaClaims(baClaims);
        }
        //合同启动
        if(StringUtils.isNotEmpty(baCollection.getStartId())){
            BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baCollection.getStartId());
            baCollection.setStartName(contractStart.getName());
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baCollection.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baCollection.setProcessInstanceId(processInstanceIds);


        return baCollection;
    }

    @Override
    public BigDecimal selectBaCollectionByyear() {
        BaCollection baCollection=new BaCollection();
        baCollection.setState("claim:pass");
        baCollection.setFlag(0L);
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal decimal = baCollectionMapper.selectBaCollectionByyear(baCollection);
        return decimal;
    }

    @Override
    public List<BaCollection> selectByyearandbiusstype(BaCollection baCollection) {
        return baCollectionMapper.selectByyearandbiusstype(baCollection);
    }

    /**
     * 查询收款信息列表
     *
     * @param baCollection 收款信息
     * @return 收款信息
     */
    @Override
    public List<BaCollection> selectBaCollectionList(BaCollection baCollection)
    {
        List<BaCollection> baCollections = baCollectionMapper.selectBaCollectionList(baCollection);
        baCollections.stream().forEach(item -> {
            if("1".equals(item.getType())){
                item.setApplyCollectionAmount(item.getActualCollectionAmount());
            }
            //转开户号
            BaBank baBank = baBankMapper.selectBaBankById(item.getOtherAccount());
            if(StringUtils.isNotNull(baBank)){
                item.setOtherAccount(baBank.getAccount());
            }
            item.setUserName(sysUserMapper.selectUserByUserName(item.getCreateBy()).getNickName());
            //终端企业（用煤单位）
            if(StringUtils.isNotEmpty(item.getPaymentCompany())){
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getPaymentCompany());
                if(StringUtils.isNotNull(enterprise)){
                    item.setPaymentCompanyName(enterprise.getName());
                }
            }
            //供应商
            if(StringUtils.isNotEmpty(item.getCollectionCompany())){
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getCollectionCompany());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setCollectionCompanyName(baSupplier.getName());
                }
            }
            //转company
            if(StringUtils.isNotEmpty(item.getCompany())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getCompany());
                if(StringUtils.isNotNull(enterprise)){
                    item.setCompany(enterprise.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getCompany());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setCompany(baSupplier.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getCompany());
                if(StringUtils.isNotNull(baCompany)){
                    item.setCompany(baCompany.getName());
                }
            }
        });
        return baCollections;
    }

    @Override
    public List<BaCollectionVoucherDTO> claimCollection(BaCollection baCollection) {
        List<BaCollectionVoucherDTO> baCollectionVoucherDTOS = baCollectionMapper.selectCollection(baCollection);
        if(StringUtils.isNotNull(baCollectionVoucherDTOS)){
            for (BaCollectionVoucherDTO baCollectionVoucherDTO:baCollectionVoucherDTOS) {
                //岗位信息
                String name = getName.getName(Long.valueOf(baCollectionVoucherDTO.getUserId()));
                //发起人
                if(name.equals("") == false){
                    baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName());
                }

                //付款账户
                if(StringUtils.isNotEmpty(baCollectionVoucherDTO.getBankId())){
                    BaBank baBank = baBankMapper.selectBaBankById(baCollectionVoucherDTO.getBankId());
                    baCollectionVoucherDTO.setPaymentAccount(baBank.getAccount());
                }
                //认领记录
                /*QueryWrapper<BaClaim> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id",baCollectionVoucherDTO.getId());
                List<BaClaim> baClaimList = baClaimMapper.selectList(queryWrapper);
                for (BaClaim baClaim:baClaimList) {
                    List<BaContractStart> baContractStartList = new ArrayList<>();
                    //查询对应历史数据
                    QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                    baClaimHistoryQueryWrapper.eq("claim_id",baClaim.getId());
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                    for (BaClaimHistory baClaimHistory:baClaimHistories) {
                        //合同启动信息
                        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baClaimHistory.getOrderId());
                        baContractStart.setCurrentCollection(baClaimHistory.getCurrentCollection());
                        baContractStart.setHistoryId(baClaimHistory.getId());
                        baContractStartList.add(baContractStart);
                    }
                    baClaim.setBaContractStartList(baContractStartList);
                }
                baCollectionVoucherDTO.setBaClaimList(baClaimList);*/
            }

        }
        return baCollectionVoucherDTOS;
    }

    @Override
    public UR statistics(BaCollection baCollection) {
        List<BaCollectionVoucherDTO> baCollectionVoucherDTOS = baCollectionMapper.selectCollection(baCollection);
        Map<String,BigDecimal> map = new HashMap<>();
        //收款金额总和
        BigDecimal collectionTotal = new BigDecimal(0);
        //已认领金额总和
        BigDecimal claimedTotal = new BigDecimal(0);
        for (BaCollectionVoucherDTO baCollectionVoucherDTO:baCollectionVoucherDTOS) {
            //已认领金额
            if(baCollectionVoucherDTO.getApplyCollectionAmount() != null){
                collectionTotal = collectionTotal.add(baCollectionVoucherDTO.getApplyCollectionAmount());
            }
            //已认领金额
            if(baCollectionVoucherDTO.getClaimed() != null){
                claimedTotal = claimedTotal.add(baCollectionVoucherDTO.getClaimed());
            }
        }
        map.put("收款金额总和",collectionTotal);
        map.put("已认领金额总和",claimedTotal);
        map.put("未认领金额总和",collectionTotal.subtract(claimedTotal));
        return UR.ok().data("map",map);
    }
   /* @Override
    public List<BaCollectionVoucherDTO> claimCollectionInWx(BaCollection baCollection) {
        //通过合同启动名称查询认领记录
        List<BaCollectionVoucherDTO> baCollectionVoucherDTOS=null;
            baCollectionVoucherDTOS = baCollectionMapper.selectBaClaimHistoryListBycontractName(baCollection);
            if(StringUtils.isNotNull(baCollectionVoucherDTOS)){
                for (BaCollectionVoucherDTO baCollectionVoucherDTO:baCollectionVoucherDTOS) {
                    //岗位信息
                    String name = getName.getName(Long.valueOf(baCollectionVoucherDTO.getUserId()));
                    //发起人
                    if(name.equals("") == false){
                        baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName()+"-"+name.substring(0,name.length()-1));
                    }else {
                        baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName());
                    }

                    //付款账户
                    if(StringUtils.isNotEmpty(baCollectionVoucherDTO.getBankId())){
                        BaBank baBank = baBankMapper.selectBaBankById(baCollectionVoucherDTO.getBankId());
                        baCollectionVoucherDTO.setPaymentAccount(baBank.getAccount());
                    }
                }
            }

        return baCollectionVoucherDTOS;
    }
*/
    /**
     * 新增收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    @Override
    public int insertBaCollection(BaCollection baCollection)
    {
        baCollection.setId(getRedisIncreID.getId());
        baCollection.setCreateTime(DateUtils.getNowDate());
        baCollection.setCreateBy(SecurityUtils.getUsername());
        baCollection.setUpdateBy(SecurityUtils.getUsername());
        baCollection.setUpdateTime(DateUtils.getNowDate());
        baCollection.setUserId(SecurityUtils.getUserId());
        baCollection.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        //银行信息
        if(StringUtils.isNotEmpty(baCollection.getBankId())){
            BaBank baBank = baBankMapper.selectBaBankById(baCollection.getBankId());
            baCollection.setOtherBank(baBank.getBankName());
            if(StringUtils.isNotEmpty(baCollection.getCompany())){
                if(baCollection.getCompany().equals(baBank.getCompanyName()) == false){
                    baCollection.setCompany(baCollection.getCompany());
                }else {
                    baCollection.setCompany(baBank.getCompanyName());
                }
            }else {
                baCollection.setCompany(baBank.getCompanyName());
            }
            BaBank baBank1 = baBankMapper.selectBaBankById(baBank.getAccount());
            if(StringUtils.isNotNull(baBank1)){
                baCollection.setOtherAccount(baBank1.getAccount());
            }
            baCollection.setOtherAccount(baBank.getAccount());
            baCollection.setOtherAccountName(baBank.getBankName());
        }
        //company转名称
        if(StringUtils.isNotEmpty(baCollection.getCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baCollection.getCompany());
            if(StringUtils.isNotNull(enterprise)){
                baCollection.setCompany(enterprise.getName());
            }
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baCollection.getCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baCollection.setCompany(baSupplier.getName());
            }
            //公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baCollection.getCompany());
            if(StringUtils.isNotNull(baCompany)){
                baCollection.setCompany(baCompany.getName());
            }
            if(StringUtils.isNull(enterprise) && StringUtils.isNull(baSupplier) && StringUtils.isNull(baCompany)){
                baCollection.setCompany(baCollection.getCompany());
            }
        }
        //订单付款信息
        if(StringUtils.isNotNull(baCollection.getBaOrderDTOs())) {
            BaSettlementDetail baSettlementDetail = new BaSettlementDetail();
            for (BaOrderDTO baOrder : baCollection.getBaOrderDTOs()) {
                //订单添加历史数据
                baSettlementDetail.setId(getRedisIncreID.getId());
                baSettlementDetail.setRelationId(baCollection.getId());
                baSettlementDetail.setOrderId(baOrder.getId());
                baSettlementDetail.setAmount(baOrder.getActualPayment());
                baSettlementDetail.setSettlementId(baOrder.getSettlementId());
                baSettlementDetail.setCreateTime(DateUtils.getNowDate());
                baSettlementDetail.setType("2");
                baSettlementDetailMapper.insertBaSettlementDetail(baSettlementDetail);
            }
        }

        //流程发起状态
        baCollection.setWorkState("1");
        //默认审批流程已通过
        baCollection.setState(AdminCodeEnum.COLLECTION_STATUS_PASS.getCode());

        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode(),SecurityUtils.getCurrComId());
        baCollection.setFlowId(flowId);
        Integer result = baCollectionMapper.insertBaCollection(baCollection);
//        if(result > 0){
//            baCollection = baCollectionMapper.selectBaCollectionById(baCollection.getId());
//            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
//            baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baCollection, AdminCodeEnum.COLLECTION_APPROVAL_CONTENT_INSERT.getCode());
//            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
//                baCollectionMapper.deleteBaCollectionById(baCollection.getId());
//                return 0;
//            }else {
//                BaCollection baCollection2 = baCollectionMapper.selectBaCollectionById(baCollection.getId());
//                baCollection2.setState(AdminCodeEnum.COLLECTION_STATUS_VERIFYING.getCode());
//                baCollection2.setCollectionState(AdminCodeEnum.COLLECTION_TO_BE.getCode());
//                baCollectionMapper.updateBaCollection(baCollection2);
//                if(StringUtils.isNotEmpty(baCollection2.getSettlementId())){
//                    BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(baCollection2.getSettlementId());
//                    baSettlement.setSettlementState("2");
//                    baSettlementMapper.updateBaSettlement(baSettlement);
//                }
//            }
//        }
        return 1;
    }


    /**
     * 收款提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaCollection collection, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(collection.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(collection.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode());
        //获取流程实例关联的业务对象
        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(collection.getId());
        SysUser sysUser = iSysUserService.selectUserById(baCollection.getUserId());
        baCollection.setUserName(sysUser.getUserName());
        baCollection.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baCollection));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);

        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO, baCollection);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime, BaCollection collection) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.COLLECTION_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改收款信息
     *
     * @param baCollection 收款信息
     * @return 结果
     */
    @Override
    public int updateBaCollection(BaCollection baCollection)
    {
        baCollection.setUpdateTime(DateUtils.getNowDate());
        baCollection.setUpdateBy(SecurityUtils.getUsername());
        if(StringUtils.isNotEmpty(baCollection.getBankId())){
            BaBank baBank = baBankMapper.selectBaBankById(baCollection.getBankId());
            baCollection.setOtherBank(baBank.getBankName());
            baCollection.setCompany(baBank.getCompanyName());
            baCollection.setOtherAccount(baBank.getAccount());
            baCollection.setOtherAccountName(baBank.getBankName());
        }
        //company转名称
        if(StringUtils.isNotEmpty(baCollection.getCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baCollection.getCompany());
            if(StringUtils.isNotNull(enterprise)){
                baCollection.setCompany(enterprise.getName());
            }
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baCollection.getCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baCollection.setCompany(baSupplier.getName());
            }
            //公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baCollection.getCompany());
            if(StringUtils.isNotNull(baCompany)){
                baCollection.setCompany(baCompany.getName());
            }
            if(StringUtils.isNull(enterprise) && StringUtils.isNull(baSupplier) && StringUtils.isNull(baCompany)){
                baCollection.setCompany(baCollection.getCompany());
            }
        }
        //订单收款信息
        if(StringUtils.isNotNull(baCollection.getBaOrderDTOs())) {
            for (BaOrderDTO baOrder : baCollection.getBaOrderDTOs()) {
                BaSettlementDetail baSettlementDetail = baSettlementDetailMapper.selectBaSettlementDetailById(baOrder.getDetailId());
                baSettlementDetail.setAmount(baOrder.getActualPayment());
                baSettlementDetailMapper.updateBaSettlementDetail(baSettlementDetail);
            }
        }
        //流程实例ID
        if(StringUtils.isNotEmpty(baCollection.getState())){
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode(),SecurityUtils.getCurrComId());
            baCollection.setFlowId(flowId);
        }
        Integer result = baCollectionMapper.updateBaCollection(baCollection);
        if(result > 0 && StringUtils.isNotEmpty(baCollection.getState())){
            BaCollection collection = baCollectionMapper.selectBaCollectionById(baCollection.getId());
            //查询流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapperRelated = new QueryWrapper<>();
            queryWrapperRelated.eq("business_id",collection.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapperRelated);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(collection, AdminCodeEnum.CLAIM_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                collection.setState(AdminCodeEnum.CLAIM_STATUS_VERIFYING.getCode());
                baCollectionMapper.updateBaCollection(collection);
            }
        }
        return result;
    }

    /**
     * 批量删除收款信息
     *
     * @param ids 需要删除的收款信息ID
     * @return 结果
     */
    @Override
    public int deleteBaCollectionByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
                baCollection.setFlag(new Long(1));
                if(baCollection.getType().equals("3")){
                    //查询认领数据
                    QueryWrapper<BaClaim> claimQueryWrapper = new QueryWrapper<>();
                    claimQueryWrapper.eq("relation_id",id);
                    List<BaClaim> baClaims = baClaimMapper.selectList(claimQueryWrapper);
                    for (BaClaim baClaim:baClaims) {
                        //查询历史数据
                        QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                        baClaimHistoryQueryWrapper.eq("claim_id",baClaim.getId());
                        List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                        for (BaClaimHistory baClaimHistory:baClaimHistories) {
                            //删除历史数据
                            baClaimHistoryMapper.deleteBaClaimHistoryById(baClaimHistory.getId());
                        }
                        //删除认领数据
                        baClaimMapper.deleteBaClaimById(baClaim.getId());
                    }
                }
                //删除收款订单历史数据
                QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id",id);
                List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper);
                if(StringUtils.isNotNull(baSettlementDetailList)){
                    for (BaSettlementDetail baSettlementDetail:baSettlementDetailList) {
                        baSettlementDetailMapper.deleteBaSettlementDetailById(baSettlementDetail.getId());
                    }
                }
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapperRelated = new QueryWrapper<>();
                queryWrapperRelated.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapperRelated);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                baCollectionMapper.updateBaCollection(baCollection);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除收款信息信息
     *
     * @param id 收款信息ID
     * @return 结果
     */
    @Override
    public int deleteBaCollectionById(String id)
    {
        return baCollectionMapper.deleteBaCollectionById(id);
    }

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询收收款信息
            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baCollection.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.COLLECTION_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baCollection.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //收款流程审批状态修改
                baCollection.setState(AdminCodeEnum.COLLECTION_STATUS_WITHDRAW.getCode());

                baCollectionMapper.updateBaCollection(baCollection);
                String userId = String.valueOf(baCollection.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baCollection, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }


    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaCollection baCollection, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baCollection.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"收款审批提醒",msgContent,baMessage.getType(),baCollection.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public int insertBaCollectionVoucher(BaCollectionVoucherDTO baCollectionVoucherDTO) {
        //查询收收款信息
        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(baCollectionVoucherDTO.getId());
        baCollection.setCollectionDate(baCollectionVoucherDTO.getCollectionDate());
        baCollection.setCollectionType(baCollectionVoucherDTO.getCollectionType());
        baCollection.setPaymentCompany(baCollectionVoucherDTO.getPaymentCompany());
        baCollection.setCompany(baCollectionVoucherDTO.getPaymentCompany());
        baCollection.setPaymentAccount(baCollectionVoucherDTO.getPaymentAccount());
        baCollection.setCollectionAccount(baCollectionVoucherDTO.getCollectionAccount());
        baCollection.setCollectionCompany(baCollectionVoucherDTO.getCollectionCompany());
        baCollection.setCollectionVoucher(baCollectionVoucherDTO.getCollectionVoucher());
        baCollection.setVoucherRemark(baCollectionVoucherDTO.getVoucherRemark());
        baCollection.setCollectionState(AdminCodeEnum.COLLECTION_RECEIVED.getCode());
        Integer result = baCollectionMapper.updateBaCollection(baCollection);
        if(result > 0){
            //更新项目阶段
            if(StringUtils.isNotEmpty(baCollection.getContractId())){
                //合同信息
                BaContract baContract = baContractMapper.selectBaContractById(baCollection.getContractId());
                if(StringUtils.isNotEmpty(baContract.getProjectId())){
                    //项目信息
                    BaProject baProject = projectMapper.selectBaProjectById(baContract.getProjectId());
                    baProject.setProjectStage(10);
                    projectMapper.updateBaProject(baProject);
                }
            }
        }
        return result;
    }

    //新增认领收款
    @Override
    public int insertCollection(BaCollection baCollection) {
        baCollection.setId(getRedisIncreID.getId());
        baCollection.setCreateTime(DateUtils.getNowDate());
        baCollection.setCreateBy(SecurityUtils.getUsername());
        /*baCollection.setUpdateBy(SecurityUtils.getUsername());
        baCollection.setUpdateTime(DateUtils.getNowDate());*/
        baCollection.setUserId(SecurityUtils.getUserId());
        baCollection.setDeptId(SecurityUtils.getDeptId());
        baCollection.setType("3");
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        //合同启动全局编号
        if(StringUtils.isNotEmpty(baCollection.getStartGlobalNumber())){
            //全局编号
            QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.eq("flag",0);
            queryWrapper.isNotNull("serial_number");
            queryWrapper.orderByDesc("serial_number");
            List<BaCollection> baCollections = baCollectionMapper.selectList(queryWrapper);
            if(baCollections.size() > 0){
                Integer serialNumber = baCollections.get(0).getSerialNumber();
                baCollection.setSerialNumber(serialNumber+1);
                if(serialNumber < 10){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"000"+baCollection.getSerialNumber());
                }else if(serialNumber >= 10){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"00"+baCollection.getSerialNumber());
                }else if(serialNumber >= 100){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"0"+baCollection.getSerialNumber());
                }else if(serialNumber >= 1000){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+baCollection.getSerialNumber());
                }
            }else {
                baCollection.setSerialNumber(1);
                baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"000"+baCollection.getSerialNumber());
            }
        }
        int result = baCollectionMapper.insertBaCollection(baCollection);
        if(result > 0){
            //判断是否直接发起流程
            if(baCollection.getCollectionCategory().equals("5") || baCollection.getCollectionCategory().equals("6")){
                 this.approvalButton(baCollection.getId(),"2");
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 认领收款提交审核
     * @param id
     * @return
     */
    @Override
    public int approvalButton(String id,String workState) {
        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
        if(!"2".equals(workState)){
            //流程发起状态
            baCollection.setWorkState("1");
            //默认审批流程已通过
            baCollection.setState(AdminCodeEnum.CLAIM_STATUS_PASS.getCode());
        }
        if("2".equals(workState)){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode(),SecurityUtils.getCurrComId());
            baCollection.setFlowId(flowId);
        }
        baCollection.setInitiationTime(DateUtils.getNowDate());
        Integer result = baCollectionMapper.updateBaCollection(baCollection);
        if(!"2".equals(workState)){
            //认领收款
            QueryWrapper<BaClaim> baClaimQueryWrapper = new QueryWrapper<>();
            baClaimQueryWrapper.eq("relation_id", baCollection.getId());
            List<BaClaim> baClaims = baClaimMapper.selectList(baClaimQueryWrapper);
            if(!CollectionUtils.isEmpty(baClaims)){
                baClaims.stream().forEach(item -> {
                    item.setClaimState("2"); //已认领
                    baClaimMapper.updateBaClaim(item);

                    //根据认领查询订单数据，统计更新认领订单累积回款
                    QueryWrapper<BaClaimHistory> baClaimHistoryQueryWrapper = new QueryWrapper<>();
                    baClaimHistoryQueryWrapper.eq("claim_id", item.getId());
                    double currentCollection = 0;
                    List<BaClaimHistory> baClaimHistories = baClaimHistoryMapper.selectList(baClaimHistoryQueryWrapper);
                    Map<String, Double> orderMap = new HashMap<>();
                    if(!CollectionUtils.isEmpty(baClaimHistories)){
                        for(BaClaimHistory baClaimHistory : baClaimHistories){
                            if(orderMap.containsKey(baClaimHistory.getOrderId())){
                                currentCollection = orderMap.get(baClaimHistory.getOrderId());
                                if(baClaimHistory.getCurrentCollection() != null){
                                    currentCollection = currentCollection + baClaimHistory.getCurrentCollection().doubleValue();
                                    orderMap.put(baClaimHistory.getOrderId(), currentCollection);
                                }
                            } else {
                                orderMap.put(baClaimHistory.getOrderId(), baClaimHistory.getCurrentCollection().doubleValue());
                            }
                        }
                    }
                    if(!CollectionUtils.isEmpty(orderMap)){
                        orderMap.entrySet().stream().forEach((entry) -> {
                            BaOrder baOrder = baOrderMapper.selectBaOrderById(entry.getKey());
                            if(!ObjectUtils.isEmpty(baOrder)){
                                baOrder.setStayCollected(new BigDecimal(entry.getValue()).add(baOrder.getStayCollected()));
                                baOrder.setUpdateBy(SecurityUtils.getUsername());
                                baOrder.setUpdateTime(DateUtils.getNowDate());
                                baOrderMapper.updateBaOrder(baOrder);
                            }
                        });
                    }
                });
            }
            baCollectionMapper.updateById(baCollection);
        }
        if("2".equals(workState)){
             if(result > 0){
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(baCollection, AdminCodeEnum.CLAIM_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                baCollection.setState(AdminCodeEnum.CLAIM_STATUS_VERIFYING.getCode());
                //去审批人
                baCollection.setApprover(SecurityUtils.getUserId());
                baCollection.setGoBack("");
                baCollectionMapper.updateBaCollection(baCollection);
             }
           }
        }
        return result;
    }

    /**
     * 认领收款提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances1(BaCollection collection, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(collection.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(collection.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CLAIM.getCode());
        //获取流程实例关联的业务对象
        BaCollection baCollection = this.selectBaCollectionById(collection.getId());
        SysUser sysUser = iSysUserService.selectUserById(baCollection.getUserId());
        baCollection.setUserName(sysUser.getUserName());
        baCollection.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baCollection, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);

        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances1(instancesRuntimeDTO, baCollection);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances1(ProcessInstancesRuntimeDTO processInstancesRuntime, BaCollection collection) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CLAIM_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 撤销认领收款审核
     */
    @Override
    public int approvalRevoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询收收款信息
            BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baCollection.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CLAIM_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baCollection.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //收款流程审批状态修改
                baCollection.setState(AdminCodeEnum.CLAIM_STATUS_WITHDRAW.getCode());

                baCollectionMapper.updateBaCollection(baCollection);
                String userId = String.valueOf(baCollection.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baCollection, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_COLLECTION.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public UR searchName(String name) {
        Map<String,String> map = new HashMap<>();
        //查询终端企业
        QueryWrapper<BaEnterprise> enterpriseQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(name)){
            enterpriseQueryWrapper.like("name",name);
        }
        enterpriseQueryWrapper.eq("state","enterprise:pass");
        enterpriseQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            enterpriseQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaEnterprise> baEnterpriseList = baEnterpriseMapper.selectList(enterpriseQueryWrapper);
        if(StringUtils.isNotNull(baEnterpriseList)){
            for (BaEnterprise baEnterprise:baEnterpriseList) {
                //税号查询
                QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
                informationQueryWrapper.eq("relevance_id",baEnterprise.getId());
                BaBillingInformation baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
                if(StringUtils.isNotNull(baBillingInformation)){
                    map.put(baEnterprise.getId(),baEnterprise.getName()+","+baBillingInformation.getDutyParagraph());
                }
            }
        }
        //查询供应商
        QueryWrapper<BaSupplier> supplierQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(name)){
            supplierQueryWrapper.like("name",name);
        }
        supplierQueryWrapper.eq("state","supplier:pass");
        supplierQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            supplierQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaSupplier> baSupplierList = baSupplierMapper.selectList(supplierQueryWrapper);
        if(StringUtils.isNotNull(baSupplierList)){
            for (BaSupplier baSupplier:baSupplierList) {
                map.put(baSupplier.getId(),baSupplier.getName()+","+baSupplier.getTaxNum());
            }
        }
        //查询公司
        QueryWrapper<BaCompany> companyQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(name)){
            companyQueryWrapper.like("name",name);
        }
        companyQueryWrapper.eq("state","company:pass");
        companyQueryWrapper.eq("flag",0);
        //租户ID
        if(StringUtils.isNotEmpty(SecurityUtils.getCurrComId())){
            companyQueryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
        }
        List<BaCompany> baCompanyList = baCompanyMapper.selectList(companyQueryWrapper);
        if(StringUtils.isNotNull(baCompanyList)){
            for (BaCompany baCompany:baCompanyList) {
                map.put(baCompany.getId(),baCompany.getName()+","+baCompany.getTaxNum());
            }
        }
        return UR.ok().data("name",map);
    }

    @Override
    public List<BaBillingInformation> invoicingInformation(BaBillingInformation baBillingInformation) {
        List<BaBillingInformation> baBillingInformations = baBillingInformationMapper.selectBaBillingInformationList(baBillingInformation);
        return baBillingInformations;
    }

    @Override
    public int goBack(String id) {
        BaCollection baCollection = baCollectionMapper.selectBaCollectionById(id);
        baCollection.setState("");
        baCollection.setFlowId("");
        baCollection.setGoBack("1");
        return baCollectionMapper.updateBaCollection(baCollection);
    }

    @Override
    public int association(BaCollection baCollection) {
        //合同启动全局编号
        if(StringUtils.isNotEmpty(baCollection.getStartGlobalNumber())){
            //全局编号
            QueryWrapper<BaCollection> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.eq("flag",0);
            queryWrapper.isNotNull("serial_number");
            queryWrapper.orderByDesc("serial_number");
            List<BaCollection> baCollections = baCollectionMapper.selectList(queryWrapper);
            if(baCollections.size() > 0){
                Integer serialNumber = baCollections.get(0).getSerialNumber();
                baCollection.setSerialNumber(serialNumber+1);
                if(serialNumber < 10){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"000"+baCollection.getSerialNumber());
                }else if(serialNumber >= 10){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"00"+baCollection.getSerialNumber());
                }else if(serialNumber >= 100){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"0"+baCollection.getSerialNumber());
                }else if(serialNumber >= 1000){
                    baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+baCollection.getSerialNumber());
                }
            }else {
                baCollection.setSerialNumber(1);
                baCollection.setGlobalNumber(baCollection.getStartGlobalNumber()+"-"+"SK"+"-"+"000"+baCollection.getSerialNumber());
            }

            //全局编号
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baCollection.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baCollection.getId());
                baGlobalNumber.setCode(baCollection.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("7");
                baGlobalNumber.setStartId(baCollection.getStartId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }
        }

        return baCollectionMapper.updateBaCollection(baCollection);
    }


    @Override
    public BaCollectionStatVO sumBaCollection() {
        BaCollection baCollection = new BaCollection();
        baCollection.setTenantId(SecurityUtils.getCurrComId());
        return baCollectionMapper.sumBaCollection(baCollection);
    }

    @Override
    public List<BaCollectionVoucherDTO> claimCollectionAuthorized(BaCollection baCollection) {
        List<BaCollectionVoucherDTO> baCollectionVoucherDTOS = baCollectionMapper.selectCollectionAuthorized(baCollection);
        if(StringUtils.isNotNull(baCollectionVoucherDTOS)){
            for (BaCollectionVoucherDTO baCollectionVoucherDTO:baCollectionVoucherDTOS) {
                //岗位信息
                String name = getName.getName(Long.valueOf(baCollectionVoucherDTO.getUserId()));
                //发起人
                if(name.equals("") == false){
                    baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    baCollectionVoucherDTO.setUserName(sysUserMapper.selectUserById(Long.valueOf(baCollectionVoucherDTO.getUserId())).getNickName());
                }

                //付款账户
                if(StringUtils.isNotEmpty(baCollectionVoucherDTO.getBankId())){
                    BaBank baBank = baBankMapper.selectBaBankById(baCollectionVoucherDTO.getBankId());
                    baCollectionVoucherDTO.setPaymentAccount(baBank.getAccount());
                }
            }

        }
        return baCollectionVoucherDTOS;
    }

    @Override
    public int authorizedBaCollection(BaCollection baCollection) {
        return baCollectionMapper.updateBaCollection(baCollection);
    }
}
