package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaAutomobileSettlement;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.BaShippingOrderCmst;
import com.business.system.mapper.BaShippingOrderCmstMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAutomobileSettlementCmstMapper;
import com.business.system.domain.BaAutomobileSettlementCmst;
import com.business.system.service.IBaAutomobileSettlementCmstService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 汽运结算Service业务层处理
 *
 * @author single
 * @date 2023-08-21
 */
@Service
public class BaAutomobileSettlementCmstServiceImpl extends ServiceImpl<BaAutomobileSettlementCmstMapper, BaAutomobileSettlementCmst> implements IBaAutomobileSettlementCmstService
{
    @Autowired
    private BaAutomobileSettlementCmstMapper baAutomobileSettlementCmstMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    /**
     * 查询汽运结算
     *
     * @param id 汽运结算ID
     * @return 汽运结算
     */
    @Override
    public BaAutomobileSettlementCmst selectBaAutomobileSettlementCmstById(String id)
    {
        BaAutomobileSettlementCmst baAutomobileSettlementCmst = baAutomobileSettlementCmstMapper.selectBaAutomobileSettlementCmstById(id);
        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baAutomobileSettlementCmst.getOrderId());
        List<BaShippingOrderCmst> baShippingOrderCmsts=new ArrayList<>();
        if(StringUtils.isNotNull(baShippingOrderCmst)) {
            baAutomobileSettlementCmst.setConsignorUserName(baShippingOrderCmst.getConsignorUserName());
            baShippingOrderCmsts.add(baShippingOrderCmst);
        }
        baAutomobileSettlementCmst.setBaShippingOrderCmst(baShippingOrderCmsts);
        return baAutomobileSettlementCmst;
    }

    /**
     * 查询汽运结算列表
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 汽运结算
     */
    @Override
    public List<BaAutomobileSettlementCmst> selectBaAutomobileSettlementCmstList(BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        List<BaAutomobileSettlementCmst> baAutomobileSettlementCmsts = baAutomobileSettlementCmstMapper.selectBaAutomobileSettlementCmstList(baAutomobileSettlementCmst);
        //查询货主名称
        for (BaAutomobileSettlementCmst baAutomobileSettlementCmst1:baAutomobileSettlementCmsts) {
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstByOrderId(baAutomobileSettlementCmst1.getOrderId());
            if(StringUtils.isNotNull(baShippingOrderCmst)) {
                baAutomobileSettlementCmst1.setConsignorUserName(baShippingOrderCmst.getConsignorUserName());
            }
        }
        return baAutomobileSettlementCmsts;
    }

    /**
     * 新增汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        baAutomobileSettlementCmst.setCreateTime(DateUtils.getNowDate());
        return baAutomobileSettlementCmstMapper.insertBaAutomobileSettlementCmst(baAutomobileSettlementCmst);
    }

    /**
     * 修改汽运结算
     *
     * @param baAutomobileSettlementCmst 汽运结算
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlementCmst(BaAutomobileSettlementCmst baAutomobileSettlementCmst)
    {
        baAutomobileSettlementCmst.setUpdateTime(DateUtils.getNowDate());
        return baAutomobileSettlementCmstMapper.updateBaAutomobileSettlementCmst(baAutomobileSettlementCmst);
    }

    /**
     * 批量删除汽运结算
     *
     * @param ids 需要删除的汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementCmstByIds(String[] ids)
    {
        return baAutomobileSettlementCmstMapper.deleteBaAutomobileSettlementCmstByIds(ids);
    }

    /**
     * 删除汽运结算信息
     *
     * @param id 汽运结算ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementCmstById(String id)
    {
        return baAutomobileSettlementCmstMapper.deleteBaAutomobileSettlementCmstById(id);
    }

    /**
     * 货主结算通知
     * @param object
     * @return
     */
    @Override
    @Transactional
    public int addSettlement(JSONObject object) {
        System.out.println(object);
        BaAutomobileSettlementCmst baAutomobileSettlementCmst = JSONObject.toJavaObject((JSONObject) JSON.toJSON(object), BaAutomobileSettlementCmst.class);
        //运单对象
        if(!ObjectUtils.isEmpty(baAutomobileSettlementCmst)){
            baAutomobileSettlementCmst.setId(baAutomobileSettlementCmst.getOrderId());
            baAutomobileSettlementCmst.setSettlementType("0");
            baAutomobileSettlementCmst.setCreateTime(DateUtils.getNowDate());
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baAutomobileSettlementCmst.getOrderId());
            if(!ObjectUtils.isEmpty(baShippingOrderCmst)){
                //已申请
                baShippingOrderCmst.setSettlementType(AdminCodeEnum.CMST_ORDER_APPLY.getCode());
                baShippingOrderCmst.setUpdateTime(DateUtils.getNowDate());
                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
            }
        }

        return baAutomobileSettlementCmstMapper.insertBaAutomobileSettlementCmst(baAutomobileSettlementCmst);
    }

    /**
     * 承运方结算通知
     * @param object
     * @return
     */
    @Override
    @Transactional
    public int editSettlement(JSONObject object) {
        System.out.println(object);
        String orderId = object.getString("orderId");
        BaAutomobileSettlementCmst baAutomobileSettlementCmst = baAutomobileSettlementCmstMapper.selectBaAutomobileSettlementCmstById(orderId);
        if(!ObjectUtils.isEmpty(baAutomobileSettlementCmst)){
            baAutomobileSettlementCmst.setTonnage(object.getBigDecimal("tonnage")); //吨位
            baAutomobileSettlementCmst.setSettleMoney(object.getBigDecimal("settleMoney")); //结算金额
            baAutomobileSettlementCmst.setUpdateTime(DateUtils.getNowDate());
            baAutomobileSettlementCmst.setSettlementType("1");
            baAutomobileSettlementCmst.setId(orderId);
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baAutomobileSettlementCmst.getOrderId());
            if(!ObjectUtils.isEmpty(baShippingOrderCmst)){
                //已结算
                baShippingOrderCmst.setSettlementType(AdminCodeEnum.CMST_ORDER_SETTLEMENT.getCode());
                baShippingOrderCmst.setUpdateTime(DateUtils.getNowDate());
                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
            }
        }

        return baAutomobileSettlementCmstMapper.updateBaAutomobileSettlementCmst(baAutomobileSettlementCmst);
    }
}
