package com.business.system.service;

import java.util.List;
import java.util.Map;

import com.business.common.core.domain.UR;
import com.business.system.domain.OaSupplyClock;

/**
 * 补卡Service接口
 *
 * @author single
 * @date 2023-11-27
 */
public interface IOaSupplyClockService
{
    /**
     * 查询补卡
     *
     * @param id 补卡ID
     * @return 补卡
     */
    public OaSupplyClock selectOaSupplyClockById(String id);

    /**
     * 查询补卡列表
     *
     * @param oaSupplyClock 补卡
     * @return 补卡集合
     */
    public List<OaSupplyClock> selectOaSupplyClockList(OaSupplyClock oaSupplyClock);

    /**
     * 新增补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    public UR insertOaSupplyClock(OaSupplyClock oaSupplyClock);

    /**
     * 补卡次数
     *
     * @return 结果
     */
    public Map<String,Integer> frequency();

    /**
     * 修改补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    public UR updateOaSupplyClock(OaSupplyClock oaSupplyClock);

    /**
     * 批量删除补卡
     *
     * @param ids 需要删除的补卡ID
     * @return 结果
     */
    public int deleteOaSupplyClockByIds(String[] ids);

    /**
     * 删除补卡信息
     *
     * @param id 补卡ID
     * @return 结果
     */
    public int deleteOaSupplyClockById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);
}
