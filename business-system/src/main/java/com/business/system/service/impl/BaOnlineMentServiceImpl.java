package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.system.domain.BaOnlineMent;
import com.business.system.mapper.BaOnlineMentMapper;
import com.business.system.service.IBaOnlineMentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 监控点在线状态Service业务层处理
 *
 * @author single
 * @date 2023-07-06
 */
@Service
public class BaOnlineMentServiceImpl extends ServiceImpl<BaOnlineMentMapper, BaOnlineMent> implements IBaOnlineMentService
{
    @Autowired
    private BaOnlineMentMapper baOnlineMentMapper;

    /**
     * 查询监控点在线状态
     *
     * @param id 监控点在线状态ID
     * @return 监控点在线状态
     */
    @Override
    public BaOnlineMent selectBaOnlineMentById(String id)
    {
        return baOnlineMentMapper.selectBaOnlineMentById(id);
    }

    /**
     * 查询监控点在线状态列表
     *
     * @param baOnlineMent 监控点在线状态
     * @return 监控点在线状态
     */
    @Override
    public List<BaOnlineMent> selectBaOnlineMentList(BaOnlineMent baOnlineMent)
    {
        return baOnlineMentMapper.selectBaOnlineMentList(baOnlineMent);
    }

    /**
     * 新增监控点在线状态
     *
     * @param baOnlineMent 监控点在线状态
     * @return 结果
     */
    @Override
    public int insertBaOnlineMent(BaOnlineMent baOnlineMent)
    {
        baOnlineMent.setCreateTime(DateUtils.getNowDate());
        return baOnlineMentMapper.insertBaOnlineMent(baOnlineMent);
    }

    /**
     * 修改监控点在线状态
     *
     * @param baOnlineMent 监控点在线状态
     * @return 结果
     */
    @Override
    public int updateBaOnlineMent(BaOnlineMent baOnlineMent)
    {
        return baOnlineMentMapper.updateBaOnlineMent(baOnlineMent);
    }

    /**
     * 批量删除监控点在线状态
     *
     * @param ids 需要删除的监控点在线状态ID
     * @return 结果
     */
    @Override
    public int deleteBaOnlineMentByIds(String[] ids)
    {
        return baOnlineMentMapper.deleteBaOnlineMentByIds(ids);
    }

    /**
     * 删除监控点在线状态信息
     *
     * @param id 监控点在线状态ID
     * @return 结果
     */
    @Override
    public int deleteBaOnlineMentById(String id)
    {
        return baOnlineMentMapper.deleteBaOnlineMentById(id);
    }

}
