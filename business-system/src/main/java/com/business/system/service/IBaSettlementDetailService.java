package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaSettlementDetail;

/**
 * 结算明细Service接口
 *
 * @author ljb
 * @date 2023-02-06
 */
public interface IBaSettlementDetailService
{
    /**
     * 查询结算明细
     *
     * @param id 结算明细ID
     * @return 结算明细
     */
    public BaSettlementDetail selectBaSettlementDetailById(String id);

    /**
     * 查询结算明细列表
     *
     * @param baSettlementDetail 结算明细
     * @return 结算明细集合
     */
    public List<BaSettlementDetail> selectBaSettlementDetailList(BaSettlementDetail baSettlementDetail);

    /**
     * 新增结算明细
     *
     * @param baSettlementDetail 结算明细
     * @return 结果
     */
    public int insertBaSettlementDetail(BaSettlementDetail baSettlementDetail);

    /**
     * 修改结算明细
     *
     * @param baSettlementDetail 结算明细
     * @return 结果
     */
    public int updateBaSettlementDetail(BaSettlementDetail baSettlementDetail);

    /**
     * 批量删除结算明细
     *
     * @param ids 需要删除的结算明细ID
     * @return 结果
     */
    public int deleteBaSettlementDetailByIds(String[] ids);

    /**
     * 删除结算明细信息
     *
     * @param id 结算明细ID
     * @return 结果
     */
    public int deleteBaSettlementDetailById(String id);


}
