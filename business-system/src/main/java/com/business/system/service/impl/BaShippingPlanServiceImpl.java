package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaShippingPlanMapper;
import com.business.system.domain.BaShippingPlan;
import com.business.system.service.IBaShippingPlanService;

/**
 * 发运计划Service业务层处理
 *
 * @author ljb
 * @date 2023-03-06
 */
@Service
public class BaShippingPlanServiceImpl extends ServiceImpl<BaShippingPlanMapper, BaShippingPlan> implements IBaShippingPlanService
{
    @Autowired
    private BaShippingPlanMapper baShippingPlanMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询发运计划
     *
     * @param id 发运计划ID
     * @return 发运计划
     */
    @Override
    public BaShippingPlan selectBaShippingPlanById(String id)
    {
        return baShippingPlanMapper.selectBaShippingPlanById(id);
    }

    /**
     * 查询发运计划列表
     *
     * @param baShippingPlan 发运计划
     * @return 发运计划
     */
    @Override
    public List<BaShippingPlan> selectBaShippingPlanList(BaShippingPlan baShippingPlan)
    {
        return baShippingPlanMapper.selectBaShippingPlanList(baShippingPlan);
    }

    /**
     * 新增发运计划
     *
     * @param baShippingPlan 发运计划
     * @return 结果
     */
    @Override
    public int insertBaShippingPlan(BaShippingPlan baShippingPlan)
    {
        baShippingPlan.setId(getRedisIncreID.getId());
        baShippingPlan.setCreateTime(DateUtils.getNowDate());
        baShippingPlan.setCreateBy(SecurityUtils.getUsername());
        return baShippingPlanMapper.insertBaShippingPlan(baShippingPlan);
    }

    /**
     * 修改发运计划
     *
     * @param baShippingPlan 发运计划
     * @return 结果
     */
    @Override
    public int updateBaShippingPlan(BaShippingPlan baShippingPlan)
    {
        baShippingPlan.setUpdateTime(DateUtils.getNowDate());
        baShippingPlan.setUpdateBy(SecurityUtils.getUsername());
        return baShippingPlanMapper.updateBaShippingPlan(baShippingPlan);
    }

    /**
     * 批量删除发运计划
     *
     * @param ids 需要删除的发运计划ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingPlanByIds(String[] ids)
    {
        return baShippingPlanMapper.deleteBaShippingPlanByIds(ids);
    }

    /**
     * 删除发运计划信息
     *
     * @param id 发运计划ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingPlanById(String id)
    {
        return baShippingPlanMapper.deleteBaShippingPlanById(id);
    }

}
