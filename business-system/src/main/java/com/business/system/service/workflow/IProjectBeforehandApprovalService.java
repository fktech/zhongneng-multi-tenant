package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.BaChargingStandards;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.BaProjectBeforehand;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 预项目管理审批接口 服务类
 * @date: 2023/9/21 11:17
 */
@Service
public class IProjectBeforehandApprovalService {

    @Autowired
    protected BaProjectBeforehandMapper baProjectBeforehandMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private ISysMenuService iSysMenuService;

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private RedisCache redisCache;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaProjectBeforehand baProjectBeforehand = new BaProjectBeforehand();
        baProjectBeforehand.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baProjectBeforehand = baProjectBeforehandMapper.selectBaProjectBeforehandById(baProjectBeforehand.getId());
        String userId = String.valueOf(baProjectBeforehand.getUserId());
        if (AdminCodeEnum.PROJECTBEFOREHAND_STATUS_PASS.getCode().equals(status)) {
            BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            BaProjectBeforehand baProjectBeforehand1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaProjectBeforehand.class);
            //更新预立项状态
            baProjectBeforehand1.setState(status);
            //跟新待定状态
            baProjectBeforehand1.setPendingStatus("0");
            //立项时间
            baProjectBeforehand1.setProjectTime(DateUtils.getTime());
            //收费调整
            if (StringUtils.isNotNull(baProjectBeforehand1.getAdjusts())) {
                if (StringUtils.isNotEmpty(baProjectBeforehand1.getAdjusts().getId())) {
                    baChargingStandardsMapper.updateBaChargingStandards(baProjectBeforehand1.getAdjusts());
                } else {
                    BaChargingStandards standards = baProjectBeforehand1.getAdjusts();
                    standards.setId(getRedisIncreID.getId());
                    standards.setCreateTime(DateUtils.getNowDate());
                    standards.setCreateBy(SecurityUtils.getUsername());
                    baChargingStandardsMapper.insertBaChargingStandards(standards);
                    baProjectBeforehand1.setAdjustId(standards.getId());
                }
            }
            baProjectBeforehandMapper.updateById(baProjectBeforehand1);
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baProjectBeforehand1, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if (AdminCodeEnum.PROJECTBEFOREHAND_STATUS_REJECT.getCode().equals(status)) {
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baProjectBeforehand, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if (!ObjectUtils.isEmpty(ajaxResult)) {
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for (LinkedHashMap map : data) {
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                            if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                this.insertMessage(baProjectBeforehand, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
            baProjectBeforehand.setState(status);
            baProjectBeforehand.setPendingStatus("0");
            baProjectBeforehandMapper.updateById(baProjectBeforehand);
        }

        return true;
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(BaProjectBeforehand baProjectBeforehand, String mId, String msgContent) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baProjectBeforehand.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECTBEFOREHAND.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaProjectBeforehand checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaProjectBeforehand baProjectBeforehand = baProjectBeforehandMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baProjectBeforehand == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PROJECTBEFOREHAND_001);
        }
        if (!AdminCodeEnum.PROJECTBEFOREHAND_STATUS_VERIFYING.getCode().equals(baProjectBeforehand.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PROJECTBEFOREHAND_002);
        }
        return baProjectBeforehand;
    }
}
