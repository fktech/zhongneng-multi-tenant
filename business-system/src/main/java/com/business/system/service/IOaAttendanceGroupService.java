package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaWorkTimes;
import com.business.system.domain.vo.ClockInVO;

/**
 * 考勤组Service接口
 *
 * @author single
 * @date 2023-11-24
 */
public interface IOaAttendanceGroupService
{
    /**
     * 查询考勤组
     *
     * @param id 考勤组ID
     * @return 考勤组
     */
    public OaAttendanceGroup selectOaAttendanceGroupById(String id);

    /**
     * 查询考勤组列表
     *
     * @param oaAttendanceGroup 考勤组
     * @return 考勤组集合
     */
    public List<OaAttendanceGroup> selectOaAttendanceGroupList(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 新增考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    public int insertOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 延迟生效
     * @param oaAttendanceGroup
     * @return
     */
    public int delayOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 修改考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    public int updateOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup);

    /**
     * 批量删除考勤组
     *
     * @param ids 需要删除的考勤组ID
     * @return 结果
     */
    public int deleteOaAttendanceGroupByIds(String[] ids);

    /**
     * 删除考勤组信息
     *
     * @param id 考勤组ID
     * @return 结果
     */
    public int deleteOaAttendanceGroupById(String id);


    public ClockInVO CheckRules(String userId);

}
