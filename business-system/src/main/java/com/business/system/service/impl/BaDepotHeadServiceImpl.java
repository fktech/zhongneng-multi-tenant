package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaDepotHeadStatVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * 出入库记录Service业务层处理
 *
 * @author single
 * @date 2023-01-05
 */
@Service
public class BaDepotHeadServiceImpl extends ServiceImpl<BaDepotHeadMapper, BaDepotHead> implements IBaDepotHeadService
{
    @Autowired
    private BaDepotHeadMapper baDepotHeadMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaCheckService baCheckService;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaBatchMapper baBatchMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private IBaOrderService baOrderService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaMaterialStockMapper baMaterialStockMapper;

    @Autowired
    private BaGoodsTypeMapper baGoodsTypeMapper;

    @Autowired
    private BaGoodsAddMapper baGoodsAddMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaDepotItemMapper baDepotItemMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private PostName getName;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    private static final Logger log = LoggerFactory.getLogger(BaDepotHeadServiceImpl.class);

    @Autowired
    private BaDepotInventoryMapper baDepotInventoryMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaPurchaseOrderMapper baPurchaseOrderMapper;

    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;

    @Autowired
    private BaPoundRoomMapper baPoundRoomMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaWashHeadMapper baWashHeadMapper;

    /**
     * 查询出入库记录
     *
     * @param id 出入库记录ID
     * @return 出入库记录
     */
    @Override
    public BaDepotHead selectBaDepotHeadById(String id)
    {
        BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baDepotHead.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baDepotHead.setUser(sysUser);

        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baDepotHead.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }

        //订单编号
        if(StringUtils.isNotEmpty(baDepotHead.getOrderId())){
           BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
           if(!ObjectUtils.isEmpty(baContractStart)){
               baDepotHead.setOrderName(baContractStart.getName());
           }
        }
        //供应商名称
        /*if(StringUtils.isNotEmpty(baDepotHead.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getStartId());
            if(StringUtils.isNotEmpty(baContractStart.getSupplierId())){
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContractStart.getSupplierId());
                baDepotHead.setSupplierName(baSupplier.getName());
                baDepotHead.setSupplierId(baSupplier.getId());
            }
        }*/
        //项目信息
        BaProject baProject = baProjectMapper.selectBaProjectById(baDepotHead.getProjectId());
        if(!ObjectUtils.isEmpty(baProject)){
            baDepotHead.setProjectName(baProject.getName());
        }
        //查询计划相关信息
        if(StringUtils.isNotEmpty(baDepotHead.getPurchaseId())){
            //查询计划清单
//            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
//            if(StringUtils.isNotNull(baPurchaseOrder)){
                //供应商信息
//                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId());
//                if(StringUtils.isNotNull(baSupplier)){
//                    baDepotHead.setSupplierName(baSupplier.getName());
//                    baDepotHead.setSupplierId(baSupplier.getId());
//                }
                //商品信息
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baDepotHead.getMaterialId());
                if(!ObjectUtils.isEmpty(baGoods)){
                    baDepotHead.setGoodsName(baGoods.getName());
                }
                //计划信息
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baDepotHead.getPurchaseId());
                if(StringUtils.isNotNull(baProcurementPlan)){
                    baDepotHead.setPlanName(baProcurementPlan.getPlanName());
                }
//            }
        }
        //批次号
        //baDepotHead.setBatchName(baBatchMapper.selectBaBatchById(baDepotHead.getBatch()).getBatch());
        //商品名称
        /*BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baDepotHead.getMaterialId());
        //设置商品分类名称
        baGoods.setGoodsTypeName(baGoodsTypeMapper.selectBaGoodsTypeById(baGoods.getGoodsType()).getName());
        QueryWrapper<BaGoodsAdd> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("goods_id", baGoods.getId());
        queryWrapper.eq("flag", "0");
        List<BaGoodsAdd> baGoodsAdds = baGoodsAddMapper.selectList(queryWrapper);
        StringBuilder stringBuilder = new StringBuilder();
        if(!CollectionUtils.isEmpty(baGoodsAdds)){
            baGoodsAdds.stream().forEach(item1 -> {
                if(stringBuilder.length() == 0){
                    stringBuilder.append(item1.getInformationName()).append(":").append(item1.getInformationValue());
                } else {
                    stringBuilder.append(",").append(item1.getInformationName()).append(":").append(item1.getInformationValue());
                }
            });
            baGoods.setExtInfo(stringBuilder.toString());
        }
        baDepotHead.setMaterialName(baGoods.getName());
        //商品信息
        baDepotHead.setBaGoods(baGoods);*/
        //仓库名称
        if(StringUtils.isNotEmpty(baDepotHead.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baDepotHead.getDepotId());
            if(!ObjectUtils.isEmpty(baDepot)){
                baDepotHead.setDepotName(baDepot.getName());
            }
        }
        //库位信息
        if(StringUtils.isNotEmpty(baDepotHead.getLocationId())){
            BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(baDepotHead.getLocationId());
            if(StringUtils.isNotNull(baDepotLocation)){
                baDepotHead.setLocationName(baDepotLocation.getName());
            }
        }
        //商品类别
//        if(StringUtils.isNotEmpty(baDepotHead.getOrderId())){
//            //合同启信息
//            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
//            baDepotHead.setStartName(baContractStart.getName());
//            if(StringUtils.isNotEmpty(baContractStart.getProjectId())){
//                //立项信息
//                baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
//                if(StringUtils.isNotEmpty(baProject.getGoodsType())){
//                    //商品类别
//                    BaGoodsType baGoodsType = baGoodsTypeMapper.selectBaGoodsTypeById(baProject.getGoodsType());
//                    if(!ObjectUtils.isEmpty(baGoodsType)){
//                        baDepotHead.setMaterialName(baGoodsType.getName());
//                    }
//                }
//                if(StringUtils.isNotEmpty(baProject.getDepotId())){
//                    //仓库信息
//                    BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
//                    if(!ObjectUtils.isEmpty(baDepot)){
//                        baDepotHead.setDepotName(baDepot.getName());
//                    }
//                }
//                if(StringUtils.isNotEmpty(baProject.getEnterpriseId())){
//                    //终端企业
//                    baDepotHead.setEnterpriseId(baProject.getEnterpriseId());
//                    //终端企业名称
//                    baDepotHead.setEnterpriseName(baEnterpriseMapper.selectBaEnterpriseById(baProject.getEnterpriseId()).getName());
//                }
//            }
//        }
        //质检信息
        QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
        checkQueryWrapper.eq("relation_id",id);
        BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
        if(StringUtils.isNotNull(check)){
            baDepotHead.setBaCheck(check);
        }
        /*//回显加工出库信息
        if(StringUtils.isNotEmpty(baDepotHead.getLinkNumber())){
            List<BaDepotHead> baDepotHeads = new ArrayList<>();
            String[] ids = baDepotHead.getLinkNumber().split(",");
            for (String code:ids ) {
               BaDepotHead baDepotHead1 = baDepotHeadMapper.selectBaDepotHeadById(code);
                //供应商名称
                baDepotHead1.setSupplierName(baSupplierMapper.selectBaSupplierById(baDepotHead1.getSupplierId()).getName());
                //批次号
                baDepotHead1.setBatchName(baBatchMapper.selectBaBatchById(baDepotHead1.getBatch()).getBatch());
                //商品名称
                baDepotHead1.setMaterialName(baGoodsMapper.selectBaGoodsById(baDepotHead1.getMaterialId()).getName());
                baDepotHead1.setMaterialCode(baGoodsMapper.selectBaGoodsById(baDepotHead1.getMaterialId()).getCode());
                //仓库名称
                baDepotHead1.setDepotName(baDepotMapper.selectBaDepotById(baDepotHead1.getDepotId()).getName());
               baDepotHeads.add(baDepotHead1);
            }
            baDepotHead.setBaDepotHeads(baDepotHeads);
        }*/
               //查询运单信息
                if(StringUtils.isNotEmpty(baDepotHead.getWaybillId())){
                    List<BaShippingOrder> shippingOrders = new ArrayList<>();
                    String[] split = baDepotHead.getWaybillId().split(",");
                    for (String sId:split) {
                        BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(sId);
                        if(StringUtils.isNotEmpty(baShippingOrder.getSupplierNo())){
                            //查询货源信息
                            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baShippingOrder.getSupplierNo());
                            baShippingOrder.setBaTransportAutomobile(baTransportAutomobile);
                            baShippingOrder.setTransWeight(baTransportAutomobile.getTransWeight());
                        }
                        //查询付款明细
                        QueryWrapper<BaAutomobileSettlementDetail> queryWrapper =new QueryWrapper<>();
                        queryWrapper.eq("external_order",baShippingOrder.getId());
                        List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(queryWrapper);
                        for (BaAutomobileSettlementDetail baAutomobileSettlementDetail:baAutomobileSettlementDetails) {
                            if(baAutomobileSettlementDetail.getState().equals("2")){
                                baShippingOrder.setPaymentStatus("1");
                            }
                        }
                        shippingOrders.add(baShippingOrder);
                    }
                    baDepotHead.setBaShippingOrders(shippingOrders);
                }


        //相关副表信息
        QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
        baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
        List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
        baDepotHead.setBaDepotItem(baDepotItems);
        //入库清单
        QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
        inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
        List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
        BigDecimal totals = new BigDecimal(0);
        BigDecimal price = new BigDecimal(0);
        if(!CollectionUtils.isEmpty(baDepotInventories)){
            for(BaDepotInventory baDepotInventory : baDepotInventories){
                //根据货源单ID查询货源
                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                    baDepotInventory.setBaTransportAutomobile(baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo()));
                }
                //修改运单状态
                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                if(!ObjectUtils.isEmpty(baShippingOrder)){
                    if(null != baShippingOrder.getRcjweight()){
                        baDepotInventory.setUnloadWeight(baShippingOrder.getRcjweight());
                    }
                    if(null != baShippingOrder.getCcjweight()){
                        baDepotInventory.setLoadingWeight(baShippingOrder.getCcjweight());
                    }
                }
                //计算totals总量
                if(baDepotHead.getType() == 1 && null != baDepotInventory.getUnloadWeight()){
                    totals = totals.add(baDepotInventory.getUnloadWeight());
                } else if(baDepotHead.getType() == 2 && null != baDepotInventory.getLoadingWeight()){
                    totals = totals.add(baDepotInventory.getLoadingWeight());
                }
                //计算价格
                if(baDepotHead.getType() == 1 && null != baDepotHead.getProcurePrice()){
                    baDepotHead.setCargoValue(totals.multiply(baDepotHead.getProcurePrice()));
                } else if(baDepotHead.getType() == 2 && null != baDepotInventory.getLoadingWeight()){
                    baDepotHead.setCargoValue(totals.multiply(baDepotHead.getProcurePrice()));
                }
                baDepotHead.setTotals(totals);
                baDepotInventoryMapper.updateBaDepotInventory(baDepotInventory);
            }
            baDepotHeadMapper.updateBaDepotHead(baDepotHead);
        }
        baDepotHead.setBaDepotInventories(baDepotInventories);
        //采购单
        if(StringUtils.isNotEmpty(baDepotHead.getPurchaseId())){
//            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
            //采购计划
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baDepotHead.getPurchaseId());
//            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId());
//            baPurchaseOrder.setMontageName(baProcurementPlan.getPlanName()+"-"+baGoods.getName()+"-"+baPurchaseOrder.getExcludingPrice());
//            if(StringUtils.isNotEmpty(baPurchaseOrder.getSupplierId())){
//                //供应商信息
//                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId());
//                baPurchaseOrder.setMontageName(baPurchaseOrder.getMontageName()+"-"+baSupplier.getCompanyAbbreviation());
//            }
            baDepotHead.setPlanName(baProcurementPlan.getPlanName());
        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baDepotHead.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baDepotHead.setProcessInstanceId(processInstanceIds);
        //查询入库品名
        BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baDepotHead.getMaterialId());
        if(!ObjectUtils.isEmpty(baGoods)){
            baDepotHead.setMaterialName(baGoods.getName());
        }
        return baDepotHead;
    }

    /**
     * 查询出入库记录列表
     *
     * @param baDepotHead 出入库记录
     * @return 出入库记录
     */
    @Override
    public List<BaDepotHead> selectBaDepotHeadList(BaDepotHead baDepotHead)
    {
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.selectBaDepotHeadList(baDepotHead);
        for (BaDepotHead depotHead:baDepotHeads) {
          //供应商
            depotHead.setSupplierName(baSupplierMapper.selectBaSupplierById(depotHead.getSupplierId()).getName());
            //商品
            BaGoods baGoods = baGoodsMapper.selectBaGoodsById(depotHead.getMaterialId());
            depotHead.setMaterialName(baGoods.getName());
            depotHead.setMaterialCode(baGoods.getCode());
            if(depotHead.getType() == 1){
                //总量baShippingOrder.setState("1"); //运单绑定状态修改
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",depotHead.getId());
                BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
                if(StringUtils.isNotNull(check)){
                    depotHead.setTotals(check.getCheckCoalNum());
                }
            }
            //批次号
            depotHead.setBatchName(baBatchMapper.selectBaBatchById(depotHead.getBatch()).getBatch());
            //仓库名称
            depotHead.setDeptName(baDepotMapper.selectBaDepotById(depotHead.getDepotId()).getName());
            //发起人
            depotHead.setUserName(sysUserMapper.selectUserById(depotHead.getUserId()).getNickName());
        }
        return baDepotHeads;
    }

    @Override
    public List<BaDepotHead> selectBaDepotHead(BaDepotHead baDepotHead) {
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.selectDepotHead(baDepotHead);
        for (BaDepotHead depotHead:baDepotHeads) {
            //入库总量
            /*if(depotHead.getType() == 1){
                //总量
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",depotHead.getId());
                BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
                if(StringUtils.isNotNull(check)){
                    depotHead.setTotals(check.getCheckCoalNum());
                }
            }*/
            //岗位信息
            String name = getName.getName(depotHead.getUserId());
            //发起人
            if(name.equals("") == false){
                depotHead.setUserName(sysUserMapper.selectUserById(depotHead.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                depotHead.setUserName(sysUserMapper.selectUserById(depotHead.getUserId()).getNickName());
            }
            //入库清单
            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",depotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            depotHead.setBaDepotInventories(baDepotInventories);

            //根据货源单ID查询货源
            if(StringUtils.isNotEmpty(depotHead.getSupplierNo())){
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(depotHead.getSupplierNo());
                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                    depotHead.setSupplierUserName(baTransportAutomobile.getSupplierName());
                    depotHead.setGoodsName(baTransportAutomobile.getGoodsName());
                }
            }
            //采购订单
            if(StringUtils.isNotEmpty(depotHead.getPurchaseId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(depotHead.getPurchaseId());
                if(StringUtils.isNotNull(baProcurementPlan)){
                    depotHead.setPurchaseName(baProcurementPlan.getPlanName());
                }
            }
            //销售订单
            if(StringUtils.isNotEmpty(depotHead.getOrderId())){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(depotHead.getOrderId());
                if(StringUtils.isNotNull(baContractStart)){
                    depotHead.setOrderName(baContractStart.getName());
                }
            }
        }
        return baDepotHeads;
    }


    /**
     * 新增入库记录
     *
     * @param baDepotHead 入库记录
     * @return 结果
     */
    @Override
    public int insertBaDepotHead(BaDepotHead baDepotHead)
    {
        //添加批次信息
        /*BaBatch batch = new BaBatch();
        if(StringUtils.isNotEmpty(baDepotHead.getBatch())){
            BaBatch baBatch = baBatchMapper.selectBaBatchById(baDepotHead.getBatch());
            if(StringUtils.isNotNull(baBatch)){
               baDepotHead.setBatch(baDepotHead.getBatch());
            }else {
                batch.setId("PC"+getRedisIncreID.getId());
                if(StringUtils.isNotEmpty(baDepotHead.getOrderId())){
                    batch.setOrderId(baDepotHead.getOrderId());
                }
                batch.setSupplierId(baDepotHead.getSupplierId());
                batch.setBatch(baDepotHead.getBatch());
                batch.setState("0");
                batch.setCreateTime(DateUtils.getNowDate());
                baBatchMapper.insertBaBatch(batch);
                baDepotHead.setBatch(batch.getId());
            }
        }*/
        baDepotHead.setId("RK"+getRedisIncreID.getId());
        baDepotHead.setCreateTime(DateUtils.getNowDate());
        baDepotHead.setCreateBy(SecurityUtils.getUsername());
        baDepotHead.setUserId(SecurityUtils.getUserId());
        baDepotHead.setDeptId(SecurityUtils.getDeptId());
        baDepotHead.setType((long) 1);
        baDepotHead.setHeadStatus("2");
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
       /* if(baDepotHead.getSubType().equals("2")){
           if(StringUtils.isNotEmpty(baDepotHead.getLinkNumber())){
               String[] split = baDepotHead.getLinkNumber().split(",");
               for (String id:split) {
                   //查询出库信息
                   BaDepotHead depotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
                   depotHead.setTypes("1");
                   baDepotHeadMapper.updateBaDepotHead(depotHead);
               }
           }
        }*/
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode(),SecurityUtils.getCurrComId());
        baDepotHead.setFlowId(flowId);
        int result = baDepotHeadMapper.insertBaDepotHead(baDepotHead);

        return result;
    }

    /**
     * 新增出库记录
     * @param baDepotHead
     * @return
     */
    @Override
    public int insertRetrieval(BaDepotHead baDepotHead) {
        baDepotHead.setId("CK"+getRedisIncreID.getId());
        baDepotHead.setCreateTime(DateUtils.getNowDate());
        baDepotHead.setCreateBy(SecurityUtils.getUsername());
        baDepotHead.setUserId(SecurityUtils.getUserId());
        baDepotHead.setDeptId(SecurityUtils.getDeptId());
        baDepotHead.setType((long) 2);
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());

        if(StringUtils.isNotNull(baDepotHead.getBaCheck())){
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baDepotHead.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            //添加质检信息
            BaCheck baCheck = baDepotHead.getBaCheck();

            if(StringUtils.isNotNull(check)){
                baCheckService.updateBaCheck(baCheck);
            }else {
                baCheck.setType(4); //出库化验
                baCheck.setRelationId(baDepotHead.getId());
                baCheckService.insertBaCheck(baCheck);
            }
        }

        //查询出库信息相关副表数据
        QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
        baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
        List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
        if(StringUtils.isNotNull(baDepotItems)){
            //删除出库相关副表数据
            for (BaDepotItem baDepotItem:baDepotItems) {
                baDepotItemMapper.deleteBaDepotItemById(baDepotItem.getId());
            }
        }
        if(StringUtils.isNotNull(baDepotHead.getBaDepotItem())){
            if(baDepotHead.getBaDepotItem().size() > 0){
                for (BaDepotItem item:baDepotHead.getBaDepotItem()) {
                    //新增副表信息
                    item.setId(getRedisIncreID.getId());
                    item.setHeaderId(baDepotHead.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baDepotItemMapper.insertBaDepotItem(item);
                }
            }
        }
        //出库清单
        if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
            if(baDepotHead.getBaDepotInventories().size() > 0){
                for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                    baDepotInventory.setId(getRedisIncreID.getId());
                    baDepotInventory.setRelationId(baDepotHead.getId());
                    baDepotInventory.setCreateBy(DateUtils.getDate());
                    baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据出库状态
                            QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                            queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                            baPoundRoom.setStatus("2");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setOutbound("1"); //货源绑定入库
                                    baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                    baTransportAutomobile.setSaleId(baDepotHead.getOrderId()); //出库绑定
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("2"); //运单绑定状态修改
                                baShippingOrder.setState("1"); //运单绑定状态修改
                                baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单出库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("2");
                                    baShippingOrder.setState("1"); //运单绑定状态修改
                                    baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setOutbound("1"); //货源绑定入库
                                            baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                            baTransportAutomobile.setSaleId(baDepotHead.getOrderId());
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单出库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("2");
                                    baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setOutbound("1"); //货源绑定入库
                                            baTransportCmst.setProjectId(baDepotHead.getProjectId());
                                            baTransportCmst.setSaleId(baDepotHead.getOrderId()); //出库绑定
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //判断是否为上海公司
        String judge = "";
        //查询销售订单
        if(StringUtils.isNotEmpty(baDepotHead.getOrderId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
            judge = baContractStart.getBelongCompanyId();
        }
        if(!"2".equals(baDepotHead.getWorkState())){
            baDepotHead.setWorkState("1");
            //默认审批流程已通过
            baDepotHead.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());

        }
        Integer result = baDepotHeadMapper.insertBaDepotHead(baDepotHead);
        if(!"2".equals(baDepotHead.getWorkState())){
            //        出库审批通过 start
            //根据出入库记录查询质检数据
            baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id", baDepotHead.getId());
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
            baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            //判断销售出库是否有出库清单
            QueryWrapper<BaDepotInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id",baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(queryWrapper1);
            if(!CollectionUtils.isEmpty(baDepotInventories) && baDepotInventories.size() > 0){
                baDepotHead.setHeadStatus("2"); //出库清单
            }
            //合同启动查询库存信息
            //baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            if(StringUtils.isNotEmpty(baDepotHead.getOrderId())) {
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
                if(!ObjectUtils.isEmpty(baContractStart)){
                    //更新出库量
                    if(baDepotHead.getTotals() != null){
                        if (baContractStart.getScheduledReceipt() != null) {
                            baContractStart.setScheduledReceipt(baContractStart.getScheduledReceipt().add(baDepotHead.getTotals()));
                        } else {
                            baContractStart.setScheduledReceipt(baDepotHead.getTotals());
                        }
                    }

                    //查询立项信息
                    BaMaterialStock materialStock = new BaMaterialStock();
                    //判断是否选择了库位
                    if(StringUtils.isNotEmpty(baDepotHead.getLocationId())) {
                        //查询立项库存信息
                        QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                        stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                        stockQueryWrapper.eq("location_id", baDepotHead.getLocationId());
                        stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                        materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                        //判断是否已存在库存, 不存在则新增
                        if (StringUtils.isNotEmpty(baDepotHead.getLocationId())) {
                            if (ObjectUtils.isEmpty(materialStock)) {
                                materialStock = new BaMaterialStock();
                                materialStock.setId(getRedisIncreID.getId());
                                materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                                materialStock.setCreateTime(DateUtils.getNowDate());
                                materialStock.setCreateBy(SecurityUtils.getUsername());
                                materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                                materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                                materialStock = this.calcBaMaterialStock1(materialStock, baDepotHead, baCheck);
                                materialStock.setTenantId(SecurityUtils.getCurrComId());
                                baMaterialStockMapper.insertBaMaterialStock(materialStock);
                            } else {
                                materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                                materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                                materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                                materialStock.setUpdateBy(SecurityUtils.getUsername());
                                materialStock.setUpdateTime(DateUtils.getNowDate());
                                materialStock = this.calcBaMaterialStock1(materialStock, baDepotHead, baCheck);
                                baMaterialStockMapper.updateBaMaterialStock(materialStock);
                            }
                        }
                    } else {
                        //查询立项库存信息
                        QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                        stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                        stockQueryWrapper.isNull("location_id");
                        stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                        materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                        if (ObjectUtils.isEmpty(materialStock)) {
                            materialStock = new BaMaterialStock();
                            materialStock.setId(getRedisIncreID.getId());
                            materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                            materialStock.setCreateTime(DateUtils.getNowDate());
                            materialStock.setCreateBy(SecurityUtils.getUsername());
                            materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                            materialStock = this.calcBaMaterialStock1(materialStock, baDepotHead, baCheck);
                            materialStock.setTenantId(SecurityUtils.getCurrComId());
                            baMaterialStockMapper.insertBaMaterialStock(materialStock);
                        } else {
                            materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                            materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                            materialStock.setUpdateBy(SecurityUtils.getUsername());
                            materialStock.setUpdateTime(DateUtils.getNowDate());
                            materialStock = this.calcBaMaterialStock1(materialStock, baDepotHead, baCheck);
                            baMaterialStockMapper.updateBaMaterialStock(materialStock);
                        }
                    }
                    //出库计算货值均价
                    if(materialStock.getIntoDepotCargoValue() == null){
                        materialStock.setIntoDepotCargoValue(new BigDecimal(0));
                    }
                    if(materialStock.getOutputDepotCargoValue() == null){
                        materialStock.setOutputDepotCargoValue(new BigDecimal(0));
                    }
                    if(baDepotHead.getCargoValue() != null){
                        //库存出库货值累计 + 本次出库货值
                        materialStock.setOutputDepotCargoValue(materialStock.getOutputDepotCargoValue().add(baDepotHead.getCargoValue()));
                    }

                    if(materialStock.getInventoryAverage() == null){
                        materialStock.setInventoryAverage(new BigDecimal(0));
                    }
                    if(materialStock.getInventoryTotal() == null){
                        materialStock.setInventoryTotal(new BigDecimal(0));
                    }
                    BigDecimal subtract = materialStock.getIntoDepotCargoValue().subtract(materialStock.getOutputDepotCargoValue());
                    if(subtract != null && materialStock.getInventoryTotal() != null && subtract.intValue() > 0 && materialStock.getInventoryTotal().intValue() > 0){
                        //计算库存均价
                        materialStock.setInventoryAverage(subtract.divide(materialStock.getInventoryTotal(),2, BigDecimal.ROUND_HALF_UP));
                    }

                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
            }
            baDepotHeadMapper.updateById(baDepotHead);
        }
//        出库审批通过 end
        if("2".equals(baDepotHead.getWorkState())){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode(),SecurityUtils.getCurrComId());
            baDepotHead.setFlowId(flowId);
        }
        baDepotHeadMapper.updateById(baDepotHead);
            //计算货值
            if(StringUtils.isNotEmpty(baDepotHead.getOrderId())) {
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
                //成本单价
                if (baContractStart.getPrice() != null && baDepotHead.getTotals() != null) {
                    baDepotHead.setCargoValue(baContractStart.getPrice().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            }
        if("2".equals(baDepotHead.getWorkState())){
            if(result > 0){
                BaDepotHead depotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(baDepotHead, AdminCodeEnum.OUTPUTDEPOT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    baDepotHeadMapper.deleteBaDepotHeadById(depotHead.getId());
                    return 0;
                }else {
                    depotHead.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_VERIFYING.getCode());
                    depotHead.setHeadStatus("1");
                    baDepotHeadMapper.updateBaDepotHead(depotHead);
                }
            }
        }
        return result;
    }

    /**
     * 出库提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances1(BaDepotHead depotHead, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(depotHead.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode());
        //查询入库立项ID
        if(!ObjectUtils.isEmpty(depotHead)){
            BaProject baProject = baProjectMapper.selectBaProjectById(depotHead.getProjectId());
            if(!ObjectUtils.isEmpty(baProject)){
                map.put("projectId", baProject.getId());
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(depotHead.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode());
        //获取流程实例关联的业务对象
        BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(depotHead.getId());
        SysUser sysUser = iSysUserService.selectUserById(baDepotHead.getUserId());
        baDepotHead.setUserName(sysUser.getUserName());
        baDepotHead.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baDepotHead));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances1(instancesRuntimeDTO, baDepotHead);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 出库启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances1(ProcessInstancesRuntimeDTO processInstancesRuntime, BaDepotHead baDepotHead) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            //查询合同启动信息
//            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getStartId());
            SysUser sysUser = sysUserMapper.selectUserByUserName(baDepotHead.getCreateBy());
            //设置入库业务发起人
            redisCache.lock(Constants.BUSINESS_MANAGER_ID, String.valueOf(sysUser.getUserId()), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                redisCache.unLock(Constants.BUSINESS_MANAGER_ID);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            redisCache.unLock(Constants.BUSINESS_MANAGER_ID);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OUTPUTDEPOT_APPROVAL_CONTENT_UPDATE.getCode())){
                //添加流程实例关系表创建日期
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }



    /**
     * 修改出入库记录
     *
     * @param baDepotHead 出入库记录
     * @return 结果
     */
    @Override
    public int updateBaDepotHead(BaDepotHead baDepotHead)
    {
        //原数据
        BaDepotHead baDepotHead1 = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
        if(baDepotHead.getTotals() == null){
            baDepotHead.setTotals(new BigDecimal(0));
        }
        baDepotHead.setUpdateBy(SecurityUtils.getUsername());
        baDepotHead.setUpdateTime(DateUtils.getNowDate());
            //入库化验
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baDepotHead.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            //添加质检信息
            if(StringUtils.isNotNull(baDepotHead.getBaCheck())){
                BaCheck baCheck = baDepotHead.getBaCheck();
                if(StringUtils.isNotNull(check)){
                    baCheckService.updateBaCheck(baCheck);
                }else {
                    baCheck.setType(3);
                    baCheck.setRelationId(baDepotHead.getId());
                    baCheckService.insertBaCheck(baCheck);
                }
            }
            //查询入库信息相关副表数据
            QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
            baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
            List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
            if(StringUtils.isNotNull(baDepotItems)){
                //删除入库相关副表数据
                for (BaDepotItem baDepotItem:baDepotItems) {
                    baDepotItemMapper.deleteBaDepotItemById(baDepotItem.getId());
                }
            }
            if(StringUtils.isNotNull(baDepotHead.getBaDepotItem())){
               if(baDepotHead.getBaDepotItem().size() > 0){
                   for (BaDepotItem item:baDepotHead.getBaDepotItem()) {
                       //新增副表信息
                       item.setId(getRedisIncreID.getId());
                       item.setHeaderId(baDepotHead.getId());
                       item.setCreateTime(DateUtils.getNowDate());
                       item.setCreateBy(SecurityUtils.getUsername());
                       baDepotItemMapper.insertBaDepotItem(item);
                   }
               }
            }
        //入库清单
        if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
            if(baDepotHead.getBaDepotInventories().size() > 0){
                //查询入库单是否存在
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                            queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                            baPoundRoom.setStatus("0");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setWarehousing("0"); //货源绑定入库
                                    baTransportAutomobile.setProjectId("");
                                    baTransportAutomobile.setProcureId("");
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("0");
                                    baShippingOrder.setState("0"); //运单绑定状态修改
                                    baShippingOrder.setDepotId("");
                                    baShippingOrder.setPositionId("");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setWarehousing("0"); //货源绑定入库
                                            baTransportAutomobile.setProjectId("");
                                            baTransportAutomobile.setProcureId("");
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("0");
                                    baShippingOrderCmst.setState("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId("");
                                    baShippingOrderCmst.setPositionId("");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setWarehousing("0"); //货源绑定入库
                                            baTransportCmst.setProjectId("");
                                            baTransportCmst.setProcureId("");
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //存在就删除
                    baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                }
                for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                    baDepotInventory.setId(getRedisIncreID.getId());
                    baDepotInventory.setRelationId(baDepotHead.getId());
                    baDepotInventory.setCreateBy(DateUtils.getDate());
                    baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                            queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                            baPoundRoom.setStatus("1");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setOutbound("1"); //货源绑定入库
                                    baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                    baTransportAutomobile.setSaleId(baDepotHead.getOrderId());
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("2"); //运单绑定状态修改
                                baShippingOrder.setState("1"); //运单绑定状态修改
                                baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("2");
                                    baShippingOrder.setState("1"); //运单绑定状态修改
                                    baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setOutbound("1"); //货源绑定出库
                                            baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                            baTransportAutomobile.setSaleId(baDepotHead.getOrderId());
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("2");
                                    baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setOutbound("1"); //货源绑定入库
                                            baTransportCmst.setProjectId(baDepotHead.getProjectId());
                                            baTransportCmst.setSaleId(baDepotHead.getOrderId());
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                //查询入库单是否存在
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    //编辑时入库单为空删除对应数据
                    baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                }
            }
        }else {
            //查询入库单是否存在
            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            for (BaDepotInventory baDepotInventory:baDepotInventories) {
                //编辑时入库单为空删除对应数据
                baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
            }
        }
            //baDepotHead.setHeadStatus("2");

            //baDepotHeadMapper.updateBaDepotHead(baDepotHead);
            //撤销编辑入库基础信息
            Integer result = baDepotHeadMapper.updateBaDepotHead(baDepotHead);
            if(result > 0){
            BaDepotHead depotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
                //清除流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",depotHead.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(depotHead, AdminCodeEnum.OUTPUTDEPOT_APPROVAL_CONTENT_UPDATE.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    baDepotHeadMapper.updateBaDepotHead(baDepotHead1);
                    return 0;
                }else {
                    depotHead.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_VERIFYING.getCode());
                    baDepotHeadMapper.updateBaDepotHead(depotHead);
                }
            }
        return 1;
    }

    @Override
    public int updateDepotHead(BaDepotHead baDepotHead) {
        //原数据
        BaDepotHead baDepotHead1 = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
        baDepotHead.setUpdateBy(SecurityUtils.getUsername());
        baDepotHead.setUpdateTime(DateUtils.getNowDate());
        //入库化验
        QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
        checkQueryWrapper.eq("relation_id",baDepotHead.getId());
        BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
        //添加质检信息
        if(StringUtils.isNotNull(baDepotHead.getBaCheck())){
            BaCheck baCheck = baDepotHead.getBaCheck();
            if(StringUtils.isNotNull(check)){
                baCheckService.updateBaCheck(baCheck);
            }else {
                baCheck.setType(3);
                baCheck.setRelationId(baDepotHead.getId());
                baCheckService.insertBaCheck(baCheck);
            }
        }
        //查询入库信息相关副表数据
        QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
        baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
        List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
        if(StringUtils.isNotNull(baDepotItems)){
            //删除入库相关副表数据
            for (BaDepotItem baDepotItem:baDepotItems) {
                baDepotItemMapper.deleteBaDepotItemById(baDepotItem.getId());
            }
        }
        if(StringUtils.isNotNull(baDepotHead.getBaDepotItem())){
            if(baDepotHead.getBaDepotItem().size() > 0){
                for (BaDepotItem item:baDepotHead.getBaDepotItem()) {
                    //新增副表信息
                    item.setId(getRedisIncreID.getId());
                    item.setHeaderId(baDepotHead.getId());
                    item.setCreateTime(DateUtils.getNowDate());
                    item.setCreateBy(SecurityUtils.getUsername());
                    baDepotItemMapper.insertBaDepotItem(item);
                }
            }
        }
        //入库清单
        if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
            if(baDepotHead.getBaDepotInventories().size() > 0){
                //查询入库单是否存在
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                            queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                            baPoundRoom.setStatus("0");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setWarehousing("0"); //货源绑定入库
                                    baTransportAutomobile.setProjectId("");
                                    baTransportAutomobile.setProcureId("");
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("0"); //运单绑定状态修改
                                    baShippingOrder.setState("0"); //运单绑定状态修改
                                    baShippingOrder.setDepotId("");
                                    baShippingOrder.setPositionId("");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setOutbound("0"); //货源绑定入库
                                            baTransportAutomobile.setProjectId("");
                                            baTransportAutomobile.setProcureId("");
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setState("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId("");
                                    baShippingOrderCmst.setPositionId("");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setOutbound("0"); //货源绑定入库
                                            baTransportCmst.setProjectId("");
                                            baTransportCmst.setProcureId("");
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //存在就删除
                    baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                }
                for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                    baDepotInventory.setId(getRedisIncreID.getId());
                    baDepotInventory.setRelationId(baDepotHead.getId());
                    baDepotInventory.setCreateBy(DateUtils.getDate());
                    baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                            queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                            baPoundRoom.setStatus("1");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                                    baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                    baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("1"); //运单绑定状态修改
                                baShippingOrder.setState("1"); //运单绑定状态修改
                                baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("1"); //运单绑定状态修改
                                    baShippingOrder.setState("1"); //运单绑定状态修改
                                    baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                                            baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                            baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("1"); //运单绑定状态修改
                                    baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setWarehousing("1"); //货源绑定入库
                                            baTransportCmst.setProjectId(baDepotHead.getProjectId());
                                            baTransportCmst.setProcureId(baDepotHead.getPurchaseId());
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                //查询入库单是否存在
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    //编辑时入库单为空删除对应数据
                    baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                }
            }
        }else {
            //查询入库单是否存在
            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            for (BaDepotInventory baDepotInventory:baDepotInventories) {
                //编辑时入库单为空删除对应数据
                baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
            }
        }
       int result = baDepotHeadMapper.updateBaDepotHead(baDepotHead);
        if(result > 0){
            BaDepotHead depotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",depotHead.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(depotHead, AdminCodeEnum.INTODEPOT_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baDepotHeadMapper.updateBaDepotHead(baDepotHead1);
                return 0;
            }else {
                depotHead.setState(AdminCodeEnum.INTODEPOT_STATUS_VERIFYING.getCode());
                baDepotHeadMapper.updateBaDepotHead(depotHead);
            }
        }
        return 1;
    }

    /**
     * 批量删除出入库记录
     *
     * @param ids 需要删除的出入库记录ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotHeadByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
                /*if(StringUtils.isNotEmpty(baDepotHead.getWaybillId())){
                    //查询关联运单编号
                    String[] split = baDepotHead.getWaybillId().split(",");
                    for (String orderId:split) {
                        BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(orderId);
                        baShippingOrder.setDepotState("0");
                        baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                    }
                }*/
               baDepotHead.setFlag((long) 1);
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                //查询所有入库单
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                            queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                            baPoundRoom.setStatus("0");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("0");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("0");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                }
                            }
                        }
                    }
                }
               baDepotHeadMapper.updateBaDepotHead(baDepotHead);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除出入库记录信息
     *
     * @param id 出入库记录ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotHeadById(String id)
    {
        return baDepotHeadMapper.deleteBaDepotHeadById(id);
    }

    /**
     * 质检提交
     * @param baDepotHead
     * @return
     */
    @Override
    public int inspection(BaDepotHead baDepotHead) {

        baDepotHead.setId("RK"+getRedisIncreID.getId());
        baDepotHead.setCreateTime(DateUtils.getNowDate());
        baDepotHead.setCreateBy(SecurityUtils.getUsername());
        baDepotHead.setUserId(SecurityUtils.getUserId());
        baDepotHead.setDeptId(SecurityUtils.getDeptId());
        baDepotHead.setType((long) 1);
        baDepotHead.setHeadStatus("2");
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        //判断是否为上海公司
        String judge = "";
        if(StringUtils.isNotEmpty(baDepotHead.getPurchaseId())){
            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
            //已采购清单查询采购计划
//            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
//            judge = baProcurementPlan.getBelongCompanyId();
        }
        //判断业务归属公司是否是上海子公司
        if("2".equals(baDepotHead.getWorkState())){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode(),SecurityUtils.getCurrComId());
            baDepotHead.setFlowId(flowId);
        }
        Integer result;
        if(StringUtils.isNotNull(baDepotHead.getBaCheck())){
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baDepotHead.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            //添加质检信息
            BaCheck baCheck = baDepotHead.getBaCheck();

            if(StringUtils.isNotNull(check)){
                result = baCheckService.updateBaCheck(baCheck);
            }else {
                baCheck.setType(3);
                baCheck.setRelationId(baDepotHead.getId());
                result = baCheckService.insertBaCheck(baCheck);
            }
        }
            //查询入库信息相关副表数据
            QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
            baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
            List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
            if(StringUtils.isNotNull(baDepotItems)){
                //删除入库相关副表数据
                for (BaDepotItem baDepotItem:baDepotItems) {
                    baDepotItemMapper.deleteBaDepotItemById(baDepotItem.getId());
                }
            }
            if(StringUtils.isNotNull(baDepotHead.getBaDepotItem())){
                if(baDepotHead.getBaDepotItem().size() > 0){
                    for (BaDepotItem item:baDepotHead.getBaDepotItem()) {
                        //新增副表信息
                        item.setId(getRedisIncreID.getId());
                        item.setHeaderId(baDepotHead.getId());
                        item.setCreateTime(DateUtils.getNowDate());
                        item.setCreateBy(SecurityUtils.getUsername());
                        baDepotItemMapper.insertBaDepotItem(item);
                    }
                }
            }
            //入库清单
            if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
               if(baDepotHead.getBaDepotInventories().size() > 0){
                   for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                       baDepotInventory.setId(getRedisIncreID.getId());
                       baDepotInventory.setRelationId(baDepotHead.getId());
                       baDepotInventory.setCreateBy(DateUtils.getDate());
                       baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                       if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                           if(baDepotInventory.getSource().equals("1")){
                               //改变磅房数据入库状态
                               QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                               queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                               BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                               baPoundRoom.setStatus("1");
                               baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                               //根据货源单ID查询货源
                               if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                   BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                   if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                       baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                                       baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                       baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                       baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                   }
                               }
                               //昕科运单入库状态修改
                               BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                               baShippingOrder.setStatus("1"); //运单绑定状态修改
                               baShippingOrder.setState("1"); //运单绑定状态修改
                               baShippingOrder.setDepotId(baDepotHead.getDepotId());
                               baShippingOrder.setPositionId(baDepotHead.getLocationId());
                               baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                           }else if(baDepotInventory.getSource().equals("2")){
                               //改变无车承运数据入库状态
                               if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                   if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                       //昕科运单入库状态修改
                                       BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                       baShippingOrder.setStatus("1"); //运单绑定状态修改
                                       baShippingOrder.setState("1"); //运单绑定状态修改
                                       baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                       baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                       baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                       //根据货源单ID查询货源
                                       if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                           BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                           if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                               baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                                               baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                               baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                               baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                           }
                                       }
                                       //修改运单状态
                                       baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                       if(!ObjectUtils.isEmpty(baShippingOrder)){
                                           baShippingOrder.setStatus("1"); //运单绑定状态修改
                                           baShippingOrder.setState("1"); //运单绑定状态修改
                                           baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                           baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                           baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                       }
                                   }else if(baDepotInventory.getPlatformType().equals("2")){
                                       //中储摘单入库状态修改
                                       BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                       baShippingOrderCmst.setStatus("1"); //运单绑定状态修改
                                       baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                       baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                       baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                       baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                        //根据货源单ID查询货源
                                       if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                           BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                           if(!ObjectUtils.isEmpty(baTransportCmst)){
                                               baTransportCmst.setWarehousing("1"); //货源绑定入库
                                               baTransportCmst.setProjectId(baDepotHead.getProjectId());
                                               baTransportCmst.setProcureId(baDepotHead.getPurchaseId());
                                               baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                           }
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
            }

        if(!"2".equals(baDepotHead.getWorkState())){
            baDepotHead.setWorkState("1");
            //默认审批流程已通过
            baDepotHead.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
        }

            result = baDepotHeadMapper.insertBaDepotHead(baDepotHead);


        if(!"2".equals(baDepotHead.getWorkState())){
//            入库审批通过逻辑  start
            baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            //根据出入库记录查询质检数据
            QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id", baDepotHead.getId());
            BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
            baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
            //设置业务状态已入库
            //判断采购入库是否有入库清单
            QueryWrapper<BaDepotInventory> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id", baDepotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(queryWrapper1);
            //更新入库时间
            baDepotHead.setOperTime(DateUtils.getNowDate());

            //查询采购计划下的清单
            BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
            //采购计划绑定项目ID
            String procurementPlanProjectId = "";
            if (!ObjectUtils.isEmpty(baPurchaseOrder)) {
                //采购清单品名
                if (StringUtils.isNotEmpty(baPurchaseOrder.getPlanId())) {
                    BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                    if (!ObjectUtils.isEmpty(baProcurementPlan)) {
                        procurementPlanProjectId = baProcurementPlan.getProjectId();
                    }
                }
            }
            //查询立项信息
            BaMaterialStock materialStock = null;
            //判断是否选择了库位
            if (StringUtils.isNotEmpty(baDepotHead.getLocationId())) {
                //查询立项库存信息
                QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                stockQueryWrapper.eq("location_id", baDepotHead.getLocationId());
                stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                //判断是否已存在库存, 不存在则新增
                if (ObjectUtils.isEmpty(materialStock)) {
                    materialStock = new BaMaterialStock();
                    materialStock.setId(getRedisIncreID.getId());
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setCreateTime(DateUtils.getNowDate());
                    materialStock.setCreateBy(SecurityUtils.getUsername());
                    materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                    materialStock.setTenantId(SecurityUtils.getCurrComId());
                    baMaterialStockMapper.insertBaMaterialStock(materialStock);
                } else {
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock.setUpdateBy(SecurityUtils.getUsername());
                    materialStock.setUpdateTime(DateUtils.getNowDate());
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }

            } else {
                //查询立项库存信息
                QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                stockQueryWrapper.isNull("location_id");
                stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                //判断是否已存在库存, 不存在则新增
                if (ObjectUtils.isEmpty(materialStock)) {
                    materialStock = new BaMaterialStock();
                    materialStock.setId(getRedisIncreID.getId());
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setCreateTime(DateUtils.getNowDate());
                    materialStock.setCreateBy(SecurityUtils.getUsername());
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock.setTenantId(SecurityUtils.getCurrComId());
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                    materialStock.setTenantId(SecurityUtils.getCurrComId());
                    baMaterialStockMapper.insertBaMaterialStock(materialStock);
                } else {
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock.setUpdateBy(SecurityUtils.getUsername());
                    materialStock.setUpdateTime(DateUtils.getNowDate());
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck);
                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
            }
            //入库计算货值均价
            if (materialStock.getIntoDepotCargoValue() == null) {
                materialStock.setIntoDepotCargoValue(new BigDecimal(0));
            }
            if (baDepotHead.getCargoValue() != null) {
                //库存入库货值累计 + 本次入库货值
                materialStock.setIntoDepotCargoValue(materialStock.getIntoDepotCargoValue().add(baDepotHead.getCargoValue()));
            }

            if (materialStock.getInventoryAverage() == null) {
                materialStock.setInventoryAverage(new BigDecimal(0));
            }
            if (materialStock.getIntoDepotCargoValue() != null && materialStock.getInventoryTotal() != null) {
                if(materialStock.getInventoryTotal().compareTo(BigDecimal.ZERO)!=0){
                    //计算库存均价
                    materialStock.setInventoryAverage(materialStock.getIntoDepotCargoValue().divide(materialStock.getInventoryTotal(),
                            2, BigDecimal.ROUND_HALF_UP));
                }

            }
            baMaterialStockMapper.updateBaMaterialStock(materialStock);
            baDepotHead.setHeadStatus("3");

            baDepotHeadMapper.updateById(baDepotHead);
        }
        //            入库审批通过逻辑  end
        //判断是否为上海分公司
        if("2".equals(baDepotHead.getWorkState())){
            if(result > 0){
                BaDepotHead depotHead = baDepotHeadMapper.selectBaDepotHeadById(baDepotHead.getId());
                //清除流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",depotHead.getId());
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                if(StringUtils.isNotEmpty(baDepotHead.getWaybillId())){
                    depotHead.setWaybillId(baDepotHead.getWaybillId());

                            /*String[] split = baDepotHead.getWaybillId().split(",");
                            for (String id:split) {
                                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(id);
                                baShippingOrder.setDepotState("1");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }*/
                }
                //判断初始库存是否存在
                        /*BaBatch baBatch = baBatchMapper.selectBaBatchById(depotHead.getBatch());
                        if(baBatch.getInitialStock() == null){
                            baBatch.setInitialStock(baCheck.getCheckCoalNum());
                            baBatchMapper.updateBaBatch(baBatch);
                        }*/
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(depotHead, AdminCodeEnum.INTODEPOT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    return 0;
                }else {
                    //启动流程成功改变入库状态
                    depotHead.setState(AdminCodeEnum.INTODEPOT_STATUS_VERIFYING.getCode());
                    depotHead.setHeadStatus("2");
                    depotHead.setInitiateTime(DateUtils.getNowDate());
                    depotHead.setTotals(baDepotHead.getTotals());
                    baDepotHeadMapper.updateBaDepotHead(depotHead);
                }
            } else {
                return -2;
            }
        }
        return 1;
    }

    /**
     * 入库质检提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaDepotHead depotHead, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(depotHead.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode());
        //查询入库立项ID
        if(!ObjectUtils.isEmpty(depotHead)){
            BaProject baProject = baProjectMapper.selectBaProjectById(depotHead.getProjectId());
            if(!ObjectUtils.isEmpty(baProject)){
                map.put("projectId", baProject.getId());
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(depotHead.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode());
        //获取流程实例关联的业务对象
        BaDepotHead baDepotHead = this.selectBaDepotHeadById(depotHead.getId());
        SysUser sysUser = iSysUserService.selectUserById(baDepotHead.getUserId());
        baDepotHead.setUserName(sysUser.getUserName());
        baDepotHead.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baDepotHead, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 入库质检启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.INTODEPOT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }


    @Override
    public Integer intoRevoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询出入库信息
            BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
            /*if(StringUtils.isNotEmpty(baDepotHead.getWaybillId())){

                String[] split = baDepotHead.getWaybillId().split(",");
                for (String orderId:split) {
                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(orderId);
                    baShippingOrder.setDepotState("0");
                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                }
            }*/
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baDepotHead.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.INTODEPOT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baDepotHead.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //入库流程审批状态修改
                baDepotHead.setState(AdminCodeEnum.INTODEPOT_STATUS_WITHDRAW.getCode());
                baDepotHead.setHeadStatus("2");

                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                //查询所有入库单
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                            queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                            baPoundRoom.setStatus("0");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("0"); //运单绑定状态修改
                                    baShippingOrder.setState("0"); //运单绑定状态修改
                                    baShippingOrder.setDepotId("");
                                    baShippingOrder.setPositionId("");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);

                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setState("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId("");
                                    baShippingOrderCmst.setPositionId("");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                }
                            }
                        }
                    }
                }

                //释放对应的货源
                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                    //查询货源下所有运单
                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                    baShippingOrder.setSupplierNo(baDepotHead.getSupplierNo());
                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                    if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                        shippingOrderQueryWrapper.eq("state",0);
                        List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                        if(waybillDTOList.size() == shippingOrders.size()){
                            BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(waybillDTOList.get(0).getSupplierNo());
                            baTransportAutomobile1.setWarehousing("0");
                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                        }
                    }else if("2".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrderCmst> cmstQueryWrapper = new QueryWrapper<>();
                        cmstQueryWrapper.eq("state",0);
                        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(cmstQueryWrapper);
                        if(waybillDTOList.size() == baShippingOrderCmsts.size()){
                            BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(waybillDTOList.get(0).getSupplierNo());
                            transportCmst.setOutbound("0");
                            baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                        }
                    }
                }
                baDepotHeadMapper.updateBaDepotHead(baDepotHead);
                String userId = String.valueOf(baDepotHead.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baDepotHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }


    @Override
    public Integer outputRevoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询出入库信息
            BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baDepotHead.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OUTPUTDEPOT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baDepotHead.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //出库流程审批状态修改
                baDepotHead.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_WITHDRAW.getCode());
                baDepotHead.setHeadStatus("1");
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                //查询所有入库单
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper1 = new QueryWrapper<>();
                            queryWrapper1.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper1);
                            baPoundRoom.setStatus("0");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setOutbound("0"); //货源绑定入库
                                    baTransportAutomobile.setProjectId("");
                                    baTransportAutomobile.setProcureId("");
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }
                            //修改运单状态
                            BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                            if(!ObjectUtils.isEmpty(baShippingOrder)){
                                baShippingOrder.setStatus("0"); //运单绑定状态修改
                                baShippingOrder.setState("0"); //运单绑定状态修改
                                baShippingOrder.setDepotId("");
                                baShippingOrder.setPositionId("");
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }
                        }else if(baDepotInventory.getSource().equals("2")){
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("0"); //运单绑定状态修改
                                    baShippingOrder.setState("0"); //运单绑定状态修改
                                    baShippingOrder.setDepotId("");
                                    baShippingOrder.setPositionId("");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);

                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                            baTransportAutomobile.setOutbound("0"); //货源绑定入库
                                            baTransportAutomobile.setProjectId("");
                                            baTransportAutomobile.setProcureId("");
                                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                        }
                                    }
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setState("0"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId("");
                                    baShippingOrderCmst.setPositionId("");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);

                                    //根据货源单ID查询货源
                                    if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())) {
                                        BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baDepotHead.getSupplierNo());
                                        if(!ObjectUtils.isEmpty(baTransportCmst)){
                                            baTransportCmst.setOutbound("0"); //货源绑定入库
                                            baTransportCmst.setProjectId("");
                                            baTransportCmst.setProcureId("");
                                            baTransportCmstMapper.updateBaTransportCmst(baTransportCmst);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //释放对应的货源
                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                    //查询货源下所有运单
                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                    baShippingOrder.setSupplierNo(baDepotHead.getSupplierNo());
                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                    if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                        shippingOrderQueryWrapper.eq("state",0);
                        List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                        if(waybillDTOList.size() == shippingOrders.size()){
                            BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(waybillDTOList.get(0).getSupplierNo());
                            baTransportAutomobile1.setWarehousing("0");
                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                        }
                    }else if("2".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrderCmst> cmstQueryWrapper = new QueryWrapper<>();
                        cmstQueryWrapper.eq("state",0);
                        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(cmstQueryWrapper);
                        if(waybillDTOList.size() == baShippingOrderCmsts.size()){
                            BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(waybillDTOList.get(0).getSupplierNo());
                            transportCmst.setOutbound("0");
                            baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                        }
                    }
                }

                baDepotHeadMapper.updateBaDepotHead(baDepotHead);
                String userId = String.valueOf(baDepotHead.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baDepotHead, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaDepotHead baDepotHead, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baDepotHead.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        if(baDepotHead.getType() == 1){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_INTODEPOT.getCode());
        }else if(baDepotHead.getType() == 2){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_OUTPUTDEPOT.getCode());
        }
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            if(baDepotHead.getType() == 1){
                baMessageService.messagePush(sysUser.getPhoneCode(),"入库审批提醒",msgContent,baMessage.getType(),baDepotHead.getId(),baMessage.getBusinessType());
            }else if(baDepotHead.getType() == 2){
                baMessageService.messagePush(sysUser.getPhoneCode(),"出库审批提醒",msgContent,baMessage.getType(),baDepotHead.getId(),baMessage.getBusinessType());
            }
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public Integer output(BaDepotHead baDepotHead) {
        if(StringUtils.isNotEmpty(baDepotHead.getId())){
            BaContractStart baContractStart = null;
            //出库清单
            if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
                if(baDepotHead.getBaDepotInventories().size() > 0){
                    //查询出库单是否存在
                    QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                    inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                    List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                    for (BaDepotInventory baDepotInventory:baDepotInventories) {
                        //存在就删除
                        baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                    }
                    for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                        baDepotInventory.setId(getRedisIncreID.getId());
                        baDepotInventory.setRelationId(baDepotHead.getId());
                        baDepotInventory.setCreateBy(DateUtils.getDate());
                        baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                        if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                            if(baDepotInventory.getSource().equals("1")){
                                //改变磅房数据入库状态
                                QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                                queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                                BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                                baPoundRoom.setStatus("1");
                                baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);

                                //根据货源单ID查询货源
                                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                    if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                        baTransportAutomobile.setOutbound("1"); //货源绑定入库
                                        baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                        baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                    }
                                }
                                //昕科运单入库状态修改
                                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                baShippingOrder.setStatus("2"); //运单绑定状态修改
                                baShippingOrder.setState("1"); //运单绑定状态修改
                                baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                            }else if(baDepotInventory.getSource().equals("2")){
                                //根据货源单ID查询货源
                                if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                    if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                        baTransportAutomobile.setOutbound("1"); //货源绑定入库
                                        baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                        baTransportAutomobile.setSaleId(baDepotHead.getOrderId());
                                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                    }
                                }
                                //改变无车承运数据入库状态
                                if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                    if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                        //昕科运单入库状态修改
                                        BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                        baShippingOrder.setStatus("2");
                                        baShippingOrder.setState("1"); //运单绑定状态修改
                                        baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                        baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                        baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                    }else if(baDepotInventory.getPlatformType().equals("2")){
                                        //中储摘单入库状态修改
                                        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                        baShippingOrderCmst.setStatus("2");
                                        baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                        baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                        baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                        baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    }
                                }
                            }
                        }
                    }
                } else if(baDepotHead.getCargoValue() != null){
                    baDepotHead.setCargoValue(new BigDecimal(0));
                }
            }

            baDepotHead.setOperTime(DateUtils.getNowDate());
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baDepotHead.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            //添加质检信息
            BaCheck baCheck = baDepotHead.getBaCheck();
            Integer result;
            if(StringUtils.isNotNull(check)){
                result = baCheckService.updateBaCheck(baCheck);
            }else {
                baCheck.setType(3);
                baCheck.setRelationId(baDepotHead.getId());
                result = baCheckService.insertBaCheck(baCheck);
            }
            //查询入库信息相关副表数据
            QueryWrapper<BaDepotItem> baDepotItemQueryWrapper = new QueryWrapper<>();
            baDepotItemQueryWrapper.eq("header_id",baDepotHead.getId());
            List<BaDepotItem> baDepotItems = baDepotItemMapper.selectList(baDepotItemQueryWrapper);
            if(StringUtils.isNotNull(baDepotItems)){
                //删除入库相关副表数据
                for (BaDepotItem baDepotItem:baDepotItems) {
                    baDepotItemMapper.deleteBaDepotItemById(baDepotItem.getId());
                }
            }
            if(StringUtils.isNotNull(baDepotHead.getBaDepotItem())){
                if(baDepotHead.getBaDepotItem().size() > 0){
                    for (BaDepotItem item:baDepotHead.getBaDepotItem()) {
                        //新增副表信息
                        item.setId(getRedisIncreID.getId());
                        item.setHeaderId(baDepotHead.getId());
                        item.setCreateTime(DateUtils.getNowDate());
                        item.setCreateBy(SecurityUtils.getUsername());
                        baDepotItemMapper.insertBaDepotItem(item);
                    }
                }
            }
            //计算货值
            if(StringUtils.isNotEmpty(baDepotHead.getOrderId())) {
                baContractStart = baContractStartMapper.selectBaContractStartById(baDepotHead.getOrderId());
                //成本单价
                if (baContractStart.getPrice() != null && baDepotHead.getTotals() != null) {
                    baDepotHead.setCargoValue(baContractStart.getPrice().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            }
            result = baDepotHeadMapper.updateBaDepotHead(baDepotHead);
            if(result > 0){
             if(StringUtils.isNotEmpty(baDepotHead.getOrderId())){
                 //更新出库量
                 if(baContractStart.getScheduledReceipt() != null){
                     baContractStart.setScheduledReceipt(baContractStart.getScheduledReceipt().add(baDepotHead.getTotals()));
                 }else {
                     baContractStart.setScheduledReceipt(baDepotHead.getTotals());
                 }
                 //查询立项信息
                 if(StringUtils.isNotEmpty(baContractStart.getProjectId())){
                     if(StringUtils.isNotEmpty(baDepotHead.getState())) {
                         if (baDepotHead.getState().equals(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode())) {
                             //出库清单
                             if(!CollectionUtils.isEmpty(baDepotHead.getBaDepotInventories()) && baDepotHead.getBaDepotInventories().size() > 0){
                                 baDepotHead.setHeadStatus("2"); //出库清单
                                 baDepotHeadMapper.updateBaDepotHead(baDepotHead);
                             }
                             //查询立项信息
                             BaProject baProject = baProjectMapper.selectBaProjectById(baContractStart.getProjectId());
                             if (StringUtils.isNotNull(baProject.getDepotId())) {
                                 BaMaterialStock materialStock = new BaMaterialStock();
                                 //判断是否选择了库位
                                 if(StringUtils.isNotEmpty(baDepotHead.getLocationId())) {
                                     //查询库存信息
                                     QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                                     stockQueryWrapper.eq("depot_id", baProject.getDepotId());
                                     stockQueryWrapper.eq("location_id", baDepotHead.getLocationId());
                                     stockQueryWrapper.eq("goods_id", baProject.getGoodsId());
                                     materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                                     //更新库存总量
                                     if (materialStock.getInventoryTotal() != null) {
                                         materialStock.setInventoryTotal(materialStock.getInventoryTotal().subtract(baDepotHead.getTotals()));
                                     } else {
                                         materialStock.setInventoryTotal(new BigDecimal(0).subtract(baDepotHead.getTotals()));
                                     }
                                     //更新库存总热值
                                     if (materialStock.getAllAverageCalorific() != null) {
                                         materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific().subtract(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals())));
                                     } else {
                                         materialStock.setAllAverageCalorific(new BigDecimal(0).subtract(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals())));
                                     }
                                     if (materialStock.getAllAverageCalorific() != null && materialStock.getInventoryTotal() != null) {
                                         //平均热值等于库存总热值除以库存总量
                                         //materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal()));
                                         if (materialStock.getInventoryTotal().intValue() == 0) {
                                             materialStock.setAverageCalorific(new BigDecimal(0));
                                         } else if (materialStock.getAllAverageCalorific() != null) {
                                             //平均热值等于库存总热值除以库存总量
                                             materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
                                         }
                                     }
                                 } else {
                                     QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                                     stockQueryWrapper.eq("depot_id", baProject.getDepotId());
                                     stockQueryWrapper.isNull("location_id");
                                     stockQueryWrapper.eq("goods_id", baProject.getGoodsId());
                                     materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                                     //更新库存总量
                                     if (materialStock.getInventoryTotal() != null) {
                                         materialStock.setInventoryTotal(materialStock.getInventoryTotal().subtract(baDepotHead.getTotals()));
                                     } else {
                                         materialStock.setInventoryTotal(new BigDecimal(0).subtract(baDepotHead.getTotals()));
                                     }
                                     //更新库存总热值
                                     if (materialStock.getAllAverageCalorific() != null && baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null) {
                                         materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific().subtract(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals())));
                                     } else if(baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null){
                                         materialStock.setAllAverageCalorific(new BigDecimal(0).subtract(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals())));
                                     }
                                     if (materialStock.getAllAverageCalorific() != null && materialStock.getInventoryTotal() != null) {
                                         //平均热值等于库存总热值除以库存总量
                                         //materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal()));
                                         if (materialStock.getInventoryTotal().intValue() == 0) {
                                             materialStock.setAverageCalorific(new BigDecimal(0));
                                         } else if (materialStock.getAllAverageCalorific() != null) {
                                             //平均热值等于库存总热值除以库存总量
                                             materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
                                         }
                                     }
                                 }
                                 baMaterialStockMapper.updateBaMaterialStock(materialStock);
                                 //出库计算货值均价
                                 if(materialStock.getOutputDepotCargoValue() == null){
                                     materialStock.setOutputDepotCargoValue(new BigDecimal(0));
                                 }
                                 if(baDepotHead.getCargoValue() != null){
                                     //库存入库货值累计 + 本次入库货值
                                     materialStock.setOutputDepotCargoValue(materialStock.getOutputDepotCargoValue().add(baDepotHead.getCargoValue()));
                                 }

                                 if(materialStock.getInventoryAverage() == null){
                                     materialStock.setInventoryAverage(new BigDecimal(0));
                                 }
                                 if(materialStock.getInventoryTotal() == null){
                                     materialStock.setInventoryTotal(new BigDecimal(0));
                                 }
                                 //计算库存均价
                                 materialStock.setInventoryAverage((materialStock.getIntoDepotCargoValue()
                                         .subtract(materialStock.getOutputDepotCargoValue()))
                                         .divide(materialStock.getInventoryTotal(),2, BigDecimal.ROUND_HALF_UP));
                                 baMaterialStockMapper.updateBaMaterialStock(materialStock);
                             }
                         }
                     }
                 }
                 baContractStartMapper.updateBaContractStart(baContractStart);
             }
             //更新库存总量
             /*BaMaterialStock baMaterialStock = new BaMaterialStock();
             baMaterialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
             baMaterialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
             baMaterialStock.setBatch(baDepotHead.getBatch()); //批次
             baMaterialStock.setSupplierId(baDepotHead.getSupplierId());//供应商
             List<BaMaterialStock> baMaterialStocks = baMaterialStockMapper.selectBaMaterialStockList(baMaterialStock);
             if(!CollectionUtils.isEmpty(baMaterialStocks)){
                 baMaterialStock = baMaterialStocks.get(0);
                 baMaterialStock.setCurrentNumber(baMaterialStock.getCurrentNumber() - baDepotHead.getTotals().longValue()); //入库增加库存
                 baMaterialStockMapper.updateBaMaterialStock(baMaterialStock); //更新库存
             }*/
         }else {
             return 0;
         }
        }else {
            return -1;
        }
        return 1;
    }

    @Override
    public List<DepotHeadDTO> outputBaDepotHead(DepotHeadDTO depotHeadDTO) {
       List<DepotHeadDTO> depotHeadDTOList = baDepotHeadMapper.outputBaDepotHead(depotHeadDTO);
        for (DepotHeadDTO dto:depotHeadDTOList) {
            //出入库总量合计
            QueryWrapper<BaDepotHead> queryWrapper = new QueryWrapper<>();
            queryWrapper.like("link_number",dto.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.eq("type",1);
            queryWrapper.eq("sub_type","2");
            queryWrapper.eq("head_status","3");
            BaDepotHead baDepotHead = baDepotHeadMapper.selectOne(queryWrapper);
            if(StringUtils.isNotNull(baDepotHead)){
                dto.setIntoTime(baDepotHead.getOperTime());
                dto.setIntoId(baDepotHead.getId());
            }
        }
        return depotHeadDTOList;
    }

    @Override
    public List<DepotHeadDTO> intoBaDepotHead(DepotHeadDTO depotHeadDTO) {
        List<DepotHeadDTO> depotHeadDTOList = baDepotHeadMapper.outputBaDepotHead(depotHeadDTO);
        for (DepotHeadDTO dto:depotHeadDTOList) {
            //入库总量
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",dto.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            if(StringUtils.isNotNull(check)){
                dto.setIntoAll(check.getCheckCoalNum());
            }

            //判断是否存在加工出库信息
            if(StringUtils.isNotEmpty(dto.getLinkNumber())){
                String[] split = dto.getLinkNumber().split(",");
                //出库总量
                BigDecimal outputAll = new BigDecimal(0);
                for (String id:split) {
                    BaDepotHead baDepotHead = baDepotHeadMapper.selectBaDepotHeadById(id);
                    outputAll = baDepotHead.getTotals().add(outputAll);
                }
                dto.setOutputAll(outputAll);
            }
        }
        return depotHeadDTOList;
    }

    @Override
    public int inventory(BaDepotHead baDepotHead) {
        BaCheck baCheck = new BaCheck();
        //入库清单
        if(StringUtils.isNotNull(baDepotHead.getBaDepotInventories())){
            if(baDepotHead.getBaDepotInventories().size() > 0){
                //查询入库单是否存在
                QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
                inventoryQueryWrapper.eq("relation_id",baDepotHead.getId());
                List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
                for (BaDepotInventory baDepotInventory:baDepotInventories) {
                    //存在就删除
                    baDepotInventoryMapper.deleteBaDepotInventoryById(baDepotInventory.getId());
                }
                for (BaDepotInventory baDepotInventory:baDepotHead.getBaDepotInventories()) {
                    baDepotInventory.setId(getRedisIncreID.getId());
                    baDepotInventory.setRelationId(baDepotHead.getId());
                    baDepotInventory.setCreateBy(DateUtils.getDate());
                    baDepotInventoryMapper.insertBaDepotInventory(baDepotInventory);

                    if(StringUtils.isNotEmpty(baDepotInventory.getSource())){
                        if(baDepotInventory.getSource().equals("1")){
                            //改变磅房数据入库状态
                            QueryWrapper<BaPoundRoom> queryWrapper = new QueryWrapper<>();
                            queryWrapper.eq("waybill_code",baDepotInventory.getWaybillCode());
                            BaPoundRoom baPoundRoom = baPoundRoomMapper.selectOne(queryWrapper);
                            baPoundRoom.setStatus("1");
                            baPoundRoomMapper.updateBaPoundRoom(baPoundRoom);
                        }else if(baDepotInventory.getSource().equals("2")){
                            //根据货源单ID查询货源
                            if(StringUtils.isNotEmpty(baDepotHead.getSupplierNo())){
                                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baDepotHead.getSupplierNo());
                                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                                    baTransportAutomobile.setWarehousing("1"); //货源绑定入库
                                    baTransportAutomobile.setProjectId(baDepotHead.getProjectId());
                                    baTransportAutomobile.setProcureId(baDepotHead.getPurchaseId());
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                                }
                            }
                            //改变无车承运数据入库状态
                            if(StringUtils.isNotEmpty(baDepotInventory.getPlatformType())){
                                if(baDepotInventory.getPlatformType().equals("1") || baDepotInventory.getPlatformType().equals("3")){
                                    //昕科运单入库状态修改
                                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baDepotInventory.getWaybillCode());
                                    baShippingOrder.setStatus("1");
                                    baShippingOrder.setState("1"); //运单绑定状态修改
                                    baShippingOrder.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrder.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                                }else if(baDepotInventory.getPlatformType().equals("2")){
                                    //中储摘单入库状态修改
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baDepotInventory.getWaybillCode());
                                    baShippingOrderCmst.setStatus("1");
                                    baShippingOrderCmst.setState("1"); //运单绑定状态修改
                                    baShippingOrderCmst.setDepotId(baDepotHead.getDepotId());
                                    baShippingOrderCmst.setPositionId(baDepotHead.getLocationId());
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                }
                            }
                        }
                    }
                }
            }
        }
        //化验信息
        if(StringUtils.isNotNull(baDepotHead.getBaCheck())){
            QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
            checkQueryWrapper.eq("relation_id",baDepotHead.getId());
            BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
            //添加质检信息
            baCheck = baDepotHead.getBaCheck();
            if(StringUtils.isNotNull(check)){
                baCheckService.updateBaCheck(baCheck);
            }else {
                baCheck.setType(3);
                baCheck.setRelationId(baDepotHead.getId());
                baCheckService.insertBaCheck(baCheck);
            }
        }
        if(StringUtils.isNotEmpty(baDepotHead.getState())){
            if(baDepotHead.getState().equals(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode())){
                baDepotHead.setHeadStatus("3");
                //补充入库清单更新库存
                //查询采购计划下的清单
                BaPurchaseOrder baPurchaseOrder = baPurchaseOrderMapper.selectBaPurchaseOrderById(baDepotHead.getPurchaseId());
                //采购计划绑定项目ID
                String procurementPlanProjectId = "";
                if(!ObjectUtils.isEmpty(baPurchaseOrder)){
                    //采购清单品名
                    if(StringUtils.isNotEmpty(baPurchaseOrder.getPlanId())){
                        BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                        if(!ObjectUtils.isEmpty(baProcurementPlan)){
                            procurementPlanProjectId = baProcurementPlan.getProjectId();
                        }
                    }
                }
                //查询立项信息
                BaMaterialStock materialStock = new BaMaterialStock();
                //判断是否选择了库位
                if(StringUtils.isNotEmpty(baDepotHead.getLocationId())) {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                    stockQueryWrapper.eq("location_id", baDepotHead.getLocationId());
                    stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setLocationId(baDepotHead.getLocationId());//库位ID
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock.setUpdateBy(SecurityUtils.getUsername());
                    materialStock.setUpdateTime(DateUtils.getNowDate());
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck, baPurchaseOrder);
                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                } else {
                    //查询立项库存信息
                    QueryWrapper<BaMaterialStock> stockQueryWrapper = new QueryWrapper<>();
                    stockQueryWrapper.eq("depot_id", baDepotHead.getDepotId());
                    stockQueryWrapper.eq("goods_id", baDepotHead.getMaterialId());
                    materialStock = baMaterialStockMapper.selectOne(stockQueryWrapper);
                    materialStock.setDepotId(baDepotHead.getDepotId()); //仓库ID
                    materialStock.setGoodsId(baDepotHead.getMaterialId()); //商品ID
                    materialStock.setUpdateBy(SecurityUtils.getUsername());
                    materialStock.setUpdateTime(DateUtils.getNowDate());
                    materialStock = this.calcBaMaterialStock(materialStock, baDepotHead, baCheck, baPurchaseOrder);
                    baMaterialStockMapper.updateBaMaterialStock(materialStock);
                }
                //入库计算货值均价
                if(materialStock.getIntoDepotCargoValue() == null){
                    materialStock.setIntoDepotCargoValue(new BigDecimal(0));
                }
                if(baDepotHead.getCargoValue() != null){
                    //库存入库货值累计 + 本次入库货值
                    materialStock.setIntoDepotCargoValue(materialStock.getIntoDepotCargoValue().add(baDepotHead.getCargoValue()));
                }

                if(materialStock.getInventoryAverage() == null){
                    materialStock.setInventoryAverage(new BigDecimal(0));
                }
                if(materialStock.getIntoDepotCargoValue() != null && materialStock.getInventoryTotal() != null){
                    //计算库存均价
                    materialStock.setInventoryAverage(materialStock.getIntoDepotCargoValue().divide(materialStock.getInventoryTotal(),
                            2, BigDecimal.ROUND_HALF_UP));
                }
                baMaterialStockMapper.updateBaMaterialStock(materialStock);
            }
        }
        return baDepotHeadMapper.updateBaDepotHead(baDepotHead);
    }

    @Override
    public int countBaDepotHeadList(BaDepotHead baDepotHead) {
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        int count = baDepotHeadMapper.countBaDepotHeadList(baDepotHead);
        BaWashHead baWashHead = new BaWashHead();
        baWashHead.setType(baDepotHead.getType());
        if(StringUtils.isNotEmpty(baDepotHead.getDepotId())){
            baWashHead.setDepotId(baDepotHead.getDepotId());
        }
        //租户ID
        baWashHead.setTenantId(SecurityUtils.getCurrComId());
        if(1 == baDepotHead.getType()){ //入库
            count = count + baWashHeadMapper.countIntoBaWashHead(baWashHead);
        } else {
            count = count + baWashHeadMapper.countOutputBaWashHead(baWashHead);
        }

        return count;
    }

    @Override
    public List<BaDepotHead> depotHeadDetail(BaDepotHead baDepotHead) {
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.depotHeadDetail(baDepotHead);
        for (BaDepotHead depotHead:baDepotHeads) {
            //出入库清单
            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",depotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            //车数
            depotHead.setCars(baDepotInventories.size());
            //货主名称
            BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
            baTransportAutomobile.setSupplierNo(depotHead.getSupplierNo());
            List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
            depotHead.setSupplierName(sourceGoodsDTOS.get(0).getSupplierName());
        }
        return baDepotHeads;
    }

    /**
     * 统计入库、出库量
     */
    @Override
    public BaDepotHeadStatVO depotHeadStat(DepotHeadDTO depotHeadDTO) {
        BaDepotHeadStatVO baDepotHeadStatVO = new BaDepotHeadStatVO();
        depotHeadDTO.setCurrentFlag("currentFlag");
        if(depotHeadDTO.getType() == 1) {
            //统计入库
            depotHeadDTO.setState(AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
            //租户ID
            depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
            //入库量
            BigDecimal inDepotTotals = baDepotHeadMapper.totals(depotHeadDTO);

            //统计洗煤、配煤入库
            BigDecimal washTotals = baWashHeadMapper.intoTotals(depotHeadDTO);
            if (inDepotTotals == null) {
                inDepotTotals = new BigDecimal(0);
            }
            if (washTotals != null) {
                inDepotTotals = inDepotTotals.add(washTotals);
            }
            baDepotHeadStatVO.setTodayTotals(inDepotTotals);
        } else {
            //统计出库
            depotHeadDTO.setType(new Long(2));
            depotHeadDTO.setState(AdminCodeEnum.OUTPUTDEPOT_STATUS_PASS.getCode());
            //租户ID
            depotHeadDTO.setTenantId(SecurityUtils.getCurrComId());
            BigDecimal outDepotTotals = baDepotHeadMapper.totals(depotHeadDTO);

            //统计洗煤、配煤出库
            BigDecimal washOutputdepotTotals = baWashHeadMapper.outputTotals(depotHeadDTO);
            if(outDepotTotals == null){
                outDepotTotals = new BigDecimal(0);
            }
            if(washOutputdepotTotals != null){
                outDepotTotals = outDepotTotals.add(washOutputdepotTotals);
            }
            baDepotHeadStatVO.setTodayTotals(outDepotTotals);
        }

        return baDepotHeadStatVO;
    }

    @Override
    public List<BaDepotHead> selectBaDepotHeadAuthorizedList(BaDepotHead baDepotHead) {
        List<BaDepotHead> baDepotHeads = baDepotHeadMapper.selectDepotHeadAuthorized(baDepotHead);
        for (BaDepotHead depotHead:baDepotHeads) {
            //入库总量
            /*if(depotHead.getType() == 1){
                //总量
                QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                checkQueryWrapper.eq("relation_id",depotHead.getId());
                BaCheck check = baCheckMapper.selectOne(checkQueryWrapper);
                if(StringUtils.isNotNull(check)){
                    depotHead.setTotals(check.getCheckCoalNum());
                }
            }*/
            //岗位信息
            String name = getName.getName(depotHead.getUserId());
            //发起人
            if(name.equals("") == false){
                depotHead.setUserName(sysUserMapper.selectUserById(depotHead.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                depotHead.setUserName(sysUserMapper.selectUserById(depotHead.getUserId()).getNickName());
            }
            //入库清单
            QueryWrapper<BaDepotInventory> inventoryQueryWrapper = new QueryWrapper<>();
            inventoryQueryWrapper.eq("relation_id",depotHead.getId());
            List<BaDepotInventory> baDepotInventories = baDepotInventoryMapper.selectList(inventoryQueryWrapper);
            depotHead.setBaDepotInventories(baDepotInventories);

            //根据货源单ID查询货源
            if(StringUtils.isNotEmpty(depotHead.getSupplierNo())){
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(depotHead.getSupplierNo());
                if(!ObjectUtils.isEmpty(baTransportAutomobile)){
                    depotHead.setSupplierUserName(baTransportAutomobile.getSupplierName());
                    depotHead.setGoodsName(baTransportAutomobile.getGoodsName());
                }
            }
            //采购订单
            if(StringUtils.isNotEmpty(depotHead.getPurchaseId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(depotHead.getPurchaseId());
                if(StringUtils.isNotNull(baProcurementPlan)){
                    depotHead.setPurchaseName(baProcurementPlan.getPlanName());
                }
            }
            //销售订单
            if(StringUtils.isNotEmpty(depotHead.getOrderId())){
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(depotHead.getOrderId());
                if(StringUtils.isNotNull(baContractStart)){
                    depotHead.setOrderName(baContractStart.getName());
                }
            }
        }
        return baDepotHeads;
    }

    @Override
    public int authorizedBaDepotHead(BaDepotHead baDepotHead) {
        return baDepotHeadMapper.updateBaDepotHead(baDepotHead);
    }

    @Override
    public List<BaDepotInventory> selectBaDepotInventory(BaDepotInventory baDepotInventory) {
        return baDepotInventoryMapper.selectBaDepotInventory(baDepotInventory);
    }

    //计算更新库存数据
    private BaMaterialStock calcBaMaterialStock(BaMaterialStock materialStock, BaDepotHead baDepotHead,BaCheck baCheck, BaPurchaseOrder baPurchaseOrder){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().add(baDepotHead.getTotals()));
        }
        //更新库存总热值
        if (materialStock.getAllAverageCalorific() == null) {
            materialStock.setAllAverageCalorific(new BigDecimal(0));
        }
        if (baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null) {
            materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific()
                    .add(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP)));
        }
        //计算库存均价, 库存均价=（入库量*采购清单含税单价-销售量*成本单价-盘点均价*库存增减）/库存吨数
        if (materialStock.getInventoryAverage() == null) {
            materialStock.setInventoryAverage(new BigDecimal(0));
        }
        //含税单价
        if(baPurchaseOrder.getIncludingPrice() != null){

        }


        materialStock.setInventoryAverage(baDepotHead.getProcurePrice());
        //库存总量（吨）
        if (materialStock.getInventoryTotal() != null) {
            if (materialStock.getInventoryTotal().intValue() == 0) {
                materialStock.setAverageCalorific(new BigDecimal(0));
            } else if (materialStock.getAllAverageCalorific() != null) {
                //平均热值等于库存总热值除以库存总量
                materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
            }
        }
        return materialStock;
    }

    /**
     * 入库申请审批通过计算更新库存数据
     * @param materialStock
     * @param baDepotHead
     * @param baCheck
     * @return
     */
    private BaMaterialStock calcBaMaterialStock(BaMaterialStock materialStock, BaDepotHead baDepotHead,BaCheck baCheck){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().add(baDepotHead.getTotals()));
        }
        //更新库存总热值
//        if (materialStock.getAllAverageCalorific() == null) {
//            materialStock.setAllAverageCalorific(new BigDecimal(0));
//        }
//        if (null != baCheck.getLowerCalorificValue() && baDepotHead.getTotals() != null) {
//            materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific()
//                    .add(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP)));
//        }
        //库存均价
        if (materialStock.getInventoryAverage() == null) {
            materialStock.setInventoryAverage(baDepotHead.getProcurePrice());
        }
        //库存总量（吨）
        if (materialStock.getInventoryTotal() != null) {
            if (materialStock.getInventoryTotal().intValue() == 0) {
                materialStock.setAverageCalorific(new BigDecimal(0));
            } else if (materialStock.getAllAverageCalorific() != null) {
                //平均热值等于库存总热值除以库存总量
                materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
            }
        }
        return materialStock;
    }

    /**
     * 出库申请审批通过计算更新库存数据
     * @param materialStock
     * @param baDepotHead
     * @param baCheck
     * @return
     */
    private BaMaterialStock calcBaMaterialStock1(BaMaterialStock materialStock, BaDepotHead baDepotHead,BaCheck baCheck){
        //更新库存总量
        if (materialStock.getInventoryTotal() == null) {
            materialStock.setInventoryTotal(new BigDecimal(0));
        }
        if (baDepotHead.getTotals() != null && baDepotHead.getTotals().compareTo(BigDecimal.ZERO) != 0) {
            materialStock.setInventoryTotal(materialStock.getInventoryTotal().subtract(baDepotHead.getTotals()));
        }
        //更新库存总热值
        if (materialStock.getAllAverageCalorific() == null) {
            materialStock.setAllAverageCalorific(new BigDecimal(0));
        }
        if (baCheck.getLowerCalorificValue() != null && baDepotHead.getTotals() != null) {
            materialStock.setAllAverageCalorific(materialStock.getAllAverageCalorific()
                    .add(baCheck.getLowerCalorificValue().multiply(baDepotHead.getTotals()).setScale(2, BigDecimal.ROUND_HALF_UP)));
        }
        //库存均价
        if (materialStock.getInventoryAverage() == null) {
            materialStock.setInventoryAverage(baDepotHead.getProcurePrice());
        }
        //库存总量（吨）
        if (materialStock.getInventoryTotal() != null) {
            if (materialStock.getInventoryTotal().intValue() == 0) {
                materialStock.setAverageCalorific(new BigDecimal(0));
            } else if (materialStock.getAllAverageCalorific() != null) {
                //平均热值等于库存总热值除以库存总量
                materialStock.setAverageCalorific(materialStock.getAllAverageCalorific().divide(materialStock.getInventoryTotal(), 2, BigDecimal.ROUND_HALF_UP));
            }
        }
        return materialStock;
    }
}
