package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IBaAutomobileSettlementInvoiceService;

/**
 * 无车承运开票Service业务层处理
 *
 * @author single
 * @date 2023-04-12
 */
@Service
public class BaAutomobileSettlementInvoiceServiceImpl extends ServiceImpl<BaAutomobileSettlementInvoiceMapper, BaAutomobileSettlementInvoice> implements IBaAutomobileSettlementInvoiceService
{
    @Autowired
    private BaAutomobileSettlementInvoiceMapper baAutomobileSettlementInvoiceMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    @Autowired
    private BaAutomobileSettlementInvoiceDetailMapper baAutomobileSettlementInvoiceDetailMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    /**
     * 查询无车承运开票
     *
     * @param id 无车承运开票ID
     * @return 无车承运开票
     */
    @Override
    public BaAutomobileSettlementInvoice selectBaAutomobileSettlementInvoiceById(String id)
    {
        BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceById(id);
        QueryWrapper<BaAutomobileSettlementInvoiceDetail> invoiceDetailQueryWrapper = new QueryWrapper<>();
        invoiceDetailQueryWrapper.eq("invoice_no",baAutomobileSettlementInvoice.getInvoiceNo());
        List<BaAutomobileSettlementInvoiceDetail> baAutomobileSettlementInvoiceDetails = baAutomobileSettlementInvoiceDetailMapper.selectList(invoiceDetailQueryWrapper);
        for (BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail:baAutomobileSettlementInvoiceDetails) {
            if(StringUtils.isNotEmpty(baAutomobileSettlementInvoiceDetail.getExternalOrder())){
                //查询运单信息
                BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(baAutomobileSettlementInvoiceDetail.getExternalOrder());
                //签收时间
                if(StringUtils.isNull(baAutomobileSettlementInvoiceDetail.getActReceiveTime())){
                   baAutomobileSettlementInvoiceDetail.setActDeliverTime(baShippingOrder.getDhdate());
                }
                //车牌号
                if(StringUtils.isEmpty(baAutomobileSettlementInvoiceDetail.getPlateNumber())){
                   baAutomobileSettlementInvoiceDetail.setPlateNumber(baShippingOrder.getCarno());
                }
                if(StringUtils.isNotEmpty(baShippingOrder.getSupplierNo())){
                    //查询货源信息
                    BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baShippingOrder.getSupplierNo());
                    baAutomobileSettlementInvoiceDetail.setSupplierNo(baTransportAutomobile.getSupplierNo());
                    //查询合同启动
                    if(StringUtils.isNotEmpty(baTransportAutomobile.getOrderId())){
                        BaTransport baTransport = baTransportMapper.selectBaTransportById(baTransportAutomobile.getOrderId());
                        if(StringUtils.isNotEmpty(baTransport.getStartId())){
                            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baTransport.getStartId());
                            baAutomobileSettlementInvoiceDetail.setStarName(baContractStart.getName());
                        }
                    }
                }
            }
        }
        baAutomobileSettlementInvoice.setBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetails);

        return baAutomobileSettlementInvoice;
    }

    /**
     * 查询无车承运开票列表
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 无车承运开票
     */
    @Override
    public List<BaAutomobileSettlementInvoice> selectBaAutomobileSettlementInvoiceList(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        List<BaAutomobileSettlementInvoice> baAutomobileSettlementInvoices = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceList(baAutomobileSettlementInvoice);
        //运费金额
        BigDecimal moneyTrans = new BigDecimal(0);
        for (BaAutomobileSettlementInvoice baAutomobileSettlementInvoice1:baAutomobileSettlementInvoices) {
            QueryWrapper<BaAutomobileSettlementInvoiceDetail> invoiceDetailQueryWrapper = new QueryWrapper<>();
            invoiceDetailQueryWrapper.eq("invoice_no",baAutomobileSettlementInvoice1.getInvoiceNo());
            List<BaAutomobileSettlementInvoiceDetail> baAutomobileSettlementInvoiceDetails = baAutomobileSettlementInvoiceDetailMapper.selectList(invoiceDetailQueryWrapper);
            //运单数
            baAutomobileSettlementInvoice1.setWaybill(baAutomobileSettlementInvoiceDetails.size());
            for (BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail:baAutomobileSettlementInvoiceDetails) {
                moneyTrans = moneyTrans.add(baAutomobileSettlementInvoiceDetail.getMoneyTrans());
            }
            baAutomobileSettlementInvoice1.setMoneyTrans(moneyTrans);
        }
        return baAutomobileSettlementInvoices;
    }

    /**
     * 新增无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        baAutomobileSettlementInvoice.setCreateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceMapper.insertBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
    }

    /**
     * 修改无车承运开票
     *
     * @param baAutomobileSettlementInvoice 无车承运开票
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlementInvoice(BaAutomobileSettlementInvoice baAutomobileSettlementInvoice)
    {
        baAutomobileSettlementInvoice.setUpdateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceMapper.updateBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
    }

    /**
     * 批量删除无车承运开票
     *
     * @param ids 需要删除的无车承运开票ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceByInvoiceNo(id);
                baAutomobileSettlementInvoice.setFlag(1L);
                baAutomobileSettlementInvoiceMapper.updateBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除无车承运开票信息
     *
     * @param id 无车承运开票ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceById(String id)
    {
        return baAutomobileSettlementInvoiceMapper.deleteBaAutomobileSettlementInvoiceById(id);
    }

    /**
     * 昕科推送发票（发票信息）
     */
    @Override
    public int addAutomobileSettlementInvoice(JSONObject automobileSettlementInvoice) {
        BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = new BaAutomobileSettlementInvoice();
        //主键id
        baAutomobileSettlementInvoice.setId(getRedisIncreID.getId());
        //开票批次号
        baAutomobileSettlementInvoice.setInvoiceNo(automobileSettlementInvoice.getString("invoiceNo"));
        //开票单据名称
        baAutomobileSettlementInvoice.setInvoiceName(automobileSettlementInvoice.getString("invoiceName"));
        //归属货主公司
        baAutomobileSettlementInvoice.setCompanyName(automobileSettlementInvoice.getString("companyName"));
        //结算批次号
        //baAutomobileSettlementInvoice.setJsNo(automobileSettlementInvoice.getString("jsNo"));
        //运费补差费（服务费）
        baAutomobileSettlementInvoice.setServiceDifferenceAmount(automobileSettlementInvoice.getBigDecimal("serviceDifferenceAmount"));
        //合计金额
        baAutomobileSettlementInvoice.setInvoiceMoney(automobileSettlementInvoice.getBigDecimal("invoiceMoney"));
        //申请时间
        baAutomobileSettlementInvoice.setApplyTime(automobileSettlementInvoice.getDate("applyTime"));
        //开票状态
        baAutomobileSettlementInvoice.setCheckFlag(1);
        //新增时间
        baAutomobileSettlementInvoice.setCreateTime(DateUtils.getNowDate());
        //获取结算详情本地环境
        //String url = "https://4942h442k4.yicp.fun/billing/invoice/invoiceDetails";
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/billing/invoice/invoiceDetails";
        Map<String, String> map = new HashMap<>();
        map.put("invoiceNo", baAutomobileSettlementInvoice.getInvoiceNo());
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(map));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        if(obj.getString("code").equals("200")){
             //获取data对象集合
            JSONArray data = obj.getJSONArray("data");
            for (Object invoiceObj : data) {
                BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail = JSONObject.toJavaObject((JSONObject) JSON.toJSON(invoiceObj), BaAutomobileSettlementInvoiceDetail.class);
                baAutomobileSettlementInvoiceDetail.setInvoiceNo(baAutomobileSettlementInvoice.getInvoiceNo());
                baAutomobileSettlementInvoiceDetail.setId(getRedisIncreID.getId());
                baAutomobileSettlementInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                baAutomobileSettlementInvoiceDetailMapper.insertBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail);
            }
        }else {
            return 0;
        }
        return baAutomobileSettlementInvoiceMapper.insertBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
    }

    @Override
    public int added(JSONObject automobileSettlementInvoice) {
        BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = JSONObject.toJavaObject((JSONObject) JSON.toJSON(automobileSettlementInvoice), BaAutomobileSettlementInvoice.class);
        baAutomobileSettlementInvoice.setId(getRedisIncreID.getId());
        return baAutomobileSettlementInvoiceMapper.insertBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
    }

    @Override
    public int updateInvoice(JSONObject object) {
        QueryWrapper<BaAutomobileSettlementInvoice> invoiceQueryWrapper = new QueryWrapper<>();
        invoiceQueryWrapper.eq("invoice_no",object.getString("invoiceNo"));
        BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectOne(invoiceQueryWrapper);
        baAutomobileSettlementInvoice.setCheckFlag(object.getInteger("checkFlag"));
        baAutomobileSettlementInvoice.setRemark(object.getString("remark"));
        return baAutomobileSettlementInvoiceMapper.updateBaAutomobileSettlementInvoice(baAutomobileSettlementInvoice);
    }

    @Override
    public int selectInvoice(String invoiceNo) {
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/billing/invoice/invoiceDetails";
        Map<String, String> map = new HashMap<>();
        map.put("invoiceNo", invoiceNo);
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(map));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        if (obj.getString("code").equals("200")) {
            //获取data对象集合
            JSONArray data = obj.getJSONArray("data");
            for (Object invoiceObj : data) {
                BaAutomobileSettlementInvoiceDetail baAutomobileSettlementInvoiceDetail = JSONObject.toJavaObject((JSONObject) JSON.toJSON(invoiceObj), BaAutomobileSettlementInvoiceDetail.class);
                baAutomobileSettlementInvoiceDetail.setInvoiceNo(invoiceNo);
                baAutomobileSettlementInvoiceDetail.setId(getRedisIncreID.getId());
                baAutomobileSettlementInvoiceDetail.setCreateTime(DateUtils.getNowDate());
                baAutomobileSettlementInvoiceDetailMapper.insertBaAutomobileSettlementInvoiceDetail(baAutomobileSettlementInvoiceDetail);
            }
            return 1;
        }
        return 0;
     }
}
