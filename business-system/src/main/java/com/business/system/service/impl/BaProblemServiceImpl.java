package com.business.system.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaContractStart;
import com.business.system.mapper.BaContractStartMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.util.PostName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaProblemMapper;
import com.business.system.domain.BaProblem;
import com.business.system.service.IBaProblemService;

/**
 * 问题Service业务层处理
 *
 * @author single
 * @date 2023-10-09
 */
@Service
public class BaProblemServiceImpl extends ServiceImpl<BaProblemMapper, BaProblem> implements IBaProblemService
{
    @Autowired
    private BaProblemMapper baProblemMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PostName getName;

    /**
     * 查询问题
     *
     * @param id 问题ID
     * @return 问题
     */
    @Override
    public BaProblem selectBaProblemById(String id)
    {
        return baProblemMapper.selectBaProblemById(id);
    }

    @Override
    public BaProblem selectBaProblemByStartId(String startId) {
        QueryWrapper<BaProblem> problemQueryWrapper = new QueryWrapper<>();
        problemQueryWrapper.eq("start_id",startId);
        problemQueryWrapper.eq("problem_state","0");
        BaProblem baProblem = baProblemMapper.selectOne(problemQueryWrapper);
        //标记人
        SysUser user = sysUserMapper.selectUserById(baProblem.getUserId());
        //岗位信息
        String name = getName.getName(baProblem.getUserId());
        //发起人
        if (name.equals("") == false) {
            baProblem.setUserName(user.getNickName() + "-" + name.substring(0, name.length() - 1));
        } else {
            baProblem.setUserName(user.getNickName());
        }
        return baProblem;
    }

    /**
     * 查询问题列表
     *
     * @param baProblem 问题
     * @return 问题
     */
    @Override
    public List<BaProblem> selectBaProblemList(BaProblem baProblem)
    {
        return baProblemMapper.selectBaProblemList(baProblem);
    }

    /**
     * 新增问题
     *
     * @param baProblem 问题
     * @return 结果
     */
    @Override
    public int insertBaProblem(BaProblem baProblem)
    {
        baProblem.setCreateTime(DateUtils.getNowDate());
        baProblem.setId(getRedisIncreID.getId());
        baProblem.setUserId(SecurityUtils.getUserId());
        baProblem.setDeptId(SecurityUtils.getDeptId());
        baProblem.setTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(baProblem.getStartId())){
            //修改合同启动问题状态
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baProblem.getStartId());
            baContractStart.setProblemMark("1");
            baContractStartMapper.updateBaContractStart(baContractStart);
        }
        return baProblemMapper.insertBaProblem(baProblem);
    }

    /**
     * 修改问题
     *
     * @param baProblem 问题
     * @return 结果
     */
    @Override
    public int updateBaProblem(BaProblem baProblem)
    {
        baProblem.setUpdateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(baProblem.getCloseIllustrate())){
            if(StringUtils.isNotEmpty(baProblem.getStartId())){
                //关闭问题项目
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baProblem.getStartId());
                baContractStart.setProblemMark("0");
                baContractStartMapper.updateBaContractStart(baContractStart);
            }
            //关闭时间
            baProblem.setTime(baProblem.getUpdateTime());
            baProblem.setProblemState("1");
        }
        //更新时间
        baProblem.setTime(baProblem.getUpdateTime());
        return baProblemMapper.updateBaProblem(baProblem);
    }

    /**
     * 批量删除问题
     *
     * @param ids 需要删除的问题ID
     * @return 结果
     */
    @Override
    public int deleteBaProblemByIds(String[] ids)
    {
        return baProblemMapper.deleteBaProblemByIds(ids);
    }

    /**
     * 删除问题信息
     *
     * @param id 问题ID
     * @return 结果
     */
    @Override
    public int deleteBaProblemById(String id)
    {
        return baProblemMapper.deleteBaProblemById(id);
    }

}
