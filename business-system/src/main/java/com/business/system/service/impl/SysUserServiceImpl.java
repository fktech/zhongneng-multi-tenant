package com.business.system.service.impl;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.validation.Validator;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.BaUserVO;
import com.business.system.mapper.*;
import com.business.system.service.DingDingService;
import com.business.system.service.ISysDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.business.common.annotation.DataScope;
import com.business.common.constant.UserConstants;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.exception.ServiceException;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanValidators;
import com.business.common.utils.spring.SpringUtils;
import com.business.system.service.ISysConfigService;
import com.business.system.service.ISysUserService;
import org.springframework.util.ObjectUtils;

/**
 * 用户 业务层处理
 *
 * @author ruoyi
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>  implements ISysUserService
{
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysPostMapper postMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SysUserPostMapper userPostMapper;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    protected Validator validator;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaClockSummaryMapper oaClockSummaryMapper;

    @Autowired
    private DingDingService dingDingService;

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUserList(SysUser user)
    {
        List<SysUser> sysUsers = userMapper.selectUserList(user);
        String postName = "";
        if(!CollectionUtils.isEmpty(sysUsers)){
            for(SysUser sysUser : sysUsers){
                postName = "";
                List<SysUserPost> sysUserPosts = userPostMapper.selectUserPost(sysUser.getUserId());
                if(!CollectionUtils.isEmpty(sysUserPosts)){
                    for(SysUserPost sysUserPost : sysUserPosts){
                        if("".equals(postName)){
                            postName = sysUserPost.getPostName();
                        } else {
                            postName = postName + "," + sysUserPost.getPostName();
                        }
                    }
                }
                sysUser.setPostName(postName);
                if(StringUtils.isNotEmpty(sysUser.getPartDeptId())){
                    //兼职部门
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(sysUser.getPartDeptId()));
                    sysUser.setPartDeptName(dept.getDeptName());
                    //查询祖籍
                    String[] split = dept.getAncestors().split(",");
                    List<Long> integerList = new ArrayList<>();
                    for (String deptId:split) {
                        integerList.add(Long.valueOf(deptId));
                    }
                    //倒叙拿到第一个公司
                    Collections.reverse(integerList);
                    for (Long itm:integerList) {
                        SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                        if(dept1.getDeptType().equals("1")){
                            sysUser.setPartDeptCompany(dept1.getDeptName());
                            break;
                        }
                    }
                }
            }
        }
        return sysUsers;
    }

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    public List<SysUser> userList(SysUser user)
    {
        List<SysUser> sysUsers = userMapper.userList(user);
        for (SysUser sysUser:sysUsers) {
            String postName = "";
            List<SysUserPost> sysUserPosts = userPostMapper.selectUserPost(sysUser.getUserId());
            for (SysUserPost userPost:sysUserPosts) {
                SysPost sysPost = postMapper.selectPostById(userPost.getPostId());
                if(StringUtils.isNotNull(sysPost)){
                    postName = sysPost.getPostName() + "," + postName;
                }
            }
            if(StringUtils.isNotEmpty(postName) && postName.equals("") == false){
                sysUser.setPostName(postName.substring(0,postName.length()-1));
            }
            if(StringUtils.isNotEmpty(sysUser.getPartDeptId()) && !"".equals(user.getPartDeptId())){
                //兼职部门
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(sysUser.getPartDeptId()));
                sysUser.setPartDeptName(dept.getDeptName());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                List<Long> integerList = new ArrayList<>();
                for (String deptId:split) {
                    integerList.add(Long.valueOf(deptId));
                }
                //倒叙拿到第一个公司
                Collections.reverse(integerList);
                for (Long itm:integerList) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                    if(dept1.getDeptType().equals("1")){
                        sysUser.setPartDeptCompany(dept1.getDeptName());
                        break;
                    }
                }
            }
        }
        return sysUsers;
    }

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    public List<SysUser> selectAllUserList(SysUser user)
    {
        return userMapper.selectUserList(user);
    }

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectTreeUserList(SysUser user)
    {
        return userMapper.selectTreeUserList(user);
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectAllocatedList(SysUser user)
    {
        return userMapper.selectAllocatedList(user);
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUnallocatedList(SysUser user)
    {
        user.setComId(SecurityUtils.getCurrComId());
        return userMapper.selectUnallocatedList(user);
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String userName)
    {
        return userMapper.selectUserByUserName(userName);
    }

    @Override
    public SysUser selectUserByPhone(String phonenumber)
    {
        return userMapper.selectUserByPhone(phonenumber);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(Long userId)
    {
        SysUser user = userMapper.selectUserById(userId);
        //归属部门
        if(StringUtils.isNotNull(user)){
            if(user.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                List<Long> integerList = new ArrayList<>();
                for (String deptId:split) {
                    integerList.add(Long.valueOf(deptId));
                }
                //倒叙拿到第一个公司
                Collections.reverse(integerList);
                for (Long itm:integerList) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                    if(StringUtils.isNotNull(dept1)){
                        if(dept1.getDeptType().equals("1")){
                            user.setPartDeptCompany(dept1.getDeptName());
                            break;
                        }
                    }
                }
                user.setDeptName(user.getPartDeptCompany()+"-"+dept.getDeptName());
            }
            //兼职部门
            if(StringUtils.isNotEmpty(user.getPartDeptId()) && !"".equals(user.getPartDeptId())){
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(user.getPartDeptId()));
                //查询祖籍
                String[] split = dept.getAncestors().split(",");
                List<Long> integerList = new ArrayList<>();
                for (String deptId:split) {
                    integerList.add(Long.valueOf(deptId));
                }
                //倒叙拿到第一个公司
                Collections.reverse(integerList);
                for (Long itm:integerList) {
                    SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                    if(StringUtils.isNotNull(dept1)) {
                        if (dept1.getDeptType().equals("1")) {
                            user.setPartDeptCompany(dept1.getDeptName());
                            break;
                        }
                    }
                }
                user.setPartDeptName(user.getPartDeptCompany()+"-"+dept.getDeptName());
            }
        }
        return user;
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName)
    {
        List<SysRole> list = roleMapper.selectRolesByUserName(userName, SecurityUtils.getCurrComId());
        if (CollectionUtils.isEmpty(list))
        {
            return StringUtils.EMPTY;
        }
        return list.stream().map(SysRole::getRoleName).collect(Collectors.joining(","));
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserPostGroup(String userName)
    {
        List<SysPost> list = postMapper.selectPostsByUserName(userName, SecurityUtils.getCurrComId());
        if (CollectionUtils.isEmpty(list))
        {
            return StringUtils.EMPTY;
        }
        return list.stream().map(SysPost::getPostName).collect(Collectors.joining(","));
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkUserNameUnique(user.getUserName());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 信链云APP注册校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public String registerCheckUserNameUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkUserNameUnique(user.getUserName());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户工号是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public String checkJobNoUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkJobNoUnique(user.getJobNo(), SecurityUtils.getCurrComId());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户岗位编码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public String checkJobCodeUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkJobCodeUnique(user.getJobCode(), SecurityUtils.getCurrComId());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户副岗位编码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public String checkDeputyPostCodeUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkDeputyPostCodeUnique(user.getDeputyPostCode(), SecurityUtils.getCurrComId());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber(), SecurityUtils.getCurrComId());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique1(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber(), null);
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = userMapper.checkEmailUnique(user.getEmail(), SecurityUtils.getCurrComId());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    @Override
    public void checkUserAllowed(SysUser user)
    {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin())
        {
            throw new ServiceException("不允许操作超级管理员用户");
        }
    }

    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    @Override
    public void checkUserDataScope(Long userId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysUser user = new SysUser();
            user.setUserId(userId);
            List<SysUser> users = SpringUtils.getAopProxy(this).selectUserList(user);
            if (StringUtils.isEmpty(users))
            {
                throw new ServiceException("没有权限访问用户数据！");
            }
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUser(SysUser user)
    {
        if(StringUtils.isEmpty(user.getComId())){
            user.setComId(SecurityUtils.getCurrComId());
        }
        SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
        user.setDept(dept);
        //中能租户下数据同步至钉钉
        //钉钉新增用户
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456")){
            try {
                String departmentCreate = dingDingService.userCreate(user);
                Object parse = JSONObject.parse(departmentCreate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    user.setDingDing(user.getUserName());
                    msg = "success";
                }else if(errmsg.equals("已发出邀请，对方同意后即可加入组织")){
                    user.setDingDing(user.getUserName());
                    user.setDingSource(1);
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int rows = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                // 新增用户信息
                rows = userMapper.insertUser(user);
                // 新增用户岗位关联
                insertUserPost(user);
                // 新增用户与角色管理
                insertUserRole(user);
            }
        }
        return rows;
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUserRegister(SysUser user)
    {
        if(StringUtils.isEmpty(user.getComId())){
            user.setComId(SecurityUtils.getCurrComId());
        }
        SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
        user.setDept(dept);
        // 新增用户信息
        int rows = userMapper.insertUser(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
        return rows;
    }

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public boolean registerUser(SysUser user)
    {
        return userMapper.insertUser(user) > 0;
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateUser(SysUser user)
    {
        //钉钉修改用户
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456")){
            try {
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                user.setDept(dept);
                String departmentCreate = dingDingService.userUpdate(user);
                Object parse = JSONObject.parse(departmentCreate);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    msg = "success";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int rows = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                Long userId = user.getUserId();
                // 删除用户与角色关联
                userRoleMapper.deleteUserRoleByUserId(userId);
                // 新增用户与角色管理
                insertUserRole(user);
                // 删除用户与岗位关联
                userPostMapper.deleteUserPostByUserId(userId);
                // 新增用户与岗位管理
                insertUserPost(user);
                rows = userMapper.updateUser(user);
            }
        }
        return rows;
    }

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    @Override
    @Transactional
    public void insertUserAuth(Long userId, Long[] roleIds)
    {
        userRoleMapper.deleteUserRoleByUserId(userId);
        insertUserRole(userId, roleIds);
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserStatus(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    @Override
    public int updateUserPhoneCode(SysUser user) {

        if(StringUtils.isEmpty(user.getPhoneCode())){
            user.setPhoneCode("");
        }

        return userMapper.updateUser(user);
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserProfile(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    @Override
    public boolean updateUserAvatar(String userName, String avatar)
    {
        return userMapper.updateUserAvatar(userName, avatar, SecurityUtils.getCurrComId()) > 0;
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(SysUser user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(String userName, String password)
    {
        return userMapper.resetUserPwd(userName, password, SecurityUtils.getCurrComId());
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user)
    {
        this.insertUserRole(user.getUserId(), user.getRoleIds());
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    public void insertUserPost(SysUser user)
    {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotEmpty(posts))
        {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>(posts.length);
            for (Long postId : posts)
            {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            userPostMapper.batchUserPost(list);
        }
    }

    /**
     * 新增用户角色信息
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserRole(Long userId, Long[] roleIds)
    {
        if (StringUtils.isNotEmpty(roleIds))
        {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>(roleIds.length);
            for (Long roleId : roleIds)
            {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(userId);
                ur.setRoleId(roleId);
                list.add(ur);
            }
            userRoleMapper.batchUserRole(list);
        }
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteUserById(Long userId)
    {
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
        userPostMapper.deleteUserPostByUserId(userId);
        return userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteUserByIds(Long[] userIds)
    {
        //删除钉钉用户
        for (Long userId : userIds)
        {
            checkUserAllowed(new SysUser(userId));
            checkUserDataScope(userId);
        }
        // 删除用户与角色关联
        userRoleMapper.deleteUserRole(userIds);
        // 删除用户与岗位关联
        userPostMapper.deleteUserPost(userIds);
        return userMapper.deleteUserByIds(userIds);
    }

    /**
     * 删除企业用户
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Override
    public int deleteUser(Long[] userIds)
    {
        return userMapper.deleteUserByIds(userIds);
    }

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        for (SysUser user : userList)
        {
            try
            {
                // 验证是否存在这个用户
                SysUser u = userMapper.selectUserByUserName(user.getUserName());
                if (StringUtils.isNull(u))
                {
                    BeanValidators.validateWithException(validator, user);
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    BeanValidators.validateWithException(validator, user);
                    checkUserAllowed(user);
                    checkUserDataScope(user.getUserId());
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 封装当前登录人用户信息
     */
    @Override
    public UserInfo getCurrentUserInfo(){
        UserInfo userInfo = new UserInfo();
        LoginUser loginUser = SecurityUtils.getLoginUser();
        if(!ObjectUtils.isEmpty(loginUser)){
            userInfo.setId(String.valueOf(loginUser.getUserId()));
            userInfo.setName(loginUser.getUsername());
            userInfo.setNickName(loginUser.getUser().getNickName());
            //根据部门ID获取部门信息
            SysDept sysDept = iSysDeptService.selectDeptById(loginUser.getDeptId());
            if(!ObjectUtils.isEmpty(sysDept)){
                userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
            }
        }

        return userInfo;
    }

    @Override
    public List<SysUser> selectUserByRoleId(SysUserRoleDTO sysUserRoleDTO) {
        return userMapper.selectUserByRoleId(sysUserRoleDTO);
    }

    @Override
    public List<SysUser> selectUserByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO) {
        return userMapper.selectUserByRole(sysUserByRoleIdDTO);
    }

    /**
     * 根据角色ID查询自定义数据权限部门列表
     * @param sysDeptRoleDTO
     * @return
     */
    public List<SysDept> selectDeptsByRoleId(SysDeptRoleDTO sysDeptRoleDTO) {
        return userMapper.selectDeptsByRoleId(sysDeptRoleDTO);
    }

    /**
     * 根据部门查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepId(SysUserDepDTO sysUserDepDTO) {
        return userMapper.selectUserByDepId(sysUserDepDTO);
    }

    /**
     * 根据部门及角色查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepIdAndRole(SysUserDepDTO sysUserDepDTO) {
        return userMapper.selectUserByDepIdAndRole(sysUserDepDTO);
    }

    /**
     * 根据部门及角色查询用户
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByDepIdAndRoleName(SysUserDepDTO sysUserDepDTO) {
        return userMapper.selectUserByDepIdAndRoleName(sysUserDepDTO);
    }

    @Override
    public List<String> selectUserByRoleIdAndDepRole(SysUserByDepRoleDTO sysUserByDepRoleDTO) {
        return userMapper.selectUserByRoleIdAndDepRole(sysUserByDepRoleDTO);
    }

    /**
     * 根据用户ID查询角色列表
     *
     */
    @Override
    public List<String> selectRolesByUser(SysRolesByUserDTO sysRolesByUserDTO) {
        return userMapper.selectRolesByUser(sysRolesByUserDTO);
    }

    /**
     * 根据用户ID查询角色列表
     *
     */
    @Override
    public List<String> selectOperationRolesByUser(SysRolesByUserDTO sysRolesByUserDTO) {
        return userMapper.selectOperationRolesByUser(sysRolesByUserDTO);
    }


    /**
     * openId获取用户信息
     */
    @Override
    public SysUser selectOpenIdByUser(String openId) {

        return userMapper.selectOpenIdByUser(openId);
    }

    /**
     * 根据角色ID查询用户ID列表
     */
    @Override
    public List<String> selectUserIdsByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO) {
        return userMapper.selectUserIdsByRole(sysUserByRoleIdDTO);
    }

    /**
     * 根据用户部门ID集合查询负责部门角色用户列表
     * @param sysRoleUserByDeptDTO
     * @return
     */
    @Override
    public List<SysUser> selectRoleUsersByUser(SysRoleUserByDeptDTO sysRoleUserByDeptDTO) {
        List<SysUser> sysUsers = userMapper.selectRoleUsersByUser(sysRoleUserByDeptDTO.getDeptId(), SecurityUtils.getCurrComId());
        return sysUsers;
    }

    /**
     * 通过用户名查询用户
     *
     * @return 用户对象信息
     */
    @Override
    public Map<String, SysUser> selectUserMapByUserName(SysUser sysUser)
    {
        Map<String, SysUser> userMap = new HashMap<>();
        List<SysUser> sysUsers = userMapper.selectUserList(sysUser);
        if(!CollectionUtils.isEmpty(sysUsers)){
            userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserName, Function.identity()));
        }
        return userMap;
    }

    /**
     * 通过用户ID查询用户
     *
     * @return 用户对象信息
     */
    @Override
    public Map<Long, SysUser> selectUserMapByUserId(SysUser sysUser)
    {
        Map<Long, SysUser> userMap = new HashMap<>();
        List<SysUser> sysUsers = userMapper.selectUserList(sysUser);
        if(!CollectionUtils.isEmpty(sysUsers)){
            userMap = sysUsers.stream().collect(Collectors.toMap(SysUser::getUserId, Function.identity()));
        }
        return userMap;
    }

    @Override
    public List<BaUserVO> countUser() {
        //查询业务管理中心、运营管理中心、子公司
        List<SysDept> sysDepts = sysDeptMapper.selectBusinessCompanyList();
        List<BaUserVO> baUserVOS = new ArrayList<>();
        SysUser sysUser = null;
        BaUserVO baUserVO = null;
        if(!CollectionUtils.isEmpty(sysDepts)){
            for(SysDept sysDept : sysDepts){
                sysUser = new SysUser();
                sysUser.setDeptId(sysDept.getDeptId());
                baUserVO = new BaUserVO();
                baUserVO.setDepName(sysDept.getDeptName());
                baUserVO.setNum(userMapper.countUser(sysUser));
                baUserVOS.add(baUserVO);
            }
        }
        return baUserVOS;
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserId(Long userId) {
        SysUser sysUser = userMapper.selectUserById(userId);
        if(!ObjectUtils.isEmpty(sysUser)){
            Long deptId = sysUser.getDeptId();
            if(!ObjectUtils.isEmpty(deptId)){
                SysDept sysDept = sysDeptMapper.selectDeptById(deptId);
                if(!ObjectUtils.isEmpty(sysDept)){
                    sysUser.setLeader(sysDept.getLeader()); //部门负责人
                    sysUser.setResponsibilities(sysDept.getResponsibilities()); //部门负责人
                }
            }
        }
        return sysUser;
    }

    @Override
    public SysDept selectUserDeptByUserId(Long userId) {
        List<SysDept> sysDepts = sysDeptMapper.selectUserDeptByUserId(userId);
        SysDept sysDept = new SysDept();
        Long parentId = new Long(0);
        if(!CollectionUtils.isEmpty(sysDepts)){
            sysDept = sysDepts.get(0);
            if(!ObjectUtils.isEmpty(sysDept)){
                parentId = sysDept.getParentId();
                if(!ObjectUtils.isEmpty(parentId)){
                    sysDept = getCompanyDeptByParentId(parentId);
                }
            }
        }
        return sysDept;
    }

    /**
     * 根据部门ID集合查询用户
     * @param deptIds
     * @return
     */
    @Override
    public List<SysUser> selectUsersByDeptIds(Long[] deptIds) {
        return userMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
    }

    @Override
    public List<SysUser> operateUser(String comId) {
        return userMapper.operateUser(comId);
    }

    @Override
    public int clockSummaryAdd(SysUser user) {
        //同步考勤数据
        //日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        String month = format.format(new Date());
        if(user.getDeptId() != null){
            SysDept sysDept = sysDeptMapper.selectDeptById(user.getDeptId());
            if(!"0".equals(sysDept.getAttendanceLock())){
                //月度汇总
                OaClockSummary oaClockSummary = new OaClockSummary();
                oaClockSummary.setUserId(String.valueOf(user.getUserId()));
                //查看部门信息
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                if(StringUtils.isNotNull(dept)){
                    oaClockSummary.setDeptId(String.valueOf(dept.getDeptId()));
                    if(!"0".equals(dept.getAttendanceLock())){
                        OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                        oaClockSummary.setAttendanceId(attendanceGroup.getId());
                        oaClockSummary.setAttendanceName(attendanceGroup.getAttendanceName());
                    }
                }
                oaClockSummary.setId(getRedisIncreID.getId());
                oaClockSummary.setSummaryDate(month);
                //租户ID
                oaClockSummary.setTenantId(user.getComId());
                oaClockSummaryMapper.insertOaClockSummary(oaClockSummary);
            }
        }else {
            return 0;
        }
        return 1;
    }

    private SysDept getCompanyDeptByParentId(Long parentId){
        SysDept sysDept = new SysDept();
        sysDept.setDeptId(parentId);
        List<SysDept> sysDepts = sysDeptMapper.deptList(sysDept);
        boolean isExistCompany = false;
        if(!CollectionUtils.isEmpty(sysDepts)){
            for(SysDept dept : sysDepts){
                sysDept = dept;
                if("1".equals(dept.getDeptType())){ //公司
                    isExistCompany = true;
                    break;
                }
            }
            if(!isExistCompany){
                sysDept = getCompanyDeptByParentId(sysDept.getParentId());
            }
        }
        return sysDept;
    }

}
