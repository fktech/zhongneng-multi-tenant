package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.business.common.constant.BusinessConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.BaProcessDefinitionDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaUserSortVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaProcessDefinitionRelatedMapper;
import com.business.system.mapper.BaProcessInstanceRelatedMapper;
import com.business.system.mapper.SysRoleMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.IBaProcessInstanceService;
import com.business.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 流程实例接口
 * @date: 2022/12/13 20:37
 */
@Service
@Slf4j
public class BaProcessInstanceServiceImpl implements IBaProcessInstanceService {

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Override
    public List<ApproveLinkHistoryTaskVO> listInstanceApproveLink(ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO) {
        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
        List<ApproveLinkHistoryTaskVO> approveLinkHistoryTaskVOList = new ArrayList<>();
        long startUserId = new Long(0);
        String depId = "";
        LinkedHashMap<String, ApproveLinkHistoryTaskVO> taskVOMap = new LinkedHashMap<>();
        if(!ObjectUtils.isEmpty(ajaxResult)){
//            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
            //转数组对象
            List<ApproveLinkHistoryTaskVO> taskVOList = JSON.parseArray(JSON.toJSONString(ajaxResult.get("data")), ApproveLinkHistoryTaskVO.class);
            if(!CollectionUtils.isEmpty(taskVOList)){
                //获取发起人ID
                for(ApproveLinkHistoryTaskVO item : taskVOList) {
                    if(!"100000".equals(String.valueOf(item.getAssignee()))) {
                        if ("提交审批".equals(item.getName())) {
                            startUserId = Long.valueOf(String.valueOf(item.getAssignee()));
                            SysUser sysUser = sysUserMapper.selectUserById(startUserId);
                            if (!ObjectUtils.isEmpty(sysUser)) {
                                depId = String.valueOf(sysUser.getDeptId());
                            }
                        }
                        if ("true".equals(item.getCountersign())) {
//                            SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(String.valueOf(item.getAssignee())));
//                            if (depId.equals(String.valueOf(sysUser1.getDeptId()))) {
//                                approveLinkHistoryTaskVOList.add(packageApproveLinkHistoryTask(item));
                                packageApproveLinkHistoryTask(item, taskVOMap);
//                            }
//                            ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = packageApproveLinkHistoryTask(item);

                        } else {
//                            ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = packageApproveLinkHistoryTask(item);
//                            approveLinkHistoryTaskVOList.add(approveLinkHistoryTaskVO);
                            packageApproveLinkHistoryTask(item, taskVOMap);
                        }
                    }
                }
                approveLinkHistoryTaskVOList = new ArrayList<>(taskVOMap.values());
            }
            //计算时间差
            for(int i = 0; i <  approveLinkHistoryTaskVOList.size(); i++){
                if(i > 0){
                    if(StringUtils.hasText(approveLinkHistoryTaskVOList.get(i).getCreateTime())
                            && StringUtils.hasText(approveLinkHistoryTaskVOList.get(i-1).getCreateTime())
                            && !BusinessConstant.PROCESS_NO_APPROVE.equals(approveLinkHistoryTaskVOList.get(i).getBusinessStatus())){
                        Date createDate = DateUtils.dateTime(DateUtils.YYYY_MM_DD_HH_MM_SS, approveLinkHistoryTaskVOList.get(i).getCreateTime());
                        Date endDate = DateUtils.dateTime(DateUtils.YYYY_MM_DD_HH_MM_SS, approveLinkHistoryTaskVOList.get(i-1).getCreateTime());
                        String timeDifference = DateUtils.formatDuration(endDate, createDate);
                        //设置办理用时
                        approveLinkHistoryTaskVOList.get(i).setTimeDifference(timeDifference);
                    }
                }
            }
        }

        return approveLinkHistoryTaskVOList;
    }

    @Override
    public List<ApproveLinkHistoryTaskVO> listDefinitionApproveLink(BaProcessDefinitionDTO baProcessDefinitionDTO) {
        List<ApproveLinkHistoryTaskVO> approveLinkHistoryTaskVOList = new ArrayList<>();
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(baProcessDefinitionDTO.getTaskType(), SecurityUtils.getCurrComId());
        baProcessDefinitionDTO.setProcessDefinitionId(flowId);
        //跟进taskType
        AjaxResult ajaxResult = workFlowFeignClient.listDefinitionApproveLink(baProcessDefinitionDTO);
        LinkedHashMap<String, ApproveLinkHistoryTaskVO> taskVOMap = new LinkedHashMap<>();
        if(!ObjectUtils.isEmpty(ajaxResult)){
            //转数组对象
            List<ApproveLinkHistoryTaskVO> taskVOList = JSON.parseArray(JSON.toJSONString(ajaxResult.get("data")), ApproveLinkHistoryTaskVO.class);
            if(!CollectionUtils.isEmpty(taskVOList)){
                //获取发起人ID
                for(ApproveLinkHistoryTaskVO item : taskVOList) {
                    if ("提交审批".equals(item.getName())) {
                        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(item.getAssignee()));
                    }
                    if ("true".equals(item.getCountersign())) {
                        packageApproveLinkHistoryTask(item, taskVOMap);
                    } else {
                        packageApproveLinkHistoryTask(item, taskVOMap);
                    }
                }
                approveLinkHistoryTaskVOList = new ArrayList<>(taskVOMap.values());
            }
        }
        return approveLinkHistoryTaskVOList;
    }

    @Override
    public ApproveLinkHistoryTaskVO getCurrentNodeApproveLink(ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO) {
        AjaxResult ajaxResult = workFlowFeignClient.getCurrentNodeApproveLink(processInstanceApproveLinkDTO);
        long startUserId = new Long(0);
        String depId = "";
        LinkedHashMap<String, ApproveLinkHistoryTaskVO> taskVOMap = new LinkedHashMap<>();
        ApproveLinkHistoryTaskVO taskVO = null;
        if(!ObjectUtils.isEmpty(ajaxResult)){
            //转数组对象
            taskVO = JSON.parseObject(JSON.toJSONString(ajaxResult.get("data")), ApproveLinkHistoryTaskVO.class);
            if(!ObjectUtils.isEmpty(taskVO)){
                //获取发起人ID
                if(!"100000".equals(String.valueOf(taskVO.getAssignee()))) {
                    if ("提交审批".equals(taskVO.getName())) {
                        startUserId = Long.valueOf(String.valueOf(taskVO.getAssignee()));
                        SysUser sysUser = sysUserMapper.selectUserById(startUserId);
                        if (!ObjectUtils.isEmpty(sysUser)) {
                            depId = String.valueOf(sysUser.getDeptId());
                        }
                    }
                    if ("true".equals(taskVO.getCountersign())) {
//                            SysUser sysUser1 = sysUserMapper.selectUserById(Long.valueOf(String.valueOf(item.getAssignee())));
//                            if (depId.equals(String.valueOf(sysUser1.getDeptId()))) {
//                                approveLinkHistoryTaskVOList.add(packageApproveLinkHistoryTask(item));
                        packageApproveLinkHistoryTask(taskVO, taskVOMap);
//                            }
//                            ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = packageApproveLinkHistoryTask(item);

                    } else {
//                            ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = packageApproveLinkHistoryTask(item);
//                            approveLinkHistoryTaskVOList.add(approveLinkHistoryTaskVO);
                        packageApproveLinkHistoryTask(taskVO, taskVOMap);
                    }
                }
            }
        }

        return taskVO;
    }

    private void packageApproveLinkHistoryTask(ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO, Map<String, ApproveLinkHistoryTaskVO> taskVOMap){
        if (StringUtils.hasText(approveLinkHistoryTaskVO.getAssignee())) {
            if(approveLinkHistoryTaskVO.getAssignee().indexOf(",") != -1){
                String[] assigneeArray = approveLinkHistoryTaskVO.getAssignee().split(",");
                StringBuilder stringBuilder = new StringBuilder();
                List<BaUserSortVO> userList = new ArrayList<>();
                BaUserSortVO baUserSortVO = null;
                StringBuilder userBuilder = new StringBuilder();
                if(assigneeArray.length > 0){
                    for(String assignee : assigneeArray){
                        baUserSortVO = new BaUserSortVO();
                        SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(assignee));
                        if (!ObjectUtils.isEmpty(sysUser)) {
                            baUserSortVO.setUserId(sysUser.getUserId());
                            baUserSortVO.setJobCode(sysUser.getJobCode());
                            baUserSortVO.setNickName(sysUser.getNickName());
                            if(stringBuilder.length() > 0){
                                stringBuilder.append(",").append(sysUser.getNickName());
                            } else {
                                stringBuilder.append(sysUser.getNickName());
                            }
                            approveLinkHistoryTaskVO.setAssigneeName(stringBuilder.toString());
                            userList.add(baUserSortVO);
                        }
                    }
                    userList = userList.stream().sorted(Comparator.comparing(BaUserSortVO::getJobCode, Comparator.nullsLast(String::compareTo))).collect(Collectors.toList());
                    for(BaUserSortVO baUserSortVO1 : userList){
                        if(userBuilder.length() > 0){
                            userBuilder.append(",").append(baUserSortVO1.getNickName());
                        } else {
                            userBuilder.append(baUserSortVO1.getNickName());
                        }
                    }
                    approveLinkHistoryTaskVO.setUserAssigneeSort(String.valueOf(userBuilder));
                }
                taskVOMap.put(approveLinkHistoryTaskVO.getName(), approveLinkHistoryTaskVO);
            } else if(!ObjectUtils.isEmpty(taskVOMap.get(approveLinkHistoryTaskVO.getName()))){
                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVOBefore = taskVOMap.get(approveLinkHistoryTaskVO.getName());
                SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(approveLinkHistoryTaskVO.getAssignee()));
                if (!ObjectUtils.isEmpty(sysUser)) {
                    approveLinkHistoryTaskVO.setAssigneeName(sysUser.getNickName());
                }
                approveLinkHistoryTaskVOBefore.setAssigneeName(approveLinkHistoryTaskVOBefore.getAssigneeName() + "," + approveLinkHistoryTaskVO.getAssigneeName());
                taskVOMap.put(approveLinkHistoryTaskVOBefore.getName(), approveLinkHistoryTaskVOBefore);
            } else {
                SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(approveLinkHistoryTaskVO.getAssignee()));
                if (!ObjectUtils.isEmpty(sysUser)) {
                    approveLinkHistoryTaskVO.setAssigneeName(sysUser.getNickName());
                    taskVOMap.put(approveLinkHistoryTaskVO.getName(), approveLinkHistoryTaskVO);
                }
            }
        } else if(ObjectUtils.isEmpty(taskVOMap.get(approveLinkHistoryTaskVO.getName()))){
            taskVOMap.put(approveLinkHistoryTaskVO.getName(), approveLinkHistoryTaskVO);
        }
        if(StringUtils.hasText(approveLinkHistoryTaskVO.getReason())){
            String reason = approveLinkHistoryTaskVO.getReason();
            if(StringUtils.hasText(reason)){
                String[] reasonArray = reason.split(",");
                StringBuilder stringBuilder = new StringBuilder();
                if(reasonArray.length > 0){
                    if(stringBuilder.length() > 0){
                        approveLinkHistoryTaskVO.setReason(stringBuilder.toString());
                    }
                }
            }
        }

        if(StringUtils.hasText(approveLinkHistoryTaskVO.getReasonUser())){
            String[] reasonUserArray = approveLinkHistoryTaskVO.getReasonUser().split(",");
            StringBuilder reasonStringBuilder = new StringBuilder();
            if(reasonUserArray.length > 0){
                for(String assignee : reasonUserArray){
                    SysUser sysUser = iSysUserService.selectUserById(Long.parseLong(assignee));
                    if (!ObjectUtils.isEmpty(sysUser)) {
                        if(reasonStringBuilder.length() > 0){
                            reasonStringBuilder.append(",").append(sysUser.getNickName());
                        } else {
                            reasonStringBuilder.append(sysUser.getNickName());
                        }
                    }
                }
                approveLinkHistoryTaskVO.setReasonUser(String.valueOf(reasonStringBuilder));
            }
            taskVOMap.put(approveLinkHistoryTaskVO.getName(), approveLinkHistoryTaskVO);
        }
    }
}
