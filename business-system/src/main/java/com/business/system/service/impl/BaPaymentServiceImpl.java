package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaPaymentStatVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 付款信息Service业务层处理
 *
 * @author single
 * @date 2022-12-26
 */
@Service
public class BaPaymentServiceImpl extends ServiceImpl<BaPaymentMapper, BaPayment> implements IBaPaymentService
{
    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaProjectMapper projectMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaCheckMapper baCheckMapper;

    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    @Autowired
    private IBaPaymentVoucherService baPaymentVoucherService;

    @Autowired
    private BaPaymentVoucherMapper baPaymentVoucherMapper;

    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    private static final Logger log = LoggerFactory.getLogger(BaPaymentServiceImpl.class);

    /**
     * 查询付款信息
     *
     * @param id 付款信息ID
     * @return 付款信息
     */
    @Override
    public BaPayment selectBaPaymentById(String id)
    {
        BaPayment baPayment = baPaymentMapper.selectBaPaymentById(id);

        //运输信息
        List<BaTransport> baTransports = new ArrayList<>();
        if(StringUtils.isNotEmpty(baPayment.getTransportId())){
            String[] transportId = baPayment.getTransportId().split(",");
            for (String ids:transportId) {
                //查询运输信息
                BaTransport baTransport = baTransportMapper.selectBaTransportById(ids);
                if(StringUtils.isNotNull(baTransport)){
                    //查询发货信息
                    QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
                    checkQueryWrapper.eq("relation_id",baTransport.getId());
                    checkQueryWrapper.eq("type",1);
                    BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
                    if(StringUtils.isNotNull(baCheck)){
                        baTransport.setDelivery(baCheck);
                    }
                    baTransports.add(baTransport);
                }
            }
        }
        baPayment.setBaTransport(baTransports);
        //查询结算单信息
//        if(StringUtils.isNotEmpty(baPayment.getSettlementId())){
//            baPayment.setBaSettlement(baSettlementMapper.selectBaSettlementById(baPayment.getSettlementId()));
//        }
        //转company
        if(StringUtils.isNotEmpty(baPayment.getCompany())){
            //终端企业
            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baPayment.getCompany());
            if(StringUtils.isNotNull(enterprise)){
                baPayment.setCompanyName(enterprise.getName());
                //税号查询
                QueryWrapper<BaBillingInformation> informationQueryWrapper = new QueryWrapper<>();
                informationQueryWrapper.eq("relevance_id",enterprise.getId());
                BaBillingInformation baBillingInformation = baBillingInformationMapper.selectOne(informationQueryWrapper);
                baPayment.setDutyParagraph(baBillingInformation.getDutyParagraph());
            }
            //供应商
            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPayment.getCompany());
            if(StringUtils.isNotNull(baSupplier)){
                baPayment.setCompanyName(baSupplier.getName());
                baPayment.setDutyParagraph(baSupplier.getTaxNum());
            }
            //公司
            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baPayment.getCompany());
            if(StringUtils.isNotNull(baCompany)){
                baPayment.setCompanyName(baCompany.getName());
                baPayment.setDutyParagraph(baCompany.getTaxNum());
            }
        }
        //合同启动信息
        BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baPayment.getStartId());
        baPayment.setBaContractStart(baContractStart);

        //账户信息
//        QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("relation_id",baPayment.getContractId());
//        queryWrapper.eq("flag",0);
//        List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
//        baPayment.setBankList(baBanks);

        BaBank baBank = baBankMapper.selectBaBankById(baPayment.getBankId());
        baPayment.setBaBank(baBank);

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("business_id",baPayment.getId());
        queryWrapper1.eq("flag",0);
        //queryWrapper.eq("approve_result",AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode());
        queryWrapper1.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper1);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baPayment.setProcessInstanceId(processInstanceIds);

        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baPayment.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位名称
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baPayment.setUser(sysUser);
        //创建人对应公司
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptMapper.selectDeptById(sysUser.getDeptId());
            //查询祖籍
            String[] split = dept.getAncestors().split(",");
            for (String depId:split) {
                SysDept dept1 = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                if(StringUtils.isNotNull(dept1)){
                    if(dept1.getDeptType().equals("1")){
                        baPayment.setComName(dept1.getDeptName());
                        break;
                    }
                }
            }
        }
        //货转凭证
        QueryWrapper<BaPaymentVoucher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("payment_id",id);
        List<BaPaymentVoucher> baPaymentVouchers = baPaymentVoucherMapper.selectList(queryWrapper);
        for (BaPaymentVoucher voucher:baPaymentVouchers) {
            if(StringUtils.isNotEmpty(voucher.getPaymentCompany())){
                //付款单位
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(voucher.getPaymentCompany()));
                voucher.setPaymentCompany(dept.getDeptName());
            }
        }
        baPayment.setBaPaymentVouchers(baPaymentVouchers);
        //采购单名称
        if(StringUtils.isNotEmpty(baPayment.getPlanId())){
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPayment.getPlanId());
            baPayment.setPlanName(baProcurementPlan.getPlanName());
        }
        //仓储立项名称
        if(StringUtils.isNotEmpty(baPayment.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baPayment.getProjectId());
            baPayment.setProjectName(baProject.getName());
        }
        return baPayment;
    }

    /**
     * 查询付款信息列表
     *
     * @param baPayment 付款信息
     * @return 付款信息
     */
    @Override
    public List<BaPayment> selectBaPaymentList(BaPayment baPayment)
    {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        List<BaPayment> baPayments = baPaymentMapper.selectBaPaymentListByCondition(baPayment);
        baPayments.stream().forEach(item -> {
//            if("1".equals(item.getType()) || "2".equals(item.getType()) || "3".equals(item.getType())){
//                item.setApplyPaymentAmount(item.getActualPaymentAmount());
//            }
            //转开户号
            BaBank baBank = baBankMapper.selectBaBankById(item.getBankId());
            if(StringUtils.isNotNull(baBank)){
                item.setOtherAccount(baBank.getAccount());
                item.setOtherBank(baBank.getBankName());
                item.setBaBank(baBank);
            }
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                }

            }
            //确认收款金额
            BigDecimal paymentAmount = new BigDecimal(0);
            //货转凭证
            QueryWrapper<BaPaymentVoucher> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("payment_id",item.getId());
            queryWrapper.orderByAsc("payment_date");
            List<BaPaymentVoucher> baPaymentVouchers = baPaymentVoucherMapper.selectList(queryWrapper);
            for (BaPaymentVoucher baPaymentVoucher:baPaymentVouchers) {
                if(baPaymentVoucher.getPaymentAmount() != null){
                    paymentAmount = paymentAmount.add(baPaymentVoucher.getPaymentAmount());
                }
                //凭证付款单位
                if(StringUtils.isNotEmpty(baPaymentVoucher.getPaymentCompany())){
                    //付款单位
                    SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(baPaymentVoucher.getPaymentCompany()));
                    baPaymentVoucher.setPaymentCompany(dept.getDeptName());
                }
            }
            //导出付款日期
            if(null != baPaymentVouchers && baPaymentVouchers.size() > 0){
                if(baPaymentVouchers.size() == 1){
                    item.setDateOfPayment(df.format(baPaymentVouchers.get(0).getPaymentDate()));
                }else {
                    item.setDateOfPayment(df.format(baPaymentVouchers.get(0).getPaymentDate())+"-"+df.format(baPaymentVouchers.get(baPaymentVouchers.size() - 1).getPaymentDate()));
                }
            }
            item.setPaymentAmount(paymentAmount);
            //付款凭证
            item.setBaPaymentVouchers(baPaymentVouchers);
            //待付款金额
            item.setPendingPayment(item.getApplyPaymentAmount().subtract(paymentAmount));
            //转company
            if(StringUtils.isNotEmpty(item.getCompany())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getCompany());
                if(StringUtils.isNotNull(enterprise)){
                    item.setCompany(enterprise.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getCompany());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setCompany(baSupplier.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getCompany());
                if(StringUtils.isNotNull(baCompany)){
                    item.setCompany(baCompany.getName());
                }
                //合同启动信息
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
                if(StringUtils.isNotNull(baContractStart)){
                    item.setStartName(baContractStart.getName());
                }
                item.setBaContractStart(baContractStart);
            }
            //采购单名称
            if(StringUtils.isNotEmpty(item.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(item.getPlanId());
                item.setPlanName(baProcurementPlan.getPlanName());
            }
            //仓储立项名称
            if(StringUtils.isNotEmpty(item.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(item.getProjectId());
                item.setProjectName(baProject.getName());
            }
            //付款日期
            item.setPaymentDate(item.getCreateTime());

        });
        return baPayments;
    }

    /**
     * 新增付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    @Override
    public int insertBaPayment(BaPayment baPayment)
    {
        baPayment.setId(getRedisIncreID.getId());
        baPayment.setCreateTime(DateUtils.getNowDate());
        baPayment.setCreateBy(SecurityUtils.getUsername());
        baPayment.setUpdateBy(SecurityUtils.getUsername());
        baPayment.setUpdateTime(DateUtils.getNowDate());
        baPayment.setUserId(SecurityUtils.getUserId());
        baPayment.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        //合同启动全局编码
        String startGlobalNumber = baPayment.getStartGlobalNumber();
        //判断是否为上海公司
        String judge = "";
        //查看合同启动
        if(StringUtils.isNotEmpty(baPayment.getStartId())){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baPayment.getStartId());
            judge = baContractStart.getBelongCompanyId();
        }
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baPayment.getWorkState())){
            //流程发起状态
            baPayment.setWorkState("1");
        }
        //银行信息
//        if(StringUtils.isNotEmpty(baPayment.getBankId())){
//           BaBank baBank = baBankMapper.selectBaBankById(baPayment.getBankId());
//           baPayment.setOtherBank(baBank.getBankName());
//           if(StringUtils.isNotEmpty(baPayment.getCompany())){
//               if(baPayment.getCompany().equals(baBank.getCompanyName()) == false){
//                   baPayment.setCompany(baPayment.getCompany());
//               }else {
//                   baPayment.setCompany(baBank.getCompanyName());
//               }
//           }else {
//               baPayment.setCompany(baBank.getCompanyName());
//           }
//            BaBank baBank1 = baBankMapper.selectBaBankById(baBank.getAccount());
//           if(StringUtils.isNotNull(baBank1)){
//               baPayment.setOtherAccount(baBank1.getAccount());
//           }
//            baPayment.setOtherAccount(baBank.getAccount());
//           baPayment.setOtherAccountName(baBank.getBankName());
//        }
        //company转名称
//        if(StringUtils.isNotEmpty(baPayment.getCompany())){
//            //终端企业
//            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baPayment.getCompany());
//            if(StringUtils.isNotNull(enterprise)){
//                baPayment.setCompany(enterprise.getName());
//            }
//            //供应商
//            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPayment.getCompany());
//            if(StringUtils.isNotNull(baSupplier)){
//                baPayment.setCompany(baSupplier.getName());
//            }
//            //公司
//            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baPayment.getCompany());
//            if(StringUtils.isNotNull(baCompany)){
//                baPayment.setCompany(baCompany.getName());
//            }
//            if(StringUtils.isNull(enterprise) && StringUtils.isNull(baSupplier) && StringUtils.isNull(baCompany)){
//                baPayment.setCompany(baPayment.getCompany());
//            }
//        }
        //订单付款信息
//        if(StringUtils.isNotNull(baPayment.getBaOrderDTOs())) {
//            BaSettlementDetail baSettlementDetail = new BaSettlementDetail();
//            for (BaOrderDTO baOrder : baPayment.getBaOrderDTOs()) {
//                //订单添加历史数据
//                baSettlementDetail.setId(getRedisIncreID.getId());
//                baSettlementDetail.setRelationId(baPayment.getId());
//                baSettlementDetail.setOrderId(baOrder.getId());
//                baSettlementDetail.setAmount(baOrder.getActualPayment());
//                baSettlementDetail.setSettlementId(baOrder.getSettlementId());
//                baSettlementDetail.setCreateTime(DateUtils.getNowDate());
//                baSettlementDetail.setType("2");
//                baSettlementDetailMapper.insertBaSettlementDetail(baSettlementDetail);
//            }
//        }
        if("2".equals(baPayment.getWorkState())){
            //流程实例ID
            String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode(),SecurityUtils.getCurrComId());
            baPayment.setFlowId(flowId);
        }
        Integer result = baPaymentMapper.insertBaPayment(baPayment);
        if(result > 0){
            baPayment = baPaymentMapper.selectBaPaymentById(baPayment.getId());
            baPayment.setStartGlobalNumber(startGlobalNumber);
            if("2".equals(baPayment.getWorkState())){
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = null;
                //启动流程实例
                baProcessInstancesRuntimeVO = doRuntimeProcessInstances(baPayment, AdminCodeEnum.PAYMENT_APPROVAL_CONTENT_INSERT.getCode());
                if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                    baPaymentMapper.deleteBaPaymentById(baPayment.getId());
                    return 0;
                }else {
                    BaPayment baPayment2 = baPaymentMapper.selectBaPaymentById(baPayment.getId());

                    baPayment2.setState(AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode());

                    //baPayment2.setState(AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode());
                    baPayment2.setPaymentState(AdminCodeEnum.PAYMENT_TO_BE.getCode());
                    if(StringUtils.isNotEmpty(baPayment2.getTransportId())){
                        String[] split = baPayment2.getTransportId().split(",");
                        for (String ids:split) {
                            BaTransport baTransport = baTransportMapper.selectBaTransportById(ids);
                            if(StringUtils.isNotNull(baTransport)){
                                baTransport.setPitchOn("1");
                                baTransportMapper.updateBaTransport(baTransport);
                            }
                        }
                    }
                    //合同启动全局编号
                    if(StringUtils.isNotEmpty(baPayment.getStartGlobalNumber())){
                        //全局编号
                        QueryWrapper<BaPayment> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
                        queryWrapper.eq("flag",0);
                        queryWrapper.isNotNull("serial_number");
                        queryWrapper.orderByDesc("serial_number");
                        List<BaPayment> invoiceList = baPaymentMapper.selectList(queryWrapper);
                        if(invoiceList.size() > 0){
                            Integer serialNumber = invoiceList.get(0).getSerialNumber();
                            baPayment2.setSerialNumber(serialNumber+1);
                            if(serialNumber < 10){
                                baPayment2.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"000"+baPayment2.getSerialNumber());
                            }else if(serialNumber >= 10){
                                baPayment2.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"00"+baPayment2.getSerialNumber());
                            }else if(serialNumber >= 100){
                                baPayment2.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"0"+baPayment2.getSerialNumber());
                            }else if(serialNumber >= 1000){
                                baPayment2.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+baPayment2.getSerialNumber());
                            }
                        }else {
                            baPayment2.setSerialNumber(1);
                            baPayment2.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"000"+baPayment2.getSerialNumber());
                        }
                    }
                    baPaymentMapper.updateBaPayment(baPayment2);
                }
            }
           else {
                BaPayment baPayment2 = baPaymentMapper.selectBaPaymentById(baPayment.getId());
                //默认审批通过(不发起审批流程)
                if(!"2".equals(baPayment.getWorkState())){
                    baPayment2.setState(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode());
                }else {
                    baPayment2.setState(AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode());
                }
                //baPayment2.setState(AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode());
                baPayment2.setPaymentState(AdminCodeEnum.PAYMENT_TO_BE.getCode());
                if(StringUtils.isNotEmpty(baPayment2.getTransportId())){
                    String[] split = baPayment2.getTransportId().split(",");
                    for (String ids:split) {
                        BaTransport baTransport = baTransportMapper.selectBaTransportById(ids);
                        if(StringUtils.isNotNull(baTransport)){
                            baTransport.setPitchOn("1");
                            baTransportMapper.updateBaTransport(baTransport);
                        }
                    }
                }
                baPaymentMapper.updateBaPayment(baPayment2);
                if(!"2".equals(baPayment.getWorkState())){
                    //审批通过修改运输信息（不发起审批流程）
                    if(!ObjectUtils.isEmpty(baPayment2)){
                        String transportId = baPayment2.getTransportId();
                        if(StringUtils.isNotEmpty(transportId)){
                            String[] transportIdArray = transportId.split(",");
                            if(transportIdArray.length > 0){
                                for(String transport : transportIdArray){
                                    BaTransport baTransport = baTransportMapper.selectBaTransportById(transport);
                                    if(!ObjectUtils.isEmpty(baTransport)){
                                        baTransport.setPaymentStatus("1"); //已付款
                                        baTransportMapper.updateBaTransport(baTransport);
                                    }
                                }
                            }
                        }
                    }
                }

//                if(StringUtils.isNotEmpty(baPayment2.getSettlementId())){
//                    BaSettlement baSettlement = baSettlementMapper.selectBaSettlementById(baPayment2.getSettlementId());
//                    baSettlement.setSettlementState("2");
//                    baSettlementMapper.updateBaSettlement(baSettlement);
//                }
            }
       }
        return 1;
    }

    /**
     * 付款提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaPayment payment, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(payment.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode());
        //查询付款立项ID
        if(!ObjectUtils.isEmpty(payment)){
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(payment.getStartId());
            if(!ObjectUtils.isEmpty(baContractStart)){
                map.put("projectId", baContractStart.getProjectId());
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(payment.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode());
        //获取流程实例关联的业务对象
        BaPayment baPayment = baPaymentMapper.selectBaPaymentById(payment.getId());
        SysUser sysUser = iSysUserService.selectUserById(baPayment.getUserId());
        baPayment.setUserName(sysUser.getUserName());
        baPayment.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baPayment));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO, payment);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime, BaPayment payment) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.PAYMENT_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    @Override
    public int updateBaPayment(BaPayment baPayment)
    {
        //原数据
        BaPayment baPayment1 = baPaymentMapper.selectBaPaymentById(baPayment.getId());
        baPayment.setUpdateTime(DateUtils.getNowDate());
        baPayment.setUpdateBy(SecurityUtils.getUsername());
        baPayment.setCreateTime(baPayment.getUpdateTime());
        if(StringUtils.isNotEmpty(baPayment.getBankId())){
            BaBank baBank = baBankMapper.selectBaBankById(baPayment.getBankId());
            baPayment.setOtherBank(baBank.getBankName());
            baPayment.setCompany(baBank.getRelationId());
            baPayment.setOtherAccount(baBank.getAccount());
            baPayment.setOtherAccountName(baBank.getBankName());
        }
        if(StringUtils.isNotEmpty(baPayment.getPaymentType())){
          if(baPayment.getType().equals("2") == false){
             baPayment.setTransportId("");
          }
        }
        //company转名称
//        if(StringUtils.isNotEmpty(baPayment.getCompany())){
//            //终端企业
//            BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(baPayment.getCompany());
//            if(StringUtils.isNotNull(enterprise)){
//                baPayment.setCompany(enterprise.getName());
//            }
//            //供应商
//            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPayment.getCompany());
//            if(StringUtils.isNotNull(baSupplier)){
//                baPayment.setCompany(baSupplier.getName());
//            }
//            //公司
//            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baPayment.getCompany());
//            if(StringUtils.isNotNull(baCompany)){
//                baPayment.setCompany(baCompany.getName());
//            }
//            if(StringUtils.isNull(enterprise) && StringUtils.isNull(baSupplier) && StringUtils.isNull(baCompany)){
//                baPayment.setCompany(baPayment.getCompany());
//            }
//        }
        //订单付款信息
//        if(StringUtils.isNotNull(baPayment.getBaOrderDTOs())) {
//            for (BaOrderDTO baOrder : baPayment.getBaOrderDTOs()) {
//                BaSettlementDetail baSettlementDetail = baSettlementDetailMapper.selectBaSettlementDetailById(baOrder.getDetailId());
//                baSettlementDetail.setAmount(baOrder.getActualPayment());
//                baSettlementDetailMapper.updateBaSettlementDetail(baSettlementDetail);
//            }
//        }
        Integer result = baPaymentMapper.updateBaPayment(baPayment);
        if(result > 0 && !baPayment1.getState().equals(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode())){
            BaPayment payment = baPaymentMapper.selectBaPaymentById(baPayment.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",payment.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(payment, AdminCodeEnum.PAYMENT_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baPaymentMapper.updateBaPayment(baPayment1);
                return 0;
            }else {
                payment.setState(AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode());
                //修改运单选中状态
                if(StringUtils.isNotEmpty(payment.getTransportId())){
                    String[] split = payment.getTransportId().split(",");
                    for (String ids:split) {
                        BaTransport baTransport = baTransportMapper.selectBaTransportById(ids);
                        if(StringUtils.isNotNull(baTransport)){
                            baTransport.setPitchOn("1");
                            baTransportMapper.updateBaTransport(baTransport);
                        }
                    }
                }
                baPaymentMapper.updateBaPayment(payment);
            }
        }
        return result;
    }

    @Override
    public int endPayment(String id) {
        BaPayment baPayment = baPaymentMapper.selectBaPaymentById(id);
        baPayment.setPaymentState(AdminCodeEnum.PAYMENT_PAID.getCode());
        return baPaymentMapper.updateBaPayment(baPayment);
    }

    /**
     * 批量删除付款信息
     *
     * @param ids 需要删除的付款信息ID
     * @return 结果
     */
    @Override
    public int deleteBaPaymentByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaPayment baPayment = baPaymentMapper.selectBaPaymentById(id);
                baPayment.setFlag(new Long(1));
                //修改相对运输单选中状态
                if(StringUtils.isNotEmpty(baPayment.getTransportId())){
                    String[] split = baPayment.getTransportId().split(",");
                    for (String transportId:split) {
                        BaTransport baTransport = baTransportMapper.selectBaTransportById(transportId);
                        baTransport.setPitchOn("0");
                        baTransportMapper.updateBaTransport(baTransport);
                    }
                }
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapperRelated = new QueryWrapper<>();
                queryWrapperRelated.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapperRelated);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
                //删除付款订单历史数据
//                QueryWrapper<BaSettlementDetail> queryWrapper = new QueryWrapper<>();
//                queryWrapper.eq("relation_id",id);
//                List<BaSettlementDetail> baSettlementDetailList = baSettlementDetailMapper.selectList(queryWrapper);
//                if(StringUtils.isNotNull(baSettlementDetailList)){
//                    for (BaSettlementDetail baSettlementDetail:baSettlementDetailList) {
//                        baSettlementDetailMapper.deleteBaSettlementDetailById(baSettlementDetail.getId());
//                    }
//                }
                baPaymentMapper.updateBaPayment(baPayment);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除付款信息信息
     *
     * @param id 付款信息ID
     * @return 结果
     */
    @Override
    public int deleteBaPaymentById(String id)
    {
        return baPaymentMapper.deleteBaPaymentById(id);
    }

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询收付款信息
            BaPayment baPayment = baPaymentMapper.selectBaPaymentById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baPayment.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.PAYMENT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baPayment.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //付款流程审批状态修改
                baPayment.setState(AdminCodeEnum.PAYMENT_STATUS_WITHDRAW.getCode());

                //修改相对运输单选中状态
                if(StringUtils.isNotEmpty(baPayment.getTransportId())){
                    String[] split = baPayment.getTransportId().split(",");
                    for (String ids:split) {
                        BaTransport baTransport = baTransportMapper.selectBaTransportById(ids);
                        if(StringUtils.isNotNull(baTransport)){
                            baTransport.setPitchOn("0");
                            baTransportMapper.updateBaTransport(baTransport);
                        }

                    }
                }
                baPaymentMapper.updateBaPayment(baPayment);
                String userId = String.valueOf(baPayment.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baPayment, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaPayment baPayment, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baPayment.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"付款审批提醒",msgContent,baMessage.getType(),baPayment.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public int insertBaPaymentVoucher(BaPaymentVoucherDTO baPaymentVoucherDTO) {

        /*baPayment.setPaymentDate(baPaymentVoucherDTO.getPaymentDate());
        baPayment.setPaymentType(baPaymentVoucherDTO.getPaymentType());
        baPayment.setPaymentCompany(baPaymentVoucherDTO.getPaymentCompany());
//        baPayment.setCompany(baPaymentVoucherDTO.getCollectionCompany());
        baPayment.setPaymentAccount(baPaymentVoucherDTO.getPaymentAccount());
        baPayment.setCollectionAccount(baPaymentVoucherDTO.getCollectionAccount());
        baPayment.setCollectionCompany(baPaymentVoucherDTO.getCollectionCompany());
        baPayment.setPaymentVoucher(baPaymentVoucherDTO.getPaymentVoucher());
        baPayment.setVoucherRemark(baPaymentVoucherDTO.getVoucherRemark());*/

        BaPaymentVoucher baPaymentVoucher = new BaPaymentVoucher();
        BeanUtils.copyBeanProp(baPaymentVoucher, baPaymentVoucherDTO);
        baPaymentVoucher.setId(getRedisIncreID.getId());
        baPaymentVoucher.setPaymentId(baPaymentVoucherDTO.getId());
        baPaymentVoucher.setAnnex(baPaymentVoucherDTO.getPaymentVoucher());
        baPaymentVoucher.setUserId(SecurityUtils.getUserId());
        baPaymentVoucher.setDeptId(SecurityUtils.getDeptId());
        Integer result = baPaymentVoucherService.insertBaPaymentVoucher(baPaymentVoucher);
            if(result > 0) {
                BigDecimal paymentAmount = new BigDecimal(0);
                //货转凭证
                QueryWrapper<BaPaymentVoucher> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("payment_id", baPaymentVoucherDTO.getId());
                List<BaPaymentVoucher> baPaymentVouchers = baPaymentVoucherMapper.selectList(queryWrapper);
                for (BaPaymentVoucher baPaymentVoucher1 : baPaymentVouchers) {
                    if (baPaymentVoucher1.getPaymentAmount() != null) {
                        paymentAmount = paymentAmount.add(baPaymentVoucher1.getPaymentAmount());
                    }
                }
                BaPayment payment = baPaymentMapper.selectBaPaymentById(baPaymentVoucherDTO.getId());
                if (payment.getApplyPaymentAmount().compareTo(paymentAmount) == 0) {
                    //查询收付款信息
                   /* BaPayment baPayment = baPaymentMapper.selectBaPaymentById(baPaymentVoucherDTO.getId());*/
                    payment.setPaymentState(AdminCodeEnum.PAYMENT_PAID.getCode());
                    baPaymentMapper.updateBaPayment(payment);
                    if (StringUtils.isNotEmpty(payment.getContractId())) {
                        //合同信息
                        BaContract baContract = baContractMapper.selectBaContractById(payment.getContractId());
                        if (StringUtils.isNotEmpty(baContract.getProjectId())) {
                            //项目信息
                            BaProject baProject = projectMapper.selectBaProjectById(baContract.getProjectId());
                            baProject.setProjectStage(9);
                            projectMapper.updateBaProject(baProject);
                        }
                    }
                }
                //更新项目阶段
            }
        return result;
    }

    @Override
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaPayment baPayment) {
        List<MonthSumDTO> monthSumDTOS1 = baPaymentMapper.SelectSalesVolumeByEveryoneMonth(baPayment);
        for (MonthSumDTO monthsum2:monthSumDTOS1) {
            if (monthsum2.getCount()==null){
                monthsum2.setCount(new BigDecimal(0.00));
            }

        }
        return monthSumDTOS1;
    }

    @Override
    public BigDecimal SelectSalesVolumeByyear() {
        BaPayment baPayment=new BaPayment();
        baPayment.setPaymentState("payment_paid");
        baPayment.setState("payment:pass");
        baPayment.setFlag(0L);
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        return baPaymentMapper.SelectSalesVolumeByyear(baPayment) ;
    }

    @Override
    public BigDecimal SelectSalesVolumeByyearAndType(String type) {
        BaPayment baPayment=new BaPayment();
        baPayment.setPaymentState("payment_paid");
        baPayment.setState("payment:pass");
        baPayment.setFlag(0L);
        baPayment.setType(type);
        //租户ID
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        return baPaymentMapper.SelectSalesVolumeByyearAndType(baPayment);
    }

    @Override
    public BigDecimal countPaymentList(CountPaymentDTO countPaymentDTO) {
        countPaymentDTO.setTenantId(SecurityUtils.getCurrComId());
        BigDecimal bigDecimal = baPaymentMapper.countPaymentList(countPaymentDTO);
        if(bigDecimal == null){
            return new BigDecimal(0);
        }
        return bigDecimal;
    }

    /**
     * 统计付款
     * @param countPaymentDTO
     * @return
     */
    @Override
    public int countDepotPaymentList(CountPaymentDTO countPaymentDTO) {
        countPaymentDTO.setTenantId(SecurityUtils.getCurrComId());
        return baPaymentMapper.countDepotPaymentList(countPaymentDTO);
    }

    /**
     * 根据付款流程实例ID查询对应项目的业务经理用户ID
     */
    @Override
    public List<String> getUserIdByProcessInstanceId(BusinessUserDTO paymentUserDTO) {
        return baPaymentMapper.getUserIdByProcessInstanceId(paymentUserDTO);
    }

    @Override
    public int association(BaPayment baPayment) {
        //合同启动全局编号
        if(StringUtils.isNotEmpty(baPayment.getStartGlobalNumber())){
            //全局编号
            QueryWrapper<BaPayment> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("tenant_id",SecurityUtils.getCurrComId());
            queryWrapper.eq("flag",0);
            queryWrapper.isNotNull("serial_number");
            queryWrapper.orderByDesc("serial_number");
            List<BaPayment> invoiceList = baPaymentMapper.selectList(queryWrapper);
            if(invoiceList.size() > 0){
                Integer serialNumber = invoiceList.get(0).getSerialNumber();
                baPayment.setSerialNumber(serialNumber+1);
                if(serialNumber < 10){
                    baPayment.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"000"+baPayment.getSerialNumber());
                }else if(serialNumber >= 10){
                    baPayment.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"00"+baPayment.getSerialNumber());
                }else if(serialNumber >= 100){
                    baPayment.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"0"+baPayment.getSerialNumber());
                }else if(serialNumber >= 1000){
                    baPayment.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+baPayment.getSerialNumber());
                }
            }else {
                baPayment.setSerialNumber(1);
                baPayment.setGlobalNumber(baPayment.getStartGlobalNumber()+"-"+"FK"+"-"+"000"+baPayment.getSerialNumber());
            }
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baPayment.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baPayment.getId());
                baGlobalNumber.setCode(baPayment.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("6");
                baGlobalNumber.setStartId(baPayment.getStartId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }
        }
        return baPaymentMapper.updateBaPayment(baPayment);
    }

    /**
     * 可视化大屏统计应付账款
     * @return
     */
    @Override
    public BaPaymentStatVO sumBaPayment() {
        BaPayment baPayment = new BaPayment();
        baPayment.setTenantId(SecurityUtils.getCurrComId());
        //查询付款统计
        BaPaymentStatVO baPaymentStatVO = baPaymentMapper.countBaPayment(baPayment);
        BaPaymentStatVO baPaymentStatVO1 = baPaymentMapper.sumBaPayment(baPayment);
        if(!ObjectUtils.isEmpty(baPaymentStatVO)){
            baPaymentStatVO.setYearActualPaymentAmount(baPaymentStatVO1.getYearActualPaymentAmount());
            baPaymentStatVO.setMonthActualPaymentAmount(baPaymentStatVO1.getMonthActualPaymentAmount());
            return baPaymentStatVO;
        } else {
            return baPaymentStatVO1;
        }
    }

    /**
     * 查询多租户授权信息付款信息列表
     * @param baPayment
     * @return
     */
    @Override
    public List<BaPayment> selectBaPaymentAuthorizedList(BaPayment baPayment) {
        List<BaPayment> baPayments = baPaymentMapper.selectBaPaymentAuthorizedListByCondition(baPayment);
        baPayments.stream().forEach(item -> {
//            if("1".equals(item.getType()) || "2".equals(item.getType()) || "3".equals(item.getType())){
//                item.setApplyPaymentAmount(item.getActualPaymentAmount());
//            }
            //转开户号
            BaBank baBank = baBankMapper.selectBaBankById(item.getBankId());
            if(StringUtils.isNotNull(baBank)){
                item.setOtherAccount(baBank.getAccount());
                item.setOtherBank(baBank.getBankName());
                item.setBaBank(baBank);
            }
            SysUser sysUser = sysUserMapper.selectUserById(item.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                //岗位信息
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
                }else {
                    item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                }

            }
            //确认收款金额
            BigDecimal paymentAmount = new BigDecimal(0);
            //货转凭证
            QueryWrapper<BaPaymentVoucher> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("payment_id",item.getId());
            List<BaPaymentVoucher> baPaymentVouchers = baPaymentVoucherMapper.selectList(queryWrapper);
            for (BaPaymentVoucher baPaymentVoucher:baPaymentVouchers) {
                if(baPaymentVoucher.getPaymentAmount() != null){
                    paymentAmount = paymentAmount.add(baPaymentVoucher.getPaymentAmount());
                }
            }
            item.setPaymentAmount(paymentAmount);
            //转company
            if(StringUtils.isNotEmpty(item.getCompany())){
                //终端企业
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getCompany());
                if(StringUtils.isNotNull(enterprise)){
                    item.setCompany(enterprise.getName());
                }
                //供应商
                BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(item.getCompany());
                if(StringUtils.isNotNull(baSupplier)){
                    item.setCompany(baSupplier.getName());
                }
                //公司
                BaCompany baCompany = baCompanyMapper.selectBaCompanyById(item.getCompany());
                if(StringUtils.isNotNull(baCompany)){
                    item.setCompany(baCompany.getName());
                }
                //合同启动信息
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getStartId());
                item.setBaContractStart(baContractStart);
            }
            //采购单名称
            if(StringUtils.isNotEmpty(item.getPlanId())){
                BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(item.getPlanId());
                item.setPlanName(baProcurementPlan.getPlanName());
            }
            //仓储立项名称
            if(StringUtils.isNotEmpty(item.getProjectId())){
                BaProject baProject = baProjectMapper.selectBaProjectById(item.getProjectId());
                item.setProjectName(baProject.getName());
            }

        });
        return baPayments;
    }

    @Override
    public int authorizedBaPayment(BaPayment baPayment) {
        return baPaymentMapper.updateBaPayment(baPayment);
    }
}
