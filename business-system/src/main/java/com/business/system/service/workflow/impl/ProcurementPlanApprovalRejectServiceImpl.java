package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDeclare;
import com.business.system.domain.BaProcurementPlan;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IDeclareApprovalService;
import com.business.system.service.workflow.IProcurementPlanApprovalService;
import org.springframework.stereotype.Service;


/**
 * 采购计划审批结果:审批拒绝
 */
@Service("procurementPlanApprovalContent:processApproveResult:reject")
public class ProcurementPlanApprovalRejectServiceImpl extends IProcurementPlanApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.PROCUREMENTPLAN_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaProcurementPlan checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
