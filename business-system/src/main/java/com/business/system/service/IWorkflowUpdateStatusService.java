package com.business.system.service;


import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;

/**
 * 接收审批结果，更新目标业务表状态的接口
 * 实现类中@Service(value)
 * value规则:WorkflowUpdateStatusDTO.targetType.split(":")[0]:WorkflowUpdateStatusDTO.approvalType
 * targetType:表示审核内容
 * approvalType表示流程审批结果
 */
public interface IWorkflowUpdateStatusService {

    /**
     * 接收审批结果:更新目标业务表的状态
     *
     * @param updateStatusDTO 审批结果对象
     */
    Object acceptApprovalResult(AcceptApprovalWorkflowDTO updateStatusDTO);
}
