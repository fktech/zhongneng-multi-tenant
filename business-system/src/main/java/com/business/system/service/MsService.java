package com.business.system.service;

import java.util.Map;

/**
 * please change here
 *
 * @author jiahe
 * @date 2020.7
 */
public interface MsService {
    //发送短信
    boolean send(String phone, Map<String, Object> param, String templateCode);

    //验证短信验证码
    Boolean checkCode(String phone, String code);

    //验证邮箱验证码
//    Boolean checkEmailCode(String email, String code);

    //发送非验证码短信
//    boolean sendNoCode(String phone, String templateCode);

//    //验证短信验证码并修改手机号
//    Boolean checkCodeUpdatePhone(String phone, String code, HttpServletRequest request);

    //发送律师入驻通知短信
//    boolean sendLawyerComeInMS(String phone);
}
