package com.business.system.service;

import com.business.system.domain.BaHiProject;

import java.util.List;

/**
 * 立项管理历史Service接口
 *
 * @author ljb
 * @date 2022-12-02
 */
public interface IBaHiProjectService
{
    /**
     * 查询立项管理
     *
     * @param id 立项管理ID
     * @return 立项管理
     */
    public BaHiProject selectBaHiProjectById(String id);

    /**
     * 查询立项管理列表
     *
     * @param baHiProject 立项管理
     * @return 立项管理集合
     */
    public List<BaHiProject> selectBaHiProjectList(BaHiProject baHiProject);

    /**
     * 查询立项管理变更记录列表
     */
    public List<BaHiProject> selectChangeDataBaHiProjectList(BaHiProject baHiProject);

}
