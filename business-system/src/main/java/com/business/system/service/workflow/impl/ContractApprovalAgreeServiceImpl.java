package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaBid;
import com.business.system.domain.BaContract;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.mapper.BaBidMapper;
import com.business.system.mapper.BaContractMapper;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 合同申请审批结果:审批通过
 */
@Service("contractApprovalContent:processApproveResult:pass")
@Slf4j
public class ContractApprovalAgreeServiceImpl extends IContractApprovalService implements IWorkflowUpdateStatusService {
    @Autowired
    private BaContractMapper baContractMapper;

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        return result;
    }
}
