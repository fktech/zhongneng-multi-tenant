package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaComment;

/**
 * 评论Service接口
 *
 * @author ljb
 * @date 2023-06-08
 */
public interface IBaCommentService
{
    /**
     * 查询评论
     *
     * @param id 评论ID
     * @return 评论
     */
    public BaComment selectBaCommentById(String id);

    /**
     * 查询评论列表
     *
     * @param baComment 评论
     * @return 评论集合
     */
    public List<BaComment> selectBaCommentList(BaComment baComment);

    /**
     * 新增评论
     *
     * @param baComment 评论
     * @return 结果
     */
    public int insertBaComment(BaComment baComment);

    /**
     * 修改评论
     *
     * @param baComment 评论
     * @return 结果
     */
    public int updateBaComment(BaComment baComment);

    /**
     * 批量删除评论
     *
     * @param ids 需要删除的评论ID
     * @return 结果
     */
    public int deleteBaCommentByIds(String[] ids);

    /**
     * 删除评论信息
     *
     * @param id 评论ID
     * @return 结果
     */
    public int deleteBaCommentById(String id);


}
