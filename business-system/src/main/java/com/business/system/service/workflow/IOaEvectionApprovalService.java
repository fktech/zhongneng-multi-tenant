package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 出差申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaEvectionApprovalService {

    @Autowired
    protected OaEvectionMapper oaEvectionMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Resource
    private RedisCache redisCache;

    @Autowired
    private OaEvectionTripMapper oaEvectionTripMapper;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaEvection oaEvection = new OaEvection();
        oaEvection.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaEvection = oaEvectionMapper.selectOaEvectionById(oaEvection.getId());
        oaEvection.setState(status);
        String userId = String.valueOf(oaEvection.getUserId());
        OaClock clock = new OaClock();
        clock.setEmployeeId(oaEvection.getUserId());
        if(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode().equals(status)){
            //查询所有行程
            QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("evection_id",oaEvection.getId());
            List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
            if(!CollectionUtils.isEmpty(oaEvectionTrips)){
                for (OaEvectionTrip oaEvectionTrip : oaEvectionTrips) {
                    //查看每次行程每一天
                    String[] split = oaEvectionTrip.getEveryDay().split(",");
                    //数组转list
                    List<String> list1 = Arrays.asList(split);
                    if(list1.size() == 1){
                        //判断时间为今天
                        //出差时间为一天
                        //查询打卡信息
                        clock.setQueryTime(list1.get(0));
                        //clock.setFlag(1L);
                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                        for (OaClock oaClock:oaClocks) {
                            if(oaEvectionTrip.getStartCycle().equals("1")){
                                //修改上班打卡数据
                                if(oaClock.getClockType().equals("1")){
                                    //打卡状态为出差
                                    oaClock.setClockState("9");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }else if(oaEvectionTrip.getStartCycle().equals("2")){
                                //修改下班打卡数据
                                if(oaClock.getClockType().equals("2")){
                                    //打卡状态为出差
                                    oaClock.setClockState("9");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                            if(oaEvectionTrip.getEndCycle().equals("1")){
                                //修改上班打卡数据
                                if(oaClock.getClockType().equals("1")){
                                    //打卡状态为出差
                                    oaClock.setClockState("9");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }else if(oaEvectionTrip.getEndCycle().equals("2")){
                                //修改上班打卡数据
                                if(oaClock.getClockType().equals("2")){
                                    //打卡状态为出差
                                    oaClock.setClockState("9");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                        }
                    }else {
                        for (String day:list1) {
                            //拿到第一个
                            if(day.equals(list1.get(0))){
                                //查询打卡信息
                                clock.setQueryTime(day);
                                //clock.setFlag(1L);
                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                for (OaClock oaClock:oaClocks) {
                                    if(oaEvectionTrip.getStartCycle().equals("1")){
                                        //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                            //打卡状态为出差
                                            oaClock.setClockState("9");
                                            oaClockMapper.updateOaClock(oaClock);
                                    }else if(oaEvectionTrip.getStartCycle().equals("2")){
                                        //修改下班打卡数据
                                        if(oaClock.getClockType().equals("2")){
                                            //打卡状态为出差
                                            oaClock.setClockState("9");
                                            oaClockMapper.updateOaClock(oaClock);
                                        }
                                    }
                                }
                            }else if(day.equals(list1.get(list1.size()-1))){
                                //最后一天做对比
                                clock.setQueryTime(day);
                                //clock.setFlag(1L);
                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                for (OaClock oaClock:oaClocks) {
                                    if(oaEvectionTrip.getEndCycle().equals("1")){
                                        //修改上班打卡数据
                                        if(oaClock.getClockType().equals("1")){
                                            //打卡状态为出差
                                            oaClock.setClockState("9");
                                            oaClockMapper.updateOaClock(oaClock);
                                        }
                                    }else if(oaEvectionTrip.getEndCycle().equals("2")){
                                        //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                            //打卡状态为出差
                                            oaClock.setClockState("9");
                                            oaClockMapper.updateOaClock(oaClock);
                                    }
                                }
                            }else {
                                clock.setQueryTime(day);
                                //clock.setFlag(1L);
                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                //上班下班都要更改
                                for (OaClock oaClock:oaClocks) {
                                    //打卡状态为出差
                                    oaClock.setClockState("9");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                        }
                    }
                }
            }
            oaEvection.setSubmitTime(DateUtils.getNowDate());

            //释放锁
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaEvection, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_EVECTION.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OAEVECTION_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaEvection, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_EVECTION.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaEvection, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_EVECTION.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        oaEvectionMapper.updateById(oaEvection);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaEvection oaEvection, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaEvection.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"出差申请审批提醒",msgContent,"oa",oaEvection.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaEvection checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaEvection oaEvection = oaEvectionMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaEvection == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAEVECTION_001);
        }
        if (!AdminCodeEnum.OAEVECTION_STATUS_VERIFYING.getCode().equals(oaEvection.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAEVECTION_002);
        }
        return oaEvection;
    }
}
