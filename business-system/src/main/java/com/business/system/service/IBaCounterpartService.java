package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaCounterpart;

/**
 * 对接人Service接口
 *
 * @author ljb
 * @date 2022-11-29
 */
public interface IBaCounterpartService
{
    /**
     * 查询对接人
     *
     * @param id 对接人ID
     * @return 对接人
     */
    public BaCounterpart selectBaCounterpartById(String id);

    /**
     * 查询对接人列表
     *
     * @param baCounterpart 对接人
     * @return 对接人集合
     */
    public List<BaCounterpart> selectBaCounterpartList(BaCounterpart baCounterpart);

    /**
     * 新增对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    public int insertBaCounterpart(BaCounterpart baCounterpart);

    /**
     * 修改对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    public int updateBaCounterpart(BaCounterpart baCounterpart);

    /**
     * 批量删除对接人
     *
     * @param ids 需要删除的对接人ID
     * @return 结果
     */
    public int deleteBaCounterpartByIds(String[] ids);

    /**
     * 删除对接人信息
     *
     * @param id 对接人ID
     * @return 结果
     */
    public int deleteBaCounterpartById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


}
