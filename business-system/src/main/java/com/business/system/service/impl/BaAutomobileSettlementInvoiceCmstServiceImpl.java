package com.business.system.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaAutomobileSettlementInvoice;
import com.business.system.domain.BaShippingOrderCmst;
import com.business.system.mapper.BaShippingOrderCmstMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaAutomobileSettlementInvoiceCmstMapper;
import com.business.system.domain.BaAutomobileSettlementInvoiceCmst;
import com.business.system.service.IBaAutomobileSettlementInvoiceCmstService;

/**
 * 中储智运无车承运开票Service业务层处理
 *
 * @author single
 * @date 2023-08-21
 */
@Service
public class BaAutomobileSettlementInvoiceCmstServiceImpl extends ServiceImpl<BaAutomobileSettlementInvoiceCmstMapper, BaAutomobileSettlementInvoiceCmst> implements IBaAutomobileSettlementInvoiceCmstService
{
    @Autowired
    private BaAutomobileSettlementInvoiceCmstMapper baAutomobileSettlementInvoiceCmstMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    /**
     * 查询中储智运无车承运开票
     *
     * @param id 中储智运无车承运开票ID
     * @return 中储智运无车承运开票
     */
    @Override
    public BaAutomobileSettlementInvoiceCmst selectBaAutomobileSettlementInvoiceCmstById(String id)
    {
        BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst = baAutomobileSettlementInvoiceCmstMapper.selectBaAutomobileSettlementInvoiceCmstById(id);
        String orderId = baAutomobileSettlementInvoiceCmst.getOrderId();
        boolean contains = orderId.contains(",");
        List<String> resultList = new ArrayList<>();
        if(contains) {
            String[] split = orderId.split(",");
            Collections.addAll(resultList,split);
        }else {
            resultList.add(orderId);
        }
       /* //服务费
        BigDecimal invoiceMoney = baAutomobileSettlementInvoiceCmst1.getInvoiceMoney();
        BigDecimal bigDecimal = invoiceMoney.multiply(new BigDecimal(0.055)).setScale(2, BigDecimal.ROUND_HALF_UP);
        bigDecimal.subtract(invoiceMoney);
        //服务费
        baAutomobileSettlementInvoiceCmst1.setServiceCharge(bigDecimal);
        //运费
        baAutomobileSettlementInvoiceCmst1.setFreight(invoiceMoney.subtract(bigDecimal));*/
        List<BaShippingOrderCmst> baShippingOrderCmsts=new ArrayList<>();
        for (String orderId1:resultList){
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baAutomobileSettlementInvoiceCmst.getOrderId());
            baShippingOrderCmst.setSignForTime(baAutomobileSettlementInvoiceCmst.getInvoiceDate());
            //运单金额
            BigDecimal bigDecimal = baShippingOrderCmst.getSettleMoney().multiply(new BigDecimal(1 - 0.055)).setScale(2, BigDecimal.ROUND_HALF_UP);
            baShippingOrderCmst.setThdnoMoney(baShippingOrderCmst.getSettleMoney().multiply(new BigDecimal(1 - 0.055)).setScale(2, BigDecimal.ROUND_HALF_UP));
            //运费单价
            baShippingOrderCmst.setUnitPrice(bigDecimal.divide(baShippingOrderCmst.getTonnage(),2,BigDecimal.ROUND_HALF_UP));
            baShippingOrderCmsts.add(baShippingOrderCmst);
        }
        //货主名称
        if(StringUtils.isNotNull(baShippingOrderCmsts.get(0))) {
            baAutomobileSettlementInvoiceCmst.setConsignorUserName(baShippingOrderCmsts.get(0).getConsignorUserName());

        }
        baAutomobileSettlementInvoiceCmst.setBaShippingOrderCmst(baShippingOrderCmsts);
        return baAutomobileSettlementInvoiceCmst;
    }

    /**
     * 查询中储智运无车承运开票列表
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 中储智运无车承运开票
     */
    @Override
    public List<BaAutomobileSettlementInvoiceCmst> selectBaAutomobileSettlementInvoiceCmstList(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        List<BaAutomobileSettlementInvoiceCmst> baAutomobileSettlementInvoiceCmsts = baAutomobileSettlementInvoiceCmstMapper.selectBaAutomobileSettlementInvoiceCmstList(baAutomobileSettlementInvoiceCmst);
        for (BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst1:baAutomobileSettlementInvoiceCmsts) {
            //运单id
            String orderId = baAutomobileSettlementInvoiceCmst1.getOrderId();
            boolean contains = orderId.contains(",");
            List<String> resultList = new ArrayList<>();
            if(contains) {
                String[] split = orderId.split(",");
                Collections.addAll(resultList,split);
            }else {
                resultList.add(orderId);
            }
            //服务费
            BigDecimal invoiceMoney = baAutomobileSettlementInvoiceCmst1.getInvoiceMoney();
            BigDecimal bigDecimal = invoiceMoney.multiply(new BigDecimal(0.055)).setScale(2, BigDecimal.ROUND_HALF_UP);
            bigDecimal.subtract(invoiceMoney);
            //服务费
            baAutomobileSettlementInvoiceCmst1.setServiceCharge(bigDecimal);
            //运费
            baAutomobileSettlementInvoiceCmst1.setFreight(invoiceMoney.subtract(bigDecimal));
            //货主名称
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstByOrderId(resultList.get(0));
            baAutomobileSettlementInvoiceCmst1.setConsignorUserName(baShippingOrderCmst.getConsignorUserName());
            baAutomobileSettlementInvoiceCmst1.setBaShippingOrderCmstNumber(resultList.size());
        }

        return baAutomobileSettlementInvoiceCmsts;
    }

    /**
     * 新增中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    @Override
    public int insertBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        baAutomobileSettlementInvoiceCmst.setCreateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceCmstMapper.insertBaAutomobileSettlementInvoiceCmst(baAutomobileSettlementInvoiceCmst);
    }

    /**
     * 修改中储智运无车承运开票
     *
     * @param baAutomobileSettlementInvoiceCmst 中储智运无车承运开票
     * @return 结果
     */
    @Override
    public int updateBaAutomobileSettlementInvoiceCmst(BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst)
    {
        baAutomobileSettlementInvoiceCmst.setUpdateTime(DateUtils.getNowDate());
        return baAutomobileSettlementInvoiceCmstMapper.updateBaAutomobileSettlementInvoiceCmst(baAutomobileSettlementInvoiceCmst);
    }

    /**
     * 批量删除中储智运无车承运开票
     *
     * @param ids 需要删除的中储智运无车承运开票ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceCmstByIds(String[] ids)
    {
        return baAutomobileSettlementInvoiceCmstMapper.deleteBaAutomobileSettlementInvoiceCmstByIds(ids);
    }

    /**
     * 删除中储智运无车承运开票信息
     *
     * @param id 中储智运无车承运开票ID
     * @return 结果
     */
    @Override
    public int deleteBaAutomobileSettlementInvoiceCmstById(String id)
    {
        return baAutomobileSettlementInvoiceCmstMapper.deleteBaAutomobileSettlementInvoiceCmstById(id);
    }

    @Override
    public int added(JSONObject jsonObject) {
        System.out.println(jsonObject);
        BaAutomobileSettlementInvoiceCmst baAutomobileSettlementInvoiceCmst = JSONObject.toJavaObject((JSONObject) JSON.toJSON(jsonObject), BaAutomobileSettlementInvoiceCmst.class);
        baAutomobileSettlementInvoiceCmst.setId(getRedisIncreID.getId());
        baAutomobileSettlementInvoiceCmst.setCreateTime(DateUtils.getNowDate());
        int result = baAutomobileSettlementInvoiceCmstMapper.insertBaAutomobileSettlementInvoiceCmst(baAutomobileSettlementInvoiceCmst);
        if(result > 0){
            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(baAutomobileSettlementInvoiceCmst.getOrderId());
            if(StringUtils.isNotNull(baShippingOrderCmst)){
                baShippingOrderCmst.setInvoiceType("1");
                baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
            }
        }
        return result;
    }

}
