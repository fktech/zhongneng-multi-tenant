package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaOther;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IEnterRegisterApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import org.springframework.stereotype.Service;


/**
 * 企业注册申请审批结果:审批拒绝
 */
@Service("enterRegisterApprovalContent:processApproveResult:reject")
public class EnterRegisterApprovalRejectServiceImpl extends IEnterRegisterApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.ENTERREGISTER_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected SysCompany checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
