package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaRailwayPlatform;

/**
 * 铁路站台Service接口
 *
 * @author ljb
 * @date 2022-11-29
 */
public interface IBaRailwayPlatformService
{
    /**
     * 查询铁路站台
     *
     * @param id 铁路站台ID
     * @return 铁路站台
     */
    public BaRailwayPlatform selectBaRailwayPlatformById(String id);

    /**
     * 查询铁路站台列表
     *
     * @param baRailwayPlatform 铁路站台
     * @return 铁路站台集合
     */
    public List<BaRailwayPlatform> selectBaRailwayPlatformList(BaRailwayPlatform baRailwayPlatform);

    /**
     * 运费表
     */
     public List<BaRailwayPlatform> freightTable();

    /**
     * 新增铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    public int insertBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform);

    /**
     * 修改铁路站台
     *
     * @param baRailwayPlatform 铁路站台
     * @return 结果
     */
    public int updateBaRailwayPlatform(BaRailwayPlatform baRailwayPlatform);

    /**
     * 批量删除铁路站台
     *
     * @param ids 需要删除的铁路站台ID
     * @return 结果
     */
    public int deleteBaRailwayPlatformByIds(String[] ids);

    /**
     * 删除铁路站台信息
     *
     * @param id 铁路站台ID
     * @return 结果
     */
    public int deleteBaRailwayPlatformById(String id);


}
