package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IIntodepotApprovalService;
import com.business.system.service.workflow.IInvoiceApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 入库申请审批结果:审批通过
 */
@Service("intodepotApprovalContent:processApproveResult:pass")
@Slf4j
public class IntodepotApprovalAgreeServiceImpl extends IIntodepotApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.INTODEPOT_STATUS_PASS.getCode());
        return result;
    }
}
