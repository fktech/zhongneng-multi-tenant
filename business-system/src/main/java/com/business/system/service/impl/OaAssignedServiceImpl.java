package com.business.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.OaApprovalOpinions;
import com.business.system.domain.OaSupervising;
import com.business.system.domain.OaSupervisingRecord;
import com.business.system.mapper.*;
import com.business.system.service.IOaSupervisingRecordService;
import com.business.system.service.IOaSupervisingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.ParameterResolutionDelegate;
import org.springframework.stereotype.Service;
import com.business.system.domain.OaAssigned;
import com.business.system.service.IOaAssignedService;

import javax.validation.constraints.Size;

/**
 * 交办数据Service业务层处理
 *
 * @author ljb
 * @date 2023-11-21
 */
@Service
public class OaAssignedServiceImpl extends ServiceImpl<OaAssignedMapper, OaAssigned> implements IOaAssignedService
{
    @Autowired
    private OaAssignedMapper oaAssignedMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaSupervisingMapper oaSupervisingMapper;

    @Autowired
    private IOaSupervisingService oaSupervisingService;

    @Autowired
    private OaSupervisingRecordMapper oaSupervisingRecordMapper;

    @Autowired
    private IOaSupervisingRecordService oaSupervisingRecordService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaApprovalOpinionsMapper oaApprovalOpinionsMapper;

    /**
     * 查询交办数据
     *
     * @param id 交办数据ID
     * @return 交办数据
     */
    @Override
    public OaAssigned selectOaAssignedById(String id)
    {
        OaAssigned assigned = oaAssignedMapper.selectOaAssignedById(id);
        //督办信息
        if(StringUtils.isNotEmpty(assigned.getSupervisingId())){
            OaSupervising oaSupervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());
            //办理记录
            OaSupervisingRecord oaSupervisingRecord = new OaSupervisingRecord();
            oaSupervisingRecord.setSupervisingId(oaSupervising.getId());
            List<OaSupervisingRecord> oaSupervisingRecords = oaSupervisingRecordService.selectOaSupervisingRecordList(oaSupervisingRecord);
            oaSupervising.setOaSupervisingRecordList(oaSupervisingRecords);
            assigned.setOaSupervising(oaSupervising);
            //所有批复意见
            List<Map<String,String>> mapList = new ArrayList<>();
            QueryWrapper<OaApprovalOpinions> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("association_id",oaSupervising.getId());
            queryWrapper.orderByAsc("create_time");
            List<OaApprovalOpinions> oaApprovalOpinions = oaApprovalOpinionsMapper.selectList(queryWrapper);
            for (OaApprovalOpinions opinions:oaApprovalOpinions) {
                Map<String,String> map = new HashMap<>();
                SysUser user = sysUserMapper.selectUserById(opinions.getUserId());
                if(StringUtils.isNotNull(user)){
                    SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                    map.put(dept.getDeptName()+user.getNickName(),opinions.getOpinion());
                }
                mapList.add(map);
            }
            oaSupervising.setMap(mapList);
            assigned.setMap(mapList);
        }
        return assigned;
    }

    /**
     * 查询交办数据列表
     *
     * @param oaAssigned 交办数据
     * @return 交办数据
     */
    @Override
    public List<OaAssigned> selectOaAssignedList(OaAssigned oaAssigned)
    {
        List<OaAssigned> oaAssigneds = oaAssignedMapper.oaAssignedList(oaAssigned);
        for (OaAssigned assigned:oaAssigneds) {
            if(StringUtils.isNotEmpty(assigned.getSupervisingId())){
                OaSupervising oaSupervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());

                    assigned.setOaSupervising(oaSupervising);

            }
        }
        return oaAssigneds;
    }

    /**
     * 新增交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    @Override
    public int insertOaAssigned(OaAssigned oaAssigned)
    {
        oaAssigned.setId(getRedisIncreID.getId());
        oaAssigned.setCreateTime(DateUtils.getNowDate());
        return oaAssignedMapper.insertOaAssigned(oaAssigned);
    }

    /**
     * 修改交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    @Override
    public int updateOaAssigned(OaAssigned oaAssigned)
    {
        oaAssigned.setUpdateTime(DateUtils.getNowDate());
        return oaAssignedMapper.updateOaAssigned(oaAssigned);
    }

    /**
     * 批量删除交办数据
     *
     * @param ids 需要删除的交办数据ID
     * @return 结果
     */
    @Override
    public int deleteOaAssignedByIds(String[] ids)
    {
        return oaAssignedMapper.deleteOaAssignedByIds(ids);
    }

    /**
     * 删除交办数据信息
     *
     * @param id 交办数据ID
     * @return 结果
     */
    @Override
    public int deleteOaAssignedById(String id)
    {
        return oaAssignedMapper.deleteOaAssignedById(id);
    }

}
