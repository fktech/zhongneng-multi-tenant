package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;

import com.business.system.domain.BaContract;
import com.business.system.domain.BaPayment;
import com.business.system.domain.dto.CountPaymentDTO;
import com.business.system.domain.dto.BaPaymentVoucherDTO;
import com.business.system.domain.dto.MonthSumDTO;
import com.business.system.domain.dto.BusinessUserDTO;
import com.business.system.domain.vo.BaPaymentStatVO;

/**
 * 付款信息Service接口
 *
 * @author single
 * @date 2022-12-26
 */
public interface IBaPaymentService
{
    /**
     * 查询付款信息
     *
     * @param id 付款信息ID
     * @return 付款信息
     */
    public BaPayment selectBaPaymentById(String id);

    /**
     * 查询付款信息列表
     *
     * @param baPayment 付款信息
     * @return 付款信息集合
     */
    public List<BaPayment> selectBaPaymentList(BaPayment baPayment);

    /**
     * 新增付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    public int insertBaPayment(BaPayment baPayment);

    /**
     * 修改付款信息
     *
     * @param baPayment 付款信息
     * @return 结果
     */
    public int updateBaPayment(BaPayment baPayment);

    /**
     * 结束付款
     * @param id
     * @return
     */
    public int endPayment(String id);

    /**
     * 批量删除付款信息
     *
     * @param ids 需要删除的付款信息ID
     * @return 结果
     */
    public int deleteBaPaymentByIds(String[] ids);

    /**
     * 删除付款信息信息
     *
     * @param id 付款信息ID
     * @return 结果
     */
    public int deleteBaPaymentById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 新增付款凭证
     */
    int insertBaPaymentVoucher(BaPaymentVoucherDTO baPaymentVoucherDTO);
    /**
     * 查询财务统计中营业额从本月去之前12个月的收款金额
     *
     * @param baPayment 认领
     * @return 认领集合
     */
    public List<MonthSumDTO> SelectSalesVolumeByEveryoneMonth(BaPayment baPayment);
    /**
     * 查询财本年度的收款金额
     *
     * @param
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByyear();
    /**
     * 查询财本年度的收款金额
     *
     * @param
     * @return 认领集合
     */
    public BigDecimal SelectSalesVolumeByyearAndType(String type);

    /**
     * PC端首页累计付款
     */
    public BigDecimal countPaymentList(CountPaymentDTO countPaymentDTO);

    /**
     * 统计付款
     * @param countPaymentDTO
     * @return
     */
    public int countDepotPaymentList(CountPaymentDTO countPaymentDTO);

    /**
     * 根据付款流程实例ID查询对应项目的业务经理用户ID
     * @param paymentUserDTO
     * @return
     */
    public List<String> getUserIdByProcessInstanceId(BusinessUserDTO businessUserDTO);

    /**
     * 关联
     * @param baPayment
     * @return
     */
    public int association(BaPayment baPayment);

    /**
     * 可视化大屏统计应付账款
     * @return
     */
    public BaPaymentStatVO sumBaPayment();

    /**
     * 查询多租户授权信息付款信息列表
     * @param baPayment
     * @return
     */
    public List<BaPayment> selectBaPaymentAuthorizedList(BaPayment baPayment);

    /**
     * 授权付款管理
     */
    public int authorizedBaPayment(BaPayment baPayment);
}
