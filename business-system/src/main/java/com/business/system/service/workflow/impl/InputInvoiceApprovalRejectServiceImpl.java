package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaInvoice;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IInputInvoiceApprovalService;
import com.business.system.service.workflow.IInvoiceApprovalService;
import org.springframework.stereotype.Service;


/**
 * 进项发票申请审批结果:审批拒绝
 */
@Service("inputInvoiceApprovalContent:processApproveResult:reject")
public class InputInvoiceApprovalRejectServiceImpl extends IInputInvoiceApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.INPUTINVOICE_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaInvoice checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
