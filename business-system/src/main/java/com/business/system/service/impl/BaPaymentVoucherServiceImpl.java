package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaPaymentVoucherMapper;
import com.business.system.domain.BaPaymentVoucher;
import com.business.system.service.IBaPaymentVoucherService;

/**
 * 付款凭证Service业务层处理
 *
 * @author single
 * @date 2023-07-15
 */
@Service
public class BaPaymentVoucherServiceImpl extends ServiceImpl<BaPaymentVoucherMapper, BaPaymentVoucher> implements IBaPaymentVoucherService
{
    @Autowired
    private BaPaymentVoucherMapper baPaymentVoucherMapper;

    /**
     * 查询付款凭证
     *
     * @param id 付款凭证ID
     * @return 付款凭证
     */
    @Override
    public BaPaymentVoucher selectBaPaymentVoucherById(String id)
    {
        return baPaymentVoucherMapper.selectBaPaymentVoucherById(id);
    }

    /**
     * 查询付款凭证列表
     *
     * @param baPaymentVoucher 付款凭证
     * @return 付款凭证
     */
    @Override
    public List<BaPaymentVoucher> selectBaPaymentVoucherList(BaPaymentVoucher baPaymentVoucher)
    {
        return baPaymentVoucherMapper.selectBaPaymentVoucherList(baPaymentVoucher);
    }

    /**
     * 新增付款凭证
     *
     * @param baPaymentVoucher 付款凭证
     * @return 结果
     */
    @Override
    public int insertBaPaymentVoucher(BaPaymentVoucher baPaymentVoucher)
    {
        baPaymentVoucher.setCreateTime(DateUtils.getNowDate());
        return baPaymentVoucherMapper.insertBaPaymentVoucher(baPaymentVoucher);
    }

    /**
     * 修改付款凭证
     *
     * @param baPaymentVoucher 付款凭证
     * @return 结果
     */
    @Override
    public int updateBaPaymentVoucher(BaPaymentVoucher baPaymentVoucher)
    {
        baPaymentVoucher.setUpdateTime(DateUtils.getNowDate());
        return baPaymentVoucherMapper.updateBaPaymentVoucher(baPaymentVoucher);
    }

    /**
     * 批量删除付款凭证
     *
     * @param ids 需要删除的付款凭证ID
     * @return 结果
     */
    @Override
    public int deleteBaPaymentVoucherByIds(String[] ids)
    {
        return baPaymentVoucherMapper.deleteBaPaymentVoucherByIds(ids);
    }

    /**
     * 删除付款凭证信息
     *
     * @param id 付款凭证ID
     * @return 结果
     */
    @Override
    public int deleteBaPaymentVoucherById(String id)
    {
        return baPaymentVoucherMapper.deleteBaPaymentVoucherById(id);
    }

}
