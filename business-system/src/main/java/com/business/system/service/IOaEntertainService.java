package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaEntertain;
import com.business.system.domain.dto.OaEntertainInstanceRelatedEditDTO;
import com.business.system.domain.dto.OaMaintenanceExpansionInstanceRelatedEditDTO;

/**
 * 招待申请Service接口
 *
 * @author ljb
 * @date 2023-11-13
 */
public interface IOaEntertainService
{
    /**
     * 查询招待申请
     *
     * @param id 招待申请ID
     * @return 招待申请
     */
    public OaEntertain selectOaEntertainById(String id);

    /**
     * 查询招待申请列表
     *
     * @param oaEntertain 招待申请
     * @return 招待申请集合
     */
    public List<OaEntertain> selectOaEntertainList(OaEntertain oaEntertain);

    /**
     * 新增招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    public int insertOaEntertain(OaEntertain oaEntertain);

    /**
     * 修改招待申请
     *
     * @param oaEntertain 招待申请
     * @return 结果
     */
    public int updateOaEntertain(OaEntertain oaEntertain);

    /**
     * 批量删除招待申请
     *
     * @param ids 需要删除的招待申请ID
     * @return 结果
     */
    public int deleteOaEntertainByIds(String[] ids);

    /**
     * 删除招待申请信息
     *
     * @param id 招待申请ID
     * @return 结果
     */
    public int deleteOaEntertainById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


    int updateProcessBusinessData(OaEntertainInstanceRelatedEditDTO oaEntertainInstanceRelatedEditDTO);
}
