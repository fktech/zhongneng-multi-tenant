package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaCoalSourceStorage;
import com.business.system.mapper.BaCoalSourceStorageMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaCoalSourceStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * 煤源库Service业务层处理
 *
 * @author single
 * @date 2024-01-25
 */
@Service
public class BaCoalSourceStorageServiceImpl extends ServiceImpl<BaCoalSourceStorageMapper, BaCoalSourceStorage> implements IBaCoalSourceStorageService
{
    @Autowired
    private BaCoalSourceStorageMapper baCoalSourceStorageMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询煤源库
     *
     * @param id 煤源库ID
     * @return 煤源库
     */
    @Override
    public BaCoalSourceStorage selectBaCoalSourceStorageById(String id)
    {
        BaCoalSourceStorage baCoalSourceStorage = baCoalSourceStorageMapper.selectBaCoalSourceStorageById(id);
        if(!ObjectUtils.isEmpty(baCoalSourceStorage)){
            //发起人信息
            if(StringUtils.isNotEmpty(baCoalSourceStorage.getCreateBy())){
                SysUser sysUser = sysUserMapper.selectUserById(baCoalSourceStorage.getUserId());
                if(!ObjectUtils.isEmpty(sysUser)){
                    baCoalSourceStorage.setCreateByName(sysUser.getNickName());
                }
            }
        }
        return baCoalSourceStorage;
    }

    /**
     * 查询煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库
     */
    @Override
    public List<BaCoalSourceStorage> selectBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage)
    {
        List<BaCoalSourceStorage> baCoalSourceStorages = baCoalSourceStorageMapper.selectBaCoalSourceStorageList(baCoalSourceStorage);
        if(!CollectionUtils.isEmpty(baCoalSourceStorages)){
            for(BaCoalSourceStorage baCoalSourceStorage1 : baCoalSourceStorages){
                if(!ObjectUtils.isEmpty(baCoalSourceStorage1)){
                    //发起人信息
                    if(StringUtils.isNotEmpty(baCoalSourceStorage1.getCreateBy())){
                        SysUser sysUser = sysUserMapper.selectUserById(baCoalSourceStorage1.getUserId());
                        if(!ObjectUtils.isEmpty(sysUser)){
                            baCoalSourceStorage1.setCreateByName(sysUser.getNickName());
                        }
                    }
                }
            }
        }
        return baCoalSourceStorages;
    }

    /**
     * 新增煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    @Override
    public int insertBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage)
    {
        baCoalSourceStorage.setId("MY"+getRedisIncreID.getId());
        baCoalSourceStorage.setCreateTime(DateUtils.getNowDate());
        baCoalSourceStorage.setCreateBy(SecurityUtils.getUsername());
        baCoalSourceStorage.setUserId(SecurityUtils.getUserId());
        baCoalSourceStorage.setDeptId(SecurityUtils.getDeptId());
        //租户ID
        baCoalSourceStorage.setTenantId(SecurityUtils.getCurrComId());
        Integer result = baCoalSourceStorageMapper.insertBaCoalSourceStorage(baCoalSourceStorage);
        return result;
    }

    /**
     * 修改煤源库
     *
     * @param baCoalSourceStorage 煤源库
     * @return 结果
     */
    @Override
    public int updateBaCoalSourceStorage(BaCoalSourceStorage baCoalSourceStorage)
    {
        baCoalSourceStorage.setUpdateBy(SecurityUtils.getUsername());
        baCoalSourceStorage.setUpdateTime(DateUtils.getNowDate());
        return baCoalSourceStorageMapper.updateBaCoalSourceStorage(baCoalSourceStorage);
    }

    /**
     * 批量删除煤源库
     *
     * @param ids 需要删除的煤源库ID
     * @return 结果
     */
    @Override
    public int deleteBaCoalSourceStorageByIds(String[] ids)
    {
        BaCoalSourceStorage baCoalSourceStorage = null;
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                baCoalSourceStorage = baCoalSourceStorageMapper.selectBaCoalSourceStorageById(id);
                if(!ObjectUtils.isEmpty(baCoalSourceStorage)){
                    baCoalSourceStorage.setFlag((long) 1);
                    baCoalSourceStorageMapper.updateBaCoalSourceStorage(baCoalSourceStorage);
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除煤源库信息
     *
     * @param id 煤源库ID
     * @return 结果
     */
    @Override
    public int deleteBaCoalSourceStorageById(String id)
    {
        return baCoalSourceStorageMapper.deleteBaCoalSourceStorageById(id);
    }

    /**
     * 统计煤源库列表
     *
     * @param baCoalSourceStorage 煤源库
     * @return 煤源库集合
     */
    @Override
    public List<BaCoalSourceStorage> countBaCoalSourceStorageList(BaCoalSourceStorage baCoalSourceStorage) {
        List<BaCoalSourceStorage> baCoalSourceStorages = baCoalSourceStorageMapper.countBaCoalSourceStorageList(baCoalSourceStorage);
        return baCoalSourceStorages;
    }

}
