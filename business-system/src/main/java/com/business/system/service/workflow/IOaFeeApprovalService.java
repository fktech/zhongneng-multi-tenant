package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaFee;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.OaEgressMapper;
import com.business.system.mapper.OaFeeMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 费用申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaFeeApprovalService {

    @Autowired
    protected OaFeeMapper oaFeeMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaFee oaFee = new OaFee();
        oaFee.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaFee = oaFeeMapper.selectOaFeeById(oaFee.getId());
        oaFee.setState(status);
        String userId = String.valueOf(oaFee.getUserId());
        if(AdminCodeEnum.OAFEE_STATUS_PASS.getCode().equals(status)){
            //释放锁
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaFee, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_FEE.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OAFEE_STATUS_REJECT.getCode().equals(status)){

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaFee, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_FEE.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaFee, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_FEE.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        oaFeeMapper.updateById(oaFee);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaFee oaFee, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaFee.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_FEE.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"费用申请审批提醒",msgContent,"oa",oaFee.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaFee checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaFee oaFee = oaFeeMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaFee == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAFEE_001);
        }
        if (!AdminCodeEnum.OAFEE_STATUS_VERIFYING.getCode().equals(oaFee.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAFEE_002);
        }
        return oaFee;
    }
}
