package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaComment;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaCommentService;
import com.business.system.service.ISysUserService;
import com.business.system.util.ProcessMapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaTopicMapper;
import com.business.system.domain.BaTopic;
import com.business.system.service.IBaTopicService;
import org.springframework.util.CollectionUtils;

/**
 * 问答Service业务层处理
 *
 * @author single
 * @date 2024-04-07
 */
@Service
public class BaTopicServiceImpl extends ServiceImpl<BaTopicMapper, BaTopic> implements IBaTopicService
{
    @Autowired
    private BaTopicMapper baTopicMapper;
    @Autowired
    private GetRedisIncreID getRedisIncreID;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private IBaCommentService baCommentService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询问答
     *
     * @param id 问答ID
     * @return 问答
     */
    @Override
    public BaTopic selectBaTopicById(String id)
    {
        BaTopic baTopic = baTopicMapper.selectBaTopicById(id);
        if(baTopic.getUserId() != null){
            SysUser user = sysUserMapper.selectUserById(baTopic.getUserId());
            baTopic.setUserName(user.getNickName());
        }

        return baTopic;
    }

    /**
     * 查询问答列表
     *
     * @param baTopic 问答
     * @return 问答
     */
    @Override
    public List<BaTopic> selectBaTopicList(BaTopic baTopic)
    {
        List<BaTopic> baTopics = baTopicMapper.selectBaTopicList(baTopic);
        for (BaTopic topic:baTopics) {
            BaComment baComment = new BaComment();
            List<BaComment> baComments = this.commentList(baComment.setBlogId(topic.getId()));
            topic.setCommentList(baComments);
            //用户姓名
            if(topic.getUserId() != null){
                SysUser user = sysUserMapper.selectUserById(topic.getUserId());
                topic.setUserName(user.getNickName());
            }
        }
        return baTopics;
    }

    /**
     * 新增问答
     *
     * @param baTopic 问答
     * @return 结果
     */
    @Override
    public int insertBaTopic(BaTopic baTopic)
    {
        baTopic.setId(getRedisIncreID.getId());
        baTopic.setCreateTime(DateUtils.getNowDate());
        baTopic.setUserId(SecurityUtils.getUserId());
        return baTopicMapper.insertBaTopic(baTopic);
    }

    /**
     * 修改问答
     *
     * @param baTopic 问答
     * @return 结果
     */
    @Override
    public int updateBaTopic(BaTopic baTopic)
    {
        baTopic.setUpdateTime(DateUtils.getNowDate());
        return baTopicMapper.updateBaTopic(baTopic);
    }

    /**
     * 批量删除问答
     *
     * @param ids 需要删除的问答ID
     * @return 结果
     */
    @Override
    public int deleteBaTopicByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaTopic baTopic = baTopicMapper.selectBaTopicById(id);
                baTopic.setFlag(1L);
                baTopicMapper.updateBaTopic(baTopic);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除问答信息
     *
     * @param id 问答ID
     * @return 结果
     */
    @Override
    public int deleteBaTopicById(String id)
    {
        return baTopicMapper.deleteBaTopicById(id);
    }

    private List<BaComment> commentList(BaComment baComment)
    {
        List<BaComment> baComments = baCommentService.selectBaCommentList(baComment);

        List<BaComment> list = ProcessMapUtil.processComments(baComments);
        for (BaComment comment:list) {
            SysUser sysUser = userService.selectUserById(comment.getUserId());
            comment.setUserName(sysUser.getNickName());
            comment.setAvatar(sysUser.getAvatar());
            //下级回复姓名
            if(!CollectionUtils.isEmpty(comment.getChild()) && comment.getChild().size() > 0){
                for (BaComment child:comment.getChild()) {
                    SysUser user = userService.selectUserById(child.getUserId());
                    child.setUserName(user.getNickName());
                }
            }
        }
        return list;
    }

}
