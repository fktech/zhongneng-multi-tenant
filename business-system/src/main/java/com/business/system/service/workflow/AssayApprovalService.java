package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 化验审批接口 服务类
 * @date: 2024/1/5 11:10
 */
@Service
public class AssayApprovalService {

    @Autowired
    protected BaSampleAssayMapper baSampleAssayMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaSampleAssay baSampleAssay = new BaSampleAssay();
        baSampleAssay.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baSampleAssay = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
        String userId = String.valueOf(baSampleAssay.getUserId());
        if(AdminCodeEnum.ASSAY_STATUS_PASS.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
                //入库化验
                    if(StringUtils.isNotEmpty(baSampleAssay.getRelationId())){
                        BaSampleAssay sampleAssay = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getRelationId());
                        if("1".equals(sampleAssay.getHeadType())){
                            if(StringUtils.isNotEmpty(sampleAssay.getThdno())){
                                String[] split = sampleAssay.getThdno().split(",");
                                for (String thdno:split) {
                                    //查询运单信息标记
                                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                                    baShippingOrder.setThdno(thdno);
                                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                                    if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                                        BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(thdno);
                                        baShippingOrder1.setExperimentId(baSampleAssay.getId());
                                        baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                                    }else if("2".equals(waybillDTOList.get(0).getType())){
                                        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(thdno);
                                        baShippingOrderCmst.setExperimentId(baSampleAssay.getId());
                                        baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    }
                                }
                            }
                        }else if("2".equals(sampleAssay.getHeadType())){
                            if(StringUtils.isNotEmpty(sampleAssay.getGoodsSource())){
                                //查找货源信息
                                BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                                baTransportAutomobile.setSupplierNo(sampleAssay.getGoodsSource());
                                List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                                //货源信息
                                if("1".equals(sourceGoodsDTOS.get(0).getType()) || "3".equals(sourceGoodsDTOS.get(0).getType())){
                                    BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(sourceGoodsDTOS.get(0).getSupplierNo());
                                    baTransportAutomobile1.setSampleType(baSampleAssay.getId());
                                    baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                                }else if("2".equals(sourceGoodsDTOS.get(0).getType())){
                                    BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(sourceGoodsDTOS.get(0).getSupplierNo());
                                    transportCmst.setSampleType(baSampleAssay.getId());
                                    baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                                }
                                //查询运单信息标记
                                BaShippingOrder baShippingOrder = new BaShippingOrder();
                                baShippingOrder.setSupplierNo(sourceGoodsDTOS.get(0).getSupplierNo());
                                List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                                for (waybillDTO waybillDTO:waybillDTOList) {
                                    if("1".equals(waybillDTO.getType()) || "3".equals(waybillDTO.getType())){
                                        BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(waybillDTO.getThdno());
                                        baShippingOrder1.setExperimentId(baSampleAssay.getId());
                                        baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                                    }else if("2".equals(waybillDTO.getType())){
                                        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getThdno());
                                        baShippingOrderCmst.setExperimentId(baSampleAssay.getId());
                                        baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                    }
                                }
                            }
                        }
                    }
            //插入消息通知信息
            this.insertMessage(baSampleAssay, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.ASSAY_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baSampleAssay, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baSampleAssay, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baSampleAssay.setState(status);
        baSampleAssayMapper.updateById(baSampleAssay);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSampleAssay baSampleAssay, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baSampleAssay.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"化验审批提醒",msgContent,baMessage.getType(),baSampleAssay.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaSampleAssay checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaSampleAssay baSampleAssay = baSampleAssayMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baSampleAssay == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_ASSAY_001);
        }
        if (!AdminCodeEnum.ASSAY_STATUS_VERIFYING.getCode().equals(baSampleAssay.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_ASSAY_002);
        }
        return baSampleAssay;
    }
}
