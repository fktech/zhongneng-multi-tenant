package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaSampleAssay;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.SampleApprovalService;
import com.business.system.service.workflow.WashIntodepotApprovalService;
import org.springframework.stereotype.Service;


/**
 * 取样申请审批结果:审批拒绝
 */
@Service("sampleApprovalContent:processApproveResult:reject")
public class SampleApprovalRejectServiceImpl extends SampleApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.SAMPLE_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaSampleAssay checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
