package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaSettlementDetailMapper;
import com.business.system.domain.BaSettlementDetail;
import com.business.system.service.IBaSettlementDetailService;

/**
 * 结算明细Service业务层处理
 *
 * @author ljb
 * @date 2023-02-06
 */
@Service
public class BaSettlementDetailServiceImpl extends ServiceImpl<BaSettlementDetailMapper, BaSettlementDetail> implements IBaSettlementDetailService
{
    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    /**
     * 查询结算明细
     *
     * @param id 结算明细ID
     * @return 结算明细
     */
    @Override
    public BaSettlementDetail selectBaSettlementDetailById(String id)
    {
        return baSettlementDetailMapper.selectBaSettlementDetailById(id);
    }

    /**
     * 查询结算明细列表
     *
     * @param baSettlementDetail 结算明细
     * @return 结算明细
     */
    @Override
    public List<BaSettlementDetail> selectBaSettlementDetailList(BaSettlementDetail baSettlementDetail)
    {
        return baSettlementDetailMapper.selectBaSettlementDetailList(baSettlementDetail);
    }

    /**
     * 新增结算明细
     *
     * @param baSettlementDetail 结算明细
     * @return 结果
     */
    @Override
    public int insertBaSettlementDetail(BaSettlementDetail baSettlementDetail)
    {
        baSettlementDetail.setCreateTime(DateUtils.getNowDate());
        return baSettlementDetailMapper.insertBaSettlementDetail(baSettlementDetail);
    }

    /**
     * 修改结算明细
     *
     * @param baSettlementDetail 结算明细
     * @return 结果
     */
    @Override
    public int updateBaSettlementDetail(BaSettlementDetail baSettlementDetail)
    {
        baSettlementDetail.setUpdateTime(DateUtils.getNowDate());
        return baSettlementDetailMapper.updateBaSettlementDetail(baSettlementDetail);
    }

    /**
     * 批量删除结算明细
     *
     * @param ids 需要删除的结算明细ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementDetailByIds(String[] ids)
    {
        return baSettlementDetailMapper.deleteBaSettlementDetailByIds(ids);
    }

    /**
     * 删除结算明细信息
     *
     * @param id 结算明细ID
     * @return 结果
     */
    @Override
    public int deleteBaSettlementDetailById(String id)
    {
        return baSettlementDetailMapper.deleteBaSettlementDetailById(id);
    }

}
