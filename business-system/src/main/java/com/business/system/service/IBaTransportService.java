package com.business.system.service;

import java.math.BigDecimal;
import java.util.List;

import com.business.system.domain.*;
import com.business.system.domain.dto.CountTransportDTO;

/**
 * 运输Service接口
 *
 * @author ljb
 * @date 2022-12-13
 */
public interface IBaTransportService
{
    /**
     * 查询运输
     *
     * @param id 运输ID
     * @return 运输
     */
    public BaTransport selectBaTransportById(String id);

    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输集合
     */
    public List<BaTransport> selectBaTransportList(BaTransport baTransport);
    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输集合
     */
    public List<BaTransport> selectBaTransport(BaTransport baTransport);
    /**
     * 查询运输列表
     *
     * @param baTransport 运输
     * @return 运输集合
     */
    public List<BaTransport> selectBaTransport1(BaTransport baTransport);
    /**

    /**
     * 新增运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    public int insertBaTransport(BaTransport baTransport);

    /**
     * 修改运输
     *
     * @param baTransport 运输
     * @return 结果
     */
    public int updateBaTransport(BaTransport baTransport);

    /**
     * 批量删除运输
     *
     * @param ids 需要删除的运输ID
     * @return 结果
     */
    public int deleteBaTransportByIds(String[] ids);

    /**
     * 删除运输信息
     *
     * @param id 运输ID
     * @return 结果
     */
    public int deleteBaTransportById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 发货
     * @param baTransport
     * @return
     */
    public int delivery(BaTransport baTransport);

    /**
     * 收货
     * @param baTransport
     * @return
     */
    public int receipt(BaTransport baTransport);

    /**
     * 货转按钮
     * @param baTransport
     * @return
     */
    public int transfer(BaTransport baTransport);

    /**
     * 运输下拉
     * @param startId
     * @return
     */
    public List<BaTransport> select(String startId);

    /**
     * 保存按钮
     * @param baTransport
     * @return
     */
    public int saveButton(BaTransport baTransport);

    /**
     * 修改货权转移证明数据
     */
    Integer updateBaCheckTransferRights(BaCheck baCheck);


    /**
     * PC端首页累计发运量统计
     */
    public BigDecimal countTransportList(CountTransportDTO countTransportDTO);

    /**
     * PC端首页仓储累计发运量统计
     */
    public BigDecimal storageCountTransportList(BaTransportAutomobile baTransportAutomobile);

    /**
     * 变更火运
     */
    public int changeBaTransport(BaTransport baTransport) throws Exception;

    /**
     * 查询火车运输变更历史列表
     */
    public List<BaHiTransport> selectChangeBaHiTransportList(BaHiTransport baHiTransport);

    /**
     * 获取当前数据和上一次变更记录对比数据
     */
    public List<String> selectChangeDataList(String id);

    /**
     * 查询多租户授权信息运输列表
     * @param baTransport
     * @return
     */
    public List<BaTransport> selectBaTransportAuthorizedList(BaTransport baTransport);

    /**
     * 授权运输管理
     */
    public int authorizedBaTransport(BaTransport baTransport);
}
