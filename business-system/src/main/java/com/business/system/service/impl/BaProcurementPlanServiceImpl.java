package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 采购计划Service业务层处理
 *
 * @author ljb
 * @date 2023-08-31
 */
@Service
public class BaProcurementPlanServiceImpl extends ServiceImpl<BaProcurementPlanMapper, BaProcurementPlan> implements IBaProcurementPlanService
{
    private static final Logger log = LoggerFactory.getLogger(BaProcurementPlanServiceImpl.class);
    @Autowired
    private BaProcurementPlanMapper baProcurementPlanMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private BaPurchaseOrderMapper baPurchaseOrderMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private PostName getName;

    @Autowired
    private BaPoundRoomMapper baPoundRoomMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private IBaContractService iBaContractService;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private IBaDepotHeadService iBaDepotHeadService;


    /**
     * 查询采购计划
     *
     * @param id 采购计划ID
     * @return 采购计划
     */
    @Override
    public BaProcurementPlan selectBaProcurementPlanById(String id)
    {
        BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(id);
        //立项信息
        if(StringUtils.isNotEmpty(baProcurementPlan.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(baProcurementPlan.getProjectId());
            baProcurementPlan.setProjectName(baProject.getName());
        }
        //仓库信息
        if(StringUtils.isNotEmpty(baProcurementPlan.getDepotId())){
            BaDepot baDepot = baDepotMapper.selectBaDepotById(baProcurementPlan.getDepotId());
            baProcurementPlan.setDepotName(baDepot.getName());
        }
        //采购清单
        QueryWrapper<BaPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("plan_id",id);
        List<BaPurchaseOrder> baPurchaseOrders = baPurchaseOrderMapper.selectList(orderQueryWrapper);
        for (BaPurchaseOrder baPurchaseOrder:baPurchaseOrders) {
            //商品名称
            if(StringUtils.isNotEmpty(baPurchaseOrder.getGoodsId())){
                baPurchaseOrder.setGoodsName(baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId()).getName());
            }
            //供应商
            if(StringUtils.isNotEmpty(baPurchaseOrder.getSupplierId())){
                baPurchaseOrder.setSupplierName(baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId()).getName());
            }
        }
        baProcurementPlan.setBaPurchaseOrders(baPurchaseOrders);
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baProcurementPlan.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        //岗位信息
        String name = getName.getName(sysUser.getUserId());
        if(name.equals("") == false){
            sysUser.setPostName(name.substring(0,name.length()-1));
        }
        baProcurementPlan.setUser(sysUser);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baProcurementPlan.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baProcurementPlan.setProcessInstanceId(processInstanceIds);

        if(StringUtils.isNotEmpty(baProcurementPlan.getBelongCompanyId())){
            SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(baProcurementPlan.getBelongCompanyId()));
            if(!ObjectUtils.isEmpty(sysDept)){
                baProcurementPlan.setBelongCompanyName(sysDept.getDeptName());
            }
        }
        //合同信息
        BaContract baContract = new BaContract();
        baContract.setProcureOrder(id);
        baContract.setState(AdminCodeEnum.CONTRACT_STATUS_PASS.getCode());
        List<BaContract> baContracts = iBaContractService.selectBaContractList(baContract);
        baProcurementPlan.setBaContractList(baContracts);
        //入库信息
        BaDepotHead baDepotHead = new BaDepotHead();
        baDepotHead.setPurchaseId(id);
        //租户ID
        baDepotHead.setTenantId(SecurityUtils.getCurrComId());
        List<BaDepotHead> baDepotHeads = iBaDepotHeadService.selectBaDepotHead(baDepotHead);
        baProcurementPlan.setBaDepotHeadList(baDepotHeads);
        return baProcurementPlan;
    }

    /**
     * 查询采购计划列表
     *
     * @param baProcurementPlan 采购计划
     * @return 采购计划
     */
    @Override
    public List<BaProcurementPlan> selectBaProcurementPlanList(BaProcurementPlan baProcurementPlan)
    {
        List<BaProcurementPlan> baProcurementPlans = baProcurementPlanMapper.baProcurementPlanList(baProcurementPlan);
        for (BaProcurementPlan procurementPlan:baProcurementPlans) {
            //岗位信息
            String name = getName.getName(procurementPlan.getUserId());
            //发起人
            if(name.equals("") == false){
                procurementPlan.setUserName(sysUserMapper.selectUserById(procurementPlan.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                procurementPlan.setUserName(sysUserMapper.selectUserById(procurementPlan.getUserId()).getNickName());
            }
            //采购清单
            QueryWrapper<BaPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("plan_id",procurementPlan.getId());
            List<BaPurchaseOrder> baPurchaseOrders = baPurchaseOrderMapper.selectList(orderQueryWrapper);
            for (BaPurchaseOrder baPurchaseOrder:baPurchaseOrders) {
                //商品名称
                if(StringUtils.isNotEmpty(baPurchaseOrder.getGoodsId())){
                    baPurchaseOrder.setGoodsName(baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId()).getName());
                }
                //供应商
                if(StringUtils.isNotEmpty(baPurchaseOrder.getSupplierId())){
                    baPurchaseOrder.setSupplierName(baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId()).getName());
                }
            }
            //主体公司
            if(StringUtils.isNotEmpty(procurementPlan.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(procurementPlan.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    procurementPlan.setBelongCompanyName(sysDept.getDeptName());
                }
            }
            procurementPlan.setBaPurchaseOrders(baPurchaseOrders);
        }
        return baProcurementPlans;
    }

    /**
     * 新增采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    @Override
    public int insertBaProcurementPlan(BaProcurementPlan baProcurementPlan)
    {
        QueryWrapper<BaProcurementPlan> planQueryWrapper = new QueryWrapper<>();
        planQueryWrapper.eq("plan_name",baProcurementPlan.getPlanName());
        planQueryWrapper.eq("flag",0);
        BaProcurementPlan procurementPlan1 = baProcurementPlanMapper.selectOne(planQueryWrapper);
        if(StringUtils.isNotNull(procurementPlan1)){
            return 0;
        }
        baProcurementPlan.setId(getRedisIncreID.getId());
        baProcurementPlan.setCreateTime(DateUtils.getNowDate());
        baProcurementPlan.setUserId(SecurityUtils.getUserId());
        baProcurementPlan.setDeptId(SecurityUtils.getDeptId());
        baProcurementPlan.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        //采购单
        if(StringUtils.isNotNull(baProcurementPlan.getBaPurchaseOrders())){
            for (BaPurchaseOrder baPurchaseOrder:baProcurementPlan.getBaPurchaseOrders()) {
                baPurchaseOrder.setId(getRedisIncreID.getId());
                baPurchaseOrder.setCreateTime(DateUtils.getNowDate());
                baPurchaseOrder.setUserId(SecurityUtils.getUserId());
                baPurchaseOrder.setDeptId(SecurityUtils.getDeptId());
                baPurchaseOrder.setCreateBy(SecurityUtils.getUsername());
                baPurchaseOrder.setPlanId(baProcurementPlan.getId());
                baPurchaseOrderMapper.insertBaPurchaseOrder(baPurchaseOrder);
            }
        }
        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baProcurementPlan.getWorkState())) {
            baProcurementPlan.setWorkState("1");
            //默认审批流程已通过
            baProcurementPlan.setState(AdminCodeEnum.PROCUREMENTPLAN_STATUS_PASS.getCode());
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode(),SecurityUtils.getCurrComId());
        baProcurementPlan.setFlowId(flowId);
        int result = baProcurementPlanMapper.insertBaProcurementPlan(baProcurementPlan);
        if(result > 0){
            //判断业务归属公司是否是上海子公司
            if("2".equals(baProcurementPlan.getWorkState())) {
                BaProcurementPlan procurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baProcurementPlan.getId());
                BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(procurementPlan, AdminCodeEnum.PROCUREMENTPLAN_APPROVAL_CONTENT_INSERT.getCode());
                if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                    if (StringUtils.isNotNull(baProcurementPlan.getBaPurchaseOrders())) {
                        //回滚采购单
                        QueryWrapper<BaPurchaseOrder> queryWrapper = new QueryWrapper<>();
                        queryWrapper.eq("plan_id", procurementPlan.getId());
                        List<BaPurchaseOrder> baPurchaseOrders = baPurchaseOrderMapper.selectList(queryWrapper);
                        for (BaPurchaseOrder baPurchaseOrder : baPurchaseOrders) {
                            baPurchaseOrderMapper.deleteBaPurchaseOrderById(baPurchaseOrder.getId());
                        }
                    }
                    //回滚
                    baProcurementPlanMapper.deleteBaProcurementPlanById(procurementPlan.getId());
                    return -1;
                } else {
                    procurementPlan.setState(AdminCodeEnum.PROCUREMENTPLAN_STATUS_VERIFYING.getCode());
                    baProcurementPlanMapper.updateBaProcurementPlan(procurementPlan);
                }
            }
        }
        return result;
    }

    /**
     * 提交合同启动审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaProcurementPlan baProcurementPlan, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baProcurementPlan.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baProcurementPlan.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaProcurementPlan baProcurementPlan1 = this.selectBaProcurementPlanById(baProcurementPlan.getId());
        SysUser sysUser = iSysUserService.selectUserById(baProcurementPlan1.getUserId());
        baProcurementPlan1.setUserName(sysUser.getUserName());
        baProcurementPlan1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(baProcurementPlan1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.PROCUREMENTPLAN_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改采购计划
     *
     * @param baProcurementPlan 采购计划
     * @return 结果
     */
    @Override
    public int updateBaProcurementPlan(BaProcurementPlan baProcurementPlan)
    {

        //源数据
        BaProcurementPlan baProcurementPlan1 = baProcurementPlanMapper.selectBaProcurementPlanById(baProcurementPlan.getId());
        if(baProcurementPlan.getPlanName().equals(baProcurementPlan1.getPlanName()) == false){
            QueryWrapper<BaProcurementPlan> planQueryWrapper = new QueryWrapper<>();
            planQueryWrapper.eq("plan_name",baProcurementPlan.getPlanName());
            planQueryWrapper.eq("flag",0);
            BaProcurementPlan procurementPlan1 = baProcurementPlanMapper.selectOne(planQueryWrapper);
            if(StringUtils.isNotNull(procurementPlan1)){
                return 0;
            }
        }
        baProcurementPlan.setUpdateTime(DateUtils.getNowDate());
        baProcurementPlan.setUpdateBy(SecurityUtils.getUsername());
        int result = baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan);
        if(result > 0){
            BaProcurementPlan procurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baProcurementPlan.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",procurementPlan.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if(baProcessInstanceRelateds.size() > 0){
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(procurementPlan, AdminCodeEnum.PROCUREMENTPLAN_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                //数据回滚
                baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan1);
                return -1;
            }else {
                //清除采购计划下所有采购单
                QueryWrapper<BaPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("plan_id",baProcurementPlan.getId());
                List<BaPurchaseOrder> baPurchaseOrders = baPurchaseOrderMapper.selectList(orderQueryWrapper);
                for (BaPurchaseOrder baPurchaseOrder:baPurchaseOrders) {
                    baPurchaseOrderMapper.deleteBaPurchaseOrderById(baPurchaseOrder.getId());
                }
                 //重新新增采购单
                if(StringUtils.isNotNull(baProcurementPlan.getBaPurchaseOrders())){
                    for (BaPurchaseOrder baPurchaseOrder:baProcurementPlan.getBaPurchaseOrders()) {
                        baPurchaseOrder.setId(getRedisIncreID.getId());
                        baPurchaseOrder.setCreateTime(DateUtils.getNowDate());
                        baPurchaseOrder.setUserId(SecurityUtils.getUserId());
                        baPurchaseOrder.setDeptId(SecurityUtils.getDeptId());
                        baPurchaseOrder.setCreateBy(SecurityUtils.getUsername());
                        baPurchaseOrder.setPlanId(baProcurementPlan.getId());
                        baPurchaseOrderMapper.insertBaPurchaseOrder(baPurchaseOrder);
                    }
                }
                baProcurementPlan.setState(AdminCodeEnum.PROCUREMENTPLAN_STATUS_VERIFYING.getCode());
                baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan);
            }
        }
        return result;
    }

    /**
     * 批量删除采购计划
     *
     * @param ids 需要删除的采购计划ID
     * @return 结果
     */
    @Override
    public int deleteBaProcurementPlanByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids){
                BaProcurementPlan procurementPlan = this.selectBaProcurementPlanById(id);
                if(StringUtils.isNotNull(procurementPlan.getBaPurchaseOrders())){
                    for (BaPurchaseOrder baPurchaseOrder:procurementPlan.getBaPurchaseOrders()) {
                        baPurchaseOrderMapper.deleteBaPurchaseOrderById(baPurchaseOrder.getId());
                    }
                }
                procurementPlan.setFlag(1L);
                baProcurementPlanMapper.updateBaProcurementPlan(procurementPlan);
                //清除流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除采购计划信息
     *
     * @param id 采购计划ID
     * @return 结果
     */
    @Override
    public int deleteBaProcurementPlanById(String id)
    {
        return baProcurementPlanMapper.deleteBaProcurementPlanById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baProcurementPlan.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.PROCUREMENTPLAN_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baProcurementPlan.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baProcurementPlan.setState(AdminCodeEnum.PROCUREMENTPLAN_STATUS_WITHDRAW.getCode());
                baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan);
                String userId = String.valueOf(baProcurementPlan.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baProcurementPlan, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int association(BaProcurementPlan baProcurementPlan) {

        return baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan);
    }

    @Override
    public List<BaPurchaseOrder> dropDown(String projectId,String goodsId) {
        List<BaPurchaseOrder> list = new ArrayList<>();
        //查询项目下所有采购计划
        QueryWrapper<BaProcurementPlan> planQueryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(projectId)){
            planQueryWrapper.eq("project_id",projectId);
        }
        planQueryWrapper.eq("state",AdminCodeEnum.PROCUREMENTPLAN_STATUS_PASS.getCode());
        planQueryWrapper.eq("flag",0);
        List<BaProcurementPlan> baProcurementPlans = baProcurementPlanMapper.selectList(planQueryWrapper);
        if(baProcurementPlans.size() > 0){
            for (BaProcurementPlan baProcurementPlan:baProcurementPlans) {
                //查看所有的采购单
                QueryWrapper<BaPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("plan_id",baProcurementPlan.getId());
                if(StringUtils.isNotEmpty(goodsId)){
                    orderQueryWrapper.eq("goods_id",goodsId);
                }
                List<BaPurchaseOrder> purchaseOrders = baPurchaseOrderMapper.selectList(orderQueryWrapper);
                for (BaPurchaseOrder baPurchaseOrder:purchaseOrders) {
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId());
                    baPurchaseOrder.setMontageName(baProcurementPlan.getPlanName()+"-"+baGoods.getName()+"-"+baPurchaseOrder.getExcludingPrice());
                    list.add(baPurchaseOrder);
                }
            }
        }
        if(list.size() > 0){
            for (BaPurchaseOrder baPurchaseOrder:list) {
                //品名
                if(StringUtils.isNotEmpty(baPurchaseOrder.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId());
                    baPurchaseOrder.setGoodsName(baGoods.getName());
                }
                //供应商
                if(StringUtils.isNotEmpty(baPurchaseOrder.getSupplierId())){
                    BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId());
                    baPurchaseOrder.setSupplierName(baSupplier.getName());
                    baPurchaseOrder.setCompanyAbbreviation(baSupplier.getCompanyAbbreviation());
                }
                //采购计划仓库
                if(StringUtils.isNotEmpty(baPurchaseOrder.getPlanId())){
                    BaProcurementPlan baProcurementPlan = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                    baPurchaseOrder.setDepotId(baProcurementPlan.getDepotId());
                }
            }
        }
        return list;
    }

    @Override
    public List<BaPurchaseOrder> selectBaPurchaseOrderList(String projectId,String planId) {
        //采购清单
        List<BaPurchaseOrder> list = new ArrayList<>();

        //查询采购计划
        QueryWrapper<BaProcurementPlan> queryWrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(projectId)){
            queryWrapper.eq("project_id",projectId);
        }
        if(StringUtils.isNotEmpty(planId)){
            queryWrapper.eq("id",planId);
        }
        queryWrapper.eq("flag",0);
        List<BaProcurementPlan> baProcurementPlans = baProcurementPlanMapper.selectList(queryWrapper);
        for (BaProcurementPlan baProcurementPlan:baProcurementPlans) {

            //查询采购清单
            QueryWrapper<BaPurchaseOrder> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("plan_id",baProcurementPlan.getId());
            List<BaPurchaseOrder> purchaseOrders = baPurchaseOrderMapper.selectList(queryWrapper1);
            for (BaPurchaseOrder baPurchaseOrder:purchaseOrders) {
                //货源
                HashSet<String> set = new HashSet<>();
                //磅房数据
                QueryWrapper<BaPoundRoom> roomQueryWrapper = new QueryWrapper<>();
                roomQueryWrapper.eq("purchase_id",baPurchaseOrder.getId());
                List<BaPoundRoom> poundRoomList = baPoundRoomMapper.selectList(roomQueryWrapper);
                //已进厂车数
                baPurchaseOrder.setEnteringCars(poundRoomList.size());
                //已进厂数量
                BigDecimal enteringNumber = new BigDecimal(0);
                for (BaPoundRoom baPoundRoom:poundRoomList) {
                    if(baPoundRoom.getUnloadWeight() != null){
                        enteringNumber = enteringNumber.add(baPoundRoom.getUnloadWeight());
                    }
                    //货源信息
                    set.add(baPoundRoom.getSourceGoods());
                }
                baPurchaseOrder.setEnteringNumber(enteringNumber);
                //已确认磅房数据
                roomQueryWrapper = new QueryWrapper<>();
                roomQueryWrapper.eq("purchase_id",baPurchaseOrder.getId());
                roomQueryWrapper.eq("entering_state","1");
                List<BaPoundRoom> poundRooms = baPoundRoomMapper.selectList(roomQueryWrapper);
                //已确认车数
                baPurchaseOrder.setConfirmCars(poundRooms.size());
                //已确认数量
                BigDecimal confirmNumber = new BigDecimal(0);
                for (BaPoundRoom baPoundRoom:poundRooms) {
                    if(baPoundRoom.getUnloadWeight() != null){
                        confirmNumber = confirmNumber.add(baPoundRoom.getUnloadWeight());
                    }
                    //货源信息
                    //set.add(baPoundRoom.getSourceGoods());
                }
                //可勾选磅房数据
                roomQueryWrapper = new QueryWrapper<>();
                roomQueryWrapper.eq("purchase_id",baPurchaseOrder.getId());
                roomQueryWrapper.eq("entering_state","0");
                List<BaPoundRoom> baPoundRooms = baPoundRoomMapper.selectList(roomQueryWrapper);
                for (BaPoundRoom baPoundRoom:baPoundRooms) {
                    baPoundRoom.setPlanId(baPurchaseOrder.getPlanId());
                }
                baPurchaseOrder.setConfirmNumber(confirmNumber);
                baPurchaseOrder.setPoundRoomList(baPoundRooms);
                //采购计划名称
                if(StringUtils.isNotEmpty(baPurchaseOrder.getPlanId())){
                    BaProcurementPlan baProcurementPlan1 = baProcurementPlanMapper.selectBaProcurementPlanById(baPurchaseOrder.getPlanId());
                    baPurchaseOrder.setPlanName(baProcurementPlan1.getPlanName());
                }
                //商品名称
                if(StringUtils.isNotEmpty(baPurchaseOrder.getGoodsId())){
                    BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId());
                    baPurchaseOrder.setGoodsName(baGoods.getName());
                }
                //货源
                baPurchaseOrder.setSourceGoods(set);
                list.add(baPurchaseOrder);
            }
        }
        return list;
    }

    @Override
    public UR purchaseRequestName(String projectId, String type) {
        BaProject baProject = baProjectMapper.selectBaProjectById(projectId);
        String contractStartName = "";
        if ("1".equals(type)) {
            contractStartName = this.addname(baProject.getName(),baProject.getId());
        } else if ("2".equals(type)) {
            contractStartName = this.addname(baProject.getName() + "(采购)",baProject.getId());
        } else if ("3".equals(type)) {
            contractStartName = this.addname(baProject.getName() + "(销售)",baProject.getId());
        }
        return UR.ok().code(200).data("name", contractStartName);
    }

    @Override
    public int countBaProcurementPlanList(BaProcurementPlan baProcurementPlan) {
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        return baProcurementPlanMapper.countBaProcurementPlanList(baProcurementPlan);
    }

    @Override
    public List<BaProcurementPlan> selectBaProcurementPlanAuthorizedList(BaProcurementPlan baProcurementPlan) {
        List<BaProcurementPlan> baProcurementPlans = baProcurementPlanMapper.baProcurementPlanAuthorizedList(baProcurementPlan);
        for (BaProcurementPlan procurementPlan:baProcurementPlans) {
            //岗位信息
            String name = getName.getName(procurementPlan.getUserId());
            //发起人
            if(name.equals("") == false){
                procurementPlan.setUserName(sysUserMapper.selectUserById(procurementPlan.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                procurementPlan.setUserName(sysUserMapper.selectUserById(procurementPlan.getUserId()).getNickName());
            }
            //采购清单
            QueryWrapper<BaPurchaseOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("plan_id",procurementPlan.getId());
            List<BaPurchaseOrder> baPurchaseOrders = baPurchaseOrderMapper.selectList(orderQueryWrapper);
            for (BaPurchaseOrder baPurchaseOrder:baPurchaseOrders) {
                //商品名称
                if(StringUtils.isNotEmpty(baPurchaseOrder.getGoodsId())){
                    baPurchaseOrder.setGoodsName(baGoodsMapper.selectBaGoodsById(baPurchaseOrder.getGoodsId()).getName());
                }
                //供应商
                if(StringUtils.isNotEmpty(baPurchaseOrder.getSupplierId())){
                    baPurchaseOrder.setSupplierName(baSupplierMapper.selectBaSupplierById(baPurchaseOrder.getSupplierId()).getName());
                }
            }
            //主体公司
            if(StringUtils.isNotEmpty(procurementPlan.getBelongCompanyId())){
                SysDept sysDept = sysDeptMapper.selectDeptById(Long.valueOf(procurementPlan.getBelongCompanyId()));
                if(!ObjectUtils.isEmpty(sysDept)){
                    procurementPlan.setBelongCompanyName(sysDept.getDeptName());
                }
            }
            procurementPlan.setBaPurchaseOrders(baPurchaseOrders);
        }
        return baProcurementPlans;
    }

    @Override
    public int authorizedBaProcurementPlan(BaProcurementPlan baProcurementPlan) {
        return baProcurementPlanMapper.updateBaProcurementPlan(baProcurementPlan);
    }

    public String addname(String name,String projectId) {
        int indexOf = 0;
        String orderNameReplace = null;
        int temp = 0;
        String contractStartName = "";
        BaProcurementPlan baProcurementPlan = new BaProcurementPlan();
        baProcurementPlan.setPlanName(name);
        baProcurementPlan.setProjectId(projectId);
        baProcurementPlan.setFlag(0L);
        //租户ID
        baProcurementPlan.setTenantId(SecurityUtils.getCurrComId());
        List<BaProcurementPlan> baContractStartList = baProcurementPlanMapper.purchaseRequestName(baProcurementPlan);
        if (!CollectionUtils.isEmpty(baContractStartList)) {
            indexOf = baContractStartList.get(0).getPlanName().lastIndexOf("-");
            orderNameReplace = baContractStartList.get(0).getPlanName().substring(indexOf + 1);
            if ("0".equals(orderNameReplace.substring(0, 1))) {
                temp = (Integer.parseInt(orderNameReplace.substring(1, 2)) + 1);
                if (temp < 10) {
                    contractStartName = name + "-0" + temp;
                } else {
                    contractStartName = name + "-" + temp;
                }
            } else {
                if (StringUtils.isNumeric(orderNameReplace)) {
                    contractStartName = name + "-" + (Integer.valueOf(orderNameReplace) + 1);
                }
            }
        } else {
            contractStartName = name + "-0" + 1;
        }
        return contractStartName;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaProcurementPlan baProcurementPlan, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baProcurementPlan.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROCUREMENTPLAN.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"采购计划审批提醒",msgContent,baMessage.getType(),baProcurementPlan.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
