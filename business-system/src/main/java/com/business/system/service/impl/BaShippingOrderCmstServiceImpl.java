package com.business.system.service.impl;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.mapper.BaSampleAssayMapper;
import com.business.system.mapper.BaTransportCmstMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaShippingOrderCmstMapper;
import com.business.system.service.IBaShippingOrderCmstService;
import org.springframework.util.ObjectUtils;

/**
 * 中储智运发运单Service业务层处理
 *
 * @author single
 * @date 2023-08-21
 */
@Service
public class BaShippingOrderCmstServiceImpl extends ServiceImpl<BaShippingOrderCmstMapper, BaShippingOrderCmst> implements IBaShippingOrderCmstService
{
    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    @Autowired
    private BaSampleAssayMapper baSampleAssayMapper;

    /**
     * 查询中储智运发运单
     *
     * @param id 中储智运发运单ID
     * @return 中储智运发运单
     */
    @Override
    public BaShippingOrderCmst selectBaShippingOrderCmstById(String id)
    {
        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(id);
        if(StringUtils.isNotEmpty(baShippingOrderCmst.getYardId())){
            BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(baShippingOrderCmst.getYardId());
            baShippingOrderCmst.setBaTransportCmst(transportCmst);
        }
        //化验信息
        BaSampleAssay sampleAssay = baSampleAssayMapper.selectBaSampleAssayById(baShippingOrderCmst.getExperimentId());
        if(StringUtils.isNotNull(sampleAssay)){
            baShippingOrderCmst.setAssay(sampleAssay);
        }
        return baShippingOrderCmst;
    }

    /**
     * 查询中储智运发运单列表
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 中储智运发运单
     */
    @Override
    public List<BaShippingOrderCmst> selectBaShippingOrderCmstList(BaShippingOrderCmst baShippingOrderCmst)
    {
        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst);
        for (BaShippingOrderCmst baShippingOrderCmst1 :baShippingOrderCmsts) {
            if(StringUtils.isNotNull(baShippingOrderCmst1)){
                baShippingOrderCmst1.setAutomobileType("2");
            }
            BaTransportCmst baTransportCmst = baTransportCmstMapper.selectBaTransportCmstById(baShippingOrderCmst1.getYardId());
            if(StringUtils.isNotNull(baTransportCmst)){
                baShippingOrderCmst1.setBaTransportCmst(baTransportCmst);
            }
        }
        return baShippingOrderCmsts;
    }

    /**
     * 新增中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    @Override
    public int insertBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst)
    {
        baShippingOrderCmst.setCreateTime(DateUtils.getNowDate());
        return baShippingOrderCmstMapper.insertBaShippingOrderCmst(baShippingOrderCmst);
    }

    /**
     * 修改中储智运发运单
     *
     * @param baShippingOrderCmst 中储智运发运单
     * @return 结果
     */
    @Override
    public int updateBaShippingOrderCmst(BaShippingOrderCmst baShippingOrderCmst)
    {
        baShippingOrderCmst.setUpdateTime(DateUtils.getNowDate());
        return baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
    }

    @Override
    public int updateShippingOrderCmst(JSONObject baShippingOrderCmst) {
        System.out.println(baShippingOrderCmst);
        //司机身份证号附件正面
        JSONArray driverIDPhotoFronts = baShippingOrderCmst.getJSONArray("driverIDPhotoFront");
        String driverIDPhotoFront = "";
        if(driverIDPhotoFronts != null){
            if(driverIDPhotoFronts.size() > 0){
                for (Object object:driverIDPhotoFronts) {
                    driverIDPhotoFront = object.toString() + "," + driverIDPhotoFront;
                }
            }
        }
        //司机身份证号附件反面
        JSONArray driverIDPhotoBacks = baShippingOrderCmst.getJSONArray("driverIDPhotoBack");
        String driverIDPhotoBack = "";
        if(driverIDPhotoBacks != null) {
            if (driverIDPhotoBacks.size() > 0) {
                for (Object o : driverIDPhotoBacks) {
                    driverIDPhotoBack = o.toString() + "," + driverIDPhotoBack;
                }
            }
        }
        //司机驾驶证证附件
        JSONArray driverLicensePhotos = baShippingOrderCmst.getJSONArray("driverLicensePhoto");
        String driverLicensePhoto = "";
        if(driverLicensePhotos != null){
            if(driverLicensePhotos.size() > 0){
                for (Object o:driverLicensePhotos) {
                    driverLicensePhoto = o.toString()+","+driverLicensePhoto;
                }
            }
        }
        //司机从业资格信息证附件
        JSONArray driverOccupationPhotos = baShippingOrderCmst.getJSONArray("driverOccupationPhoto");
        String driverOccupationPhoto = "";
        if(driverOccupationPhotos != null){
            if(driverOccupationPhotos.size() > 0){
                for (Object o:driverOccupationPhotos) {
                    driverOccupationPhoto = o.toString()+","+driverOccupationPhoto;
                }
            }
        }
        //车辆行驶证附件
        JSONArray truckLicensePhotos = baShippingOrderCmst.getJSONArray("TruckLicensePhoto");
        String truckLicensePhoto = "";
        if(truckLicensePhotos != null){
            if(truckLicensePhotos.size() > 0){
                for (Object o:truckLicensePhotos) {
                    truckLicensePhoto = o.toString() + "," + truckLicensePhoto;
                }
            }
        }
        //道路运输证附件
        JSONArray truckRoadPhotos = baShippingOrderCmst.getJSONArray("TruckRoadPhoto");
        String truckRoadPhoto = "";
        if(truckRoadPhotos != null){
            if(truckRoadPhotos.size() > 0){
                for (Object o:truckRoadPhotos) {
                    truckRoadPhoto = o.toString() + "," + truckRoadPhoto;
                }
            }
        }
        //挂车行驶证附件
        JSONArray truckLicensePhototrailers = baShippingOrderCmst.getJSONArray("TruckLicensePhototrailer");
        String truckLicensePhototrailer = "";
        if(truckLicensePhototrailers != null){
            if(truckLicensePhototrailers.size() > 0){
                for (Object o:truckLicensePhototrailers) {
                    truckLicensePhototrailer = o.toString()+","+truckLicensePhototrailer;
                }
            }
        }
        //发货照片
        JSONArray shipmentPhotoss = baShippingOrderCmst.getJSONArray("shipmentPhotos");
        String shipmentPhotos = "";
        if(shipmentPhotoss != null){
            if(shipmentPhotoss.size() > 0){
                for (Object o:shipmentPhotoss) {
                    shipmentPhotos = o.toString()+","+shipmentPhotos;
                }
            }
        }
        //人车照片
        JSONArray carPhotos = baShippingOrderCmst.getJSONArray("carPhoto");
        String carPhoto = "";
        if(carPhotos != null){
            if(carPhotos.size() > 0){
                for (Object o:carPhotos) {
                    carPhoto = o.toString()+","+carPhoto;
                }
            }
        }
        //收货照片
        JSONArray receiptImagess = baShippingOrderCmst.getJSONArray("receiptImages");
        String receiptImages = "";
        if(receiptImagess != null){
            if(receiptImagess.size() > 0){
                for (Object o:receiptImagess) {
                    receiptImages = o.toString()+","+receiptImages;
                }
            }
        }
        //司机名称
        String driverName = baShippingOrderCmst.getString("driverName");
        //司机联系方式
        String contactNumber = baShippingOrderCmst.getString("contactNumber");
        BaShippingOrderCmst shippingOrderCmst = JSONObject.toJavaObject((JSONObject) JSON.toJSON(baShippingOrderCmst), BaShippingOrderCmst.class);
        shippingOrderCmst.setTruckLicenseNumbertrailer(baShippingOrderCmst.getString("TruckLicenseNumbertrailer"));
        shippingOrderCmst.setUpdateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(driverName)){
            shippingOrderCmst.setDriverUserName(driverName);
        }
        if(StringUtils.isNotEmpty(contactNumber)){
            shippingOrderCmst.setDriverMobile(contactNumber);
        }
        if(driverIDPhotoFront.equals("") == false){
            shippingOrderCmst.setDriverIDPhotoFront(driverIDPhotoFront.substring(0,driverIDPhotoFront.length()-1));
        }else {
            shippingOrderCmst.setDriverIDPhotoFront(null);
        }
        if(driverIDPhotoBack.equals("") == false){
            shippingOrderCmst.setDriverIDPhotoBack(driverIDPhotoBack.substring(0,driverIDPhotoBack.length()-1));
        }else {
            shippingOrderCmst.setDriverIDPhotoBack(null);
        }
        if(driverLicensePhoto.equals("") == false){
            shippingOrderCmst.setDriverLicensePhoto(driverLicensePhoto.substring(0,driverLicensePhoto.length()-1));
        }else {
            shippingOrderCmst.setDriverLicensePhoto(null);
        }
        if(driverOccupationPhoto.equals("") == false){
            shippingOrderCmst.setDriverOccupationPhoto(driverOccupationPhoto.substring(0,driverOccupationPhoto.length()-1));
        }else {
            shippingOrderCmst.setDriverOccupationPhoto(null);
        }
        if(truckLicensePhoto.equals("") == false){
            shippingOrderCmst.setTruckLicensePhoto(truckLicensePhoto.substring(0,truckLicensePhoto.length()-1));
        }else {
            shippingOrderCmst.setTruckLicensePhoto(null);
        }
        if(truckRoadPhoto.equals("") == false){
            shippingOrderCmst.setTruckRoadPhoto(truckRoadPhoto.substring(0,truckRoadPhoto.length()-1));
        }else {
            shippingOrderCmst.setTruckRoadPhoto(null);
        }
        if(truckLicensePhototrailer.equals("") == false){
            shippingOrderCmst.setTruckLicensePhototrailer(truckLicensePhototrailer.substring(0,truckLicensePhototrailer.length()-1));
        }else {
            shippingOrderCmst.setTruckLicensePhototrailer(null);
        }
        if(shipmentPhotos.equals("") == false){
            shippingOrderCmst.setShipmentPhotos(shipmentPhotos.substring(0,shipmentPhotos.length()-1));
        }else {
            shippingOrderCmst.setShipmentPhotos(null);
        }
        if(carPhoto.equals("") == false){
            shippingOrderCmst.setCarPhoto(carPhoto.substring(0,carPhoto.length()-1));
        }else {
            shippingOrderCmst.setCarPhoto(null);
        }
        if(receiptImages.equals("") == false){
            shippingOrderCmst.setReceiptImages(receiptImages.substring(0,receiptImages.length()-1));
        }else {
            shippingOrderCmst.setReceiptImages(null);
        }
        shippingOrderCmst.setId(shippingOrderCmst.getOrderId());
        //存在发货照片存入发货时间
        if(StringUtils.isNotEmpty(shipmentPhotos)){
            shippingOrderCmst.setDeliveryTime(DateUtils.getNowDate());
        }
        //存在收货照片存入收货时间
        if(StringUtils.isNotEmpty(receiptImages)){
            shippingOrderCmst.setReceivingTime(DateUtils.getNowDate());
        }
        return baShippingOrderCmstMapper.updateBaShippingOrderCmst(shippingOrderCmst);
    }

    /**
     * 批量删除中储智运发运单
     *
     * @param ids 需要删除的中储智运发运单ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingOrderCmstByIds(String[] ids)
    {
        return baShippingOrderCmstMapper.deleteBaShippingOrderCmstByIds(ids);
    }

    /**
     * 删除中储智运发运单信息
     *
     * @param id 中储智运发运单ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingOrderCmstById(String id)
    {
        return baShippingOrderCmstMapper.deleteBaShippingOrderCmstById(id);
    }


    /**
     * 回单确认通知
     * @param object
     * @return
     */
    @Override
    public int receiptShippingOrder(JSONObject object) {
        System.out.println(object);
        String orderId = object.getString("orderId");
        BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(orderId);
        if(!ObjectUtils.isEmpty(baShippingOrderCmst)){
            baShippingOrderCmst.setTonnage(object.getBigDecimal("tonnage")); //吨位
            baShippingOrderCmst.setSettleMoney(object.getBigDecimal("settleMoney")); //结算金额
            baShippingOrderCmst.setUpdateTime(DateUtils.getNowDate());
        }
        return baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
    }
}
