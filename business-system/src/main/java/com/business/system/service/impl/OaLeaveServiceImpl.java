package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 请假申请Service业务层处理
 *
 * @author ljb
 * @date 2023-11-11
 */
@Service
public class OaLeaveServiceImpl extends ServiceImpl<OaLeaveMapper, OaLeave> implements IOaLeaveService
{
    private static final Logger log = LoggerFactory.getLogger(OaLeaveServiceImpl.class);
    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询请假申请
     *
     * @param id 请假申请ID
     * @return 请假申请
     */
    @Override
    public OaLeave selectOaLeaveById(String id)
    {
        OaLeave leave = oaLeaveMapper.selectOaLeaveById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",leave.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        leave.setProcessInstanceId(processInstanceIds);
        //申请人名称
        if(leave.getUserId() != null){
            leave.setUserName(sysUserMapper.selectUserById(leave.getUserId()).getNickName());
        }
        //申请部门
        if(leave.getDeptId() != null){
            leave.setDeptName(sysDeptMapper.selectDeptById(leave.getDeptId()).getDeptName());
        }
        return leave;
    }

    /**
     * 查询请假申请列表
     *
     * @param oaLeave 请假申请
     * @return 请假申请
     */
    @Override
    public List<OaLeave> selectOaLeaveList(OaLeave oaLeave)
    {
        List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeaveList(oaLeave);
        if(!CollectionUtils.isEmpty(oaLeaves)){
            for(OaLeave leave : oaLeaves){
                //申请人名称
                if(leave.getUserId() != null){
                    leave.setUserName(sysUserMapper.selectUserById(leave.getUserId()).getNickName());
                }
                //申请部门
                if(leave.getDeptId() != null){
                    leave.setDeptName(sysDeptMapper.selectDeptById(leave.getDeptId()).getDeptName());
                }
            }
        }
        return oaLeaves;
    }

    /**
     * 新增请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    @Override
    public int insertOaLeave(OaLeave oaLeave)
    {
        //判断保存数据是否存在
        if(StringUtils.isNotEmpty(oaLeave.getId()) && "0".equals(oaLeave.getSubmitState())){
            return oaLeaveMapper.updateOaLeave(oaLeave);
        }
        //判断保存数据是否提交
        if(StringUtils.isNotEmpty(oaLeave.getId()) && "1".equals(oaLeave.getSubmitState())){
            OaLeave leave = oaLeaveMapper.selectOaLeaveById(oaLeave.getId());
            leave.setFlag(1L);
            oaLeaveMapper.updateOaLeave(leave);
        }
        oaLeave.setId(getRedisIncreID.getId());
        oaLeave.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaLeave.setTenantId(SecurityUtils.getCurrComId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_LEAVE.getCode(),SecurityUtils.getCurrComId());
        oaLeave.setFlowId(flowId);
        int result = oaLeaveMapper.insertOaLeave(oaLeave);
        if(result > 0 && "1".equals(oaLeave.getSubmitState())){
            OaLeave leave = oaLeaveMapper.selectOaLeaveById(oaLeave.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(leave, AdminCodeEnum.OALEAVE_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaLeaveMapper.deleteOaLeaveById(leave.getId());
                return 0;
            }else {
                leave.setState(AdminCodeEnum.OALEAVE_STATUS_VERIFYING.getCode());
                oaLeaveMapper.updateOaLeave(leave);
            }
        }
        return result;
    }

    /**
     * 提交请假申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaLeave leave, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(leave.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_LEAVE.getCode(), AdminCodeEnum.TASK_TYPE_OA_LEAVE.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(leave.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_LEAVE.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        OaLeave oaLeave = this.selectOaLeaveById(leave.getId());
        SysUser sysUser = iSysUserService.selectUserById(leave.getUserId());
        oaLeave.setUserName(sysUser.getNickName());
        oaLeave.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaLeave, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OALEAVE_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    @Override
    public int updateOaLeave(OaLeave oaLeave)
    {
        //原数据
        OaLeave oaLeave1 = oaLeaveMapper.selectOaLeaveById(oaLeave.getId());

        oaLeave.setUpdateTime(DateUtils.getNowDate());
        int result = oaLeaveMapper.updateOaLeave(oaLeave);
        if(result > 0 && "1".equals(oaLeave.getSubmitState())){
            OaLeave leave = oaLeaveMapper.selectOaLeaveById(oaLeave.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", leave.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(leave, AdminCodeEnum.OALEAVE_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaLeaveMapper.updateOaLeave(oaLeave1);
                return 0;
            }else {
                leave.setState(AdminCodeEnum.OALEAVE_STATUS_VERIFYING.getCode());
                oaLeaveMapper.updateOaLeave(leave);
            }
        }
        return result;
    }

    /**
     * 批量删除请假申请
     *
     * @param ids 需要删除的请假申请ID
     * @return 结果
     */
    @Override
    public int deleteOaLeaveByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaLeave leave = oaLeaveMapper.selectOaLeaveById(id);
                leave.setFlag(1L);
                oaLeaveMapper.updateOaLeave(leave);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除请假申请信息
     *
     * @param id 请假申请ID
     * @return 结果
     */
    @Override
    public int deleteOaLeaveById(String id)
    {
        return oaLeaveMapper.deleteOaLeaveById(id);
    }


    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaLeave leave = oaLeaveMapper.selectOaLeaveById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",leave.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OALEAVE_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(leave.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                leave.setState(AdminCodeEnum.OALEAVE_STATUS_WITHDRAW.getCode());
                oaLeaveMapper.updateOaLeave(leave);
                String userId = String.valueOf(leave.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(leave, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_LEAVE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaLeave oaLeave, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaLeave.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_LEAVE.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"请假申请审批提醒",msgContent,"oa",oaLeave.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
