package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 取样化验Service业务层处理
 *
 * @author ljb
 * @date 2024-01-06
 */
@Service
public class BaSampleAssayServiceImpl extends ServiceImpl<BaSampleAssayMapper, BaSampleAssay> implements IBaSampleAssayService
{
    private static final Logger log = LoggerFactory.getLogger(BaSampleAssayServiceImpl.class);
    @Autowired
    private BaSampleAssayMapper baSampleAssayMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;

    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;

    @Autowired
    private BaTransportCmstMapper baTransportCmstMapper;

    /**
     * 查询取样化验
     *
     * @param id 取样化验ID
     * @return 取样化验
     */
    @Override
    public BaSampleAssay selectBaSampleAssayById(String id)
    {
        BaSampleAssay sampleAssay = baSampleAssayMapper.selectBaSampleAssayById(id);
        //项目信息
        if(StringUtils.isNotEmpty(sampleAssay.getProjectId())){
            BaProject baProject = baProjectMapper.selectBaProjectById(sampleAssay.getProjectId());
            sampleAssay.setProjectName(baProject.getName());
        }
        //运单信息
        List<waybillDTO> list = new ArrayList<>();
        if(StringUtils.isNotEmpty(sampleAssay.getThdno())){
            String[] split = sampleAssay.getThdno().split(",");
            for (String thdno:split) {
                BaShippingOrder baShippingOrder = new BaShippingOrder();
                baShippingOrder.setThdno(thdno);
                List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                list.add(waybillDTOList.get(0));
            }
            sampleAssay.setWaybillDTOList(list);
        }
        //取样包含化验信息
        QueryWrapper<BaSampleAssay> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",sampleAssay.getId());
        queryWrapper.eq("flag",0);
        queryWrapper.eq("type","2");
        List<BaSampleAssay> baSampleAssays1 = baSampleAssayMapper.selectList(queryWrapper);
        if(baSampleAssays1.size() > 0){
            sampleAssay.setAssay(baSampleAssays1.get(0));
        }
        //化验取样信息
        if(StringUtils.isNotEmpty(sampleAssay.getRelationId())){
            BaSampleAssay sample = baSampleAssayMapper.selectBaSampleAssayById(sampleAssay.getRelationId());
            sampleAssay.setSample(sample);
        }
        //取样人名称
        if(StringUtils.isNotEmpty(sampleAssay.getSamplePeople())){
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(sampleAssay.getSamplePeople()));
            sampleAssay.setSamplePeopleName(user.getNickName());
        }
        //检测人名称
        if(StringUtils.isNotEmpty(sampleAssay.getDetectionPeople())){
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(sampleAssay.getDetectionPeople()));
            sampleAssay.setDetectionPeopleName(user.getNickName());
        }
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",sampleAssay.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        sampleAssay.setProcessInstanceId(processInstanceIds);
        //发起人
        SysUser user = sysUserMapper.selectUserById(sampleAssay.getUserId());
        user.setUserName(user.getNickName());
        //岗位名称
        String name = getName.getName(user.getUserId());
        if(name.equals("") == false){
            user.setPostName(name.substring(0,name.length()-1));
        }
        sampleAssay.setUser(user);
        return sampleAssay;
    }

    /**
     * 查询取样化验列表
     *
     * @param baSampleAssay 取样化验
     * @return 取样化验
     */
    @Override
    public List<BaSampleAssay> selectBaSampleAssayList(BaSampleAssay baSampleAssay)
    {
        List<BaSampleAssay> baSampleAssays = baSampleAssayMapper.selectBaSampleAssayList(baSampleAssay);
        for (BaSampleAssay sampleAssay:baSampleAssays) {
            //岗位信息
            String name = getName.getName(sampleAssay.getUserId());
            //发起人
            if (name.equals("") == false) {
                sampleAssay.setUserName(sysUserMapper.selectUserById(sampleAssay.getUserId()).getNickName() + "-" + name.substring(0, name.length() - 1));
            } else {
                sampleAssay.setUserName(sysUserMapper.selectUserById(sampleAssay.getUserId()).getNickName());
            }
            //取样人名称
            if(StringUtils.isNotEmpty(sampleAssay.getSamplePeople())){
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(sampleAssay.getSamplePeople()));
                sampleAssay.setSamplePeopleName(user.getNickName());
            }
            //检测人名称
            if(StringUtils.isNotEmpty(sampleAssay.getDetectionPeople())){
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(sampleAssay.getDetectionPeople()));
                sampleAssay.setDetectionPeopleName(user.getNickName());
            }
            //化验信息
            QueryWrapper<BaSampleAssay> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",sampleAssay.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.eq("type","2");
            List<BaSampleAssay> baSampleAssays1 = baSampleAssayMapper.selectList(queryWrapper);
            if(baSampleAssays1.size() > 0){
                //检测人名称
                if(StringUtils.isNotEmpty(baSampleAssays1.get(0).getDetectionPeople())){
                    SysUser user = sysUserMapper.selectUserById(Long.valueOf(baSampleAssays1.get(0).getDetectionPeople()));
                    baSampleAssays1.get(0).setDetectionPeopleName(user.getNickName());
                }
                sampleAssay.setAssay(baSampleAssays1.get(0));
            }
            //取样信息
            if(StringUtils.isNotEmpty(sampleAssay.getRelationId())){
                BaSampleAssay sample = baSampleAssayMapper.selectBaSampleAssayById(sampleAssay.getRelationId());
                //取样人名称
                if(StringUtils.isNotEmpty(sample.getSamplePeople())){
                    SysUser user = sysUserMapper.selectUserById(Long.valueOf(sample.getSamplePeople()));
                    sample.setSamplePeopleName(user.getNickName());
                }
                sampleAssay.setSample(sample);
            }
            //商品名称
            if(StringUtils.isNotEmpty(sampleAssay.getDetectionType())){
                    if(StringUtils.isNotEmpty(sampleAssay.getGoodsSource())){
                         //查询货源商品
                        BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                        baTransportAutomobile.setSupplierNo(sampleAssay.getGoodsSource());
                        List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                        if(!CollectionUtils.isEmpty(sourceGoodsDTOS)){
                            sampleAssay.setGoodsName(sourceGoodsDTOS.get(0).getGoodsName());
                        }
                    }
            }

        }
        return baSampleAssays;
    }

    /**
     * 新增取样
     *
     * @param baSampleAssay 取样
     * @return 结果
     */
    @Override
    public int insertBaSampleAssay(BaSampleAssay baSampleAssay)
    {
        baSampleAssay.setId(baSampleAssay.getSampleCode());
        baSampleAssay.setCreateTime(DateUtils.getNowDate());
        baSampleAssay.setUserId(SecurityUtils.getUserId());
        baSampleAssay.setDeptId(SecurityUtils.getDeptId());
        baSampleAssay.setCreateBy(SecurityUtils.getUsername());
        //租户ID
        baSampleAssay.setTenantId(SecurityUtils.getCurrComId());
        baSampleAssay.setType("1");
        //查询取样是否存在化验信息
        if(StringUtils.isNotEmpty(baSampleAssay.getRelationId())){
            QueryWrapper<BaSampleAssay> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",baSampleAssay.getRelationId());
            queryWrapper.eq("flag",0);
            List<BaSampleAssay> baSampleAssays = baSampleAssayMapper.selectList(queryWrapper);
            if(baSampleAssays.size() > 0){
                return -1;
            }
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode(),SecurityUtils.getCurrComId());
        baSampleAssay.setFlowId(flowId);
        //运单编号
        if(StringUtils.isNotEmpty(baSampleAssay.getHeadType())){
            if("1".equals(baSampleAssay.getHeadType())){
                //货源信息绑定
                if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                    //查找货源信息
                    BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                    baTransportAutomobile.setSupplierNo(baSampleAssay.getGoodsSource());
                    List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                    if("1".equals(sourceGoodsDTOS.get(0).getType()) || "3".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(sourceGoodsDTOS.get(0).getSupplierNo());
                        baTransportAutomobile1.setSampleType("1");
                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                    }else if("2".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(sourceGoodsDTOS.get(0).getSupplierNo());
                        transportCmst.setSampleType("1");
                        baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                }
                if(StringUtils.isNotEmpty(baSampleAssay.getThdno())){
                    String[] split = baSampleAssay.getThdno().split(",");
                    for (String thdno:split) {
                        //查询运单信息标记
                        BaShippingOrder baShippingOrder = new BaShippingOrder();
                        baShippingOrder.setThdno(thdno);
                        List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                        if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                            BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(thdno);
                            baShippingOrder1.setExperimentId("1");
                            baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                        }else if("2".equals(waybillDTOList.get(0).getType())){
                            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(thdno);
                            baShippingOrderCmst.setExperimentId("1");
                            baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                        }
                    }
                }
            }else if("2".equals(baSampleAssay.getHeadType())){
                if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                    //查找货源信息
                    BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                    baTransportAutomobile.setSupplierNo(baSampleAssay.getGoodsSource());
                    List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                    //货源信息
                    if("1".equals(sourceGoodsDTOS.get(0).getType()) || "3".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(sourceGoodsDTOS.get(0).getSupplierNo());
                        baTransportAutomobile1.setSampleType("2");
                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                    }else if("2".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(sourceGoodsDTOS.get(0).getSupplierNo());
                        transportCmst.setSampleType("2");
                        baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                    //查询运单信息标记
                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                    baShippingOrder.setSupplierNo(sourceGoodsDTOS.get(0).getSupplierNo());
                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                    for (waybillDTO waybillDTO:waybillDTOList) {
                        if("1".equals(waybillDTO.getType()) || "3".equals(waybillDTO.getType())){
                            BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(waybillDTO.getThdno());
                            baShippingOrder1.setExperimentId("1");
                            baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                        }else if("2".equals(waybillDTO.getType())){
                            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getThdno());
                            baShippingOrderCmst.setExperimentId("1");
                            baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                        }
                    }
                }
            }

        }

        int result = baSampleAssayMapper.insertBaSampleAssay(baSampleAssay);
        if(result > 0){
            BaSampleAssay sample = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(sample, AdminCodeEnum.SAMPLE_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baSampleAssayMapper.deleteBaSampleAssayById(sample.getId());
                return 0;
            }else {
                sample.setState(AdminCodeEnum.SAMPLE_STATUS_VERIFYING.getCode());
                baSampleAssayMapper.updateBaSampleAssay(sample);
            }
        }
        return result;
    }

    /**
     * 提交取样申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaSampleAssay sample, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(sample.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode());
        //查询取样立项ID
        if(!ObjectUtils.isEmpty(sample)){
            BaProject baProject = baProjectMapper.selectBaProjectById(sample.getProjectId());
            if(!ObjectUtils.isEmpty(baProject)){
                map.put("projectId", baProject.getId());
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(sample.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaSampleAssay sample1 = this.selectBaSampleAssayById(sample.getId());
        SysUser sysUser = iSysUserService.selectUserById(sample1.getUserId());
        sample1.setUserName(sysUser.getNickName());
        sample1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(sample1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.SAMPLE_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改取样
     *
     * @param baSampleAssay 取样化验
     * @return 结果
     */
    @Override
    public int updateBaSampleAssay(BaSampleAssay baSampleAssay)
    {
        //原数据
        BaSampleAssay sample1 = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());

        baSampleAssay.setUpdateTime(DateUtils.getNowDate());

        //运单编号
        if(StringUtils.isNotEmpty(baSampleAssay.getHeadType())){
            if("1".equals(baSampleAssay.getHeadType())){
                //货源信息绑定
                if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                    //查找货源信息
                    BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                    baTransportAutomobile.setSupplierNo(baSampleAssay.getGoodsSource());
                    List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                    if("1".equals(sourceGoodsDTOS.get(0).getType()) || "3".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(sourceGoodsDTOS.get(0).getSupplierNo());
                        baTransportAutomobile1.setSampleType("1");
                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                    }else if("2".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(sourceGoodsDTOS.get(0).getSupplierNo());
                        transportCmst.setSampleType("1");
                        baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                }
                if(StringUtils.isNotEmpty(baSampleAssay.getThdno())){
                    String[] split = baSampleAssay.getThdno().split(",");
                    for (String thdno:split) {
                        //查询运单信息标记
                        BaShippingOrder baShippingOrder = new BaShippingOrder();
                        baShippingOrder.setThdno(thdno);
                        List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                        if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                            BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(thdno);
                            baShippingOrder1.setExperimentId("1");
                            baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                        }else if("2".equals(waybillDTOList.get(0).getType())){
                            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(thdno);
                            baShippingOrderCmst.setExperimentId("1");
                            baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                        }
                    }
                }
            }else if("2".equals(baSampleAssay.getHeadType())){
                if(StringUtils.isNotEmpty(baSampleAssay.getGoodsSource())){
                    //查找货源信息
                    BaTransportAutomobile baTransportAutomobile = new BaTransportAutomobile();
                    baTransportAutomobile.setSupplierNo(baSampleAssay.getGoodsSource());
                    List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
                    //货源信息
                    if("1".equals(sourceGoodsDTOS.get(0).getType()) || "3".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(sourceGoodsDTOS.get(0).getSupplierNo());
                        baTransportAutomobile1.setSampleType("2");
                        baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                    }else if("2".equals(sourceGoodsDTOS.get(0).getType())){
                        BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(sourceGoodsDTOS.get(0).getSupplierNo());
                        transportCmst.setSampleType("2");
                        baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                    }
                    //查询运单信息标记
                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                    baShippingOrder.setSupplierNo(sourceGoodsDTOS.get(0).getSupplierNo());
                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                    for (waybillDTO waybillDTO:waybillDTOList) {
                        if("1".equals(waybillDTO.getType()) || "3".equals(waybillDTO.getType())){
                            BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(waybillDTO.getThdno());
                            baShippingOrder1.setExperimentId("1");
                            baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                        }else if("2".equals(waybillDTO.getType())){
                            BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getThdno());
                            baShippingOrderCmst.setExperimentId("1");
                            baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                        }
                    }
                }
            }

        }

        int result = baSampleAssayMapper.updateBaSampleAssay(baSampleAssay);
        if(result > 0){
            BaSampleAssay sample = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", sample.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(sample, AdminCodeEnum.SAMPLE_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baSampleAssayMapper.updateBaSampleAssay(sample1);
                return 0;
            }else {
                sample.setState(AdminCodeEnum.SAMPLE_STATUS_VERIFYING.getCode());
                baSampleAssayMapper.updateBaSampleAssay(sample);
            }
        }
        return result;
    }


    /**
     * 新增化验
     * @param baSampleAssay 化验
     * @return
     */
    @Override
    public int insertAssay(BaSampleAssay baSampleAssay) {
        baSampleAssay.setId(getRedisIncreID.getId());
        baSampleAssay.setCreateTime(DateUtils.getNowDate());
        //租户ID为空
        if(StringUtils.isEmpty(baSampleAssay.getTenantId())){
            //租户ID
            baSampleAssay.setTenantId(SecurityUtils.getCurrComId());
        }
        //判断来源是否为H5
        if("1".equals(baSampleAssay.getSource())){
            if(StringUtils.isNotEmpty(baSampleAssay.getDetectionPeople())){
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(baSampleAssay.getDetectionPeople()));
                baSampleAssay.setUserId(user.getUserId());
                baSampleAssay.setDeptId(user.getDeptId());
                baSampleAssay.setCreateBy(user.getUserName());
            }
        }else {
            baSampleAssay.setUserId(SecurityUtils.getUserId());
            baSampleAssay.setDeptId(SecurityUtils.getDeptId());
            baSampleAssay.setCreateBy(SecurityUtils.getUsername());
        }
        baSampleAssay.setType("2");
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode(),baSampleAssay.getTenantId());
        baSampleAssay.setFlowId(flowId);
        int result = baSampleAssayMapper.insertBaSampleAssay(baSampleAssay);
        if(result > 0){
            BaSampleAssay assay = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
            //来源
            assay.setSource(baSampleAssay.getSource());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(assay, AdminCodeEnum.ASSAY_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baSampleAssayMapper.deleteBaSampleAssayById(assay.getId());
                return 0;
            }else {
                assay.setState(AdminCodeEnum.ASSAY_STATUS_VERIFYING.getCode());
                baSampleAssayMapper.updateBaSampleAssay(assay);
            }
        }
        return result;
    }

    @Override
    public int updateAssay(BaSampleAssay baSampleAssay) {
        //原数据
        BaSampleAssay assay = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());

        baSampleAssay.setUpdateTime(DateUtils.getNowDate());
        int result = baSampleAssayMapper.updateBaSampleAssay(baSampleAssay);
        if(result > 0){
            BaSampleAssay assay1 = baSampleAssayMapper.selectBaSampleAssayById(baSampleAssay.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", assay1.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances1(assay1, AdminCodeEnum.ASSAY_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                baSampleAssayMapper.updateBaSampleAssay(assay);
                return 0;
            }else {
                assay1.setState(AdminCodeEnum.ASSAY_STATUS_VERIFYING.getCode());
                baSampleAssayMapper.updateBaSampleAssay(assay1);
            }
        }
        return result;
    }

    /**
     * 提交取样申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances1(BaSampleAssay assay, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(assay.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode());
        //查询取样立项ID
        if(!ObjectUtils.isEmpty(assay)){
            //根据化验查取样
            BaSampleAssay baSampleAssay = baSampleAssayMapper.selectBaSampleAssayById(assay.getRelationId());
            if(!ObjectUtils.isEmpty(baSampleAssay)){
                BaProject baProject = baProjectMapper.selectBaProjectById(baSampleAssay.getProjectId());
                if(!ObjectUtils.isEmpty(baProject)){
                    map.put("projectId", baProject.getId());
                }
            }
        }
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(assay.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaSampleAssay sample1 = this.selectBaSampleAssayById(assay.getId());
        SysUser sysUser = iSysUserService.selectUserById(sample1.getUserId());
        sample1.setUserName(sysUser.getNickName());
        sample1.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(sample1, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances1(instancesRuntimeDTO,assay);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances1(ProcessInstancesRuntimeDTO processInstancesRuntime, BaSampleAssay assay) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //判断数据来源是否为H5
        SysUser user = sysUserMapper.selectUserById(Long.valueOf(assay.getDetectionPeople()));
        if("1".equals(assay.getSource())){
            UserInfo userInfo = new UserInfo();
            userInfo.setId(String.valueOf(user.getUserId()));
            userInfo.setName(user.getUserName());
            userInfo.setNickName(user.getNickName());
            //根据部门ID获取部门信息
            SysDept sysDept = iSysDeptService.selectDeptById(user.getDeptId());
            if(!ObjectUtils.isEmpty(sysDept)){
                userInfo.setDepartment(String.valueOf(sysDept.getDeptName()));
            }
            startProcessInstanceDTO.setStartUserInfo(userInfo);
        }else {
            //设置发起人
            UserInfo userInfo = iSysUserService.getCurrentUserInfo();
            startProcessInstanceDTO.setStartUserInfo(userInfo);
        }
        startProcessInstanceDTO.setTenantId(assay.getTenantId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            if("1".equals(assay.getSource())){
                LoginUser loginUser = new LoginUser();
                loginUser.setUserId(user.getUserId());
                loginUser.setUser(user);
                redisCache.lock(Constants.LOGIN_USER, loginUser, lockTime);
            }else {
                redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            }
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.ASSAY_APPROVAL_CONTENT_UPDATE.getCode())){
                //来源H5
                if("1".equals(assay.getSource())){
                    baProcessInstanceRelated.setUpdateBy(user.getUserName());
                    baProcessInstanceRelated.setCreateBy(user.getUserName());
                }else {
                    baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                }
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                //来源H5
                if("1".equals(assay.getSource())){
                    baProcessInstanceRelated.setCreateBy(user.getUserName());
                }else {
                    baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
                }
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 批量删除取样化验
     *
     * @param ids 需要删除的取样化验ID
     * @return 结果
     */
    @Override
    public int deleteBaSampleAssayByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaSampleAssay sampleAssay = baSampleAssayMapper.selectBaSampleAssayById(id);
                sampleAssay.setFlag(1L);
                baSampleAssayMapper.updateBaSampleAssay(sampleAssay);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id",id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if(baProcessInstanceRelateds.size() > 0){
                    for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除取样化验信息
     *
     * @param id 取样化验ID
     * @return 结果
     */
    @Override
    public int deleteBaSampleAssayById(String id)
    {
        return baSampleAssayMapper.deleteBaSampleAssayById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaSampleAssay sample = baSampleAssayMapper.selectBaSampleAssayById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",sample.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.SAMPLE_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(sample.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                sample.setState(AdminCodeEnum.SAMPLE_STATUS_WITHDRAW.getCode());
                baSampleAssayMapper.updateBaSampleAssay(sample);
                //判断出入库
                if(StringUtils.isNotEmpty(sample.getHeadType())){
                    //入库
                    if("1".equals(sample.getHeadType())){
                        //释放对应运单状态
                        if(StringUtils.isNotEmpty(sample.getThdno())){
                            String[] split = sample.getThdno().split(",");
                            for (String thdno:split) {
                                //查询运单信息标记
                                BaShippingOrder baShippingOrder = new BaShippingOrder();
                                baShippingOrder.setThdno(thdno);
                                List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                                if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                                    BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(thdno);
                                    baShippingOrder1.setExperimentId("0");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                                }else if("2".equals(waybillDTOList.get(0).getType())){
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(thdno);
                                    baShippingOrderCmst.setExperimentId("0");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                }
                            }
                        }
                    }else if("2".equals(sample.getHeadType())){
                        if(StringUtils.isNotEmpty(sample.getGoodsSource())){
                            //查询运单信息标记
                            BaShippingOrder baShippingOrder = new BaShippingOrder();
                            baShippingOrder.setSupplierNo(sample.getGoodsSource());
                            List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                            for (waybillDTO waybillDTO:waybillDTOList) {
                                if("1".equals(waybillDTO.getType()) || "3".equals(waybillDTO.getType())){
                                    BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectBaShippingOrderById(waybillDTO.getThdno());
                                    baShippingOrder1.setExperimentId("0");
                                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder1);
                                }else if("2".equals(waybillDTOList.get(0).getType())){
                                    BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getThdno());
                                    baShippingOrderCmst.setExperimentId("0");
                                    baShippingOrderCmstMapper.updateBaShippingOrderCmst(baShippingOrderCmst);
                                }
                            }
                        }
                    }
                }
                //释放对应的货源
                if(StringUtils.isNotEmpty(sample.getGoodsSource())){
                    //查询货源下所有运单
                    BaShippingOrder baShippingOrder = new BaShippingOrder();
                    baShippingOrder.setSupplierNo(sample.getGoodsSource());
                    List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
                    if("1".equals(waybillDTOList.get(0).getType()) || "3".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                        shippingOrderQueryWrapper.eq("experiment_id",0);
                        List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                        if(waybillDTOList.size() == shippingOrders.size()){
                            BaTransportAutomobile baTransportAutomobile1 = baTransportAutomobileMapper.selectBaTransportAutomobileById(waybillDTOList.get(0).getSupplierNo());
                            baTransportAutomobile1.setSampleType("0");
                            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile1);
                        }
                    }else if("2".equals(waybillDTOList.get(0).getType())){
                        QueryWrapper<BaShippingOrderCmst> cmstQueryWrapper = new QueryWrapper<>();
                        cmstQueryWrapper.eq("experiment_id",0);
                        List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectList(cmstQueryWrapper);
                        if(waybillDTOList.size() == baShippingOrderCmsts.size()){
                            BaTransportCmst transportCmst = baTransportCmstMapper.selectBaTransportCmstById(waybillDTOList.get(0).getSupplierNo());
                            transportCmst.setSampleType("0");
                            baTransportCmstMapper.updateBaTransportCmst(transportCmst);
                        }
                    }
                }
                String userId = String.valueOf(sample.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(sample, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int revokeAssay(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaSampleAssay assay = baSampleAssayMapper.selectBaSampleAssayById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",assay.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.ASSAY_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(assay.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                assay.setState(AdminCodeEnum.ASSAY_STATUS_WITHDRAW.getCode());
                baSampleAssayMapper.updateBaSampleAssay(assay);
                String userId = String.valueOf(assay.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(assay, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaSampleAssay sample, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(sample.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        if("1".equals(sample.getType())){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_SAMPLE.getCode());
        }else  if("2".equals(sample.getType())){
            baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ASSAY.getCode());
        }
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            if("1".equals(sample.getType())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"取样审批提醒",msgContent,baMessage.getType(),sample.getId(),baMessage.getBusinessType());
            }else if("2".equals(sample.getType())){
                baMessageService.messagePush(sysUser.getPhoneCode(),"化验审批提醒",msgContent,baMessage.getType(),sample.getId(),baMessage.getBusinessType());
            }
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
