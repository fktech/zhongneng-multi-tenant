package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaContractForward;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IContractForwardApprovalService;
import com.business.system.service.workflow.IContractStartApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 合同结转审批结果:审批通过
 */
@Service("contractForwardApprovalContent:processApproveResult:pass")
@Slf4j
public class ContractForwardApprovalAgreeServiceImpl extends IContractForwardApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional(value = "transactionManager")
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaContractForward baContractForward = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CONTRACTFORWARD_STATUS_PASS.getCode());
        return result;
    }
}
