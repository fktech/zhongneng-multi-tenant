package com.business.system.service.workflow;

import com.business.common.core.domain.UR;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.dto.BaContractStartDTO;
import com.business.system.domain.vo.BaDriveAi;
import com.business.system.domain.vo.BaOngoingContractStartVO;
import com.business.system.domain.vo.DataLayoutStatVO;

import java.util.List;

/**
 * 立项管理Service接口
 *
 * @author ljb
 * @date 2022-12-02
 */
public interface IBaDriveAiService
{
    /**
     * 立项项目依照业务类型分类查询
     *
     * @param
     * @return 结果
     */
    public List<BaDriveAi> businesstype();

    /**
     * 客商数据统计
     */
    public UR counts();

    /**
     * 事业部排名
     */
    public List<DataLayoutStatVO> departmentRanking(Long deptId, String type);

    /**
     * 进行中的业务
     */
    public List<BaOngoingContractStartVO> contractStartList(BaContractStartDTO baContractStartDTO);
}
