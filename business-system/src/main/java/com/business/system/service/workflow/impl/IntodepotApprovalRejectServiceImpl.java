package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaInvoice;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IIntodepotApprovalService;
import com.business.system.service.workflow.IInvoiceApprovalService;
import org.springframework.stereotype.Service;


/**
 * 入库申请审批结果:审批拒绝
 */
@Service("intodepotApprovalContent:processApproveResult:reject")
public class IntodepotApprovalRejectServiceImpl extends IIntodepotApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.INTODEPOT_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaDepotHead checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}