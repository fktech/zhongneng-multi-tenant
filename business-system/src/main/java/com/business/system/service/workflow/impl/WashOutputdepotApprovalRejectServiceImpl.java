package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaDepotHead;
import com.business.system.domain.BaWashHead;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOutputdepotApprovalService;
import com.business.system.service.workflow.WashOutputdepotApprovalService;
import org.springframework.stereotype.Service;


/**
 * 洗煤出库申请审批结果:审批拒绝
 */
@Service("washOutputdepotApprovalContent:processApproveResult:reject")
public class WashOutputdepotApprovalRejectServiceImpl extends WashOutputdepotApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.WASHIOUTPUTDEPOT_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaWashHead checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
