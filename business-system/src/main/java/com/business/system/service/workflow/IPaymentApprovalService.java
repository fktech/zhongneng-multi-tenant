package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 付款审批接口 服务类
 * @date: 2022/12/27 11:53
 */
@Service
public class IPaymentApprovalService {

    @Autowired
    protected BaPaymentMapper baPaymentMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;
    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaPayment baPayment = new BaPayment();
        baPayment.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baPayment = baPaymentMapper.selectBaPaymentById(baPayment.getId());
        String userId = String.valueOf(baPayment.getUserId());
        if(AdminCodeEnum.PAYMENT_STATUS_PASS.getCode().equals(status)){
            if(!ObjectUtils.isEmpty(baPayment)){
                String transportId = baPayment.getTransportId();
                if(StringUtils.isNotEmpty(transportId)){
                    String[] transportIdArray = transportId.split(",");
                    if(transportIdArray.length > 0){
                        for(String transport : transportIdArray){
                            BaTransport baTransport = baTransportMapper.selectBaTransportById(transport);
                            if(!ObjectUtils.isEmpty(baTransport)){
                                baTransport.setPaymentStatus("1"); //已付款
                                baTransportMapper.updateBaTransport(baTransport);
                            }
                        }
                    }
                }
            }
            //审批通过时间
            baPayment.setPassTime(DateUtils.getNowDate());

            //全局编号
            //全局编号关系表新增数据
            if(StringUtils.isNotEmpty(baPayment.getGlobalNumber())){
                BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                baGlobalNumber.setId(getRedisIncreID.getId());
                baGlobalNumber.setBusinessId(baPayment.getId());
                baGlobalNumber.setCode(baPayment.getGlobalNumber());
                baGlobalNumber.setHierarchy("3");
                baGlobalNumber.setType("6");
                baGlobalNumber.setStartId(baPayment.getStartId());
                baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baPayment, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.PAYMENT_STATUS_REJECT.getCode().equals(status)){
            //拒绝
            if(!ObjectUtils.isEmpty(baPayment)){
                String transportId = baPayment.getTransportId();
                if(StringUtils.isNotEmpty(transportId)){
                    String[] transportIdArray = transportId.split(",");
                    if(transportIdArray.length > 0){
                        for(String transport : transportIdArray){
                            BaTransport baTransport = baTransportMapper.selectBaTransportById(transport);
                            if(!ObjectUtils.isEmpty(baTransport)){
                                baTransport.setPitchOn("0"); // 未选中
                                baTransportMapper.updateBaTransport(baTransport);
                            }
                        }
                    }
                }
                BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baPayment.getStartId());
                if(!ObjectUtils.isEmpty(baContractStart)){
                    //完结的合同启动以及待汇款中不更改原有状态
                    if(baContractStart.getStartType().equals("4") == false&&baContractStart.getStartType().equals("4") == false) {
                        baContractStart.setStartType("2");
                    }
                }
                baContractStartMapper.updateBaContractStart(baContractStart);
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baPayment, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getDescription(), MessageConstant.APPROVAL_REJECT));
            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baPayment, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baPayment.setState(status);
        baPaymentMapper.updateById(baPayment);

        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaPayment baPayment, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baPayment.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PAYMENT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"付款审批提醒",msgContent,baMessage.getType(),baPayment.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaPayment checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaPayment baPayment = baPaymentMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baPayment == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PAYMENT_001);
        }
        if (!AdminCodeEnum.PAYMENT_STATUS_VERIFYING.getCode().equals(baPayment.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PAYMENT_002);
        }
        return baPayment;
    }
}
