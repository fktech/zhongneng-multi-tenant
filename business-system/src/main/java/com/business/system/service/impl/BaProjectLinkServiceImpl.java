package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaProjectLinkMapper;
import com.business.system.domain.BaProjectLink;
import com.business.system.service.IBaProjectLinkService;

/**
 * 立项授权Service业务层处理
 *
 * @author ljb
 * @date 2023-06-05
 */
@Service
public class BaProjectLinkServiceImpl extends ServiceImpl<BaProjectLinkMapper, BaProjectLink> implements IBaProjectLinkService
{
    @Autowired
    private BaProjectLinkMapper baProjectLinkMapper;

    /**
     * 查询立项授权
     *
     * @param id 立项授权ID
     * @return 立项授权
     */
    @Override
    public BaProjectLink selectBaProjectLinkById(String id)
    {
        return baProjectLinkMapper.selectBaProjectLinkById(id);
    }

    /**
     * 查询立项授权列表
     *
     * @param baProjectLink 立项授权
     * @return 立项授权
     */
    @Override
    public List<BaProjectLink> selectBaProjectLinkList(BaProjectLink baProjectLink)
    {
        return baProjectLinkMapper.selectBaProjectLinkList(baProjectLink);
    }

    /**
     * 新增立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    @Override
    public int insertBaProjectLink(BaProjectLink baProjectLink)
    {
        baProjectLink.setCreateTime(DateUtils.getNowDate());
        return baProjectLinkMapper.insertBaProjectLink(baProjectLink);
    }

    /**
     * 修改立项授权
     *
     * @param baProjectLink 立项授权
     * @return 结果
     */
    @Override
    public int updateBaProjectLink(BaProjectLink baProjectLink)
    {
        baProjectLink.setUpdateTime(DateUtils.getNowDate());
        return baProjectLinkMapper.updateBaProjectLink(baProjectLink);
    }

    /**
     * 批量删除立项授权
     *
     * @param ids 需要删除的立项授权ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectLinkByIds(String[] ids)
    {
        return baProjectLinkMapper.deleteBaProjectLinkByIds(ids);
    }

    /**
     * 删除立项授权信息
     *
     * @param id 立项授权ID
     * @return 结果
     */
    @Override
    public int deleteBaProjectLinkById(String id)
    {
        return baProjectLinkMapper.deleteBaProjectLinkById(id);
    }

}
