package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaAttendance;
import com.business.system.domain.dto.ClockCountDTO;

/**
 * 打卡Service接口
 *
 * @author single
 * @date 2023-10-11
 */
public interface IBaAttendanceService
{
    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    public BaAttendance selectBaAttendanceById(String id);

    /**
     * 查询打卡列表
     *
     * @param baAttendance 打卡
     * @return 打卡集合
     */
    public List<BaAttendance> selectBaAttendanceList(BaAttendance baAttendance);

    /**
     * 新增打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    public int insertBaAttendance(BaAttendance baAttendance);

    /**
     * 修改打卡
     *
     * @param baAttendance 打卡
     * @return 结果
     */
    public int updateBaAttendance(BaAttendance baAttendance);

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的打卡ID
     * @return 结果
     */
    public int deleteBaAttendanceByIds(String[] ids);

    /**
     * 删除打卡信息
     *
     * @param id 打卡ID
     * @return 结果
     */
    public int deleteBaAttendanceById(String id);

    /**
     * 打卡统计
     * @param baAttendance
     * @return
     */
    public ClockCountDTO clockCount(BaAttendance baAttendance);



}
