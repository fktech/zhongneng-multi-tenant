package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaPurchaseOrderMapper;
import com.business.system.domain.BaPurchaseOrder;
import com.business.system.service.IBaPurchaseOrderService;

/**
 * 采购清单Service业务层处理
 *
 * @author ljb
 * @date 2023-08-31
 */
@Service
public class BaPurchaseOrderServiceImpl extends ServiceImpl<BaPurchaseOrderMapper, BaPurchaseOrder> implements IBaPurchaseOrderService
{
    @Autowired
    private BaPurchaseOrderMapper baPurchaseOrderMapper;

    /**
     * 查询采购清单
     *
     * @param id 采购清单ID
     * @return 采购清单
     */
    @Override
    public BaPurchaseOrder selectBaPurchaseOrderById(String id)
    {
        return baPurchaseOrderMapper.selectBaPurchaseOrderById(id);
    }

    /**
     * 查询采购清单列表
     *
     * @param baPurchaseOrder 采购清单
     * @return 采购清单
     */
    @Override
    public List<BaPurchaseOrder> selectBaPurchaseOrderList(BaPurchaseOrder baPurchaseOrder)
    {
        return baPurchaseOrderMapper.selectBaPurchaseOrderList(baPurchaseOrder);
    }

    /**
     * 新增采购清单
     *
     * @param baPurchaseOrder 采购清单
     * @return 结果
     */
    @Override
    public int insertBaPurchaseOrder(BaPurchaseOrder baPurchaseOrder)
    {
        baPurchaseOrder.setCreateTime(DateUtils.getNowDate());
        return baPurchaseOrderMapper.insertBaPurchaseOrder(baPurchaseOrder);
    }

    /**
     * 修改采购清单
     *
     * @param baPurchaseOrder 采购清单
     * @return 结果
     */
    @Override
    public int updateBaPurchaseOrder(BaPurchaseOrder baPurchaseOrder)
    {
        baPurchaseOrder.setUpdateTime(DateUtils.getNowDate());
        return baPurchaseOrderMapper.updateBaPurchaseOrder(baPurchaseOrder);
    }

    /**
     * 批量删除采购清单
     *
     * @param ids 需要删除的采购清单ID
     * @return 结果
     */
    @Override
    public int deleteBaPurchaseOrderByIds(String[] ids)
    {
        return baPurchaseOrderMapper.deleteBaPurchaseOrderByIds(ids);
    }

    /**
     * 删除采购清单信息
     *
     * @param id 采购清单ID
     * @return 结果
     */
    @Override
    public int deleteBaPurchaseOrderById(String id)
    {
        return baPurchaseOrderMapper.deleteBaPurchaseOrderById(id);
    }

}
