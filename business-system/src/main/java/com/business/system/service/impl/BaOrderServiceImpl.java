package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.*;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.domain.vo.OrderStatVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 订单Service业务层处理
 *
 * @author ljb
 * @date 2022-12-09
 */
@Service
public class BaOrderServiceImpl extends ServiceImpl<BaOrderMapper, BaOrder> implements IBaOrderService
{
    private static final Logger log = LoggerFactory.getLogger(BaOrderServiceImpl.class);

    @Autowired
    private BaOrderMapper baOrderMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaGoodsMapper baGoodsMapper;

    @Autowired
    private BaContractMapper baContractMapper;

    @Autowired
    private BaSupplierMapper baSupplierMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaTransportMapper baTransportMapper;

    @Autowired
    private BaBankMapper baBankMapper;

    @Autowired
    private BaSettlementMapper baSettlementMapper;

    @Autowired
    private BaSettlementDetailMapper baSettlementDetailMapper;

    @Autowired
    private BaCompanyMapper baCompanyMapper;

    @Autowired
    private BaProjectMapper baProjectMapper;

    @Autowired
    private BaPaymentMapper baPaymentMapper;

    @Autowired
    private BaCollectionMapper baCollectionMapper;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public BaOrder selectBaOrderById(String id)
    {
       BaOrder baOrder = baOrderMapper.selectBaOrderById(id);
        //收货单位（用煤单位）
        if(StringUtils.isNotEmpty(baOrder.getEnterpriseId())){
           BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baOrder.getEnterpriseId());
            baOrder.setEnterpriseName(baEnterprise.getName());
        }
        //商品信息
        if(StringUtils.isNotEmpty(baOrder.getGoodsId())){
           BaGoods baGoods = baGoodsMapper.selectBaGoodsById(baOrder.getGoodsId());
           baOrder.setGoodsName(baGoods.getName());
        }
        //发起人
        SysUser sysUser = sysUserMapper.selectUserById(baOrder.getUserId());
        sysUser.setUserName(sysUser.getNickName());
        baOrder.setUser(sysUser);
        //合同关联数据
        if(StringUtils.isNotEmpty(baOrder.getCompanyId())){
           BaContract baContract = baContractMapper.selectBaContractById(baOrder.getCompanyId());
           baOrder.setCompanyName(baContract.getName());
           baOrder.setContractAmount(baContract.getAmount());
           baOrder.setContractTotal(baContract.getContractTotal().longValue());
           //用煤单位名称
           if(StringUtils.isNotEmpty(baContract.getEnterpriseId())){
              BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baContract.getEnterpriseId());
              baOrder.setContractEnterpriseName(baEnterprise.getName());
              baOrder.setContractEnterpriseId(baEnterprise.getId());
           }
            //供应商
            if(StringUtils.isNotEmpty(baContract.getSupplierId())){
               BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baContract.getSupplierId());
               baOrder.setContractSupplierName(baSupplier.getName());
               baOrder.setContractSupplierId(baSupplier.getId());
            }
        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id",baOrder.getId());
        queryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.ORDER_STATUS_VERIFYING.getCode());
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baOrder.setProcessInstanceId(processInstanceIds);

        return baOrder;
    }

    /**
     * 查询订单列表
     *
     * @param baOrder 订单
     * @return 订单
     */
    @Override
    @DataScope(deptAlias = "o",userAlias = "o")
    public List<BaOrder> selectBaOrderList(BaOrder baOrder)
    {
       List<BaOrder> baOrders = baOrderMapper.selectBaOrderList(baOrder);
        for (BaOrder order:baOrders) {
        if(StringUtils.isNotEmpty(order.getCompanyId())){
            //查看合同信息
           BaContract baContract = baContractMapper.selectBaContractById(order.getCompanyId());
           order.setCompanyName(baContract.getName());
        }
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(order.getUserId());
            order.setUserName(sysUser.getNickName());
            //查看采购合同
            if(StringUtils.isNotEmpty(order.getPurchaseCompanyId())){
                //查看合同信息
                BaContract baContract = baContractMapper.selectBaContractById(order.getPurchaseCompanyId());
                order.setCompanyName(baContract.getName());
            }
            //收货单位（用煤单位）
            if(StringUtils.isNotEmpty(order.getEnterpriseId())){
                BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(order.getEnterpriseId());
                order.setEnterpriseName(baEnterprise.getName());
            }
            //商品信息
            if(StringUtils.isNotEmpty(order.getGoodsId())){
                BaGoods baGoods = baGoodsMapper.selectBaGoodsById(order.getGoodsId());
                order.setGoodsName(baGoods.getName());
            }
        }
        return baOrders;
    }

    /**
     * 新增订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    @Override
    public int insertBaOrder(BaOrder baOrder)
    {
        BaContract baContract = null;
        List<BaOrder> baOrders = null;
        String orderName = "";
        //判断合同类型
        if(StringUtils.isNotEmpty(baOrder.getPurchaseCompanyId())){
            baContract = baContractMapper.selectBaContractById(baOrder.getPurchaseCompanyId());
            QueryWrapper<BaOrder> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("purchase_company_id", baContract.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.orderByDesc("create_time");
            baOrders = baOrderMapper.selectList(queryWrapper);
        } else {
            //查询销售合同
            baContract = baContractMapper.selectBaContractById(baOrder.getCompanyId());
            BaOrder baOrder1 = new BaOrder();
            baOrder1.setFlag(0);
            baOrder1.setCompanyId(baContract.getId());
            baOrder1.setSaleOrderType("true");
            baOrders = baOrderMapper.selectBaOrderList(baOrder1);
        }
        if(!CollectionUtils.isEmpty(baOrders)){
            String orderNameReplace = baOrders.get(0).getOrderName().replace(baContract.getName() + "-", "");
            if("0".equals(orderNameReplace.substring(0,1))){
                int temp = (Integer.parseInt(orderNameReplace.substring(1,2)) + 1);
                if(temp < 10){
                    orderName = baContract.getName() + "-0" + temp;
                } else {
                    orderName = baContract.getName() + "-" + temp;
                }
            } else {
                if(StringUtils.isNumeric(orderNameReplace)){
                    orderName = baContract.getName() + "-" + (Integer.valueOf(orderNameReplace) + 1);
                }
            }
        } else {
            orderName = baContract.getName() + "-0" + 1;
        }
        baOrder.setId(getRedisIncreID.getId());
        baOrder.setOrderName(orderName);
        //判断如果既有销售合同，又有采购合同，则订单类型为4,既是销售订单又是采购订单
        if(StringUtils.isNotEmpty(baOrder.getCompanyId()) && StringUtils.isNotEmpty(baOrder.getPurchaseCompanyId())){
            baOrder.setOrderType("4");
        } else {
            if(StringUtils.isNotEmpty(baOrder.getPurchaseCompanyId())){
                baOrder.setOrderType("2");
            } else {
                baOrder.setOrderType("1");
            }
        }
        baOrder.setConfirmStatus("待确认");
        baOrder.setCreateTime(DateUtils.getNowDate());
        baOrder.setCreateBy(SecurityUtils.getUsername());
        baOrder.setUserId(SecurityUtils.getUserId());
        baOrder.setDeptId(SecurityUtils.getDeptId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACT.getCode(),SecurityUtils.getCurrComId());
        baOrder.setFlowId(flowId);
        Integer result = baOrderMapper.insertBaOrder(baOrder);
        if(result > 0){
           BaOrder order = baOrderMapper.selectBaOrderById(baOrder.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(order, AdminCodeEnum.ORDER_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                baOrderMapper.deleteBaOrderById(order.getId());
                return 0;
            }else {
            BaOrder baOrder2 = baOrderMapper.selectBaOrderById(baOrder.getId());
            baOrder2.setState(AdminCodeEnum.ORDER_STATUS_VERIFYING.getCode());
            baOrderMapper.updateBaOrder(baOrder2);
            }
        }
        return 1;
    }

    /**
     * 提交订单审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaOrder order, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(order.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(order.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode());
        //获取流程实例关联的业务对象
        BaOrder baOrder = baOrderMapper.selectBaOrderById(order.getId());
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        SysUser sysUser = iSysUserService.selectUserById(baOrder.getUserId());
        baOrder.setUserName(sysUser.getUserName());
        baOrder.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baOrder));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.ORDER_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改订单
     *
     * @param baOrder 订单
     * @return 结果
     */
    @Override
    public int updateBaOrder(BaOrder baOrder)
    {
        baOrder.setUpdateTime(DateUtils.getNowDate());
        baOrder.setUpdateBy(SecurityUtils.getUsername());
        //判断如果既有销售合同，又有采购合同，则订单类型为4,既是销售订单又是采购订单
        if(StringUtils.isNotEmpty(baOrder.getCompanyId()) && StringUtils.isNotEmpty(baOrder.getPurchaseCompanyId())){
            baOrder.setOrderType("4");
        } else {
            if(StringUtils.isNotEmpty(baOrder.getPurchaseCompanyId())){
                baOrder.setOrderType("2");
            } else if(StringUtils.isNotEmpty(baOrder.getCompanyId())){
                baOrder.setOrderType("1");
            }
        }
        Integer result = baOrderMapper.updateBaOrder(baOrder);
        if(result > 0){
        BaOrder order = baOrderMapper.selectBaOrderById(baOrder.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(order, AdminCodeEnum.ORDER_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                order.setState(AdminCodeEnum.ORDER_STATUS_VERIFYING.getCode());
                order.setCreateTime(baOrder.getUpdateTime());
                baOrderMapper.updateBaOrder(order);
            }
        }
        return result;
    }

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    @Override
    public int deleteBaOrderByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
               BaOrder baOrder = baOrderMapper.selectBaOrderById(id);
               baOrder.setFlag(1);
               baOrderMapper.updateBaOrder(baOrder);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteBaOrderById(String id)
    {
        return baOrderMapper.deleteBaOrderById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            //查询订单信息
           BaOrder baOrder = baOrderMapper.selectBaOrderById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baOrder.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.ORDER_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baOrder.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //订单流程审批状态修改
                baOrder.setState(AdminCodeEnum.ORDER_STATUS_WITHDRAW.getCode());
                baOrderMapper.updateBaOrder(baOrder);
                String userId = String.valueOf(baOrder.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baOrder, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }


    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaOrder baOrder, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baOrder.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_ORDER.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    @Override
    public int complete(String id) {
        if(StringUtils.isNotEmpty(id)){
            BaOrder baOrder = baOrderMapper.selectBaOrderById(id);
            baOrder.setOrderState("2");
            baOrderMapper.updateBaOrder(baOrder);
        }else {
            return 0;
        }
        return 1;
    }

    @Override
    public OrderStatVO statOrderInfo(BaOrder baOrder) {
        //查询所有采购订单
        List<BaOrder> baOrders = baOrderMapper.selectBaOrderList(baOrder);
        int orderTotals = 0;
        int runtimeOrderTotals = 0;
        double purchaseVolume = 0;
        double amountPaid = 0;
        if(!CollectionUtils.isEmpty(baOrders)){
            orderTotals = baOrders.size();
        }
        //进行中订单
        baOrder.setOrderState("1");
        baOrders = baOrderMapper.selectBaOrderList(baOrder);
        if(!CollectionUtils.isEmpty(baOrders)){
            runtimeOrderTotals = baOrders.size();
        }
        //设置合同类型
        OrderStatVO orderStatVO = new OrderStatVO();
        //采购订单进度统计
        List<BaOrder> baOrderList = null;
        baOrderList = this.statPurchaseProgress(baOrder);
        if(!CollectionUtils.isEmpty(baOrderList)){
            for(BaOrder baOrder1 : baOrderList) {
                if(baOrder1.getPurchaseVolume() != null){
                    purchaseVolume = purchaseVolume + Double.valueOf(baOrder1.getPurchaseVolume());
                }
                if(baOrder1.getAmountPaid() != null){
                    amountPaid = amountPaid + baOrder1.getAmountPaid().doubleValue();
                }
            }
            orderStatVO.setPurchaseVolume(String.valueOf(purchaseVolume));
            orderStatVO.setPurchasePayment(new BigDecimal(amountPaid).divide(new BigDecimal(10000),20,BigDecimal.ROUND_HALF_UP));
            orderStatVO.setOrderTotals(orderTotals);
            orderStatVO.setRuntimeOrderTotals(runtimeOrderTotals);
        }

        return orderStatVO;
    }

    @Override
    public List<BaOrder> statPurchaseProgress(BaOrder baOrder) {
        baOrder.setSettlementDetailType("2"); //统计实付金额
        List<BaOrder> baOrders = baOrderMapper.selectStatOrderInfoList(baOrder);
        //合同ID
        String companyId = "";
        if(!CollectionUtils.isEmpty(baOrders)){
            for(BaOrder baOrder1 : baOrders){
                //根据订单类型获取关联供应商
                if("2".equals(baOrder.getOrderType())){
                    companyId = baOrder1.getPurchaseCompanyId();
                } else if("1".equals(baOrder.getOrderType())){
                    companyId = baOrder1.getCompanyId();
                }
                QueryWrapper<BaBank> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id", companyId);
                List<BaBank> baBanks = baBankMapper.selectList(queryWrapper);
                if(!CollectionUtils.isEmpty(baBanks)){
                    for(BaBank baBank : baBanks){
                        if("供应商".equals(baBank.getCompanyType())){
                            //查询供应商
                            BaSupplier baSupplier = baSupplierMapper.selectBaSupplierById(baBank.getCompanyName());
                            if(!ObjectUtils.isEmpty(baSupplier)){
                                baOrder1.setContractSupplierName(baSupplier.getName());
                            }
                        } else if("托盘公司".equals(baBank.getCompanyType())){
                            BaCompany baCompany = baCompanyMapper.selectBaCompanyById(baBank.getCompanyName());
                            if(!ObjectUtils.isEmpty(baCompany)){
                                baOrder1.setPalletCompany(baCompany.getName());
                            }
                        } else if("终端企业".equals(baBank.getCompanyType())){
                            BaEnterprise baEnterprise = baEnterpriseMapper.selectBaEnterpriseById(baBank.getCompanyName());
                            if(!ObjectUtils.isEmpty(baEnterprise)){
                                baOrder1.setContractEnterpriseName(baEnterprise.getName());
                            }
                        }
                    }
                }

                //统计应付金额
                baOrder1.setAmount(getAmmountPaidAll(companyId, "1", baOrder1)); //应付金额
                baOrder1.setAmountPaid(getAmmountAll(companyId, baOrder.getOrderType(), baOrder1)); //已付金额

                //待付金额
                if(baOrder1.getAmount() != null && baOrder1.getAmountPaid() != null){
                    baOrder1.setAmountToBePaid(baOrder1.getAmount().subtract(baOrder1.getAmountPaid()));
                }

                //获取项目信息
                BaContract baContract = baContractMapper.selectBaContractById(companyId);
                if(!ObjectUtils.isEmpty(baContract)){
                    BaProject baProject = baProjectMapper.selectBaProjectById(baContract.getProjectId());
                    if(!ObjectUtils.isEmpty(baProject)){
                        //年化利率
                        baOrder1.setAnnualization(baProject.getAnnualization());
                        //服务周期
                        baOrder1.setServiceCycle(baProject.getServiceCycle());
                    }
                }
                //获取今年天数
                Calendar calOne = Calendar.getInstance();
                int year = calOne.get(Calendar.YEAR);
                Calendar calTwo = new GregorianCalendar(year, 11, 31);
                int days = calTwo.get(Calendar.DAY_OF_YEAR);
                //预计利息计算公式：服务周期*订单金额*年化利率/365
                //订单金额
                double amountValue = 0;
                if(baOrder1.getAmount() != null){
                    amountValue = baOrder1.getAmount().doubleValue();
                }
                //年化利率
                double annualizationValue = 0;
                if(baOrder1.getAnnualization() != null){
                    annualizationValue = baOrder1.getAnnualization().doubleValue();
                }
                if(!ObjectUtils.isEmpty(baOrder1.getServiceCycle())){
                    baOrder1.setEstimatedInterest(new BigDecimal(baOrder1.getServiceCycle()*amountValue*annualizationValue/days));
                }

                //订单付款状态
                if(baOrder1.getAmountToBePaid() != null && baOrder1.getAmountToBePaid().intValue() == 0){
                    //已付款
                    baOrder1.setOrderPaymentState("已付款");
                } else if(baOrder1.getAmountPaid() != null && baOrder1.getAmountPaid().intValue() == 0){
                    //未付款
                    baOrder1.setOrderPaymentState("未付款");
                } else if(baOrder1.getAmountPaid() != null && baOrder1.getAmount() != null && baOrder1.getAmountPaid().doubleValue() < baOrder1.getAmount().doubleValue()){
                    //付款中
                    baOrder1.setOrderPaymentState("付款中");
                }
            }
        }
        return baOrders;
    }

    /**
     * 计算累计订单金额
     * type 金额类型
      */
    private BigDecimal getAmmountAll(String companyId, String orderType, BaOrder baOrder){
        double amountAll = 0;
        if(!StringUtils.isEmpty(companyId)){
            QueryWrapper<BaSettlement> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("contract_id", companyId);
            queryWrapper1.eq("flag", 0);
            List<BaSettlement> baSettlements = baSettlementMapper.selectList(queryWrapper1);
            if(!CollectionUtils.isEmpty(baSettlements)){
                for(BaSettlement baSettlement : baSettlements){
                    //采购
                    if("2".equals(orderType)){
                        QueryWrapper<BaPayment> queryWrapperBaPayment = new QueryWrapper<>();
                        queryWrapperBaPayment.eq("flag", 0);
                        queryWrapperBaPayment.eq("state", "payment:pass");
                        queryWrapperBaPayment.eq("settlement_id", baSettlement.getId());
                        List<BaPayment> baPayments = baPaymentMapper.selectList(queryWrapperBaPayment);
                        if(!CollectionUtils.isEmpty(baPayments)){
                            for(BaPayment baPayment : baPayments){
                                QueryWrapper<BaSettlementDetail> queryWrapper2 = new QueryWrapper<>();
//                                queryWrapper2.eq("settlement_id", baSettlement.getId());
                                queryWrapper2.eq("order_id", baOrder.getId());
                                queryWrapper2.eq("relation_id", baPayment.getId());
                                queryWrapper2.eq("type", "2"); //（1:应付金额，2:实付金额）
                                queryWrapper2.eq("flag", 0);
                                List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper2);
                                if(!CollectionUtils.isEmpty(baSettlementDetails)){
                                    for(BaSettlementDetail baSettlementDetail : baSettlementDetails){
                                        if (baSettlementDetail.getAmount() != null) {
                                            amountAll = amountAll + baSettlementDetail.getAmount().doubleValue();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if("1".equals(orderType)){ //销售
                        QueryWrapper<BaCollection> queryWrapperBaCollection = new QueryWrapper<>();
                        queryWrapperBaCollection.eq("flag", 0);
                        queryWrapperBaCollection.eq("state", "collection:pass");
                        queryWrapperBaCollection.eq("settlement_id", baSettlement.getId());
                        List<BaCollection> baCollections = baCollectionMapper.selectList(queryWrapperBaCollection);
                        if(!CollectionUtils.isEmpty(baCollections)){
                            for(BaCollection baCollection : baCollections){
                                QueryWrapper<BaSettlementDetail> queryWrapper2 = new QueryWrapper<>();
                                queryWrapper2.eq("settlement_id", baSettlement.getId());
                                queryWrapper2.eq("order_id", baOrder.getId());
                                queryWrapper2.eq("relation_id", baCollection.getId());
                                queryWrapper2.eq("type", "2"); //（1:应付金额，2:实付金额）
                                queryWrapper2.eq("flag", 0);
                                List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper2);
                                if(!CollectionUtils.isEmpty(baSettlementDetails)){
                                    for(BaSettlementDetail baSettlementDetail : baSettlementDetails){
                                        if (baSettlementDetail.getAmount() != null) {
                                            amountAll = amountAll + baSettlementDetail.getAmount().doubleValue();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        return new BigDecimal(amountAll);
    }

    /**
     * 计算累计订单金额
     * @param type 金额类型
     */
    private BigDecimal getAmmountPaidAll(String companyId, String type, BaOrder baOrder){
        double amountAll = 0;
        if(!StringUtils.isEmpty(companyId)){
            QueryWrapper<BaSettlement> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("contract_id", companyId);
            queryWrapper1.eq("flag", 0);
            List<BaSettlement> baSettlements = baSettlementMapper.selectList(queryWrapper1);
            if(!CollectionUtils.isEmpty(baSettlements)){
                for(BaSettlement baSettlement : baSettlements){
                    //采购
                    QueryWrapper<BaSettlementDetail> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("settlement_id", baSettlement.getId());
                    queryWrapper2.eq("order_id", baOrder.getId());
                    queryWrapper2.eq("type", type); //（1:应付金额，2:实付金额）
                    queryWrapper2.eq("flag", 0);
                    List<BaSettlementDetail> baSettlementDetails = baSettlementDetailMapper.selectList(queryWrapper2);
                    if(!CollectionUtils.isEmpty(baSettlementDetails)){
                        for(BaSettlementDetail baSettlementDetail : baSettlementDetails){
                            if (baSettlementDetail.getAmount() != null) {
                                amountAll = amountAll + baSettlementDetail.getAmount().doubleValue();
                            }
                        }
                    }
                }

            }
        }
        return new BigDecimal(amountAll);
    }

}
