package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaDepotItemMapper;
import com.business.system.domain.BaDepotItem;
import com.business.system.service.IBaDepotItemService;

/**
 * 出入库副Service业务层处理
 *
 * @author ljb
 * @date 2023-04-23
 */
@Service
public class BaDepotItemServiceImpl extends ServiceImpl<BaDepotItemMapper, BaDepotItem> implements IBaDepotItemService
{
    @Autowired
    private BaDepotItemMapper baDepotItemMapper;

    /**
     * 查询出入库副
     *
     * @param id 出入库副ID
     * @return 出入库副
     */
    @Override
    public BaDepotItem selectBaDepotItemById(String id)
    {
        return baDepotItemMapper.selectBaDepotItemById(id);
    }

    /**
     * 查询出入库副列表
     *
     * @param baDepotItem 出入库副
     * @return 出入库副
     */
    @Override
    public List<BaDepotItem> selectBaDepotItemList(BaDepotItem baDepotItem)
    {
        return baDepotItemMapper.selectBaDepotItemList(baDepotItem);
    }

    /**
     * 新增出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    @Override
    public int insertBaDepotItem(BaDepotItem baDepotItem)
    {
        baDepotItem.setCreateTime(DateUtils.getNowDate());
        return baDepotItemMapper.insertBaDepotItem(baDepotItem);
    }

    /**
     * 修改出入库副
     *
     * @param baDepotItem 出入库副
     * @return 结果
     */
    @Override
    public int updateBaDepotItem(BaDepotItem baDepotItem)
    {
        baDepotItem.setUpdateTime(DateUtils.getNowDate());
        return baDepotItemMapper.updateBaDepotItem(baDepotItem);
    }

    /**
     * 批量删除出入库副
     *
     * @param ids 需要删除的出入库副ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotItemByIds(String[] ids)
    {
        return baDepotItemMapper.deleteBaDepotItemByIds(ids);
    }

    /**
     * 删除出入库副信息
     *
     * @param id 出入库副ID
     * @return 结果
     */
    @Override
    public int deleteBaDepotItemById(String id)
    {
        return baDepotItemMapper.deleteBaDepotItemById(id);
    }

}
