package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaOvertime;

/**
 * 加班申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaOvertimeService
{
    /**
     * 查询加班申请
     *
     * @param id 加班申请ID
     * @return 加班申请
     */
    public OaOvertime selectOaOvertimeById(String id);

    /**
     * 查询加班申请列表
     *
     * @param oaOvertime 加班申请
     * @return 加班申请集合
     */
    public List<OaOvertime> selectOaOvertimeList(OaOvertime oaOvertime);

    /**
     * 新增加班申请
     *
     * @param oaOvertime 加班申请
     * @return 结果
     */
    public int insertOaOvertime(OaOvertime oaOvertime);

    /**
     * 修改加班申请
     *
     * @param oaOvertime 加班申请
     * @return 结果
     */
    public int updateOaOvertime(OaOvertime oaOvertime);

    /**
     * 批量删除加班申请
     *
     * @param ids 需要删除的加班申请ID
     * @return 结果
     */
    public int deleteOaOvertimeByIds(String[] ids);

    /**
     * 删除加班申请信息
     *
     * @param id 加班申请ID
     * @return 结果
     */
    public int deleteOaOvertimeById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


}
