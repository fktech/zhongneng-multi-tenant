package com.business.system.service;

import java.util.List;
import com.business.system.domain.BaProblem;

/**
 * 问题Service接口
 *
 * @author single
 * @date 2023-10-09
 */
public interface IBaProblemService
{
    /**
     * 查询问题
     *
     * @param id 问题ID
     * @return 问题
     */
    public BaProblem selectBaProblemById(String id);

    public BaProblem selectBaProblemByStartId(String startId);

    /**
     * 查询问题列表
     *
     * @param baProblem 问题
     * @return 问题集合
     */
    public List<BaProblem> selectBaProblemList(BaProblem baProblem);

    /**
     * 新增问题
     *
     * @param baProblem 问题
     * @return 结果
     */
    public int insertBaProblem(BaProblem baProblem);

    /**
     * 修改问题
     *
     * @param baProblem 问题
     * @return 结果
     */
    public int updateBaProblem(BaProblem baProblem);

    /**
     * 批量删除问题
     *
     * @param ids 需要删除的问题ID
     * @return 结果
     */
    public int deleteBaProblemByIds(String[] ids);

    /**
     * 删除问题信息
     *
     * @param id 问题ID
     * @return 结果
     */
    public int deleteBaProblemById(String id);


}
