package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaFee;

/**
 * 费用申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaFeeService
{
    /**
     * 查询费用申请
     *
     * @param id 费用申请ID
     * @return 费用申请
     */
    public OaFee selectOaFeeById(String id);

    /**
     * 查询费用申请列表
     *
     * @param oaFee 费用申请
     * @return 费用申请集合
     */
    public List<OaFee> selectOaFeeList(OaFee oaFee);

    /**
     * 新增费用申请
     *
     * @param oaFee 费用申请
     * @return 结果
     */
    public int insertOaFee(OaFee oaFee);

    /**
     * 修改费用申请
     *
     * @param oaFee 费用申请
     * @return 结果
     */
    public int updateOaFee(OaFee oaFee);

    /**
     * 批量删除费用申请
     *
     * @param ids 需要删除的费用申请ID
     * @return 结果
     */
    public int deleteOaFeeByIds(String[] ids);

    /**
     * 删除费用申请信息
     *
     * @param id 费用申请ID
     * @return 结果
     */
    public int deleteOaFeeById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


}
