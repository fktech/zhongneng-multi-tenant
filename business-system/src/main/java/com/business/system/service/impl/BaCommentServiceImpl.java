package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.MessageConstant;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaCompany;
import com.business.system.domain.BaMessage;
import com.business.system.mapper.BaMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaCommentMapper;
import com.business.system.domain.BaComment;
import com.business.system.service.IBaCommentService;

/**
 * 评论Service业务层处理
 *
 * @author ljb
 * @date 2023-06-08
 */
@Service
public class BaCommentServiceImpl extends ServiceImpl<BaCommentMapper, BaComment> implements IBaCommentService
{
    @Autowired
    private BaCommentMapper baCommentMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    /**
     * 查询评论
     *
     * @param id 评论ID
     * @return 评论
     */
    @Override
    public BaComment selectBaCommentById(String id)
    {
        return baCommentMapper.selectBaCommentById(id);
    }

    /**
     * 查询评论列表
     *
     * @param baComment 评论
     * @return 评论
     */
    @Override
    public List<BaComment> selectBaCommentList(BaComment baComment)
    {
        return baCommentMapper.selectBaCommentList(baComment);
    }

    /**
     * 新增评论
     *
     * @param baComment 评论
     * @return 结果
     */
    @Override
    public int insertBaComment(BaComment baComment)
    {
        baComment.setCreateTime(DateUtils.getNowDate());
        baComment.setId(getRedisIncreID.getId());
        int result = baCommentMapper.insertBaComment(baComment);
        if(result > 0){
            this.insertMessage(baComment.getBlogId(), String.valueOf(baComment.getSendUserId()), MessageFormat.format(MessageConstant.COMMENT_MSG, baComment.getBusinessName()), baComment.getBusinessType(), baComment.getProcessInstanceId());
        }
        return result;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(String businessId, String mId, String msgContent, String businessType, String processInstanceId){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(businessId);
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("2"); //评论提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(businessType);
        baMessage.setProcessInstanceId(processInstanceId);
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 修改评论
     *
     * @param baComment 评论
     * @return 结果
     */
    @Override
    public int updateBaComment(BaComment baComment)
    {
        baComment.setUpdateTime(DateUtils.getNowDate());
        return baCommentMapper.updateBaComment(baComment);
    }

    /**
     * 批量删除评论
     *
     * @param ids 需要删除的评论ID
     * @return 结果
     */
    @Override
    public int deleteBaCommentByIds(String[] ids)
    {
        return baCommentMapper.deleteBaCommentByIds(ids);
    }

    /**
     * 删除评论信息
     *
     * @param id 评论ID
     * @return 结果
     */
    @Override
    public int deleteBaCommentById(String id)
    {
        return baCommentMapper.deleteBaCommentById(id);
    }

}
