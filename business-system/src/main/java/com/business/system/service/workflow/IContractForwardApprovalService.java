package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaContractForward;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaMessage;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaContractForwardMapper;
import com.business.system.mapper.BaContractStartMapper;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IBaMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 合同结转审批接口 服务类
 * @date: 2023/3/8 10:37
 */
@Service
public class IContractForwardApprovalService {

    @Autowired
    protected BaContractForwardMapper baContractForwardMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaContractForward baContractForward = new BaContractForward();
        baContractForward.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        baContractForward = baContractForwardMapper.selectBaContractForwardById(baContractForward.getId());
        baContractForward.setState(status);
        String userId = String.valueOf(baContractForward.getUserId());
        if(AdminCodeEnum.CONTRACTFORWARD_STATUS_PASS.getCode().equals(status)){
            //计算结转金额，结入加，结出减
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getStartId());
            if(!ObjectUtils.isEmpty(baContractStart)){
                //结出
                if(baContractForward.getWay() == 1){
                    if(baContractStart.getCurrentSettlementDifference() == null){
                        baContractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    //当前结算差额
                    baContractForward.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference());
                    baContractStart.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference().subtract(baContractForward.getAmount()));


                    //结转来源
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getSource());
                    if(!ObjectUtils.isEmpty(contractStart)){
                        if(contractStart.getCurrentSettlementDifference() == null){
                            contractStart.setCurrentSettlementDifference(new BigDecimal(0));
                        }
                        //当前结算差额
                        baContractForward.setSettlementDifference(contractStart.getCurrentSettlementDifference());
                        contractStart.setCurrentSettlementDifference(contractStart.getCurrentSettlementDifference().add(baContractForward.getAmount()));
                        baContractStartMapper.updateBaContractStart(contractStart);
                    }
                } else {
                    if(baContractStart.getCurrentSettlementDifference() == null){
                        baContractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    baContractForward.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference());
                    baContractStart.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference().add(baContractForward.getAmount()));

                    //结转来源
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getSource());
                    if(contractStart.getCurrentSettlementDifference() == null){
                        contractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    if(!ObjectUtils.isEmpty(contractStart)){
                        //当前结算差额
                        baContractForward.setSettlementDifference(contractStart.getCurrentSettlementDifference());
                        contractStart.setCurrentSettlementDifference(contractStart.getCurrentSettlementDifference().subtract(baContractForward.getAmount()));
                        baContractStartMapper.updateBaContractStart(contractStart);
                    }
                }
            }
            baContractStartMapper.updateBaContractStart(baContractStart);

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baContractForward, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.CONTRACTFORWARD_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baContractForward, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baContractForward, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        baContractForwardMapper.updateById(baContractForward);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaContractForward baContractForward, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baContractForward.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"合同结转审批提醒",msgContent,baMessage.getType(),baContractForward.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaContractForward checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        BaContractForward baContractForward = baContractForwardMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (baContractForward == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CONTRACTFORWARD_001);
        }
        if (!AdminCodeEnum.CONTRACTFORWARD_STATUS_VERIFYING.getCode().equals(baContractForward.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_CONTRACTFORWARD_002);
        }
        return baContractForward;
    }
}
