package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.annotation.DataScope;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaMessage;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.domain.BaContractForward;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 合同结转Service业务层处理
 *
 * @author ljb
 * @date 2023-03-06
 */
@Service
public class BaContractForwardServiceImpl extends ServiceImpl<BaContractForwardMapper, BaContractForward> implements IBaContractForwardService
{

    private static final Logger log = LoggerFactory.getLogger(BaContractForwardServiceImpl.class);
    @Autowired
    private BaContractForwardMapper baContractForwardMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaContractStartMapper baContractStartMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private IBaMessageService baMessageService;

    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;




    /**
     * 查询合同结转
     *
     * @param id 合同结转ID
     * @return 合同结转
     */
    @Override
    public BaContractForward selectBaContractForwardById(String id)
    {
        BaContractForward baContractForward = baContractForwardMapper.selectBaContractForwardById(id);
        if(StringUtils.isNotNull(baContractForward)){
            if(StringUtils.isNotEmpty(baContractForward.getStartId())){
                //合同启动
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getStartId());
                baContractForward.setBaContractStart(contractStart);
                //初始化结转差额
                if(baContractForward.getCurrentSettlementDifference() == null){
                    baContractForward.setCurrentSettlementDifference(contractStart.getCurrentSettlementDifference());
                }
            }
            if(StringUtils.isNotEmpty(baContractForward.getSource())){
                //结转来源
                BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getSource());
                baContractForward.setContractStart(contractStart);
                //初始化结转源差额
                if(baContractForward.getSettlementDifference() == null){
                    baContractForward.setSettlementDifference(contractStart.getCurrentSettlementDifference());
                }
            }
            //发起人
            SysUser sysUser = sysUserMapper.selectUserById(baContractForward.getUserId());
            if(!ObjectUtils.isEmpty(sysUser)){
                sysUser.setUserName(sysUser.getNickName());
                //岗位名称
                String name = getName.getName(sysUser.getUserId());
                if(name.equals("") == false){
                    sysUser.setPostName(name.substring(0,name.length()-1));
                }
            }
            baContractForward.setUser(sysUser);
        }

        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
        relatedQueryWrapper.eq("business_id",baContractForward.getId());
        relatedQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        relatedQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        baContractForward.setProcessInstanceId(processInstanceIds);
        return baContractForward;
    }

    /**
     * 查询合同结转列表
     *
     * @param baContractForward 合同结转
     * @return 合同结转
     */
    @Override
    //@DataScope(deptAlias = "f",userAlias = "f")
    public List<BaContractForward> selectBaContractForwardList(BaContractForward baContractForward)
    {
        List<BaContractForward> baContractForwards = baContractForwardMapper.selectBaContractForward(baContractForward);

        for (BaContractForward contractForward:baContractForwards) {
            //岗位信息
            String name = getName.getName(contractForward.getUserId());
            //发起人
            if(name.equals("") == false){
                contractForward.setUserName(sysUserMapper.selectUserById(contractForward.getUserId()).getNickName()+"-"+name.substring(0,name.length()-1));
            }else {
                contractForward.setUserName(sysUserMapper.selectUserById(contractForward.getUserId()).getNickName());
            }

        }
        return baContractForwards;
    }

    /**
     * 新增合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    @Override
    public int insertBaContractForward(BaContractForward baContractForward)
    {
        baContractForward.setId(getRedisIncreID.getId());
        baContractForward.setCreateTime(DateUtils.getNowDate());
        baContractForward.setCreateBy(SecurityUtils.getUsername());
        baContractForward.setUserId(SecurityUtils.getUserId());
        baContractForward.setDeptId(SecurityUtils.getDeptId());
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode(),SecurityUtils.getCurrComId());
        baContractForward.setFlowId(flowId);
        int result = baContractForwardMapper.insertBaContractForward(baContractForward);

        //判断业务归属公司是否是上海子公司
        if(!"2".equals(baContractForward.getWorkState())) {
            //流程发起状态
            baContractForward.setWorkState("1");
            //默认审批流程已通过
            baContractForward.setState(AdminCodeEnum.CONTRACTFORWARD_STATUS_PASS.getCode());

            //计算结转金额，结入加，结出减
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getStartId());
            if (!ObjectUtils.isEmpty(baContractStart)) {
                //结出
                if (baContractForward.getWay() == 1) {
                    if (baContractStart.getCurrentSettlementDifference() == null) {
                        baContractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    //当前结算差额
                    baContractForward.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference());
                    baContractStart.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference().subtract(baContractForward.getAmount()));


                    //结转来源
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getSource());
                    if (!ObjectUtils.isEmpty(contractStart)) {
                        if (contractStart.getCurrentSettlementDifference() == null) {
                            contractStart.setCurrentSettlementDifference(new BigDecimal(0));
                        }
                        //当前结算差额
                        baContractForward.setSettlementDifference(contractStart.getCurrentSettlementDifference());
                        contractStart.setCurrentSettlementDifference(contractStart.getCurrentSettlementDifference().add(baContractForward.getAmount()));
                        baContractStartMapper.updateBaContractStart(contractStart);
                    }
                } else {
                    if (baContractStart.getCurrentSettlementDifference() == null) {
                        baContractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    baContractForward.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference());
                    baContractStart.setCurrentSettlementDifference(baContractStart.getCurrentSettlementDifference().add(baContractForward.getAmount()));

                    //结转来源
                    BaContractStart contractStart = baContractStartMapper.selectBaContractStartById(baContractForward.getSource());
                    if (contractStart.getCurrentSettlementDifference() == null) {
                        contractStart.setCurrentSettlementDifference(new BigDecimal(0));
                    }
                    if (!ObjectUtils.isEmpty(contractStart)) {
                        //当前结算差额
                        baContractForward.setSettlementDifference(contractStart.getCurrentSettlementDifference());
                        contractStart.setCurrentSettlementDifference(contractStart.getCurrentSettlementDifference().subtract(baContractForward.getAmount()));
                        baContractStartMapper.updateBaContractStart(contractStart);
                    }
                }
            }
        }
        if(result > 0 && "2".equals(baContractForward.getWorkState())){
            BaContractForward contractForward = baContractForwardMapper.selectBaContractForwardById(baContractForward.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractForward, AdminCodeEnum.CONTRACTFORWARD_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                deleteBaContractForwardById(baContractForward.getId());
                return 0;
            }else {
                contractForward.setState(AdminCodeEnum.CONTRACTFORWARD_STATUS_VERIFYING.getCode());
                baContractForwardMapper.updateBaContractForward(contractForward);
            }
        }
        return result;
    }

    /**
     * 提交合同结转审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaContractForward baContractForward, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(baContractForward.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(baContractForward.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        BaContractForward contractForward = this.selectBaContractForwardById(baContractForward.getId());
        SysUser sysUser = iSysUserService.selectUserById(contractForward.getUserId());
        contractForward.setUserName(sysUser.getUserName());
        contractForward.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(contractForward, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.CONTRACTFORWARD_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改合同结转
     *
     * @param baContractForward 合同结转
     * @return 结果
     */
    @Override
    public int updateBaContractForward(BaContractForward baContractForward)
    {
        baContractForward.setUpdateTime(DateUtils.getNowDate());
        baContractForward.setUpdateBy(SecurityUtils.getUsername());
        int result = baContractForwardMapper.updateBaContractForward(baContractForward);
        if(result > 0){
            BaContractForward contractForward = baContractForwardMapper.selectBaContractForwardById(baContractForward.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", contractForward.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(contractForward, AdminCodeEnum.CONTRACTFORWARD_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                deleteBaContractForwardById(baContractForward.getId());
                return 0;
            }else {
                contractForward.setState(AdminCodeEnum.CONTRACTFORWARD_STATUS_VERIFYING.getCode());
                baContractForwardMapper.updateBaContractForward(contractForward);
            }
        }
        return result;
    }

    /**
     * 批量删除合同结转
     *
     * @param ids 需要删除的合同结转ID
     * @return 结果
     */
    @Override
    public int deleteBaContractForwardByIds(String[] ids)
    {
        return baContractForwardMapper.deleteBaContractForwardByIds(ids);
    }

    /**
     * 删除合同结转信息
     *
     * @param id 合同结转ID
     * @return 结果
     */
    @Override
    public int deleteBaContractForwardById(String id)
    {
        return baContractForwardMapper.deleteBaContractForwardById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            BaContractForward baContractForward = baContractForwardMapper.selectBaContractForwardById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baContractForward.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.CONTRACTFORWARD_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baContractForward.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                baContractForward.setState(AdminCodeEnum.CONTRACTFORWARD_STATUS_WITHDRAW.getCode());
                //baContractStartMapper.updateBaContractStart(baContractStart);
                baContractForwardMapper.updateBaContractForward(baContractForward);
                String userId = String.valueOf(baContractForward.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baContractForward, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaContractForward baContractForward, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baContractForward.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_CONTRACTFORWARD.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"合同结转审批提醒",msgContent,baMessage.getType(),baContractForward.getId(),baMessage.getBusinessType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

}
