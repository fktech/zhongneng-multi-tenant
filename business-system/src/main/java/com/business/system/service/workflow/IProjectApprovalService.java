package com.business.system.service.workflow;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysMenu;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaHiProjectService;
import com.business.system.service.IBaMessageService;
import com.business.system.service.IBaProjectService;
import com.business.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 项目管理审批接口 服务类
 * @date: 2022/12/8 17:53
 */
@Service
public class IProjectApprovalService {

    @Autowired
    protected BaProjectMapper baProjectMapper;

    @Autowired
    protected BaHiProjectMapper baHiProjectMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private BaChargingStandardsMapper baChargingStandardsMapper;

    @Autowired
    private BaDepotMapper baDepotMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IBaHiProjectService baHiProjectService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private BaEarlyWarningMapper baEarlyWarningMapper;

    @Autowired
    private BaGlobalNumberMapper baGlobalNumberMapper;

    @Autowired
    private IBaMessageService baMessageService;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        BaProject baProject = new BaProject();
        baProject.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        //变更状态
        BaProcessInstanceRelated applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
        BaHiProject baHiProject = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaHiProject.class);
        String userId = "";
        if(AdminCodeEnum.PROJECT_STATUS_PASS.getCode().equals(status)){
            BaProject baProject1 = new BaProject();
            if((ObjectUtils.isEmpty(baHiProject)) || (!ObjectUtils.isEmpty(baHiProject) && !"2".equals(baHiProject.getApproveFlag()))) {
                baProject = baProjectMapper.selectBaProjectById(baProject.getId());
                userId = String.valueOf(baProject.getUserId());
                applyBusinessDataByProcessInstance = baProcessInstanceRelatedMapper.getApplyBusinessDataByProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
                baProject1 = JSONObject.toJavaObject(JSONObject.parseObject(applyBusinessDataByProcessInstance.getBusinessData()), BaProject.class);
                //更新项目阶段
                baProject1.setProjectStage(1);
                baProject1.setProjectState(AdminCodeEnum.PROJECT_UNDER_WAY.getCode());
                baProject1.setState(status);
                //审批通过时间
                baProject1.setPassTime(DateUtils.getNowDate());
                //收费标准
                if (StringUtils.isNotNull(baProject1.getStandards())) {
                    if (StringUtils.isNotEmpty(baProject1.getStandards().getId())) {
                        baChargingStandardsMapper.updateBaChargingStandards(baProject1.getStandards());
                    } else {
                        BaChargingStandards standards = baProject1.getStandards();
                        standards.setId(getRedisIncreID.getId());
                        standards.setCreateTime(DateUtils.getNowDate());
                        standards.setCreateBy(SecurityUtils.getUsername());
                        baChargingStandardsMapper.insertBaChargingStandards(standards);
                        baProject1.setStandardsId(standards.getId());
                    }
                }
                baProjectMapper.updateById(baProject1);


                //审批通过时新增预警
                if(baProject1.getProjectType().equals("4")){
                    BaEarlyWarning baEarlyWarning = new BaEarlyWarning();
                    baEarlyWarning.setId(getRedisIncreID.getId());
                    baEarlyWarning.setRelevanceId(baProject1.getId());
                    baEarlyWarning.setBusinessType("1");
                    baEarlyWarning.setCycleUpperLimit(Long.valueOf(baProject1.getPaymentCycle()));
                    baEarlyWarningMapper.insertBaEarlyWarning(baEarlyWarning);
                }
                //全局编号
                if(StringUtils.isNotEmpty(baProject1.getGlobalNumber())){
                    //全局编号关系表新增数据
                    BaGlobalNumber baGlobalNumber = new BaGlobalNumber();
                    baGlobalNumber.setId(getRedisIncreID.getId());
                    baGlobalNumber.setBusinessId(baProject1.getId());
                    baGlobalNumber.setCode(baProject1.getGlobalNumber());
                    baGlobalNumber.setHierarchy("1");
                    baGlobalNumber.setType("1");
                    baGlobalNumber.setTenantId(SecurityUtils.getCurrComId());
                    baGlobalNumber.setCreateTime(DateUtils.getNowDate());
                    baGlobalNumberMapper.insertBaGlobalNumber(baGlobalNumber);
                }

            } else {
                String id = baHiProject.getId();
                if(!ObjectUtils.isEmpty(baHiProject)){
                    userId = String.valueOf(baHiProject.getUserId());
                }

                if(StringUtils.isNotEmpty(baHiProject.getSalesman())) {
                    if(StringUtils.isNotEmpty(baHiProject.getSalesman())){
                        String salesmanStr = "";
                        StringBuilder salesmanStringBuilder = new StringBuilder();
                        String[] salesmanArray = baHiProject.getSalesman().split(";");
                        for(String s : salesmanArray){
                            if(StringUtils.isNotEmpty(s)){
                                String[] salesmanSplit = s.split(",");
                                salesmanStr = salesmanSplit[salesmanSplit.length-1];
                                if(StringUtils.isNotEmpty(salesmanStr)){
                                    String nickName = sysUserMapper.selectUserById(Long.valueOf(salesmanStr)).getNickName();
                                    if(salesmanStringBuilder.length() > 0){
                                        salesmanStringBuilder.append(",").append(nickName);
                                    } else {
                                        salesmanStringBuilder.append(nickName);
                                    }
                                }
                            }
                        }
                        baHiProject.setSalesmanName(salesmanStringBuilder.toString());
                    }
                }
                if(!baHiProject.getState().equals(AdminCodeEnum.PROJECT_STATUS_PASS.getCode()) && !baHiProject.getState().equals(AdminCodeEnum.PROJEC_BUSINESS_STATUS_PASS.getCode())){

                    baHiProject.setSubmitTime(DateUtils.getNowDate());
                    //审批状态
                    baHiProject.setState(AdminCodeEnum.PROJECT_STATUS_PASS.getCode());
                    baHiProject.setProjectState(AdminCodeEnum.PROJECT_UNDER_WAY.getCode());
                }
                //拼接归属部门
                if (baHiProject.getDeptId() != null) {
                    SysDept dept = sysDeptMapper.selectDeptById(baHiProject.getDeptId());
                    //查询祖籍
                    String[] split = dept.getAncestors().split(",");
                    List<Long> integerList = new ArrayList<>();
                    for (String deptId:split) {
                        integerList.add(Long.valueOf(deptId));
                    }
                    //倒叙拿到第一个公司
                    Collections.reverse(integerList);
                    String deptName = "";
                    for (Long itm:integerList) {
                        SysDept dept1 = sysDeptMapper.selectDeptById(itm);
                        if(StringUtils.isNotNull(dept1)){
                            if(dept1.getDeptType().equals("1")){
                                deptName = dept1.getDeptName();
                                break;
                            }
                        }
                    }
                    baHiProject.setPartDeptName(deptName + "-" + dept.getDeptName());
                }
                baHiProjectMapper.updateBaHiProjectChange(baHiProject);

                baProject1 = new BaProject();
                BaHiProject baHiProjectTemp = baHiProjectMapper.selectBaHiProjectById(baHiProject.getId());
                BaProject baProject2 = baProjectMapper.selectBaProjectById(baHiProjectTemp.getRelationId());
                BeanUtils.copyBeanProp(baProject1, baHiProject);
                baProject1.setApproveFlag("0");
                baProject1.setId(baProject2.getId());
                baProject1.setState(status);
                baProjectMapper.updateBaProjectChange(baProject1);

                BeanUtils.copyBeanProp(baHiProject, baProject2);
                baHiProject.setApproveFlag("0");
                baHiProject.setId(id);
                baHiProject.setState(status);
                baHiProjectMapper.updateBaHiProjectChange(baHiProject);

                //查询原数据流程实例ID
                QueryWrapper<BaProcessInstanceRelated> relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", baProject2.getId());

                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                BaProcessInstanceRelated baProcessInstanceRelatedBefore = new BaProcessInstanceRelated();
                BaProcessInstanceRelated baProcessInstanceRelatedBeforeTemp = new BaProcessInstanceRelated();
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelatedBefore = baProcessInstanceRelateds.get(0);
                    BeanUtils.copyBeanProp(baProcessInstanceRelatedBeforeTemp, baProcessInstanceRelatedBefore);
                }

                BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
                relatedQueryWrapper = new QueryWrapper<>();
                relatedQueryWrapper.eq("business_id", id);
                relatedQueryWrapper.eq("flag", 0);
                //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
                relatedQueryWrapper.orderByDesc("create_time");
                baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(relatedQueryWrapper);
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                }

                baProcessInstanceRelatedBefore.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                baProcessInstanceRelatedBefore.setBusinessData(baProcessInstanceRelated.getBusinessData());
                baProcessInstanceRelatedBefore.setApproveType(baProcessInstanceRelated.getApproveType());
                baProcessInstanceRelatedBefore.setApproveResult(baProcessInstanceRelated.getApproveResult());
                baProcessInstanceRelatedBefore.setReason(baProcessInstanceRelated.getReason());
                baProcessInstanceRelatedBefore.setCreateBy(baProcessInstanceRelated.getCreateBy());
                baProcessInstanceRelatedBefore.setCreateTime(baProcessInstanceRelated.getCreateTime());
                baProcessInstanceRelatedBefore.setUpdateBy(baProcessInstanceRelated.getUpdateBy());
                baProcessInstanceRelatedBefore.setUpdateTime(baProcessInstanceRelated.getUpdateTime());
                baProcessInstanceRelatedBefore.setState(baProcessInstanceRelated.getState());
                baProcessInstanceRelatedBefore.setFlag(baProcessInstanceRelated.getFlag());
                baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelatedBefore); //对换流程实例ID


                baProcessInstanceRelated.setProcessInstanceId(baProcessInstanceRelatedBeforeTemp.getProcessInstanceId());
                baProcessInstanceRelated.setBusinessData(baProcessInstanceRelatedBeforeTemp.getBusinessData());
                baProcessInstanceRelated.setApproveType(baProcessInstanceRelatedBeforeTemp.getApproveType());
                baProcessInstanceRelated.setApproveResult(baProcessInstanceRelatedBeforeTemp.getApproveResult());
                baProcessInstanceRelated.setReason(baProcessInstanceRelatedBeforeTemp.getReason());
                baProcessInstanceRelated.setCreateBy(baProcessInstanceRelatedBeforeTemp.getCreateBy());
                baProcessInstanceRelated.setCreateTime(baProcessInstanceRelatedBeforeTemp.getCreateTime());
                baProcessInstanceRelated.setUpdateBy(baProcessInstanceRelatedBeforeTemp.getUpdateBy());
                baProcessInstanceRelated.setUpdateTime(baProcessInstanceRelatedBeforeTemp.getUpdateTime());
                baProcessInstanceRelated.setState(baProcessInstanceRelatedBeforeTemp.getState());
                baProcessInstanceRelated.setFlag(baProcessInstanceRelatedBeforeTemp.getFlag());
                baProcessInstanceRelatedMapper.updateInstanceRelatedProcessInstanceId(baProcessInstanceRelated); //对换流程实例ID

                //原收费标准修改为新收费标准
                QueryWrapper<BaChargingStandards> baChargingStandardsQueryWrapper = new QueryWrapper<>();
                baChargingStandardsQueryWrapper.eq("relation_id", baProject2.getId());
                List<BaChargingStandards> baChargingStandards = baChargingStandardsMapper.selectList(baChargingStandardsQueryWrapper);

                //新收费标准修改为原收费标准
                baChargingStandardsQueryWrapper = new QueryWrapper<>();
                baChargingStandardsQueryWrapper.eq("relation_id", id);
                List<BaChargingStandards> baChargingStandardsHi = baChargingStandardsMapper.selectList(baChargingStandardsQueryWrapper);
                List<String> baChargingStandardIds = null;
                if(!CollectionUtils.isEmpty(baChargingStandards)){
                    baChargingStandardIds = baChargingStandards.stream().map(BaChargingStandards::getId).collect(Collectors.toList());
                    baChargingStandardsMapper.deleteBaChargingStandardsByIds(baChargingStandardIds.toArray(new String[0]));
                    for(BaChargingStandards baChargingStandards1 : baChargingStandards){
                        baChargingStandards1.setRelationId(id);
                        baChargingStandardsMapper.insertBaChargingStandards(baChargingStandards1);
                    }
                }

                if(!CollectionUtils.isEmpty(baChargingStandardsHi)){
                    baChargingStandardIds = baChargingStandardsHi.stream().map(BaChargingStandards::getId).collect(Collectors.toList());
                    baChargingStandardsMapper.deleteBaChargingStandardsByIds(baChargingStandardIds.toArray(new String[0]));
                    for(BaChargingStandards baChargingStandards2 : baChargingStandardsHi){
                        baChargingStandards2.setRelationId(baProject2.getId());
                        baChargingStandardsMapper.insertBaChargingStandards(baChargingStandards2);
                    }
                }

            }

                /*//查询所有菜单数据
                List<SysMenu> sysMenus1 = menuMapper.menuList(new SysMenu());

                //定义角色菜单权限
                List<SysRoleMenu> roleMenuList = new ArrayList<>();
                //立项审批通过新增菜单
                SysMenu menu = new SysMenu();
                menu.setMenuId(sysMenus1.get(0).getMenuId()+1);
                menu.setComponent("system/ongoing/ongoing");
                menu.setMenuName(baProject1.getName());
                menu.setIsCache("0");
                menu.setIsFrame("1");
                menu.setMark("0");
                menu.setMenuType("C");
                menu.setOrderNum(1);
                menu.setParentId(2668L);
                menu.setQuery("{"+" \"id\" "+":"+"\"" + baProject1.getId() + "\""+"}");
                menu.setPath(baProject1.getNameShorter());
                menu.setIcon("xiangmuyuhetong");
                iSysMenuService.insertMenu(menu);
                //权限绑定
                List<SysRoleMenu> sysRoleMenus = roleMenuMapper.sysRoleMenuList(2668L);
                for (SysRoleMenu sysRoleMenu1:sysRoleMenus) {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setMenuId(menu.getMenuId());
                    sysRoleMenu.setRoleId(sysRoleMenu1.getRoleId());
                    roleMenuList.add(sysRoleMenu);
                }
                roleMenuMapper.batchRoleMenu(roleMenuList);*/



            //插入库存表
            //判断仓库业务
//            if("9".equals(baProject1.getBusinessType())){
//                BaMaterialStock baMaterialStock = new BaMaterialStock();
//                baMaterialStock.setId(getRedisIncreID.getId());
//                baMaterialStock.setProjectId(baProject1.getId()); //项目ID
//                baMaterialStock.setDepotId(baProject1.getDepotId()); //仓库ID
//                baMaterialStock.setCreateTime(DateUtils.getNowDate());
//                baMaterialStock.setCreateBy(SecurityUtils.getUsername());
//                baMaterialStockMapper.insertBaMaterialStock(baMaterialStock);
//            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(baProject1, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.PROJECT_STATUS_REJECT.getCode().equals(status)){
            baProject = baProjectMapper.selectBaProjectById(baProject.getId());
            userId = String.valueOf(baProject.getUserId());
            //修改仓库选用状态
            if(StringUtils.isNotEmpty(baProject.getDepotId())){
                BaDepot baDepot = baDepotMapper.selectBaDepotById(baProject.getDepotId());
//                baDepot.setIsSelected("0");
                baDepotMapper.updateBaDepot(baDepot);
            }
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage( baProject, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(baProject, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }

            baProject = baProjectMapper.selectBaProjectById(baProject.getId());
            if(!ObjectUtils.isEmpty(baProject)){
                baProject.setState(status);
                baProjectMapper.updateById(baProject);
            } else if(ObjectUtils.isEmpty(baHiProject)){
                baHiProject.setState(status);
                baHiProject.setApproveFlag("0");
                baHiProjectMapper.updateById(baHiProject);
            }
        }

        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaProject baProject, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baProject.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_PROJECT.getCode());
        //查询是否存在手机标识
        SysUser sysUser = sysUserMapper.selectUserById(Long.valueOf(mId));
        if(null != sysUser.getPhoneCode() && !"".equals(sysUser.getPhoneCode())){
            baMessageService.messagePush(sysUser.getPhoneCode(),"立项审批提醒",msgContent,baMessage.getType(),baProject.getId(),baProject.getProjectType());
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected BaProject checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        //变更状态
        BaHiProject baHiProject = baHiProjectMapper.selectBaHiProjectById(approvalWorkflowDTO.getTargetId());
        BaProject baProject = new BaProject();
        if((ObjectUtils.isEmpty(baHiProject)) || (!ObjectUtils.isEmpty(baHiProject) && !"2".equals(baHiProject.getApproveFlag()))) {
            baProject = baProjectMapper.selectById(approvalWorkflowDTO.getTargetId());
            if (baProject == null) {
                throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PROJECT_001);
            }
            if (!AdminCodeEnum.PROJECT_STATUS_VERIFYING.getCode().equals(baProject.getState())) {
                throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_PROJECT_002);
            }
        }

        return baProject;
    }
}
