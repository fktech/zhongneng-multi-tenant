package com.business.system.service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.vo.ClockInVO;
import com.business.system.mapper.*;
import com.business.system.service.DingDingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.service.IOaAttendanceGroupService;

/**
 * 考勤组Service业务层处理
 *
 * @author single
 * @date 2023-11-24
 */
@Service
public class OaAttendanceGroupServiceImpl extends ServiceImpl<OaAttendanceGroupMapper, OaAttendanceGroup> implements IOaAttendanceGroupService
{
    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private OaWeekdayMapper oaWeekdayMapper;

    @Autowired
    private OaAttendanceAddressMapper oaAttendanceAddressMapper;

    @Autowired
    private OaWorkTimesMapper oaWorkTimesMapper;

    @Autowired
    private OaHolidayMapper oaHolidayMapper;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private OaEvectionMapper oaEvectionMapper;

    @Autowired
    private OaEvectionTripMapper oaEvectionTripMapper;

    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private OaEgressMapper oaEgressMapper;

    @Autowired
    private DingDingService dingDingService;

    /**
     * 查询考勤组
     *
     * @param id 考勤组ID
     * @return 考勤组
     */
    @Override
    public OaAttendanceGroup selectOaAttendanceGroupById(String id)
    {
        OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(id);
        if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
            //考勤部门名称
            String name = "";
            String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
            for (String depId:split) {
                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(depId));
                name = dept.getDeptName() + "," + name;
            }
            oaAttendanceGroup.setAttendanceMembersName(name);
        }
        if(StringUtils.isNotEmpty(oaAttendanceGroup.getNonAttendanceMembers())){
            //非考勤成员名称
            String userName = "";
            String[] split = oaAttendanceGroup.getNonAttendanceMembers().split(",");
            for (String userId:split) {
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                if(StringUtils.isNotNull(user)) {
                    userName = user.getNickName() + "," + userName;
                }
            }
            oaAttendanceGroup.setNonAttendanceMembersName(userName);
        }
        //工作日设置
        OaWeekday weekday = new OaWeekday();
        weekday.setRelationId(oaAttendanceGroup.getId());
        weekday.setType("1");
        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
        oaWeekdays.stream().forEach(itm->{
            if(StringUtils.isNotEmpty(itm.getWorkTimeId())){
                //班次信息
                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(itm.getWorkTimeId());
                itm.setWorkTime(oaWorkTimes);
            }
        });
        oaAttendanceGroup.setOaWeekdays(oaWeekdays);
        //必须打卡
        weekday.setType("2");
        List<OaWeekday> mustSpecialSettingsList = oaWeekdayMapper.selectOaWeekdayList(weekday);
        mustSpecialSettingsList.stream().forEach(itm->{
            if(StringUtils.isNotEmpty(itm.getWorkTimeId())){
                //班次信息
                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(itm.getWorkTimeId());
                itm.setWorkTime(oaWorkTimes);
            }
        });
        oaAttendanceGroup.setMustSpecialSettingsList(mustSpecialSettingsList);
        //无需打卡
        weekday.setType("3");
        List<OaWeekday> notSpecialSettingsList = oaWeekdayMapper.selectOaWeekdayList(weekday);
        notSpecialSettingsList.stream().forEach(itm->{
            if(StringUtils.isNotEmpty(itm.getWorkTimeId())){
                //班次信息
                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(itm.getWorkTimeId());
                itm.setWorkTime(oaWorkTimes);
            }
        });
        oaAttendanceGroup.setNotSpecialSettingsList(notSpecialSettingsList);
        //打卡地点
        OaAttendanceAddress attendanceAddress = new OaAttendanceAddress();
        attendanceAddress.setRelationId(oaAttendanceGroup.getId());
        List<OaAttendanceAddress> oaAttendanceAddresses = oaAttendanceAddressMapper.selectOaAttendanceAddressList(attendanceAddress);
        oaAttendanceGroup.setOaAttendanceAddresses(oaAttendanceAddresses);
        return oaAttendanceGroup;
    }

    /**
     * 查询考勤组列表
     *
     * @param oaAttendanceGroup 考勤组
     * @return 考勤组
     */
    @Override
    public List<OaAttendanceGroup> selectOaAttendanceGroupList(OaAttendanceGroup oaAttendanceGroup)
    {
        List<OaAttendanceGroup> oaAttendanceGroups = oaAttendanceGroupMapper.selectOaAttendanceGroupList(oaAttendanceGroup);
        oaAttendanceGroups.stream().forEach(itm ->{
              if(StringUtils.isNotEmpty(itm.getAttendanceMembers())){
                  //考勤人员转换
                  String[] split = itm.getAttendanceMembers().split(",");
                  List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                  Long[] deptIds = l.toArray(new Long[l.size()]);
                  //查看需打卡人员
                  List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                  itm.setAttendanceNumber(sysUsers.size());
                  //考勤时间拼接
                  Map<String,String> map = new HashMap<>();
                  //工作日设置
                  OaWeekday weekday = new OaWeekday();
                  weekday.setRelationId(itm.getId());
                  weekday.setType("1");
                  List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                  for (OaWeekday weekday1:oaWeekdays) {
                      if(StringUtils.isNotEmpty(weekday1.getWorkTimeId())){
                          boolean  b = map.containsKey(weekday1.getWorkTimeId());
                          if(!b){
                              map.put(weekday1.getWorkTimeId(),weekday1.getName());
                          }else {
                              String s = map.get(weekday1.getWorkTimeId());
                              map.put(weekday1.getWorkTimeId(),s+"、"+weekday1.getName());
                          }
                          //工作日关联的班次
                          OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday1.getWorkTimeId());
                          weekday1.setWorkTime(oaWorkTimes);
                      }
                  }
                  itm.setOaWeekdays(oaWeekdays);
                  //遍历map
                  /*for (Map.Entry<String,String> entry:map.entrySet()) {
                      if("0".equals(entry.getKey())){
                          String s = map.get("0");
                          map.put(entry.getKey(),s+","+"休假");
                      }else {
                          OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(entry.getKey());
                          String s = map.get(entry.getKey());
                          map.put(oaWorkTimes.getId(),s+","+oaWorkTimes.getName()+":"+oaWorkTimes.getGoWorkTime()+"-"+oaWorkTimes.getOffWorkTime());
                      }
                  }*/
                  itm.setMap(map);
              }
        });
        return oaAttendanceGroups;
    }

    /**
     * 新增考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    @Override
    public int insertOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup)
    {
        //钉钉创建考勤组
        //钉钉修改用户
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456") && oaAttendanceGroup.getFieldClock().equals("2")){
            try {
                String groupAdd = dingDingService.groupAdd(oaAttendanceGroup);
                Object parse = JSONObject.parse(groupAdd);
                JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                String errmsg = jsonObject.getString("errmsg");
                if(errmsg.equals("ok")){
                    JSONObject result = jsonObject.getJSONObject("result");
                    String id = result.getString("id");
                    oaAttendanceGroup.setDingDing(id);
                    //更新参与考勤人员
                    String groupMemberUpdate = dingDingService.groupMemberUpdate(oaAttendanceGroup);
                    Object parse1 = JSONObject.parse(groupMemberUpdate);
                    JSONObject jsonObject1 = (JSONObject) JSON.toJSON(parse1);
                    String errMsg1 = jsonObject1.getString("errmsg");
                    if(errMsg1.equals("ok")){
                        msg = "success";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                //日期格式化
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String queryTime = df.format(new Date());

                oaAttendanceGroup.setId(getRedisIncreID.getId());
                oaAttendanceGroup.setCreateTime(DateUtils.getNowDate());
                //租户ID
                oaAttendanceGroup.setTenantId(SecurityUtils.getCurrComId());

                if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
                    //锁定对应部门
                    String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
                    for (String deptId:split) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(oaAttendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getNonAttendanceMembers())){
                    //标记不用打卡用户
                    String[] split = oaAttendanceGroup.getNonAttendanceMembers().split(",");
                    for (String userId:split) {
                        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                        user.setNeedNot("1");
                        sysUserMapper.updateUser(user);
                    }
                }
                //工作日设置
                if(oaAttendanceGroup.getOaWeekdays().size() > 0){
                    for (OaWeekday weekday:oaAttendanceGroup.getOaWeekdays()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("1");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //地点
                if(oaAttendanceGroup.getOaAttendanceAddresses().size() > 0){
                    for (OaAttendanceAddress attendanceAddress:oaAttendanceGroup.getOaAttendanceAddresses()) {
                        attendanceAddress.setId(getRedisIncreID.getId());
                        attendanceAddress.setRelationId(oaAttendanceGroup.getId());
                        attendanceAddress.setCreateTime(DateUtils.getNowDate());
                        oaAttendanceAddressMapper.insertOaAttendanceAddress(attendanceAddress);
                    }
                }
                //必须打卡
                if(oaAttendanceGroup.getMustSpecialSettingsList().size() > 0){
                    for (OaWeekday weekday:oaAttendanceGroup.getMustSpecialSettingsList()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("2");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //无需打卡
                if(oaAttendanceGroup.getNotSpecialSettingsList().size() > 0){
                    for (OaWeekday weekday:oaAttendanceGroup.getNotSpecialSettingsList()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("3");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //立即生效创建打卡历史数据
                if("0".equals(oaAttendanceGroup.getState())){
                    //新增打卡缓存数据
                    //获取所有考勤组成员
                    String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
                    List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                    Long[] deptIds = l.toArray(new Long[l.size()]);
                    List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                    //无需打卡人员
            /*String nonAttendanceMembers = oaAttendanceGroup.getNonAttendanceMembers();
            String[] split1 = nonAttendanceMembers.split(",");*/
                    for (SysUser user:sysUsers) {
                        if("0".equals(user.getNeedNot())){
                            //查询打卡记录表中存在今天数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(user.getUserId());
                            //clock.setFlag(1L);
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            //工作日查询
                            OaWeekday weekday = new OaWeekday();
                            weekday.setRelationId(oaAttendanceGroup.getId());
                            //查询是否必须打卡
                            weekday.setType("2");
                            weekday.setAttendanceStart(queryTime);
                            List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(mustWeekdays.size() > 0){
                                if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                    //必须打卡日期
                                    //缓存打卡数据
                                    if(oaClocks.size() == 0){
                                        OaClock oaClock = new OaClock();
                                        oaClock.setId(getRedisIncreID.getId());
                                        oaClock.setFlag(1L);
                                        oaClock.setClockType("1");
                                        oaClock.setType("1");
                                        oaClock.setEmployeeId(user.getUserId());
                                        //租户ID
                                        oaClock.setTenantId(user.getComId());
                                        oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                        oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                        oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                        oaClock.setCreateTime(DateUtils.getNowDate());
                                        oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                        oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                        oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                        oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                        oaClock.setClockState("0");
                                        oaClockMapper.insertOaClock(oaClock);
                                        //下班打卡数据
                                        OaClock oaClock1 = new OaClock();
                                        BeanUtils.copyBeanProp(oaClock1, oaClock);
                                        oaClock1.setId(getRedisIncreID.getId());
                                        oaClock1.setClockType("2");
                                        oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                        oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                        oaClockMapper.insertOaClock(oaClock1);
                                    }else {
                                        oaClocks.stream().forEach(itm ->{
                                            //如有上班打卡时间或者下班打卡时间不更新
                                            if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                itm.setClockState("0");
                                                oaClockMapper.updateOaClock(itm);
                                            }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                itm.setClockState("0");
                                                oaClockMapper.updateOaClock(itm);
                                            }
                                        });
                                    }
                                }
                            }
                            //查询是否无需打卡
                            weekday.setType("3");
                            List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(oaWeekdays1.size() > 0){
                                if(oaClocks.size() == 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    oaClocks.stream().forEach(itm ->{
                                        //如有上班打卡时间或者下班打卡时间不更新
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                            //必须打卡与无需打卡不存在
                            if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0) {
                                OaHoliday oaHoliday = new OaHoliday();
                                oaHoliday.setHolidaysDate(queryTime);
                                oaHoliday.setHoliday("true");
                                List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                                if(oaHolidays.size() > 0){
                                    if(oaClocks.size() == 0){
                                        //缓存打卡数据
                                        OaClock oaClock = new OaClock();
                                        oaClock.setId(getRedisIncreID.getId());
                                        oaClock.setFlag(1L);
                                        oaClock.setClockType("1");
                                        oaClock.setType("1");
                                        oaClock.setEmployeeId(user.getUserId());
                                        //租户ID
                                        oaClock.setTenantId(user.getComId());
                                        oaClock.setClockState("5");
                                        oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                        oaClock.setCreateTime(DateUtils.getNowDate());
                                        oaClockMapper.insertOaClock(oaClock);
                                        //下班打卡数据
                                        OaClock oaClock1 = new OaClock();
                                        BeanUtils.copyBeanProp(oaClock1, oaClock);
                                        oaClock1.setId(getRedisIncreID.getId());
                                        oaClock1.setClockType("2");
                                        oaClockMapper.insertOaClock(oaClock1);
                                    }else {
                                        oaClocks.stream().forEach(itm ->{
                                            //如有上班打卡时间或者下班打卡时间不更新
                                            if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setAttendanceType("0");
                                                itm.setClockState("5");
                                                oaClockMapper.updateOaClock(itm);
                                            }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setAttendanceType("0");
                                                itm.setClockState("5");
                                                oaClockMapper.updateOaClock(itm);
                                            }
                                        });
                                    }
                                }
                                weekday.setType("1");
                                weekday.setAttendanceStart("");
                                try {
                                    //前一天时间转周几
                                    weekday.setName(DateUtils.dateToWeek(queryTime));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(oaHolidays.size() == 0){
                                    if("1".equals(oaAttendanceGroup.getAttendanceType())){
                                        //固定班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        //获取班次信息
                                        if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                            if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                                if(oaClocks.size() == 0){
                                                    //缓存打卡数据
                                                    OaClock oaClock = new OaClock();
                                                    oaClock.setId(getRedisIncreID.getId());
                                                    oaClock.setFlag(1L);
                                                    oaClock.setType("1");
                                                    oaClock.setClockType("1");
                                                    oaClock.setEmployeeId(user.getUserId());
                                                    //租户ID
                                                    oaClock.setTenantId(user.getComId());
                                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                    oaClock.setClockState("0");
                                                    oaClockMapper.insertOaClock(oaClock);
                                                    //下班打卡数据
                                                    OaClock oaClock1 = new OaClock();
                                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                    oaClock1.setId(getRedisIncreID.getId());
                                                    oaClock1.setClockType("2");
                                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                    oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                    oaClockMapper.insertOaClock(oaClock1);
                                                }else {
                                                    oaClocks.stream().forEach(itm ->{
                                                        //如有上班打卡时间或者下班打卡时间不更新
                                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                            itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                            itm.setClockState("0");
                                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                            oaClockMapper.updateOaClock(itm);
                                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                            itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                            itm.setClockState("0");
                                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                            oaClockMapper.updateOaClock(itm);
                                                        }
                                                    });
                                                }
                                            }else {
                                                if(oaClocks.size() == 0){
                                                    //缓存打卡数据
                                                    OaClock oaClock = new OaClock();
                                                    oaClock.setId(getRedisIncreID.getId());
                                                    oaClock.setFlag(1L);
                                                    oaClock.setClockType("1");
                                                    oaClock.setType("1");
                                                    oaClock.setEmployeeId(user.getUserId());
                                                    //租户ID
                                                    oaClock.setTenantId(user.getComId());
                                                    oaClock.setClockState("5");
                                                    oaClock.setAttendanceType("0");
                                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                                    oaClockMapper.insertOaClock(oaClock);
                                                    //下班打卡数据
                                                    OaClock oaClock1 = new OaClock();
                                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                    oaClock1.setId(getRedisIncreID.getId());
                                                    oaClock1.setClockType("2");
                                                    oaClockMapper.insertOaClock(oaClock1);
                                                }else {
                                                    oaClocks.stream().forEach(itm ->{
                                                        //如有上班打卡时间或者下班打卡时间不更新
                                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setAttendanceType("0");
                                                            itm.setClockState("5");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setAttendanceType("0");
                                                            itm.setClockState("5");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }else if("2".equals(oaAttendanceGroup.getAttendanceType())){
                                        //弹性班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        //弹性工作日不包含本天设置为休假
                                        if(oaWeekdays.size() == 0){
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    //如有上班打卡时间或者下班打卡时间不更新
                                                    if("1".equals(itm.getClockType())){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType())){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }else {
                                            OaWeekday oaWeekday = oaWeekdays.get(0);
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                oaClock.setWorkHours(oaWeekday.getWorkHours());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setClockState("0");
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    //如有上班打卡时间或者下班打卡时间不更新
                                                    if("1".equals(itm.getClockType())){
                                                        itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                        itm.setWorkHours(oaWeekday.getWorkHours());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType())){
                                                        itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                        itm.setWorkHours(oaWeekday.getWorkHours());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            //判断是否有已通过的请假
                            OaLeave leave = new OaLeave();
                            leave.setUserId(user.getUserId());
                            leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                            leave.setStart(queryTime);
                            leave.setEnd(queryTime);
                            List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                            //判断当天是否有外出
                            OaEgress egress = new OaEgress();
                            egress.setUserId(user.getUserId());
                            egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                            egress.setStart(queryTime);
                            egress.setEnd(queryTime);
                            //外出信息
                            List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                            //判断是否有已通过的出差信息
                            OaEvection evection = new OaEvection();
                            evection.setUserId(user.getUserId());
                            evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                            evection.setStartTime(queryTime);
                            evection.setEndTime(queryTime);
                            List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                            //执行类型
                            String executeType = "";
                            //请假为空其余不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }else {
                                    executeType = "evection";
                                }
                            }
                            //请假、外出不为空出差为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                //请假开始与结束都是当天上午或者下午并且外出不能重复
                                if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                        || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaLeaves.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //外出开始与结束都是当天上午或者下午
                                if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                        || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaEgresses.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }else {
                                    executeType = "egress";
                                }
                            }
                            //请假、出差不为空外出为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }else {
                                    executeType = "egress";
                                }
                            }
                            //请假、出差、外出都不为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //请假时间在外出时间之后又在出差之后：以请假为准
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }
                                //外出时间在请假时间之后又在出差时间之后
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }
                                //出差时间在请假时间之后又在出差时间之后
                                if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "evection";
                                }
                            }
                            //请假不为空其余为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                                executeType = "leave";
                            }
                            //请假、外出为空出差不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                executeType = "evection";
                            }
                            //请假、出差为空外出不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                executeType = "egress";
                            }

                            //请假信息
                            if("leave".equals(executeType)){
                                if(oaLeaves.size() > 0){
                                    OaLeave oaLeave = oaLeaves.get(0);
                                    //查看每次行程每一天
                                    String[] leaveSplit = oaLeave.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(leaveSplit);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //出差时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaLeave.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为出差
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaLeave.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为请假
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaLeave.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);

                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }


                            //外出信息
                            if("egress".equals(executeType)){
                                if(oaEgresses.size() > 0){
                                    OaEgress oaEgress = oaEgresses.get(0);
                                    //查看每一天外出
                                    String[] egressSplit = oaEgress.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(egressSplit);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //出差时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEgress.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaEgress.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEgress.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);

                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //出差申请
                            if("evection".equals(executeType)){
                                //拿到最后审批通过数据
                                if(oaEvections.size() > 0 ){
                                    OaEvection evection1 = oaEvections.get(0);
                                    //查询所有行程
                                    QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                    queryWrapper.eq("evection_id",evection1.getId());
                                    List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                    for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                        //查看每次行程每一天
                                        String[] split1 = oaEvectionTrip.getEveryDay().split(",");
                                        //数组转list
                                        List<String> list1 = Arrays.asList(split1);
                                        if(list1.size() == 1){
                                            //判断时间为今天
                                            //出差时间为一天
                                            if(list1.get(0).equals(queryTime)){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                    if("1".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("1")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("2")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }

                                            }
                                        }else {
                                            for (String day:list1) {
                                                if(day.equals(queryTime)){
                                                    //拿到第一个
                                                    if(day.equals(list1.get(0))){
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getStartCycle())){
                                                                //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);

                                                            }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                                //修改下班打卡数据
                                                                if("2".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }
                                                        }
                                                    }else if(day.equals(list1.get(list1.size()-1))){
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getEndCycle())){
                                                                //修改上班打卡数据
                                                                if("1".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                                //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }else {
                                                        //上班下班都要更改
                                                        for (OaClock oaClock:oaClocks) {
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                result = oaAttendanceGroupMapper.insertOaAttendanceGroup(oaAttendanceGroup);
            }
        }

        return result;
    }

    /**
     * 延迟生效
     */
    @Override
    public int delayOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup) {
        if(StringUtils.isNotEmpty(oaAttendanceGroup.getParentId())){
            oaAttendanceGroup.setId(getRedisIncreID.getId());
            oaAttendanceGroup.setCreateTime(DateUtils.getNowDate());
            oaAttendanceGroup.setState("1");
            //租户ID
            oaAttendanceGroup.setTenantId(SecurityUtils.getCurrComId());
            /*//查询原数据
            OaAttendanceGroup oaAttendanceGroup1 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getParentId());
            //考勤组成员
            if(StringUtils.isNotEmpty(oaAttendanceGroup1.getAttendanceMembers())){
                String[] split = oaAttendanceGroup1.getAttendanceMembers().split(",");
                //现有数据考勤组成员
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
                    String[] split1 = oaAttendanceGroup.getAttendanceMembers().split(",");
                    //对比新增考勤组成员
                    List<String> compare = compare(split, split1);
                    for (String deptId:compare) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(oaAttendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }
            }*/
            //工作日设置
            if(oaAttendanceGroup.getOaWeekdays().size() > 0){
                for (OaWeekday weekday:oaAttendanceGroup.getOaWeekdays()) {
                    weekday.setId(getRedisIncreID.getId());
                    weekday.setRelationId(oaAttendanceGroup.getId());
                    weekday.setCreateTime(DateUtils.getNowDate());
                    weekday.setType("1");
                    oaWeekdayMapper.insertOaWeekday(weekday);
                }
            }
            //地点
            if(oaAttendanceGroup.getOaAttendanceAddresses().size() > 0){
                for (OaAttendanceAddress attendanceAddress:oaAttendanceGroup.getOaAttendanceAddresses()) {
                    attendanceAddress.setId(getRedisIncreID.getId());
                    attendanceAddress.setRelationId(oaAttendanceGroup.getId());
                    attendanceAddress.setCreateTime(DateUtils.getNowDate());
                    oaAttendanceAddressMapper.insertOaAttendanceAddress(attendanceAddress);
                }
            }
            //必须打卡
            if(oaAttendanceGroup.getMustSpecialSettingsList().size() > 0){
                for (OaWeekday weekday:oaAttendanceGroup.getMustSpecialSettingsList()) {
                    weekday.setId(getRedisIncreID.getId());
                    weekday.setRelationId(oaAttendanceGroup.getId());
                    weekday.setCreateTime(DateUtils.getNowDate());
                    weekday.setType("2");
                    oaWeekdayMapper.insertOaWeekday(weekday);
                }
            }
            //无需打卡
            if(oaAttendanceGroup.getNotSpecialSettingsList().size() > 0){
                for (OaWeekday weekday:oaAttendanceGroup.getNotSpecialSettingsList()) {
                    weekday.setId(getRedisIncreID.getId());
                    weekday.setRelationId(oaAttendanceGroup.getId());
                    weekday.setCreateTime(DateUtils.getNowDate());
                    weekday.setType("3");
                    oaWeekdayMapper.insertOaWeekday(weekday);
                }
            }
            //伪删除原数据
            OaAttendanceGroup oaAttendanceGroup2 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getParentId());
            oaAttendanceGroup2.setFlag(1L);
            oaAttendanceGroupMapper.updateOaAttendanceGroup(oaAttendanceGroup2);
        }else {
            return 0;
        }
        return oaAttendanceGroupMapper.insertOaAttendanceGroup(oaAttendanceGroup);
    }

    /**
     * 修改考勤组
     *
     * @param oaAttendanceGroup 考勤组
     * @return 结果
     */
    @Override
    public int updateOaAttendanceGroup(OaAttendanceGroup oaAttendanceGroup)
    {
        //钉钉创建考勤组
        //钉钉修改用户
        String msg = "";
        if(SecurityUtils.getCurrComId().equals("123456") && StringUtils.isNotEmpty(oaAttendanceGroup.getDingDing())){
            try {
                //groupId转换为groupKey
                String groupIdToKey = dingDingService.groupIdToKey(oaAttendanceGroup.getDingDing());
                Object parse2 = JSONObject.parse(groupIdToKey);
                JSONObject jsonObject2 = (JSONObject) JSON.toJSON(parse2);
                String errmsg2 = jsonObject2.getString("errmsg");
                if(errmsg2.equals("ok")){
                    String groupKey = jsonObject2.getString("result");
                    //删除钉钉考勤组
                    String groupDelete = dingDingService.groupDelete(groupKey);
                    Object parse3 = JSONObject.parse(groupDelete);
                    JSONObject jsonObject3 = (JSONObject) JSON.toJSON(parse3);
                    String errmsg3 = jsonObject3.getString("errmsg");
                    if(errmsg3.equals("ok")){
                        //钉钉创建考勤组
                        String groupAdd = dingDingService.groupAdd(oaAttendanceGroup);
                        Object parse = JSONObject.parse(groupAdd);
                        JSONObject jsonObject = (JSONObject) JSON.toJSON(parse);
                        String errmsg = jsonObject.getString("errmsg");
                        if(errmsg.equals("ok")){
                            JSONObject result = jsonObject.getJSONObject("result");
                            String id = result.getString("id");
                            oaAttendanceGroup.setDingDing(id);
                            //更新参与考勤人员
                            String groupMemberUpdate = dingDingService.groupMemberUpdate(oaAttendanceGroup);
                            Object parse1 = JSONObject.parse(groupMemberUpdate);
                            JSONObject jsonObject1 = (JSONObject) JSON.toJSON(parse1);
                            String errMsg1 = jsonObject1.getString("errmsg");
                            if(errMsg1.equals("ok")){
                                msg = "success";
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            msg = "success";
        }
        int result = 0;
        if(StringUtils.isNotEmpty(msg)){
            if(msg.equals("success")){
                //日期格式化
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String queryTime = df.format(new Date());

                //立即生效释放老版本数据
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getParentId()) && !"1".equals(oaAttendanceGroup.getState())){
                    OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getParentId());
                    //判断两次考勤组考勤部门是否一样
                    if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
                        //现有考勤组部门
                        String[] split1 = oaAttendanceGroup.getAttendanceMembers().split(",");
                        if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){
                            //历史考勤组部门
                            String[] split = attendanceGroup.getAttendanceMembers().split(",");
                            //删除释放部门相关打卡初始化数据
                            List<String> compare = compare(split, split1);
                            String[] split2 = compare.toArray(new String[compare.size()]);
                            //String[] split = attendanceGroup.getAttendanceMembers().split(",");
                            List<Long> l = Arrays.stream(split2).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                            Long[] deptIds = l.toArray(new Long[l.size()]);
                            //判断是否有需要释放的部门
                            if(deptIds.length > 0){
                                List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                                for (SysUser user:sysUsers) {
                                    OaClock clock = new OaClock();
                                    clock.setQueryTime(queryTime);
                                    clock.setEmployeeId(user.getUserId());
                                    List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                                    oaClockList.stream().forEach(itm ->{
                                        oaClockMapper.deleteOaClockById(itm.getId());
                                    });
                                }
                            }
                            for (String deptId:split) {
                                //释放对应部门
                                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                                dept.setAttendanceLock("0");
                                sysDeptMapper.updateDept(dept);
                            }
                        }
                        if(StringUtils.isNotEmpty(attendanceGroup.getNonAttendanceMembers())){

                            String[] split = attendanceGroup.getNonAttendanceMembers().split(",");
                            for (String userId:split) {
                                //删除对应人员当天打卡数据
                                OaClock clock = new OaClock();
                                clock.setQueryTime(queryTime);
                                clock.setEmployeeId(Long.valueOf(userId));
                                List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                                for (OaClock oaClock:oaClockList) {
                                    oaClockMapper.deleteOaClockById(oaClock.getId());
                                }
                                //释放不用打卡用户
                                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                                user.setNeedNot("0");
                                sysUserMapper.updateUser(user);
                            }
                        }
                    }
                }
                //原数据
                OaAttendanceGroup oaAttendanceGroup1 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getId());

                oaAttendanceGroup.setUpdateTime(DateUtils.getNowDate());
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers()) && !"1".equals(oaAttendanceGroup.getState())){
                    //释放原有数据考勤人员
                    if(StringUtils.isNotEmpty(oaAttendanceGroup1.getAttendanceMembers())){
                        //现有考勤组部门
                        String[] split1 = oaAttendanceGroup.getAttendanceMembers().split(",");
                        //删除释放部门相关打卡初始化数据
                        String[] split = oaAttendanceGroup1.getAttendanceMembers().split(",");
                        List<String> compare = compare(split, split1);
                        String[] split2 = compare.toArray(new String[compare.size()]);
                        List<Long> l = Arrays.stream(split2).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                        Long[] deptIds = l.toArray(new Long[l.size()]);
                        //判断是否有需要释放的部门
                        if(deptIds.length > 0){
                            List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                            for (SysUser user:sysUsers) {
                                OaClock clock = new OaClock();
                                clock.setQueryTime(queryTime);
                                clock.setEmployeeId(user.getUserId());
                                List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                                oaClockList.stream().forEach(itm ->{
                                    oaClockMapper.deleteOaClockById(itm.getId());
                                });
                            }
                        }
                        for (String deptId:split) {
                            //释放对应部门
                            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                            dept.setAttendanceLock("0");
                            sysDeptMapper.updateDept(dept);
                        }
                    }
                    //锁定对应部门
                    String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
                    for (String deptId:split) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(oaAttendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }else {
            /*//查询原生效数据
            if(StringUtils.isNotEmpty(oaAttendanceGroup.getParentId())){
                OaAttendanceGroup oaAttendanceGroup2 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getParentId());
                String[] split = oaAttendanceGroup2.getAttendanceMembers().split(",");
                //现有数据考勤组成员
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
                    String[] split1 = oaAttendanceGroup.getAttendanceMembers().split(",");
                    //对比新增考勤组成员
                    List<String> compare = compare(split, split1);
                    for (String deptId:compare) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(oaAttendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }
            }*/
                    //释放原
                }
                if(!"1".equals(oaAttendanceGroup.getState())){
                    //原人员释放
                    if(StringUtils.isNotEmpty(oaAttendanceGroup1.getNonAttendanceMembers())){
                        //释放不用打卡用户
                        String[] split = oaAttendanceGroup1.getNonAttendanceMembers().split(",");
                        for (String userId:split) {
                            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                            user.setNeedNot("0");
                            sysUserMapper.updateUser(user);
                        }
                    }
                    if(StringUtils.isNotEmpty(oaAttendanceGroup.getNonAttendanceMembers())){
                        String[] split = oaAttendanceGroup.getNonAttendanceMembers().split(",");
                        for (String userId:split) {
                            //删除对应人员当天打卡数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(Long.valueOf(userId));
                            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                            for (OaClock oaClock:oaClockList) {
                                oaClockMapper.deleteOaClockById(oaClock.getId());
                            }
                            //标记不用打卡用户
                            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                            user.setNeedNot("1");
                            sysUserMapper.updateUser(user);
                        }
                    }
                }
                //工作日设置
                if(oaAttendanceGroup.getOaWeekdays().size() > 0){
                    //删除历史数据工作日设置信息

                    QueryWrapper<OaWeekday> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("relation_id",oaAttendanceGroup1.getId());
                    queryWrapper.eq("type","1");
                    List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectList(queryWrapper);
                    oaWeekdays.stream().forEach(itm ->{
                        oaWeekdayMapper.deleteOaWeekdayById(itm.getId());
                    });

                    for (OaWeekday weekday:oaAttendanceGroup.getOaWeekdays()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("1");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //地点
                if(oaAttendanceGroup.getOaAttendanceAddresses().size() > 0){
                    //删除历史数据地点信息

                    OaAttendanceAddress oaAttendanceAddress = new OaAttendanceAddress();
                    oaAttendanceAddress.setRelationId(oaAttendanceGroup1.getId());
                    List<OaAttendanceAddress> oaAttendanceAddresses = oaAttendanceAddressMapper.selectOaAttendanceAddressList(oaAttendanceAddress);
                    oaAttendanceAddresses.stream().forEach(itm->{
                        oaAttendanceAddressMapper.deleteOaAttendanceAddressById(itm.getId());
                    });
                    //新增地点信息
                    for (OaAttendanceAddress attendanceAddress:oaAttendanceGroup.getOaAttendanceAddresses()) {
                        attendanceAddress.setId(getRedisIncreID.getId());
                        attendanceAddress.setRelationId(oaAttendanceGroup.getId());
                        attendanceAddress.setCreateTime(DateUtils.getNowDate());
                        oaAttendanceAddressMapper.insertOaAttendanceAddress(attendanceAddress);
                    }
                }
                //必须打卡
                //删除原有必须打卡规则
                OaWeekday mustWeekday = new OaWeekday();
                mustWeekday.setRelationId(oaAttendanceGroup1.getId());
                mustWeekday.setType("2");
                List<OaWeekday> aa = oaWeekdayMapper.selectOaWeekdayList(mustWeekday);
                aa.stream().forEach(itm->{
                    oaWeekdayMapper.deleteOaWeekdayById(itm.getId());
                });
                if(oaAttendanceGroup.getMustSpecialSettingsList().size() > 0){
                    for (OaWeekday weekday:oaAttendanceGroup.getMustSpecialSettingsList()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("2");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //无需打卡
                //删除原有无需打卡配置
                mustWeekday.setType("3");
                List<OaWeekday> weekdays = oaWeekdayMapper.selectOaWeekdayList(mustWeekday);
                weekdays.stream().forEach(itm->{
                    oaWeekdayMapper.deleteOaWeekdayById(itm.getId());
                });
                if(oaAttendanceGroup.getNotSpecialSettingsList().size() > 0){
                    for (OaWeekday weekday:oaAttendanceGroup.getNotSpecialSettingsList()) {
                        weekday.setId(getRedisIncreID.getId());
                        weekday.setRelationId(oaAttendanceGroup.getId());
                        weekday.setCreateTime(DateUtils.getNowDate());
                        weekday.setType("3");
                        oaWeekdayMapper.insertOaWeekday(weekday);
                    }
                }
                //伪删除原有数据
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getParentId()) && "0".equals(oaAttendanceGroup.getState())){
                    OaAttendanceGroup oaAttendanceGroup2 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(oaAttendanceGroup.getParentId());
                    oaAttendanceGroup2.setFlag(1L);
                    oaAttendanceGroup2.setState("3");
                    oaAttendanceGroupMapper.updateOaAttendanceGroup(oaAttendanceGroup2);
                }
                //立即生效创建打卡历史数据
                if("0".equals(oaAttendanceGroup.getState())){
                    //新增打卡缓存数据
                    //获取所有考勤组成员
                    String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
                    List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                    Long[] deptIds = l.toArray(new Long[l.size()]);
                    List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                    //无需打卡人员
            /*String nonAttendanceMembers = oaAttendanceGroup.getNonAttendanceMembers();
            String[] split1 = nonAttendanceMembers.split(",");*/
                    for (SysUser user:sysUsers) {
                        //无需打卡人员不初始化打卡数据
                        if("0".equals(user.getNeedNot())){
                            //查询打卡记录表中存在今天数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(user.getUserId());
                            //clock.setFlag(1L);
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            //工作日查询
                            OaWeekday weekday = new OaWeekday();
                            weekday.setRelationId(oaAttendanceGroup.getId());
                            //查询是否必须打卡
                            weekday.setType("2");
                            weekday.setAttendanceStart(queryTime);
                            List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(mustWeekdays.size() > 0){
                                if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                    //必须打卡日期
                                    //缓存打卡数据
                                    if(oaClocks.size() == 0){
                                        OaClock oaClock = new OaClock();
                                        oaClock.setId(getRedisIncreID.getId());
                                        oaClock.setFlag(1L);
                                        oaClock.setClockType("1");
                                        oaClock.setType("1");
                                        oaClock.setEmployeeId(user.getUserId());
                                        //租户ID
                                        oaClock.setTenantId(user.getComId());
                                        oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                        oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                        oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                        oaClock.setCreateTime(DateUtils.getNowDate());
                                        oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                        oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                        oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                        oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                        oaClock.setClockState("0");
                                        oaClockMapper.insertOaClock(oaClock);
                                        //下班打卡数据
                                        OaClock oaClock1 = new OaClock();
                                        BeanUtils.copyBeanProp(oaClock1, oaClock);
                                        oaClock1.setId(getRedisIncreID.getId());
                                        oaClock1.setClockType("2");
                                        oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                        oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                        oaClockMapper.insertOaClock(oaClock1);
                                    }else {
                                        oaClocks.stream().forEach(itm ->{
                                            //如有上班打卡时间或者下班打卡时间不更新
                                            if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                itm.setClockState("0");
                                                itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClockMapper.updateOaClock(itm);
                                            }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                itm.setClockState("0");
                                                itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClockMapper.updateOaClock(itm);
                                            }
                                        });
                                    }
                                }
                            }
                            //查询是否无需打卡
                            weekday.setType("3");
                            List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(oaWeekdays1.size() > 0){
                                if(oaClocks.size() == 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType("0");
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    oaClocks.stream().forEach(itm ->{
                                        //如有上班打卡时间或者下班打卡时间不更新
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                            //必须打卡与无需打卡不存在
                            if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0) {
                                OaHoliday oaHoliday = new OaHoliday();
                                oaHoliday.setHolidaysDate(queryTime);
                                oaHoliday.setHoliday("true");
                                List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                                if(oaHolidays.size() > 0){
                                    if(oaClocks.size() == 0){
                                        //缓存打卡数据
                                        OaClock oaClock = new OaClock();
                                        oaClock.setId(getRedisIncreID.getId());
                                        oaClock.setFlag(1L);
                                        oaClock.setClockType("1");
                                        oaClock.setType("1");
                                        oaClock.setEmployeeId(user.getUserId());
                                        //租户ID
                                        oaClock.setTenantId(user.getComId());
                                        oaClock.setClockState("5");
                                        oaClock.setAttendanceType("0");
                                        oaClock.setCreateTime(DateUtils.getNowDate());
                                        oaClockMapper.insertOaClock(oaClock);
                                        //下班打卡数据
                                        OaClock oaClock1 = new OaClock();
                                        BeanUtils.copyBeanProp(oaClock1, oaClock);
                                        oaClock1.setId(getRedisIncreID.getId());
                                        oaClock1.setClockType("2");
                                        oaClockMapper.insertOaClock(oaClock1);
                                    }else {
                                        oaClocks.stream().forEach(itm ->{
                                            //如有上班打卡时间或者下班打卡时间不更新
                                            if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setAttendanceType("0");
                                                itm.setClockState("5");
                                                oaClockMapper.updateOaClock(itm);
                                            }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                itm.setAttendanceType("0");
                                                itm.setClockState("5");
                                                oaClockMapper.updateOaClock(itm);
                                            }
                                        });
                                    }
                                }
                                weekday.setType("1");
                                weekday.setAttendanceStart("");
                                try {
                                    //前一天时间转周几
                                    weekday.setName(DateUtils.dateToWeek(queryTime));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(oaHolidays.size() == 0){
                                    if("1".equals(oaAttendanceGroup.getAttendanceType())){
                                        //固定班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        //获取班次信息
                                        if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                            if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                                if(oaClocks.size() == 0){
                                                    //缓存打卡数据
                                                    OaClock oaClock = new OaClock();
                                                    oaClock.setId(getRedisIncreID.getId());
                                                    oaClock.setFlag(1L);
                                                    oaClock.setType("1");
                                                    oaClock.setClockType("1");
                                                    oaClock.setEmployeeId(user.getUserId());
                                                    //租户ID
                                                    oaClock.setTenantId(user.getComId());
                                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                    oaClock.setClockState("0");
                                                    oaClockMapper.insertOaClock(oaClock);
                                                    //下班打卡数据
                                                    OaClock oaClock1 = new OaClock();
                                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                    oaClock1.setId(getRedisIncreID.getId());
                                                    oaClock1.setClockType("2");
                                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                    oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                    oaClockMapper.insertOaClock(oaClock1);
                                                }else {
                                                    oaClocks.stream().forEach(itm ->{
                                                        //如有上班打卡时间或者下班打卡时间不更新
                                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                            itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                            itm.setClockState("0");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                            itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                            itm.setClockState("0");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }
                                                    });
                                                }
                                            }else {
                                                if(oaClocks.size() == 0){
                                                    //缓存打卡数据
                                                    OaClock oaClock = new OaClock();
                                                    oaClock.setId(getRedisIncreID.getId());
                                                    oaClock.setFlag(1L);
                                                    oaClock.setClockType("1");
                                                    oaClock.setType("1");
                                                    oaClock.setEmployeeId(user.getUserId());
                                                    //租户ID
                                                    oaClock.setTenantId(user.getComId());
                                                    oaClock.setClockState("5");
                                                    oaClock.setAttendanceType("0");
                                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                                    oaClockMapper.insertOaClock(oaClock);
                                                    //下班打卡数据
                                                    OaClock oaClock1 = new OaClock();
                                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                    oaClock1.setId(getRedisIncreID.getId());
                                                    oaClock1.setClockType("2");
                                                    oaClockMapper.insertOaClock(oaClock1);
                                                }else {
                                                    oaClocks.stream().forEach(itm ->{
                                                        //如有上班打卡时间或者下班打卡时间不更新
                                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setAttendanceType("0");
                                                            itm.setClockState("5");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                            itm.setAttendanceType("0");
                                                            itm.setClockState("5");
                                                            oaClockMapper.updateOaClock(itm);
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    }else if("2".equals(oaAttendanceGroup.getAttendanceType())){
                                        //弹性班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        //弹性工作日不包含本天设置为休假
                                        if(oaWeekdays.size() == 0){
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    //如有上班打卡时间或者下班打卡时间不更新
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }else {
                                            OaWeekday oaWeekday = oaWeekdays.get(0);
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                oaClock.setWorkHours(oaWeekday.getWorkHours());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                oaClock.setClockState("0");
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    //如有上班打卡时间或者下班打卡时间不更新
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                        itm.setWorkHours(oaWeekday.getWorkHours());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                        itm.setWorkHours(oaWeekday.getWorkHours());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            //判断是否有已通过的请假
                            OaLeave leave = new OaLeave();
                            leave.setUserId(user.getUserId());
                            leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                            leave.setStart(queryTime);
                            leave.setEnd(queryTime);
                            List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                            //判断当天是否有外出
                            OaEgress egress = new OaEgress();
                            egress.setUserId(user.getUserId());
                            egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                            egress.setStart(queryTime);
                            egress.setEnd(queryTime);
                            //外出信息
                            List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                            //判断是否有已通过的出差信息
                            OaEvection evection = new OaEvection();
                            evection.setUserId(user.getUserId());
                            evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                            evection.setStartTime(queryTime);
                            evection.setEndTime(queryTime);
                            List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                            //执行类型
                            String executeType = "";
                            //请假为空其余不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }else {
                                    executeType = "evection";
                                }
                            }
                            //请假、外出不为空出差为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                //请假开始与结束都是当天上午或者下午并且外出不能重复
                                if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                        || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaLeaves.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //外出开始与结束都是当天上午或者下午
                                if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                        || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaEgresses.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }else {
                                    executeType = "egress";
                                }
                            }
                            //请假、出差不为空外出为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }else {
                                    executeType = "evection";
                                }
                            }
                            //请假、出差、外出都不为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //请假时间在外出时间之后又在出差之后：以请假为准
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leave";
                                }
                                //外出时间在请假时间之后又在出差时间之后
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }
                                //出差时间在请假时间之后又在出差时间之后
                                if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "evection";
                                }
                            }
                            //请假不为空其余为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                                executeType = "leave";
                            }
                            //请假、外出为空出差不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                executeType = "evection";
                            }
                            //请假、出差为空外出不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                executeType = "egress";
                            }

                            //请假信息
                            if("leave".equals(executeType)){
                                if(oaLeaves.size() > 0){
                                    OaLeave oaLeave = oaLeaves.get(0);
                                    //查看每次行程每一天
                                    String[] leaveSplit = oaLeave.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(leaveSplit);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //出差时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaLeave.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);

                                                        }else if("2".equals(oaLeave.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaLeave.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为出差
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }


                            //外出信息
                            if("egress".equals(executeType)){
                                if(oaEgresses.size() > 0){
                                    OaEgress oaEgress = oaEgresses.get(0);
                                    //查看每一天外出
                                    String[] egressSplit = oaEgress.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(egressSplit);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //外出时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEgress.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);

                                                        }else if("2".equals(oaEgress.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEgress.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //出差申请
                            if("evection".equals(executeType)){
                                //拿到最后审批通过数据
                                if(oaEvections.size() > 0 ){
                                    OaEvection evection1 = oaEvections.get(0);
                                    //查询所有行程
                                    QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                    queryWrapper.eq("evection_id",evection1.getId());
                                    List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                    for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                        //查看每次行程每一天
                                        String[] split1 = oaEvectionTrip.getEveryDay().split(",");
                                        //数组转list
                                        List<String> list1 = Arrays.asList(split1);
                                        if(list1.size() == 1){
                                            //判断时间为今天
                                            //出差时间为一天
                                            if(list1.get(0).equals(queryTime)){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                    if("1".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("1")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("2")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }

                                            }
                                        }else {
                                            for (String day:list1) {
                                                if(day.equals(queryTime)){
                                                    //拿到第一个
                                                    if(day.equals(list1.get(0))){
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getStartCycle())){
                                                                //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                                //修改下班打卡数据
                                                                if("2".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }
                                                        }
                                                    }else if(day.equals(list1.get(list1.size()-1))){
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getEndCycle())){
                                                                //修改上班打卡数据
                                                                if("1".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                                //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }else {
                                                        //上班下班都要更改
                                                        for (OaClock oaClock:oaClocks) {
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                result = oaAttendanceGroupMapper.updateOaAttendanceGroup(oaAttendanceGroup);
            }
        }

        return result;
    }

    /**
     * 批量删除考勤组
     *
     * @param ids 需要删除的考勤组ID
     * @return 结果
     */
    @Override
    public int deleteOaAttendanceGroupByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                String msg = "";
                OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(id);
                if(StringUtils.isNotEmpty(oaAttendanceGroup.getDingDing())){
                    try {
                        //groupId转换为groupKey
                        String groupIdToKey = dingDingService.groupIdToKey(oaAttendanceGroup.getDingDing());
                        Object parse2 = JSONObject.parse(groupIdToKey);
                        JSONObject jsonObject2 = (JSONObject) JSON.toJSON(parse2);
                        String errmsg2 = jsonObject2.getString("errmsg");
                        if(errmsg2.equals("ok")){
                            String groupKey = jsonObject2.getString("result");
                            //删除钉钉考勤组
                            String groupDelete = dingDingService.groupDelete(groupKey);
                            Object parse3 = JSONObject.parse(groupDelete);
                            JSONObject jsonObject3 = (JSONObject) JSON.toJSON(parse3);
                            String errmsg3 = jsonObject3.getString("errmsg");
                            if(errmsg3.equals("ok")){
                               msg = "success";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    msg = "success";
                }
                //钉钉数据处理完成
                if(StringUtils.isNotEmpty(msg)){
                    if(msg.equals("success")){
                        //释放考勤部门
                        if(StringUtils.isNotEmpty(oaAttendanceGroup.getAttendanceMembers())){
                            //释放对应部门
                            String[] split = oaAttendanceGroup.getAttendanceMembers().split(",");
                            for (String deptId:split) {
                                SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                                dept.setAttendanceLock("0");
                                sysDeptMapper.updateDept(dept);
                            }
                        }
                        //原人员释放
                        if(StringUtils.isNotEmpty(oaAttendanceGroup.getNonAttendanceMembers())){
                            //释放不用打卡用户
                            String[] split = oaAttendanceGroup.getNonAttendanceMembers().split(",");
                            for (String userId:split) {
                                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                                user.setNeedNot("0");
                                sysUserMapper.updateUser(user);
                            }
                        }
                        oaAttendanceGroup.setFlag(1L);
                        oaAttendanceGroupMapper.updateOaAttendanceGroup(oaAttendanceGroup);
                    }
                }else {
                    return 0;
                }
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除考勤组信息
     *
     * @param id 考勤组ID
     * @return 结果
     */
    @Override
    public int deleteOaAttendanceGroupById(String id)
    {
        return oaAttendanceGroupMapper.deleteOaAttendanceGroupById(id);
    }

    @Override
    public ClockInVO CheckRules(String userId){
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(new Date());

        // 获取当前日期
        Calendar calendar = Calendar.getInstance();
        // 将日期减1
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        String previousDay = df.format(calendar.getTime());

        ClockInVO clockInVO = new ClockInVO();
        //查询用户信息是否无需打卡
        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
        if("0".equals(user.getNeedNot())){
            clockInVO.setClockIn("0");
        }else {
            clockInVO.setClockIn("1");
        }
        //查询对应的部门
        SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
        if(!"0".equals(dept.getAttendanceLock())){
            //查询考勤组
            OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                clockInVO.setAttendanceName(oaAttendanceGroup.getAttendanceName());
                clockInVO.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                clockInVO.setFieldClock(oaAttendanceGroup.getFieldClock());
                //考勤打卡范围
                OaAttendanceAddress oaAttendanceAddress = new OaAttendanceAddress();
                oaAttendanceAddress.setRelationId(oaAttendanceGroup.getId());
                List<OaAttendanceAddress> oaAttendanceAddresses = oaAttendanceAddressMapper.selectOaAttendanceAddressList(oaAttendanceAddress);
                clockInVO.setAttendanceAddresses(oaAttendanceAddresses);
                //查询是否必须打卡
                OaWeekday weekday = new OaWeekday();
                weekday.setRelationId(oaAttendanceGroup.getId());
                weekday.setType("2");
                weekday.setAttendanceStart(queryTime);
                List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                if(oaWeekdays.size() > 0){
                    clockInVO.setHoliday("1");
                    if(StringUtils.isNotEmpty(oaWeekdays.get(0).getWorkTimeId())){
                        OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekdays.get(0).getWorkTimeId());
                        clockInVO.setOaWorkTimes(oaWorkTimes);
                    }
                }
                //查询是否无需打卡
                weekday.setType("3");
                List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                if(oaWeekdays1.size() > 0){
                    clockInVO.setHoliday("0");
                }
                //查询国定节假日
                if(oaWeekdays.size() == 0 && oaWeekdays1.size() == 0){
                    OaHoliday oaHoliday = new OaHoliday();
                    oaHoliday.setHolidaysDate(queryTime);
                    oaHoliday.setHoliday("true");
                    List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                    if(oaHolidays.size() > 0){
                        clockInVO.setHoliday("0");
                    }
                    weekday.setType("1");
                    weekday.setAttendanceStart("");
                    try {
                        //当前时间转周几
                        weekday.setName(DateUtils.dateToWeek(queryTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(oaHolidays.size() == 0){
                        if("1".equals(oaAttendanceGroup.getAttendanceType())){
                            //固定班制
                            List<OaWeekday> oaWeekdays2 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            OaWeekday oaWeekday = oaWeekdays2.get(0);
                            if(StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())){
                                if(!"0".equals(oaWeekday.getWorkTimeId())){
                                    clockInVO.setHoliday("1");
                                }else {
                                    clockInVO.setHoliday("0");
                                }
                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                if(StringUtils.isNotNull(oaWorkTimes)){
                                    oaWorkTimes.setCurrentTime(queryTime);
                                    clockInVO.setOaWorkTimes(oaWorkTimes);
                                }
                            }
                            //获取前一天班次
                            try {
                                //前一天时间转周几
                                weekday.setName(DateUtils.dateToWeek(previousDay));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            List<OaWeekday> oaWeekdays3 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            OaWeekday weekday1 = oaWeekdays3.get(0);
                            if(!"0".equals(weekday1.getWorkTimeId())){
                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(weekday1.getWorkTimeId());
                                oaWorkTimes.setCurrentTime(previousDay);
                                clockInVO.setBeforeWorkTimes(oaWorkTimes);
                            }
                        }else if("2".equals(oaAttendanceGroup.getAttendanceType())){
                            List<OaWeekday> oaWeekdays2 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            //弹性工作制不包含今天
                            if(oaWeekdays2.size() == 0){
                                clockInVO.setHoliday("0");
                            }else {
                                OaWeekday oaWeekday = oaWeekdays2.get(0);
                                clockInVO.setAttendanceStart(oaWeekday.getAttendanceStart());
                                clockInVO.setWorkHours(oaWeekday.getWorkHours());
                            }
                        }
                    }

                }
            }
        return clockInVO;
    }

    //两个数组对比拿出不同数据
    public static <T> List<T> compare(T[] t1, T[] t2) {
        List<T> list1 = Arrays.asList(t1);
        List<T> list2 = new ArrayList<T>();
        for (T t : t2) {
            if (!list1.contains(t)) {
                list2.add(t);
            }
        }
        return list2;
    }
}
