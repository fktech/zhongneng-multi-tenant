package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaCargoOwner;
import com.business.system.domain.BaEnterprise;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.ICargoOwnerApprovalService;
import com.business.system.service.workflow.IEnterpriseApprovalService;
import org.springframework.stereotype.Service;


/**
 * 货主申请审批结果:审批拒绝
 */
@Service("cargoOwnerApprovalContent:processApproveResult:reject")
public class CargoOwnerApprovalRejectServiceImpl extends ICargoOwnerApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.CARGOOWNER_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaCargoOwner checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
