package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.BaBid;
import com.business.system.domain.BaSettlement;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IBidApprovalService;
import com.business.system.service.workflow.ISettlementApprovalService;
import org.springframework.stereotype.Service;


/**
 * 结算申请审批结果:审批拒绝
 */
@Service("settlementApprovalContent:processApproveResult:reject")
public class SettlementApprovalRejectServiceImpl extends ISettlementApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.SETTLEMENT_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected BaSettlement checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
