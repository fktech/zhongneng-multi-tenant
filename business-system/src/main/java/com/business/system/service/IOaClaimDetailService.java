package com.business.system.service;

import com.business.system.domain.OaClaimDetail;
import com.business.system.domain.vo.OaClaimStatVO;

import java.util.List;

/**
 * 报销明细Service接口
 *
 * @author single
 * @date 2023-12-11
 */
public interface IOaClaimDetailService
{
    /**
     * 查询报销明细
     *
     * @param id 报销明细ID
     * @return 报销明细
     */
    public OaClaimDetail selectOaClaimDetailById(String id);

    /**
     * 查询报销明细列表
     *
     * @param oaClaimDetail 报销明细
     * @return 报销明细集合
     */
    public List<OaClaimDetail> selectOaClaimDetailList(OaClaimDetail oaClaimDetail);

    /**
     * 新增报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    public int insertOaClaimDetail(OaClaimDetail oaClaimDetail);

    /**
     * 修改报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    public int updateOaClaimDetail(OaClaimDetail oaClaimDetail);

    /**
     * 批量删除报销明细
     *
     * @param ids 需要删除的报销明细ID
     * @return 结果
     */
    public int deleteOaClaimDetailByIds(String[] ids);

    /**
     * 删除报销明细信息
     *
     * @param id 报销明细ID
     * @return 结果
     */
    public int deleteOaClaimDetailById(String id);

}
