package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.BaServiceVoucherRecordMapper;
import com.business.system.domain.BaServiceVoucherRecord;
import com.business.system.service.IBaServiceVoucherRecordService;

/**
 * 进厂确认附Service业务层处理
 *
 * @author single
 * @date 2023-09-19
 */
@Service
public class BaServiceVoucherRecordServiceImpl extends ServiceImpl<BaServiceVoucherRecordMapper, BaServiceVoucherRecord> implements IBaServiceVoucherRecordService
{
    @Autowired
    private BaServiceVoucherRecordMapper baServiceVoucherRecordMapper;

    /**
     * 查询进厂确认附
     *
     * @param id 进厂确认附ID
     * @return 进厂确认附
     */
    @Override
    public BaServiceVoucherRecord selectBaServiceVoucherRecordById(String id)
    {
        return baServiceVoucherRecordMapper.selectBaServiceVoucherRecordById(id);
    }

    /**
     * 查询进厂确认附列表
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 进厂确认附
     */
    @Override
    public List<BaServiceVoucherRecord> selectBaServiceVoucherRecordList(BaServiceVoucherRecord baServiceVoucherRecord)
    {
        return baServiceVoucherRecordMapper.selectBaServiceVoucherRecordList(baServiceVoucherRecord);
    }

    /**
     * 新增进厂确认附
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 结果
     */
    @Override
    public int insertBaServiceVoucherRecord(BaServiceVoucherRecord baServiceVoucherRecord)
    {
        baServiceVoucherRecord.setCreateTime(DateUtils.getNowDate());
        return baServiceVoucherRecordMapper.insertBaServiceVoucherRecord(baServiceVoucherRecord);
    }

    /**
     * 修改进厂确认附
     *
     * @param baServiceVoucherRecord 进厂确认附
     * @return 结果
     */
    @Override
    public int updateBaServiceVoucherRecord(BaServiceVoucherRecord baServiceVoucherRecord)
    {
        baServiceVoucherRecord.setUpdateTime(DateUtils.getNowDate());
        return baServiceVoucherRecordMapper.updateBaServiceVoucherRecord(baServiceVoucherRecord);
    }

    /**
     * 批量删除进厂确认附
     *
     * @param ids 需要删除的进厂确认附ID
     * @return 结果
     */
    @Override
    public int deleteBaServiceVoucherRecordByIds(String[] ids)
    {
        return baServiceVoucherRecordMapper.deleteBaServiceVoucherRecordByIds(ids);
    }

    /**
     * 删除进厂确认附信息
     *
     * @param id 进厂确认附ID
     * @return 结果
     */
    @Override
    public int deleteBaServiceVoucherRecordById(String id)
    {
        return baServiceVoucherRecordMapper.deleteBaServiceVoucherRecordById(id);
    }

}
