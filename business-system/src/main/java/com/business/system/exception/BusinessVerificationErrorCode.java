package com.business.system.exception;


import com.business.common.exception.ErrorCode;

/**
 * @author js
 * @version 1.0
 * @date 2021/8/17
 * @since
 */
public enum BusinessVerificationErrorCode implements ErrorCode {
    VERIFICATION_PROJECT_001("PROJECT001", "立项信息不存在", "立项信息不存在"),
    VERIFICATION_PROJECT_002("PROJECT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_BID_001("BID001", "投标信息不存在", "投标信息不存在"),
    VERIFICATION_BID_002("BID002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CONTRACT_001("CONTRACT001", "合同信息不存在", "合同信息不存在"),
    VERIFICATION_CONTRACT_002("CONTRACT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_ORDER_001("ORDER001", "订单信息不存在", "订单信息不存在"),
    VERIFICATION_ORDER_002("ORDER002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_TRANSPORT_001("TRANSPORT001", "运输信息不存在", "运输信息不存在"),
    VERIFICATION_TRANSPORT_002("TRANSPORT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_PAYMENT_001("PAYMENT001", "付款信息不存在", "付款信息不存在"),
    VERIFICATION_PAYMENT_002("PAYMENT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_COLLECTION_001("COLLECTION001", "收款信息不存在", "收款信息不存在"),
    VERIFICATION_COLLECTION_002("COLLECTION002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_INVOICE_001("INVOICE001", "销项发票信息不存在", "销项发票信息不存在"),
    VERIFICATION_INVOICE_002("INVOICE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_INTODEPOT_001("INTODEPOT001", "入库信息不存在", "入库信息不存在"),
    VERIFICATION_INTODEPOT_002("INTODEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OUTPUTDEPOT_001("OUTPUTDEPOT001", "出库信息不存在", "出库信息不存在"),
    VERIFICATION_OUTPUTDEPOT_002("OUTPUTDEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CHECKED_001("CHECKED001", "盘点信息不存在", "盘点信息不存在"),
    VERIFICATION_CHECKED_002("CHECKED002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CLAIM_001("CLAIM001", "收款认领信息不存在", "收款认领信息不存在"),
    VERIFICATION_CLAIM_002("CLAIM002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_AUTOMOBILE_001("AUTOMOBILE001", "运输信息不存在", "运输信息不存在"),
    VERIFICATION_AUTOMOBILE_002("AUTOMOBILE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_SUPPLIER_001("SUPPLIER001", "供应商信息不存在", "供应商信息不存在"),
    VERIFICATION_SUPPLIER_002("SUPPLIER002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_ENTERPRISE_001("ENTERPRISE001", "终端企业信息不存在", "终端企业信息不存在"),
    VERIFICATION_ENTERPRISE_002("ENTERPRISE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_COMPANY_001("COMPANY001", "公司信息不存在", "公司信息不存在"),
    VERIFICATION_COMPANY_002("COMPANY002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CONTRACTSTART_001("CONTRACTSTART001", "合同启动信息不存在", "合同启动信息不存在"),
    VERIFICATION_CONTRACTSTART_002("CONTRACTSTART002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CONTRACTFORWARD_001("CONTRACTFORWARD001", "合同结转信息不存在", "合同结转信息不存在"),
    VERIFICATION_CONTRACTFORWARD_002("CONTRACTFORWARD002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_SETTLEMENT_001("SETTLEMENT001", "结算信息不存在", "结算信息不存在"),
    VERIFICATION_SETTLEMENT_002("SETTLEMENT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_INPUTINVOICE_001("INPUTINVOICE001", "进项发票信息不存在", "进项发票信息不存在"),
    VERIFICATION_INPUTINVOICE_002("INPUTINVOICE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_SEAL_001("SEAL001", "用印申请不存在", "用印申请不存在"),
    VERIFICATION_SEAL_002("SEAL002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_DECLARE_001("DECLARE001", "计划申报信息不存在", "计划申报信息不存在"),
    VERIFICATION_DECLARE_002("DECLARE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_FINANCING_001("FINANCING001", "融资信息不存在", "融资信息不存在"),
    VERIFICATION_FINANCING_002("FINANCING002", "当前状态下禁止修改", "当前状态下禁止修改"),
    VERIFICATION_DEPOT_001("DEPOT001", "仓库信息不存在", "融资信息不存在"),
    VERIFICATION_DEPOT_002("DEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_PROCUREMENTPLAN_001("PROCUREMENTPLAN001", "采购计划信息不存在", "采购计划信息不存在"),
    VERIFICATION_PROCUREMENTPLAN_002("PROCUREMENTPLAN002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_PROJECTBEFOREHAND_001("PROJECTBEFOREHAND001", "预立项信息不存在", "预立项信息不存在"),
    VERIFICATION_PROJECTBEFOREHAND_002("PROJECTBEFOREHAND002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_GOODS_001("GOODS001", "商品信息不存在", "商品信息不存在"),
    VERIFICATION_GOODS_002("GOODS002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OALEAVE_001("OALEAVE001", "请假申请不存在", "请假申请不存在"),
    VERIFICATION_OALEAVE_002("OALEAVE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAOVERTIME_001("OAOVERTIME001", "加班申请不存在", "加班申请不存在"),
    VERIFICATION_OAOVERTIME_002("OAOVERTIME002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAEVECTION_001("OAOVERTIME001", "出差申请不存在", "出差申请不存在"),
    VERIFICATION_OAEVECTION_002("OAOVERTIME002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAEGRESS_001("OAEGRESS001", "外出申请不存在", "外出申请不存在"),
    VERIFICATION_OAEGRESS_002("OAEGRESS002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAMAINTENANCEEXPANSION_001("OAMAINTENANCEEXPANSION001", "业务维护/拓展不存在", "业务维护/拓展不存在"),
    VERIFICATION_OAMAINTENANCEEXPANSION_002("OAMAINTENANCEEXPANSION002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAFEE_001("OAFEE001", "费用申请不存在", "费用申请不存在"),
    VERIFICATION_OAFEE_002("OAFEE002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAENTERTAIN_001("OAENTERTAIN001", "招待申请不存在", "招待申请不存在"),
    VERIFICATION_OAENTERTAIN_002("OAENTERTAIN002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OAOTHER_001("OAOTHER001", "其他申请不存在", "其他申请不存在"),
    VERIFICATION_OAOTHER_002("OAOTHER002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OASUPPLYCLOCK_001("OASUPPLYCLOCK001", "补卡申请不存在", "补卡申请不存在"),
    VERIFICATION_OASUPPLYCLOCK_002("OASUPPLYCLOCK002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_OACLAIM_001("OACLAIM001", "报销申请不存在", "报销申请不存在"),
    VERIFICATION_OACLAIM_002("OACLAIM002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_CARGOOWNER_001("CARGOOWNER001", "货主档案信息不存在", "货主档案信息不存在"),
    VERIFICATION_CARGOOWNER_002("CARGOOWNER002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_WASHINTODEPOT_001("WASHINTODEPOT001", "洗煤入库信息不存在", "洗煤入库信息不存在"),
    VERIFICATION_WASHINTODEPOT_002("WASHINTODEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_WASHIOUTPUTDEPOT_001("WASHOUTPUTDEPOT001", "洗煤出库信息不存在", "洗煤出库信息不存在"),
    VERIFICATION_WASHIOUTPUTDEPOT_002("WASHOUTPUTDEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_BLENDINTODEPOT_001("BLENDINTODEPOT001", "配煤入库信息不存在", "配煤入库信息不存在"),
    VERIFICATION_BLENDINTODEPOT_002("BLENDINTODEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_BLENDOUTPUTDEPOT_001("BLENDOUTPUTDEPOT001", "配煤出库信息不存在", "配煤出库信息不存在"),
    VERIFICATION_BLENDOUTPUTDEPOT_002("WASHOUTPUTDEPOT002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_SAMPLE_001("SAMPLE001", "取样信息不存在", "取样信息不存在"),
    VERIFICATION_SAMPLE_002("SAMPLE002", "当前状态下禁止修改", "当前状态下禁止修改"),
    VERIFICATION_ASSAY_001("ASSAY001", "化验信息不存在", "化验信息不存在"),
    VERIFICATION_ASSAY_002("ASSAY002", "当前状态下禁止修改", "当前状态下禁止修改"),

    VERIFICATION_ENTERREGISTER_001("ENTERREGISTER001", "企业注册申请不存在", "企业注册申请不存在"),
    VERIFICATION_ENTERREGISTER_002("ENTERREGISTER002", "当前状态下禁止修改", "当前状态下禁止修改"),

    ;

    private final String code;
    private final Object description;
    private Object message;

    private BusinessVerificationErrorCode(String code, Object message, Object description) {
        this.code = code;
        this.message=message;
        this.description = description;

    }

    public BusinessVerificationErrorCode andMessageIs(Object message) {
        this.message = message;
        return this;
    }

    public String getCode() {
        return this.code;
    }

    public Object getDescription() {
        return this.description;
    }

    public Object getMessage() {
        return this.message;
    }

    public Object getMessageOrElseDescription() {
        return this.message != null && !"".equals(this.message) ? this.message : this.description;
    }
}

