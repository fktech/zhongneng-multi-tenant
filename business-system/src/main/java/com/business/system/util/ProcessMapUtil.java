package com.business.system.util;

import com.alibaba.fastjson2.JSONObject;
import com.business.common.utils.GsonUtil;

import java.util.*;

import com.business.system.domain.*;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Map 转换器
 */
@Component
public class ProcessMapUtil {

    /**
     * 把source转为target
     * @param source source
     * @param target target
     * @param <T> 返回值类型
     * @return 返回值
     * @throws Exception newInstance可能会抛出的异常
     */
    public static <T> T mapToObj(Map source, Class<T> target) throws Exception {
        Field[] fields = target.getDeclaredFields();
        T o = target.newInstance();
        for(Field field:fields){
            Object val;
            if((val=source.get(field.getName()))!=null){
                field.setAccessible(true);
                field.set(o,val);
            }
        }
        return o;
    }

    /**
     * 利用反射将map集合封装成bean对象
     *
     * @param clazz
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, Class<?> clazz) throws Exception {
        Object obj = clazz.newInstance();
        if (map != null && !map.isEmpty() && map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String propertyName = entry.getKey(); 	// 属性名
                Object value = entry.getValue();		// 属性值
                String setMethodName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
                Field field = getClassField(clazz, propertyName);	//获取和map的key匹配的属性名称
                if (field == null){
                    continue;
                }
                Class<?> fieldTypeClass = field.getType();
                value = convertValType(value, fieldTypeClass);
                try {
                    clazz.getMethod(setMethodName, field.getType()).invoke(obj, value);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }
        return (T) obj;
    }

    /**
     * 根据给定对象类匹配对象中的特定字段
     * @param clazz
     * @param fieldName
     * @return
     */
    private static Field getClassField(Class<?> clazz, String fieldName) {
        if (Object.class.getName().equals(clazz.getName())) {
            return null;
        }
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        Class<?> superClass = clazz.getSuperclass();	//如果该类还有父类，将父类对象中的字段也取出
        if (superClass != null) {						//递归获取
            return getClassField(superClass, fieldName);
        }
        return null;
    }

    /**
     * 将map的value值转为实体类中字段类型匹配的方法
     * @param value
     * @param fieldTypeClass
     * @return
     */
    private static Object convertValType(Object value, Class<?> fieldTypeClass) {
        Object retVal = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (Long.class.getName().equals(fieldTypeClass.getName())
                || long.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Long.parseLong(value.toString());
        } else if (Integer.class.getName().equals(fieldTypeClass.getName())
                || int.class.getName().equals(fieldTypeClass.getName())) {
            if(value.toString().equals("0.0")){
                retVal = 0;
            } else {
                retVal = Integer.parseInt(value.toString());
            }
        } else if (Float.class.getName().equals(fieldTypeClass.getName())
                || float.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Float.parseFloat(value.toString());
        } else if (Double.class.getName().equals(fieldTypeClass.getName())
                || double.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Double.parseDouble(value.toString());
        } else if (Date.class.getName().equals(fieldTypeClass.getName())) {
            try {
                Date date = new Date(value.toString());
                String format = sdf.format(date);
                retVal = sdf.parse(format);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if(BigDecimal.class.getName().equals(fieldTypeClass.getName())){
            BigDecimal bigDecimal = new BigDecimal(value.toString());
            retVal = bigDecimal;
        } else if(value.toString().equals("0.0")){
            retVal = Integer.parseInt(value.toString());
        } else if(BaBillingInformation.class.getName().equals(fieldTypeClass.getName())){
            try {
                retVal = ProcessMapUtil.mapToBean((Map)value, BaBillingInformation.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(BaEnterpriseRelevance.class.getName().equals(fieldTypeClass.getName())){
            try {
                retVal = ProcessMapUtil.mapToBean((Map)value, BaEnterpriseRelevance.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(BaBank.class.getName().equals(fieldTypeClass.getName())){
            try {
                retVal = ProcessMapUtil.mapToBean((Map)value, BaBank.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(List.class.getName().equals(fieldTypeClass.getName())){
            try {
                retVal = (List)value;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            retVal = value;
        }
        return retVal;
    }

    /**
     * 构建评论树
     * @param list
     * @return
     */
    public static List<BaComment> processComments(List<BaComment> list) {
        Map<String, BaComment> map = new HashMap<>();   // (id, Comment)
        List<BaComment> result = new ArrayList<>();
        // 将所有根评论加入 map
        for(BaComment baComment : list) {
            if(baComment.getParentId() == null || baComment.getParentId().equals(""))
                result.add(baComment);
            map.put(baComment.getId(), baComment);
        }
        // 子评论加入到父评论的 child 中
        for(BaComment baComment : list) {
            String id = baComment.getParentId();
            if(id != null) {   // 当前评论为子评论
                BaComment p = map.get(id);
                // child 为空，则创建
                if(!ObjectUtils.isEmpty(p)) {
                    if (ObjectUtils.isEmpty(p.getChild()))
                        p.setChild(new ArrayList<>());
                    p.getChild().add(baComment);
                }
            }
        }
        return result;
    }


}
