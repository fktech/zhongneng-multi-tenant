package com.business.system.feign;

import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.system.domain.ProcessHiIdentitylink;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.FlowElementVO;
import com.business.system.domain.vo.HistoryTaskVO;
import com.business.system.domain.vo.RuntimeTaskVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: js
 * @Description: 调用工作流微服务OpenFeignClient
 * @Date: 2022/12/9 10:46
 */
@Component
@FeignClient(name = "business-workflow",path = "")
public interface WorkFlowFeignClient {

    @ApiOperation(value="/process/instance/start", notes = "启动流程接口")
    @PostMapping("/process/instance/start")
    public AjaxResult start(@RequestBody StartProcessInstanceDTO startProcessInstanceDTO);

    @ApiOperation(value="/process/task/listRuntimeTask", notes = "分页查询我的待办")
    @PostMapping("/process/task/listRuntimeTask")
    public TableDataInfo listRuntimeTask(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/listRuntimeTaskAll", notes = "查看我的待办所有记录")
    @PostMapping("/process/task/listRuntimeTaskAll")
    public List<RuntimeTaskVO> listRuntimeTaskAll(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation("撤回流程实例")
    @PostMapping("/process/instance/cancelProcessInstance")
    public AjaxResult cancelProcessInstance(@RequestBody CancelProcessInstanceDTO cancelProcessInstanceDTO);

    @ApiOperation("办理待办任务")
    @PostMapping("/process/task/completeTask")
    public AjaxResult completeTask(@RequestBody ProcessTaskDTO processTaskDTO);

    @ApiOperation("判断流程实例是否结束")
    @GetMapping("/process/instance/processInstanceIsEnd")
    public AjaxResult processInstanceIsEnd(@RequestParam("processInstanceId") String processInstanceId);

    @ApiOperation(value="/process/task/listHistoryProcessTask", notes = "分页查询我的已办")
    @PostMapping("/process/task/listHistoryProcessTask")
    public TableDataInfo listHistoryProcessTask(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/listHistoryProcessTaskAll", notes = "查询我的已办")
    @PostMapping("/process/task/listHistoryProcessTaskAll")
    public List<HistoryTaskVO> listHistoryProcessTaskAll(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/instance/listInstanceApproveLink", notes = "查询流程实例审批链路信息")
    @PostMapping("/process/instance/listInstanceApproveLink")
    public AjaxResult listInstanceApproveLink(@RequestBody ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO);

    @ApiOperation(value = "/process/task/countRuntimeTask", notes = "统计某个业务我的待办总数")
    @PostMapping("/process/task/countRuntimeTask")
    public AjaxResult countRuntimeTask(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value = "/process/task/countHistoryProcessTask", notes = "统计某个业务我的已办总数")
    @PostMapping("/process/task/countHistoryProcessTask")
    public AjaxResult countHistoryProcessTask(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/myApply", notes = "查询我的申请")
    @PostMapping("/process/task/myApply")
    public TableDataInfo myApply(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/listRuntimeTaskSend", notes = "分页查询抄送给我的")
    @PostMapping("/process/task/listRuntimeTaskSend")
    public TableDataInfo listRuntimeTaskSend(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation("查询抄送数据")
    @PostMapping("/process/task/listSendUser")
    public List<ProcessHiIdentitylink> listSendUser(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/instance/listDefinitionApproveLink", notes = "查询流程定义审批链路信息")
    @PostMapping("/process/instance/listDefinitionApproveLink")
    public AjaxResult listDefinitionApproveLink(@RequestBody BaProcessDefinitionDTO baProcessDefinitionDTO);

    @ApiOperation(value="/process/instance/getCurrentNodeApproveLink", notes = "查询流程当前节点审批链路信息")
    @PostMapping("/process/instance/getCurrentNodeApproveLink")
    public AjaxResult getCurrentNodeApproveLink(@RequestBody ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO);

    @ApiOperation(value="/process/task/refuse", notes = "驳回")
    @PostMapping("/process/task/refuse")
    public AjaxResult refuse(@RequestBody ProcessTaskDTO processTaskDTO);

    @ApiOperation(value="/process/instance/listDefinitionNode", notes = "查询流程定义节点信息")
    @PostMapping("/process/instance/listDefinitionNode")
    public List<FlowElementVO> listDefinitionNode(@RequestBody BaProcessDefinitionDTO baProcessDefinitionDTO);

    @ApiOperation(value="/process/task/listRuntimeTaskApp", notes = "分页查询我的待办")
    @PostMapping("/process/task/listRuntimeTaskApp")
    public TableDataInfo listRuntimeTaskApp(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/listHistoryProcessTaskApp", notes = "分页查询我的已办")
    @PostMapping("/process/task/listHistoryProcessTaskApp")
    public TableDataInfo listHistoryProcessTaskApp(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation(value="/process/task/listRuntimeTaskSendApp", notes = "APP分页查询抄送给我的")
    @PostMapping("/process/task/listRuntimeTaskSendApp")
    public TableDataInfo listRuntimeTaskSendApp(@RequestBody ProcessTaskQueryDTO processTaskQueryDTO);

    @ApiOperation("初始化流程定义")
    @PostMapping("/process/templates/copyTemplates")
    public AjaxResult copyTemplates(@RequestBody CopyProcessTemplatesDTO copyProcessTemplatesDTO);

}
