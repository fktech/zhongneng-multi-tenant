package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaContractStartDTO对象", description = "合同发起对象")
public class BaContractStartDTO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private String type;

    /** 合同启动名称 */
    @ApiModelProperty(value = "合同启动名称")
    private String name;

    /** 部门类型 **/
    @ApiModelProperty(value = "部门类型")
    private String deptType;

    /** 项目类型 **/
    @ApiModelProperty(value = "项目类型")
    private String projectType;

    /** 业务类型 **/
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 正常合同，问题合同 **/
    @ApiModelProperty(value = "正常合同 1，问题合同 2")
    private String problemType;

    /** 正常合同，问题合同 **/
    @ApiModelProperty(value = "正常合同 1，问题合同 2")
    private String problemMark;

    /** 立项ID **/
    @ApiModelProperty(value = "立项ID")
    private String projectId;


}
