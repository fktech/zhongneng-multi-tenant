package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 事件记录对象 ba_event_recording
 *
 * @author single
 * @date 2023-07-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "事件记录对象",description = "")
@TableName("ba_event_recording")
public class BaEventRecording extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 事件分类 */
    @Excel(name = "事件分类")
    @ApiModelProperty(value = "事件分类")
    private String ability;

    /** 事件id */
    @Excel(name = "事件id")
    @ApiModelProperty(value = "事件id")
    private String eventLogId;

    /** 事件类型 */
    @Excel(name = "事件类型")
    @ApiModelProperty(value = "事件类型")
    private String eventType;

    /** 事件类型名称 */
    @Excel(name = "事件类型名称")
    @ApiModelProperty(value = "事件类型名称")
    private String eventTypeName;

    /** 扩展字段1 */
    @Excel(name = "扩展字段1")
    @ApiModelProperty(value = "扩展字段1")
    private String extend1;

    /** 扩展字段2 */
    @Excel(name = "扩展字段2")
    @ApiModelProperty(value = "扩展字段2")
    private String extend2;

    /** 所属位置 */
    @Excel(name = "所属位置")
    @ApiModelProperty(value = "所属位置")
    private String locationIndexCode;

    /** 所属位置名称 */
    @Excel(name = "所属位置名称")
    @ApiModelProperty(value = "所属位置名称")
    private String locationName;

    /** 所属区域编号 */
    @Excel(name = "所属区域编号")
    @ApiModelProperty(value = "所属区域编号")
    private String regionIndexCode;

    /** 所属区域名称 */
    @Excel(name = "所属区域名称")
    @ApiModelProperty(value = "所属区域名称")
    private String regionName;

    /** 事件源名称 */
    @Excel(name = "事件源名称")
    @ApiModelProperty(value = "事件源名称")
    private String resindexCode;

    /** 事件源编号 */
    @Excel(name = "事件源编号")
    @ApiModelProperty(value = "事件源编号")
    private String resName;

    /** 事件源类型 */
    @Excel(name = "事件源类型")
    @ApiModelProperty(value = "事件源类型")
    private String resType;

    /** 事件开始事件 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "事件开始事件", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "事件开始事件")
    private Date startTime;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;



}
