package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 无车承运开票详情对象 ba_automobile_settlement_invoice_detail
 *
 * @author single
 * @date 2023-04-13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "无车承运开票详情对象",description = "")
@TableName("ba_automobile_settlement_invoice_detail")
public class BaAutomobileSettlementInvoiceDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 开票编号 */
    @Excel(name = "开票编号")
    @ApiModelProperty(value = "开票编号")
    private String invoiceNo;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String plateNumber;

    /** 装货地 */
    @Excel(name = "装货地")
    @ApiModelProperty(value = "装货地")
    private String deliverAddressName;

    /** 卸货地 */
    @Excel(name = "卸货地")
    @ApiModelProperty(value = "卸货地")
    private String receiveaddressname;

    /** 开票单据名称 */
    @Excel(name = "开票单据名称")
    @ApiModelProperty(value = "开票单据名称")
    private String goodsName;

    /** 归属货主 */
    @Excel(name = "归属货主")
    @ApiModelProperty(value = "归属货主")
    private String orderSn;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货时间")
    private Date actDeliverTime;

    /** 签收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签收时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "签收时间")
    private Date actReceiveTime;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 发货数量 */
    @Excel(name = "发货数量")
    @ApiModelProperty(value = "发货数量")
    private BigDecimal actDeliverQuantity;

    /** 签收数量 */
    @Excel(name = "签收数量")
    @ApiModelProperty(value = "签收数量")
    private BigDecimal actReceiveQuantity;

    /** 途损数量 */
    @Excel(name = "途损数量")
    @ApiModelProperty(value = "途损数量")
    private BigDecimal amountLoss;

    /** 结算数量 */
    @Excel(name = "结算数量")
    @ApiModelProperty(value = "结算数量")
    private BigDecimal transAmount;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal moneyTransPrice;

    /** 运费金额 */
    @Excel(name = "运费金额")
    @ApiModelProperty(value = "运费金额")
    private BigDecimal moneyTrans;

    /** 现金结算金额 */
    @Excel(name = "现金结算金额")
    @ApiModelProperty(value = "现金结算金额")
    private BigDecimal moneyFinal;

    /** 司机名称 */
    @Excel(name = "司机名称")
    @ApiModelProperty(value = "司机名称")
    private String driverName;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String externalOrder;

    /** 货源单号 */
    @ApiModelProperty(value = "货源单号")
    @TableField(exist = false)
    private String supplierNo;

    /** 合同启动 */
    @ApiModelProperty(value = "合同启动")
    @TableField(exist = false)
    private String starName;



}
