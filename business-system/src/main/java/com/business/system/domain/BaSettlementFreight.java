package com.business.system.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 汽运结算对象 ba_settlement_freight
 *
 * @author ljb
 * @date 2023-02-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "汽运结算对象",description = "")
@TableName("ba_settlement_freight")
public class BaSettlementFreight extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 货源id */
    @Excel(name = "货源id")
    @ApiModelProperty(value = "货源id")
    private String automobileId;

    /** 总车数 */
    @Excel(name = "总车数")
    @ApiModelProperty(value = "总车数")
    private Long carNum;

    /** 已付车数 */
    @Excel(name = "已付车数")
    @ApiModelProperty(value = "已付车数")
    private Long carsPaid;

    /** 应结算金额 */
    @Excel(name = "应结算金额")
    @ApiModelProperty(value = "应结算金额")
    private BigDecimal yfratesmoney;

    /** 已付款金额 */
    @Excel(name = "已付款金额")
    @ApiModelProperty(value = "已付款金额")
    private BigDecimal amountPaid;

    /** 标识 */
    @Excel(name = "标识")
    @ApiModelProperty(value = "标识")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;



}
