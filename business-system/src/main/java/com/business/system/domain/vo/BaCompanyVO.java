package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value="BaCompanyVO对象",description="承运单位发运量")
public class BaCompanyVO {
    /** 承运单位名称 */
    @ApiModelProperty(value = "承运单位名称")
    private String name;

    /** 实际发运量 */
    @ApiModelProperty(value = "实际发运量")
    private BigDecimal num;
}
