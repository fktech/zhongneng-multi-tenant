package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.List;

/**
 * 汽车运输/货源对象 ba_transport_automobile
 *
 * @author ljb
 * @date 2023-02-17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "汽车运输/货源对象",description = "")
@TableName("ba_transport_automobile")
public class BaTransportAutomobile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 货源编号 */
    @Excel(name = "货源编号")
    @ApiModelProperty(value = "货源编号")
    private String supplierNo;

    /** 货源名称 */
    @Excel(name = "货源名称")
    @ApiModelProperty(value = "货源名称")
    private String sourceName;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发布时间")
    private Date deliveryTime;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "有效期")
    private Date validityTime;

    /** 装卸时间差 */
    @Excel(name = "装卸时间差")
    @ApiModelProperty(value = "装卸时间差")
    private String difference;

    /** 货物名称 */
    @Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String goodsName;

    /** 发货类型 */
    @Excel(name = "发货类型")
    @ApiModelProperty(value = "发货类型")
    private String fhType;

    /** 运输吨数 */
    @Excel(name = "运输吨数")
    @ApiModelProperty(value = "运输吨数")
    private Long transWeight;

    /** 运输车数 */
    @Excel(name = "运输车数")
    @ApiModelProperty(value = "运输车数")
    private Long transNum;

    /** 发货模式 */
    @Excel(name = "发货模式")
    @ApiModelProperty(value = "发货模式")
    private String deliveryMode;

    /** 经济人ID */
    @Excel(name = "经济人ID")
    @ApiModelProperty(value = "经济人ID")
    private Long carteamId;

    /** 经济人名称 */
    @Excel(name = "经济人名称")
    @ApiModelProperty(value = "经济人名称")
    private String carteamName;

    /** 车队长名称 */
    @Excel(name = "车队长名称")
    @ApiModelProperty(value = "车队长名称")
    private String captainName;

    /** 车队长ID */
    @Excel(name = "车队长ID")
    @ApiModelProperty(value = "车队长ID")
    private Long captainId;

    /** 发货人 */
    @Excel(name = "发货人")
    @ApiModelProperty(value = "发货人")
    private String depName;

    /** 发货人联系方式 */
    @Excel(name = "发货人联系方式")
    @ApiModelProperty(value = "发货人联系方式")
    private String fhTel;

    /** 发货方地址 */
    @Excel(name = "发货方地址")
    @ApiModelProperty(value = "发货方地址")
    private String placeDep;

    /** 发货方详细地址 */
    @Excel(name = "发货方详细地址")
    @ApiModelProperty(value = "发货方详细地址")
    private String depDetailed;

    /** 收货人 */
    @Excel(name = "收货人")
    @ApiModelProperty(value = "收货人")
    private String desName;

    /** 收货人联系方式 */
    @Excel(name = "收货人联系方式")
    @ApiModelProperty(value = "收货人联系方式")
    private String shTel;

    /** 收货	方地址 */
    @Excel(name = "收货	方地址")
    @ApiModelProperty(value = "收货	方地址")
    private String placeDes;

    /** 收货方详细地址 */
    @Excel(name = "收货方详细地址")
    @ApiModelProperty(value = "收货方详细地址")
    private String desDetailed;

    /** 油卡金额 */
    @Excel(name = "油卡金额")
    @ApiModelProperty(value = "油卡金额")
    private BigDecimal oilPrice;

    /** 发货地经度 */
    @Excel(name = "发货地经度")
    @ApiModelProperty(value = "发货地经度")
    private String depJd;

    /** 发货地纬度	 */
    @Excel(name = "发货地纬度	")
    @ApiModelProperty(value = "发货地纬度	")
    private String depWd;

    /** 收货地经度 */
    @Excel(name = "收货地经度")
    @ApiModelProperty(value = "收货地经度")
    private String desJd;

    /** 收货地纬度 */
    @Excel(name = "收货地纬度")
    @ApiModelProperty(value = "收货地纬度")
    private String desWd;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;
    @TableField(exist = false)
    @ApiModelProperty(value = "货主名称")
    private String consignorUserName;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 运输状态（1:运输中 2:已完成） */
    @Excel(name = "运输状态", readConverterExp = "1=:运输中,2=:已完成")
    @ApiModelProperty(value = "运输状态")
    private String transportStatus;

    /** 待发状态（1:待发 2.已发) */
    @Excel(name = "待发状态", readConverterExp = "待发状态（1:待发 2.已发)")
    @ApiModelProperty(value = "待发状态")
    private String confirmStatus;

    /** 审批流程ID */
    @Excel(name = "审批流程ID")
    @ApiModelProperty(value = "审批流程ID")
    private String flowId;

    /** 运输类型 */
    @Excel(name = "运输类型")
    @ApiModelProperty(value = "运输类型")
    private String transportType;

    /** 所属组织机构代码 */
    @Excel(name = "所属组织机构代码")
    @ApiModelProperty(value = "所属组织机构代码")
    private String sysCompanyCode;

    /** 货源发布人id */
    @Excel(name = "货源发布人id")
    @ApiModelProperty(value = "货源发布人id")
    private Long supplierId;

    /** 货源发布人名称 */
    @Excel(name = "货源发布人名称")
    @ApiModelProperty(value = "货源发布人名称")
    private String supplierName;

    /** 接货类型：0-指派车辆，1-指派经济人，2-抢单 */
    @Excel(name = "接货类型：0-指派车辆，1-指派经济人，2-抢单")
    @ApiModelProperty(value = "接货类型：0-指派车辆，1-指派经济人，2-抢单")
    private Long jhType;

    /** 车辆id */
    @Excel(name = "车辆id")
    @ApiModelProperty(value = "车辆id")
    private String carId;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String carName;

    /** 货主运价 */
    @Excel(name = "货主运价")
    @ApiModelProperty(value = "货主运价")
    private BigDecimal ratesPrice;

    /** 经济人运价 */
    @Excel(name = "经济人运价")
    @ApiModelProperty(value = "经济人运价")
    private BigDecimal agentPrice;

    /** 气卡金额 */
    @Excel(name = "气卡金额")
    @ApiModelProperty(value = "气卡金额")
    private BigDecimal qkPrice;

    /** 上级货源id */
    @Excel(name = "上级货源id")
    @ApiModelProperty(value = "上级货源id")
    private Long pId;

    /** 抢单类型：0-定价抢单，1-竞价抢单 */
    @Excel(name = "抢单类型：0-定价抢单，1-竞价抢单")
    @ApiModelProperty(value = "抢单类型：0-定价抢单，1-竞价抢单")
    private Long qdType;

    /** 竞拍最高价格 */
    @Excel(name = "竞拍最高价格")
    @ApiModelProperty(value = "竞拍最高价格")
    private BigDecimal maxPrice;

    /** 发货地半径 */
    @Excel(name = "发货地半径")
    @ApiModelProperty(value = "发货地半径")
    private String depRadius;

    /** 收货地半径 */
    @Excel(name = "收货地半径")
    @ApiModelProperty(value = "收货地半径")
    private String desRadius;

    /** 结算类型：0-按吨结算，1-按车结算，2-按方结算 */
    @Excel(name = "结算类型：0-按吨结算，1-按车结算，2-按方结算")
    @ApiModelProperty(value = "结算类型：0-按吨结算，1-按车结算，2-按方结算")
    private Long jsType;

    /** 抢单车数统计 */
    @Excel(name = "抢单车数统计")
    @ApiModelProperty(value = "抢单车数统计")
    private Long qdCount;

    /** 接单吨数统计 */
    @Excel(name = "接单吨数统计")
    @ApiModelProperty(value = "接单吨数统计")
    private BigDecimal weightCount;

    /** 是否抢单完毕：0-未抢完，1-已抢完 */
    @Excel(name = "是否抢单完毕：0-未抢完，1-已抢完")
    @ApiModelProperty(value = "是否抢单完毕：0-未抢完，1-已抢完")
    private Long qdCountType;

    /** 装车开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "装车开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "装车开始时间")
    private Date yfStartDate;

    /** 装车结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "装车结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "装车结束时间")
    private Date yfEndDate;

    /** 到货截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "到货截止时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到货截止时间")
    private Date dhEndDate;

    /** 发货省 */
    @Excel(name = "发货省")
    @ApiModelProperty(value = "发货省")
    private String fhs;

    /** 发货城市 */
    @Excel(name = "发货城市")
    @ApiModelProperty(value = "发货城市")
    private String fhcity;

    /** 发货区县 */
    @Excel(name = "发货区县")
    @ApiModelProperty(value = "发货区县")
    private String fhqx;

    /** 收货省 */
    @Excel(name = "收货省")
    @ApiModelProperty(value = "收货省")
    private String shs;

    /** 收货市 */
    @Excel(name = "收货市")
    @ApiModelProperty(value = "收货市")
    private String shcity;

    /** 收货区县 */
    @Excel(name = "收货区县")
    @ApiModelProperty(value = "收货区县")
    private String shqx;

    /** 运输距离 */
    @Excel(name = "运输距离")
    @ApiModelProperty(value = "运输距离")
    private BigDecimal moveDistance;

    /** 审核标记：0-未审核，1-审核通过，2-审核拒绝 */
    @Excel(name = "审核标记：0-未审核，1-审核通过，2-审核拒绝")
    @ApiModelProperty(value = "审核标记：0-未审核，1-审核通过，2-审核拒绝")
    private Long checkFlag;

    /** 执行状态标记：0-执行中，1-执行结束 */
    @Excel(name = "执行状态标记：0-执行中，1-执行结束")
    @ApiModelProperty(value = "执行状态标记：0-执行中，1-执行结束")
    private Long workFlag;

    /** 删除标记：0-未删除，1-已删除 */
    @Excel(name = "删除标记：0-未删除，1-已删除")
    @ApiModelProperty(value = "删除标记：0-未删除，1-已删除")
    private Long isDelete;

    /** 竞拍终止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "竞拍终止时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "竞拍终止时间")
    private Date qdOverTime;

    /** 二维码图片地址 */
    @Excel(name = "二维码图片地址")
    @ApiModelProperty(value = "二维码图片地址")
    private String qrcode;

    /** 申请结算状态 */
    @Excel(name = "申请结算状态")
    @ApiModelProperty(value = "申请结算状态")
    private Long declareFlag;

    /** 接收发布时间参数 */
    @Excel(name = "接收发布时间参数")
    @ApiModelProperty(value = "接收发布时间参数")
    private String date1;

    /** 订单接单状态0-未接单1-已接单 */
    @Excel(name = "订单接单状态0-未接单1-已接单")
    @ApiModelProperty(value = "订单接单状态0-未接单1-已接单")
    private Long orderReceivingFlag;

    /** 是否收藏货源0-未收藏1-已收藏 */
    @Excel(name = "是否收藏货源0-未收藏1-已收藏")
    @ApiModelProperty(value = "是否收藏货源0-未收藏1-已收藏")
    private Long isCollect;


    /** 文件夹id */
    @Excel(name = "文件夹id")
    @ApiModelProperty(value = "文件夹id")
    private Long folderId;

    /** 经济人改价比例 */
    @Excel(name = "经济人改价比例")
    @ApiModelProperty(value = "经济人改价比例")
    private Long priceRatio;

    /** 是否启用经济人改价比例设置（默认0-未开启 1-已开启） */
    @Excel(name = "是否启用经济人改价比例设置", readConverterExp = "默=认0-未开启,1=-已开启")
    @ApiModelProperty(value = "是否启用经济人改价比例设置")
    private Long isUsePriceRatio;

    /** 经济人电话 */
    @Excel(name = "经济人电话")
    @ApiModelProperty(value = "经济人电话")
    private String agentPhone;

    /** 车队长电话 */
    @Excel(name = "车队长电话")
    @ApiModelProperty(value = "车队长电话")
    private String captainPhone;

    /** 审批状态 */
    @Excel(name = "审批状态")
    @ApiModelProperty(value = "审批状态")
    private String state;

    /** 订单编号 */
    @Excel(name = "订单编号")
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    /** 订单编号 **/
    @TableField(exist = false)
    private String orderName;

    @TableField(exist = false)
    private String projectName;

    /** 计划发货量 **/
    @TableField(exist = false)
    private BigDecimal saleNum;

    /** 车数 **/
    @TableField(exist = false)
    private Integer carNum;


    /** 公路运输平台 */
    @Excel(name = "公路运输平台")
    @ApiModelProperty(value = "公路运输平台")
    private String companyId;

    @TableField(exist = false)
    @ApiModelProperty(value = "公路运输平台名称")
    private String companyName;

    /** 运输实控人 */
    @Excel(name = "运输实控人")
    @ApiModelProperty(value = "运输实控人")
    private String counterpartId;

    /** 验收总量 */
    @Excel(name = "验收总量")
    @ApiModelProperty(value = "验收总量")
    private BigDecimal totalAcceptance;

    /**
     * 货主userId
     */
    @ApiModelProperty(value = "货主userId")
    private Long uId;

    /**
     * 货主公司名称
     */
    @ApiModelProperty(value = "货主公司名称")
    private String company;

    @ApiModelProperty(value = "发货信息")
    @TableField(exist = false)
    private BaCheck delivery;

    /**
     * 收货信息
     */
    @ApiModelProperty(value = "收货信息")
    @TableField(exist = false)
    private BaCheck receipt;

    /**
     * 是否全部签收
     */
    @ApiModelProperty(value = "是否全部签收")
    @TableField(exist = false)
    private Boolean sign;




    /**
     * 货源中运价对象集合
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "货源中运价对象集合")
    private List<BaAutomobuleStandard> ratesItemList;

    /**
     * 货源中运价对象集合
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "货源中运价对象集合")
    private List<BaShippingOrder> shippingOrdeList;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 时间
     */
    @TableField(exist = false)
    private List<String> time;

    /** 隐藏运价 */
    @ApiModelProperty(value = "隐藏运价")
    @TableField(exist = false)
    private Integer isShowPrice;

    /** 结算依据 */
    @ApiModelProperty(value = "结算依据")
    @TableField(exist = false)
    private String jsyjtype;

    /** 货转凭证 **/
    @ApiModelProperty(value = "货转凭证")
    private String transferVoucher;

    /** 路耗扣款标准 */
    @TableField(exist = false)
    @ApiModelProperty(value = "路耗扣款标准")
    private String kkbz;

    /** 合理路耗标准 */
    @TableField(exist = false)
    @ApiModelProperty(value = "合理路耗标准")
    private String luhaobz;

    /** 路耗类型 */
    @TableField(exist = false)
    @ApiModelProperty(value = "路耗类型")
    private String luhaotype;

    /** 运费单价 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运费单价")
    private BigDecimal price;

    /** 关联主键 */
    @TableField(exist = false)
    @ApiModelProperty(value = "关联主键")
    private String relationId;

    /** 精度 */
    @TableField(exist = false)
    @ApiModelProperty(value = "精度")
    private String yfprecision;

    /** 合理涨吨标准 */
    @TableField(exist = false)
    @ApiModelProperty(value = "合理涨吨标准")
    private String zdbz;

    /** 涨吨扣款标准 */
    @TableField(exist = false)
    @ApiModelProperty(value = "涨吨扣款标准")
    private String zdkkbz;

    /** 涨吨类型 */
    @TableField(exist = false)
    @ApiModelProperty(value = "涨吨类型")
    private String zdtype;

    /** 启动名称 **/
    @ApiModelProperty(value = "启动名称")
    @TableField(exist = false)
    private String startName;

    /** 货源类型 **/
    @ApiModelProperty(value = "货源类型")
    private String type;

    /** 出库 **/
    @ApiModelProperty(value = "出库")
    private String outbound;

    /** 入库 **/
    @ApiModelProperty(value = "入库")
    private String warehousing;

    /** 下拉 **/
    @ApiModelProperty(value = "下拉")
    @TableField(exist = false)
    private String dropDown;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /**
     * 仓储立项
     */
    @ApiModelProperty(value = "仓储立项")
    private String projectId;

    @ApiModelProperty(value = "仓储立项名称")
    @TableField(exist = false)
    private String storageName;
    /**
     * 采购申请
     */
    @ApiModelProperty(value = "采购申请")
    private String procureId;

    @ApiModelProperty(value = "采购申请名称")
    @TableField(exist = false)
    private String procureName;

    /**
     * 销售申请
     */
    @ApiModelProperty(value = "销售申请")
    private String saleId;

    @ApiModelProperty(value = "销售申请名称")
    @TableField(exist = false)
    private String saleName;

    /**
     * 取样出入库
     */
    @ApiModelProperty(value = "取样出入库")
    private String sampleType;

    /**
     * 当前日期
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "当前日期")
    private String currentFlag;

    /**
     * 租户ID
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
