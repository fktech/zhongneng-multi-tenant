package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TerminalDTO对象", description = "终端统计")
public class TerminalDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /**
     * 合同启动ID
     */
    private String startId;

    /**
     * 立项ID
     */
    private String projectId;

    /**
     * 合同启动名称
     */
    private String startName;

    /**
     * 终端ID
     */
    private String enterpriseId;

    /**
     * 集团名称
     */
    private String companyName;

    /**
     * 集团名称
     */
    private String deptId;

    /**
     * 租户ID
     */
    private String tenantId;
}
