package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 部门
 * @Date: 2022/11/29 10:02
 */
@Data
@ApiModel(value = "SysDeptDTO对象", description = "部门对象")
public class SysDeptDTO implements Serializable {

    private static final long serialVersionUID = -2690560932813895834L;

    @ApiModelProperty("父部门ID")
    private Long parentId;

    @ApiModelProperty("类型")
    private String type;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
