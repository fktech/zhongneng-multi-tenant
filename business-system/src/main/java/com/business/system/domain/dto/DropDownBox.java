package com.business.system.domain.dto;

import com.business.system.domain.BaDepot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class DropDownBox implements Serializable {

    private static final long serialVersionUID = 9127532221382240134L;


    @ApiModelProperty(value = "Id")
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty
    private String type;

    @ApiModelProperty
    private String abbreviation;

}
