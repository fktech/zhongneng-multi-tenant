package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 付款明细对象 ba_automobile_settlement_detail
 *
 * @author ljb
 * @date 2023-04-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "付款明细对象",description = "")
@TableName("ba_automobile_settlement_detail")
public class BaAutomobileSettlementDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String plateNumber;

    /** 装货地 */
    @Excel(name = "装货地")
    @ApiModelProperty(value = "装货地")
    private String deliverAddressName;

    /** 卸货地 */
    @Excel(name = "卸货地")
    @ApiModelProperty(value = "卸货地")
    private String receiveAddressName;

    /** 货品 */
    @Excel(name = "货品")
    @ApiModelProperty(value = "货品")
    private String goodsName;

    /** 运单号 */
    @Excel(name = "运单号")
    @ApiModelProperty(value = "运单号")
    private String orderSn;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货时间")
    private Date actDeliverTime;

    /** 签收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签收时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "签收时间")
    private Date actReceiveTime;

    /** 发货数量 */
    @Excel(name = "发货数量")
    @ApiModelProperty(value = "发货数量")
    private BigDecimal actDeliverQuantity;

    /** 签收量 */
    @Excel(name = "签收量")
    @ApiModelProperty(value = "签收量")
    private BigDecimal actReceiveQuantity;

    /** 途损数量 */
    @Excel(name = "途损数量")
    @ApiModelProperty(value = "途损数量")
    private BigDecimal amountLoss;

    /** 货品单价 */
    @Excel(name = "货品单价")
    @ApiModelProperty(value = "货品单价")
    private BigDecimal goodsPrice;

    /** 结算数量 */
    @Excel(name = "结算数量")
    @ApiModelProperty(value = "结算数量")
    private BigDecimal transAmount;

    /** 途损扣款 */
    @Excel(name = "途损扣款")
    @ApiModelProperty(value = "途损扣款")
    private BigDecimal moneyLoss;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal moneyTransPrice;

    /** 运费金额 */
    @Excel(name = "运费金额")
    @ApiModelProperty(value = "运费金额")
    private BigDecimal moneyTrans;

    /** 现金结算金额 */
    @Excel(name = "现金结算金额")
    @ApiModelProperty(value = "现金结算金额")
    private BigDecimal moneyFinal;

    /** 应付金额 */
    @Excel(name = "应付金额")
    @ApiModelProperty(value = "应付金额")
    private BigDecimal moneyPay;

    /** 已付金额 */
    @Excel(name = "已付金额")
    @ApiModelProperty(value = "已付金额")
    private BigDecimal moneyPaid;

    /** 未付金额 */
    @Excel(name = "未付金额")
    @ApiModelProperty(value = "未付金额")
    private BigDecimal moneyNoPay;

    /** 司机姓名 */
    @Excel(name = "司机姓名")
    @ApiModelProperty(value = "司机姓名")
    private String driverName;

    /** 外单编号 */
    @Excel(name = "外单编号")
    @ApiModelProperty(value = "外单编号")
    private String externalOrder;

    /** 付款单号 */
    @Excel(name = "付款单号")
    @ApiModelProperty(value = "付款单号")
    private String paymentOrderNum;

    /** 付款状态 */
    @Excel(name = "付款状态")
    @ApiModelProperty(value = "付款状态")
    private String state;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 收款人 */
    @Excel(name = "收款人")
    @ApiModelProperty(value = "收款人")
    private String belonger;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    @ApiModelProperty(value = "银行卡号")
    private String bankCode;

    /** 银行卡预留手机号 */
    @Excel(name = "银行卡预留手机号")
    @ApiModelProperty(value = "银行卡预留手机号")
    private String bankPhoneNumber;

    /** 银行卡所属人身份证号 */
    @Excel(name = "银行卡所属人身份证号")
    @ApiModelProperty(value = "银行卡所属人身份证号")
    private String bankIdNumber;

    /** 开户行名称 */
    @Excel(name = "开户行名称")
    @ApiModelProperty(value = "开户行名称")
    private String bankName;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请时间")
    private Date applyTime;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "付款时间")
    private Date jsTime;

    /** 运单编号 */
    @ApiModelProperty(value = "运单编号")
    @TableField(exist = false)
    private String shippingId;

    /** 货源单号 */
    @ApiModelProperty(value = "货源单号")
    @TableField(exist = false)
    private String supplierNo;

    /** 合同启动 */
    @ApiModelProperty(value = "合同启动")
    @TableField(exist = false)
    private String starName;


}
