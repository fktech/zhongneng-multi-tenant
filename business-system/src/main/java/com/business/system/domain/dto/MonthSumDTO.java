package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0
 * @Author: js
 * @Description: 用户
 * @Date: 2022/11/29 10:02
 */
@Data
@ApiModel(value = "MonthSumDTO", description = "年度营业对象")
public class MonthSumDTO {
    @ApiModelProperty("月份")
    private String month;

    @ApiModelProperty("数量")
    private BigDecimal count;
}
