package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 监控对象 ba_monitor
 *
 * @author single
 * @date 2024-06-04
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "监控对象",description = "")
@TableName("ba_monitor")
public class BaMonitor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 区域唯一标识码 */
    @Excel(name = "区域唯一标识码")
    @ApiModelProperty(value = "区域唯一标识码")
    private String indexCode;

    /** 区域名称 */
    @Excel(name = "区域名称")
    @ApiModelProperty(value = "区域名称")
    private String name;

    /** 父区域唯一标识码 */
    @Excel(name = "父区域唯一标识码")
    @ApiModelProperty(value = "父区域唯一标识码")
    private String parentIndexCode;

    /** 用于标识区域节点是否有权限操作，true：有权限 false：无权限 */
    @Excel(name = "用于标识区域节点是否有权限操作，true：有权限 false：无权限")
    @ApiModelProperty(value = "用于标识区域节点是否有权限操作，true：有权限 false：无权限")
    private String available;

    /** true:是叶子节点，表示该区域下面未挂区域 false:不是叶子节点，表示该区域下面挂有区域 */
    @Excel(name = "true:是叶子节点，表示该区域下面未挂区域 false:不是叶子节点，表示该区域下面挂有区域")
    @ApiModelProperty(value = "true:是叶子节点，表示该区域下面未挂区域 false:不是叶子节点，表示该区域下面挂有区域")
    private String leaf;

    /** 级联平台标识，多个级联编号以@分隔，本级区域默认值“0” */
    @Excel(name = "级联平台标识，多个级联编号以@分隔，本级区域默认值“0”")
    @ApiModelProperty(value = "级联平台标识，多个级联编号以@分隔，本级区域默认值“0”")
    private String cascadeCode;

    /** 区域标识 0：本级 1：级联 2：混合，下级推送给上级的本级点（杭州本级有滨江，然后下级滨江又把自己推送上来了，滨江是混合区域节点） 入参cascadeFlag与返回值对应： cascadeFlag=0：返回0、1、2 cascadeFlag=1：返回0、2 cascadeFlag=2：返回1、2 */
    @Excel(name = "区域标识 0：本级 1：级联 2：混合，下级推送给上级的本级点", readConverterExp = "杭=州本级有滨江，然后下级滨江又把自己推送上来了，滨江是混合区域节点")
    @ApiModelProperty(value = "区域标识 0：本级 1：级联 2：混合，下级推送给上级的本级点")
    private String cascadeType;

    /** 区域类型， 国标区域：0， 雪亮工程区域：1， 司法行政区域：2， 自定义区域：9， 历史兼容版本占用普通区域:10， 级联区域:11， 楼栋单元:12 */
    @Excel(name = "区域类型， 国标区域：0， 雪亮工程区域：1， 司法行政区域：2， 自定义区域：9， 历史兼容版本占用普通区域:10， 级联区域:11， 楼栋单元:12")
    @ApiModelProperty(value = "区域类型， 国标区域：0， 雪亮工程区域：1， 司法行政区域：2， 自定义区域：9， 历史兼容版本占用普通区域:10， 级联区域:11， 楼栋单元:12")
    private String catalogType;

    /** 外码(如：国际码) */
    @Excel(name = "外码(如：国际码)")
    @ApiModelProperty(value = "外码(如：国际码)")
    private String externalIndexCode;

    /** 同级区域顺序 */
    @Excel(name = "同级区域顺序")
    @ApiModelProperty(value = "同级区域顺序")
    private String sort;

    /** 区域完整路径，含本节点，@进行分割，上级节点在前 */
    @Excel(name = "区域完整路径，含本节点，@进行分割，上级节点在前")
    @ApiModelProperty(value = "区域完整路径，含本节点，@进行分割，上级节点在前")
    private String regionPath;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    /** 位置描述 */
    @Excel(name = "位置描述")
    @ApiModelProperty(value = "位置描述")
    private String positionDescribe;

    /** 权限标识 1:所有  2:个人 3:部门 */
    @Excel(name = "权限标识")
    @ApiModelProperty(value = "权限标识")
    private String authorityMark;

    /** 权限范围 */
    @Excel(name = "权限范围")
    @ApiModelProperty(value = "权限范围")
    private String authorityRange;

    /** 权限范围名称 */
    @Excel(name = "权限范围名称")
    @ApiModelProperty(value = "权限范围名称")
    @TableField(exist = false)
    private String authorityName;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 在线状态，0离线，1在线 */
    @Excel(name = "在线状态")
    @ApiModelProperty(value = "在线状态")
    private String online;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;



}
