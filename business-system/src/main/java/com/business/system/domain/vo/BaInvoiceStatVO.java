package com.business.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 发票税价合计
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaInvoiceStatVO {
    //发票数量
    private Long count;
    //税价合计
    private BigDecimal taxPriceTotal;
}
