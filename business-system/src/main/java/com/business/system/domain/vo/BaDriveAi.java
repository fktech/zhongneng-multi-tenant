package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.BaEnterprise;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 立项管理对象 ba_project
 *
 * @author ljb
 * @date 2022-12-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class BaDriveAi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String businesstype;
    private Double  typenum;


}
