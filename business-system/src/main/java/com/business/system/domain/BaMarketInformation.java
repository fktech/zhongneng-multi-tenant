package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 市场信息管理对象 ba_market_information
 *
 * @author ljb
 * @date 2023-03-22
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "市场信息管理对象",description = "")
@TableName("ba_market_information")
public class BaMarketInformation
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String id;

    /** 市场信息名称 */
    @Excel(name = "市场信息名称")
    @ApiModelProperty(value = "市场信息名称")
    private String name;

    /** 数据条数 */
    @Excel(name = "数据条数")
    @ApiModelProperty(value = "数据条数")
    private Long rowCount;

    /** 最近更新人 */
    @Excel(name = "最近更新人")
    @ApiModelProperty(value = "最近更新人")
    private String recently;

    /** 最近更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最近更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "最近更新时间")
    private Date recentlyTime;

    /** 市场信息更新 **/
    @ApiModelProperty(value = "市场信息更新")
    @TableField(exist = false)
    private List<BaRenew> baRenewList;

    /** 历史数据 **/
    @Excel(name = "历史数据")
    @ApiModelProperty(value = "历史数据")
    private Integer historicalData;

    /** 类型 **/
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 排序 **/
    @ApiModelProperty(value = "排序")
    @TableField(exist = false)
    private String sorts;

    /** 租户ID **/
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;



}
