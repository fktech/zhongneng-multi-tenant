package com.business.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 付款统计
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaPaymentStatVO {

    //本年实际付款
    private Long yearCount;

    //付款金额
    private BigDecimal yearActualPaymentAmount;

    //本月实际付款
    private Long monthCount;

    //本月付款金额
    private BigDecimal monthActualPaymentAmount;
}
