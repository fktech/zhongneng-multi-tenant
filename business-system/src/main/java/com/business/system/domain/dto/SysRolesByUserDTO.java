package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 根据用户查询角色DTO
 * @Date: 2023/4/4 10:54
 */
@Data
@ApiModel(value = "SysRolesByUserDTO对象", description = "根据用户查询角色DTO")
public class SysRolesByUserDTO implements Serializable {

    private static final long serialVersionUID = -2819327953622316262L;

    @ApiModelProperty("用户ID")
    private String userId;
}
