package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 钉钉人脸识别打卡对象
 *
 * @author js
 * @date 2024-7-5
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "钉钉人脸识别打卡对象DTO",description = "")
public class DingTalkClockDTO
{
    private static final long serialVersionUID = 1L;

    /** 打卡开始日期 */
    @ApiModelProperty(value = "打卡开始日期")
    private String workDateFrom;

    /** 打卡结束日期 */
    @ApiModelProperty(value = "打卡结束日期")
    private String workDateTo;

    /** 打卡用户 */
    @ApiModelProperty(value = "打卡用户")
    private List<String> userIdList;

}
