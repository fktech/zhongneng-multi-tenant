package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 发票信息列对象 ba_invoice
 *
 * @author single
 * @date 2022-12-28
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "发票信息列对象",description = "")
@TableName("ba_invoice")
public class BaInvoice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 结算单ID */
    //@Excel(name = "结算单ID")
    @ApiModelProperty(value = "结算单ID")
    private String settlementId;

    /** 合同ID */
    //@Excel(name = "合同ID")
    @ApiModelProperty(value = "合同ID")
    private String contractId;

    /** 合同名称 */
    //@Excel(name = "合同名称")
    @ApiModelProperty(value = "合同名称")
    private String contractName;

    /** 发票类型 */
    //@Excel(name = "发票类型")
    @ApiModelProperty(value = "发票类型")
    private String invoiceType;

    /** 开票金额 */
    //@Excel(name = "开票金额")
    @ApiModelProperty(value = "开票金额")
    private BigDecimal invoicedAmount;

    /** 收票单位 */
    //@Excel(name = "收票单位")
    @ApiModelProperty(value = "收票单位")
    private String buyer;


    /** 开票单位 */
    //@Excel(name = "开票单位")
    @ApiModelProperty(value = "开票单位")
    private String seller;

    /** 收票单位税号 */
    //@Excel(name = "收票单位税号")
    @ApiModelProperty(value = "收票单位税号")
    private String buyerUnit;

    /** 备注 */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 发票代码 */
    //@Excel(name = "发票代码")
    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    /** 发票号码 */
    //@Excel(name = "发票号码")
    @ApiModelProperty(value = "发票号码")
    private String invoiceNo;

    /** 税额 */
    //@Excel(name = "税额")
    @ApiModelProperty(value = "税额")
    private BigDecimal taxAmount;

    /** 发票凭证 */
    //@Excel(name = "发票凭证")
    @ApiModelProperty(value = "发票凭证")
    private String invoiceVoucher;

    /** 发票状态 */
    //@Excel(name = "发票状态")
    @ApiModelProperty(value = "发票状态")
    private String invoiceState;

    /** 开票金额 */
    //@Excel(name = "开票金额")
    @ApiModelProperty(value = "开票金额")
    private BigDecimal sellerAmount;

    /** 开票日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "开票日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "开票日期")
    private Date invoiceTime;


    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 进项发票 */
    //@Excel(name = "进项发票")
    @ApiModelProperty(value = "进项发票")
    private String annex;

    /** 下游结算单 */
    //@Excel(name = "下游结算单")
    @ApiModelProperty(value = "下游结算单")
    private String downstreamSettlement;

    /** 其他附件 */
    @Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;


    /** 审批流程Id */
    //@Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;


    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 开户信息 */
    @TableField(exist = false)
    private List<BaBank> bankList;

    /** 结算单信息 */
    @TableField(exist = false)
    private BaSettlement baSettlement;

    /** 开票备注*/
    //@Excel(name = "开票备注")
    @ApiModelProperty(value = "开票备注")
    private String voucherRemarks;

    /**
     * 全局编号
     */
    @Excel(name = "全局编号", needMerge = true)
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /** 收票单位名称 */
    @Excel(name = "开票单位/收票单位", needMerge = true)
    @TableField(exist = false)
    @ApiModelProperty(value = "收票单位名称")
    private String buyerName;

    /** 发票类别 */
    //@Excel(name = "发票类型", readConverterExp = "1=普通发票,2=增值税专业发票")
    @ApiModelProperty(value = "发票类别")
    private String invoiceCategory;

    /**
     * 货物名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "货物名称")
    private String goodsName;

    /**
     * 开票品名
     */
    @Excel(name = "开票品名", needMerge = true)
    @ApiModelProperty(value = "开票品名")
    private String invoiceProduct;

    /** 开票金额、收票金额 */
    @Excel(name = "税价合计", needMerge = true)
    @ApiModelProperty(value = "开票金额、收票金额")
    @TableField(exist = false)
    private BigDecimal taxPriceTotal;

    /** 数量 */
    @Excel(name = "数量合计", needMerge = true)
    @ApiModelProperty(value = "数量")
    @TableField(exist = false)
    private BigDecimal amountTotal;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd", needMerge = true)
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /**
     * 发起人名称
     */
    @Excel(name = "归属岗位", needMerge = true)
    @TableField(exist = false)
    private String userName;

    /** 银行ID **/
    @ApiModelProperty(value = "银行ID")
    private String bankId;

    /** 开户行 */
    @TableField(exist = false)
    private String otherBank;

    /** 合同启动ID */
    //@Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    @ApiModelProperty(value = "合同启动对象")
    @TableField(exist = false)
    private BaContractStart baContractStart;

    /**
     * 货物ID
     */
    //@Excel(name = "货物ID")
    @ApiModelProperty(value = "货物ID")
    private String goodsId;


    /** 数量 */
    //@Excel(name = "数量")
    @ApiModelProperty(value = "数量")
    private BigDecimal amount;

    /** 发票文件 */
    //@Excel(name = "发票文件")
    @ApiModelProperty(value = "发票文件")
    private String invoiceAnnex;

    /** 开户信息 */
    @TableField(exist = false)
    private BaBank baBank;

    /** 合同启动名称 */
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 待开票 */
    @ApiModelProperty(value = "待开票")
    @TableField(exist = false)
    private String openBallot;

    /** 发票信息详情对象列表 */
    @Excel(name = "开票信息")
    @ApiModelProperty(value = "发票信息详情对象列表")
    @TableField(exist = false)
    private List<BaInvoiceDetail> baInvoiceDetailList;




    /** 状态 */
    //@Excel(name = "审批状态", readConverterExp = "inputInvoice:verifying=审批中,inputInvoice:pass=已通过,inputInvoice:reject=已拒绝,inputInvoice:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;



    /** 工作流状态 **/
    //@Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 立项类型 **/
    @ApiModelProperty(value = "立项类型")
    @TableField(exist = false)
    private String projectType;

    /** 业务类型 */
    //@Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String projectTypes;

    /** 仓储立项编号 **/
    //@Excel(name = "仓储立项编号")
    @ApiModelProperty(value = "仓储立项编号")
    private String projectId;

    /** 仓储立项名 **/
    @ApiModelProperty(value = "仓储立项名")
    @TableField(exist = false)
    private String projectName;

    /** 采购计划编号 **/
    //@Excel(name = "采购计划编号")
    @ApiModelProperty(value = "采购计划编号")
    private String planId;

    /** 采购计划名 **/
    @ApiModelProperty(value = "采购计划名")
    @TableField(exist = false)
    private String planName;

    /** 订单ID **/
    //@Excel(name = "订单ID")
    @ApiModelProperty(value = "订单ID")
    private String orderId;

    /** 订单名称 **/
    @ApiModelProperty(value = "订单名称")
    @TableField(exist = false)
    private String orderName;

    /** 租户ID **/
    @ApiModelProperty(value = "租户ID")
    private String tenantId;


    /**
     * 是否为回款时间计算节点
     */
    @ApiModelProperty(value = "是否为回款时间计算节点")
    private String collectionTime;


    /** 预计回款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "预计回款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计回款时间")
    private Date expectReturnTime;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    /**
     * 创建人公司名称
     */
    //@Excel(name = "创建人公司名称")
    @TableField(exist = false)
    private String comName;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

    /**
     * 进项发票种类
     */
    @ApiModelProperty(value = "进项发票种类")
    private String invoiceIlk;
}
