package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 组织架构信息统计
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaUserVO对象",description="组织架构信息统计")
public class BaUserVO
{
    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String depName;

    /** 人员数量 */
    @ApiModelProperty(value = "人员数量")
    private int num;
}
