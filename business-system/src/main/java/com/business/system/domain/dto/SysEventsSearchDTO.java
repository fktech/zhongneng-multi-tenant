package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * @version 1.0
 * @Author: js
 * @Description: 用户
 * @Date: 2022/11/29 10:02
 */
@Data
@ApiModel(value = "SysEventsSearchDTO对象", description = "联动事件")
public class SysEventsSearchDTO implements Serializable {

    private static final long serialVersionUID = 5585852811162906546L;

    @ApiModelProperty("事件规则")
    private String eventRuleId;

    @ApiModelProperty("事件分类")
    private String ability;

    @ApiModelProperty("区域编号")
    private String regionIndexCode;

    @ApiModelProperty("所属位置")
    private List<String> locationIndexCodes;

    @ApiModelProperty("事件源名称")
    private String resName;

    @ApiModelProperty("事件源编号")
    private List<String> resIndexCodes;

    @ApiModelProperty("事件源类型")
    private String resTypes;

    @ApiModelProperty("事件类型")
    private String eventTypes;

    @ApiModelProperty("事件等级")
    private String[] eventLevels;

    @ApiModelProperty("处理意见")
    private String remark;

    @ApiModelProperty("处理状态")
    private String handleStatus;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("分页大小")
    private String pageSize;

    @ApiModelProperty("页码")
    private String pageNo;

    @ApiModelProperty("接参")
    private String indexCode;
}
