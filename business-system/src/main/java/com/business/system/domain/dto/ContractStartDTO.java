package com.business.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "contractStartDTO对象", description = "合同发起DTO对象")
public class ContractStartDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 主键 */
    @ApiModelProperty(value = "主键")
    private String id;

    /** 合同启动名称 */
    @ApiModelProperty(value = "合同启动名称")
    private String name;

    /** 业务线 */
    @ApiModelProperty(value = "业务线")
    private String businessLines;

    /** 吨数 */
    @ApiModelProperty(value = "吨数")
    private BigDecimal tonnage;

    /** 发起人名称 */
    @ApiModelProperty(value = "发起人名称")
    private String userName;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /** 贸易金额 */
    @ApiModelProperty(value = "贸易金额(元)")
    private BigDecimal tradeAmount;

    /** 实际发运量 */
    @ApiModelProperty(value = "实际发运量")
    private BigDecimal checkCoalNum;


    /** 资金占用 */
    @ApiModelProperty(value = "资金占用")
    private BigDecimal fundOccupancy;

    /** 毛利润 */
    @ApiModelProperty(value = "毛利润")
    private BigDecimal grossProfit;

    /** 待收款 **/
    @ApiModelProperty(value = "待收款")
    private BigDecimal toBeCollected;

}
