package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 融资审批对象 ba_financing_approval
 *
 * @author single
 * @date 2023-05-03
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "融资审批对象",description = "")
@TableName("ba_financing_approval")
public class BaFinancingApproval extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 融资编号 */
    @Excel(name = "融资编号")
    @ApiModelProperty(value = "融资编号")
    private String financingNum;

    /** 融资企业 */
    @Excel(name = "融资企业")
    @ApiModelProperty(value = "融资企业")
    private String financingCompany;

    /** 申请金额 */
    @Excel(name = "申请金额")
    @ApiModelProperty(value = "申请金额")
    private BigDecimal applicationsAmount;

    /** 申请期限 */
    @Excel(name = "申请期限")
    @ApiModelProperty(value = "申请期限")
    private String applicationDeadline;

    /** 利率 */
    @Excel(name = "利率")
    @ApiModelProperty(value = "利率")
    private String interestRate;

    /** 质押物 */
    @Excel(name = "质押物")
    @ApiModelProperty(value = "质押物")
    private String pledgedProperty;

    /** 资金用途 */
    @Excel(name = "资金用途")
    @ApiModelProperty(value = "资金用途")
    private String fundUse;

    /** 审批状态 */
    @Excel(name = "审批状态")
    @ApiModelProperty(value = "审批状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户编号 */
    @Excel(name = "用户编号")
    @ApiModelProperty(value = "用户编号")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 部门编号 */
    @Excel(name = "部门编号")
    @ApiModelProperty(value = "部门编号")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流ID */
    @Excel(name = "审批流ID")
    @ApiModelProperty(value = "审批流ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 材料 */
    @Excel(name = "材料")
    @ApiModelProperty(value = "材料")
    private String material;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 审批金额 */
    @Excel(name = "审批金额")
    @ApiModelProperty(value = "审批金额")
    private BigDecimal approveAmount;

    /** 审批贷款期限 */
    @Excel(name = "审批贷款期限")
    @ApiModelProperty(value = "审批贷款期限")
    private String approveDeadline;

    /** 审批利率 */
    @Excel(name = "审批利率")
    @ApiModelProperty(value = "审批利率")
    private String approveInterestRate;

    /** 审批备注 */
    @Excel(name = "审批备注")
    @ApiModelProperty(value = "审批备注")
    private String approveRemarks;

    /** 合同启动编号 */
    @Excel(name = "合同启动编号")
    @ApiModelProperty(value = "合同启动编号")
    private String startId;

    /** 立项编号 */
    @Excel(name = "立项编号")
    @ApiModelProperty(value = "立项编号")
    private String projectId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;


}
