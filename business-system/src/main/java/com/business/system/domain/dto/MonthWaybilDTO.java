package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "网络货运当月运单DTO",description = "网络货运统计当月运单对象")
public class MonthWaybilDTO {

    /** 收货重量 */
    @ApiModelProperty(value = "收货重量")
    private BigDecimal weight;

    /** 发货重量 */
    @ApiModelProperty(value = "发货重量")
    private BigDecimal deliveryWeight;

    /** 运费 */
    @ApiModelProperty(value = "运费")
    private BigDecimal freight;

    /** 司机 */
    @ApiModelProperty(value = "司机")
    private String carDriver;

    /** 车牌号 */
    @ApiModelProperty(value = "车牌号")
    private String carNo;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /** 查询条件 */
    @ApiModelProperty(value = "查询条件")
    private String condition;

}
