package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 报销申请对象 oa_claim
 *
 * @author single
 * @date 2023-12-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "报销申请对象",description = "")
@TableName("oa_claim")
public class OaClaim extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    //发起人名称
    @Excel(name = "申请人", needMerge = true)
    @TableField(exist = false)
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门", needMerge = true)
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 申请日期 */
    @Excel(name = "申请日期", width = 30, dateFormat = "yyyy-MM-dd", needMerge = true)
    @TableField(exist = false)
    private Date applyDate;


    /** 状态 */
    @Excel(name = "审批状态", needMerge = true, readConverterExp = "oaClaim:verifying=审批中,oaClaim:pass=已通过,oaClaim:reject=已拒绝,oaClaim:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", needMerge = true)
    @TableField(exist = false)
    private Date createDate;

    /** 报销明细 */
    @Excel(name = "报销明细")
    @ApiModelProperty(value = "报销明细")
    @TableField(exist = false)
    private List<OaClaimDetail> oaClaimDetailList;

    /** 报销金额 */
    @TableField(exist = false)
    @ApiModelProperty(value = "报销金额")
    private BigDecimal claimAmounts;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 提交状态 */
    @ApiModelProperty(value = "提交状态")
    private String submitState;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 银行账号 */
    @ApiModelProperty(value = "银行账号")
    private String account;

    /** 开户行 */
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 付款方式 */
    @ApiModelProperty(value = "付款方式")
    private String paymentType;

    /** 项目ID */
    @ApiModelProperty(value = "项目ID")
    private String projectId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 收款人全称 */
    @ApiModelProperty(value = "收款人全称")
    private String collectionUser;

    /** 备注 */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 项目名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 用户ID集合 */
    @ApiModelProperty(value = "用户ID集合")
    @TableField(exist = false)
    private String userIds;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 报销类型 */
    @ApiModelProperty(value = "报销类型")
    @TableField(exist = false)
    private String types;
}
