package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 煤源库对象 ba_coal_source_storage
 *
 * @author single
 * @date 2024-01-25
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "煤源库对象",description = "")
@TableName("ba_coal_source_storage")
public class BaCoalSourceStorage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 产地 */
    @ApiModelProperty(value = "产地")
    private String producer;

    /** 煤种 */
    @ApiModelProperty(value = "煤种")
    private String coalSort;

    /** 详细地址 */
    @ApiModelProperty(value = "详细地址")
    private String address;

    /** 经度 */
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** Mt */
    @ApiModelProperty(value = "Mt")
    private BigDecimal indexMt;

    /** Ad */
    @ApiModelProperty(value = "Ad")
    private BigDecimal indexAd;

    /** Vdaf */
    @ApiModelProperty(value = "Vdaf")
    private BigDecimal indexVdaf;

    /** St,d */
    @ApiModelProperty(value = "St,d")
    private BigDecimal indexSt;

    /** G */
    @ApiModelProperty(value = "G")
    private BigDecimal indexG;

    /** Y */
    @ApiModelProperty(value = "Y")
    private BigDecimal indexY;

    /** Ro */
    @ApiModelProperty(value = "Ro")
    private BigDecimal indexRo;

    /** 标准差 */
    @ApiModelProperty(value = "标准差")
    private BigDecimal standardDeviation;

    /** 到厂价 */
    @ApiModelProperty(value = "到厂价")
    private BigDecimal factoryPrice;

    /** 联系人 */
    @ApiModelProperty(value = "联系人")
    private String counterpart;

    /** 联系人电话 */
    @ApiModelProperty(value = "联系人电话")
    private String phone;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private String type;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String file;

    /** 用户名称 */
    @Excel(name = "姓名")
    @TableField(exist = false)
    @ApiModelProperty(value = "用户名称")
    private String nickName;

    /** 部门名称 */
    @Excel(name = "部门")
    @TableField(exist = false)
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 新增煤源数量 */
    @Excel(name = "新增煤源数量")
    @TableField(exist = false)
    @ApiModelProperty(value = "新增煤源数量")
    private String currCount;

    @Excel(name = "已上传煤源总数")
    /** 已上传煤源总数 */
    @TableField(exist = false)
    @ApiModelProperty(value = "已上传煤源总数")
    private String allCount;

    /** 创建人名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /** 最近上传时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最近上传时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    @ApiModelProperty(value = "最近上传时间")
    private Date maxCreateTime;

    /** 日期查询 */
    @ApiModelProperty(value = "日期查询")
    @TableField(exist = false)
    private String queryTime;

    /** 用户IDs */
    @TableField(exist = false)
    @ApiModelProperty(value = "用户IDs")
    private String userIds;

    /** X */
    @ApiModelProperty(value = "X")
    private BigDecimal indexX;

}
