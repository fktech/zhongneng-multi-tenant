package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (HiIdentitylink)实体类
 *
 * @author js
 * @since 2023-3-09 22:07
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProcessHiIdentitylink implements Serializable {

    private static final long serialVersionUID = -2074332100013363961L;

    /**
    * 审批摸板ID
    */
    private String id;

    private String groupId;

    private String type;

    private String userId;

    private String userType;

    private String taskId;

    private Date createTime;

    private String processInstanceId;

    private String scopeId;

    private String subScopeId;

    private String scopeType;

    private String scopeDefinitionId;

    private String taskNode;
}
