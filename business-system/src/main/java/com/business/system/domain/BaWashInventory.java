package com.business.system.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 洗煤配煤入库清单对象 ba_wash_inventory
 *
 * @author single
 * @date 2024-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "洗煤配煤入库清单对象",description = "")
@TableName("ba_wash_inventory")
public class BaWashInventory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 库位ID */
    @Excel(name = "库位ID")
    @ApiModelProperty(value = "库位ID")
    private String locationId;

    /** 产品id */
    @Excel(name = "产品id")
    @ApiModelProperty(value = "产品id")
    private String goodsId;

    /** 损耗量 */
    @Excel(name = "损耗量")
    @ApiModelProperty(value = "损耗量")
    private BigDecimal washTotal;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 关联主键 */
    @Excel(name = "关联主键")
    @ApiModelProperty(value = "关联主键")
    private String relationId;

    /** 规格型号 */
    @Excel(name = "规格型号")
    @ApiModelProperty(value = "规格型号")
    private String goodSpec;

    /** 计量单位 */
    @Excel(name = "计量单位")
    @ApiModelProperty(value = "计量单位")
    private String unitId;

    /** 库存总量 */
    @Excel(name = "库存总量")
    @ApiModelProperty(value = "库存总量")
    private BigDecimal totals;

    /** 单价 */
    @Excel(name = "单价")
    @ApiModelProperty(value = "单价")
    private BigDecimal procurePrice;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 商品名称 **/
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** 商品类型 **/
    @ApiModelProperty(value = "商品类型")
    @TableField(exist = false)
    private String goodsType;

    /** 单位名称 **/
    @ApiModelProperty(value = "单位名称")
    @TableField(exist = false)
    private String unitName;


    @TableField(exist = false)
    @ApiModelProperty(value = "仓位名称")
    private String locationName;

    /** 库存id */
    @Excel(name = "库存id")
    @ApiModelProperty(value = "库存id")
    private String materialStockId;

    @TableField(exist = false)
    @ApiModelProperty(value = "编号")
    private String headNum;

    @TableField(exist = false)
    @ApiModelProperty(value = "时间")
    private String washTime;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 类型 **/
    @ApiModelProperty(value = "类型")
    @TableField(exist = false)
    private String type;

    /** 业务类型 **/
    @ApiModelProperty(value = "业务类型")
    @TableField(exist = false)
    private String subType;

    /** 编号 **/
    @ApiModelProperty(value = "编号")
    @TableField(exist = false)
    private String idCode;

}
