package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 中储智运无车承运开票对象 ba_automobile_settlement_invoice_cmst
 *
 * @author single
 * @date 2023-08-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "中储智运无车承运开票对象",description = "")
@TableName("ba_automobile_settlement_invoice_cmst")
public class BaAutomobileSettlementInvoiceCmst extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单号 */
    @Excel(name = "订单号")
    @ApiModelProperty(value = "订单号")
    private String orderId;

    /** 运单开票金额 */
    @Excel(name = "运单开票金额")
    @ApiModelProperty(value = "运单开票金额")
    private BigDecimal invoiceMoney;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 发票含税金额 */
    @Excel(name = "发票含税金额")
    @ApiModelProperty(value = "发票含税金额")
    private BigDecimal invoiceTaxMoney;

    /** 发票不含税金额 */
    @Excel(name = "发票不含税金额")
    @ApiModelProperty(value = "发票不含税金额")
    private BigDecimal noTaxMoney;

    /** 发票抬头 */
    @Excel(name = "发票抬头")
    @ApiModelProperty(value = "发票抬头")
    private String invoiceTitle;

    /** 运单优惠奖励金额 */
    @Excel(name = "运单优惠奖励金额")
    @ApiModelProperty(value = "运单优惠奖励金额")
    private BigDecimal rebateMoney;

    /** 开票状态 1 已开票 0未开票（对应发票作废） */
    @Excel(name = "开票状态 1 已开票 0未开票", readConverterExp = "对=应发票作废")
    @ApiModelProperty(value = "开票状态 1 已开票 0未开票")
    private String invoiceState;

    /** 发票号 */
    @Excel(name = "发票号")
    @ApiModelProperty(value = "发票号")
    private String invoiceNo;

    /** 发票代码 */
    @Excel(name = "发票代码")
    @ApiModelProperty(value = "发票代码")
    private String invoiceCode;

    /** 开票日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开票日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开票日期")
    private Date invoiceDate;
    /** 货主名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "货主名称")
    private String consignorUserName;
    @TableField(exist = false)

    @ApiModelProperty(value = "运单数")
    private Integer baShippingOrderCmstNumber;

    @TableField(exist = false)
    @ApiModelProperty(value = "服务费")
    private BigDecimal serviceCharge;

    @TableField(exist = false)
    @ApiModelProperty(value = "运费金额")
    private BigDecimal freight;

    /** 开票审核状态 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "开票审核状态")
    private Integer checkFlag;
    /** 货主名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运单对象")
    private List<BaShippingOrderCmst> baShippingOrderCmst;
}
