package com.business.system.domain.dto;

import com.business.system.domain.OaEntertain;
import com.business.system.domain.OaOther;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 其他申请与流程实例关联关系审批编辑对象
 * @date: 2023/12/12
 */
@Data
@ApiModel(value = "OaOtherInstanceRelatedEditDTO对象", description = "其他申请与流程实例关联关系审批编辑对象")
public class OaOtherInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private OaOther oaOther;
}
