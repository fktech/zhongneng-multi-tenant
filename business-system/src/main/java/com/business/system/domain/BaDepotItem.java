package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 出入库副对象 ba_depot_item
 *
 * @author ljb
 * @date 2023-04-23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "出入库副对象",description = "")
@TableName("ba_depot_item")
public class BaDepotItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 表头 */
    @Excel(name = "表头")
    @ApiModelProperty(value = "表头")
    private String headerId;

    /** 商品id */
    @Excel(name = "商品id")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    /** 商品扩展 */
    @Excel(name = "商品扩展")
    @ApiModelProperty(value = "商品扩展")
    private String goodsExtendId;

    /** 商品计量单位 */
    @Excel(name = "商品计量单位")
    @ApiModelProperty(value = "商品计量单位")
    private String goodsUnit;

    /** 商品属性 */
    @Excel(name = "商品属性")
    @ApiModelProperty(value = "商品属性")
    private String sku;

    /** 数量 */
    @Excel(name = "数量")
    @ApiModelProperty(value = "数量")
    private Long operNumber;

    /** 基础数量 */
    @Excel(name = "基础数量")
    @ApiModelProperty(value = "基础数量")
    private Long basicNumber;

    /** 单价 */
    @Excel(name = "单价")
    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    /** 含税单价 */
    @Excel(name = "含税单价")
    @ApiModelProperty(value = "含税单价")
    private BigDecimal taxUnitPrice;

    /** 金额 */
    @Excel(name = "金额")
    @ApiModelProperty(value = "金额")
    private BigDecimal allPrice;

    /** 调拨仓库id */
    @Excel(name = "调拨仓库id")
    @ApiModelProperty(value = "调拨仓库id")
    private String anotherDepotId;

    /** 税率 */
    @Excel(name = "税率")
    @ApiModelProperty(value = "税率")
    private String taxRate;

    /** 税额 */
    @Excel(name = "税额")
    @ApiModelProperty(value = "税额")
    private BigDecimal taxMoney;

    /** 价税合计 */
    @Excel(name = "价税合计")
    @ApiModelProperty(value = "价税合计")
    private BigDecimal taxLastMoney;

    /** 商品类型 */
    @Excel(name = "商品类型")
    @ApiModelProperty(value = "商品类型")
    private String goodsType;

    /** 供应商 */
    @Excel(name = "供应商")
    @ApiModelProperty(value = "供应商")
    private String supplierId;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String thdno;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String carno;

    /** 装车时间（原发时间） */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "装车时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "装车时间")
    private Date yfdate;

    /** 卸车时间（进场时间） */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "卸车时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "卸车时间")
    private Date dhdate;

    /** 装车净重（出场净重） */
    @Excel(name = "装车净重", readConverterExp = "出=场净重")
    @ApiModelProperty(value = "装车净重")
    private BigDecimal ccjweight;

    /** 卸车净重（入场净重） */
    @Excel(name = "卸车净重", readConverterExp = "入=场净重")
    @ApiModelProperty(value = "卸车净重")
    private BigDecimal rcjweight;

    /** 装车皮重 */
    @Excel(name = "装车皮重")
    @ApiModelProperty(value = "装车皮重")
    private BigDecimal loadingTare;

    /** 装车毛重 */
    @Excel(name = "装车毛重")
    @ApiModelProperty(value = "装车毛重")
    private BigDecimal loadingGrossWeight;

    /** 卸车皮重 */
    @Excel(name = "卸车皮重")
    @ApiModelProperty(value = "卸车皮重")
    private BigDecimal unloadTare;

    /** 卸车毛重 */
    @Excel(name = "卸车毛重")
    @ApiModelProperty(value = "卸车毛重")
    private BigDecimal unloadGrossWeight;

    /** 运单id **/
    @Excel(name = "运单id")
    @ApiModelProperty(value = "运单id")
    private String waybillId;


    /** 运单对象 **/
    @ApiModelProperty(value = "运单对象")
    @TableField(exist = false)
    private BaShippingOrder baShippingOrder;


}
