package com.business.system.domain.dto;

import com.business.system.domain.OaContractSeal;
import com.business.system.domain.OaEntertain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 合同用印申请与流程实例关联关系审批编辑对象
 * @date: 2024/6/7
 */
@Data
@ApiModel(value = "OaContractSealInstanceRelatedEditDTO对象", description = "合同用印申请与流程实例关联关系审批编辑对象")
public class OaContractSealInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private OaContractSeal oaContractSeal;
}
