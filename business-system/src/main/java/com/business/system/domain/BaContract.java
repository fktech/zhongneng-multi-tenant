package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 合同对象 ba_contract
 *
 * @author ljb
 * @date 2022-12-07
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "合同对象",description = "")
@TableName("ba_contract")
public class BaContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /**
     * 全局编号
     */
    @Excel(name = "全局编号")
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 合同名称 */
    @Excel(name = "合同名称")
    @ApiModelProperty(value = "合同名称")
    private String name;

    /** 合同类型 */
    @Excel(name = "合同类型", readConverterExp = "6=上游合同,7=下游合同,9=上游补充协议,10=下游补充协议,5=运输合同,8=其他")
    @ApiModelProperty(value = "合同类型")
    private String contractType;

    /** 合同编号 */
    @Excel(name = "合同编号")
    @ApiModelProperty(value = "合同编号")
    private String num;


    /** 项目编号 */
    //@Excel(name = "项目编号")
    @ApiModelProperty(value = "项目编号")
    private String projectId;


    /** 项目名称 */
    //@Excel(name = "项目名称")
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 项目对象 */
    @ApiModelProperty(value = "项目对象")
    @TableField(exist = false)
    private List<BaProject> baProjects;

    /** 项目结算比例 **/
    @TableField(exist = false)
    private String settlementProportion;

    /** 签订单位 */
    //@Excel(name = "签订单位")
    @ApiModelProperty(value = "签订单位")
    private String signedBy;

    /** 单位编号 */
    //@Excel(name = "单位编号")
    @ApiModelProperty(value = "单位编号")
    private String companyId;

    /** 合同金额(元) */
    //@Excel(name = "合同金额(元)")
    @ApiModelProperty(value = "合同金额(元)")
    private BigDecimal amount;

    /** 合同签订日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "合同签订日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "合同签订日期")
    private Date signingDate;

    /** 合同结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "合同结束日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "合同结束日期")
    private Date endTime;

    /** 合同开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "合同开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "合同开始时间")
    private Date startTime;

    /** 对方单位名称 */
    @Excel(name = "对方单位名称")
    @ApiModelProperty(value = "对方单位名称")
    @TableField(exist = false)
    private String otherUtilsId;

    /** 合同总价 */
    @Excel(name = "合同单价")
    @ApiModelProperty(value = "合同总价")
    private BigDecimal priceTotal;

    /** 合同总量 */
    @Excel(name = "合同吨数")
    @ApiModelProperty(value = "合同总量")
    private BigDecimal contractTotal;

    /** 履约期 */
    @Excel(name = "履约期")
    @ApiModelProperty(value = "履约期")
    @TableField(exist = false)
    private String performancePeriod;

    /** 合同附件 */
    //@Excel(name = "合同附件")
    @ApiModelProperty(value = "合同附件")
    private String contractAnnex;

    /** 其他附件 */
    //@Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 申请备注 */
    //@Excel(name = "申请备注")
    @ApiModelProperty(value = "申请备注")
    private String applyRemarks;

    /** 签订结果 */
    //@Excel(name = "签订结果")
    @ApiModelProperty(value = "签订结果")
    private String signingResult;

    /** 结果备注 */
    //@Excel(name = "结果备注")
    @ApiModelProperty(value = "结果备注")
    private String resultRemarks;

    /** 已签合同文件 */
    //@Excel(name = "已签合同文件")
    @ApiModelProperty(value = "已签合同文件")
    private String file;


    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @Excel(name = "归属岗位")
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起所属公司
    @TableField(exist = false)
    private String comName;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    //@Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程id */
    //@Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 供应商ID */
    //@Excel(name = "供应商ID")
    @ApiModelProperty(value = "供应商ID")
    private String supplierId;

    /** 供应商名称 */
    //@Excel(name = "供应商名称")
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 用煤单位ID */
    //@Excel(name = "用煤单位ID")
    @ApiModelProperty(value = "用煤单位ID")
    private String enterpriseId;

    /** 用煤单位名称 */
    //@Excel(name = "用煤单位名称")
    @ApiModelProperty(value = "用煤单位名称")
    private String enterpriseName;

    /** 商品ID */
    //@Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 商品名称 */
    //@Excel(name = "商品名称")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;


    /** 参数指标 */
    //@Excel(name = "参数指标")
    @ApiModelProperty(value = "参数指标")
    private String parameterIndex;

    /** 始发地 */
    //@Excel(name = "始发地")
    @ApiModelProperty(value = "始发地")
    private String origin;

    /** 目的地 */
    //@Excel(name = "目的地")
    @ApiModelProperty(value = "目的地")
    private String destination;

    /** 合同状态 */
    @Excel(name = "业务状态")
    @ApiModelProperty(value = "合同状态")
    private String contractState;

    //开户信息
    @TableField(exist = false)
    private List<BaBank> bankList;

    /** 签订状态 */
    //@Excel(name = "签订状态")
    @ApiModelProperty(value = "签订状态")
    private String signingStatus;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 单价
     */
    //@Excel(name = "单价")
    @ApiModelProperty(value = "单价")
    private BigDecimal unitPrice;

    /** 交货地点 */
    //@Excel(name = "交货地点")
    @ApiModelProperty(value = "交货地点")
    private String deliveryPlace;

    /**
     * 合同累计发量（吨）
     */
    @TableField(exist = false)
    private BigDecimal cumulativeVolume;

    /** 银行账户id */
    //@Excel(name = "银行账户id")
    @ApiModelProperty(value = "银行账户id")
    private String bankId;

    /** 合同启动编号 */
    //@Excel(name = "合同启动编号")
    @ApiModelProperty(value = "合同启动编号")
    private String startId;

    /** 不包含合同启动编号 */
    @ApiModelProperty(value = "不包含合同启动编号")
    @TableField(exist = false)
    private String notStartId;

    /** 合同启动编号 */
    @ApiModelProperty(value = "合同启动编号")
    @TableField(exist = false)
    private String startName;

    /** 合同启动对象 */
    @ApiModelProperty(value = "合同启动对象")
    @TableField(exist = false)
    private List<BaContractStart> baContractStart;

    /** 我方单位名称 */
    //@Excel(name = "我方单位名称")
    @ApiModelProperty(value = "我方单位名称")
    private String ourUtilsName;

    /** 对方单位名称 */
    //@Excel(name = "对方单位名称")
    @ApiModelProperty(value = "对方单位名称")
    private String otherUtilsName;

    /** 快递公司 */
    //@Excel(name = "快递公司")
    @ApiModelProperty(value = "快递公司")
    private String expressCompany;

    /** 对方快递公司 */
    //@Excel(name = "对方快递公司")
    @ApiModelProperty(value = "对方快递公司")
    private String otherExpressCompany;

    /** 快递单号 */
    //@Excel(name = "快递单号")
    @ApiModelProperty(value = "快递单号")
    private String trackingNumber;

    /** 对方快递单号 */
    //@Excel(name = "对方快递单号")
    @ApiModelProperty(value = "对方快递单号")
    private String otherTrackingNumber;

    /** 邮寄信息 */
    //@Excel(name = "邮寄信息")
    @ApiModelProperty(value = "邮寄信息")
    private String mailingInformation;

    /** 对方邮寄信息 */
    //@Excel(name = "对方邮寄信息")
    @ApiModelProperty(value = "对方邮寄信息")
    private String otherMailingInformation;

    /** 我方责任人 */
    //@Excel(name = "我方责任人")
    @ApiModelProperty(value = "我方责任人")
    private String weResponsiblePerson;

    /** 对方责任人 */
    //@Excel(name = "对方责任人")
    @ApiModelProperty(value = "对方责任人")
    private String otherResponsiblePerson;


    /** 合同信息链接 */
    //@Excel(name = "合同信息链接")
    @ApiModelProperty(value = "合同信息链接")
    private String contractLink;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 是否绑定合同启动 */
    @ApiModelProperty(value = "是否绑定合同启动")
    @TableField(exist = false)
    private String startState;

    @TableField(exist = false)
    @ApiModelProperty(value = "多条件查询")
    private String keyWords;

    /** 销售订单 */
    //@Excel(name = "销售订单")
    @ApiModelProperty(value = "销售订单")
    private String orderId;

    /** 销售订单对象 */
    @ApiModelProperty(value = "销售订单对象")
    @TableField(exist = false)
    private List<BaContractStart> orders;

    /** 仓储立项 */
    //@Excel(name = "仓储立项")
    @ApiModelProperty(value = "仓储立项")
    private String storageId;

    /** 仓储立项对象 */
    @ApiModelProperty(value = "仓储立项对象")
    @TableField(exist = false)
    private List<BaProject> storageList;

    /** 不包含仓储立项 */
    @ApiModelProperty(value = "不包含仓储立项")
    @TableField(exist = false)
    private String notStorageId;

    /** 关联立项、合同启动、销售订单名称 */
    @ApiModelProperty(value = "关联立项、合同启动、销售订单名称")
    @TableField(exist = false)
    private String connProjectName;

    /** 关联合同 */
    //@Excel(name = "关联合同")
    @ApiModelProperty(value = "关联合同")
    private String relationContract;

    /** 关联合同名称 */
    //@Excel(name = "关联合同名称")
    @ApiModelProperty(value = "关联合同名称")
    @TableField(exist = false)
    private String relationContractName;

    /** 关联补充协议 */
    @ApiModelProperty(value = "关联补充协议")
    @TableField(exist = false)
    private List<BaContract> baContractList;

    /** 仓库id */
    @ApiModelProperty(value = "仓库id")
    @TableField(exist = false)
    private String depotId;

    /** 我方单位Id */
    //@Excel(name = "我方单位Id")
    @ApiModelProperty(value = "我方单位Id")
    private String ourUtilsId;

    /** 销售订单 */
    //@Excel(name = "销售订单")
    @ApiModelProperty(value = "销售订单")
    private String saleOrder;

    /** 销售订单对象 **/
    @ApiModelProperty(value = "销售订单对象")
    @TableField(exist = false)
    private List<BaContractStart> saleOrderList;

    /** 采购订单 */
    //@Excel(name = "采购订单")
    @ApiModelProperty(value = "采购订单")
    private String procureOrder;

    /** 采购订单对象 **/
    @ApiModelProperty(value = "采购订单对象")
    @TableField(exist = false)
    private List<BaProcurementPlan> procureOrderList;

    /** 租户ID */
    @ApiModelProperty(value = "采购订单")
    private String tenantId;

    /** 判断条件 */
    @ApiModelProperty(value = "判断条件")
    @TableField(exist = false)
    private String judge;


    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;


    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
