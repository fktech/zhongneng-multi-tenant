package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.*;
import com.business.system.domain.dto.json.UserInfo;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: js
 * @Description: 归档附件
 * @date: 2023/2/10 19:05
 */
@Data
@ApiModel(value="BaContractVO对象",description="归档附件对象")
public class BaContractVO implements Serializable {

    private static final long serialVersionUID = -5566043497952968276L;

    /** 主键 */
    private String id;

    /** 合同编号 */
    @ApiModelProperty(value = "合同编号")
    private String num;

    /** 合同名称 */
    @ApiModelProperty(value = "合同名称")
    private String name;

    /** 合同类型 */
    @ApiModelProperty(value = "合同类型")
    private String contractType;

    /** 项目编号 */
    @ApiModelProperty(value = "项目编号")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 签订单位 */
    @ApiModelProperty(value = "签订单位")
    private String signedBy;

    /** 单位编号 */
    @ApiModelProperty(value = "单位编号")
    private String companyId;

    /** 合同金额(元) */
    @ApiModelProperty(value = "合同金额(元)")
    private BigDecimal amount;

    /** 合同签订日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "合同签订日期")
    private Date signingDate;

    /** 合同结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "合同结束日期")
    private Date endTime;

    /** 合同开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "合同开始时间")
    private Date startTime;

    /** 合同附件 */
    @ApiModelProperty(value = "合同附件")
    private String contractAnnex;

    /** 申请备注 */
    @ApiModelProperty(value = "申请备注")
    private String applyRemarks;

    /** 签订结果 */
    @ApiModelProperty(value = "签订结果")
    private String signingResult;

    /** 结果备注 */
    @ApiModelProperty(value = "结果备注")
    private String resultRemarks;

    /** 已签合同文件 */
    @ApiModelProperty(value = "已签合同文件")
    private String file;

    /** 合同状态 */
    @ApiModelProperty(value = "合同状态")
    private String contractState;

    /** 项目信息 **/
    @ApiModelProperty(value = "项目信息")
    private BaProject baProject;

    /** 订单附件 **/
    @ApiModelProperty(value = "订单附件")
    private List<BaOrder> baOrderList;

    /** 结算附件 **/
    @ApiModelProperty(value = "结算附件")
    private List<BaSettlement> baSettlementList;

    /** 付款凭证附件 **/
    @ApiModelProperty(value = "付款凭证附件")
    private List<BaPayment> baPayments;

    /** 收款凭证附件 **/
    @ApiModelProperty(value = "收款凭证附件")
    private List<BaCollection> baCollections;

    /** 采购发票附件 **/
    @ApiModelProperty(value = "采购发票附件")
    private List<BaInvoice> baInvoices;
}
