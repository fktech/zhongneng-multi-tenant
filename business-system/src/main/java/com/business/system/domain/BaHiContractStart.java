package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.HomeDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 历史合同启动对象 ba_hi_contract_start
 *
 * @author ljb
 * @date 2023-03-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "历史合同启动对象",description = "")
@TableName("ba_hi_contract_start")
public class BaHiContractStart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 项目ID */
    @Excel(name = "项目ID")
    @ApiModelProperty(value = "项目ID")
    private String projectId;

    /** 项目编号 */
    @Excel(name = "项目编号")
    @ApiModelProperty(value = "项目编号")
    private String projectNum;

    /** 项目名称 **/
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 立项吨费 **/
    @Excel(name = "立项吨费")
    @ApiModelProperty(value = "立项吨费")
    private BigDecimal tonnageCharge;

    /** 立项年化 **/
    @Excel(name = "立项年化")
    @ApiModelProperty(value = "立项年化")
    private BigDecimal annualization;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 供应商 */
    @Excel(name = "供应商")
    @ApiModelProperty(value = "供应商")
    private String supplierId;

    /** 供应商名称 **/
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String supplierName;

    /** 终端企业名称 **/
    @ApiModelProperty(value = "终端企业名称")
    @TableField(exist = false)
    private String enterpriseName;

    /** 终端企业编号 **/
    @ApiModelProperty(value = "终端企业编号")
    @TableField(exist = false)
    private String enterpriseId;

    /** 终端企业 **/
    @Excel(name = "终端企业")
    @ApiModelProperty(value = "终端企业")
    private String enterprise;

    /** 托盘公司 */
    @Excel(name = "托盘公司")
    @ApiModelProperty(value = "托盘公司")
    private String companyId;

    /** 托盘公司名称 **/
    @ApiModelProperty(value = "托盘公司名称")
    @TableField(exist = false)
    private String companyName;

    /** 托盘公司年化 **/
    @Excel(name = "托盘公司年化")
    @ApiModelProperty(value = "托盘公司年化")
    private BigDecimal companyAnnualizedRate;

    /** 年化 */
    @Excel(name = "年化")
    @ApiModelProperty(value = "年化")
    private BigDecimal annualizedRate;

    /** 业务线 */
    @Excel(name = "业务线")
    @ApiModelProperty(value = "业务线")
    private String businessLines;

    /** 吨费调整 */
    @Excel(name = "吨费调整")
    @ApiModelProperty(value = "吨费调整")
    private BigDecimal tonnageFee;

    /** 年化调整 */
    @Excel(name = "年化调整")
    @ApiModelProperty(value = "年化调整")
    private BigDecimal annualizedAdjustment;

    /** 单价 */
    @Excel(name = "单价")
    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    /** 吨数 */
    @Excel(name = "吨数")
    @ApiModelProperty(value = "吨数")
    private BigDecimal tonnage;

    /** 参数指标 */
    @Excel(name = "参数指标")
    @ApiModelProperty(value = "参数指标")
    private String parameterIndex;

    /** 运输类型 */
    @Excel(name = "运输类型")
    @ApiModelProperty(value = "运输类型")
    private String transportWay;

    /** 运输杂费 */
    @Excel(name = "运输杂费")
    @ApiModelProperty(value = "运输杂费")
    private BigDecimal transportIncidentals;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal transportPrice;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /** 发站 */
    @Excel(name = "发站")
    @ApiModelProperty(value = "发站")
    private String departureStation;

    /** 终站 */
    @Excel(name = "终站")
    @ApiModelProperty(value = "终站")
    private String terminal;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String appendix;

    /** 补充信息 */
    @Excel(name = "补充信息")
    @ApiModelProperty(value = "补充信息")
    private String sideInformation;

    /** 货款单价 */
    @Excel(name = "货款单价")
    @ApiModelProperty(value = "货款单价")
    private BigDecimal freightPrice;

    /** 货款总价 */
    @Excel(name = "货款总价")
    @ApiModelProperty(value = "货款总价")
    private BigDecimal freightTotal;

    /** 运费总价 */
    @Excel(name = "运费总价")
    @ApiModelProperty(value = "运费总价")
    private BigDecimal carriageTotal;

    /** 付款开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "付款开始时间")
    private Date paymentStartTime;

    /** 付款结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "付款结束时间")
    private Date paymentEndTime;

    /** 回款开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "回款开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "回款开始时间")
    private Date collectionStartTime;

    /** 回款结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "回款结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "回款结束时间")
    private Date collectionEndTime;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 初始结算差额 */
    @Excel(name = "初始结算差额")
    @ApiModelProperty(value = "初始结算差额")
    private BigDecimal initialSettlementDifference;

    /** 当前结算差额 */
    @Excel(name = "当前结算差额")
    @ApiModelProperty(value = "当前结算差额")
    private BigDecimal currentSettlementDifference;

    /** 合同状态 */
    @Excel(name = "合同状态")
    @ApiModelProperty(value = "合同状态")
    private String contractStatus;

    /** 合同启动类型 */
    @Excel(name = "合同启动类型")
    @ApiModelProperty(value = "合同启动类型")
    private String type;

    /** 采购量/销售量 */
    @Excel(name = "采购量")
    @ApiModelProperty(value = "采购量")
    private BigDecimal procurementVolume;

    /** 采购单价/销售单价 */
    @Excel(name = "采购单价")
    @ApiModelProperty(value = "采购单价")
    private BigDecimal procurementPrice;

    /** 参数指标 */
    @Excel(name = "参数指标")
    @ApiModelProperty(value = "参数指标")
    private String parameterIndicators;

    /** 订单开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单开始时间")
    private Date orderStartTime;

    /** 订单结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单结束时间")
    private Date orderEndTime;

    /** 结算比例 */
    @Excel(name = "结算比例")
    @ApiModelProperty(value = "结算比例")
    private String settlementProportion;

    /** 入库量/出库量 */
    @Excel(name = "入库量/出库量")
    @ApiModelProperty(value = "入库量/出库量")
    private BigDecimal scheduledReceipt;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 业务经理 */
    @Excel(name = "业务经理")
    @ApiModelProperty(value = "业务经理")
    private String serviceManager;

    /** 业务经理名称 */
    @ApiModelProperty(value = "业务经理名称")
    @TableField(exist = false)
    private String serviceManagerName;

    /** 运营专员 */
    @Excel(name = "运营专员")
    @ApiModelProperty(value = "运营专员")
    private String personCharge;

    /** 运营专员名称 */
    @ApiModelProperty(value = "运营专员名称")
    @TableField(exist = false)
    private String personChargeName;

    /** 业务员 */
    @Excel(name = "业务员")
    @ApiModelProperty(value = "业务员")
    private String salesman;

    /** 是否垫付运费 */
    @Excel(name = "是否垫付运费")
    @ApiModelProperty(value = "是否垫付运费")
    private Integer judgment;

    /** 发运计划 **/
    @ApiModelProperty(value = "发运计划")
    @TableField(exist = false)
    private List<BaShippingPlan> baShippingPlans;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 商品分类 **/
    @ApiModelProperty(value = "商品分类")
    @TableField(exist = false)
    private String goodsType;

    /** 排序 **/
    @ApiModelProperty(value = "排序")
    @TableField(exist = false)
    private String sorts;

    /** 完结 **/
    @ApiModelProperty(value = "完结")
    @TableField(exist = false)
    private Long completion;

    /** 本期回款 **/
    @ApiModelProperty(value = "本期回款")
    @TableField(exist = false)
    private BigDecimal currentCollection;

    /** 历史认领记录 **/
    @ApiModelProperty(value = "历史认领记录")
    @TableField(exist = false)
    private String historyId;

    /** 历史记录 **/
    @ApiModelProperty(value = "历史记录")
    @TableField(exist = false)
    private HomeDTO homeDTO;

    /** 合同启动阶段类型 */
    @Excel(name = "合同启动阶段类型")
    @ApiModelProperty(value = "合同启动阶段类型")
    private String startType;

    /** 发运量 **/
    @ApiModelProperty(value = "发运量")
    @TableField(exist = false)
    private BigDecimal checkCoalNum;

    /** 仓库ID **/
    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    /** 仓库名称 **/
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 库存总量（吨） */
    @ApiModelProperty(value = "库存总量（吨）")
    @TableField(exist = false)
    private BigDecimal inventoryTotal;

    /** 是否预付运费 */
    @Excel(name = "是否预付运费")
    @ApiModelProperty(value = "是否预付运费")
    private Integer prepaidFreight;

    /** 是否有托盘公司 */
    @Excel(name = "是否有托盘公司")
    @ApiModelProperty(value = "是否有托盘公司")
    private Integer existCompany;

    /** 放标公司 */
    @Excel(name = "放标公司")
    @ApiModelProperty(value = "放标公司")
    private String labelingCompanyId;

    /** 放标公司 */
    @Excel(name = "放标公司")
    @TableField(exist = false)
    private String labelingCompanyName;

    /** 放标公司 */
    @Excel(name = "放标公司")
    @TableField(exist = false)
    private String labelingType;

    /** 放标公司类型 */
    @Excel(name = "放标公司类型")
    @ApiModelProperty(value = "放标公司类型")
    private String labelingCompanyType;

    /** 投标委托人 */
    @Excel(name = "投标委托人")
    @ApiModelProperty(value = "投标委托人")
    private String bidPerson;

    /** 投标委托人姓名 */
    @ApiModelProperty(value = "投标委托人姓名")
    @TableField(exist = false)
    private String bidPersonName;

    /** 指标信息 */
    @Excel(name = "指标信息")
    @ApiModelProperty(value = "指标信息")
    private String indicatorInformation;

    /** 指标信息对象 */
    @ApiModelProperty(value = "指标信息对象")
    @TableField(exist = false)
    private BaEnterpriseRelevance baEnterpriseRelevance;

    /** 收费标准 **/
    @ApiModelProperty(value = "收费标准")
    @TableField(exist = false)
    private BaChargingStandards baChargingStandards;

    /** 合同启动投标信息运输对象 **/
    @ApiModelProperty(value = "合同启动投标信息运输对象")
    @TableField(exist = false)
    private List<BaBidTransport> baBidTransports;

    /** 提交意见 **/
    @Excel(name = "提交意见")
    @ApiModelProperty(value = "提交意见")
    private String submitComments;

    /** 立项对象 */
    @ApiModelProperty(value = "立项对象")
    @TableField(exist = false)
    private BaProject baProject;

    /** 合同启动编号 **/
    @Excel(name = "合同启动编号")
    @ApiModelProperty(value = "合同启动编号")
    private Integer startSerial;

    /** 序号 **/
    @Excel(name = "序号")
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /** 合同启动简称 **/
    @Excel(name = "合同启动简称")
    @ApiModelProperty(value = "合同启动简称")
    private String startShorter;

    /** 商品id **/
    @Excel(name = "商品id")
    @ApiModelProperty(value = "商品id")
    private String goodsId;

    /** 商品名称 **/
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** App商品名称 **/
    @ApiModelProperty(value = "App商品名称")
    @TableField(exist = false)
    private String appGoodsName;

    /** 履约开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "履约开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "履约开始时间")
    private Date performanceStartTime;

    /** 履约结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "履约结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "履约结束时间")
    private Date performanceEndTime;

    /** 是否垫付杂费 */
    @Excel(name = "是否垫付杂费")
    @ApiModelProperty(value = "是否垫付杂费")
    private Integer whetherExtras;

    /** 资金计划id **/
    @Excel(name = "资金计划id")
    @ApiModelProperty(value = "资金计划id")
    private String declareId;

    /**
     * 计划申报
     */
    @ApiModelProperty(value = "计划申报")
    @TableField(exist = false)
    private List<BaDeclare> baDeclares;

    /** 上游内部对接人 **/
    @Excel(name = "上游内部对接人")
    @ApiModelProperty(value = "上游内部对接人")
    private String internalContactPerson;

    /** 上游内部对接人名称 **/
    @ApiModelProperty(value = "上游内部对接人名称")
    @TableField(exist = false)
    private String internalContactPersonName;

    /** 上游外部对接人 **/
    @Excel(name = "上游外部对接人")
    @ApiModelProperty(value = "上游外部对接人")
    private String externalContactPerson;

    /** 上游对接人状态 **/
    @Excel(name = "上游对接人状态")
    @ApiModelProperty(value = "上游对接人状态")
    private String contactStatus;

    /** 下游内部对接人 **/
    @Excel(name = "下游内部对接人")
    @ApiModelProperty(value = "下游内部对接人")
    private String lowInternalContactPerson;

    /** 下游内部对接人名称 **/
    @ApiModelProperty(value = "下游内部对接人名称")
    @TableField(exist = false)
    private String lowInternalContactPersonName;

    /** 下游外部对接人 **/
    @Excel(name = "下游外部对接人")
    @ApiModelProperty(value = "下游外部对接人")
    private String lowExternalContactPerson;

    /** 下游对接状态 **/
    @Excel(name = "下游对接状态")
    @ApiModelProperty(value = "下游对接状态")
    private String lowContactStatus;

    /** 下游委托开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下游委托开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游委托开始时间")
    private Date lowStartTime;

    /** 下游委托结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下游委托结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游委托结束时间")
    private Date lowEndTime;

    /** 合同启动团队 **/
    @Excel(name = "合同启动团队")
    @ApiModelProperty(value = "合同启动团队")
    private String startTeam;

    /** 合同启动团队名称 **/
    @ApiModelProperty(value = "合同启动团队名称")
    @TableField(exist = false)
    private String startTeamName;

    /** 上游委托开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上游委托开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "上游委托开始时间")
    private Date upperStartTime;

    /** 上游委托结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "上游委托结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "上游委托结束时间")
    private Date upperEndTime;

    /** 招标类型 **/
    @Excel(name = "招标类型")
    @ApiModelProperty(value = "招标类型")
    private String bidType;

    /** 合同管理ID **/
    @Excel(name = "合同管理ID")
    @ApiModelProperty(value = "合同管理ID")
    private String contractId;

    /** 合同管理名称 **/
    @ApiModelProperty(value = "合同管理名称")
    @TableField(exist = false)
    private String contractName;

    /** 商品 **/
    @ApiModelProperty(value = "商品")
    @TableField(exist = false)
    private BaGoods baGoods;

    /** 子公司 **/
    @Excel(name = "子公司")
    @ApiModelProperty(value = "子公司")
    private String subsidiary;

    /** 子公司名称 **/
    @ApiModelProperty(value = "子公司名称")
    @TableField(exist = false)
    private String subsidiaryName;

    /** 问题合同启动标识 **/
    @ApiModelProperty(value = "问题合同启动标识")
    @TableField(exist = false)
    private boolean hasProblem;

    /** 需要标红的字段 */
    @ApiModelProperty(value = "需要标红的字段")
    @TableField(exist = false)
    private List<String> isChangeList;

    @ApiModelProperty(value = "合同额(万元)")
    @TableField(exist = false)
    private BigDecimal contractAmount;

    @ApiModelProperty(value = "已付款(万元)")
    @TableField(exist = false)
    private BigDecimal paymentAmount;

    @ApiModelProperty(value = "已收款(万元)")
    @TableField(exist = false)
    private BigDecimal collectionAmount;

    @ApiModelProperty(value = "已收票金额(万元)")
    @TableField(exist = false)
    private BigDecimal inVoIcePic;

    @ApiModelProperty(value = "累计计划发运量")
    @TableField(exist = false)
    private BigDecimal saleNum;

    @ApiModelProperty(value = "项目类型")
    @TableField(exist = false)
    private String projectType;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /**
     * 下游贸易商
     */
    @ApiModelProperty(value = "下游贸易商")
    private String downstreamTraders;

    /**
     * 下游贸易商名称
     */
    @ApiModelProperty(value = "下游贸易商")
    @TableField(exist = false)
    private String downstreamTradersName;

    /** 业务归属公司 */
    @Excel(name = "业务归属公司")
    @ApiModelProperty(value = "业务归属公司")
    private String belongCompanyId;

    /** 业务归属公司名称 */
    @Excel(name = "业务归属公司名称")
    @ApiModelProperty(value = "业务归属公司名称")
    @TableField(exist = false)
    private String belongCompanyName;

    /** 问题标识 */
    @Excel(name = "问题标识")
    @ApiModelProperty(value = "问题标识")
    private String problemMark;

    @TableField(exist = false)
    @ApiModelProperty(value = "多条件查询")
    private String keyWords;

    /**
     * 业务类型
     */
    @ApiModelProperty(value = "业务类型")
    @TableField(exist = false)
    private String businessTypes;

    /**
     * 订单ID
     */
    @ApiModelProperty(value = "订单ID")
    @TableField(exist = false)
    private String orderId;

    /** 合同管理已绑定 **/
    @ApiModelProperty(value = "合同管理已绑定")
    @TableField(exist = false)
    private String contractTypes;

    /**
     * 业务经理兼职部门
     */
    @ApiModelProperty(value = "业务经理兼职部门")
    private String partDeptId;

    /**
     * 事业部
     */
    @ApiModelProperty(value = "事业部")
    private String division;

    /**
     * 事业部名称
     */
    @ApiModelProperty(value = "事业部名称")
    @TableField(exist = false)
    private String divisionName;

    /**
     * 经理归属部门ID
     */
    @ApiModelProperty(value = "经理归属部门ID")
    private Long manageDeptId;


    /**
     * 上游公司
     */
    @ApiModelProperty(value = "上游公司")
    private String upstreamCompany;

    @ApiModelProperty(value = "上游公司名称")
    @TableField(exist = false)
    private String upstreamCompanyName;

    /**
     * 主体公司
     */
    @ApiModelProperty(value = "主体公司")
    private String subjectCompany;

    @ApiModelProperty(value = "主体公司名称")
    @TableField(exist = false)
    private String subjectCompanyName;

    /**
     * 下游公司
     */
    @ApiModelProperty(value = "下游公司")
    private String downstreamCompany;

    @ApiModelProperty(value = "下游公司名称")
    @TableField(exist = false)
    private String downstreamCompanyName;

    /**
     * 终端公司
     */
    @ApiModelProperty(value = "终端公司")
    private String terminalCompany;

    @ApiModelProperty(value = "终端公司名称")
    @TableField(exist = false)
    private String terminalCompanyName;

    /**
     * 简称
     */
    @ApiModelProperty(value = "简称")
    private String shortName;


    /** 计划销售时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划销售时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "计划销售时间")
    private Date plannedSalesTime;

    /** 合同信息 **/
    @ApiModelProperty(value = "合同信息")
    @TableField(exist = false)
    private List<BaContract> baContractList;

    /** 出库信息 **/
    @ApiModelProperty(value = "出库信息")
    @TableField(exist = false)
    private List<BaDepotHead> baDepotHeadList;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 上游
     */
    @ApiModelProperty(value = "上游")
    private String upstream;

    /**
     * 下游
     */
    @ApiModelProperty(value = "下游")
    private String downstream;

    /**
     * 所属事业部
     */
    @ApiModelProperty(value = "所属事业部")
    @TableField(exist = false)
    private String partDeptName;

    /**
     * 下游开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游开始时间")
    @TableField(exist = false)
    private Date downstreamStartTime;

    /**
     * 下游结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游结束时间")
    @TableField(exist = false)
    private Date downstreamEndTime;

    /**
     * 进场吨数
     */
    @ApiModelProperty(value = "进场吨数")
    @TableField(exist = false)
    private BigDecimal entryTonnage;

    /** 合同完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "合同完成时间")
    @TableField(exist = false)
    private Date transportDate;

    /** 下游结算数量 **/
    @ApiModelProperty(value = "下游结算数量")
    @TableField(exist = false)
    private BigDecimal downstreamSettlementNum;

    /** 下游结算金额(元) */
    @ApiModelProperty(value = "下游结算金额(元)")
    @TableField(exist = false)
    private BigDecimal downstreamSettlementAmount;

    /** 下游结算单价(元) */
    @ApiModelProperty(value = "下游结算单价(元)")
    @TableField(exist = false)
    private BigDecimal downstreamSettlementPrice;

    /** 开票时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开票时间")
    @TableField(exist = false)
    private Date invoiceTime;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;


    /** 变更标识 */
    @Excel(name = "变更标识")
    @ApiModelProperty(value = "变更标识")
    private String approveFlag;

    /** 变更人 */
    @Excel(name = "变更人")
    @ApiModelProperty(value = "变更人")
    private String changeBy;

    /** 变更时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "变更时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "变更时间")
    private Date changeTime;

    /** 父ID */
    @Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;

    /** 关联ID */
    @ApiModelProperty(value = "关联ID")
    private String relationId;


    /**
     * 变更人
     */
    @ApiModelProperty(value = "变更人")
    @TableField(exist = false)
    private SysUser changeUser;

    /** 占资账期上限 */
    @Excel(name = "占资账期上限")
    @ApiModelProperty(value = "占资账期上限")
    private Long accountUpperLimit;

    /** 指标内容 */
    @Excel(name = "指标内容")
    @ApiModelProperty(value = "指标内容")
    private String indicatorContent;

    /** 指标考核 */
    @Excel(name = "指标考核")
    @ApiModelProperty(value = "指标考核")
    private String indicatorAssessment;

    /** 数量考核 */
    @Excel(name = "数量考核")
    @ApiModelProperty(value = "数量考核")
    private String quantityAssessment;

    /**
     * 全局编号
     */
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /**
     * 创建人公司名称
     */
    @Excel(name = "创建人公司名称")
    @TableField(exist = false)
    private String comName;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

    /** 业务线简称 **/
    @Excel(name = "业务线简称")
    @ApiModelProperty(value = "业务线简称")
    private String businessShortName;
}
