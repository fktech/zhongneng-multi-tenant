package com.business.system.domain.dto;

import com.business.system.domain.BaProject;
import com.business.system.domain.BaProjectBeforehand;
import com.business.system.domain.OaMaintenanceExpansion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: OA业务拓展维护与流程实例关联关系审批编辑对象
 * @date: 2023/12/1
 */
@Data
@ApiModel(value = "OaMaintenanceExpansionInstanceRelatedEditDTO对象", description = "OA业务拓展维护与流程实例关联关系审批编辑对象")
public class OaMaintenanceExpansionInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private OaMaintenanceExpansion oaMaintenanceExpansion;
}
