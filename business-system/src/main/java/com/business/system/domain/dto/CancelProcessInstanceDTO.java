package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @version 1.0
 * @Author: js
 * @Description: 撤回流程实例前端控制器接口
 * @Date: 2022/12/7 17:03
 */
@Data
@ApiModel(value = "CancelProcessInstanceDTO对象", description = "撤回流程实例对象")
public class CancelProcessInstanceDTO implements Serializable {

    private static final long serialVersionUID = 2864377063681486682L;

    @ApiModelProperty("流程实例ID")
    private List<String> processInstanceIds;

    @ApiModelProperty("撤回用户ID")
    private String userId;

    @ApiModelProperty("撤回用户名称")
    private String userName;

    @ApiModelProperty("取消原因")
    private String reason;
}
