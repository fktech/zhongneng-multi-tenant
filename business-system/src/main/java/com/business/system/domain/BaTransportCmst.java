package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 中储智运对象 ba_transport_cmst
 *
 * @author single
 * @date 2023-08-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "中储智运对象",description = "")
@TableName("ba_transport_cmst")
public class BaTransportCmst extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单类型：0 抢单,1 竞价 */
    @Excel(name = "订单类型：0 抢单,1 竞价")
    @ApiModelProperty(value = "订单类型：0 抢单,1 竞价")
    private String orderModel;

    /** 自定义编号/标签 */
    @Excel(name = "自定义编号/标签")
    @ApiModelProperty(value = "自定义编号/标签")
    private String selfComment;

    /** 紧急联系人 */
    @Excel(name = "紧急联系人")
    @ApiModelProperty(value = "紧急联系人")
    private String contactName;

    /** 紧急联系电话 */
    @Excel(name = "紧急联系电话")
    @ApiModelProperty(value = "紧急联系电话")
    private String contactPhone;

    /** 车型要求: 平板、高栏车、低栏车、高低平板、厢式车、罐式车、普通、集装车、封闭、集装箱、自卸、轴线板、满轮车、高低高板、轴拉板、簸箕板、叶片车、开顶厢、其他、半挂车、敞篷车、中栏车、飞翼车、大载车、封闭货车、不限 */
    @Excel(name = "车型要求: 平板、高栏车、低栏车、高低平板、厢式车、罐式车、普通、集装车、封闭、集装箱、自卸、轴线板、满轮车、高低高板、轴拉板、簸箕板、叶片车、开顶厢、其他、半挂车、敞篷车、中栏车、飞翼车、大载车、封闭货车、不限")
    @ApiModelProperty(value = "车型要求: 平板、高栏车、低栏车、高低平板、厢式车、罐式车、普通、集装车、封闭、集装箱、自卸、轴线板、满轮车、高低高板、轴拉板、簸箕板、叶片车、开顶厢、其他、半挂车、敞篷车、中栏车、飞翼车、大载车、封闭货车、不限")
    private String vehicleType;

    /** 车长要求，单位米（参数不带单位），建议3.0-20.0，可传不限 */
    @Excel(name = "车长要求，单位米", readConverterExp = "参=数不带单位")
    @ApiModelProperty(value = "车长要求，单位米")
    private String  vehicleLength;

    /** 是否加急：否, 是 */
    @Excel(name = "是否加急：否, 是")
    @ApiModelProperty(value = "是否加急：否, 是")
    private String urgentFlag;

    /** 装货开始时间 日期格式:yyyy-mm-dd hh:mm (在当前时间至少1小时之后) */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "装货开始时间 日期格式:yyyy-mm-dd hh:mm (在当前时间至少1小时之后)", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "装货开始时间 日期格式:yyyy-mm-dd hh:mm (在当前时间至少1小时之后)")
    private Date despatchStart;

    /** 信息有效期大于当时时间，格式为yyyy-mm-dd hh:mm */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "信息有效期大于当时时间，格式为yyyy-mm-dd hh:mm", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "信息有效期大于当时时间，格式为yyyy-mm-dd hh:mm")
    private Date validTime;

    /** 单价货值 */
    @Excel(name = "单价货值")
    @ApiModelProperty(value = "单价货值")
    private String cargoUnitMoney;

    /** 运费单价(货主含税价)与承运方预估到手价，只能二选一 */
    @Excel(name = "运费单价(货主含税价)与承运方预估到手价，只能二选一")
    @ApiModelProperty(value = "运费单价(货主含税价)与承运方预估到手价，只能二选一")
    private BigDecimal  unitAmount;

    /** 承运方预估到手价与运费单价，只能二选一 */
    @Excel(name = "承运方预估到手价与运费单价，只能二选一")
    @ApiModelProperty(value = "承运方预估到手价与运费单价，只能二选一")
    private String carrierPredictMoney;

    /** 装卸货要求 */
    @Excel(name = "装卸货要求")
    @ApiModelProperty(value = "装卸货要求")
    private String prompt;

    /** 是否押回单：0 否,1 是 */
    @Excel(name = "是否押回单：0 否,1 是")
    @ApiModelProperty(value = "是否押回单：0 否,1 是")
    private String receiptFlag;

    /** 是否购买保险 1 同意在平台购买货物保障服务 2 要求承运方购买货物保障服务 3 我已阅读知晓并同意《中储智运平台交易规则——货物保障服务》中的约定，不在平台购买货物保障服务 */
    @Excel(name = "是否购买保险 1 同意在平台购买货物保障服务 2 要求承运方购买货物保障服务 3 我已阅读知晓并同意《中储智运平台交易规则——货物保障服务》中的约定，不在平台购买货物保障服务")
    @ApiModelProperty(value = "是否购买保险 1 同意在平台购买货物保障服务 2 要求承运方购买货物保障服务 3 我已阅读知晓并同意《中储智运平台交易规则——货物保障服务》中的约定，不在平台购买货物保障服务")
    private String policyFlag;

    /** 是否包含油气品：0 否,1 是 */
    @Excel(name = "是否包含油气品：0 否,1 是")
    @ApiModelProperty(value = "是否包含油气品：0 否,1 是")
    private String supportSdOilCardFlag;

    /** 油品比例（是否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例），是否包含油气品为否时，不填），范围是1-40 */
    @Excel(name = "油品比例", readConverterExp = "是=否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例")
    @ApiModelProperty(value = "油品比例")
    private String oilCardRatio;

    /** 汽品比例（是否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例），是否包含油气品为否时，不填），范围是1-40 */
    @Excel(name = "汽品比例", readConverterExp = "是=否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例")
    @ApiModelProperty(value = "汽品比例")
    private String gasPercent;

    /** 油品固定额度（是否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例），是否包含油气品为否时，不填） */
    @Excel(name = "油品固定额度", readConverterExp = "是=否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例")
    @ApiModelProperty(value = "油品固定额度")
    private Long oilFixedCredit;

    /** 气品固定额度（是否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例），是否包含油气品为否时，不填） */
    @Excel(name = "气品固定额度", readConverterExp = "是=否包含油气品为是时，同一种类比例与固定额度互斥，不同种类类型要一致（要么油汽品都是固定额度要么都是比例")
    @ApiModelProperty(value = "气品固定额度")
    private String gasFixedCredit;

    /** 是否余量预警 1是 0 否 */
    @Excel(name = "是否余量预警 1是 0 否")
    @ApiModelProperty(value = "是否余量预警 1是 0 否")
    private String needWarningFlag;

    /** 余量预警吨位,剩余多少吨位预警 */
    @Excel(name = "余量预警吨位,剩余多少吨位预警")
    @ApiModelProperty(value = "余量预警吨位,剩余多少吨位预警")
    private BigDecimal remainAmountWarning;

    /** 自动成交规则名称,议价模式下可填 */
    @Excel(name = "自动成交规则名称,议价模式下可填")
    @ApiModelProperty(value = "自动成交规则名称,议价模式下可填")
    private String autoDealRuleName;

    /** 亏涨吨扣款配置名称 */
    @Excel(name = "亏涨吨扣款配置名称")
    @ApiModelProperty(value = "亏涨吨扣款配置名称")
    private String lossincTonRuleName;

    /** 结算依据：1 按发货磅单结算,2 按收货磅单结算,3 按收发货磅单中较小吨位值结算 */
    @Excel(name = "结算依据：1 按发货磅单结算,2 按收货磅单结算,3 按收发货磅单中较小吨位值结算")
    @ApiModelProperty(value = "结算依据：1 按发货磅单结算,2 按收货磅单结算,3 按收发货磅单中较小吨位值结算")
    private String settleBasis;

    /** 摘单咨询电话 */
    @Excel(name = "摘单咨询电话")
    @ApiModelProperty(value = "摘单咨询电话")
    private String pickOrderAdvisoryPhon;

    /** 结算咨询电话 */
    @Excel(name = "结算咨询电话")
    @ApiModelProperty(value = "结算咨询电话")
    private String settlementAdvisoryPhone;

    /** （竞价单）必填 报价结束时间 日期格式:yyyy-mm-dd hh:mm */
    @Excel(name = "", readConverterExp = "竞=价单")
    @ApiModelProperty(value = "")
    private Date quotationEndTime;

    /** 必填 是否指定单：0 否，1 是 */
    @Excel(name = "必填 是否指定单：0 否，1 是")
    @ApiModelProperty(value = "必填 是否指定单：0 否，1 是")
    private String specifyFlag;

    /** 货物信息 */
    @Excel(name = "货物信息")
    @ApiModelProperty(value = "货物信息")
    private String cargoInfo;

    /** 货物名称 */
    @Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String cargoName;

    /** 规格型号 */
    @Excel(name = "规格型号")
    @ApiModelProperty(value = "规格型号")
    private String cargoVersion;

    /** 货物类别：1：重货，2：泡货 */
    @Excel(name = "货物类别：1：重货，2：泡货")
    @ApiModelProperty(value = "货物类别：1：重货，2：泡货")
    private String cargoCategory;

    /** 货物重量或体积 */
    @Excel(name = "货物重量或体积")
    @ApiModelProperty(value = "货物重量或体积")
    private BigDecimal weight;

    /** 规格-长 */
    @Excel(name = "规格-长")
    @ApiModelProperty(value = "规格-长")
    private BigDecimal cargoLength;

    /** 规格-宽 */
    @Excel(name = "规格-宽")
    @ApiModelProperty(value = "规格-宽")
    private BigDecimal cargoWidth;

    /** 规格-高 */
    @Excel(name = "规格-高")
    @ApiModelProperty(value = "规格-高")
    private BigDecimal cargoHeight;

    /** 包装类型：铁箱、卷、捆包、钢瓶、电缆线盘、桶、无、木箱、托盘、袋装、纸箱 */
    @Excel(name = "包装类型：铁箱、卷、捆包、钢瓶、电缆线盘、桶、无、木箱、托盘、袋装、纸箱")
    @ApiModelProperty(value = "包装类型：铁箱、卷、捆包、钢瓶、电缆线盘、桶、无、木箱、托盘、袋装、纸箱")
    private String pack;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    @ApiModelProperty(value = "仓库名称")
    private String warehouseName;

    /** 仓库位置 */
    @Excel(name = "仓库位置")
    @ApiModelProperty(value = "仓库位置")
    private String warehouseLocation;

    /** 收发货信息 */
    @Excel(name = "收发货信息")
    @ApiModelProperty(value = "收发货信息")
    private String orderAddressInfo;

    /** 发货单位名称 */
    @Excel(name = "发货单位名称")
    @ApiModelProperty(value = "发货单位名称")
    private String despatchCompanyName;

    /** 发货人姓名 */
    @Excel(name = "发货人姓名")
    @ApiModelProperty(value = "发货人姓名")
    private String despatchName;

    /** 发货人联系电话 */
    @Excel(name = "发货人联系电话")
    @ApiModelProperty(value = "发货人联系电话")
    private String despatchMobile;

    /** 发货人备用电话 */
    @Excel(name = "发货人备用电话")
    @ApiModelProperty(value = "发货人备用电话")
    private String despatchBackupMobile;

    /** 启运地省 */
    @Excel(name = "启运地省")
    @ApiModelProperty(value = "启运地省")
    private String despatchPro;

    /** 启运地市 */
    @Excel(name = "启运地市")
    @ApiModelProperty(value = "启运地市")
    private String despatchCity;

    /** 启运地区 */
    @Excel(name = "启运地区")
    @ApiModelProperty(value = "启运地区")
    private String despatchDis;

    /** 启运地详细地址 */
    @Excel(name = "启运地详细地址")
    @ApiModelProperty(value = "启运地详细地址")
    private String despatchPlace;

    /** 收货单位名称 */
    @Excel(name = "收货单位名称")
    @ApiModelProperty(value = "收货单位名称")
    private String deliverCompanyName;

    /** 收货人名称 */
    @Excel(name = "收货人名称")
    @ApiModelProperty(value = "收货人名称")
    private String deliverName;

    /** 收货人联系电话 */
    @Excel(name = "收货人联系电话")
    @ApiModelProperty(value = "收货人联系电话")
    private String deliverMobile;

    /** 收货人备用联系电话 */
    @Excel(name = "收货人备用联系电话")
    @ApiModelProperty(value = "收货人备用联系电话")
    private String deliverBackupMobile;

    /** 目的地省 */
    @Excel(name = "目的地省")
    @ApiModelProperty(value = "目的地省")
    private String deliverPro;

    /** 目的地市 */
    @Excel(name = "目的地市")
    @ApiModelProperty(value = "目的地市")
    private String deliverCity;

    /** 目的地区 */
    @Excel(name = "目的地区")
    @ApiModelProperty(value = "目的地区")
    private String deliverDis;

    /** 目的地详细地址 */
    @Excel(name = "目的地详细地址")
    @ApiModelProperty(value = "目的地详细地址")
    private String deliverPlace;

    /** 押回单信息，是否押回单选择是时必填，否则传空 */
    @Excel(name = "押回单信息，是否押回单选择是时必填，否则传空")
    @ApiModelProperty(value = "押回单信息，是否押回单选择是时必填，否则传空")
    private String orderReceiptInfo;

    /** 押回单标签 是否押回单选择是时必填 */
    @Excel(name = "押回单标签 是否押回单选择是时必填")
    @ApiModelProperty(value = "押回单标签 是否押回单选择是时必填")
    private String receiptLabel;

    /** 回单金额 是否押回单选择是时可填 */
    @Excel(name = "回单金额 是否押回单选择是时可填")
    @ApiModelProperty(value = "回单金额 是否押回单选择是时可填")
    private Long receiptMoney;

    /** 承运人手机号 是否指定单选择是时必填 */
    @Excel(name = "承运人手机号 是否指定单选择是时必填")
    @ApiModelProperty(value = "承运人手机号 是否指定单选择是时必填")
    private String carrierMobile;

    /** 加盟运力手机号 */
    @Excel(name = "加盟运力手机号")
    @ApiModelProperty(value = "加盟运力手机号")
    private String leagueMobile;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 二维码 */
    @Excel(name = "二维码")
    @ApiModelProperty(value = "二维码")
    private String qrCodeUrl;

    /** 母单号 **/
    @Excel(name = "母单号")
    @ApiModelProperty(value = "母单号")
    private String yardld;

    /** 关联id **/
    @Excel(name = "关联id")
    @ApiModelProperty(value = "关联id")
    private String relationId;

    /** 运输车数 */
    /*@Excel(name = "运输车数")
    @ApiModelProperty(value = "运输车数")
    private Long transNum;*/

    /** 实际发运量 */
    @ApiModelProperty(value = "实际发运量")
    @TableField(exist = false)
    private BigDecimal saleNum;

    /** 车数 **/
    @ApiModelProperty(value = "车数")
    @TableField(exist = false)
    private Integer carNum;

    /** 启动名称 **/
    @ApiModelProperty(value = "启动名称")
    @TableField(exist = false)
    private String startName;

    /** 项目名称 **/
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 货源路线 **/
    @ApiModelProperty(value = "货源路线")
    private String sourceName;

    /** 商品名称 **/
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** 货主名称 **/
    @ApiModelProperty(value = "货主名称")
    @TableField(exist = false)
    private String consignorUserName;

    /** 运单 **/
    @ApiModelProperty(value = "运单")
    @TableField(exist = false)
    private List<BaShippingOrderCmst> baShippingOrderCmsts;

    /** 货源类型 **/
    @ApiModelProperty(value = "货源类型")
    private String type;

    /** 出库 **/
    @ApiModelProperty(value = "出库")
    private String outbound;

    /** 入库 **/
    @ApiModelProperty(value = "入库")
    private String warehousing;

    /**
     * 仓储立项
     */
    @ApiModelProperty(value = "仓储立项")
    private String projectId;

    @ApiModelProperty(value = "仓储立项名称")
    @TableField(exist = false)
    private String storageName;
    /**
     * 采购申请
     */
    @ApiModelProperty(value = "采购申请")
    private String procureId;

    @ApiModelProperty(value = "采购申请名称")
    @TableField(exist = false)
    private String procureName;

    /**
     * 销售申请
     */
    @ApiModelProperty(value = "销售申请")
    private String saleId;

    @ApiModelProperty(value = "销售申请名称")
    @TableField(exist = false)
    private String saleName;

    /**
     * 取样出入库
     */
    @ApiModelProperty(value = "取样出入库")
    private String sampleType;

}
