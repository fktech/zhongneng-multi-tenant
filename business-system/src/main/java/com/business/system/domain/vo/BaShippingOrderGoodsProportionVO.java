package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 统计拉运货物占比
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaShippingOrderGoodsProportionVO对象",description="统计拉运货物占比")
public class BaShippingOrderGoodsProportionVO
{
    /** 货物名称 */
    @ApiModelProperty(value = "货物名称")
    private String coaltypeName;

    /** 货物重量 */
    @ApiModelProperty(value = "货物重量")
    private BigDecimal number;

    /** 占比 */
    @ApiModelProperty(value = "占比")
    private String percent;
}
