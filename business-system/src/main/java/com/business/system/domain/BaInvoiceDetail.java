package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 发票信息详情对象 ba_invoice_detail
 *
 * @author single
 * @date 2023-03-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "发票信息详情对象",description = "")
@TableName("ba_invoice_detail")
public class BaInvoiceDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 发票ID */
    //@Excel(name = "发票ID")
    @ApiModelProperty(value = "发票ID")
    private String invoiceId;

    /** 开票日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开票日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开票日期")
    private Date invoiceTime;

    /** 发票号 */
    @Excel(name = "发票号")
    @ApiModelProperty(value = "发票号")
    private String invoiceNum;

    /** 数量 */
    @Excel(name = "数量")
    @ApiModelProperty(value = "数量")
    private BigDecimal account;

    /** 金额 */
    @Excel(name = "金额")
    @ApiModelProperty(value = "金额")
    private BigDecimal invoicedAmount;

    /** 税率 */
    @Excel(name = "税率")
    @ApiModelProperty(value = "税率")
    private BigDecimal taxRate;

    /** 税额 */
    @Excel(name = "税额")
    @ApiModelProperty(value = "税额")
    private BigDecimal taxAmount;

    /** 税价合计 */
    @Excel(name = "税价合计")
    @ApiModelProperty(value = "税价合计")
    private BigDecimal taxPriceTotal;

    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;



}
