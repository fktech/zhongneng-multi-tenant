package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 出入库记录对象 ba_depot_head
 *
 * @author single
 * @date 2023-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "出入库记录对象",description = "")
@TableName("ba_depot_head")
public class BaDepotHead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 类型（出库/入库） */
    @Excel(name = "类型", readConverterExp = "出=库/入库")
    @ApiModelProperty(value = "类型")
    private Long type;

    /** 出入库分类 */
    @Excel(name = "出入库分类")
    @ApiModelProperty(value = "出入库分类")
    private String subType;

    /** 初始票据号 */
    @Excel(name = "初始票据号")
    @ApiModelProperty(value = "初始票据号")
    private String defaultNumber;

    /** 票据号 */
    @Excel(name = "票据号")
    @ApiModelProperty(value = "票据号")
    private String number;

    /** 出入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出入库时间")
    private Date operTime;

    /** 供应商 */
    @Excel(name = "供应商")
    @ApiModelProperty(value = "供应商")
    private String supplierId;

    @TableField(exist = false)
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 采购订单编号 */
    @Excel(name = "采购订单编号")
    @ApiModelProperty(value = "采购订单编号")
    private String orderId;

    @TableField(exist = false)
    @ApiModelProperty(value = "订单名称")
    private String orderName;

    @TableField(exist = false)
    @ApiModelProperty(value = "销售订单合同编号")
    private String orderNum;

    /** 批次ID */
    @Excel(name = "批次ID")
    @ApiModelProperty(value = "批次ID")
    private String batch;

    @TableField(exist = false)
    @ApiModelProperty(value = "批次号")
    private String batchName;

    /** 商品id */
    @Excel(name = "商品id")
    @ApiModelProperty(value = "商品id")
    private String materialId;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品名称")
    private String materialName;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品编码")
    private String materialCode;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depotName;

    /** 附件名称 */
    @Excel(name = "附件名称")
    @ApiModelProperty(value = "附件名称")
    private String fileName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String status;

    /** 关联加工出库 */
    @Excel(name = "关联加工出库")
    @ApiModelProperty(value = "关联加工出库")
    private String linkNumber;

    /*@ApiModelProperty(value = "关联加工出库")
    @TableField(exist = false)
    private List<BaDepotHead> baDepotHeads;*/

    /** 加工类型 */
    @Excel(name = "加工类型")
    @ApiModelProperty(value = "加工类型")
    private String machiningType;

    /** 损耗阈值 */
    @Excel(name = "损耗阈值")
    @ApiModelProperty(value = "损耗阈值")
    private String threshold;

    /** 预入库日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "预入库日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预入库日期")
    private Date time;

    /** 条形码(多个条形码) */
    @Excel(name = "条形码(多个条形码)")
    @ApiModelProperty(value = "条形码(多个条形码)")
    private String barCode;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String types;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户名称")
    private String userName;

    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    @TableField(exist = false)
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 出入库业务状态 */
    @Excel(name = "出入库业务状态")
    @ApiModelProperty(value = "出入库业务状态")
    private String headStatus;

    /** 总量 **/
    @Excel(name = "总量")
    @ApiModelProperty(value = "总量")
    private BigDecimal totals;

    /** 合同启动id **/
    @Excel(name = "合同启动id")
    @ApiModelProperty(value = "合同启动id")
    private String startId;

    /** 运输类型 **/
    @Excel(name = "运输类型")
    @ApiModelProperty(value = "运输类型")
    private String transportType;

    /**
     * 合同启动名称
     */
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /**
     * 立项编号
     */
    @ApiModelProperty(value = "立项编号")
    private String projectId;

    /** 发起时间 **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发起时间")
    private Date initiateTime;

    /** 计划量 **/
    @Excel(name = "计划量")
    @ApiModelProperty(value = "计划量")
    private BigDecimal planMeasure;

    /** 采购单价 **/
    @Excel(name = "采购单价")
    @ApiModelProperty(value = "采购单价")
    private BigDecimal procurePrice;

    /** 验收 **/
    @ApiModelProperty(value = "质检信息")
    @TableField(exist = false)
    private BaCheck baCheck;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 质检信息 **/
    @TableField(exist = false)
    private BaCheck check;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 出入库标识
     */
    @TableField(exist = false)
    private String logo;

    /**
     * 终端企业名称
     */
    @TableField(exist = false)
    private String enterpriseName;

    /**
     * 终端企业编号
     */
    @TableField(exist = false)
    private String enterpriseId;

    /**
     * 平均热值
     */
    @TableField(exist = false)
    private BigDecimal averageCalorific;

    /**
     * 商品信息
     */
    /*@TableField(exist = false)
    private BaGoods baGoods;*/

    /**
     * 出入库副表
     */
    @ApiModelProperty(value = "出入库副表")
    @TableField(exist = false)
    private List<BaDepotItem> baDepotItem;

    /** 运单id **/
    @Excel(name = "运单id")
    @ApiModelProperty(value = "运单id")
    private String waybillId;

    /** 运单对象 **/
    @ApiModelProperty(value = "运单对象")
    @TableField(exist = false)
    private List<BaShippingOrder> baShippingOrders;

    /**
     * 采购单ID
     */
    @ApiModelProperty(value = "采购单ID")
    private String purchaseId;

    /**
     * 销售订单名称
     */
    @ApiModelProperty(value = "销售订单名称")
    @TableField(exist = false)
    private String purchaseName;

    @ApiModelProperty(value = "销售订单合同编号")
    @TableField(exist = false)
    private String purchaseNum;

    /**
     * 合同编号
     */
    @ApiModelProperty(value = "合同编号")
    @TableField(exist = false)
    private String contractNum;

    /**
     * 采购单对象
     */
    @ApiModelProperty(value = "采购单对象")
    @TableField(exist = false)
    private BaPurchaseOrder baPurchaseOrder;

    /**
     * 库位ID
     */
    @ApiModelProperty(value = "库位ID")
    private String locationId;

    /**
     * 入库清单
     */
    @ApiModelProperty(value = "入库清单")
    @TableField(exist = false)
    private List<BaDepotInventory> baDepotInventories;

    /**
     * 采购计划ID
     */
    @ApiModelProperty(value = "采购计划ID")
    @TableField(exist = false)
    private String planId;

    /**
     * 商品ID
     */
    @ApiModelProperty(value = "商品ID")
    @TableField(exist = false)
    private String goodsId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /**
     * 商品类别
     */
    @ApiModelProperty(value = "商品类别")
    @TableField(exist = false)
    private String goodsType;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsTitle;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /**
     * 货位名称
     */
    @ApiModelProperty(value = "货位名称")
    @TableField(exist = false)
    private String locationName;

    /** 货值 **/
    @Excel(name = "货值")
    @ApiModelProperty(value = "货值")
    private BigDecimal cargoValue;

    /** 库存总量（吨） */
    @Excel(name = "库存总量（吨）")
    @ApiModelProperty(value = "库存总量（吨）")
    private BigDecimal inventoryTotal;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 货源单编号 */
    @Excel(name = "货源单编号")
    @ApiModelProperty(value = "货源单编号")
    private String supplierNo;

    /** 货源发布人名称 */
    @ApiModelProperty(value = "货源发布人名称")
    @TableField(exist = false)
    private String supplierUserName;

    /** 货源平台 */
    @Excel(name = "货源平台")
    @ApiModelProperty(value = "货源平台")
    private String supplierPlatform;

    /** 采购计划名称 */
    @ApiModelProperty(value = "采购计划名称")
    @TableField(exist = false)
    private String planName;

    /** 车数 */
    @ApiModelProperty(value = "车数")
    @TableField(exist = false)
    private Integer cars;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /**
     * 创建人公司名称
     */
    @Excel(name = "创建人公司名称")
    @TableField(exist = false)
    private String comName;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

}
