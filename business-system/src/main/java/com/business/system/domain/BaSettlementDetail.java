package com.business.system.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 结算明细对象 ba_settlement_detail
 *
 * @author ljb
 * @date 2023-02-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "结算明细对象",description = "")
@TableName("ba_settlement_detail")
public class BaSettlementDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 结算项 */
    @Excel(name = "结算项")
    @ApiModelProperty(value = "结算项")
    private String settlementType;

    /** 金额 */
    @Excel(name = "金额")
    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    /** 结算ID */
    @Excel(name = "结算ID")
    @ApiModelProperty(value = "结算ID")
    private String settlementId;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 订单编号 */
    @Excel(name = "订单编号")
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 审批流程id */
    @Excel(name = "收付款编号")
    @ApiModelProperty(value = "收付款编号")
    private String relationId;



}
