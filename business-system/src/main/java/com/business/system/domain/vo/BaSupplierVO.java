package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.BaBank;
import com.business.system.domain.BaBillingInformation;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 供应商对象 ba_supplier
 *
 * @author ljb
 * @date 2022-11-28
 */
@Data
@ApiModel(value="BaSupplierVO对象",description="供应商对象")
public class BaSupplierVO
{
    /** 主键 */
    private String id;

    /** 公司名称 */
    @ApiModelProperty(value = "公司名称")
    private String name;

    /** 省 */
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 */
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 */
    @ApiModelProperty(value = "区")
    private String area;

    /** 地址 */
    @ApiModelProperty(value = "地址")
    private String address;

    /** 联系人 */
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /** 联系人电话 */
    @ApiModelProperty(value = "联系人电话")
    private String mobilePhone;

    /** 成立日期 */
    @ApiModelProperty(value = "成立日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tenableDate;

    /** 注册资本（万元） */
    @ApiModelProperty(value = "注册资本")
    private BigDecimal registeredCapital;

    /** 发运规模（万吨/年） */
    @ApiModelProperty(value = "发运规模")
    private BigDecimal shippingScale;

    /** 发票面额 */
    @ApiModelProperty(value = "发票面额")
    private BigDecimal invoiceValue;

    /** 开户行 */
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 开户名 */
    @ApiModelProperty(value = "开户名")
    private String bankOwner;

    /** 账户号 */
    @Excel(name = "账户号")
    @ApiModelProperty(value = "账户号")
    private String accountNumber;

    /** 纳税人识别号 */
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 法人身份证号 */
    @ApiModelProperty(value = "法人身份证号")
    private String corporateCapacity;

    /** 营业执照 */
    @ApiModelProperty(value = "营业执照")
    private String file;

    /** 法人身份证 */
    @ApiModelProperty(value = "法人身份证")
    private String corporateCapacityIdCard;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 收款开户行 */
    @ApiModelProperty(value = "收款开户行")
    private String collectionBankName;

    /** 收款开户名 */
    @ApiModelProperty(value = "收款开户名")
    private String collectionAccountName;

    /** 收款账号 */
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 银行联号 */
    @ApiModelProperty(value = "银行联号")
    private String interBankNo;

    /** 用户id */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 审批流程Id **/
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 租户Id **/
    @ApiModelProperty(value = "审批流程Id")
    private String tenantId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @TableField(exist = false)
    private String userName;


    /** 收款账户 */
    @TableField(exist = false)
    private List<BaBank> baBankList ;

    /** 基本账户 **/
    @TableField(exist = false)
    private BaBank baBank;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 公司简称 **/
    @ApiModelProperty(value = "公司简称")
    private String companyAbbreviation;

    /** 法人名称 **/
    @ApiModelProperty(value = "法人名称")
    private String corporateName;

    /** 法人电话 **/
    @ApiModelProperty(value = "法人电话")
    private String corporatePhone;

    /** 附件 **/
    @ApiModelProperty(value = "附件")
    private String appendix;

    /** 实控人 **/
    @ApiModelProperty(value = "实控人")
    private String leader;

    /** 实控人电话 **/
    @ApiModelProperty(value = "实控人电话")
    private String leaderPhone;

    /**
     * 开票信息
     */
    @ApiModelProperty(value = "开票信息")
    @TableField(exist = false)
    private BaBillingInformation billingInformation;

    /** 搜索值 */
    @TableField(exist = false)
    private String searchValue;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @TableField(exist = false)
    private String remark;

    /** 请求参数 */
    @TableField(exist = false)
    private Map<String, Object> params;

}
