package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "网络货运统计地图DTO",description = "网络货运统计地图对象")
public class AtlasDataDTO {

    /** 收货经度 */
    @ApiModelProperty(value = "收货经度")
    private String depJd;

    /** 收货纬度 */
    @ApiModelProperty(value = "收货纬度")
    private String depWd;

    /** 发货经度 */
    @ApiModelProperty(value = "发货经度")
    private String desJd;

    /** 发货纬度 */
    @ApiModelProperty(value = "发货纬度")
    private String desWd;


    /** 发货市 */
    @ApiModelProperty(value = "发货市")
    private String fhCity;

    /** 收货市 */
    @ApiModelProperty(value = "收货市")
    private String shCity;

}
