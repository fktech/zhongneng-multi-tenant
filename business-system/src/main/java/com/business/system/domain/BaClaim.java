package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.system.domain.dto.BaOrderDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 认领对象 ba_claim
 *
 * @author ljb
 * @date 2023-01-31
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "认领对象",description = "")
@TableName("ba_claim")
public class BaClaim extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 已认领 */
    //@Excel(name = "已认领")
    @ApiModelProperty(value = "已认领")
    private BigDecimal claimed;

    /** 待认领 */
    //@Excel(name = "待认领")
    @ApiModelProperty(value = "待认领")
    private BigDecimal beClaimed;


    /** 关联Id */
    //@Excel(name = "关联Id")
    @ApiModelProperty(value = "关联Id")
    private String relationId;

    /** 订单编号 */
    //@Excel(name = "订单编号")
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 订单对象集合
     */
    @ApiModelProperty(value = "订单对象集合")
    @TableField(exist = false)
    private List<BaOrderDTO> baOrderDTOs;

    /**
     * 合同发起对象集合
     */
    @ApiModelProperty(value = "合同发起对象集合")
    @TableField(exist = false)
    private List<BaContractStart> baContractStartList;

    /**
     * 本期回款总量
     */
    @ApiModelProperty(value = "本期回款总量")
    @TableField(exist = false)
    private BigDecimal thisClaimAll;


    /**
     * 全局编号
     */
    @Excel(name = "全局编号")
    @ApiModelProperty(value = "全局编号")
    @TableField(exist = false)
    private String globalNumber;

    /**
     * 付款单位
     */
    @Excel(name = "付款单位")
    @ApiModelProperty(value = "付款单位")
    @TableField(exist = false)
    private String paymentCompany;

    /**
     * 收款单位
     */
    @ApiModelProperty(value = "收款单位")
    @TableField(exist = false)
    private String collectionCompany;

    /**
     * 收款日期
     */
    @Excel(name = "收款日期", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "收款日期")
    @TableField(exist = false)
    private Date collectionDate;

    /**
     * 收款金额
     */
    @Excel(name = "收款金额")
    @ApiModelProperty(value = "收款金额")
    @TableField(exist = false)
    private BigDecimal applyCollectionAmount;

    /** 本次认领 */
    @Excel(name = "认领金额")
    @ApiModelProperty(value = "本次认领")
    private BigDecimal thisClaim;

    /** 本合同认领金额 */
    @Excel(name = "本合同认领金额")
    @ApiModelProperty(value = "本合同认领金额")
    @TableField(exist = false)
    private BigDecimal thisClaimHistory;

    /**
     * 认领状态
     */
    @Excel(name = "认领状态", readConverterExp = "1=待认领,2=已认领")
    @ApiModelProperty(value = "认领状态")
    private String claimState;

    /**
     * 认领人
     */
    @Excel(name = "认领人")
    @ApiModelProperty(value = "认领人")
    @TableField(exist = false)
    private String userName;

    /**
     * 认领时间
     */
    @Excel(name = "认领时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "认领时间")
    @TableField(exist = false)
    private Date claimTime;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 审批状态 **/
    @ApiModelProperty(value = "审批状态")
    @TableField(exist = false)
    private String state;


    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 退回
     */
    @ApiModelProperty(value = "退回")
    @TableField(exist = false)
    private String goBack;

}
