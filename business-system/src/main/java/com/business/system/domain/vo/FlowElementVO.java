package com.business.system.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 立项管理对象 ba_project
 *
 * @author ljb
 * @date 2022-12-02
 */
@Data
@Accessors(chain = true)
public class FlowElementVO
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;
    private String name;

}
