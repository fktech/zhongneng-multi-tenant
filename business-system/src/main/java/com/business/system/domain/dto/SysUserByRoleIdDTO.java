package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 用户角色查询DTO
 * @Date: 2023/2/24 23:14
 */
@Data
@ApiModel(value = "SysUserByRoleIdDTO对象", description = "用户角色查询对象")
public class SysUserByRoleIdDTO implements Serializable {

    private static final long serialVersionUID = -2865020511591145690L;

    @ApiModelProperty("角色ID")
    private String roleId;

    @ApiModelProperty("租户ID")
    private String comId;
}
