package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 公司对象 ba_company
 *
 * @author ljb
 * @date 2022-11-30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "公司对象",description = "")
@TableName("ba_company")
public class BaCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 运输单位名称 */
    @Excel(name = "运输单位名称")
    @ApiModelProperty(value = "运输单位名称")
    private String name;

    /** 省 */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 */
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @ApiModelProperty(value = "详细地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String phone;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 开户名 */
    @Excel(name = "开户名")
    @ApiModelProperty(value = "开户名")
    private String accountName;

    /** 账户号 */
    @Excel(name = "账户号")
    @ApiModelProperty(value = "账户号")
    private String account;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 营业执照 */
    @Excel(name = "营业执照")
    @ApiModelProperty(value = "营业执照")
    private String businessLicense;

    /** 公司类型 */
    @Excel(name = "公司类型")
    @ApiModelProperty(value = "公司类型")
    private String companyType;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 收货地址 */
    @Excel(name = "收货地址")
    @ApiModelProperty(value = "收货地址")
    private String receiptAddress;

    /** 到站专用线 */
    @Excel(name = "到站专用线")
    @ApiModelProperty(value = "到站专用线")
    private String dedicatedLine;

    /** 收费标准 */
    @Excel(name = "收费标准")
    @ApiModelProperty(value = "收费标准")
    private String chargingStandard;

    /** 用煤单位（终端企业） */
    @Excel(name = "用煤单位（终端企业）")
    @ApiModelProperty(value = "用煤单位（终端企业）")
    private String enterpriseId;

    @TableField(exist = false)
    @ApiModelProperty(value = "用煤单位（终端企业）")
    private String enterpriseName;

    /** 装货站编号 */
    @Excel(name = "装货站编号")
    @ApiModelProperty(value = "装货站编号")
    private String stationId;

    /** 装货站名称 */
    @Excel(name = "装货站名称")
    @ApiModelProperty(value = "装货站名称")
    @TableField(exist = false)
    private String stationName;

    /** 注册资本（万元） */
    @Excel(name = "注册资本（万元）")
    @ApiModelProperty(value = "注册资本（万元）")
    private BigDecimal registeredCapital;

    /** 银行联行号 */
    @Excel(name = "银行联行号")
    @ApiModelProperty(value = "银行联行号")
    private String interBankNo;

    /** 账户类型 */
    @Excel(name = "账户类型")
    @ApiModelProperty(value = "账户类型")
    private String accountType;

    /** 成立日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "成立日期")
    private Date establishDate;

    /** 收款开户行 */
    @Excel(name = "收款开户行")
    @ApiModelProperty(value = "收款开户行")
    private String collectionBankName;

    /** 收款开户名 */
    @Excel(name = "收款开户名")
    @ApiModelProperty(value = "收款开户名")
    private String collectionAccountName;

    /** 收款账号 */
    @Excel(name = "收款账号")
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 收款账户 */
    @TableField(exist = false)
    private List<BaBank> baBankList ;

    /** 基本账户 **/
    @TableField(exist = false)
    private BaBank baBank ;

    /** 法人身份证 */
    @Excel(name = "法人身份证")
    @ApiModelProperty(value = "法人身份证")
    private String corporateCapacityIdCard;

    /** 法人身份证号 */
    @Excel(name = "法人身份证号")
    @ApiModelProperty(value = "法人身份证号")
    private String corporateCapacity;

    /** 法人名称 */
    @Excel(name = "法人名称")
    @ApiModelProperty(value = "法人名称")
    private String corporationName;

    /** 法人电话 */
    @Excel(name = "法人电话")
    @ApiModelProperty(value = "法人电话")
    private String corporationPhone;

    /** 负责人名称 */
    @Excel(name = "负责人名称/负责人身份证号")
    @ApiModelProperty(value = "负责人名称")
    private String superintendentName;

    /** 负责人电话 */
    @Excel(name = "负责人电话")
    @ApiModelProperty(value = "负责人电话")
    private String superintendentPhone;

    /** 公司简称 */
    @Excel(name = "公司简称")
    @ApiModelProperty(value = "公司简称")
    private String companyAbbreviation;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String appendix;

    /** 年化率 */
    @Excel(name = "年化率")
    @ApiModelProperty(value = "年化率")
    private BigDecimal annualizedRate;

    /** 公司性质 */
    @Excel(name = "公司性质")
    @ApiModelProperty(value = "公司性质")
    private String enterpriseNature;

    /** 节点比例 */
    @Excel(name = "节点比例")
    @ApiModelProperty(value = "节点比例")
    private String panelPoint;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 审批流程Id **/
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 租户Id **/
    @Excel(name = "租户Id")
    @ApiModelProperty(value = "租户Id")
    private String tenantId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    /**
     * 开票信息
     */
    @ApiModelProperty(value = "开票信息")
    @TableField(exist = false)
    private BaBillingInformation billingInformation;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 加工费用
     */
    @Excel(name = "加工费用")
    @ApiModelProperty(value = "加工费用")
    private BigDecimal processingCosts;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

}
