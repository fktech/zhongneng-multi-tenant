package com.business.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
         * 组织结构
         *
         * @author : js
         * @version : 1.0
         */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressBook {
            private String name;
            private Integer numer;
            private String type;
            private  String id ;
            private  String address ;
            private List<AddressBook> children;

}
