package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 用户
 * @Date: 2022/11/29 10:02
 */
@Data
@ApiModel(value = "SysUserDTO对象", description = "用户对象")
public class SysUserDTO implements Serializable {

    private static final long serialVersionUID = 5585852811162906546L;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("用户昵称")
    private String nickName;
}
