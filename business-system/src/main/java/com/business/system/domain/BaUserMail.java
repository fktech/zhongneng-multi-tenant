package com.business.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 通讯录对象 ba_user_mail
 *
 * @author ljb
 * @date 2023-06-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "通讯录对象",description = "")
@TableName("ba_user_mail")
public class BaUserMail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 员工姓名 */
    @Excel(name = "员工姓名")
    @ApiModelProperty(value = "员工姓名")
    private String name;

    /** 联系方式 */
    @Excel(name = "联系方式")
    @ApiModelProperty(value = "联系方式")
    private String phone;

    /** 所属部门 */
    @Excel(name = "所属部门")
    @ApiModelProperty(value = "所属部门")
    private Long deptId;

    /** 所属部门名称 */
    @ApiModelProperty(value = "所属部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 角色ID */
    @Excel(name = "角色ID")
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    /** 角色名称 */
    @ApiModelProperty(value = "角色名称")
    @TableField(exist = false)
    private String roleName;

    /** 入职日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "入职日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "入职日期")
    private Date joinTime;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private String flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 部门层级 **/
    @Excel(name = "部门层级")
    @ApiModelProperty(value = "部门层级")
    private String deptPid;

    /** 本数据本门id **/
    @Excel(name = "本数据本门id")
    @ApiModelProperty(value = "本数据本门id")
    private Long deptNum;

    /**
     * 租户ID
     */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;


}
