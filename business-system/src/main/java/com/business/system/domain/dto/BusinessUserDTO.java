package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "BusinessUserDTO对象", description = "业务用户DTO对象")
public class BusinessUserDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 立项ID */
    @ApiModelProperty(value = "立项ID")
    private String projectId;

}
