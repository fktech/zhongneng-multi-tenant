package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 根据运营角色查询业务经理DTO
 * @Date: 2023/4/4 10:07
 */
@Data
@ApiModel(value = "SysUserByDepRoleDTO对象", description = "根据运营角色查询业务经理DTO")
public class SysUserByDepRoleDTO implements Serializable {

    private static final long serialVersionUID = -2865020511591145690L;

    @ApiModelProperty("角色ID")
    private String roleId;

    @ApiModelProperty("部门角色ID")
    private String deptRoleId;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
