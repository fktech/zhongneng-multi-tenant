package com.business.system.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 进厂确认附对象 ba_service_voucher_record
 *
 * @author single
 * @date 2023-09-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "进厂确认附对象",description = "")
@TableName("ba_service_voucher_record")
public class BaServiceVoucherRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 清单ID */
    @Excel(name = "清单ID")
    @ApiModelProperty(value = "清单ID")
    private String purchaseId;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String associationId;

    /** 批次进厂车数 */
    @Excel(name = "批次进厂车数")
    @ApiModelProperty(value = "批次进厂车数")
    private Long batchCars;

    /** 批次进厂数量 */
    @Excel(name = "批次进厂数量")
    @ApiModelProperty(value = "批次进厂数量")
    private BigDecimal batchNumber;

    /** 货值 */
    @Excel(name = "货值")
    @ApiModelProperty(value = "货值")
    private BigDecimal goodsValue;

    /** 需付款 */
    @Excel(name = "需付款")
    @ApiModelProperty(value = "需付款")
    private BigDecimal paymentRequired;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;



}
