package com.business.system.domain.vo;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 组织结构
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrgTreeVo {

    private Long id;

    private String name;

    private String type;

    private String avatar;

    private String sex;

    private Boolean selected;

    private String nickName;

    private SysUser user;

    private SysDept dept;

    private String userName;

    private String deptType;

    private Integer numer;
    private String phonenumber;
    private String postName;

    private List<OrgTreeVo> children;

    private List<OrgTreeVo> orgTreeVoList;

    private String roleKey;
}
