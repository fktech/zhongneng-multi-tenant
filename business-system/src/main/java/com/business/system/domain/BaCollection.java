package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.BaOrderDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 收款信息对象 ba_collection
 *
 * @author single
 * @date 2022-12-26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "收款信息对象",description = "")
@TableName("ba_collection")
public class BaCollection extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 结算单编号 */
    @Excel(name = "结算单编号")
    @ApiModelProperty(value = "结算单编号")
    private String settlementId;

    /** 本期收款比例 */
    @Excel(name = "本期收款比例")
    @ApiModelProperty(value = "本期收款比例")
    private String currentCollectionRatio;

    /** 本期应收金额(元) */
    @Excel(name = "本期应收金额(元)")
    @ApiModelProperty(value = "本期应收金额(元)")
    private BigDecimal currentCollectionAmount;

    /** 实收金额(元) */
    @Excel(name = "实收金额(元)")
    @ApiModelProperty(value = "实收金额(元)")
    private BigDecimal actualCollectionAmount;

    /** 申请收款金额(元) */
    @Excel(name = "申请收款金额(元)")
    @ApiModelProperty(value = "申请收款金额(元)")
    private BigDecimal applyCollectionAmount;

    /** 申请收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请收款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请收款日期")
    private Date applyCollectionDate;

    /** 我方付款账户 */
    @Excel(name = "我方付款账户")
    @ApiModelProperty(value = "我方付款账户")
    private String ourPaymentAccount;

    /** 账户类型 */
    @Excel(name = "账户类型")
    @ApiModelProperty(value = "账户类型")
    private String accountType;

    /** 收款单位 */
    @Excel(name = "收款单位")
    @ApiModelProperty(value = "收款单位")
    private String company;

    /** 账户号 */
    @Excel(name = "账户号")
    @ApiModelProperty(value = "账户号")
    private String otherAccount;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String otherBank;

    /** 开户名称 */
    @Excel(name = "开户名称")
    @ApiModelProperty(value = "开户名称")
    private String otherAccountName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 合同编号 */
    @Excel(name = "合同编号")
    @ApiModelProperty(value = "合同编号")
    private String contractNum;

    /** 合同ID */
    @Excel(name = "合同ID")
    @ApiModelProperty(value = "合同ID")
    private String contractId;
    @TableField(exist = false)
    @ApiModelProperty(value = "移动端查询的临时字段-付款")
    private String keyWords;
    /** 付款类型 */
    @Excel(name = "付款类型")
    @ApiModelProperty(value = "付款类型")
    private String type;

    /** 收款回单 */
    @Excel(name = "收款回单")
    @ApiModelProperty(value = "收款回单")
    private String annex;

    /** 其他附件 */
    @Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "收款日期")
    private Date collectionDate;

    /** 收款方式 */
    @Excel(name = "收款方式")
    @ApiModelProperty(value = "收款方式")
    private String collectionType;

    /** 付款单位 */
    @Excel(name = "付款单位")
    @ApiModelProperty(value = "付款单位")
    private String paymentCompany;

    @TableField(exist = false)
    @ApiModelProperty(value = "付款单位名称")
    private String paymentCompanyName;

    @TableField(exist = false)
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 付款账号 */
    @Excel(name = "付款账号")
    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    /** 收款单位 */
    @Excel(name = "收款单位")
    @ApiModelProperty(value = "收款单位")
    private String collectionCompany;

    /** 收款单位名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "收款单位名称")
    private String collectionCompanyName;

    /** 收款账号 */
    @Excel(name = "收款账号")
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 收款凭证 */
    @Excel(name = "收款凭证")
    @ApiModelProperty(value = "收款凭证")
    private String collectionVoucher;

    /** 凭证备注 */
    @Excel(name = "凭证备注")
    @ApiModelProperty(value = "凭证备注")
    private String voucherRemark;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    @TableField(exist = false)
    private BaSettlement baSettlement;

    /** 开户信息 */
    @TableField(exist = false)
    private List<BaBank> bankList;

    /** 开户信息 */
    @TableField(exist = false)
    private BaBank bank;

    /** 业务状态 */
    @Excel(name = "业务状态")
    @ApiModelProperty(value = "业务状态")
    private String collectionState;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 发起人名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /** 账户id **/
    @ApiModelProperty(value = "账户ID")
    private String bankId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /**
     * 已认领金额
     */
    @Excel(name = "已认领金额(元)")
    @ApiModelProperty(value = "已认领金额(元)")
    private BigDecimal claimed;

    /**
     * 认领记录
     */
    @ApiModelProperty(value = "认领记录")
    @TableField(exist = false)
    private List<BaClaim> baClaims;

    /**
     * 认领状态
     */
    @TableField(exist = false)
    private String claimState;

    /**
     * 收款订单对象
     */
    @TableField(exist = false)
    private List<BaOrderDTO> baOrderDTOs;

    /**
     * 标识
     */
    @TableField(exist = false)
    private String logo;


    /** 审批发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "审批发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "审批发起时间")
    private Date initiationTime;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID **/
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 去审批人
     */
    @Excel(name = "去审批人")
    @ApiModelProperty(value = "去审批人")
    private Long approver;

    /**
     * 退回
     */
    @ApiModelProperty(value = "退回")
    private String goBack;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发起时间")
    private Date launchTime;

    /**
     * 收款类别
     */
    @ApiModelProperty(value = "收款类别")
    private String collectionCategory;

    /**
     * 合同启动ID
     */
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /**
     * 合同启动名称
     */
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /**
     * 交易摘要
     */
    @ApiModelProperty(value = "交易摘要")
    private String transactionSummary;

    /**
     * 创建人公司名称
     */
    @Excel(name = "创建人公司名称")
    @TableField(exist = false)
    private String comName;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /**
     * 全局编号
     */
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
