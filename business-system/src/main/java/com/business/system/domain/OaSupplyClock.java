package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 补卡对象 oa_supply_clock
 *
 * @author single
 * @date 2023-11-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "补卡对象",description = "")
@TableName("oa_supply_clock")
public class OaSupplyClock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 员工ID */
    @Excel(name = "员工ID")
    @ApiModelProperty(value = "员工ID")
    private Long employeeId;

    /** 经度 */
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** 补卡地址 */
    @Excel(name = "补卡地址")
    @ApiModelProperty(value = "补卡地址")
    private String address;

    /** 考勤类型 1 内勤 2 外勤 */
    @Excel(name = "考勤类型 1 内勤 2 外勤")
    @ApiModelProperty(value = "考勤类型 1 内勤 2 外勤")
    private String type;

    /** 补卡班次 1 上班补卡 2 下班补卡 */
    @Excel(name = "补卡班次 1 上班补卡 2 下班补卡")
    @ApiModelProperty(value = "补卡班次 1 上班补卡 2 下班补卡")
    private String clockType;

    /** 补卡日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "补卡日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "补卡日期")
    private Date clockDate;

    /** 补卡时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "补卡时间", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "补卡时间")
    private Date clockTime;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 补卡状态（1：正常  2：迟到  3：早退） */
    @Excel(name = "补卡状态", readConverterExp = "1=：正常,2=：迟到,3=：早退")
    @ApiModelProperty(value = "补卡状态")
    private String clockState;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 提交状态 */
    @Excel(name = "提交状态")
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 班次ID */
    @Excel(name = "班次ID")
    @ApiModelProperty(value = "班次ID")
    private String workTimeId;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 补卡条件 */
    @ApiModelProperty(value = "补卡条件")
    @TableField(exist = false)
    private String cardConditions;
}
