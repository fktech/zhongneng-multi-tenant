package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 入库清单对象 ba_depot_inventory
 *
 * @author ljb
 * @date 2023-09-07
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "入库清单对象",description = "")
@TableName("ba_depot_inventory")
public class BaDepotInventory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String plateNumber;

    /** 司机名称 */
    @Excel(name = "司机名称")
    @ApiModelProperty(value = "司机名称")
    private String driverUserName;

    /** 司机手机号 */
    @Excel(name = "司机手机号")
    @ApiModelProperty(value = "司机手机号")
    private String driverMobile;

    /** 卸车净重 */
    @Excel(name = "卸车净重")
    @ApiModelProperty(value = "卸车净重")
    private BigDecimal unloadWeight;

    /** 卸车磅单 */
    @Excel(name = "卸车磅单")
    @ApiModelProperty(value = "卸车磅单")
    private String unloadPound;

    /** 收货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "收货时间")
    private Date receivingTime;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 关联主键 */
    @Excel(name = "关联主键")
    @ApiModelProperty(value = "关联主键")
    private String relationId;

    /** 来源 */
    @Excel(name = "来源")
    @ApiModelProperty(value = "来源")
    private String source;

    /** 平台类型 */
    @Excel(name = "平台类型")
    @ApiModelProperty(value = "平台类型")
    private String platformType;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货时间")
    private Date deliveryTime;

    /** 装车净重 */
    @Excel(name = "装车净重")
    @ApiModelProperty(value = "装车净重")
    private BigDecimal loadingWeight;

    /** 装车磅单 */
    @Excel(name = "装车磅单")
    @ApiModelProperty(value = "装车磅单")
    private String loadingPound;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String waybillCode;

    /** 货源线路 */
    @ApiModelProperty(value = "货源发布人名称")
    @TableField(exist = false)
    private BaTransportAutomobile baTransportAutomobile;

    /**
     * 项目名称
     */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /**
     * 仓库名称
     */
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    @TableField(exist = false)
    private String type;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;

    /**
     * 仓库ID
     */
    @ApiModelProperty(value = "仓库ID")
    @TableField(exist = false)
    private String depotId;

}
