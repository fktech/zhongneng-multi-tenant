package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.DepotHeadDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 仓库信息对象 ba_depot
 *
 * @author ljb
 * @date 2022-11-30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "仓库信息对象",description = "")
@TableName("ba_depot")
public class BaDepot extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    @ApiModelProperty(value = "仓库名称")
    private String name;

    /** 仓库地址 */
    @Excel(name = "仓库地址")
    @ApiModelProperty(value = "仓库地址")
    private String address;

    /** 仓储费 */
    @Excel(name = "仓储费")
    @ApiModelProperty(value = "仓储费")
    private BigDecimal warehousing;

    /** 搬运费 */
    @Excel(name = "搬运费")
    @ApiModelProperty(value = "搬运费")
    private BigDecimal truckage;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /** 负责人 */
    @Excel(name = "负责人")
    @ApiModelProperty(value = "负责人")
    private String principal;

    /** 是否默认 */
    @Excel(name = "是否默认")
    @ApiModelProperty(value = "是否默认")
    private Integer isDefault;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;
    /** 状态 */
    @Excel(name = "开启状态")
    @ApiModelProperty(value = "开启状态")
    private String status;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 联系电话 **/
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String phone;

    /** 省 **/
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 **/
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 **/
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 所属公司 **/
    @Excel(name = "所属公司")
    @ApiModelProperty(value = "所属公司")
    private String company;

    /** 所属公司名称 **/
    @ApiModelProperty(value = "所属公司名称")
    @TableField(exist = false)
    private String companyName;

    /** 收费标准 **/
    @Excel(name = "收费标准")
    @ApiModelProperty(value = "收费标准")
    private String chargesStandard;

    /** 仓库面积 **/
    @Excel(name = "仓库面积")
    @ApiModelProperty(value = "仓库面积")
    private BigDecimal depotArea;
    /**  用户id**/
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;
    /** 部门id **/
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;
    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /**
     * 发起人名称
     */
    @TableField(exist = false)
    private String userName;
    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /**
     * 库位信息
     */
    @TableField(exist = false)
    private List<BaDepotLocation> baDepotLocationList;

    /**
     * 货位图
     */
    @TableField(exist = false)
    private BaDepotMap baDepotMap;

    @TableField(exist = false)
    @ApiModelProperty(value = "库存总量")
    private BigDecimal inventoryTotal;

    @TableField(exist = false)
    @ApiModelProperty(value = "货值")
    private BigDecimal depotCargoValue;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 选中状态 **/
    @Excel(name = "选中状态")
    @ApiModelProperty(value = "选中状态")
    private String isSelected;

    /** 经度 **/
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 纬度 **/
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** 编码 */
    @Excel(name = "编码")
    @ApiModelProperty(value = "编码")
    private String idCode;

    /** 仓库签署方 */
    //@Excel(name = "仓库签署方")
    @ApiModelProperty(value = "仓库签署方")
    private String depotSigned;

    /** 仓库监管方 */
    //@Excel(name = "仓库监管方")
    @ApiModelProperty(value = "仓库监管方")
    private String depotSupervise;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;


}
