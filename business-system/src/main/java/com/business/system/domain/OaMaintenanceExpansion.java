package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 业务维护/拓展对象 oa_maintenance_expansion
 *
 * @author ljb
 * @date 2023-11-13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "业务维护/拓展对象",description = "")
@TableName("oa_maintenance_expansion")
public class OaMaintenanceExpansion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    //发起人名称
    @Excel(name = "申请人")
    @TableField(exist = false)
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 申请类型 */
    @Excel(name = "业务类型", readConverterExp = "1=业务维护或业务拓展,2=业务维护,3=驻场项目")
    @ApiModelProperty(value = "申请类型")
    private String applicationType;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请时间")
    private Date applicationTime;

    /** 出发地 */
    @Excel(name = "出发地")
    @ApiModelProperty(value = "出发地")
    private String origin;

    /** 目的地 */
    @Excel(name = "目的地")
    @ApiModelProperty(value = "目的地")
    private String destination;

    /** 出行方式 */
    @Excel(name = "出行方式", readConverterExp = "1=自驾,2=飞机,3=火车,4=其他")
    @ApiModelProperty(value = "出行方式")
    private String travelMode;

    /** 起始公里数 */
    @Excel(name = "起始里程（公里）")
    @ApiModelProperty(value = "起始公里数")
    private BigDecimal starKilometers;


    /** 到达公里数 */
    @Excel(name = "到达里程（公里）")
    @ApiModelProperty(value = "到达公里数")
    private BigDecimal reachKilometers;

    /** 实际公里数 */
    @Excel(name = "实际里程（公里）")
    @ApiModelProperty(value = "实际公里数")
    private BigDecimal realityKilometers;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String carNumber;


    /** 状态 */
    @Excel(name = "审批状态", readConverterExp = "oaMaintenanceExpansion:verifying=审批中,oaMaintenanceExpansion:pass=已通过,oaMaintenanceExpansion:reject=已拒绝,oaMaintenanceExpansion:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;


    /** 申请事由 */
    @ApiModelProperty(value = "申请事由")
    private String applicationReason;


    /** 车辆名称 */
    @ApiModelProperty(value = "车辆名称")
    @TableField(exist = false)
    private String carName;

    /** 车型 */
    @ApiModelProperty(value = "车型")
    private String vehicleModel;

    /** 起始照片 */
    @ApiModelProperty(value = "起始照片")
    private String starPicture;

    /** 预计公里数 */
    @ApiModelProperty(value = "预计公里数")
    private BigDecimal expectKilometers;


    /** 到达照片 */
    @ApiModelProperty(value = "到达照片")
    private String reachPicture;



    /** 备注 */
    @ApiModelProperty(value = "备注")
    private String notes;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;


    //多用户
    @TableField(exist = false)
    private String userIds;


    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 提交状态 */
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 车辆信息 */
    @ApiModelProperty(value = "车辆信息")
    private String carType;

    /** 车辆备注 */
    @ApiModelProperty(value = "车辆备注")
    private String remarks;

    /** 加油发票 */
    @ApiModelProperty(value = "加油发票")
    private String oilInvoice;

    /** 过路费发票 */
    @ApiModelProperty(value = "过路费发票")
    private String tollInvoice;

    /** 出发时手机设备编码 */
    @ApiModelProperty(value = "出发时手机设备编码")
    private String deviceOne;

    /** 到达时手机设备编码 */
    @ApiModelProperty(value = "到达时手机设备编码")
    private String deviceTwo;

    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    @TableField(exist = false)
    private String businessType;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private Date createDate;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 费用金额 */
    @Excel(name = "费用金额")
    @ApiModelProperty(value = "费用金额")
    private BigDecimal costAmount;

    /** 燃油费 */
    @Excel(name = "燃油费")
    @ApiModelProperty(value = "燃油费")
    private BigDecimal fuelCost;

    /** 过路/过桥费 */
    @Excel(name = "过路/过桥费")
    @ApiModelProperty(value = "过路/过桥费")
    private BigDecimal roadToll;
}
