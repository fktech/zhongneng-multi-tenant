package com.business.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 发票纳税金额统计
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaInvoiceVO {
    //本月纳税金额统计
    private BigDecimal currentMonthTaxAmount ;
    //本年度纳税累计金额
    private BigDecimal currentYearTaxAmount ;
    //增加历年纳税总额
    private BigDecimal taxAmount ;
}
