package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.system.domain.BaCheck;
import com.business.system.domain.BaTransportAutomobile;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "运单对象DTO",description = "运单信息")
public class waybillDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** ID **/
    @ApiModelProperty(value = "ID")
    private String id;

    /** 仓储立项名称 **/
    @ApiModelProperty(value = "仓储立项名称")
    private String storageName;

    /** 销售订单名称 **/
    @ApiModelProperty(value = "销售订单名称")
    private String saleName;

    /** 采购订单名称 **/
    @ApiModelProperty(value = "采购订单名称")
    private String procureName;

    /** 平台 **/
    @ApiModelProperty(value = "平台")
    private String platform;

    /** 货源编码 **/
    @ApiModelProperty(value = "货源编码")
    private String supplierNo;

    /** 运单编码 **/
    @ApiModelProperty(value = "运单编码")
    private String thdno;

    /** 司机姓名 **/
    @ApiModelProperty(value = "司机姓名")
    private String cardriver;


    /** 车号 **/
    @ApiModelProperty(value = "车号")
    private String carno;

    /** 发货地区 **/
    @ApiModelProperty(value = "发货地区")
    private String placeDep;

    /** 收货地区 **/
    @ApiModelProperty(value = "收货地区")
    private String placeDes;

    /** 司机手机号 **/
    @ApiModelProperty(value = "司机手机号")
    private String cartel;

    /** 品名 **/
    @ApiModelProperty(value = "品名")
    private String coaltypename;

    /** 装货时间 **/
    @ApiModelProperty(value = "装货时间")
    private String yfdate;

    /** 装车净重 **/
    @ApiModelProperty(value = "装车净重")
    private BigDecimal ccjweight;

    /** 卸货时间 **/
    @ApiModelProperty(value = "卸货时间")
    private String dhdate;

    /** 卸车净重 **/
    @ApiModelProperty(value = "卸车净重")
    private BigDecimal rcjweight;

    /** 结算状态 **/
    @ApiModelProperty(value = "结算状态")
    private String isjsratesflag;

    /** 开票状态 **/
    @ApiModelProperty(value = "开票状态")
    private String checkFlag;

    /** 创建时间 **/
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /** 删除标记 */
    @ApiModelProperty(value = "删除标记：0-未删除，1-已删除")
    private Long isDelete;

    @ApiModelProperty(value = "待发状态")
    private String confirmStatus;

    @ApiModelProperty(value = "订单编号")
    private String orderId;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "出库")
    private String outbound;

    @ApiModelProperty(value = "入库")
    private String warehousing;

    @ApiModelProperty(value = "出入库类型")
    private String status;

    @ApiModelProperty(value = "出入库状态")
    private String state;

    @ApiModelProperty(value = "货源线路")
    private String sourceName;

    @ApiModelProperty(value = "发货信息")
    private BaCheck delivery;

    @ApiModelProperty(value = "货主名称")
    private String supplierName;

    /** 仓库名称 **/
    @ApiModelProperty(value = "仓库ID")
    private String depotName;

    /** 仓位名称 **/
    @ApiModelProperty(value = "仓位ID")
    private String positionName;


    /** 终端 **/
    @ApiModelProperty(value = "终端")
    private String enterprise;

    /** 终端名称 **/
    @ApiModelProperty(value = "终端名称")
    private String enterpriseName;

    /** 货源信息 **/
    @ApiModelProperty(value = "货源信息")
    @TableField(exist = false)
    private BaTransportAutomobile baTransportAutomobile;

    /** 试验ID **/
    @ApiModelProperty(value = "试验ID")
    private String experimentId;
}
