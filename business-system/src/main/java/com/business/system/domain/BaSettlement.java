package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.BaOrderDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 结算信息对象 ba_settlement
 *
 * @author ljb
 * @date 2022-12-26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "结算信息对象",description = "")
@TableName("ba_settlement")
public class BaSettlement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 业务类型 */
    //@Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 结算方 */
    //@Excel(name = "结算方")
    @ApiModelProperty(value = "结算方")
    private String settlementParty;

    /** 结算方名称 */
    @ApiModelProperty(value = "结算方名称")
    @TableField(exist = false)
    private String settlementPartyName;

    /** 结算日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "结算日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结算日期")
    private Date paymentDate;

    /** 关联发运单 */
    //@Excel(name = "关联发运单")
    @ApiModelProperty(value = "关联发运单")
    private String transportId;

    /** 关联订单 */
    //@Excel(name = "关联订单")
    @ApiModelProperty(value = "关联订单")
    private String orderId;

    /**
     * 订单对象集合
     */
    @ApiModelProperty(value = "订单对象集合")
    @TableField(exist = false)
    private List<BaOrderDTO> baOrderDTOs;

    /** 关联合同 */
    //@Excel(name = "关联合同")
    @ApiModelProperty(value = "关联合同")
    private String contractId;

    @TableField(exist = false)
    private String contractName;

    /**
     * 全局编号
     */
    @Excel(name = "全局编号")
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /** 结算单名称 */
    @Excel(name = "结算单名称")
    @ApiModelProperty(value = "结算单名称")
    private String settlementName;

    /** 结算类型(1:采购结算单2:销售结算单3:运输结算单4:站台结算单) */
    @Excel(name = "结算类型", readConverterExp = "1=上游结算单,2=下游结算单,3=终端结算单")
    @ApiModelProperty(value = "结算类型(1:采购结算单2:销售结算单3:运输结算单4:站台结算单)")
    private Long settlementType;

    /** 结算比例 */
    @Excel(name = "结算比例")
    @ApiModelProperty(value = "结算比例")
    private String settlementProportion;

    /** 结算数量 **/
    @Excel(name = "结算数量")
    @ApiModelProperty(value = "结算数量")
    private BigDecimal settlementNum;

    /** 结算金额(元) */
    @Excel(name = "结算金额(元)")
    @ApiModelProperty(value = "结算金额(元)")
    private BigDecimal settlementAmount;

    /** 已付款金额(元) */
    //@Excel(name = "已付款金额(元)")
    @ApiModelProperty(value = "已付款金额(元)")
    private BigDecimal paidAmount;

    /** 未付款金额(元) */
    //@Excel(name = "未付款金额(元)")
    @ApiModelProperty(value = "未付款金额(元)")
    private BigDecimal unpaidAmount;

    /** 结算单 */
    //@Excel(name = "结算单")
    @ApiModelProperty(value = "结算单")
    private String annex;

    /** 其他附件 */
    //@Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 备注 */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    //发起人名称
    @Excel(name = "归属岗位")
    @TableField(exist = false)
    private String userName;

    /** 状态 */
    @Excel(name = "状态", readConverterExp = "settlement:verifying=审批中,settlement:pass=已通过,settlement:reject=已拒绝,settlement:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 热值金额 */
    //@Excel(name = "热值金额")
    @ApiModelProperty(value = "热值金额")
    private BigDecimal calorificAmount;

    /** 灰分金额 */
    //@Excel(name = "灰分金额")
    @ApiModelProperty(value = "灰分金额")
    private BigDecimal ashContentAmount;

    /** 挥发金额 */
    //@Excel(name = "挥发金额")
    @ApiModelProperty(value = "挥发金额")
    private BigDecimal volatileMatterAmount;

    /** 硫份金额 */
    //@Excel(name = "硫份金额")
    @ApiModelProperty(value = "硫份金额")
    private BigDecimal sulphurAmount;

    /** 全水金额 */
    //@Excel(name = "全水金额")
    @ApiModelProperty(value = "全水金额")
    private BigDecimal totalWaterAmount;

    /** 其他金额 */
    //@Excel(name = "其他金额")
    @ApiModelProperty(value = "其他金额")
    private BigDecimal otherAmount;

    /** 实际扣款金额 */
    //@Excel(name = "实际扣款金额")
    @ApiModelProperty(value = "实际扣款金额")
    private BigDecimal actualDeductionAmount;

    /** 煤量 */
    //@Excel(name = "煤量")
    @ApiModelProperty(value = "煤量")
    private BigDecimal coalQuantity;

    /** 不含税单价 */
    //@Excel(name = "不含税单价")
    @ApiModelProperty(value = "不含税单价")
    private BigDecimal excludingTaxUnivalent;

    /** 不含税金额 */
    //@Excel(name = "不含税金额")
    @ApiModelProperty(value = "不含税金额")
    private BigDecimal excludingTaxAmount;

    /** 税率 */
    //@Excel(name = "税率")
    @ApiModelProperty(value = "税率")
    private BigDecimal taxRate;

    /** 税额 */
    //@Excel(name = "税额")
    @ApiModelProperty(value = "税额")
    private BigDecimal taxAmount;

    /** 含税单价 */
    //@Excel(name = "含税单价")
    @ApiModelProperty(value = "含税单价")
    private BigDecimal taxIncludedUnivalent;

    /** 合计金额 */
    //@Excel(name = "合计金额")
    @ApiModelProperty(value = "合计金额")
    private BigDecimal totalAmount;

    /** 结算状态(1:未发起2:结算中3:已完结) */
    //@Excel(name = "结算状态(1:未发起2:结算中3:已完结)")
    @ApiModelProperty(value = "结算状态(1:未发起2:结算中3:已完结)")
    private String settlementState;

    /** 支付方式 */
    //@Excel(name = "支付方式")
    @ApiModelProperty(value = "支付方式")
    private String paymentMethod;

    /** 运输类型 */
    //@Excel(name = "运输类型")
    @ApiModelProperty(value = "运输类型")
    private String transportType;

    /** 公司 */
    //@Excel(name = "公司")
    @ApiModelProperty(value = "公司")
    private String companyId;

    /** 用户id */
    //@Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;


    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门id */
    //@Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    //部门名称
    @TableField(exist = false)
    private String deptName;

    /** 审批流程id */
    //@Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 付款信息 **/
    @TableField(exist = false)
    private List<BaPayment> paymentList;

    /** 收款信息 **/
    @TableField(exist = false)
    private List<BaCollection> collectionList;

    /** 已付款金额 **/
    @TableField(exist = false)
    private BigDecimal decimal;

    /** 发票数量 **/
    @TableField(exist = false)
    private Integer invoiceNum;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    @TableField(exist = false)
    @ApiModelProperty(value = "移动端查询的临时字段-结算单")
    private String keyWords;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    @TableField(exist = false)
    private String collectionState;

    /** 工作流状态 **/
    //@Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 兼职部门id */
    @ApiModelProperty(value = "兼职部门id")
    @TableField(exist = false)
    private Long belongCompanyId;

    /** 仓储立项编号 **/
    //@Excel(name = "仓储立项编号")
    @ApiModelProperty(value = "仓储立项编号")
    private String projectId;

    /** 仓储立项名 **/
    @ApiModelProperty(value = "仓储立项名")
    @TableField(exist = false)
    private String projectName;

    /** 采购计划编号 **/
    //@Excel(name = "采购计划编号")
    @ApiModelProperty(value = "采购计划编号")
    private String planId;

    /** 采购计划名 **/
    @ApiModelProperty(value = "采购计划名")
    @TableField(exist = false)
    private String planName;

    /** 项目类型 **/
    //@Excel(name = "项目类型")
    @ApiModelProperty(value = "项目类型")
    private String projectTypes;

    /** 仓库id */
    @ApiModelProperty(value = "仓库id")
    @TableField(exist = false)
    private String depotId;

    /** 租户ID **/
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 最终标识 0:否  1:是 **/
    //@Excel(name = "最终标识")
    @ApiModelProperty(value = "最终标识")
    private String finalMark;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
