package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @Author: js
 * @Description: 流程删除对象
 * @date: 2022-12-2 15:29:02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProcessDefinitionRelatedDeleteDTO对象", description = "流程删除对象")
public class BaProcessDefinitionRelatedDeleteDTO implements Serializable {

    private static final long serialVersionUID = 6246147416312139684L;

    @ApiModelProperty("流程分类")
    @NotEmpty(message = "请选择记录")
    private List<Long> ids;
}
