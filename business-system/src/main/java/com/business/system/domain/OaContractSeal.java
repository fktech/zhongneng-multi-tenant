package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * oa合同用印对象 oa_contract_seal
 *
 * @author single
 * @date 2024-06-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "oa合同用印对象",description = "")
@TableName("oa_contract_seal")
public class OaContractSeal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 合同名称 */
    @Excel(name = "合同名称")
    @ApiModelProperty(value = "合同名称")
    private String name;

    /** 合同编号 */
    @Excel(name = "合同编号")
    @ApiModelProperty(value = "合同编号")
    private String code;

    /** 签约日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "签约日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "签约日期")
    private Date signingDate;

    /** 我方单位名称 */
    @Excel(name = "我方单位名称")
    @ApiModelProperty(value = "我方单位名称")
    private String ourUnitName;

    /** 对方单位名称 */
    @Excel(name = "对方单位名称")
    @ApiModelProperty(value = "对方单位名称")
    private String opposingUnit;

    /** 合同量 */
    @Excel(name = "合同量")
    @ApiModelProperty(value = "合同量")
    private String contractQuantity;

    /** 合同类型 */
    @Excel(name = "合同类型")
    @ApiModelProperty(value = "合同类型")
    private String contractType;

    /** 生效日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "生效日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "生效日期")
    private Date effectiveDate;

    /** 完成日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "完成日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "完成日期")
    private Date completionDate;

    /** 备注及其他 */
    @Excel(name = "备注及其他")
    @ApiModelProperty(value = "备注及其他")
    private String remarks;

    /** 用印前合同 */
    @Excel(name = "用印前合同")
    @ApiModelProperty(value = "用印前合同")
    private String priorContract;

    /** 文件份数 */
    @Excel(name = "文件份数")
    @ApiModelProperty(value = "文件份数")
    private Long fileNumber;

    /** 用印类型 */
    @Excel(name = "用印类型")
    @ApiModelProperty(value = "用印类型")
    private String sealType;

    /** 是否需要用章所属公司提供材料 */
    @Excel(name = "是否需要用章所属公司提供材料")
    @ApiModelProperty(value = "是否需要用章所属公司提供材料")
    private String whether;

    /** 备注内写明所需材料 */
    @Excel(name = "备注内写明所需材料")
    @ApiModelProperty(value = "备注内写明所需材料")
    private String requiredMaterials;

    /** 用印后合同 */
    @Excel(name = "用印后合同")
    @ApiModelProperty(value = "用印后合同")
    private String afterPrinting;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户 */
    @Excel(name = "用户")
    @ApiModelProperty(value = "用户")
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    //发起人名称
    @Excel(name = "申请人")
    @TableField(exist = false)
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;



}
