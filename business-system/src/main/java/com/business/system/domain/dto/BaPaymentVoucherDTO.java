package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 付款信息对象 ba_payment
 *
 * @author js
 * @date 2022-12-26
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "付款信息对象",description = "")
public class BaPaymentVoucherDTO extends BaseEntity
{

    /** 主键 */
    private String id;

    /** 付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "付款日期")
    private Date paymentDate;

    /** 付款方式 */
    @ApiModelProperty(value = "付款方式")
    private String paymentType;

    /** 付款单位 */
    @ApiModelProperty(value = "付款单位")
    private String paymentCompany;

    /** 付款账号 */
    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    /** 收款单位 */
    @ApiModelProperty(value = "收款单位")
    private String collectionCompany;

    /** 收款账号 */
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 付款凭证 */
    @ApiModelProperty(value = "付款凭证")
    private String paymentVoucher;

    /** 凭证备注 */
    @ApiModelProperty(value = "凭证备注")
    private String voucherRemark;

    /** 确认付款金额 */
    @ApiModelProperty(value = "确认付款金额")
    private BigDecimal paymentAmount;

    /** 最后一次付款标记 **/
    @ApiModelProperty(value = "最后一次付款标记")
    private String mark;

    /** 可付款金额 */
    @ApiModelProperty(value = "可付款金额")
    private BigDecimal payableAmount;


}
