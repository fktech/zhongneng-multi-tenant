package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 用煤企业对象 ba_enterprise
 *
 * @author ljb
 * @date 2022-11-29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "用煤企业对象",description = "")
@TableName("ba_enterprise")
public class BaEnterprise extends BaseEntity
{

    private static final long serialVersionUID = -710788161924307804L;

    /** 主键 */
    private String id;

    /** 企业名称 */
    @Excel(name = "企业名称")
    @ApiModelProperty(value = "企业名称")
    private String name;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty(value = "联系人")
    private String counterpart;

    /** 省 */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 */
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @ApiModelProperty(value = "详细地址")
    private String address;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String mobilePhone;

    /** 所属集团公司 */
    @Excel(name = "所属集团公司")
    @ApiModelProperty(value = "所属集团公司")
    private String companyNature;

    /** 所属公司名称 **/
    @ApiModelProperty(value = "所属公司名称")
    @TableField(exist = false)
    private String companyName;

    /** 合作年限 */
    @Excel(name = "合作年限")
    @ApiModelProperty(value = "合作年限")
    private String cooperationYears;

    /** 回款账期（天） */
    @Excel(name = "回款账期", readConverterExp = "天=")
    @ApiModelProperty(value = "回款账期")
    private Integer collectionPeriod;

    /** 付款方式 */
    @Excel(name = "付款方式")
    @ApiModelProperty(value = "付款方式")
    private String paymentType;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 开户名 */
    @Excel(name = "开户名")
    @ApiModelProperty(value = "开户名")
    private String accountName;

    /** 账号 */
    @Excel(name = "账号")
    @ApiModelProperty(value = "账号")
    private String account;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 法人身份证号 */
    @Excel(name = "法人身份证号")
    @ApiModelProperty(value = "法人身份证号")
    private String corporateCapacity;

    /** 营业执照 */
    @Excel(name = "营业执照")
    @ApiModelProperty(value = "营业执照")
    private String businessLicense;

    /** 法人身份证 */
    @Excel(name = "法人身份证")
    @ApiModelProperty(value = "法人身份证")
    private String corporateCapacityIdCard;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 投标网站 */
    @Excel(name = "投标网站")
    @ApiModelProperty(value = "投标网站")
    private String bidWebsite;

    /** 投标网站 */
    @Excel(name = "投标地址")
    @ApiModelProperty(value = "投标地址")
    private String bidAddress;


    /** 收款开户行 */
    @Excel(name = "收款开户行")
    @ApiModelProperty(value = "收款开户行")
    private String collectionBankName;

    /** 收款开户名 */
    @Excel(name = "收款开户名")
    @ApiModelProperty(value = "收款开户名")
    private String collectionAccountName;

    /** 收款账号 */
    @Excel(name = "收款账号")
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 银行联行号 */
    @Excel(name = "银行联行号")
    @ApiModelProperty(value = "银行联行号")
    private String interBankNo;

    /** 企业分类 */
    @Excel(name = "企业分类")
    @ApiModelProperty(value = "企业分类")
    private Integer companyClassify;

    /** 分类级别 */
    @Excel(name = "分类级别")
    @ApiModelProperty(value = "分类级别")
    private Integer classifyLevel;

    /** 总公司 */
    @Excel(name = "总公司")
    @ApiModelProperty(value = "总公司")
    private String headquarters;
    /** 违约考核 */
    @Excel(name = "违约考核")
    @ApiModelProperty(value = "违约考核")
    private String defaultAssessment;
    /** 履约保证金 */
    @Excel(name = "履约保证金缴罚规则")
    @ApiModelProperty(value = "履约保证金缴罚规则")
    private String performanceBond;
    /** 额度上限 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportType;
    /** 额度上限 */
    @Excel(name = "额度上限")
    @ApiModelProperty(value = "额度上限")
    private BigDecimal ceiling;
    /** 回款周期 */
    @Excel(name = "回款周期")
    @ApiModelProperty(value = "回款周期")
    private String paymentTime;

    /**
     * 分公司
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "分公司")
    List<BaEnterprise> baEnterprises;

    /** 收款账户 */
    @TableField(exist = false)
    private List<BaBank> baBankList ;

    /** 基本账户 **/
    @TableField(exist = false)
    private BaBank baBank ;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 审批流程Id **/
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 租户Id **/
    @Excel(name = "租户Id")
    @ApiModelProperty(value = "租户Id")
    private String tenantId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    /**
     * 所需煤种
     */
    @ApiModelProperty(value = "所需煤种")
    @TableField(exist = false)
    private List<BaEnterpriseRelevance> enterpriseRelevance;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 所需煤种商品名称
     */
    @ApiModelProperty(value = "所需煤种商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** 成立日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "成立日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "成立日期")
    private Date tenableDate;

    /** 所属行业 **/
    @Excel(name = "所属行业")
    @ApiModelProperty(value = "所属行业")
    private String industry;

    /** 企业性质 **/
    @Excel(name = "企业性质")
    @ApiModelProperty(value = "企业性质")
    private String enterpriseNature;

    /** 到站 **/
    @Excel(name = "到站")
    @ApiModelProperty(value = "到站")
    private String station;


    /** 公司简称 **/
    @Excel(name = "到站")
    @ApiModelProperty(value = "公司简称")
    private String companyAbbreviation;

    /**
     * 开票信息
     */
    @ApiModelProperty(value = "开票信息")
    @TableField(exist = false)
    private BaBillingInformation billingInformation;

    /** 企业说明 **/
    @Excel(name = "企业说明")
    @ApiModelProperty(value = "企业说明")
    private String enterpriseDesc;

    /** 到站地址 **/
    @Excel(name = "到站地址")
    @ApiModelProperty(value = "到站地址")
    private String stationAddress;

    /** 投标保证金缴罚规则 **/
    @Excel(name = "投标保证金缴罚规则")
    @ApiModelProperty(value = "投标保证金缴罚规则")
    private String bidPenaltyRules;

    /** 收费标准 **/
    @ApiModelProperty(value = "收费标准")
    @TableField(exist = false)
    private BaChargingStandards baChargingStandards;

    /**
     * 所属集团公司
     */
    @ApiModelProperty(value = "所属集团公司")
    @TableField(exist = false)
    private BaCompanyGroup baCompanyGroup;

    /** 父ID */
    @Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 变更标识 */
    @Excel(name = "变更标识")
    @ApiModelProperty(value = "变更标识")
    private String approveFlag;

    /** 变更人 */
    @Excel(name = "变更人")
    @ApiModelProperty(value = "变更人")
    private String changeBy;

    /** 变更时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "变更时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "变更时间")
    private Date changeTime;

    /** 累计发运 */
    @ApiModelProperty(value = "累计发运")
    @TableField(exist = false)
    private BigDecimal checkCoalNum;

    /** 累计贸易 */
    @ApiModelProperty(value = "累计贸易")
    @TableField(exist = false)
    private BigDecimal tradeAmount;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 营业执照 */
    @Excel(name = "营业执照")
    @ApiModelProperty(value = "营业执照")
    private String file;

    /**
     * 企业资质
     */
    //@Excel(name = "企业资质")
    @ApiModelProperty(value = "企业资质")
    private String qualificationsAnnex;

    /** 其他附件 **/
    @Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /**
     * 终端类型
     */
    @ApiModelProperty(value = "终端类型")
    private String enterpriseType;
}
