package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 商品对象 ba_goods
 *
 * @author ljb
 * @date 2022-12-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "商品对象",description = "")
@TableName("ba_goods")
public class BaGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 商品编码 */
    @Excel(name = "商品编码")
    @ApiModelProperty(value = "商品编码")
    private String code;

    /** 产品类型 */
    @Excel(name = "产品类型")
    @ApiModelProperty(value = "产品类型")
    private String goodsType;

    /** 产品类型名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "产品类型名称")
    private String goodsTypeName;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 制造商 */
    @Excel(name = "制造商")
    @ApiModelProperty(value = "制造商")
    private String mfrs;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    @ApiModelProperty(value = "供应商编号")
    private String supplierId;

    /** 安全存量 */
    @Excel(name = "安全存量")
    @ApiModelProperty(value = "安全存量")
    private String safetyStock;

    /** 型号 */
    @Excel(name = "型号")
    @ApiModelProperty(value = "型号")
    private String model;

    /** 规格 */
    @Excel(name = "规格")
    @ApiModelProperty(value = "规格")
    private String standard;

    /** 颜色 */
    @Excel(name = "颜色")
    @ApiModelProperty(value = "颜色")
    private String color;

    /** 单位 */
    @Excel(name = "单位")
    @ApiModelProperty(value = "单位")
    private String unit;

    /** 参考购买价格 */
    @Excel(name = "参考购买价格")
    @ApiModelProperty(value = "参考购买价格")
    private BigDecimal buyPrice;

    /** 参考销售价格 */
    @Excel(name = "参考销售价格")
    @ApiModelProperty(value = "参考销售价格")
    private BigDecimal salePrice;

    /** 拓展信息 */
    @Excel(name = "拓展信息")
    @ApiModelProperty(value = "拓展信息")
    private String extInfo;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 图片名称 */
    @Excel(name = "图片名称")
    @ApiModelProperty(value = "图片名称")
    private String imgName;

    /** 计量单位 */
    @Excel(name = "计量单位")
    @ApiModelProperty(value = "计量单位")
    private String unitId;

    /** 是否启停用 */
    @Excel(name = "是否启停用")
    @ApiModelProperty(value = "是否启停用")
    private Integer enabled;

    /** 是否开启序列号 */
    @Excel(name = "是否开启序列号")
    @ApiModelProperty(value = "是否开启序列号")
    private String enabledSerialNumber;

    /** 商品条码 */
    @Excel(name = "商品条码")
    @ApiModelProperty(value = "商品条码")
    private String barCode;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 商品附加信息 **/
    @TableField(exist = false)
    private List<BaGoodsAdd> goodsAddList;

    /** 信息名称 */
    @ApiModelProperty(value = "信息名称")
    @TableField(exist = false)
    private String informationName;

    /** 信息值 */
    @ApiModelProperty(value = "信息值")
    @TableField(exist = false)
    private String informationValue;

    /** 扩展信息备注 */
    @ApiModelProperty(value = "扩展信息备注")
    @TableField(exist = false)
    private String addRemarks;

    /** 扩展信息集合 */
    @TableField(exist = false)
    @ApiModelProperty(value = "扩展信息集合")
    private List<BaGoodsAdd> baGoodsAdds;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID **/
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 拓展信息包含其他信息 */
    @Excel(name = "拓展信息包含其他信息")
    @ApiModelProperty(value = "拓展信息包含其他信息")
    private String extInfoAll;

    /** 规格型号 */
    @Excel(name = "规格型号")
    @ApiModelProperty(value = "规格型号")
    private String specificationModel;

    /** 参考成本 */
    @Excel(name = "参考成本")
    @ApiModelProperty(value = "参考成本")
    private String referenceCost;

    /** 开票品名 */
    @Excel(name = "开票品名")
    @ApiModelProperty(value = "开票品名")
    private String invoicedGoods;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

}
