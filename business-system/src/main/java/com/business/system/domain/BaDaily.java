package com.business.system.domain;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 日报对象 ba_daily
 *
 * @author single
 * @date 2023-10-09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "日报对象",description = "")
@TableName("ba_daily")
public class BaDaily extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "日期")
    private Date time;

    /** 始发地 */
    @Excel(name = "始发地")
    @ApiModelProperty(value = "始发地")
    private String origin;

    /** 目的地 */
    @Excel(name = "目的地")
    @ApiModelProperty(value = "目的地")
    private String destination;

    /** 完成情况 */
    @Excel(name = "完成情况")
    @ApiModelProperty(value = "完成情况")
    private String completionStatus;

    /** 工作内容 */
    @Excel(name = "工作内容")
    @ApiModelProperty(value = "工作内容")
    private String workContent;

    /** 问题与障碍 */
    @Excel(name = "问题与障碍")
    @ApiModelProperty(value = "问题与障碍")
    private String problem;

    /** 下一步计划 */
    @Excel(name = "下一步计划")
    @ApiModelProperty(value = "下一步计划")
    private String plan;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 日期查询 */
    @ApiModelProperty(value = "日期查询")
    @TableField(exist = false)
    private String queryTime;

    /** 日报类型 */
    @Excel(name = "日报类型")
    @ApiModelProperty(value = "日报类型")
    private String dailyType;

    /** 当前状态 */
    @Excel(name = "当前状态")
    @ApiModelProperty(value = "当前状态")
    private String currentState;

    /** 所属项目 */
    @Excel(name = "所属项目")
    @ApiModelProperty(value = "所属项目")
    private String affiliationProject;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 其他事项 */
    @Excel(name = "其他事项")
    @ApiModelProperty(value = "其他事项")
    private String others;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 日报状态 */
    @Excel(name = "日报状态")
    @ApiModelProperty(value = "日报状态")
    private String dailyState;

    /** 事项 */
    @Excel(name = "事项")
    @ApiModelProperty(value = "事项")
    private String matter;

    /** 星期几 */
    @Excel(name = "星期几")
    @ApiModelProperty(value = "星期几")
    private String week;

    /** 关键词 */
    @ApiModelProperty(value = "关键词")
    @TableField(exist = false)
    private String keyWord;

    /** 维护项目 */
    @ApiModelProperty(value = "维护项目")
    @TableField(exist = false)
    private String operationProject;

    /**
     * 业务维护与拓展
     */
    @ApiModelProperty(value = "业务维护与拓展")
    @TableField(exist = false)
    private String maintenanceExpansion;

    /**
     * 业务维护/拓展对象
     */
    @TableField(exist = false)
    private List<OaMaintenanceExpansion> maintenanceExpansionList;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 本月数据 */
    @ApiModelProperty(value = "本月数据")
    @TableField(exist = false)
    private String thisMonth;

    /** 是否分组 */
    @ApiModelProperty(value = "是否分组")
    @TableField(exist = false)
    private String group;

    /**
     * 周时间区间
     */
    @ApiModelProperty(value = "周时间区间")
    @TableField(exist = false)
    private String weekTime;

    /** 用户查询 */
    @ApiModelProperty(value = "用户查询")
    @TableField(exist = false)
    private String userIds;
}
