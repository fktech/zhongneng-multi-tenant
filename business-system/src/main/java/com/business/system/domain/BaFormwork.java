package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 模板对象 ba_formwork
 *
 * @author ljb
 * @date 2023-03-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "模板对象",description = "")
@TableName("ba_formwork")
public class BaFormwork extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 模板名称 */
    @Excel(name = "模板名称")
    @ApiModelProperty(value = "模板名称")
    private String name;

    /** 版本 */
    @Excel(name = "版本")
    @ApiModelProperty(value = "版本")
    private String edition;

    /** 模板 */
    @Excel(name = "模板")
    @ApiModelProperty(value = "模板")
    private String formwork;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;



}
