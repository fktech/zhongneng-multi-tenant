package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 统计业务类型占比
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaContractStartTonVO对象",description="统计业务类型占比")
public class BaContractStartTonVO
{
    /** 货物名称 */
    @ApiModelProperty(value = "类型")
    private String type;

    /** 货物重量 */
    @ApiModelProperty(value = "数量")
    private Integer count;

    /** 占比 */
    @ApiModelProperty(value = "占比")
    private String percent;

    /** 合同启动ID */
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
