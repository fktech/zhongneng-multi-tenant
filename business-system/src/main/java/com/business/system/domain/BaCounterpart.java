package com.business.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 对接人对象 ba_counterpart
 *
 * @author ljb
 * @date 2022-11-29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "对接人对象",description = "")
@TableName("ba_counterpart")
public class BaCounterpart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty(value = "姓名")
    private String name;

    /** 手机号 */
    @Excel(name = "手机号")
    @ApiModelProperty(value = "手机号")
    private String phone;

    /** 省 */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    @TableField(exist = false)
    @ApiModelProperty(value = "省")
    private String provinceName;

    /** 市 */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    @TableField(exist = false)
    @ApiModelProperty(value = "市")
    private String cityName;

    /** 区 */
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    @TableField(exist = false)
    @ApiModelProperty(value = "区")
    private String areaName;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @ApiModelProperty(value = "详细地址")
    private String address;

    /** 添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "添加时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "添加时间")
    private Date registrationTime;

    /** 帐号状态 */
    @Excel(name = "帐号状态")
    @ApiModelProperty(value = "帐号状态")
    private String accountStatus;

    /** 身份证号 */
    @Excel(name = "身份证号")
    @ApiModelProperty(value = "身份证号")
    private String idCard;

    /** 身份证正面 */
    @Excel(name = "身份证正面")
    @ApiModelProperty(value = "身份证正面")
    private String cardPositive;

    /** 反面 */
    @Excel(name = "反面")
    @ApiModelProperty(value = "反面")
    private String cardOpposite;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 类型（对接人:1，汽运发货人:2，汽运收货人:3） */
    @Excel(name = "类型")
    @ApiModelProperty(value = "标记")
    private Integer type;

    /** 所属公司 */
    @Excel(name = "所属公司")
    @ApiModelProperty(value = "所属公司")
    private String company;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 审批流程Id **/
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 租户Id **/
    @Excel(name = "租户Id")
    @ApiModelProperty(value = "租户Id")
    private String tenantId;

    /** 审批状态 **/
    @Excel(name = "审批状态")
    @ApiModelProperty(value = "审批状态")
    private String status;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @TableField(exist = false)
    private String userName;



}
