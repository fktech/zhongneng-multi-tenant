package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 地区对象 districts
 *
 * @author ljb
 * @date 2022-11-28
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "地区对象",description = "")
@TableName("districts")
public class Districts
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Integer id;

    private transient Integer value;

    public void setValue(){
        value = id;
    }

    public Integer getValue(){
        return id;
    }

    /** 上级编号 */
    @Excel(name = "上级编号")
    @ApiModelProperty(value = "上级编号")
    private Integer pid;

    /** 层级 */
    @Excel(name = "层级")
    @ApiModelProperty(value = "层级")
    private Integer deep;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 拼音 */
    @Excel(name = "拼音")
    @ApiModelProperty(value = "拼音")
    private String pinyin;

    /** 拼音缩写 */
    @Excel(name = "拼音缩写")
    @ApiModelProperty(value = "拼音缩写")
    private String pinyinShor;

    /** 扩展名 */
    @Excel(name = "扩展名")
    @ApiModelProperty(value = "扩展名")
    private String extName;

    private transient String label;

    public void setLabel(){
        label = extName;
    }

    public String getLabel(){
        return extName;
    }

    /** 操作人 */
    @Excel(name = "操作人")
    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty
    private transient List<Districts> children;

    private Date createTime;

    private Date updateTime;

    private transient String updateBy;

    private transient String userId;

    private transient String createBy;
}
