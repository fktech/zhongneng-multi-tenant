package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 角色部门查询DTO
 * @Date: 2023/2/24 23:14
 */
@Data
@ApiModel(value = "SysDeptRoleDTO对象", description = "角色部门查询DTO")
public class SysDeptRoleDTO implements Serializable {

    private static final long serialVersionUID = 5134468488459658729L;

    @ApiModelProperty("角色ID")
    private String roleId;


}
