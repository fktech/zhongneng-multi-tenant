package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 报销对象
 *
 * @author js
 * @date 2023-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "OaClaimRelationDTO对象", description = "报销对象")
public class OaClaimRelationDTO implements Serializable {

    private static final long serialVersionUID = -4087664702895881999L;

    /** 报销状态 0 未报销 1 报销中 2 已报销 */
    @Excel(name = "报销状态 0 未报销 1 报销中 2 已报销")
    @ApiModelProperty(value = "报销状态 0 未报销 1 报销中 2 已报销")
    private int claimState;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM")
    @ApiModelProperty(value = "申请时间")
    private Date applyTime;
}
