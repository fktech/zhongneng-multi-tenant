package com.business.system.domain.dto;

import com.business.system.domain.SysCompany;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "SysCompanyInstanceRelatedEditDTO对象", description = "企业信息与流程实例关联关系审批编辑对象")
public class SysCompanyInstanceRelatedEditDTO {
    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private SysCompany sysCompany;
}
