package com.business.system.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 终端企业关联对象 ba_enterprise_relevance
 *
 * @author ljb
 * @date 2023-03-01
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "终端企业关联对象",description = "")
@TableName("ba_enterprise_relevance")
public class BaEnterpriseRelevance
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 商品编号 */
    @Excel(name = "商品编号")
    @ApiModelProperty(value = "商品编号")
    private String goodsId;

    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;


    /** 指标 */
    @Excel(name = "指标")
    @ApiModelProperty(value = "指标")
    private String target;

    /** 月度用量 */
    @Excel(name = "月度用量")
    @ApiModelProperty(value = "月度用量")
    private BigDecimal monthlyConsumption;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;

    /** 父ID */
    @Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;

}
