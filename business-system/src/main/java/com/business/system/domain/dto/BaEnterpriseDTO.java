package com.business.system.domain.dto;

import com.business.system.domain.BaEnterprise;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaEnterpriseDTO对象", description = "终端对象")
public class BaEnterpriseDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 集团数 */
    private Integer group;

    /** 电厂 */
    private Integer powerPlant;

    /** 累计发运 */
    private BigDecimal checkCoalNum;

    /** 累计贸易 */
    private BigDecimal tradeAmount;

    /** 终端 */
    private List<BaEnterprise> baEnterprises;
}
