package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 公司信息对象 sys_company
 *
 * @author ruoyi
 * @date 2020-07-11
 */
@Data
public class SysCompany extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 公司名 */
    @Excel(name = "公司名")
    private String companyName;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 有效截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activeTime;

    /** 是否激活 */
    @Excel(name = "是否激活", readConverterExp = "1=激活,0=禁用")
    private Integer activeFlag;

    /** 模板ID */
    private String tempId;

    /**
     * 公司代码
     */
    @Excel(name = "公司代码")
    private Integer comCode;

    /**
     * 模板名
     */
    @Excel(name = "模板名称")
    @TableField(exist = false)
    private String tempName;

    /**
     * 行政区域编码
     */
    private String regionCodes;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** PC欢迎页 */
    @Excel(name = "PC欢迎页")
    private String annex;

    /** APP欢迎页 */
    @Excel(name = "APP欢迎页")
    private String smallAnnex;

    /** 法人 */
    @Excel(name = "法人")
    private String legalPerson;

    /** 企业编号 */
    @Excel(name = "企业编号")
    private String enterpriseId;

    /** 营业执照类型 */
    @Excel(name = "营业执照类型")
    private String businessLicenseType;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 营业执照 */
    @Excel(name = "营业执照")
    private String enterpriseCertificationInformation;

    /** 委托书 */
    @Excel(name = "委托书",cellType = Excel.ColumnType.IMAGE, height = 100)
    private String powerAttorney;

    /** 审批状态 */
    /*@Excel(name = "审批状态")*/
    private String state;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 税务图片 */
    @ApiModelProperty(value = "税务图片")
    private String taxationImg;

    /** 拒绝原因 */
    @ApiModelProperty(value = "拒绝原因")
    private String refuseReason;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 来源 */
    @Excel(name = "来源", readConverterExp = "1=手工录入,0=自主注册")
    @ApiModelProperty(value = "来源")
    private String source;

    /** 租户ID */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;

    /** 列表筛选标识 */
    @ApiModelProperty(value = "列表筛选标识")
    @TableField(exist = false)
    private String screen;

    /** 拒绝原因 */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "拒绝原因")
    @TableField(exist = false)
    private String rejectReason;

    /** 公司简称 */
    @ApiModelProperty(value = "公司简称")
    private String companyAbbreviation;

    /** 公司LOGO */
    @ApiModelProperty(value = "公司LOGO")
    private String companyLogo;
}
