package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 进度信息对象 ba_progress_information
 *
 * @author single
 * @date 2024-04-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "进度信息对象",description = "")
@TableName("ba_progress_information")
public class BaProgressInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 业务部意见 */
    @Excel(name = "业务部意见")
    @ApiModelProperty(value = "业务部意见")
    private String businessView;

    /** 资金方 */
    @Excel(name = "资金方")
    @ApiModelProperty(value = "资金方")
    private String funder;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 收费意见 */
    @Excel(name = "收费意见")
    @ApiModelProperty(value = "收费意见")
    private String chargeViwe;

    /** 预立项ID */
    @Excel(name = "预立项ID")
    @ApiModelProperty(value = "预立项ID")
    private String beforehandId;



}
