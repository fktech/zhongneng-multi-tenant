package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 合同启动投标信息运输对象 ba_bid_transport
 *
 * @author single
 * @date 2023-06-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "合同启动投标信息运输对象",description = "")
@TableName("ba_bid_transport")
public class BaBidTransport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 运输方式 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportType;

    /** 承运单位（托运公司） */
    @Excel(name = "承运单位", readConverterExp = "托=运公司")
    @ApiModelProperty(value = "承运单位")
    private String transportCompany;

    /** 承运单位名称（托运公司）*/
    @ApiModelProperty(value = "承运单位名称")
    @TableField(exist = false)
    private String transportCompanyName;

    /** 承运单位简称（托运公司）*/
    @ApiModelProperty(value = "承运单位简称")
    @TableField(exist = false)
    private String companyAbbreviation;

    /** 发站 */
    @Excel(name = "发站")
    @ApiModelProperty(value = "发站")
    private String origin;

    /** 到站 */
    @Excel(name = "到站")
    @ApiModelProperty(value = "到站")
    private String destination;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 是否垫付运费 */
    @Excel(name = "是否垫付运费")
    @ApiModelProperty(value = "是否垫付运费")
    private Integer isFreightPrice;

    /** 垫付运费(元) */
    @Excel(name = "垫付运费(元)")
    @ApiModelProperty(value = "垫付运费(元)")
    private BigDecimal freightPrice;

    /** 运费(元) */
    @Excel(name = "运费(元)")
    @ApiModelProperty(value = "运费(元)")
    private BigDecimal estimatedFreight;

    /** 杂费 */
    @Excel(name = "杂费")
    @ApiModelProperty(value = "杂费")
    private BigDecimal transportIncidentals;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /** 合同启动名称 */
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 杂费公司 */
    @Excel(name = "杂费公司")
    @ApiModelProperty(value = "杂费公司")
    private String incidentalsCompany;

    /** 运输对象 */
    @ApiModelProperty(value = "运输对象")
    @TableField(exist = false)
    private List<BaTransport> baTransports;

    @Excel(name = "累计发运量")
    @ApiModelProperty(value = "累计发运量")
    @TableField(exist = false)
    private BigDecimal saleNum;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;

}
