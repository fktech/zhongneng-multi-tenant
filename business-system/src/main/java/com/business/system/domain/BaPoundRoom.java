package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 磅房数据对象 ba_pound_room
 *
 * @author ljb
 * @date 2023-09-04
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "磅房数据对象",description = "")
@TableName("ba_pound_room")
public class BaPoundRoom extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 序号 */
    @Excel(name = "序号")
    @ApiModelProperty(value = "序号")
    @TableField(exist = false)
    private String serial;

    /** 立项名称 */
    //@Excel(name = "立项名称")
    @ApiModelProperty(value = "立项名称")
    private String projectName;

    /** 立项ID */
    //@Excel(name = "立项ID")
    @ApiModelProperty(value = "立项ID")
    private String projectId;

    /** 采购计划名称 */
   //@Excel(name = "计划名称(采购计划或是运输计划)")
    @ApiModelProperty(value = "采购计划名称")
    private String planName;

    /** 计划采购日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "计划采购日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划采购日期")
    private Date planTime;

    /** 运输方式 */
    //@Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportWay;

    /** 采购量 */
    //@Excel(name = "采购量")
    @ApiModelProperty(value = "采购量")
    private BigDecimal procurementVolume;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String plateNumber;

    /** 司机名称 */
    @Excel(name = "司机名称")
    @ApiModelProperty(value = "司机名称")
    private String driverUserName;

    /** 司机手机号 */
    @Excel(name = "司机手机号")
    @ApiModelProperty(value = "司机手机号")
    private String driverMobile;

    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货时间")
    private Date deliveryTime;

    /** 装车净重 */
    @Excel(name = "发货数量")
    @ApiModelProperty(value = "装车净重")
    private BigDecimal loadingWeight;

    /** 装车磅单 */
    //@Excel(name = "装车磅单", cellType = Excel.ColumnType.IMAGE, width = 16, height = 64)
    @ApiModelProperty(value = "装车磅单")
    private String loadingPound;

    /** 卸车净重 */
    @Excel(name = "收货数量")
    @ApiModelProperty(value = "卸车净重")
    private BigDecimal unloadWeight;

    /** 卸车磅单 */
    //@Excel(name = "卸车磅单", cellType = Excel.ColumnType.IMAGE, width = 16, height = 64)
    @ApiModelProperty(value = "卸车磅单")
    private String unloadPound;

    /** 收货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "收货时间")
    private Date receivingTime;

    /** 不含税单价 */
    //@Excel(name = "不含税单价")
    @ApiModelProperty(value = "不含税单价")
    private BigDecimal excludingPrice;

    /** 货值 */
    //@Excel(name = "货值")
    @ApiModelProperty(value = "货值")
    private BigDecimal goodsValue;

    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    //@Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 订单ID */
    //@Excel(name = "订单ID")
    @ApiModelProperty(value = "订单ID")
    private String orderId;

    /** 订单名称 */
    //@Excel(name = "订单名称")
    @ApiModelProperty(value = "订单名称")
    private String orderName;

    /** 货源 **/
    //@Excel(name = "货源")
    @ApiModelProperty(value = "货源")
    private String sourceGoods;

    /** 品名 **/
    @Excel(name = "品名")
    @ApiModelProperty(value = "品名")
    private String goodsName;

    /** 商品ID **/
    //@Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 入库状态 **/
    //@Excel(name = "业务状态")
    @ApiModelProperty(value = "入库状态")
    private String status;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String waybillCode;

    /**
     * 采购单ID
     */
    @ApiModelProperty(value = "采购单ID")
    private String purchaseId;

    /**
     * 合同启动
     */
    @ApiModelProperty(value = "合同启动")
    private String startId;

    /**
     * 运输ID
     */
    @ApiModelProperty(value = "运输ID")
    private String transportId;

    /** 不含税单价 */
    //@Excel(name = "含税单价")
    @ApiModelProperty(value = "含税单价")
    private BigDecimal includingPrice;

    @ApiModelProperty(value = "附件")
    private String annex;


    /**
     * 发货备注
     */
    @ApiModelProperty(value = "发货备注")
    private String shippingRemarks;

    /**
     * 收货备注
     */
    @ApiModelProperty(value = "收货备注")
    private String receivingRemarks;

    /**
     * 项目类型
     */
    @ApiModelProperty(value = "项目类型")
    @TableField(exist = false)
    private String projectType;

    /**
     * 供应商ID
     */
    @ApiModelProperty(value = "供应商ID")
    private String supplierId;

    /** 供应商名称 **/
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String supplierName;

    /** 收货判断 **/
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String takeOver;

    /**
     * 进厂状态
     */
    @ApiModelProperty(value = "进厂状态")
    private String enteringState;

    /** 采购计划 */
    @ApiModelProperty(value = "采购计划")
    @TableField(exist = false)
    private String planId;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
