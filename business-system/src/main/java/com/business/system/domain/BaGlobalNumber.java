package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 全局编号对象 ba_global_number
 *
 * @author ljb
 * @date 2024-06-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "全局编号对象",description = "")
@TableName("ba_global_number")
public class BaGlobalNumber extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 编号 */
    @Excel(name = "编号")
    @ApiModelProperty(value = "编号")
    private String code;

    /** 业务ID */
    @Excel(name = "业务ID")
    @ApiModelProperty(value = "业务ID")
    private String businessId;

    /** 层级(1:立项 2:合同启动 3:合同启动向下所有模块) */
    @Excel(name = "层级(1:立项 2:合同启动 3:合同启动向下所有模块)")
    @ApiModelProperty(value = "层级(1:立项 2:合同启动 3:合同启动向下所有模块)")
    private String hierarchy;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型（1:立项 2:合同启动 3:合同 4:运输 5:结算 6:付款 7:收款 8:认领收款 9:进项发票 10:销项发票） */
    @Excel(name = "类型（1:立项 2:合同启动 3:合同 4:运输 5:结算 6:付款 7:收款 8:认领收款 9:进项发票 10:销项发票）")
    @ApiModelProperty(value = "类型（1:立项 2:合同启动 3:合同 4:运输 5:结算 6:付款 7:收款 8:认领收款 9:进项发票 10:销项发票）")
    private String type;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /**
     * 立项
     */
    @ApiModelProperty(value = "立项")
    @TableField(exist = false)
    private BaProject baProject;

    /**
     * 合同启动
     */
    @ApiModelProperty(value = "合同启动")
    @TableField(exist = false)
    private List<BaContractStart> baContractStarts;

    /**
     * 合同
     */
    @ApiModelProperty(value = "合同")
    @TableField(exist = false)
    private List<BaContract> baContract;

    /**
     * 运输
     */
    @ApiModelProperty(value = "运输")
    @TableField(exist = false)
    private List<BaTransport> baTransports;

    /**
     * 结算
     */
    @ApiModelProperty(value = "结算")
    @TableField(exist = false)
    private List<BaSettlement> baSettlements;

    /**
     * 收款
     */
    @ApiModelProperty(value = "收款")
    @TableField(exist = false)
    private List<BaCollection> baCollections;

    /**
     * 认领收款
     */
    @ApiModelProperty(value = "认领收款")
    @TableField(exist = false)
    private List<BaClaimHistory> baClaimHistories;

    /**
     * 付款
     */
    @ApiModelProperty(value = "付款")
    @TableField(exist = false)
    private List<BaPayment> baPayments;

    /**
     * 发票
     */
    @ApiModelProperty(value = "发票")
    @TableField(exist = false)
    private List<BaInvoice> baInvoices;



}
