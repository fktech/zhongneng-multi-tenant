package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.BaBank;
import com.business.system.domain.BaBillingInformation;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 立项对象
 *
 * @author js
 * @date 2023-6-26
 */
@Data
@ApiModel(value="BaProjectVO对象",description="立项对象")
public class BaProjectVO
{
    @ApiModelProperty("ID")
    private String id;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "部门id")
    private Long deptId;

    @ApiModelProperty("发起人名称")
    private String userName;

    @ApiModelProperty("项目名称")
    private String name;

    @ApiModelProperty(value = "业务线")
    private String businessLines;

    @ApiModelProperty(value = "合同数量")
    private BigDecimal tonnage;

    @ApiModelProperty(value = "已发煤量")
    private BigDecimal checkCoalNum;

    @ApiModelProperty(value = "火运")
    private String train;

    @ApiModelProperty(value = "汽运")
    private String automobile;
}
