package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.system.domain.BaDaily;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserDailyDTO", description = "用户日报")
public class UserDailyDTO implements Serializable {

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    private String userName;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    /**
     * 用户岗位
     */
    @ApiModelProperty(value = "用户岗位")
    private String postName;

    /**
     * 用户部门
     */
    @ApiModelProperty(value = "用户部门")
    private String deptName;

    /**
     * 日报信息
     */
    @ApiModelProperty(value = "日报信息")
    private List<BaDaily> dailyList;

    /** 关键词 */
    @ApiModelProperty(value = "关键词")
    private String keyWord;

    /** 用户ID */
    @ApiModelProperty(value = "关键词")
    private Long userId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
