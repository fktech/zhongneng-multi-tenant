package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 审批任务查询对象
 * @date: 2022/12/11 13:39
 */
@Data
@ApiModel(value = "CountRuntimeTaskDTO对象", description = "审批任务查询对象")
public class CountRuntimeTaskDTO extends BaseDTO{

    private static final long serialVersionUID = 2715419589898089846L;

    @ApiModelProperty("受理人id")
    private String userId;

    @ApiModelProperty("过滤条件，任务类型")
    private String filterCondition;
}
