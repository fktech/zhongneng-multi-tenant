package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 付款凭证对象 ba_payment_voucher
 *
 * @author single
 * @date 2023-07-15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "付款凭证对象",description = "")
@TableName("ba_payment_voucher")
public class BaPaymentVoucher extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 付款凭证附件 */
    //@Excel(name = "付款凭证附件", cellType = Excel.ColumnType.IMAGE, height = 100)
    @ApiModelProperty(value = "付款凭证附件")
    private String annex;

    /** 凭证备注 */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "凭证备注")
    private String voucherRemark;

    /** 付款单位 */
    @Excel(name = "付款单位")
    @ApiModelProperty(value = "付款单位")
    private String paymentCompany;

    /** 付款账号 */
    @Excel(name = "付款账号")
    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    /** 收款单位 */
    @Excel(name = "收款单位")
    @ApiModelProperty(value = "收款单位")
    private String collectionCompany;

    /** 收款账号 */
    @Excel(name = "收款账号")
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "付款日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "付款日期")
    private Date paymentDate;

    /** 确认付款金额 */
    @Excel(name = "确认付款金额")
    @ApiModelProperty(value = "确认付款金额")
    private BigDecimal paymentAmount;

    /** 可付款金额 */
    //@Excel(name = "可付款金额")
    @ApiModelProperty(value = "可付款金额")
    private BigDecimal payableAmount;

    /** 付款ID */
    //@Excel(name = "付款ID")
    @ApiModelProperty(value = "付款ID")
    private String paymentId;

    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    //@Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;



}
