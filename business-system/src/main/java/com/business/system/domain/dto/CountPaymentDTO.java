package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CountPaymentDTO对象", description = "付款DTO对象")
public class CountPaymentDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 立项类型 **/
    @ApiModelProperty(value = "立项类型")
    private String projectType;

    /**
     * 经理归属部门ID
     */
    @ApiModelProperty(value = "经理归属部门ID")
    private Long deptId;

    /**
     * 归属公司
     */
    @ApiModelProperty(value = "归属公司")
    private String belongCompanyId;

    /**
     * 兼职部门Id
     */
    @ApiModelProperty(value = "兼职部门Id")
    private String partDeptId;

    /** 业务经理归属部门id */
    @ApiModelProperty(value = "业务经理归属部门id")
    private String manageDeptId;

    /** 仓库id */
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 租户id */
    @ApiModelProperty(value = "租户id")
    private String tenantId;



}
