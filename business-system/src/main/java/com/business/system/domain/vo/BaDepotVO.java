package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 仓库
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaDepotVO {

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depotName;
}
