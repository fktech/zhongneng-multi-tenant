package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: js
 * @Description: 报销统计对象
 * @date: 2023/12/12 17:51
 */
@Data
@ApiModel(value="OaClaimStatVO对象",description="报销统计对象")
public class OaClaimStatVO implements Serializable {

    private static final long serialVersionUID = -2643237482208628875L;

    /** 待报销金额 */
//    @ApiModelProperty(value = "待报销金额")
    private String toBeClaim;

    /** 待报销金额 */
//    @ApiModelProperty(value = "待报销金额")
    private String doingClaim;

    /** 已报销金额 */
//    @ApiModelProperty(value = "已报销金额")
    private String endClaim;
}
