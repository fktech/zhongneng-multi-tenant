package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 库存对象 ba_material_stock
 *
 * @author single
 * @date 2023-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "库存对象",description = "")
@TableName("ba_material_stock")
public class BaMaterialStock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 产品id */
    @Excel(name = "产品id")
    @ApiModelProperty(value = "产品id")
    private String goodsId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    @ApiModelProperty(value = "产品名称")
    private String goodsName;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    @ApiModelProperty(value = "仓库名称")
    private String depName;

    /** 当前库存 */
    @Excel(name = "当前库存")
    @ApiModelProperty(value = "当前库存")
    private BigDecimal currentNumber;

    /** 初始库存 */
    @Excel(name = "初始库存")
    @ApiModelProperty(value = "初始库存")
    private BigDecimal startNumber;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    @ApiModelProperty(value = "供应商编号")
    private String supplierId;

    /** 批次 */
    @Excel(name = "批次")
    @ApiModelProperty(value = "批次")
    private String batch;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品")
    private BaGoods baGoods;

    /** 批次号 */
    @Excel(name = "批次号")
    @ApiModelProperty(value = "批次号")
    @TableField(exist = false)
    private String  batchNum;

    /** 产品编码 */
    @Excel(name = "产品编码")
    @ApiModelProperty(value = "产品编码")
    @TableField(exist = false)
    private String goodsCode;

    /** 项目id */
    @Excel(name = "项目id")
    @ApiModelProperty(value = "项目id")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 库存总量（吨） */
    @Excel(name = "库存总量（吨）")
    @ApiModelProperty(value = "库存总量（吨）")
    private BigDecimal inventoryTotal;

    /** 库存总值 */
    @Excel(name = "库存总值")
    @ApiModelProperty(value = "库存总值")
    private BigDecimal inventoryValue;

    /** 库存均价 */
    @Excel(name = "库存均价")
    @ApiModelProperty(value = "库存均价")
    private BigDecimal inventoryAverage;

    /** 平均热值 */
    @Excel(name = "平均热值")
    @ApiModelProperty(value = "平均热值")
    private BigDecimal averageCalorific;

    /** 总热值 */
    @Excel(name = "总热值")
    @ApiModelProperty(value = "总热值")
    private BigDecimal allAverageCalorific;

    /**
     * 入库、出库、盘点启动流程保存业务经理人ID
     */
    public static final String BUSINESS_MANAGER_ID = "business_manager_id";

    /** 库位ID */
    @Excel(name = "库位ID")
    @ApiModelProperty(value = "库位ID")
    private String locationId;

    /** 库位名称 */
    @Excel(name = "库位名称")
    @ApiModelProperty(value = "库位名称")
    private String locationName;

    /** 库位集合 */
    @ApiModelProperty(value = "库位集合")
    @TableField(exist = false)
    private List<BaDepotLocation> baDepotLocationList;

    /** 入库货值累计 */
    @Excel(name = "入库货值累计")
    @ApiModelProperty(value = "入库货值累计")
    private BigDecimal intoDepotCargoValue;

    /** 出库货值累计 */
    @Excel(name = "出库货值累计")
    @ApiModelProperty(value = "出库货值累计")
    private BigDecimal outputDepotCargoValue;

    /** 盘点货值累计 */
    @Excel(name = "盘点货值累计")
    @ApiModelProperty(value = "盘点货值累计")
    private BigDecimal checkedCargoValue;

    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depotName;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品编码")
    private String code;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品类型")
    private String goodsType;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型（出库/入库） */
    @Excel(name = "类型", readConverterExp = "出库/入库")
    @TableField(exist = false)
    private String depotType;

    /** 年（出库/入库）时间 */
    @TableField(exist = false)
    private String yearDepotTime;

    /** 月（出库/入库）时间 */
    @TableField(exist = false)
    private String monthDepotTime;
}
