package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 监控点在线状态对象 ba_online_ment
 *
 * @author single
 * @date 2023-07-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "监控点在线状态对象",description = "")
@TableName("ba_online_ment")
public class BaOnlineMent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private String id;

    /** 设备型号 */
    @Excel(name = "设备型号")
    @ApiModelProperty(value = "设备型号")
    private String deviceType;

    /** 设备唯一编码 */
    @Excel(name = "设备唯一编码")
    @ApiModelProperty(value = "设备唯一编码")
    private String deviceIndexCode;

    /** 区域编码 */
    @Excel(name = "区域编码")
    @ApiModelProperty(value = "区域编码")
    private String regionIndexCode;

    /** 采集时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "采集时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "采集时间")
    private Date collectTime;

    /** 区域名字 */
    @Excel(name = "区域名字")
    @ApiModelProperty(value = "区域名字")
    private String regionName;

    /** 资源唯一编码 */
    @Excel(name = "资源唯一编码")
    @ApiModelProperty(value = "资源唯一编码")
    private String indexCode;

    /** 设备名称 */
    @Excel(name = "设备名称")
    @ApiModelProperty(value = "设备名称")
    private String cn;

    /** 码流传输协议，0：UDP，1：TCP */
    @Excel(name = "码流传输协议，0：UDP，1：TCP")
    @ApiModelProperty(value = "码流传输协议，0：UDP，1：TCP")
    private String treatyType;

    /** 厂商，hikvision-海康，dahua-大华 */
    @Excel(name = "厂商，hikvision-海康，dahua-大华")
    @ApiModelProperty(value = "厂商，hikvision-海康，dahua-大华")
    private String manufacturer;

    /** ip地址，监控点无此值 */
    @Excel(name = "ip地址，监控点无此值")
    @ApiModelProperty(value = "ip地址，监控点无此值")
    private String ip;

    /** 端口，监控点无此值 */
    @Excel(name = "端口，监控点无此值")
    @ApiModelProperty(value = "端口，监控点无此值")
    private String port;

    /** 在线状态，0离线，1在线 */
    @Excel(name = "在线状态，0离线，1在线")
    @ApiModelProperty(value = "在线状态，0离线，1在线")
    private String online;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标识 */
    @Excel(name = "标识")
    @ApiModelProperty(value = "标识")
    private String flag;

    /** 掉线原因 */
    @Excel(name = "掉线原因")
    @ApiModelProperty(value = "掉线原因")
    private String disconnectReason;

    /** 处理办法 */
    @Excel(name = "处理办法")
    @ApiModelProperty(value = "处理办法")
    private String treatment;

    /** 预计恢复时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预计恢复时间")
    private Date estimatedRecoveryTime;



}
