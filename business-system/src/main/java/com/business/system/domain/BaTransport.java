package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 运输对象 ba_transport
 *
 * @author ljb
 * @date 2022-12-13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "运输对象",description = "")
@TableName("ba_transport")
public class BaTransport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    @TableField(exist = false)
    private String orderNum;

    /** 运输日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "运输日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "运输日期")
    private Date transportDate;

    /** 运输批次 */
    @Excel(name = "运输批次")
    @ApiModelProperty(value = "运输批次")
    private String transportBatch;

    /** 运输类型 */
    @Excel(name = "运输类型")
    @ApiModelProperty(value = "运输类型")
    private String transportType;

    /** 承运单位（托运公司） */
    @Excel(name = "承运单位", readConverterExp = "托运公司")
    @ApiModelProperty(value = "承运单位")
    private String transportCompany;

    @ApiModelProperty(value = "承运单位")
    @TableField(exist = false)
    private String transportCompanyName;

    /** 收货单位 */
    @Excel(name = "收货单位")
    @ApiModelProperty(value = "收货单位")
    private String receiveCompany;

    @ApiModelProperty(value = "收货单位")
    @TableField(exist = false)
    private String receiveId;

    @ApiModelProperty(value = "终端企业名称")
    @TableField(exist = false)
    private String enterpriseName;

    /** 始发地(发站) */
    @Excel(name = "始发地(发站)")
    @ApiModelProperty(value = "始发地(发站)")
    private String origin;

    @ApiModelProperty(value = "始发地(发站)名称")
    @TableField(exist = false)
    private String originName;

    /** 目的地(到站) */
    @Excel(name = "目的地(到站)")
    @ApiModelProperty(value = "目的地(到站)")
    private String destination;

    @ApiModelProperty(value = "目的地(到站)")
    @TableField(exist = false)
    private String destinationName;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 产品名称 */
    @Excel(name = "产品名称")
    @ApiModelProperty(value = "产品名称")
    private String goodsName;

    /** 产品ID */
    @Excel(name = "产品ID")
    @ApiModelProperty(value = "产品ID")
    private String goodsId;

    /** 发货量 */
    @Excel(name = "发货量")
    @ApiModelProperty(value = "发货量")
    private Long deliveryCount;

    /** 预计运费单价(元/吨) */
    @Excel(name = "预计运费单价(元/吨)")
    @ApiModelProperty(value = "预计运费单价(元/吨)")
    private BigDecimal freightPrice;

    /** 预计运费(元) */
    @Excel(name = "预计运费(元)")
    @ApiModelProperty(value = "预计运费(元)")
    private BigDecimal estimatedFreight;

    /** 装车数量(吨) */
    @Excel(name = "装车数量(吨)")
    @ApiModelProperty(value = "装车数量(吨)")
    private Long shippedQuantity;

    /** 车次(铁路) */
    @Excel(name = "车次(铁路)")
    @ApiModelProperty(value = "车次(铁路)")
    private String trainNumber;

    /** 发车备注（铁路） */
    @Excel(name = "发车备注", readConverterExp = "铁=路")
    @ApiModelProperty(value = "发车备注")
    private String carRemark;

    /** 车数 */
    @Excel(name = "车数")
    @ApiModelProperty(value = "车数")
    private String carCount;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 其他附件 */
    @Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 验收数量 */
    @Excel(name = "验收数量")
    @ApiModelProperty(value = "验收数量")
    private Long acceptanceQuantity;

    /** 验收水分 */
    @Excel(name = "验收水分")
    @ApiModelProperty(value = "验收水分")
    private BigDecimal acceptanceMoisture;

    /** 验收灰分 */
    @Excel(name = "验收灰分")
    @ApiModelProperty(value = "验收灰分")
    private BigDecimal acceptanceAsh;

    /** 验收挥发分 */
    @Excel(name = "验收挥发分")
    @ApiModelProperty(value = "验收挥发分")
    private BigDecimal acceptanceVolatile;

    /** 验收固定碳 */
    @Excel(name = "验收固定碳")
    @ApiModelProperty(value = "验收固定碳")
    private BigDecimal acceptanceCarbon;

    /** 验收硫分 */
    @Excel(name = "验收硫分")
    @ApiModelProperty(value = "验收硫分")
    private BigDecimal acceptanceSulfur;

    /** 验收热值 */
    @Excel(name = "验收热值")
    @ApiModelProperty(value = "验收热值")
    private BigDecimal acceptanceCalorific;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 计划发货量 */
    @Excel(name = "计划发货量")
    @ApiModelProperty(value = "计划发货量")
    private BigDecimal saleNum;

    /** 运输实控人 */
    @Excel(name = "运输实控人")
    @ApiModelProperty(value = "运输实控人")
    private String actualController;

    @ApiModelProperty(value = "运输实控人名称")
    @TableField(exist = false)
    private String actualName;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 业务经理归属部门id */
    @ApiModelProperty(value = "业务经理归属部门id")
    @TableField(exist = false)
    private Long manageDeptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 运输状态 */
    @Excel(name = "运输状态")
    @ApiModelProperty(value = "运输状态")
    private String transportStatus;

    /** 待发状态 */
    @Excel(name = "待发状态")
    @ApiModelProperty(value = "待发状态")
    private String confirmStatus;

    /** 审批流程ID */
    @Excel(name = "审批流程ID")
    @ApiModelProperty(value = "审批流程ID")
    private String flowId;

    /** 品类 */
    @Excel(name = "品类")
    @ApiModelProperty(value = "品类")
    private String goodsType;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /** 货转凭证 */
    @Excel(name = "货转凭证")
    @ApiModelProperty(value = "货转凭证")
    private String transferVoucher;

    /** 付款状态 */
    @ApiModelProperty(value = "付款状态")
    private String paymentStatus;

    /** 选中状态 */
    @ApiModelProperty(value = "选中状态")
    private String pitchOn;

    /**
     * 发货信息
     */
    @ApiModelProperty(value = "发货信息")
    @TableField(exist = false)
    private BaCheck delivery;

    /**
     * 收货信息
     */
    @ApiModelProperty(value = "收货信息")
    @TableField(exist = false)
    private BaCheck receipt;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 到货附件
     */
    @TableField(exist = false)
    private BaCheck baCheckSend;

    /**
     * 验收附件
     */
    @TableField(exist = false)
    private BaCheck baCheckReceive;

    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 合同启动线路 */
    @Excel(name = "合同启动线路")
    @ApiModelProperty(value = "合同启动线路")
    @TableField(exist = false)
    private String businessLines;

    /** 来源 */
    @ApiModelProperty(value = "来源")
    private String source;

    /** 计划资金 **/
    @ApiModelProperty(value = "计划资金")
    private String plannedFunding;

    /** 关联ID */
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 判断 */
    @ApiModelProperty(value = "判断")
    @TableField(exist = false)
    private String judge;

    /** 完成 */
    @ApiModelProperty(value = "完成")
    @TableField(exist = false)
    private String complete;

    /** 合同启动简称 **/
    @Excel(name = "合同启动简称")
    @TableField(exist = false)
    private String startShorter;

    /** 供应商名称 **/
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String supplierName;

    /** 汽运类型 */
    @Excel(name = "汽运类型")
    @ApiModelProperty(value = "汽运类型")
    private String carType;

    /** 货源ID */
    @Excel(name = "货源ID")
    @ApiModelProperty(value = "货源ID")
    private String automobileId;

    /** 货源类型 */
    @Excel(name = "货源类型")
    @ApiModelProperty(value = "货源类型")
    private String automobileType;

    /** 装车量 */
    @Excel(name = "装车量")
    @ApiModelProperty(value = "装车量")
    @TableField(exist = false)
    private BigDecimal loadingCapacity;

    /** 卸车量 */
    @Excel(name = "卸车量")
    @ApiModelProperty(value = "卸车量")
    @TableField(exist = false)
    private BigDecimal unloadingVolume;

    /** 昕科 */
    @ApiModelProperty(value = "昕科运单")
    @TableField(exist = false)
    private List<BaShippingOrder> baShippingOrders;

    /** 中储 */
    @ApiModelProperty(value = "中储运单")
    @TableField(exist = false)
    private List<BaShippingOrderCmst> baShippingOrderCmsts;

    /** 矿发煤量 */
    @Excel(name = "矿发煤量")
    @ApiModelProperty(value = "矿发煤量")
    @TableField(exist = false)
    private BigDecimal checkCoalNum;

    /** 出场净重 */
    @Excel(name = "出场净重")
    @ApiModelProperty(value = "出场净重")
    @TableField(exist = false)
    private BigDecimal ccjweight;

    /** 确认发货重量 */
    @Excel(name = "确认发货重量")
    @ApiModelProperty(value = "确认发货重量")
    @TableField(exist = false)
    private BigDecimal confirmedDeliveryWeight;

    /** 磅房数据装车重量 */
    @Excel(name = "磅房数据装车重量")
    @ApiModelProperty(value = "磅房数据装车重量")
    @TableField(exist = false)
    private BigDecimal loadingWeight;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 归属公司id */
    @ApiModelProperty(value = "归属公司id")
    @TableField(exist = false)
    private Long belongCompanyId;

    /** 兼职部门Id */
    @ApiModelProperty(value = "兼职部门Id")
    @TableField(exist = false)
    private Long partDeptId;

    /** 租户ID **/
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    @ApiModelProperty(value = "类型")
    @TableField(exist = false)
    private String type;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 变更标识 */
    @Excel(name = "变更标识")
    @ApiModelProperty(value = "变更标识")
    private String approveFlag;

    /** 变更人 */
    @Excel(name = "变更人")
    @ApiModelProperty(value = "变更人")
    private String changeBy;

    /** 变更时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "变更时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "变更时间")
    private Date changeTime;

    /** 父ID */
    @Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;

    /**
     * 发货数质量
     */
    @TableField(exist = false)
    private List<BaQualityReport> baQualityReportDeliveryList;

    /**
     * 验收数质量
     */
    @TableField(exist = false)
    private List<BaQualityReport> baQualityReportReceiptList;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /**
     * 全局编号
     */
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
