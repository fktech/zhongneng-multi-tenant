package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: js
 * @Description: 任务查询对象
 * @date: 2022/12/6 13:61
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TaskQueryDTO对象", description = "任务查询对象")
public class TaskQueryDTO extends BaseDTO {

    private static final long serialVersionUID = -8966817671548276056L;

    @ApiModelProperty("受理人id")
    private String userId;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("过滤条件 vars[key]=value&vars[key1]=value1")
    private String filterCondition;
}
