package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 订单对象 ba_order
 *
 * @author ljb
 * @date 2022-12-09
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "订单对象",description = "")
@TableName("ba_order")
public class BaOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单名称 */
    @Excel(name = "订单名称")
    @ApiModelProperty(value = "订单名称")
    private String orderName;

    /** 销售合同 */
    @Excel(name = "销售合同")
    @ApiModelProperty(value = "销售合同")
    private String companyId;

    /** 采购合同 */
    @Excel(name = "采购合同")
    @ApiModelProperty(value = "采购合同")
    private String purchaseCompanyId;

    @ApiModelProperty(value = "关联合同名称")
    @TableField(exist = false)
    private String companyName;

    /** 订单类型 */
    @Excel(name = "订单类型")
    @ApiModelProperty(value = "订单类型")
    private String orderType;

    /** 签订单位 */
    @Excel(name = "签订单位")
    @ApiModelProperty(value = "签订单位")
    private String signedBy;

    /** 订单金额（元） */
    @Excel(name = "订单金额", readConverterExp = "元=")
    @ApiModelProperty(value = "订单金额")
    private BigDecimal amount;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 完成备注 */
    @Excel(name = "完成备注")
    @ApiModelProperty(value = "完成备注")
    private String finishRemarks;

    /** 完成附件 */
    @Excel(name = "完成附件")
    @ApiModelProperty(value = "完成附件")
    private String finishAnnex;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 商品ID */
    @Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    /** 指标参数 */
    @Excel(name = "指标参数")
    @ApiModelProperty(value = "指标参数")
    private String parameterIndex;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用煤单位ID（收货单位） */
    @Excel(name = "用煤单位ID", readConverterExp = "收货单位")
    @ApiModelProperty(value = "用煤单位ID")
    private String enterpriseId;

    /** 用煤单位名称（收货单位） */
    @Excel(name = "用煤单位名称", readConverterExp = "收货单位")
    @ApiModelProperty(value = "用煤单位名称")
    private String enterpriseName;

    /** 交货地点 */
    @Excel(name = "交货地点")
    @ApiModelProperty(value = "交货地点")
    private String deliveryPlace;

    /** 交货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "交货时间")
    private Date deliveryTime;

    /** 交货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "交货日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "交货日期")
    private Date deliveryEndTime;

    /** 运输方式 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportType;

    /** 运输状态 */
    @Excel(name = "运输状态")
    @ApiModelProperty(value = "运输状态")
    private Integer transportState;

    /** 订单业务状态 */
    @Excel(name = "订单业务状态")
    @ApiModelProperty(value = "订单业务状态")
    private String orderState;

    /** 确认状态 */
    @Excel(name = "确认状态")
    @ApiModelProperty(value = "确认状态")
    private String confirmStatus;

    /** 计划销售数量 */
    @Excel(name = "计划销售数量")
    @ApiModelProperty(value = "计划销售数量")
    private Integer saleNum;

    @ApiModelProperty(value = "合同金额(元)")
    @TableField(exist = false)
    private BigDecimal  contractAmount;

    @ApiModelProperty(value = "合同总量")
    @TableField(exist = false)
    private Long contractTotal;

    @ApiModelProperty(value = "合同用煤单位名称")
    @TableField(exist = false)
    private String contractEnterpriseName;

    @ApiModelProperty(value = "合同用煤单位id")
    @TableField(exist = false)
    private String contractEnterpriseId;

    @ApiModelProperty(value = "合同供应商名称")
    @TableField(exist = false)
    private String contractSupplierName;

    @ApiModelProperty(value = "合同供应商id")
    @TableField(exist = false)
    private String contractSupplierId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 待回款
     */
    @Excel(name = "待回款")
    @ApiModelProperty(value = "待回款")
    private BigDecimal stayCollected;

    /** 订单应收（元） */
    @Excel(name = "订单应收", readConverterExp = "元=")
    @ApiModelProperty(value = "订单应收")
    private BigDecimal orderReceive;

    /** 订单应付（元） */
    @Excel(name = "订单应付", readConverterExp = "元=")
    @ApiModelProperty(value = "订单应付")
    private BigDecimal orderPay;

    @ApiModelProperty(value = "销售结算标识")
    private String settlementMark;

    @ApiModelProperty(value = "采购结算标识")
    private String settlementSign;

    @ApiModelProperty(value = "运输结算标识")
    private String transportMark;
    /**
     * 类型（1:应付金额，2:实付金额）
     */
    @ApiModelProperty(value = "类型（1:应付金额，2:实付金额）")
    @TableField(exist = false)
    private String settlementDetailType;

    /**
     * 累计采购量
     */
    @ApiModelProperty(value = "累计采购量")
    @TableField(exist = false)
    private String purchaseVolume;

    /**
     * 累计付款金额
     */
    @ApiModelProperty(value = "累计付款金额")
    @TableField(exist = false)
    private String purchasePayment;

    /**
     * 计划发运数量
     */
    @ApiModelProperty(value = "计划发运数量")
    @TableField(exist = false)
    private String deliveryCount;

    /** 年化利率 **/
    @ApiModelProperty(value = "年化利率")
    @TableField(exist = false)
    private BigDecimal annualization;

    /** 已付金额（元） */
    @ApiModelProperty(value = "已付金额")
    @TableField(exist = false)
    private BigDecimal amountPaid;

    /** 已收金额（元） */
    @ApiModelProperty(value = "已收金额")
    @TableField(exist = false)
    private BigDecimal collectionAmountPaid;

    /** 待付金额（元） */
    @ApiModelProperty(value = "待付金额")
    @TableField(exist = false)
    private BigDecimal amountToBePaid;

    /** 已付比例 */
    @ApiModelProperty(value = "已付比例")
    @TableField(exist = false)
    private BigDecimal amountProportion;

    /** 服务周期 **/
    @ApiModelProperty(value = "服务周期")
    @TableField(exist = false)
    private Integer serviceCycle;

    /** 预计利息 **/
    @ApiModelProperty(value = "预计利息")
    @TableField(exist = false)
    private BigDecimal estimatedInterest;

    /** 托盘公司 **/
    @ApiModelProperty(value = "托盘公司")
    @TableField(exist = false)
    private String palletCompany;

    /** 订单付款状态 */
    @ApiModelProperty(value = "订单付款状态")
    @TableField(exist = false)
    private String orderPaymentState;

    /** 查询历史销售订单 */
    @TableField(exist = false)
    private String saleOrderType;

    /** 发运计划发货量 */
    @TableField(exist = false)
    @ApiModelProperty(value = "发运计划发货量")
    private String transportSaleNum;

    /** 合同类型 */
    @TableField(exist = false)
    @ApiModelProperty(value = "合同类型")
    private String contractType;

    /** 发货信息 **/
    @ApiModelProperty(value = "发货信息")
    @TableField(exist = false)
    private BaTransport baTransport;
}
