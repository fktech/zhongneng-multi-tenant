package com.business.system.domain.dto.json;

import lombok.Data;

/**
 * @Author:js
 * @Description: 用户信息
 * @Date:Created in 2022/12/1 16:10
 */
@Data
public class UserInfo {
  private String id;
  private String name;
  private String nickName;
  private String type;
  private String sex;
  private Boolean selected;
  private String department;
}
