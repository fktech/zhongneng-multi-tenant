package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系对象
 * @date: 2022/12/1
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ba_process_instance_related")
public class BaProcessInstanceRelated extends BaseEntity {

    private static final long serialVersionUID = 6541508894146019902L;
    /**
     * 主键
     */
    private String id;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 业务数据
     */
    private String businessData;

    /**
     * 审核类型
     */
    private String approveType;

    /**
     * 审核结果
     */
    private String approveResult;

    /**
     * 原因
     */
    private String reason;

    /**
     * 状态
     */
    private String state;

    /**
     * 标记
     */
    private Integer flag;

    /**
     * 发起人
     */
    private String createBy;

    /**
     * 过滤未开发数据
     */
    @TableField(exist = false)
    private List<String> filterConditionNotEqual;

    /**
     * 查询指定类型数据
     */
    @TableField(exist = false)
    private List<String> filterConditionEqual;
}
