package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author: js
 * @Description: 流程对象
 * @date: 2022-12-2 15:28:28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProcessDefinitionRelatedSaveDTO对象", description = "流程对象")
public class BaProcessDefinitionRelatedSaveDTO implements Serializable {

    private static final long serialVersionUID = -4810512325565649409L;

    @ApiModelProperty("流程分类")
    @NotBlank(message = "流程类型不能为空")
    private String businessType;

    @ApiModelProperty("流程定义id")
    @NotBlank(message = "流程id不能为空")
    private String processDefinitionId;

    @ApiModelProperty("流程key")
    private String processKey;

    @ApiModelProperty("流程定义名称")
    @NotBlank(message = "流程名称不能为空")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "流程版本")
    private String customVersion;

    @ApiModelProperty(value = "流程模板ID")
    private String templateId;


    @ApiModelProperty("租户ID")
    private String tenantId;
}
