package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * please change here
 *
 * @author songkai
 * @date 2023/4
 */
@Data
@ApiModel(value = "OngoingcontractsDto对象", description = "智慧驾驶舱进行中合同对象")
public class OngoingcontractsDto implements Serializable {
    private static final long serialVersionUID = -2865020511591145789L;
    @ApiModelProperty("合同名称")
    private String contractsName;
    @ApiModelProperty("状态")
    private String state;
    @ApiModelProperty("发运量")
    private Double sendNum;
    @ApiModelProperty("收款金额")
    private Double collectionAmount;
    @ApiModelProperty("事业部")
    private String dept;
    @ApiModelProperty("启动日期")
    private Date startTime;


}
