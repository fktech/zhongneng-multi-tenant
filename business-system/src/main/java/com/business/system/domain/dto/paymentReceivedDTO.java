package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "paymentReceivedDTO对象", description = "收付款统计")
public class paymentReceivedDTO {

    /** 首付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "首付款日期")
    private Date downPayment;

    /** 付款总数 */
    @ApiModelProperty(value = "付款总数")
    private BigDecimal paymentTotal;

    /** 最后收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "最后收款日期")
    private Date finalPayment;

    /** 最后认领收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "最后认领收款日期")
    private Date finalClaimPayment;

    /** 收款总数 */
    @ApiModelProperty(value = "收款总数")
    private BigDecimal collectionTotal;

    /** 占资金额 */
    @ApiModelProperty(value = "占资金额")
    private BigDecimal actualCapitalOccupation;
}
