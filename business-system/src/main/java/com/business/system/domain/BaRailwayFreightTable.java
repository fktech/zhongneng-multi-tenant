package com.business.system.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 铁路运费对象 ba_railway_freight_table
 *
 * @author ljb
 * @date 2022-11-29
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "铁路运费对象",description = "")
@TableName("ba_railway_freight_table")
public class BaRailwayFreightTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 站台ID */
    @Excel(name = "站台ID")
    @ApiModelProperty(value = "站台ID")
    private String platformId;

    /** 到站ID */
    @Excel(name = "到站ID")
    @ApiModelProperty(value = "到站ID")
    private String platformLevel;

    /** 运费 */
    @Excel(name = "运费")
    @ApiModelProperty(value = "运费")
    private BigDecimal freight;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 始发站名称 **/
    @ApiModelProperty(value = "始发站名称")
    @TableField(exist = false)
    private String platformIdName;

    /** 终点站名称 **/
    @ApiModelProperty(value = "到站ID")
    @TableField(exist = false)
    private String platformLevelName;

    /** 线路名称 **/
    @ApiModelProperty(value = "线路名称")
    @TableField(exist = false)
    private String lineName;

    /** 地铁费用 */
    @Excel(name = "地铁费用")
    @ApiModelProperty(value = "地铁费用")
    private BigDecimal metroCosts;

    /** 备注 **/
    @ApiModelProperty(value = "备注")
    @TableField(exist = false)
    private String remarks;



}
