package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 集团公司对象 ba_company_group
 *
 * @author ljb
 * @date 2023-03-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "集团公司对象",description = "")
@TableName("ba_company_group")
public class BaCompanyGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 公司名称 */
    @Excel(name = "公司名称")
    @ApiModelProperty(value = "公司名称")
    private String companyName;

    /** 公司简称 */
    @Excel(name = "公司简称")
    @ApiModelProperty(value = "公司简称")
    private String companyAbbreviation;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private Long sort;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名 */
    @ApiModelProperty(value = "用户名")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 累计合同额 */
    @ApiModelProperty(value = "累计合同额")
    @TableField(exist = false)
    private BigDecimal contractAmount;

    /** 累计贸易额 */
    @ApiModelProperty(value = "累计贸易额")
    @TableField(exist = false)
    private BigDecimal tradeAmount;

    /** 累计运输量 */
    @ApiModelProperty(value = "累计运输量")
    @TableField(exist = false)
    private BigDecimal checkCoalNum;

    /** 筛选条件 */
    @ApiModelProperty(value = "筛选条件")
    @TableField(exist = false)
    private String screen;


}
