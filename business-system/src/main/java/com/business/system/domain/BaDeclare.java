package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 计划申报对象 ba_declare
 *
 * @author single
 * @date 2023-03-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "计划申报对象",description = "")
@TableName("ba_declare")
public class BaDeclare extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 计划申报信息详情对象列表 */
    @ApiModelProperty(value = "计划申报信息详情对象列表")
    @TableField(exist = false)
    private List<BaDeclareDetail> baDeclareDetailList;

    /**
     * 发起人名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /** 合同启动名称 */
    @Excel(name = "合同启动名称")
    @ApiModelProperty(value = "合同启动名称")
    private String startName;
    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 计划发货量 */
    @TableField(exist = false)
    @ApiModelProperty(value = "计划发货量")
    private BigDecimal deliveryCount;

    /** 计划资金 */
    @TableField(exist = false)
    @ApiModelProperty(value = "计划资金")
    private BigDecimal plannedAmount;

    /**
     * 本月标识
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "本月标识")
    private String thisMonth;
    /**
     * 本月标识
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "移动端查询的临时字段")
    private String keyWords;
    /** 计划时间 */
    @Excel(name = "计划时间")
    @ApiModelProperty(value = "计划时间")
    private String planTime;

    /** 运杂费计划 */
    @Excel(name = "运杂费计划")
    @ApiModelProperty(value = "运杂费计划")
    private BigDecimal freightMoney;

    /** 贷款计划 */
    @Excel(name = "货款计划")
    @ApiModelProperty(value = "货款计划")
    private BigDecimal loanMoney;

    /** 回款计划 */
    @Excel(name = "回款计划")
    @ApiModelProperty(value = "回款计划")
    private BigDecimal paymentMoney;

    /**
     * 来源标识
     */
    @Excel(name = "来源标识")
    @ApiModelProperty(value = "来源标识")
    private Integer source;

    /**
     * 本周标识
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "本周标识")
    private String thisWeek;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

    /** 附件 */
    //@Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

}
