package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ClockCountDTO对象", description = "打卡统计对象")
public class ClockCountDTO implements Serializable {

    private static final long serialVersionUID = 9127532221382240134L;

    @ApiModelProperty(value = "应出勤人数")
    private Long peopleTotal;

    @ApiModelProperty(value = "实际出勤人数")
    private Long peopleReality;

    @ApiModelProperty(value = "迟到人数")
    private Long beLate;

    @ApiModelProperty(value = "早退人数")
    private Long leaveEarly;

    @ApiModelProperty(value = "缺卡人数")
    private Long LackCards;
}
