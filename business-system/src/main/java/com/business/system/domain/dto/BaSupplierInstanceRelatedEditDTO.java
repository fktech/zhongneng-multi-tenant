package com.business.system.domain.dto;

import com.business.system.domain.BaSupplier;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 供应商业务与流程实例关联关系审批编辑对象
 * @date: 2023/1/10
 */
@Data
@ApiModel(value = "BaSupplierInstanceRelatedEditDTO对象", description = "供应商业务与流程实例关联关系审批编辑对象")
public class BaSupplierInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private BaSupplier baSupplier;
}
