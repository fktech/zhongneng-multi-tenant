package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0
 * @Author: js
 * @Description: 立项查询DTO
 * @Date: 2023/2/24 23:14
 */
@Data
@ApiModel(value = "BaProjectDTO对象", description = "立项查询对象")
public class BaProjectDTO implements Serializable {

    private static final long serialVersionUID = -2865020511591145690L;

    @ApiModelProperty("发起人ID")
    private String startUserId;

    @ApiModelProperty("ID")
    private String id;

    @ApiModelProperty("项目名称")
    private String projectName;

    @ApiModelProperty("终端企业")
    private String enterpriseName;

    @ApiModelProperty("发起人")
    private String userName;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty(value = "验收煤量")
    private BigDecimal checkCoalNum;

    @ApiModelProperty(value = "收款金额")
    private BigDecimal applyCollectionAmount;

    @ApiModelProperty(value = "实付金额(元)")
    private BigDecimal actualPaymentAmount;

    @ApiModelProperty(value = "贸易金额(元)")
    private BigDecimal tradeAmount;

    @ApiModelProperty(value = "收益率")
    private BigDecimal yield;

    @ApiModelProperty(value = "立项类型")
    private String projectType;

    @ApiModelProperty(value = "合同数量")
    private BigDecimal tonnage;

    @ApiModelProperty(value = "业务线")
    private String businessLines;
}
