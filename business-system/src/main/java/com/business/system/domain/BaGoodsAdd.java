package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * 商品附加对象 ba_goods_add
 *
 * @author ljb
 * @date 2022-12-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "商品附加对象",description = "")
@TableName("ba_goods_add")
public class BaGoodsAdd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 信息名称 */
    @Excel(name = "信息名称")
    @ApiModelProperty(value = "信息名称")
    private String informationName;

    /** 信息值 */
    @Excel(name = "信息值")
    @ApiModelProperty(value = "信息值")
    private String informationValue;

    /** 商品ID */
    @Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 颜色 */
    @Excel(name = "颜色")
    @ApiModelProperty(value = "颜色")
    private String color;

    /** 等级 */
    @Excel(name = "等级")
    @ApiModelProperty(value = "等级")
    private String grade;

    /** 型号 */
    @Excel(name = "型号")
    @ApiModelProperty(value = "型号")
    private String model;

    /** 规格 */
    @Excel(name = "规格")
    @ApiModelProperty(value = "规格")
    private String standard;

    /** 数据拼接 **/
    @ApiModelProperty(value = "数据拼接")
    @TableField(exist = false)
    private List<String> montage;



}
