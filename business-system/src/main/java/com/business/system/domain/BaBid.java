package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 投标管理对象 ba_bid
 *
 * @author ljb
 * @date 2022-12-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "投标管理对象",description = "")
@TableName("ba_bid")
public class BaBid extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 投标名称 */
    @Excel(name = "投标名称")
    @ApiModelProperty(value = "投标名称")
    private String name;

    /** 项目编号 */
    @Excel(name = "项目编号")
    @ApiModelProperty(value = "项目编号")
    private String projectId;

    /** 项目名称 */
    @Excel(name = "项目名称")
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 投标类型 */
    @Excel(name = "投标类型")
    @ApiModelProperty(value = "投标类型")
    private String bidType;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 供应商编号 */
    @Excel(name = "供应商编号")
    @ApiModelProperty(value = "供应商编号")
    private String supplierId;

    /** 用煤企业名称 */
    @Excel(name = "用煤企业名称")
    @ApiModelProperty(value = "用煤企业名称")
    private String enterpriseName;

    /** 用煤企业编号 */
    @Excel(name = "用煤企业编号")
    @ApiModelProperty(value = "用煤企业编号")
    private String enterpriseId;

    /** 投标发起人 */
    @Excel(name = "投标发起人")
    @ApiModelProperty(value = "投标发起人")
    private String sponsor;

    /** 投标网名称 */
    @Excel(name = "投标网名称")
    @ApiModelProperty(value = "投标网名称")
    private String bidWebsite;

    /** 投标网链接地址 */
    @Excel(name = "投标网链接地址")
    @ApiModelProperty(value = "投标网链接地址")
    private String websiteAddress;

    /** 投标申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "投标申请时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
    @ApiModelProperty(value = "投标申请时间")
    private Date startTime;

    /** 结果反馈时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结果反馈时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "结果反馈时间")
    private Date endTime;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 投标附件图片 */
    @Excel(name = "投标附件图片")
    @ApiModelProperty(value = "投标附件图片")
    private String bidAnnex;

    /** 投标反馈情况 */
    @Excel(name = "投标反馈情况")
    @ApiModelProperty(value = "投标反馈情况")
    private String feedback;

    /** 投标反馈附件 */
    @Excel(name = "投标反馈附件")
    @ApiModelProperty(value = "投标反馈附件")
    private String feedbackAnnex;

    /** 结果反馈情况 */
    @Excel(name = "结果反馈情况")
    @ApiModelProperty(value = "结果反馈情况")
    private String result;

    /** 结果反馈附件 */
    @Excel(name = "结果反馈附件")
    @ApiModelProperty(value = "结果反馈附件")
    private String resultAnnex;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 始发地 */
    @Excel(name = "始发地")
    @ApiModelProperty(value = "始发地")
    private String origin;

    /** 目的地 */
    @Excel(name = "目的地")
    @ApiModelProperty(value = "目的地")
    private String destination;

    /** 投标时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "投标时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "投标时间")
    private Date bidTime;

    /** 参数指标 */
    @Excel(name = "参数指标")
    @ApiModelProperty(value = "参数指标")
    private String parameterIndex;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @TableField(exist = false)
    private String userName;


    /** 投标网站 */
    @TableField(exist = false)
    private String bidAddress;

    /** 投标状态 **/
    @Excel(name = "投标状态")
    @ApiModelProperty(value = "投标状态")
    private String bidState;

    /** 商品ID */
    @Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    /** 单价 **/
    @Excel(name = "单价")
    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    /** 投标总量 **/
    @Excel(name = "投标总量")
    @ApiModelProperty(value = "投标总量")
    private Integer bidTotal;

    /** 审批流程Id **/
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;




}
