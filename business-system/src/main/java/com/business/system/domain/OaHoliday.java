package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 节假日对象 oa_holiday
 *
 * @author ljb
 * @date 2023-11-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "节假日对象",description = "")
@TableName("oa_holiday")
public class OaHoliday extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 返回值 */
    @Excel(name = "返回值")
    @ApiModelProperty(value = "返回值")
    private String holiday;

    /** 节假日中文名称 */
    @Excel(name = "节假日中文名称")
    @ApiModelProperty(value = "节假日中文名称")
    private String name;

    /** 薪资倍数 */
    @Excel(name = "薪资倍数")
    @ApiModelProperty(value = "薪资倍数")
    private String wage;

    /** 节假日期 */
    @Excel(name = "节假日期")
    @ApiModelProperty(value = "节假日期")
    private String holidaysDate;

    /** 星期几 */
    @Excel(name = "星期几")
    @ApiModelProperty(value = "星期几")
    private String week;

    /** 所属年 */
    @Excel(name = "所属年")
    @ApiModelProperty(value = "所属年")
    private String affiliationYear;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 类型(日报、周报、月报) */
    @Excel(name = "类型(日报、周报、月报)")
    @ApiModelProperty(value = "类型(日报、周报、月报)")
    private String type;

    /** true表示放完假后调休，false表示先调休再放假 */
    @Excel(name = "true表示放完假后调休，false表示先调休再放假")
    @ApiModelProperty(value = "true表示放完假后调休，false表示先调休再放假")
    private String after;

    /** 表示调休的节假日 */
    @Excel(name = "表示调休的节假日")
    @ApiModelProperty(value = "表示调休的节假日")
    private String target;



}
