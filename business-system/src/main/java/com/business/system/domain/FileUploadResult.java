package com.business.system.domain;

import lombok.Data;

/**
 * 法律工具类
 *
 * @author jiahe
 * @date 2020.7
 */
@Data
public class FileUploadResult {

    // 文件唯一标识
    private String uid;
    // 文件路径
    private String url;
    // 文件名
    private String name;
    // 状态有：uploading done error removed
    private String status;
    // 服务端响应内容，如：'{"status": "success"}'
    private String response;

}
