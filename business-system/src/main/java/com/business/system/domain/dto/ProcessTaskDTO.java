package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Map;

/**
 * @Author: js
 * @Description: 办理任务对象
 * @date: 2022/12/8 15:14
 */
@Data
@ApiModel(value = "ProcessTaskDTO对象", description = "办理任务对象")
public class ProcessTaskDTO implements Serializable {

    private static final long serialVersionUID = 6768363876510701786L;

    @ApiModelProperty("任务id")
    private String taskId;

    @ApiModelProperty("处理结果，可选值：通过AGREE、拒绝DISAGREE、驳回REFUSE")
    @NotBlank(message = "办理必填")
    private String opinion;

    @ApiModelProperty("一般为表单变量")
    private Map<String, Object> variables;

    @ApiModelProperty("原因")
    @Size(max = 84,message = "审批原因超长，最大长度84")
    private String reason;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("流程节点")
    private String taskDefinitionKey;

    @ApiModelProperty("业务key")
    private String businessId;

    @ApiModelProperty("审核类型")
    private String approveType;

    @ApiModelProperty(value = "判断是编辑审批还是变更审批")
    private String approveFlag;

    @ApiModelProperty("退回节点id")
    private String rollbackId;

}
