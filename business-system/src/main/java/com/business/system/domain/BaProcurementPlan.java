package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 采购计划对象 ba_procurement_plan
 *
 * @author ljb
 * @date 2023-08-31
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "采购计划对象",description = "")
@TableName("ba_procurement_plan")
public class BaProcurementPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 计划名称 */
    @Excel(name = "计划名称")
    @ApiModelProperty(value = "计划名称")
    private String planName;

    /** 项目id */
    @Excel(name = "项目id")
    @ApiModelProperty(value = "项目id")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 计划采购日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "计划采购日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "计划采购日期")
    private Date planTime;

    /** 总货值 */
    @Excel(name = "总货值")
    @ApiModelProperty(value = "总货值")
    private BigDecimal goodsTotal;

    /** 运输方式 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportWay;

    /** 计划用款 */
    @Excel(name = "计划用款")
    @ApiModelProperty(value = "计划用款")
    private BigDecimal plannedFunds;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 仓库名称 */
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 仓库负责人 */
    @Excel(name = "仓库负责人")
    @ApiModelProperty(value = "仓库负责人")
    private String depotHead;

    /** 打印备注 */
    @Excel(name = "打印备注")
    @ApiModelProperty(value = "打印备注")
    private String printNotes;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 采购单 */
    @ApiModelProperty(value = "采购单")
    @TableField(exist = false)
    private List<BaPurchaseOrder> baPurchaseOrders;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /** 业务归属公司 */
    @Excel(name = "业务归属公司")
    @ApiModelProperty(value = "业务归属公司")
    private String belongCompanyId;

    /** 业务归属公司名称 */
    @Excel(name = "业务归属公司名称")
    @ApiModelProperty(value = "业务归属公司名称")
    @TableField(exist = false)
    private String belongCompanyName;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 合同信息 **/
    @ApiModelProperty(value = "合同信息")
    @TableField(exist = false)
    private List<BaContract> baContractList;

    /** 入库信息 */
    @ApiModelProperty(value = "入库信息")
    @TableField(exist = false)
    private List<BaDepotHead> baDepotHeadList;

    /** 租户ID **/
    @TableField(exist = false)
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

}
