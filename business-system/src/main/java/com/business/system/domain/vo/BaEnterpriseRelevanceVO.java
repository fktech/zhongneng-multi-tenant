package com.business.system.domain.vo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 终端企业指标信息对比
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaEnterpriseRelevanceVO对象",description="终端企业指标信息对比")
public class BaEnterpriseRelevanceVO {
    /** 指标信息标题 */
    @ApiModelProperty(value = "指标信息标题")
    private JSONObject label;

    /** 指标信息值 */
    @ApiModelProperty(value = "指标信息值")
    private JSONObject value;
}
