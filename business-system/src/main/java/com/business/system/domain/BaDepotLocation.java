package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 仓库库位信息对象 ba_depot_location
 *
 * @author single
 * @date 2023-09-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "仓库库位信息对象",description = "")
@TableName("ba_depot_location")
public class BaDepotLocation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 库位编码 */
    @Excel(name = "库位编码")
    @ApiModelProperty(value = "库位编码")
    private String code;

    /** 库位名称 */
    @Excel(name = "库位名称")
    @ApiModelProperty(value = "库位名称")
    private String name;

    /** 库位面积 */
    @Excel(name = "库位面积")
    @ApiModelProperty(value = "库位面积")
    private BigDecimal depotArea;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 是否开启 */
    @Excel(name = "是否开启")
    @ApiModelProperty(value = "是否开启")
    private Integer status;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;

    /** 库存总量（吨） */
    @ApiModelProperty(value = "库存总量（吨）")
    @TableField(exist = false)
    private BigDecimal inventoryTotal;

}
