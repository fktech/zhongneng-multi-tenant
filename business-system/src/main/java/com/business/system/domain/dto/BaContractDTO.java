package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 合同对象
 *
 * @author js
 * @date 2023-2-2
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaContractDTO对象", description = "合同对象")
public class BaContractDTO implements Serializable {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 销售合同ID */
    @ApiModelProperty(value = "销售合同ID")
    private String contractId;

    /** 采购合同ID */
    @ApiModelProperty(value = "采购合同ID")
    private String purchaseContractId;

    /** 运输状态（1:运输中 2:已验收） */
    @ApiModelProperty(value = "运输状态")
    private String transportStatus;
}
