package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 数质量报告对象 ba_quality_report
 *
 * @author single
 * @date 2023-07-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "数质量报告对象",description = "")
@TableName("ba_quality_report")
public class BaQualityReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 流转类型 */
    @Excel(name = "流转类型")
    @ApiModelProperty(value = "流转类型")
    private String circulationType;

    /** 附件类型 */
    @Excel(name = "附件类型")
    @ApiModelProperty(value = "附件类型")
    private String attachmentType;

    /** 运输方式 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportWay;

    /** 发运ID */
    @Excel(name = "发运ID")
    @ApiModelProperty(value = "发运ID")
    private String transportId;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String starId;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 其他附件 */
    @Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 发运信息 **/
    @ApiModelProperty(value = "发运信息")
    @TableField(exist = false)
    private BaTransport baTransport;

    /** 计划提报日期 **/
    @ApiModelProperty(value = "计划提报日期")
    @TableField(exist = false)
    private String transportDate;

    /** 列车号 **/
    @ApiModelProperty(value = "列车号")
    @TableField(exist = false)
    private String trainNumber;

    /** 历史发运信息 **/
    @ApiModelProperty(value = "历史发运信息")
    @TableField(exist = false)
    private BaHiTransport baHiTransport;

}
