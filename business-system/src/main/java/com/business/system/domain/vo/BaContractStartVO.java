package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="BaContractStartVO对象",description="移动端合同启动面板列表详情")
public class BaContractStartVO {

    //合同启动ID
    @ApiModelProperty(value = "合同启动ID")
    private String id;
    //合同启动名称
    @ApiModelProperty(value = "合同启动名称")
    private String name;
    //归属事业部
    @ApiModelProperty(value = "所属事业部")
    private String partDeptName;
    //业务线
    @ApiModelProperty(value = "业务线")
    private String businessLines;
    //品名
    @ApiModelProperty(value = "商品名称")
    private String goodsName;
    //销售单价
    @ApiModelProperty(value = "单价")
    private BigDecimal price;
    //数量
    @ApiModelProperty(value = "吨数")
    private BigDecimal tonnage;
    //已发运数量
    @ApiModelProperty(value = "发运量")
    private BigDecimal checkCoalNum;
    //未发运数量
    @ApiModelProperty(value = "未发运量")
    private BigDecimal notCheckCoalNum;
    //已到厂数量
    @ApiModelProperty(value = "进场吨数")
    private BigDecimal entryTonnage;
    //未到厂数量
    @ApiModelProperty(value = "未到厂数量")
    private BigDecimal notEntryTonnage;
    //已付货款
    @ApiModelProperty(value = "货款")
    private BigDecimal paymentForGoods;
    //已付运杂费
    @ApiModelProperty(value = "运输杂费")
    private BigDecimal transportIncidentals;
    //累计付款
    @ApiModelProperty(value = "累计付款")
    private BigDecimal paymentAmount;
    //已收货款
    @ApiModelProperty(value = "已收款(万元)")
    private BigDecimal collectionAmount;
    //目前占资
    @ApiModelProperty(value = "占资(万元)")
    private BigDecimal capitalOccupation;
    //上游供货商
    @ApiModelProperty(value = "上游公司名称")
    private String upstreamCompanyName;
    //下游客户
    @ApiModelProperty(value = "终端企业名称")
    private String enterpriseName;
    //上游合同/补充协议
    @ApiModelProperty(value = "上游")
    private String upstream;
    //下游合同/补充协议
    @ApiModelProperty(value = "下游")
    private String downstream;
    //上游物流凭证
    @ApiModelProperty(value = "物流凭证")
    private String logisticsVoucher;
    //进场数质量
    @ApiModelProperty(value = "进场数质量")
    private String numberQuality;
    //货转
    @ApiModelProperty(value = "货转")
    private String freightTransfer;
    //已付货款
    @ApiModelProperty(value = "已付货款")
    private String paid;
    //已收货款
    @ApiModelProperty(value = "已收货款")
    private String receivedPayment;
    //已付运费
    @ApiModelProperty(value = "已付运费")
    private String shippingFee;
    //上游结算单
    @ApiModelProperty(value = "上游结算单")
    private String upstreamSettlement;
    //上游结算单名称
    @ApiModelProperty(value = "上游结算单名称")
    private List<String> upstreamSettlementName;
    //下游结算单
    @ApiModelProperty(value = "下游结算单")
    private String downstreamSettlement;
    //下游结算单名称
    @ApiModelProperty(value = "下游结算单名称")
    private List<String> downstreamSettlementName;
    //进项发票
    @ApiModelProperty(value = "进项发票")
    private String inputInvoice;
    /** 开票金额、收票金额 */
    @ApiModelProperty(value = "开票金额、收票金额")
    private BigDecimal inputTaxPriceTotal;
    /** 数量 */
    @ApiModelProperty(value = "数量")
    private BigDecimal inputAmountTotal;
    //销项发票
    @ApiModelProperty(value = "销项发票")
    private String outputInvoice;
    /** 开票金额、收票金额 */
    @ApiModelProperty(value = "开票金额、收票金额")
    private BigDecimal taxPriceTotal;
    /** 数量 */
    @ApiModelProperty(value = "数量")
    private BigDecimal amountTotal;
    //运杂费发票
    @ApiModelProperty(value = "运杂费发票")
    private String freightInvoice;
    /** 运杂费税价合计 */
    @ApiModelProperty(value = "开票金额、收票金额")
    private BigDecimal freightPriceTotal;
    /** 运杂费数量 */
    @ApiModelProperty(value = "数量")
    private BigDecimal freightAmountTotal;
    //运输发票
    @ApiModelProperty(value = "运输发票")
    private String transportInvoice;
    //上游最终付款时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上游最终付款时间")
    private Date upstreamPaymentTime;
    //下游最终回款时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游最终回款时间")
    private Date downstreamPaymentTime;
    //上游供应商
    @ApiModelProperty(value = "上游供应商")
    private String upstreamSupplier;
    //下游客户
    @ApiModelProperty(value = "下游客户")
    private String downstreamCustomer;
    //上游附件名称
    @ApiModelProperty(value = "上游附件名称")
    private List<String> upstreamAnnex;
    //下游附件名称
    @ApiModelProperty(value = "下游附件名称")
    private List<String> downstreamAnnex;
    /** 完结 **/
    @ApiModelProperty(value = "完结")
    private Long completion;
    /** 问题标识 */
     @ApiModelProperty(value ="问题标识")
     private String problemMark;
}
