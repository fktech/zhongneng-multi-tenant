package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 洗煤配煤出入库记录对象 ba_wash_head
 *
 * @author single
 * @date 2024-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "洗煤配煤出入库记录对象",description = "")
@TableName("ba_wash_head")
public class BaWashHead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 类型（出库/入库）入库:1  出库:2 */
    @Excel(name = "类型", readConverterExp = "出=库/入库")
    @ApiModelProperty(value = "类型")
    private Long type;

    /** 洗煤 1 配煤 2 */
    @Excel(name = "洗煤 1 配煤 2")
    @ApiModelProperty(value = "洗煤 1 配煤 2")
    private String subType;

    /** 经办人 */
    @Excel(name = "经办人")
    @ApiModelProperty(value = "经办人")
    private String handledBy;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 附件名称 */
    @Excel(name = "附件名称")
    @ApiModelProperty(value = "附件名称")
    private String fileName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String status;

    /** 关联洗煤入库 */
    @Excel(name = "关联洗煤入库")
    @ApiModelProperty(value = "关联洗煤入库")
    private String linkNumber;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型(1:洗煤中 2:已完成) */
    @Excel(name = "类型(1:洗煤中 2:已完成)")
    @ApiModelProperty(value = "类型(1:洗煤中 2:已完成)")
    private String types;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 出入库业务状态 1:未入库或未出库 2:已入库或已出库 */
    @Excel(name = "出入库业务状态 1:未入库或未出库 2:已入库或已出库")
    @ApiModelProperty(value = "出入库业务状态 1:未入库或未出库 2:已入库或已出库")
    private String headStatus;

    /** 总量 */
    @Excel(name = "总量")
    @ApiModelProperty(value = "总量")
    private BigDecimal totals;

    /** 库存总量 */
    @Excel(name = "库存总量")
    @ApiModelProperty(value = "库存总量")
    private BigDecimal inventoryTotal;

    /** 流程发起状态 */
    @Excel(name = "流程发起状态")
    @ApiModelProperty(value = "流程发起状态")
    private String workState;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /** 洗煤时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "洗煤时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "洗煤时间")
    private Date washTime;

    /** 洗煤总量 */
    @Excel(name = "洗煤总量")
    @ApiModelProperty(value = "洗煤总量")
    private BigDecimal washTotal;

    /** 洗出总量 */
    @Excel(name = "洗出总量")
    @ApiModelProperty(value = "洗出总量")
    private BigDecimal outTotal;

    /** 损耗量 */
    @Excel(name = "损耗量")
    @ApiModelProperty(value = "损耗量")
    private BigDecimal wastage;

    /** 配煤方案ID */
    @Excel(name = "配煤方案ID")
    @ApiModelProperty(value = "配煤方案ID")
    private String blendId;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "洗煤出库清单")
    @TableField(exist = false)
    private List<BaWashInventory> baWashInventories;

    /** 仓库名称 **/
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private Date beginTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @TableField(exist = false)
    @ApiModelProperty(value = "结束时间")
    private Date stopTime;

    @TableField(exist = false)
    private SysUser user;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    @TableField(exist = false)
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 洗煤单编码 */
    @Excel(name = "洗煤单编码")
    @ApiModelProperty(value = "洗煤单编码")
    private String idCode;

    @ApiModelProperty(value = "洗煤入库清单")
    @TableField(exist = false)
    private List<BaWashInventory> baWashIntoInventories;

    /** 配煤方案名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "配煤方案名称")
    private String blendName;

    /** 出库对应入库对象 */
    @TableField(exist = false)
    @ApiModelProperty(value = "出库对应入库对象")
    private BaWashHead intoBaWashHead;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;
}
