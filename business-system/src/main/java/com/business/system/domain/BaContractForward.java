package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 合同结转对象 ba_contract_forward
 *
 * @author ljb
 * @date 2023-03-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "合同结转对象",description = "")
@TableName("ba_contract_forward")
public class BaContractForward extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 合同启动编号 */
    @Excel(name = "合同启动编号")
    @ApiModelProperty(value = "合同启动编号")
    private String startId;


    @ApiModelProperty(value = "合同启动对象")
    @TableField(exist = false)
    private BaContractStart baContractStart;

    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String name;

    /** 结转金额 */
    @Excel(name = "结转金额")
    @ApiModelProperty(value = "结转金额")
    private BigDecimal amount;

    /** 结转方式 */
    @Excel(name = "结转方式")
    @ApiModelProperty(value = "结转方式")
    private Long way;

    /** 结转来源 */
    @Excel(name = "结转来源")
    @ApiModelProperty(value = "结转来源")
    private String source;

    /** 结转来源对象 */
    @ApiModelProperty(value = "结转来源对象")
    @TableField(exist = false)
    private BaContractStart contractStart;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String appendix;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程ID */
    @Excel(name = "审批流程ID")
    @ApiModelProperty(value = "审批流程ID")
    private String flowId;

    /** 合同启动线路 */
    @Excel(name = "合同启动线路")
    @ApiModelProperty(value = "合同启动线路")
    @TableField(exist = false)
    private String businessLines;

    /** 结转来源线路 */
    @Excel(name = "结转来源线路")
    @ApiModelProperty(value = "结转来源线路")
    @TableField(exist = false)
    private String sourceLines;

    /** 当前结算差额 */
    @Excel(name = "当前结算差额")
    @ApiModelProperty(value = "当前结算差额")
    private BigDecimal currentSettlementDifference;

    /** 当前结算差额 */
    @Excel(name = "结转源差额")
    @ApiModelProperty(value = "结转源差额")
    private BigDecimal settlementDifference;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /** 租户ID **/
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;

}
