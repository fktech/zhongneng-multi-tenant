package com.business.system.domain.dto;

import com.business.system.domain.BaProject;
import com.business.system.domain.BaProjectBeforehand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 立项管理业务与流程实例关联关系审批编辑对象
 * @date: 2023/4/18
 */
@Data
@ApiModel(value = "BaProjectInstanceRelatedEditDTO对象", description = "立项管理业务与流程实例关联关系审批编辑对象")
public class BaProjectInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private BaProject baProject;

    @ApiModelProperty("预立项")
    private BaProjectBeforehand baProjectBeforehand;
}
