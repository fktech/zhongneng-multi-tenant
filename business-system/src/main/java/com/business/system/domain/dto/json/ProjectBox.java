package com.business.system.domain.dto.json;

import com.business.system.domain.BaContractStart;
import com.business.system.domain.BaProject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProjectBox", description = "项目下拉框")
public class ProjectBox implements Serializable {

    private static final long serialVersionUID = 6281016059931188895L;

    /** 类型对应键值 */
    @ApiModelProperty(value = "类型对应键值")
    private String value;

    /** 合同启动类型 */
    @ApiModelProperty(value = "项目类型")
    private String label;

    /** 合同启动 */
    @ApiModelProperty(value = "项目")
    private List<BaProject> children;
}
