package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 进厂确认单对象 ba_service_voucher
 *
 * @author single
 * @date 2023-09-19
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "进厂确认单对象",description = "")
@TableName("ba_service_voucher")
public class BaServiceVoucher extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 采购计划ID */
    @Excel(name = "采购计划ID")
    @ApiModelProperty(value = "采购计划ID")
    private String planId;

    /** 采购计划名称 */
    @ApiModelProperty(value = "采购计划名称")
    @TableField(exist = false)
    private String planName;

    /** 批次名称 */
    @Excel(name = "批次名称")
    @ApiModelProperty(value = "批次名称")
    private String batchName;

    /** 批次进厂车数 */
    @Excel(name = "批次进厂车数")
    @ApiModelProperty(value = "批次进厂车数")
    private Long batchCars;

    /** 批次进厂数量 */
    @Excel(name = "批次进厂数量")
    @ApiModelProperty(value = "批次进厂数量")
    private BigDecimal batchNumber;

    /** 货值 */
    @Excel(name = "货值")
    @ApiModelProperty(value = "货值")
    private BigDecimal goodsValue;

    /** 需付款 */
    @Excel(name = "需付款")
    @ApiModelProperty(value = "需付款")
    private BigDecimal paymentRequired;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户ID")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 进厂确认单附表 */
    @ApiModelProperty(value = "进厂确认单附表")
    @TableField(exist = false)
    private List<BaServiceVoucherRecord> baServiceVoucherRecords;

    /** 磅房ID */
    @ApiModelProperty(value = "磅房ID")
    @TableField(exist = false)
    private String roomId;

    /** 打印备注 */
    @Excel(name = "打印备注")
    @ApiModelProperty(value = "打印备注")
    private String printNotes;

    /** 仓库名称 */
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 仓库管理员 */
    @ApiModelProperty(value = "仓库管理员")
    @TableField(exist = false)
    private String depotHead;

    /** 立项编号 */
    @ApiModelProperty(value = "立项编号")
    @TableField(exist = false)
    private String projectId;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;
}
