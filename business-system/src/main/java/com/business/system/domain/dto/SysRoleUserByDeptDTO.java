package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 根据用户部门ID集合查询负责部门角色用户列表
 * @Date: 2023/6/6 10:54
 */
@Data
@ApiModel(value = "SysRoleUserByDeptDTO对象", description = "根据用户部门ID集合查询负责部门角色用户列表")
public class SysRoleUserByDeptDTO implements Serializable {

    private static final long serialVersionUID = -1166347315782308025L;

    @ApiModelProperty("部门ID")
    private String deptId;
}
