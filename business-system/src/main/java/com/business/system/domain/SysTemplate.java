package com.business.system.domain;

import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.List;

/**
 * 权限模板对象 sys_template
 *
 * @author ruoyi
 * @date 2020-07-09
 */
@Data
public class SysTemplate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 模板名称 */
    @Excel(name = "模板名称")
    private String name;

    private List<Integer> menuIds;

}
