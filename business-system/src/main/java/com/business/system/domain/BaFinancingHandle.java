package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 融资办理对象 ba_financing_handle
 *
 * @author single
 * @date 2023-05-03
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "融资办理对象",description = "")
@TableName("ba_financing_handle")
public class BaFinancingHandle extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 融资审批编号 */
    @Excel(name = "融资审批编号")
    @ApiModelProperty(value = "融资审批编号")
    private String financingId;

    /** 类型（1：放款 2：还款） */
    @Excel(name = "类型", readConverterExp = "1=：放款,2=：还款")
    @ApiModelProperty(value = "类型")
    private Long type;

    /** 收款/还款金额 */
    @Excel(name = "收款/还款金额")
    @ApiModelProperty(value = "收款/还款金额")
    private BigDecimal amount;

    /** 放款/收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "放款/收款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "放款/收款日期")
    private Date time;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 收款/放款凭证 */
    @Excel(name = "收款/放款凭证")
    @ApiModelProperty(value = "收款/放款凭证")
    private String voucher;

    /** 应还本息 */
    @Excel(name = "应还本息")
    @ApiModelProperty(value = "应还本息")
    private BigDecimal principalRepayable;

    /** 利息 */
    @Excel(name = "利息")
    @ApiModelProperty(value = "利息")
    private BigDecimal interest;

    /** 审批状态 */
    @Excel(name = "审批状态")
    @ApiModelProperty(value = "审批状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String deptName;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;


    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;



}
