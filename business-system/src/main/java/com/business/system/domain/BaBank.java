package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 开户行对象 ba_bank
 *
 * @author ljb
 * @date 2022-12-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "开户行对象",description = "")
@TableName("ba_bank")
public class BaBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 开户名 */
    @Excel(name = "开户名")
    @ApiModelProperty(value = "开户名")
    private String openName;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 开户号 */
    @Excel(name = "开户号")
    @ApiModelProperty(value = "开户号")
    private String account;

    /** 开户号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "开户号")
    private String accountName;

    /** 银行联号 */
    @Excel(name = "银行联号")
    @ApiModelProperty(value = "银行联号")
    private String interBankNumber;

    /** 开户地址 */
    @Excel(name = "开户地址")
    @ApiModelProperty(value = "开户地址")
    private String bankAddress;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "关联编号")
    private String relationId;

    /** 单位名称 */
    @Excel(name = "单位编号")
    @ApiModelProperty(value = "单位编号")
    private String companyName;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 单位类型 */
    @Excel(name = "单位类型")
    @ApiModelProperty(value = "单位类型")
    private String companyType;

    /** 签署方 */
    @Excel(name = "签署方")
    @ApiModelProperty(value = "签署方")
    private String partyName;

    /** 单位名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "单位名称")
    private String company;

    /** 默认状态 */
    @ApiModelProperty(value = "默认状态")
    private String mark;

    /** 父ID */
    @Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;
}
