package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 日报关注对象 ba_daily_follow
 *
 * @author ljb
 * @date 2023-11-08
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "日报关注对象",description = "")
@TableName("ba_daily_follow")
public class BaDailyFollow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 被关注人ID */
    @Excel(name = "被关注人ID")
    @ApiModelProperty(value = "被关注人ID")
    private Long followedId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /**
     * 关联数组
     */
    @ApiModelProperty(value = "关联数组")
    @TableField(exist = false)
    private String followed;

    /**
     * 被关注人id
     */
    @Excel(name = "被关注人id")
    @ApiModelProperty(value = "被关注人id")
    private Long deptId;

    /** 关键词 */
    @ApiModelProperty(value = "关键词")
    @TableField(exist = false)
    private String keyWord;

    /** 用户对象 */
    @ApiModelProperty(value = "用户对象")
    @TableField(exist = false)
    private SysUser user;





}
