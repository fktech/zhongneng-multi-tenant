package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 市场动态更新对象 ba_renew
 *
 * @author ljb
 * @date 2023-03-22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "市场动态更新对象",description = "")
@TableName("ba_renew")
public class BaRenew extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 商品主键 */
    @Excel(name = "商品主键")
    @ApiModelProperty(value = "商品主键")
    private String goodsId;

    /** 商品名称 */
    @ApiModelProperty(value = "商品主键")
    @TableField(exist = false)
    private String goodsName;

    /** 品牌 */
    @Excel(name = "品牌")
    @ApiModelProperty(value = "品牌")
    private String brand;

    /** 规格 */
    @Excel(name = "规格")
    @ApiModelProperty(value = "规格")
    private String specs;

    /** 口岸名称 */
    @Excel(name = "口岸名称")
    @ApiModelProperty(value = "口岸名称")
    private String portName;

    /** 价格 */
    @Excel(name = "价格")
    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    /** 涨跌 */
    @Excel(name = "涨跌")
    @ApiModelProperty(value = "涨跌")
    private String upAndDown;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 港口 */
    @Excel(name = "港口")
    @ApiModelProperty(value = "港口")
    private String port;

    /** 产地 */
    @Excel(name = "产地")
    @ApiModelProperty(value = "产地")
    private String producingArea;

    /** 成交量 */
    @Excel(name = "成交量")
    @ApiModelProperty(value = "成交量")
    private String turnover;

    /** 地区 */
    @Excel(name = "地区")
    @ApiModelProperty(value = "地区")
    private String area;

    /** 电厂名称 */
    @Excel(name = "电厂名称")
    @ApiModelProperty(value = "电厂名称")
    private String powerPlant;

    /** 中标价 */
    @Excel(name = "中标价")
    @ApiModelProperty(value = "中标价")
    private BigDecimal bidWinningPrice;

    /** 中标量 */
    @Excel(name = "中标量")
    @ApiModelProperty(value = "中标量")
    private BigDecimal mesoscalar;

    /** 始发地 */
    @Excel(name = "始发地")
    @ApiModelProperty(value = "始发地")
    private String origin;

    /** 目的地 */
    @Excel(name = "目的地")
    @ApiModelProperty(value = "目的地")
    private String destination;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal freightUnit;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 历史数据 **/
    @Excel(name = "历史数据")
    @ApiModelProperty(value = "历史数据")
    private Integer historicalData;


    /** 时间 */
    @ApiModelProperty(value = "时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date renewTime;

    /** 质量 */
    @Excel(name = "质量")
    @ApiModelProperty(value = "质量")
    private BigDecimal quality;

    /**
     * 租户ID
     */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;


}
