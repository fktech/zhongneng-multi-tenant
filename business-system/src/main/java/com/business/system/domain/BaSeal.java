package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 用印对象 ba_seal
 *
 * @author ljb
 * @date 2023-03-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "用印对象",description = "")
@TableName("ba_seal")
public class BaSeal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /** 合同启动名称 */
    @Excel(name = "合同启动名称")
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 用印文件名称 */
    @Excel(name = "用印文件名称")
    @ApiModelProperty(value = "用印文件名称")
    private String name;

    /** 模板ID */
    @Excel(name = "模板ID")
    @ApiModelProperty(value = "模板ID")
    private String formworkId;

    /** 模板名称 */
    @Excel(name = "模板名称")
    @ApiModelProperty(value = "模板名称")
    @TableField(exist = false)
    private String formworkName;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用印文件 */
    @Excel(name = "用印文件")
    @ApiModelProperty(value = "用印文件")
    private String file;

    /** 其他附件 */
    //@Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 类型(普通用印 1，业务用印2，财务章（业务用印）3) */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 状态 **/
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用章类型 **/
    @Excel(name = "用章类型")
    @ApiModelProperty(value = "用章类型")
    private String sealType;


    /** 文件份数 **/
    @Excel(name = "文件份数")
    @ApiModelProperty(value = "文件份数")
    private Long copies;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID **/
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 创建人对应公司 */
    @ApiModelProperty(value = "创建人对应公司")
    @TableField(exist = false)
    private String companyName;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;

}
