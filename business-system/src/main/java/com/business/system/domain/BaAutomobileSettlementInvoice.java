package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 无车承运开票对象 ba_automobile_settlement_invoice
 *
 * @author single
 * @date 2023-04-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "无车承运开票对象",description = "")
@TableName("ba_automobile_settlement_invoice")
public class BaAutomobileSettlementInvoice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 开票编号 */
    @Excel(name = "开票编号")
    @ApiModelProperty(value = "开票编号")
    private String invoiceNo;

    /** 结算批次号 */
    @Excel(name = "结算批次号")
    @ApiModelProperty(value = "结算批次号")
    private String jsNo;

    /** 运费补差费（服务费） */
    @Excel(name = "运费补差费", readConverterExp = "服=务费")
    @ApiModelProperty(value = "运费补差费")
    private BigDecimal serviceDifferenceAmount;

    /** 合计金额 */
    @Excel(name = "合计金额")
    @ApiModelProperty(value = "合计金额")
    private BigDecimal invoiceMoney;

    /** 开票单据名称 */
    @Excel(name = "开票单据名称")
    @ApiModelProperty(value = "开票单据名称")
    private String invoiceName;

    /** 归属货主 */
    @Excel(name = "归属货主")
    @ApiModelProperty(value = "归属货主")
    private String kpfName;

    /** 运单量(吨) */
    @Excel(name = "运单量(吨)")
    @ApiModelProperty(value = "运单量(吨)")
    private BigDecimal settlementamount;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 申请时间 **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请时间")
    private Date applyTime;

    /** 开票审核状态 **/
    @Excel(name = "开票审核状态")
    @ApiModelProperty(value = "开票审核状态")
    private Integer checkFlag;

    /** 归属货主（公司） */
    @Excel(name = "归属货主（公司）")
    @ApiModelProperty(value = "归属货主（公司）")
    private String companyName;

    /** 运单数 **/
    @ApiModelProperty(value = "运单数")
    @TableField(exist = false)
    private Integer waybill;

    /** 运费金额 */
    @ApiModelProperty(value = "运费金额")
    @TableField(exist = false)
    private BigDecimal moneyTrans;

    /** 开票明细 */
    @ApiModelProperty(value = "开票明细")
    @TableField(exist = false)
    private List<BaAutomobileSettlementInvoiceDetail> baAutomobileSettlementInvoiceDetail;






}
