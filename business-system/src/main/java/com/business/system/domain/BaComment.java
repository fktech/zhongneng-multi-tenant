package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 评论对象 ba_comment
 *
 * @author ljb
 * @date 2023-06-08
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "评论对象",description = "")
@TableName("ba_comment")
public class BaComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private String id;

    /** 评论内容 */
    @Excel(name = "评论内容")
    @ApiModelProperty(value = "评论内容")
    private String content;

    /** 评论人id */
    @Excel(name = "评论人id")
    @ApiModelProperty(value = "评论人id")
    private Long userId;

    /** 评论名称 */
    @Excel(name = "评论名称")
    @ApiModelProperty(value = "评论名称")
    private String userName;

    /**
     * 头像
     */
    @ApiModelProperty(value = "评论名称")
    @TableField(exist = false)
    private String avatar;

    /** 记录审批状态,0审批中,1已完成 */
    @Excel(name = "记录审批状态,0审批中,1已完成")
    @ApiModelProperty(value = "记录审批状态,0审批中,1已完成")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 业务id */
    @Excel(name = "业务id")
    @ApiModelProperty(value = "业务id")
    private String blogId;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 父评论id */
    @Excel(name = "父评论id")
    @ApiModelProperty(value = "父评论id")
    private String parentId;

    /** 根评论id */
    @Excel(name = "根评论id")
    @ApiModelProperty(value = "根评论id")
    private String rootParentId;

    /** 流程实例ID */
    @Excel(name = "流程实例ID")
    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    /** 流程节点ID */
    @Excel(name = "流程节点ID")
    @ApiModelProperty(value = "流程节点ID")
    private String taskKeyId;

    /** 本评论下的子评论 */
    @TableField(exist = false)
    private List<BaComment> child;

    /** 角色 */
    @TableField(exist = false)
    private String roleName;

    /** 业务名称(项目名称或合同启动名称) */
    @Excel(name = "评论内容(项目名称或合同启动名称)")
    @ApiModelProperty(value = "评论内容(项目名称或合同启动名称)")
    private String businessName;

    /** 发消息UserId */
    @TableField(exist = false)
    @ApiModelProperty(value = "发消息UserId")
    private Long sendUserId;

    /**
     * 回复
     */
    @ApiModelProperty(value = "回复")
    private String reply;
}
