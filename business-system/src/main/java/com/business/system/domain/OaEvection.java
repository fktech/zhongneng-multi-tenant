package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 出差申请对象 oa_evection
 *
 * @author ljb
 * @date 2023-11-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "出差申请对象",description = "")
@TableName("oa_evection")
public class OaEvection extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 附件 */
    //@Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;


    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    //@Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @Excel(name = "申请人",needMerge = true)
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称",needMerge = true)
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 租户ID */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 提交状态 */
    //@Excel(name = "提交状态")
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 同行人 */
    //@Excel(name = "同行人")
    @ApiModelProperty(value = "同行人")
    private String peers;

    /** 同行人名称 */
    @Excel(name = "同行人", needMerge = true)
    @ApiModelProperty(value = "同行人名称")
    @TableField(exist = false)
    private String peersName;

    /** 状态 */
    @Excel(name = "状态", needMerge = true, readConverterExp = "oaEvection:verifying=审批中,oaEvection:pass=已通过,oaEvection:reject=已拒绝,oaEvection:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 原因 */
    //@Excel(name = "原因" ,needMerge = true)
    @ApiModelProperty(value = "原因")
    private String reason;

    /** 创建时间 */
    @Excel(name = "创建时间" ,needMerge = true,dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(exist = false)
    private Date creationTime;

    /** 行程信息 */
    @Excel(name = "行程信息")
    @ApiModelProperty(value = "行程信息")
    @TableField(exist = false)
    private List<OaEvectionTrip> oaEvectionTripList;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 用户ID集合 */
    @ApiModelProperty(value = "用户ID集合")
    @TableField(exist = false)
    private String userIds;

}
