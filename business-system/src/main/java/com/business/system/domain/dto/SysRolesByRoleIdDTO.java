package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 根据角色ID查询角色DTO
 * @Date: 2023/4/4 10:54
 */
@Data
@ApiModel(value = "SysRolesByRoleIdDTO对象", description = "根据角色ID查询角色DTO")
public class SysRolesByRoleIdDTO implements Serializable {

    private static final long serialVersionUID = -2819327953622316262L;

    @ApiModelProperty("角色ID")
    private String roleId;
}
