package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 采购清单对象 ba_purchase_order
 *
 * @author ljb
 * @date 2023-08-31
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "采购清单对象",description = "")
@TableName("ba_purchase_order")
public class BaPurchaseOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 商品id */
    @Excel(name = "商品id")
    @ApiModelProperty(value = "商品id")
    private String goodsId;
    /** 商品名称 */
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** 供应商id */
    @Excel(name = "供应商id")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    /** 供应商名称 */
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String supplierName;

    @ApiModelProperty(value = "公司简称")
    @TableField(exist = false)
    private String companyAbbreviation;

    /** 不含税单价 */
    @Excel(name = "不含税单价")
    @ApiModelProperty(value = "不含税单价")
    private BigDecimal excludingPrice;

    /** 含税单价 */
    @Excel(name = "含税单价")
    @ApiModelProperty(value = "含税单价")
    private BigDecimal includingPrice;

    /** 采购量 */
    @Excel(name = "采购量")
    @ApiModelProperty(value = "采购量")
    private BigDecimal procurementVolume;

    /** 货值 */
    @Excel(name = "货值")
    @ApiModelProperty(value = "货值")
    private BigDecimal goodsValue;

    /** 计划用款 */
    @Excel(name = "计划用款")
    @ApiModelProperty(value = "计划用款")
    private BigDecimal plannedFunds;

    /** 指标 */
    @Excel(name = "指标")
    @ApiModelProperty(value = "指标")
    private String target;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 采购计划id */
    @Excel(name = "采购计划id")
    @ApiModelProperty(value = "采购计划id")
    private String planId;

    /** 采购计划名称 */
    @ApiModelProperty(value = "采购计划名称")
    @TableField(exist = false)
    private String planName;

    /** 名称拼接 **/
    @ApiModelProperty(value = "名称拼接")
    @TableField(exist = false)
    private String montageName;

    /** 仓库id */
    @ApiModelProperty(value = "仓库id")
    @TableField(exist = false)
    private String depotId;

    /** 进厂车数 */
    @ApiModelProperty(value = "进厂车数")
    @TableField(exist = false)
    private Integer enteringCars;

    /** 进厂数量 */
    @ApiModelProperty(value = "进厂数量")
    @TableField(exist = false)
    private BigDecimal enteringNumber;

    /** 确认车数 */
    @ApiModelProperty(value = "确认车数")
    @TableField(exist = false)
    private Integer confirmCars;

    /** 确认数量 */
    @ApiModelProperty(value = "确认数量")
    @TableField(exist = false)
    private BigDecimal confirmNumber;

    /** 磅房数据 **/
    @ApiModelProperty(value = "磅房数据")
    @TableField(exist = false)
    private List<BaPoundRoom> poundRoomList;

    /** 货源 **/
    //@Excel(name = "货源")
    @ApiModelProperty(value = "货源")
    @TableField(exist = false)
    private Set<String> sourceGoods;





}
