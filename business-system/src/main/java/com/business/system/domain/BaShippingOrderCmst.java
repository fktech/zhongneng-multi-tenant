package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 中储智运发运单对象 ba_shipping_order_cmst
 *
 * @author js
 * @date 2023-08-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "中储智运发运单对象",description = "")
@TableName("ba_shipping_order_cmst")
public class BaShippingOrderCmst extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 0 抢单,1 竞价 */
    @Excel(name = "0 抢单,1 竞价")
    @ApiModelProperty(value = "0 抢单,1 竞价")
    private String orderModel;

    /** 订单号，如果是批量货，就是子单号 */
    @Excel(name = "订单号，如果是批量货，就是子单号")
    @ApiModelProperty(value = "订单号，如果是批量货，就是子单号")
    private String orderId;

    /** 如果为批量货，就为母单号，普通货为空 */
    @Excel(name = "如果为批量货，就为母单号，普通货为空")
    @ApiModelProperty(value = "如果为批量货，就为母单号，普通货为空")
    private String yardId;

    /** 自定义单号 */
    @Excel(name = "自定义单号")
    @ApiModelProperty(value = "自定义单号")
    private String selfComment;

    /** 货主名称 */
    @Excel(name = "货主名称")
    @ApiModelProperty(value = "货主名称")
    private String consignorUserName;

    /** 货主名称手机号 */
    @Excel(name = "货主名称手机号")
    @ApiModelProperty(value = "货主名称手机号")
    private String consignorMobile;

    /** 承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止 */
    @Excel(name = "承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止")
    @ApiModelProperty(value = "承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止")
    private String consignorState;

    /** 货物名称 */
    @Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String cargoName;

    /** 承运方姓名 */
    @Excel(name = "承运方姓名")
    @ApiModelProperty(value = "承运方姓名")
    private String carrierName;

    /** 承运方手机号 */
    @Excel(name = "承运方手机号")
    @ApiModelProperty(value = "承运方手机号")
    private String carrierMobile;

    /** 摘牌时间，日期格式 格式:yyyy-mm-dd hh:mm:ss */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "摘牌时间，日期格式 格式:yyyy-mm-dd hh:mm:ss", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "摘牌时间，日期格式 格式:yyyy-mm-dd hh:mm:ss")
    private Date delistTime;

    /** 摘单吨位 */
    @Excel(name = "摘单吨位")
    @ApiModelProperty(value = "摘单吨位")
    private BigDecimal weight;

    /** 车牌号 */
    @Excel(name = "车牌号")
    @ApiModelProperty(value = "车牌号")
    private String plateNumber;

    /** 司机姓名 */
    @Excel(name = "司机姓名")
    @ApiModelProperty(value = "司机姓名")
    private String driverUserName;

    /** 司机手机号 */
    @Excel(name = "司机手机号")
    @ApiModelProperty(value = "司机手机号")
    private String driverMobile;

    /** 司机身份证号码 */
    @Excel(name = "司机身份证号码")
    @ApiModelProperty(value = "司机身份证号码")
    private String driverIdNumber;

    /** 司机身份证号附件,正、反 */
    @Excel(name = "司机身份证号附件,正、反")
    @ApiModelProperty(value = "司机身份证号附件,正、反")
    private String driverIdPhoto;

    /** 司机驾驶证证号 */
    @Excel(name = "司机驾驶证证号")
    @ApiModelProperty(value = "司机驾驶证证号")
    private String driverLicenseNumber;

    /** 司机驾驶证证附件 */
    @Excel(name = "司机驾驶证证附件")
    @ApiModelProperty(value = "司机驾驶证证附件")
    private String driverLicensePhoto;

    /** 司机从业资格信息证号 */
    @Excel(name = "司机从业资格信息证号")
    @ApiModelProperty(value = "司机从业资格信息证号")
    private String driverOccupationNumber;

    /** 司机从业资格信息证附件 */
    @Excel(name = "司机从业资格信息证附件")
    @ApiModelProperty(value = "司机从业资格信息证附件")
    private String driverOccupationPhoto;

    /** 车辆行驶证号码 */
    @Excel(name = "车辆行驶证号码")
    @ApiModelProperty(value = "车辆行驶证号码")
    private String truckLicenseNumber;

    /** 车辆行驶证附件 */
    @Excel(name = "车辆行驶证附件")
    @ApiModelProperty(value = "车辆行驶证附件")
    private String truckLicensePhoto;

    /** 道路运输证号 */
    @Excel(name = "道路运输证号")
    @ApiModelProperty(value = "道路运输证号")
    private String truckRoadNumber;

    /** 道路运输证附件 */
    @Excel(name = "道路运输证附件")
    @ApiModelProperty(value = "道路运输证附件")
    private String truckRoadPhoto;

    /** 确认发货重量 */
    @Excel(name = "确认发货重量")
    @ApiModelProperty(value = "确认发货重量")
    private BigDecimal confirmedDeliveryWeight;

    /** 发货照片 */
    @Excel(name = "发货照片")
    @ApiModelProperty(value = "发货照片")
    private String shipmentPhotos;

    /** 人车照片 */
    @Excel(name = "人车照片")
    @ApiModelProperty(value = "人车照片")
    private String carPhoto;

    /** 确认收货重量 */
    @Excel(name = "确认收货重量")
    @ApiModelProperty(value = "确认收货重量")
    private BigDecimal confirmedReceiveWeight;

    /** 收货照片 */
    @Excel(name = "收货照片")
    @ApiModelProperty(value = "收货照片")
    private String receiptImages;

    /** 吨位 */
    @Excel(name = "吨位")
    @ApiModelProperty(value = "吨位")
    private BigDecimal tonnage;

    /** 结算金额 */
    @Excel(name = "结算金额")
    @ApiModelProperty(value = "结算金额")
    private BigDecimal settleMoney;

    /** 货主结算价格 */
    @Excel(name = "货主结算价格")
    @ApiModelProperty(value = "货主结算价格")
    private BigDecimal settlementAmount;

    /** 结算时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结算时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结算时间")
    private Date settlementTime;

    /** 结算状态 */
    @Excel(name = "结算状态")
    @ApiModelProperty(value = "结算状态")
    private String settlementState;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 操作 1：同意， 2：驳回（拒绝） */
    @Excel(name = "操作 1：同意， 2：驳回", readConverterExp = "拒=绝")
    @ApiModelProperty(value = "操作 1：同意， 2：驳回")
    private String operation;

    /** 违约金额 */
    @Excel(name = "违约金额")
    @ApiModelProperty(value = "违约金额")
    private BigDecimal consignorAmount;

    /** 运单是否终止（1-是，0-否） */
    @Excel(name = "运单是否终止", readConverterExp = "1=-是，0-否")
    @ApiModelProperty(value = "运单是否终止")
    private String isStop;

    /** 是否最终处理结果（固定值 1） */
    @Excel(name = "是否最终处理结果", readConverterExp = "固=定值,1=")
    @ApiModelProperty(value = "是否最终处理结果")
    private String platformResults;

    /** 结算状态，1 未申请 2 已申请 3已结算 */
    @Excel(name = "结算状态，1 未申请 2 已申请 3已结算")
    @ApiModelProperty(value = "结算状态，1 未申请 2 已申请 3已结算")
    private String settlementType;

    @Excel(name = "开票状态，1 已开票")
    @ApiModelProperty(value = "开票状态，1 已开票")
    private String invoiceType;

    /** 司机身份证号附件正面 */
    @ApiModelProperty(value = "司机身份证号附件正面")
    private String driverIDPhotoFront;

    /** 司机身份证号附件反面 */
    @ApiModelProperty(value = "司机身份证号附件反面")
    private String driverIDPhotoBack;

    /** 挂车行驶证号码 */
    @ApiModelProperty(value = "挂车行驶证号码")
    private String truckLicenseNumbertrailer;

    /** 挂车行驶证附件 */
    @Excel(name = "挂车行驶证附件")
    @ApiModelProperty(value = "挂车行驶证附件")
    private String truckLicensePhototrailer;
    /** 运单编号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运单编号")
    private String thdno;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /**
     * 签收状态
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "签收状态")
    private String signFor;
    /** 货物名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "货源类型")
    private String automobileType;


    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date deliveryTime;

    /** 收货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date receivingTime;

    /** 货源对象 */
    @TableField(exist = false)
    @ApiModelProperty(value = "货源对象")
    private BaTransportCmst baTransportCmst;
    /** 货源对象 */
    @TableField(exist = false)
    @ApiModelProperty(value = "签收时间")
    private Date signForTime;
    /** 运单编号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运单金额")
    private BigDecimal thdnoMoney;

    /** 运单编号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运单单价")
    private BigDecimal unitPrice;

    /**
     * 入库状态
     */
    @ApiModelProperty(value = "入库状态")
    private String status;

    @Excel(name = "出入库状态")
    @ApiModelProperty(value = "出入库状态")
    private String state;

    /** 仓库ID **/
    @Excel(name = "仓库ID")
    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    /** 仓位ID **/
    @Excel(name = "仓位ID")
    @ApiModelProperty(value = "仓位ID")
    private String positionId;

    /** 试验ID **/
    @Excel(name = "试验ID")
    @ApiModelProperty(value = "试验ID")
    private String experimentId;

    /** 化验对象 **/
    @ApiModelProperty(value = "化验对象")
    @TableField(exist = false)
    private BaSampleAssay assay;
}
