package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 人员督办统计
 * @date: 2023/12/19 9:20
 */
@Data
@ApiModel(value="OaSupervisingReceiveVO对象",description="人员督办统计")
public class OaSupervisingReceiveVO implements Serializable {

    private static final long serialVersionUID = 1210903556307284643L;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    private String userName;

    /** 数量 */
    @ApiModelProperty(value = "数量")
    private Long count;

    /** 回告数量 */
    @ApiModelProperty(value = "回告数量")
    private Long replyCount;
}
