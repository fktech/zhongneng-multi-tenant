package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 预警对象 ba_early_warning
 *
 * @author single
 * @date 2024-05-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "预警对象",description = "")
@TableName("ba_early_warning")
public class BaEarlyWarning
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 关联ID（合同启动/立项） */
    @Excel(name = "关联ID", readConverterExp = "合=同启动/立项")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;

    /** 尾日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "尾日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "尾日期")
    private Date tailTime;

    /** 预估回款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "预估回款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "预估回款日期")
    private Date estimatedPaymentDate;

    /** 最终回款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最终回款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "最终回款日期")
    private Date finalPaymentDate;

    /** 当前账期 */
    @Excel(name = "当前账期")
    @ApiModelProperty(value = "当前账期")
    private Long currentAccountPeriod;

    /** 最终账期 */
    @Excel(name = "最终账期")
    @ApiModelProperty(value = "最终账期")
    private Long finalAccountPeriod;

    /** 周期上限 */
    @Excel(name = "周期上限")
    @ApiModelProperty(value = "周期上限")
    private Long cycleUpperLimit;

    /** 提前预警 */
    @Excel(name = "提前预警")
    @ApiModelProperty(value = "提前预警")
    private Long earlyWarning;

    /** 预警状态 */
    @Excel(name = "预警状态")
    @ApiModelProperty(value = "预警状态")
    private String state;

    /** 预警类型 */
    @Excel(name = "预警类型")
    @ApiModelProperty(value = "预警类型")
    private String type;

    /** 总付款 */
    @Excel(name = "总付款")
    @ApiModelProperty(value = "总付款")
    private BigDecimal paymentTotal;

    /** 总收款 */
    @Excel(name = "总收款")
    @ApiModelProperty(value = "总收款")
    private BigDecimal collectionTotal;

    /** 实际占资 */
    @Excel(name = "实际占资")
    @ApiModelProperty(value = "实际占资")
    private BigDecimal actualCapitalOccupation;

    /** 额度上限 */
    @Excel(name = "额度上限")
    @ApiModelProperty(value = "额度上限")
    private BigDecimal limitLimit;

    /** 首付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "首付款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "首付款日期")
    private Date downPaymentTime;

    /** 占资账期上限 */
    @Excel(name = "占资账期上限")
    @ApiModelProperty(value = "占资账期上限")
    private Long accountUpperLimit;

    /** 当前占资账期 */
    @Excel(name = "当前占资账期")
    @ApiModelProperty(value = "当前占资账期")
    private Long currentUpperLimit;

    /** 最终占资账期 */
    @Excel(name = "最终占资账期")
    @ApiModelProperty(value = "最终占资账期")
    private Long finalUpperLimit;

    /** 应收预警状态 */
    @Excel(name = "应收预警状态")
    @ApiModelProperty(value = "应收预警状态")
    private String receivableState;

    /** 额度预警状态 */
    @Excel(name = "额度预警状态")
    @ApiModelProperty(value = "额度预警状态")
    private String limitState;

    /** 账期预警状态 */
    @Excel(name = "账期预警状态")
    @ApiModelProperty(value = "账期预警状态")
    private String accountPeriodState;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 还款完成 */
    @Excel(name = "还款完成")
    @ApiModelProperty(value = "还款完成")
    private String repaymentCompleted;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 合同启动简称 */
    @ApiModelProperty(value = "合同启动简称")
    @TableField(exist = false)
    private String startShorter;

    /** 合同名称 */
    @ApiModelProperty(value = "合同名称")
    @TableField(exist = false)
    private String startName;

    /** 下游公司ID */
    @ApiModelProperty(value = "下游公司ID")
    @TableField(exist = false)
    private String downstreamCompany;

    /** 下游公司简称 */
    @ApiModelProperty(value = "下游公司简称")
    @TableField(exist = false)
    private String companyShort;

    /** 项目ID */
    @ApiModelProperty(value = "项目ID")
    @TableField(exist = false)
    private String projectId;

    /** 合同启动创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "合同启动创建时间")
    @TableField(exist = false)
    private Date createTime;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 上游公司 **/
    @ApiModelProperty(value = "上游公司")
    @TableField(exist = false)
    private String upstreamCompany;

    /** 上游公司简称 **/
    @ApiModelProperty(value = "上游公司简称")
    @TableField(exist = false)
    private String companyAbbreviation;

    /** 发货预警状态 */
    @ApiModelProperty(value = "发货预警状态")
    private String deliveryAlertState;

    /** 发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "发货日期")
    @TableField(exist = false)
    private Date checkTime;

    /** 发货结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "发货结束日期")
    @TableField(exist = false)
    private Date checkEndTime;
}
