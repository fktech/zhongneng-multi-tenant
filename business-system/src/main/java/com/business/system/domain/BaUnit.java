package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 单位对象 ba_unit
 *
 * @author ljb
 * @date 2022-11-30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "单位对象",description = "")
@TableName("ba_unit")
public class BaUnit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 基础单位 */
    @Excel(name = "基础单位")
    @ApiModelProperty(value = "基础单位")
    private String basicUnit;

    /** 副单位 */
    @Excel(name = "副单位")
    @ApiModelProperty(value = "副单位")
    private String otherUnit;

    /** 比例 */
    @Excel(name = "比例")
    @ApiModelProperty(value = "比例")
    private String ratio;

    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /** 英文名 */
    @Excel(name = "英文名")
    @ApiModelProperty(value = "英文名")
    private String englishName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;



}
