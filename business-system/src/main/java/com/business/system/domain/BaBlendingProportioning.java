package com.business.system.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 配煤配比对象 ba_blending_proportioning
 *
 * @author ljb
 * @date 2024-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "配煤配比对象",description = "")
@TableName("ba_blending_proportioning")
public class BaBlendingProportioning extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 产地 */
    @Excel(name = "产地")
    @ApiModelProperty(value = "产地")
    private String producer;

    /** 煤种 */
    @Excel(name = "煤种")
    @ApiModelProperty(value = "煤种")
    private String coalSort;

    /** Mt */
    @Excel(name = "Mt")
    @ApiModelProperty(value = "Mt")
    private BigDecimal indexMt;

    /** Ad */
    @Excel(name = "Ad")
    @ApiModelProperty(value = "Ad")
    private BigDecimal indexAd;

    /** Vdaf */
    @Excel(name = "Vdaf")
    @ApiModelProperty(value = "Vdaf")
    private BigDecimal indexVdaf;

    /** St,d */
    @Excel(name = "St,d")
    @ApiModelProperty(value = "St,d")
    private BigDecimal indexSt;

    /** G */
    @Excel(name = "G")
    @ApiModelProperty(value = "G")
    private BigDecimal indexG;

    /** Y */
    @Excel(name = "Y")
    @ApiModelProperty(value = "Y")
    private BigDecimal indexY;

    /** Ro */
    @Excel(name = "Ro")
    @ApiModelProperty(value = "Ro")
    private BigDecimal indexRo;

    /** 标准差 */
    @Excel(name = "标准差")
    @ApiModelProperty(value = "标准差")
    private BigDecimal standardDeviation;

    /** 到厂价 */
    @Excel(name = "到厂价")
    @ApiModelProperty(value = "到厂价")
    private BigDecimal factoryPrice;

    /** 数量 */
    @Excel(name = "数量")
    @ApiModelProperty(value = "数量")
    private BigDecimal quantity;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** X */
    @Excel(name = "X")
    @ApiModelProperty(value = "X")
    private BigDecimal indexX;



}
