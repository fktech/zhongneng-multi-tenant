package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.system.domain.BaCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel(value = "货源对象DTO",description = "货源信息")
public class sourceGoodsDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 仓储立项名称 **/
    @ApiModelProperty(value = "仓储立项名称")
    private String storageName;

    /** 销售订单名称 **/
    @ApiModelProperty(value = "销售订单名称")
    private String saleName;

    /** 销售订单ID **/
    @ApiModelProperty(value = "销售订单ID")
    private String saleId;

    /** 采购订单名称 **/
    @ApiModelProperty(value = "采购订单名称")
    private String procureName;

    /** 采购订单ID **/
    @ApiModelProperty(value = "采购订单ID")
    private String procureID;

    /** 平台 **/
    @ApiModelProperty(value = "平台")
    private String platform;

    /** 货主名称 **/
    @ApiModelProperty(value = "货主名称")
    private String supplierName;

    /** 货源编码 **/
    @ApiModelProperty(value = "货源编码")
    private String id;

    /** 品名 **/
    @ApiModelProperty(value = "品名")
    private String goodsName;


    /** 货源路线 **/
    @ApiModelProperty(value = "货源路线")
    private String sourceName;

    /** 发货地区 **/
    @ApiModelProperty(value = "发货地区")
    private String placeDep;

    /** 收货地区 **/
    @ApiModelProperty(value = "收货地区")
    private String placeDes;

    /** 车数 **/
    @ApiModelProperty(value = "车数")
    private Integer carNum;

    /** 实际发运量 **/
    @ApiModelProperty(value = "实际发运量")
    private BigDecimal saleNum;

    /** 创建时间 **/
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /** 货源单编号 **/
    @ApiModelProperty(value = "货源单编号")
    private String supplierNo;

    /** 用户ID **/
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /** 审批状态 **/
    @ApiModelProperty(value = "审批状态")
    private String state;

    /** 二维码图片地址 **/
    @ApiModelProperty(value = "二维码图片地址")
    private String qrcode;

    /** 删除标记 */
    @ApiModelProperty(value = "删除标记：0-未删除，1-已删除")
    private Long isDelete;

    @ApiModelProperty(value = "待发状态")
    private String confirmStatus;

    @ApiModelProperty(value = "订单编号")
    private String orderId;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "出库")
    private String outbound;

    @ApiModelProperty(value = "入库")
    private String warehousing;

    @ApiModelProperty(value = "发货信息")
    private BaCheck delivery;

    /** 终端 **/
    @ApiModelProperty(value = "终端")
    private String enterprise;

    /** 终端名称 **/
    @ApiModelProperty(value = "终端名称")
    private String enterpriseName;

    /**
     * 取样出入库
     */
    @ApiModelProperty(value = "取样出入库")
    private String sampleType;



}
