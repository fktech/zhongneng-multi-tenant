package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.system.domain.BaClaim;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 收款信息对象
 *
 * @author js
 * @date 2022-12-26
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "收款信息对象",description = "")
public class BaCollectionVoucherDTO extends BaseEntity
{

    /** 主键 */
    private String id;

    /** 收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "收款日期")
    private Date collectionDate;

    /** 付款方式 */
    @ApiModelProperty(value = "收款方式")
    private String collectionType;

    /** 付款单位 */
    @ApiModelProperty(value = "付款单位")
    private String paymentCompany;

    @Excel(name = "付款单位名称")
    @ApiModelProperty(value = "付款单位名称")
    private String paymentCompanyName;

    /** 付款账号 */
    @Excel(name = "付款账号")
    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    /**
     * 收款类别
     */
    @Excel(name = "收款类型", readConverterExp = "1=下游回款,2=上游退款,3=运费退款,4=其他收款,5=保证金退款,6=保证金收款")
    @ApiModelProperty(value = "收款类别")
    private String collectionCategory;


    /** 收款单位 */
    @ApiModelProperty(value = "收款单位")
    private String collectionCompany;

    /** 收款账号 */
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 付款凭证 */
    @ApiModelProperty(value = "收款凭证")
    private String collectionVoucher;

    /** 凭证备注 */
    @ApiModelProperty(value = "凭证备注")
    private String voucherRemark;

    /** 收款金额 **/
    @Excel(name = "收款金额")
    @ApiModelProperty(value = "收款金额")
    private BigDecimal applyCollectionAmount;

    /** 已认领金额 **/
    @ApiModelProperty(value = "已认领金额")
    private BigDecimal claimed;

    /** 审核状态 **/
    @Excel(name = "审核状态", readConverterExp = "claim:verifying=审批中,claim:pass=已通过,claim:reject=已拒绝,claim:withdraw=已撤回")
    @ApiModelProperty(value = "审核状态")
    private String state;

    /** 发起人 **/
    @Excel(name = "归属岗位")
    @ApiModelProperty(value = "发起人")
    private String userName;

    /** 发起人编号 **/
    @ApiModelProperty(value = "发起人编号")
    private String userId;

    /** 银行ID **/
    @ApiModelProperty(value = "银行ID")
    private String bankId;

    /** 标记 **/
    @ApiModelProperty(value = "标记")
    private String flag;

    /** 去审批发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "去审批发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "去审批发起时间")
    private Date initiationTime;

    /** 认领记录 **/
    @ApiModelProperty(value = "认领记录")
    private List<BaClaim> baClaimList;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID **/
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    @ApiModelProperty(value = "去审批人")
    private Long approver;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date launchTime;


    /**
     * 合同启动ID
     */
    @ApiModelProperty(value = "合同启动ID")
    private String startId;


}
