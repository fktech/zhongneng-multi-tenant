package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="钉钉打卡信息",description="钉钉打卡对象")
public class DingDingVO {

    /** 用户打卡定位精度 */
    @ApiModelProperty(value = "用户打卡定位精度")
    private String userAccuracy;

    /** 班次ID */
    @ApiModelProperty(value = "班次ID")
    private Long classId;

    /** 用户打卡纬度 */
    @ApiModelProperty(value = "用户打卡纬度")
    private String userLatitude;

    /** 用户打卡经度 */
    @ApiModelProperty(value = "用户打卡经度")
    private String userLongitude;

    /** 用户打卡地址 */
    @ApiModelProperty(value = "用户打卡地址")
    private String userAddress;


    /** 打卡设备ID */
    @ApiModelProperty(value = "打卡设备ID")
    private String deviceId;

    /** 定位方法 */
    @ApiModelProperty(value = "定位方法")
    private String locationMethod;

    /** 是否合法 */
    @ApiModelProperty(value = "是否合法")
    private String isLegal;

    /** 实际打卡时间 */
    @ApiModelProperty(value = "实际打卡时间")
    private String userCheckTime;

    /** 关联的审批实例ID，当该字段非空时，表示打卡记录与请假、加班等审批有关 */
    @ApiModelProperty(value = "关联的审批实例ID，当该字段非空时，表示打卡记录与请假、加班等审批有关")
    private String procInstId;

    /** 计算迟到和早退，基准时间；也可作为排班打卡时间 */
    @ApiModelProperty(value = "计算迟到和早退，基准时间；也可作为排班打卡时间")
    private String baseCheckTime;

    /** 关联的审批ID，当该字段非空时，表示打卡记录与请假、加班等审批有关 */
    @ApiModelProperty(value = "关联的审批ID，当该字段非空时，表示打卡记录与请假、加班等审批有关")
    private String approveId;

    /** 打卡结果 */
    @ApiModelProperty(value = "打卡结果")
    private String timeResult;

    /** 位置结果 */
    @ApiModelProperty(value = "位置结果")
    private String locationResult;

    /** 考勤类型 */
    @ApiModelProperty(value = "考勤类型")
    private String checkType;

    /** 数据来源 */
    @ApiModelProperty(value = "数据来源")
    private String sourceType;

    /** 打卡人的userId */
    @ApiModelProperty(value = "打卡人的userId")
    private String userId;

    /** 工作日 */
    @ApiModelProperty(value = "位置结果")
    private String workDate;

    /** 企业ID */
    @ApiModelProperty(value = "企业ID")
    private String corpId;

    /** 排班ID */
    @ApiModelProperty(value = "排班ID")
    private String planId;

    /** 考勤组ID */
    @ApiModelProperty(value = "考勤组ID")
    private String groupId;

    /** 考勤ID */
    @ApiModelProperty(value = "考勤ID")
    private String id;

    /** 异常信息类型 */
    @ApiModelProperty(value = "异常信息类型")
    private String invalidRecordType;

    /** 用户打卡wifi SSID */
    @ApiModelProperty(value = "用户打卡wifi SSID")
    private String userSsid;

    /** 用户打卡wifi Mac地址 */
    @ApiModelProperty(value = "用户打卡wifi Mac地址")
    private String userMacAddr;

    /** 排班打卡时间 */
    @ApiModelProperty(value = "排班打卡时间")
    private String planCheckTime;

    /** 基准地址 */
    @ApiModelProperty(value = "基准地址")
    private String baseAddress;

    /** 基准经度 */
    @ApiModelProperty(value = "基准经度")
    private String baseLongitude;

    /** 基准纬度 */
    @ApiModelProperty(value = "基准纬度")
    private String baseLatitude;

    /** 基准定位精度 */
    @ApiModelProperty(value = "基准定位精度")
    private String baseAccuracy;

    /** 基准wifi ssid */
    @ApiModelProperty(value = "基准wifi ssid")
    private String baseSsid;

    /** 基准Mac地址 */
    @ApiModelProperty(value = "基准Mac地址")
    private String baseMacAddr;

    /** 打卡记录创建时间 */
    @ApiModelProperty(value = "打卡记录创建时间")
    private String gmtCreate;

    /** 对应的invalidRecordType异常信息的具体描述 */
    @ApiModelProperty(value = "对应的invalidRecordType异常信息的具体描述")
    private String invalidRecordMsg;

    /** 打卡记录修改时间 */
    @ApiModelProperty(value = "打卡记录修改时间")
    private String gmtModified;

    /** 打卡备注 */
    @ApiModelProperty(value = "打卡备注")
    private String outsideRemark;

    /** 打卡设备序列号 */
    @ApiModelProperty(value = "打卡设备序列号")
    private String deviceSN;

    /** 关联的业务ID */
    @ApiModelProperty(value = "基准纬度")
    private String bizId;

    /** 拍照图片 URL */
    @ApiModelProperty(value = "拍照图片 URL")
    private String photoUrl;


}
