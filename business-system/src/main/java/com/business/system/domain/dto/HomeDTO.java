package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.business.system.domain.BaContract;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "HomeDTO对象", description = "首页选项卡")
public class HomeDTO implements Serializable {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 月发运数量 **/
    @ApiModelProperty(value = "月发运数量")
    private BigDecimal shippingNum;

    /** 立项种类 **/
    @ApiModelProperty(value = "立项种类")
    private String projectType;

    /*** 货物种类 **/
    @ApiModelProperty(value = "货物种类")
    private String goodsType;

    /** 单价 */
    @ApiModelProperty(value = "单价")
    private BigDecimal price;

    /** 吨数 */
    @ApiModelProperty(value = "吨数")
    private BigDecimal tonnage;

    /** 运输类型 */
    @ApiModelProperty(value = "运输类型")
    private String transportWay;

    /** 上游合同 */
    @ApiModelProperty(value = "上游合同")
    private Integer upstream;

    /** 下游合同 */
    @ApiModelProperty(value = "下游合同")
    private Integer downstream;

    /** 其他合同 */
    @ApiModelProperty(value = "其他合同")
    private Integer other;

    /** 运输合同 */
    @ApiModelProperty(value = "运输合同")
    private Integer transportContract;

    /** 上游协议 */
    @ApiModelProperty(value = "上游协议")
    private Integer agreeOn;

    /** 下游协议 */
    @ApiModelProperty(value = "下游协议")
    private Integer belowAgreeOn;

    /** 已发货 */
    @ApiModelProperty(value = "已发货")
    private BigDecimal shipped;

    /** 已到货 */
    @ApiModelProperty(value = "已到货")
    private BigDecimal arrived;

    /** 运输中 */
    @ApiModelProperty(value = "运输中")
    private BigDecimal transit;

    /** 已验收 */
    @ApiModelProperty(value = "已验收")
    private BigDecimal accepted;

    /** 损耗量 */
    @ApiModelProperty(value = "损耗量")
    private BigDecimal wastage;

    /** 验收日期/发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "验收日期")
    private Date checkTime;

    /** 终端结算数量 **/
    @ApiModelProperty(value = "终端结算数量")
    private BigDecimal settlementNum;

    /** 终端结算金额(元) */
    @ApiModelProperty(value = "终端结算金额(元)")
    private BigDecimal settlementAmount;

    /** 终端创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "终端创建时间")
    private Date createTime;

    /** 上游结算数量 **/
    @ApiModelProperty(value = "上游结算数量")
    private BigDecimal upstreamSettlementNum;

    /** 上游结算金额(元) */
    @ApiModelProperty(value = "上游结算金额(元)")
    private BigDecimal upstreamSettlementAmount;

    /** 上游创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "上游创建时间")
    private Date upstreamCreateTime;

    /** 下游结算数量 **/
    @ApiModelProperty(value = "下游结算数量")
    private BigDecimal downstreamSettlementNum;

    /** 下游结算金额(元) */
    @ApiModelProperty(value = "下游结算金额(元)")
    private BigDecimal downstreamSettlementAmount;

    /** 下游创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "下游创建时间")
    private Date downstreamCreateTime;

    /** 货款 */
    @ApiModelProperty(value = "货款")
    private BigDecimal paymentForGoods;

    /** 运费 */
    @ApiModelProperty(value = "运费")
    private BigDecimal freight;


    /** 服务费 */
    @ApiModelProperty(value = "服务费")
    private BigDecimal serviceCharge;

    /** 保证金 */
    @ApiModelProperty(value = "保证金")
    private BigDecimal paymentMargin;

    /** 其他 */
    @ApiModelProperty(value = "其他")
    private BigDecimal paymentOther;

    /**
     * 已收票数量
     */
    @ApiModelProperty(value = "已收票数量")
    private Integer inVoIce;

    /**
     * 已收票金额
     */
    @ApiModelProperty(value = "已收票金额")
    private BigDecimal inVoIcePic;

    /**
     * 已开票数量
     */
    @ApiModelProperty(value = "已开票数量")
    private Integer invoicedQuantity;

    /**
     * 已开票金额
     */
    @ApiModelProperty(value = "已收票金额")
    private BigDecimal invoicedQuantityPic;

    /**
     * 利润估算
     */
    /*@ApiModelProperty(value = "利润估算")
    private BigDecimal profitEstimation;*/

    /**
     * 利润测算
     */
    @ApiModelProperty(value = "利润测算")
    private BigDecimal profitCalculation;

    /**
     * 汽运
     */
    @ApiModelProperty(value = "汽运")
    private String automobileTransportation;

    /**
     * 火运
     */
    @ApiModelProperty(value = "火运")
    private String fireTransport;

    /**
     * 认领收款
     */
    @ApiModelProperty(value = "认领收款")
    private BigDecimal applyCollectionAmount;

    /**
     * 终端结算
     */
    @ApiModelProperty(value = "终端结算")
    private String settlement;
    /**
     * 下游结算
     */
    @ApiModelProperty(value = "下游结算")
    private String downstreamSettlement;

    /**
     * 合同管理
     */
    @ApiModelProperty(value = "合同管理")
    private List<BaContract> baContractList;
}
