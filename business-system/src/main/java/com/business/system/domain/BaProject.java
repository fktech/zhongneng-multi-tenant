package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.annotation.Excels;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 立项管理对象 ba_project
 *
 * @author ljb
 * @date 2022-12-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "立项管理对象",description = "")
@TableName("ba_project")
public class BaProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 可编辑用户 */
    //@Excel(name = "可编辑用户")
    @ApiModelProperty(value = "可编辑用户")
    private String users;

    /** 项目名称 */
    @Excel(name = "项目名称")
    @ApiModelProperty(value = "项目名称")
    private String name;

    /**
     * 项目类型
     */
    @Excel(name = "项目类型",readConverterExp = "4=常规立项,2=仓储立项")
    @ApiModelProperty(value = "项目类型")
    private String projectType;

    /**
     * 货物名称
     */
    @Excel(name = "品名")
    @TableField(exist = false)
    @ApiModelProperty(value = "货物名称")
    private String goodsName;

    /**
     * 拼接名称（终端企业/客户）
     */
    @Excel(name = "终端企业/客户")
    @ApiModelProperty(value = "终端企业/客户")
    @TableField(exist = false)
    private String spliceName;

    /**
     * 所属事业部
     */
    //@Excel(name = "所属事业部")
    @ApiModelProperty(value = "所属事业部")
    private String partDeptName;

    /** 关联销售合同 */
    //@Excel(name = "关联销售合同")
    @ApiModelProperty(value = "关联销售合同")
    private String contractName;

    /** 合同编号 */
    //@Excel(name = "合同编号")
    @ApiModelProperty(value = "合同编号")
    private String contractId;

    /** 业务类型 */
    //@Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 对接人 */
    //@Excel(name = "实际发运人")
    @ApiModelProperty(value = "实际发运人")
    private String counterpartId;

    /** 对接人名称 */
    //@Excel(name = "实际发运人名称")
    @ApiModelProperty(value = "实际发运人名称")
    private String counterpartName;

    /** 供应商名称 */
    //@Excel(name = "供应商名称")
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 供应商编号 */
    //@Excel(name = "供应商编号")
    @ApiModelProperty(value = "供应商编号")
    private String supplierId;

    /** 用煤企业名称 */
    //@Excel(name = "用煤企业名称")
    @ApiModelProperty(value = "用煤企业名称")
    private String enterpriseName;

    /** 用煤企业编号 */
    //@Excel(name = "用煤企业编号")
    @ApiModelProperty(value = "用煤企业编号")
    private String enterpriseId;

    /** 项目开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "项目开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目开始时间")
    private Date startTime;

    /** 项目截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "项目截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目截止时间")
    private Date endTime;

    /** 预估项目金额 */
    //@Excel(name = "预估项目金额")
    @ApiModelProperty(value = "预估项目金额")
    private BigDecimal amount;

    /** 备注 */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 开户名 */
    //@Excel(name = "开户名")
    @ApiModelProperty(value = "开户名")
    private String openName;

    /** 开户行 */
    //@Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 账户号 */
    //@Excel(name = "账户号")
    @ApiModelProperty(value = "账户号")
    private String account;

    /** 开户行地址 */
    //@Excel(name = "开户行地址")
    @ApiModelProperty(value = "开户行地址")
    private String bankAddress;

    /** 立项状态 */
    //@Excel(name = "立项状态")
    @ApiModelProperty(value = "立项状态")
    private String projectState;

    /** 状态 */
    //@Excel(name = "审批状态")
    @ApiModelProperty(value = "审批状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 类型 */
    //@Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 用户id */
    //@Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    //@Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 部门名称 */
    //@Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 结算比例 **/
    //@Excel(name = "结算比例")
    @ApiModelProperty(value = "结算比例")
    private String settlementProportion;

    /** 预估服务周期 **/
    //@Excel(name = "预估服务周期")
    @ApiModelProperty(value = "预估服务周期")
    private Integer serviceCycle;

    /** 月发运数量 **/
    //@Excel(name = "月发运数量")
    @ApiModelProperty(value = "月发运数量")
    private BigDecimal shippingNum;

    /** 服务费收取模式 **/
    //@Excel(name = "服务费收取模式")
    @ApiModelProperty(value = "服务费收取模式")
    private String pattern;

    /** 立项年化 **/
    //@Excel(name = "立项年化")
    @ApiModelProperty(value = "立项年化")
    private BigDecimal annualization;

    /** 保证金金额 **/
    //@Excel(name = "保证金金额")
    @ApiModelProperty(value = "保证金金额")
    private String bond;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private String start;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private String end;

    /** 审批流程Id **/
    //@Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /**
     * 项目编号
     */
    //@Excel(name = "项目编号")
    @ApiModelProperty(value = "项目编号")
    private String projectNum;


    /**
     * 立项申请表
     */
    //@Excel(name = "立项申请表")
    @ApiModelProperty(value = "立项申请表")
    private String annex;

    /**
     * 企业资质
     */
    //@Excel(name = "企业资质")
    @ApiModelProperty(value = "企业资质")
    private String qualificationsAnnex;

    /**
     * 其他附件
     */
    //@Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /**
     * 发起人
     */
    @Excels({
            @Excel(name = "所属事业部", targetAttr = "partDeptName", type = Excel.Type.EXPORT)
    })
    @TableField(exist = false)
    private SysUser user;

    /**
     * 发起人名称
     */
    @Excel(name = "发起人")
    @TableField(exist = false)
    private String userName;

    /**
     * 创建人公司名称
     */
    //@Excel(name = "创建人公司名称")
    @TableField(exist = false)
    private String comName;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 项目阶段
     */
    //@Excel(name = "项目阶段")
    @ApiModelProperty(value = "项目阶段")
    private Integer projectStage;

    /**
     * 立项吨费（元/吨）
     */
    //@Excel(name = "立项吨费（元/吨）")
    @ApiModelProperty(value = "立项吨费（元/吨）")
    private BigDecimal tonnageCharge;

    /**
     * 预估单价
     */
    //@Excel(name = "预估单价")
    @ApiModelProperty(value = "预估单价")
    private BigDecimal unitPrice;

    /**
     * 当前单价
     */
    //@Excel(name = "当前单价")
    @ApiModelProperty(value = "当前单价")
    private BigDecimal currentPrice;

    /**
     * 货物种类
     */
    //@Excel(name = "货物种类")
    @ApiModelProperty(value = "货物种类")
    private String goodsType;

    /**
     * 货物ID
     */
    //@Excel(name = "货物ID")
    @ApiModelProperty(value = "货物ID")
    private String goodsId;

    /**
     * 货物对象
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "货物名称")
    private List<BaGoods> goodsList;

    /**
     * 付款计划
     */
    //@Excel(name = "付款计划")
    @ApiModelProperty(value = "付款计划")
    private String paymentPlan;

    /**
     * 回款计划
     */
    //@Excel(name = "回款计划")
    @ApiModelProperty(value = "回款计划")
    private String collectionPlan;

    /**
     * 保证金考核
     */
    //@Excel(name = "保证金考核")
    @ApiModelProperty(value = "保证金考核")
    private String bondCheck;

    /**
     * 违约考核
     */
    //@Excel(name = "违约考核")
    @ApiModelProperty(value = "违约考核")
    private String defaultCheck;

    /**
     * 风险把控
     */
    //@Excel(name = "风险把控")
    @ApiModelProperty(value = "风险把控")
    private String riskControl;

    /**
     * 计划资金
     */
    //@Excel(name = "计划资金")
    @ApiModelProperty(value = "计划资金")
    private String plannedAmount;

    /**
     * 年化合计
     */
    //@Excel(name = "年化合计")
    @ApiModelProperty(value = "年化合计")
    private String totalAnnualized;

    /**
     * 吨费
     */
    //@Excel(name = "吨费")
    @ApiModelProperty(value = "吨费")
    private String tonnageFee;

    /**
     * 业务经理
     */
    //@Excel(name = "业务经理")
    @ApiModelProperty(value = "业务经理")
    private String serviceManager;

    /**
     * 现场人员
     */
    //@Excel(name = "现场人员")
    @ApiModelProperty(value = "现场人员")
    private String personScene;

    /**
     * 运营责任人
     */
    //@Excel(name = "运营责任人")
    @ApiModelProperty(value = "运营责任人")
    private String personCharge;

    /**
     * 现场负责人
     */
    //@Excel(name = "现场负责人")
    @ApiModelProperty(value = "现场负责人")
    private String personSite;

    /**
     * 运输方式
     */
    //@Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportType;

    /**
     * 交货方式
     */
    //@Excel(name = "交货方式")
    @ApiModelProperty(value = "交货方式")
    private String shippingType;

    /**
     * 立项序号
     */
    //@Excel(name = "立项序号")
    @ApiModelProperty(value = "立项序号")
    private String projectSerial;

    /**
     * 收费标准
     */
    //@Excel(name = "收费标准")
    @ApiModelProperty(value = "收费标准")
    private String standardsId;

    @ApiModelProperty(value = "收费标准对象")
    @TableField(exist = false)
    private BaChargingStandards standards;

    /**
     * 回款周期
     */
    //@Excel(name = "回款周期")
    @ApiModelProperty(value = "回款周期")
    private String paymentCycle;

    /**
     * 履约保证金出发规则
     */
    //@Excel(name = "履约保证金出发规则")
    @ApiModelProperty(value = "履约保证金出发规则")
    private String performancePenaltyRules;

    /**
     * 违约考核规则
     */
    //@Excel(name = "违约考核规则")
    @ApiModelProperty(value = "违约考核规则")
    private String defaultAssessmentRules;

    /**
     * 发运环节
     */
    //@Excel(name = "发运环节")
    @ApiModelProperty(value = "发运环节")
    private String shippingProcess;

    /**
     * 回款环节
     */
    //@Excel(name = "回款环节")
    @ApiModelProperty(value = "回款环节")
    private String collectionProcess;

    /**
     * 付款环节
     */
    //@Excel(name = "付款环节")
    @ApiModelProperty(value = "付款环节")
    private String paymentProcess;

    /**
     * 委托授权时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "委托授权时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date authorizationTime;

    /**
     * 内部对接人
     */
    //@Excel(name = "内部对接人")
    @ApiModelProperty(value = "内部对接人")
    private String internalContactPerson;

    /**
     * 内部对接人名称
     */
    @ApiModelProperty(value = "内部对接人名称")
    @TableField(exist = false)
    private String internalContactPersonName;

    /**
     * 外部对接人
     */
    //@Excel(name = "外部对接人")
    @ApiModelProperty(value = "外部对接人")
    private String externalContactPerson;

    /**
     * 对接人状态
     */
    //@Excel(name = "对接人状态")
    @ApiModelProperty(value = "对接人状态")
    private String contactStatus;

    /**
     * 提交意见
     */
    //@Excel(name = "提交意见")
    @ApiModelProperty(value = "提交意见")
    private String submitComments;

    /**
     * 产品类型名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "货物种类名称")
    private String goodsTypeName;

    /**
     * 业务经理名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "业务经理名称")
    private String serviceManagerName;

    /**
     * 现场人员名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "现场人员名称")
    private String personSceneName;

    /**
     * 运营责任人名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "运营责任人名称")
    private String personChargeName;

    /**
     * 现场负责人名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "现场负责人名称")
    private String personSiteName;

    /**
     *已发运量
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "已发运量")
    private BigDecimal trafficVolume;
    /**
     * 已收金额
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "已收金额")
    private BigDecimal received;
    /**
     * 已付金额
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "已付金额")
    private BigDecimal paid;

    /** 终端企业  */
    @TableField(exist = false)
    @ApiModelProperty(value = "终端企业")
    private BaEnterprise baEnterprise;

    /** 托盘公司ID **/
    @ApiModelProperty(value = "托盘公司ID")
    private String palletCompanyId;

    /** 托盘公司名称 */
    //@Excel(name = "托盘公司名称")
    @TableField(exist = false)
    @ApiModelProperty(value = "托盘公司名称")
    private String palletCompanyName;

    /** 仓库id **/
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 仓库名称 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depotName;

    /** 仓库所属公司 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "仓库所属公司")
    private String depotCompany;

    /** 筛选标识 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "筛选标识")
    private String pepsi;

    /**
     * 授权标记
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "授权标记")
    private String empower;

    /**
     * 项目简称
     */
    //@Excel(name = "项目简称")
    @ApiModelProperty(value = "项目简称")
    private String nameShorter;

    /** 额度上限 **/
    //@Excel(name = "额度上限")
    @ApiModelProperty(value = "额度上限")
    private BigDecimal upperLimit;

    /** 业务线 */
    //@Excel(name = "业务线")
    @ApiModelProperty(value = "业务线")
    private String businessLines;

    /** 下游月需求量 **/
    //@Excel(name = "下游月需求量")
    @ApiModelProperty(value = "下游月需求量")
    private BigDecimal monthlyDemand;

    /** 投标保证金缴罚规则 */
    //@Excel(name = "投标保证金缴罚规则")
    @ApiModelProperty(value = "投标保证金缴罚规则")
    private String bidSecurityPayment;

    /** 团队 */
    //@Excel(name = "团队")
    @ApiModelProperty(value = "团队")
    private String projectTeam;

    /** 业务专员 */
    //@Excel(name = "业务专员")
    @ApiModelProperty(value = "业务专员")
    private String salesman;

    /** 下游内部对接人 */
    //@Excel(name = "下游内部对接人")
    @ApiModelProperty(value = "下游内部对接人")
    private String lowInternalContactPerson;

    /** 下游内部对接人名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "下游内部对接人名称")
    private String lowInternalContactPersonName;

    /** 下游外部对接人 */
    //@Excel(name = "下游外部对接人")
    @ApiModelProperty(value = "下游外部对接人")
    private String lowExternalContactPerson;

    /** 下游对接人状态 */
   //@Excel(name = "下游对接人状态")
    @ApiModelProperty(value = "下游对接人状态")
    private String lowContactStatus;

    /** 下游项目开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "下游项目开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游项目开始时间")
    private Date lowStartTime;

    /** 下游项目截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "下游项目截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "下游项目截止时间")
    private Date lowEndTime;

    /** 业务专员 */
    @TableField(exist = false)
    @ApiModelProperty(value = "业务专员")
    private String salesmanName;

    /** 包含业务助理角色 */
    //@Excel(name = "包含业务助理角色")
    @TableField(exist = false)
    @ApiModelProperty(value = "包含业务助理角色")
    private String isYewuzhuli;

    /** 收付款 */
    //@Excel(name = "收付款")
    @ApiModelProperty(value = "收付款")
    private String collectionPayment;

    /** 客户ID */
    //@Excel(name = "客户ID")
    @ApiModelProperty(value = "客户ID")
    private String customId;

    /** 客户名称 */
    //@Excel(name = "客户名称")
    @ApiModelProperty(value = "客户名称")
    @TableField(exist = false)
    private String customName;

    /** 客户背景 */
    //@Excel(name = "客户背景")
    @ApiModelProperty(value = "客户背景")
    private String customBackground;

    /** 业务模式 */
    //@Excel(name = "业务模式")
    @ApiModelProperty(value = "业务模式")
    private String businessModel;

    /** 仓库签署方 */
    //@Excel(name = "仓库签署方")
    @ApiModelProperty(value = "仓库签署方")
    private String depotSigned;

    /** 仓库监管方 */
    //@Excel(name = "仓库监管方")
    @ApiModelProperty(value = "仓库监管方")
    private String depotSupervise;

    /** 监管内容 */
    //@Excel(name = "监管内容")
    @ApiModelProperty(value = "监管内容")
    private String superviseContent;

    /** 装卸服务公司 */
    //@Excel(name = "装卸服务公司")
    @ApiModelProperty(value = "装卸服务公司")
    private String stevedoringCompany;

    /** 客户合作协议内容 */
    //@Excel(name = "客户合作协议内容")
    @ApiModelProperty(value = "客户合作协议内容")
    private String customerCooperationAgreement;

    /** 客户提供担保内容 */
    //@Excel(name = "客户提供担保内容")
    @ApiModelProperty(value = "客户提供担保内容")
    private String customerGuarantee;

    /** 仓储、加工、监管内容 */
    //@Excel(name = "仓储、加工、监管内容")
    @ApiModelProperty(value = "仓储、加工、监管内容")
    private String content;

    /** 入库货值核定内容 */
    //@Excel(name = "入库货值核定内容")
    @ApiModelProperty(value = "入库货值核定内容")
    private String warehousingVerification;

    /** 下游合同收款内容 */
    //@Excel(name = "下游合同收款内容")
    @ApiModelProperty(value = "下游合同收款内容")
    private String paymentContent;

    /** 事业部 */
    //@Excel(name = "事业部")
    @ApiModelProperty(value = "事业部")
    private String division;

    @ApiModelProperty(value = "事业部")
    @TableField(exist = false)
    private String divisionName;

    /** 监管费用 */
    //@Excel(name = "监管费用")
    @ApiModelProperty(value = "监管费用")
    private BigDecimal regulatoryFees;

    /**
     * 计划申报
     */
    @ApiModelProperty(value = "计划申报")
    @TableField(exist = false)
    private List<BaDeclare> baDeclares;

    /** 子公司 */
    //@Excel(name = "子公司")
    @ApiModelProperty(value = "子公司")
    private String subsidiary;

    /** 子公司名称 **/
    @ApiModelProperty(value = "子公司名称")
    @TableField(exist = false)
    private String subsidiaryName;

    /** 子公司简称 **/
    @ApiModelProperty(value = "子公司简称")
    @TableField(exist = false)
    private String subsidiaryAbbreviation;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    @TableField(exist = false)
    private String workState;

    /**
     * 下游贸易商
     */
    @ApiModelProperty(value = "下游贸易商")
    private String downstreamTraders;

    /**
     * 下游贸易商名称
     */
    @ApiModelProperty(value = "下游贸易商")
    @TableField(exist = false)
    private String downstreamTradersName;

    /** 通过时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审批通过日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "通过时间")
    private Date passTime;

    /**
     * 业务类型
     */
    @ApiModelProperty(value = "业务类型")
    @TableField(exist = false)
    private String businessTypes;

    /**
     * 主键搜索
     */
    @ApiModelProperty(value = "主键搜索")
    @TableField(exist = false)
    private String projectId;


    /**
     * 仓库简称
     */
    @ApiModelProperty(value = "仓库简称")
    private String warehouseAbbreviation;

    /**
     * 仓储收费标准
     */
    @ApiModelProperty(value = "仓储收费标准")
    private String storageFeeStandard;

    /**
     * 当前流程节点审批状态
     */
    @ApiModelProperty(value = "当前流程节点审批状态")
    @TableField(exist = false)
    private ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO;

    /**
     * 上游公司
     */
    @ApiModelProperty(value = "上游公司")
    private String upstreamCompany;

    @ApiModelProperty(value = "上游公司名称")
    @TableField(exist = false)
    private String upstreamCompanyName;

    /**
     * 主体公司
     */
    @ApiModelProperty(value = "主体公司")
    private String subjectCompany;

    @ApiModelProperty(value = "主体公司名称")
    @TableField(exist = false)
    private String subjectCompanyName;

    /**
     * 下游公司
     */
    @ApiModelProperty(value = "下游公司")
    private String downstreamCompany;

    @ApiModelProperty(value = "下游公司名称")
    @TableField(exist = false)
    private String downstreamCompanyName;

    /**
     * 终端公司
     */
    @ApiModelProperty(value = "终端公司")
    private String terminalCompany;

    @ApiModelProperty(value = "终端公司名称")
    @TableField(exist = false)
    private String terminalCompanyName;

    /** 合同信息 **/
    @ApiModelProperty(value = "合同信息")
    @TableField(exist = false)
    private List<BaContract> baContractList;

    /** 多部门查询 */
    @ApiModelProperty(value = "多部门查询")
    @TableField(exist = false)
    private String deptIds;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 货物来源地
     */
    @ApiModelProperty(value = "货物来源地")
    private String goodsSourceLand;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 占资账期上限 */
    //@Excel(name = "占资账期上限")
    @ApiModelProperty(value = "占资账期上限")
    private Long accountUpperLimit;

    /** 变更标识 */
    //@Excel(name = "变更标识")
    @ApiModelProperty(value = "变更标识")
    private String approveFlag;

    /** 变更人 */
    //@Excel(name = "变更人")
    @ApiModelProperty(value = "变更人")
    private String changeBy;

    /** 变更时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "变更时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "变更时间")
    private Date changeTime;

    /** 父ID */
    //@Excel(name = "父ID")
    @ApiModelProperty(value = "父ID")
    private String parentId;

    /**
     * 名称转换
     */
    @ApiModelProperty(value = "名称转换")
    private String nameConversion;

    /**
     * 序列号
     */
    @ApiModelProperty(value = "序列号")
    private Integer serialNumber;

    /**
     * 全局编号
     */
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
