package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CountContractStartDTO对象", description = "合同启动统计DTO对象")
public class CountContractStartDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    @ApiModelProperty(value = "立项类型")
    private String projectType;

    @ApiModelProperty(value = "订单类型")
    private String type;

    @ApiModelProperty(value = "审批状态")
    private String state;

    @ApiModelProperty(value = "数量统计")
    private String counting;

    @ApiModelProperty(value = "部门ID")
    private String deptId;

    @ApiModelProperty(value = "兼职部门ID")
    private String belongCompanyId;

    @ApiModelProperty(value = "业务阶段")
    private String startType;

    /** 问题标识 */
    @ApiModelProperty(value = "问题标识")
    private String problemMark;

    /** 业务类型 **/
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "业务经理归属部门ID")
    private String manageDeptId;

    @ApiModelProperty(value = "业务经理兼职部门ID")
    private String partDeptId;

    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
