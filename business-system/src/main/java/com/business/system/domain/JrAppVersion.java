package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * app版本对象 jr_app_version
 *
 * @author single
 * @date 2022-04-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "app版本对象",description = "")
@TableName("jr_app_version")
public class JrAppVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 版本编号 */
    private String versionId;

    /** app编号 */
    @Excel(name = "app编号")
    @ApiModelProperty(value = "app编号")
    private String appId;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String appName;

    /** 状态[1-历史版本需要升级,0-当前版本无需升级] */
    @Excel(name = "状态[1-历史版本需要升级,0-当前版本无需升级]")
    @ApiModelProperty(value = "状态[1-历史版本需要升级,0-当前版本无需升级]")
    private Integer status;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String note;

    /** 下载地址 */
    @Excel(name = "下载地址")
    @ApiModelProperty(value = "下载地址")
    private String url;

    /** 版本号 */
    @Excel(name = "版本号")
    @ApiModelProperty(value = "版本号")
    private String version;

    /** 删除标记 */
    private Integer delFlag;



}
