package com.business.system.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 计划申报详情对象 ba_declare_detail
 *
 * @author single
 * @date 2023-03-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "计划申报详情对象",description = "")
@TableName("ba_declare_detail")
public class BaDeclareDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 申报ID */
    @Excel(name = "申报ID")
    @ApiModelProperty(value = "申报ID")
    private String declareId;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 合同启动ID */
    @Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;

    /** 合同启动名称 */
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 计划发货量 */
    @Excel(name = "计划发货量")
    @ApiModelProperty(value = "计划发货量")
    private BigDecimal deliveryCount;

    /** 计划资金 */
    @Excel(name = "计划资金")
    @ApiModelProperty(value = "计划资金")
    private BigDecimal plannedAmount;

}
