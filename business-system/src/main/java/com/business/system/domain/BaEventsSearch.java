package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 联动事件对象 ba_events_search
 *
 * @author single
 * @date 2023-07-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "联动事件对象",description = "")
@TableName("ba_events_search")
public class BaEventsSearch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 事件规则id */
    @Excel(name = "事件规则id")
    @ApiModelProperty(value = "事件规则id")
    private String eventRuleId;

    /** 事件分类 */
    @Excel(name = "事件分类")
    @ApiModelProperty(value = "事件分类")
    private String ability;

    /** 区域编号 */
    @Excel(name = "区域编号")
    @ApiModelProperty(value = "区域编号")
    private String regionIndexCode;

    /** 事件源名称 */
    @Excel(name = "事件源名称")
    @ApiModelProperty(value = "事件源名称")
    private String ruleName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /** 分页大小 */
    @Excel(name = "分页大小")
    @ApiModelProperty(value = "分页大小")
    private Long pageSize;

    /** 页码 */
    @Excel(name = "页码")
    @ApiModelProperty(value = "页码")
    private Long pageNo;

    /** 事件等级 */
    @Excel(name = "事件等级")
    @ApiModelProperty(value = "事件等级")
    private String eventLevel;

    /** 事件等级 */
    @Excel(name = "事件等级")
    @ApiModelProperty(value = "事件等级")
    private String eventLevelValue;

    /** 事件等级颜色 */
    @Excel(name = "事件等级颜色")
    @ApiModelProperty(value = "事件等级颜色")
    private String eventLevelColor;

    /** 事件处理状态，0-未处理，1-已处理 */
    @Excel(name = "事件处理状态，0-未处理，1-已处理")
    @ApiModelProperty(value = "事件处理状态，0-未处理，1-已处理")
    private String handleStatus;

    /** 事件记录 */
    @Excel(name = "事件记录")
    @ApiModelProperty(value = "事件记录")
    private String eventlogsrclist;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;



}
