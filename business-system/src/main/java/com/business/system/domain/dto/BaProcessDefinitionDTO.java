package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author: js
 * @Description: 流程定义对象
 * @date: 2022-12-2 15:28:28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "BaProcessDefinitionDTO对象", description = "流程定义对象")
public class BaProcessDefinitionDTO implements Serializable {

    private static final long serialVersionUID = -4810512325565649409L;

    @ApiModelProperty("任务类型")
    private String taskType;

    @ApiModelProperty("发起人ID")
    private String startUserId;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;
}
