package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 盘点对象 ba_checked
 *
 * @author single
 * @date 2023-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "盘点对象",description = "")
@TableName("ba_checked")
public class BaChecked extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 盘点结果 */
    @Excel(name = "盘点结果")
    @ApiModelProperty(value = "盘点结果")
    private String resultType;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 发起人名称
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 盘点详情信息 */
    @TableField(exist = false)
    @ApiModelProperty(value = "盘点详情信息")
    private List<BaCheckedHead> baCheckedHeadList;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 盘库日期 */
    @TableField(exist = false)
    private String checkedDate;

    /** 项目名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 盘点本次货值累计 */
    @Excel(name = "盘点本次货值累计")
    @ApiModelProperty(value = "盘点本次货值累计")
    private BigDecimal checkedCargoValue;

    /** 工作流状态 **/
    @Excel(name = "工作流状态")
    @ApiModelProperty(value = "工作流状态")
    private String workState;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
