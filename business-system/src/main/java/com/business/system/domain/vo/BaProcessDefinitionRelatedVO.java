package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: js
 * @Description:
 * @date: 2022/12/2 14:59
 */
@Data
@ApiModel(value="BaProcessDefinitionRelatedVO对象",description="流程定义对象")
public class BaProcessDefinitionRelatedVO implements Serializable {

    private static final long serialVersionUID = -8630544987927686000L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "流程分类")
    private String businessType;

    @ApiModelProperty(value = "流程模板ID")
    private String templateId;

    @ApiModelProperty(value = "流程id")
    private String processDefinitionId;

    @ApiModelProperty(value = "流程key")
    private String processKey;

    @ApiModelProperty(value = "流程定义名称")
    private String name;

    @ApiModelProperty(value = "流程定义描述")
    private String description;

    @ApiModelProperty(value = "流程定义版本")
    private String customVersion;

    @ApiModelProperty(value = "维护人")
    private String updatedBy;

    @ApiModelProperty(value = "维护时间")
    private LocalDateTime updatedTime;

    @ApiModelProperty(value = "状态")
    private String state;

    @ApiModelProperty(value = "标记")
    private Integer flag;
}
