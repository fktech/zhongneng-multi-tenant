package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 汽运结算对象 ba_automobile_settlement
 *
 * @author js
 * @date 2023-02-17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "汽运结算对象",description = "")
@TableName("ba_automobile_settlement")
public class BaAutomobileSettlement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 申请人id */
    @Excel(name = "申请人id")
    @ApiModelProperty(value = "申请人id")
    private Long applyUserId;

    /** 申请人名称 */
    @Excel(name = "申请人名称")
    @ApiModelProperty(value = "申请人名称")
    private String applyUserName;

    /** 结算人id */
    @Excel(name = "结算人id")
    @ApiModelProperty(value = "结算人id")
    private Long jsUserId;

    /** 结算人名称 */
    @Excel(name = "结算人名称")
    @ApiModelProperty(value = "结算人名称")
    private String jsUserName;

    /** 应结算金额 */
    @Excel(name = "应结算金额")
    @ApiModelProperty(value = "应结算金额")
    private BigDecimal yjmoney;

    /** 实际结算金额 */
    @Excel(name = "实际结算金额")
    @ApiModelProperty(value = "实际结算金额")
    private BigDecimal sjmoney;

    /** 结算发货吨数 */
    @Excel(name = "结算发货吨数")
    @ApiModelProperty(value = "结算发货吨数")
    private BigDecimal jsFhDun;

    /** 结算收货吨数 */
    @Excel(name = "结算收货吨数")
    @ApiModelProperty(value = "结算收货吨数")
    private BigDecimal jsShDun;

    /** 结算吨数 */
    @Excel(name = "结算吨数")
    @ApiModelProperty(value = "结算吨数")
    private BigDecimal jsDun;

    /** $column.columnComment */
    @Excel(name = "结算吨数")
    @ApiModelProperty(value = "结算吨数")
    private BigDecimal infocost;

    /** 申请结算时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请结算时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请结算时间")
    private Date applyTime;

    /** 结算处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结算处理时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结算处理时间")
    private Date jsTime;

    /** 结算申请类型：0-司机申请，1-经济人申请 */
    @Excel(name = "结算申请类型：0-司机申请，1-经济人申请")
    @ApiModelProperty(value = "结算申请类型：0-司机申请，1-经济人申请")
    private Long applyFlag;

    /** 结算磅差 */
    @Excel(name = "结算磅差")
    @ApiModelProperty(value = "结算磅差")
    private Long jsBangcha;

    /** 运费扣款 */
    @Excel(name = "运费扣款")
    @ApiModelProperty(value = "运费扣款")
    private BigDecimal yfKouMoney;

    /** 其他扣款 */
    @Excel(name = "其他扣款")
    @ApiModelProperty(value = "其他扣款")
    private BigDecimal qtKouMoney;

    /** 油卡金额 */
    @Excel(name = "油卡金额")
    @ApiModelProperty(value = "油卡金额")
    private BigDecimal oilMoney;

    /** 气卡金额 */
    @Excel(name = "气卡金额")
    @ApiModelProperty(value = "气卡金额")
    private BigDecimal qkMoney;

    /** 货源单编号 */
    @Excel(name = "货源单编号")
    @ApiModelProperty(value = "货源单编号")
    private String supplierNo;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String transNo;

    /** 发货方 */
    @Excel(name = "发货方")
    @ApiModelProperty(value = "发货方")
    private String fhf;

    /** 收货方 */
    @Excel(name = "收货方")
    @ApiModelProperty(value = "收货方")
    private String shf;

    /** 发货地 */
    @Excel(name = "发货地")
    @ApiModelProperty(value = "发货地")
    private String placeDep;

    /** 发货详细地址 */
    @Excel(name = "发货详细地址")
    @ApiModelProperty(value = "发货详细地址")
    private String depDetailed;

    /** 收货地 */
    @Excel(name = "收货地")
    @ApiModelProperty(value = "收货地")
    private String placeDes;

    /** 收货详细地址 */
    @Excel(name = "收货详细地址")
    @ApiModelProperty(value = "收货详细地址")
    private String desDetailed;

    /** 货物名称 */
    @Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String goodsName;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal ratresprice;

    /** 经济人运费单价 */
    @Excel(name = "经济人运费单价")
    @ApiModelProperty(value = "经济人运费单价")
    private BigDecimal ratespriceEconomic;

    /** 运单id */
    @Excel(name = "运单id")
    @ApiModelProperty(value = "运单id")
    private Long transId;

    /** 订单id */
    @Excel(name = "订单id")
    @ApiModelProperty(value = "订单id")
    private Long supplierId;

    /** 审核状态：0-未审核，1-已审核 */
    @Excel(name = "审核状态：0-未审核，1-已审核")
    @ApiModelProperty(value = "审核状态：0-未审核，1-已审核")
    private Long checkFlag;

    /** 开票状态0已开票1未开票 */
    @Excel(name = "开票状态0已开票1未开票")
    @ApiModelProperty(value = "开票状态0已开票1未开票")
    private Long settlementStatus;

    /** 申请类型 */
    @Excel(name = "申请类型")
    @ApiModelProperty(value = "申请类型")
    private Long applyType;

    /** 服务费 */
    @Excel(name = "服务费")
    @ApiModelProperty(value = "服务费")
    private BigDecimal shuiMoney;

    /** 经纪人应付运费 */
    @Excel(name = "经纪人应付运费")
    @ApiModelProperty(value = "经纪人应付运费")
    private BigDecimal yfratesmoneyEconomic;

    /** 经济人实付运费 */
    @Excel(name = "经济人实付运费")
    @ApiModelProperty(value = "经济人实付运费")
    private BigDecimal sfratesmoneyEconomic;

    /** 发货方id */
    @Excel(name = "发货方id")
    @ApiModelProperty(value = "发货方id")
    private Long fhfId;

    /** 付款单号 */
    @Excel(name = "付款单号")
    @ApiModelProperty(value = "付款单号")
    private String paymentOrderNum;

    /** 开票单状态 */
    @Excel(name = "开票单状态")
    @ApiModelProperty(value = "开票单状态")
    private Long paymentOrderStatus;

    /** 结算运单状态 */
    @Excel(name = "结算运单状态")
    @ApiModelProperty(value = "结算运单状态")
    private Long jsAuditStatus;

    /** 结算运单审核备注 */
    @Excel(name = "结算运单审核备注")
    @ApiModelProperty(value = "结算运单审核备注")
    private String jsAuditRemoke;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 付款单名称 */
    @Excel(name = "付款单名称")
    @ApiModelProperty(value = "付款单名称")
    private String paymentOrderName;

    /** 归属货主（公司） */
    @Excel(name = "归属货主（公司）")
    @ApiModelProperty(value = "归属货主（公司）")
    private String companyName;

    /** 金额总计 */
    @Excel(name = "金额总计")
    @ApiModelProperty(value = "金额总计")
    private BigDecimal freightAmount;

    /** 运单数 **/
    @ApiModelProperty(value = "运单数")
    @TableField(exist = false)
    private Integer waybill;

    /** 付款明细 **/
    @ApiModelProperty(value = "付款明细")
    @TableField(exist = false)
    private List<BaAutomobileSettlementDetail> baAutomobileSettlementDetail;

}
