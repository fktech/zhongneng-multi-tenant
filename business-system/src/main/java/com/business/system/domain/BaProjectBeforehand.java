package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.DropDownBox;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 预立项对象 ba_project_beforehand
 *
 * @author ljb
 * @date 2023-09-20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "预立项对象",description = "")
@TableName("ba_project_beforehand")
public class BaProjectBeforehand extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 终端企业ID */
    @Excel(name = "终端企业ID")
    @ApiModelProperty(value = "终端企业ID")
    private String enterpriseId;

    /** 终端企业名称 */
    @ApiModelProperty(value = "终端企业名称")
    @TableField(exist = false)
    private String enterpriseName;

    /** 企业类型（对应数据字典） */
    @Excel(name = "企业类型", readConverterExp = "对应数据字典")
    @ApiModelProperty(value = "企业类型")
    private String enterpriseType;

    /** 公司性质（对应数据字典） */
    @Excel(name = "公司性质", readConverterExp = "对应数据字典")
    @ApiModelProperty(value = "公司性质")
    private String enterpriseNature;

    /** 商品ID */
    @Excel(name = "商品ID")
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 商品名称 */
    @ApiModelProperty(value = "商品名称")
    @TableField(exist = false)
    private String goodsName;

    /** 月供应量 */
    @Excel(name = "月供应量")
    @ApiModelProperty(value = "月供应量")
    private String monthlySupply;

    /** 运输方式 */
    @Excel(name = "运输方式")
    @ApiModelProperty(value = "运输方式")
    private String transportType;

    /** 供应商ID */
    @Excel(name = "供应商ID")
    @ApiModelProperty(value = "供应商ID")
    private String supplierId;

    /** 供应商名称 */
    @ApiModelProperty(value = "供应商名称")
    @TableField(exist = false)
    private String supplierName;

    /** 业务线 */
    @Excel(name = "业务线")
    @ApiModelProperty(value = "业务线")
    private String businessLines;

    /** 发货地 */
    @Excel(name = "发货地")
    @ApiModelProperty(value = "发货地")
    private String placeDelivery;

    /** 收费标准ID */
    @Excel(name = "收费标准ID")
    @ApiModelProperty(value = "收费标准ID")
    private String standardsId;

    @ApiModelProperty(value = "收费标准对象")
    @TableField(exist = false)
    private BaChargingStandards standards;

    /** 付款节点 */
    @Excel(name = "付款节点")
    @ApiModelProperty(value = "付款节点")
    private String settlementProportion;

    /** 付款备注 */
    @Excel(name = "付款备注")
    @ApiModelProperty(value = "付款备注")
    private String paymentRemarks;

    /** 当期单价 */
    @Excel(name = "当期单价")
    @ApiModelProperty(value = "当期单价")
    private BigDecimal periodPrice;

    /** 上期单价 */
    @Excel(name = "上期单价")
    @ApiModelProperty(value = "上期单价")
    private BigDecimal previousPrice;

    /** 付款形式（对应数据字典） */
    @Excel(name = "付款形式", readConverterExp = "对应数据字典")
    @ApiModelProperty(value = "付款形式")
    private String paymentMethod;

    /** 付款内容（付款形式扩展） */
    @Excel(name = "付款内容", readConverterExp = "付款形式扩展")
    @ApiModelProperty(value = "付款内容")
    private String paymentContent;

    /** 账期 */
    @Excel(name = "账期")
    @ApiModelProperty(value = "账期")
    private String accountPeriod;

    /** 事业部 */
    @Excel(name = "事业部")
    @ApiModelProperty(value = "事业部")
    private Long division;


    /** 托盘公司ID */
    @Excel(name = "托盘公司ID")
    @ApiModelProperty(value = "托盘公司ID")
    private String palletCompanyId;

    /** 托盘公司名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "托盘公司名称")
    private String palletCompanyName;

    /** 主体公司 */
    @Excel(name = "主体公司")
    @ApiModelProperty(value = "主体公司")
    private String principalCompany;

    /** 收费调整（收费标准ID） */
    @Excel(name = "收费调整", readConverterExp = "收费标准ID")
    @ApiModelProperty(value = "收费调整")
    private String adjustId;

    @ApiModelProperty(value = "收费调整对象")
    @TableField(exist = false)
    private BaChargingStandards adjusts;

    /** 立项主体 */
    @Excel(name = "立项主体")
    @ApiModelProperty(value = "立项主体")
    private String projectBulk;

    /** 意见 */
    @Excel(name = "意见")
    @ApiModelProperty(value = "意见")
    private String opinion;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /**
     * 发起人名称
     */
    @TableField(exist = false)
    private String userName;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 审批流程id */
    @Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    /** 预立项编号 */
    @Excel(name = "预立项编号")
    @ApiModelProperty(value = "预立项编号")
    private String beforehandNum;

    /** 立项时间 */
    @Excel(name = "立项时间")
    @ApiModelProperty(value = "立项时间")
    private String projectTime;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 负责人 */
    @Excel(name = "负责人")
    @ApiModelProperty(value = "负责人")
    private String serviceManager;


    /** 结束账期 */
    @Excel(name = "结束账期")
    @ApiModelProperty(value = "结束账期")
    private String endAccountPeriod;

    /** 付款节点 */
    @Excel(name = "付款节点")
    @ApiModelProperty(value = "付款节点")
    private String paymentNode;

    /** 待定状态 */
    @Excel(name = "待定状态")
    @ApiModelProperty(value = "待定状态")
    private String pendingStatus;

    /** 预立项名称 */
    @Excel(name = "预立项名称")
    @ApiModelProperty(value = "预立项名称")
    private String name;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 供应商状态 */
    @Excel(name = "供应商状态")
    @ApiModelProperty(value = "供应商状态")
    private String enterpriseStatus;

    /** 回款类型 */
    @Excel(name = "回款类型")
    @ApiModelProperty(value = "回款类型")
    private String collectionStatus;

    /** 回款内容（回款扩展） */
    @Excel(name = "回款内容（回款扩展）")
    @ApiModelProperty(value = "回款内容（回款扩展）")
    private String collectionContent;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 企业资质 */
//    @Excel(name = "企业资质")
    @ApiModelProperty(value = "企业资质")
    private String otherAnnex;


    /** 企业资质 */
//    @Excel(name = "企业资质")
    @ApiModelProperty(value = "企业资质")
    private String annex;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 客户信息 */
    @ApiModelProperty(value = "客户信息")
    private String customer;

    /** 进度信息 */
    @ApiModelProperty(value = "进度信息")
    @TableField(exist = false)
    private List<BaProgressInformation> baProgressInformationList;

    /** 预立项状态 */
    @ApiModelProperty(value = "预立项状态")
    private String beforehandState;

    /** 回复周期 */
    @ApiModelProperty(value = "回复周期")
    private String returningCycle;


    /** 回复时间 */
    @JsonFormat(pattern = "HH:mm")
    @ApiModelProperty(value = "回复时间")
    private Date recoveryTime;

    /** 提醒时间 */
    @ApiModelProperty(value = "提醒时间")
    private String reminderTime;

    /** 复合参数 */
    @ApiModelProperty(value = "复合参数")
    @TableField(exist = false)
    private String composite;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 下拉对象 */
    @ApiModelProperty(value = "下拉对象")
    @TableField(exist = false)
    private DropDownBox dropDownBox;



}
