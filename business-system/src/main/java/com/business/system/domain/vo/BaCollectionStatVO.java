package com.business.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 收款统计
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaCollectionStatVO {

    //本年实际收款
    private Long yearCount;

    //收款金额
    private BigDecimal yearApplyCollectionAmount;

    //本月实际收款
    private Long monthCount;

    //本月收款金额
    private BigDecimal monthApplyCollectionAmount;
}
