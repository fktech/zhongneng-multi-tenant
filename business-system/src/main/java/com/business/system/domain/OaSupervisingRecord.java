package com.business.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 办理记录对象 oa_supervising_record
 *
 * @author ljb
 * @date 2023-11-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "办理记录对象",description = "")
@TableName("oa_supervising_record")
public class OaSupervisingRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 办理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "办理时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "办理时间")
    private Date handleTime;

    /** 办理内容 */
    @Excel(name = "办理内容")
    @ApiModelProperty(value = "办理内容")
    private String handleContent;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String notes;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 督办ID */
    @Excel(name = "督办ID")
    @ApiModelProperty(value = "督办ID")
    private String supervisingId;



}
