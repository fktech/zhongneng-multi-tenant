package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.waybillDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 取样化验对象 ba_sample_assay
 *
 * @author ljb
 * @date 2024-01-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "取样化验对象",description = "")
@TableName("ba_sample_assay")
public class BaSampleAssay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 取样单号 */
    @Excel(name = "取样单号")
    @ApiModelProperty(value = "取样单号")
    private String sampleCode;

    /** 检测类型 */
    @Excel(name = "检测类型")
    @ApiModelProperty(value = "检测类型")
    private String detectionType;

    /** 项目 */
    @Excel(name = "项目")
    @ApiModelProperty(value = "项目")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 仓库ID */
    @Excel(name = "仓库ID")
    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    /** 仓库名称 */
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 检测时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "检测时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "检测时间")
    private Date detectionTime;

    /** 取样人 */
    @Excel(name = "取样人")
    @ApiModelProperty(value = "取样人")
    private String samplePeople;

    /** 取样人名称 */
    @ApiModelProperty(value = "取样人名称")
    @TableField(exist = false)
    private String samplePeopleName;

    /** 检测人 */
    @Excel(name = "检测人")
    @ApiModelProperty(value = "检测人")
    private String detectionPeople;

    /** 检测人名称 */
    @ApiModelProperty(value = "检测人名称")
    @TableField(exist = false)
    private String detectionPeopleName;

    /** 检测结果 */
    @Excel(name = "检测结果")
    @ApiModelProperty(value = "检测结果")
    private String detectionResult;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 用户对象 */
    @ApiModelProperty(value = "用户对象")
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型（1:取样  2:化验） */
    @Excel(name = "类型", readConverterExp = "1=:取样,2=:化验")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 出入库类型（1:出库   2:入库） */
    @Excel(name = "出入库类型", readConverterExp = "1=:出库,2=:入库")
    @ApiModelProperty(value = "出入库类型")
    private String headType;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 货源 */
    @Excel(name = "货源")
    @ApiModelProperty(value = "货源")
    private String goodsSource;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 指标 */
    @Excel(name = "指标")
    @ApiModelProperty(value = "指标")
    private String target;

    /** 备注2 */
    @Excel(name = "备注2")
    @ApiModelProperty(value = "备注2")
    private String remaeks2;

    /** 附件2 */
    @Excel(name = "附件2")
    @ApiModelProperty(value = "附件2")
    private String annex2;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String thdno;

    /** 运单信息 **/
    @ApiModelProperty(value = "运单信息")
    @TableField(exist = false)
    private List<waybillDTO> waybillDTOList;

    /** 化验时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "化验时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "化验时间")
    private Date assayTime;

    /**
     * 化验信息
     */
    @ApiModelProperty(value = "化验信息")
    @TableField(exist = false)
    private BaSampleAssay assay;

    /**
     * 取样信息
     */
    @ApiModelProperty(value = "取样信息")
    @TableField(exist = false)
    private BaSampleAssay sample;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 来源 **/
    @ApiModelProperty(value = "来源")
    @TableField(exist = false)
    private String source;

    /** 商品 **/
    @ApiModelProperty(value = "商品")
    @TableField(exist = false)
    private String goodsName;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

}
