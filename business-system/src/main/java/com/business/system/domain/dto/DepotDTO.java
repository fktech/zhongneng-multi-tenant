package com.business.system.domain.dto;

import com.business.common.annotation.Excel;
import com.business.system.domain.BaDepot;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author js
 * @version 1.0
 * @description: DTO基类
 * @date 2022-12-2 14:09
 */
@Data
public class DepotDTO implements Serializable {

    private static final long serialVersionUID = 9127532221382240134L;


    @ApiModelProperty(value = "仓库个数")
    private Long depotNumber;

    @ApiModelProperty(value = "仓库面积")
    private BigDecimal InventoryArea
    ;

    @ApiModelProperty(value = "仓库库存总量")
    private BigDecimal inventoryTotals;

    @ApiModelProperty(value = "仓库")
    private List<BaDepot> depotList;


}
