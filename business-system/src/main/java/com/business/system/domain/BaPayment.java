package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.dto.BaOrderDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 付款信息对象 ba_payment
 *
 * @author single
 * @date 2022-12-26
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "付款信息对象",description = "")
@TableName("ba_payment")
public class BaPayment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /**
     * 全局编号
     */
    @Excel(name = "全局编号", needMerge = true)
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /** 合同启动名称 */
    @Excel(name = "合同启动名称/采购计划", needMerge = true)
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /** 发起时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发起时间", needMerge = true, width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "发起时间")
    private Date initiationTime;

    /** 付款类型 */
    @Excel(name = "付款类型", needMerge = true, readConverterExp = "1=货款,2=运杂费,3=服务费,4=保证金,5=其他,6=退下游款")
    @ApiModelProperty(value = "付款类型")
    private String type;

    /** 付款金额(元) */
    @Excel(name = "付款申请金额(元)", needMerge = true)
    @ApiModelProperty(value = "申请付款金额(元)")
    private BigDecimal applyPaymentAmount;

    /** 导出付款日期 **/
    @Excel(name = "付款日期", needMerge = true)
    @ApiModelProperty(value = "导出付款日期")
    @TableField(exist = false)
    private String dateOfPayment;

    /** 确定付款金额 **/
    @Excel(name = "已付款金额(元)", needMerge = true)
    @ApiModelProperty(value = "确认付款金额")
    @TableField(exist = false)
    private BigDecimal paymentAmount;

    /** 待付款金额 **/
    @Excel(name = "待付款金额(元)", needMerge = true)
    @ApiModelProperty(value = "待付款金额")
    @TableField(exist = false)
    private BigDecimal pendingPayment;

    /** 业务类型 */
    //@Excel(name = "项目类型", needMerge = true, readConverterExp = "1=立项（原）,2=仓库立项,3=标准立项,4=常规立项")
    @ApiModelProperty(value = "业务类型")
    private String projectTypes;

    /** 付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "付款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", needMerge = true)
    @ApiModelProperty(value = "付款日期")
    private Date paymentDate;

    /** 状态 */
    //@Excel(name = "审批状态", needMerge = true, readConverterExp = "payment:verifying=审批中,payment:pass=已通过,payment:reject=已拒绝,payment:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 收款单位 */
    @Excel(name = "收款单位" , needMerge = true)
    @ApiModelProperty(value = "收款单位")
    private String company;

    /** 收款账号 */
    //@Excel(name = "收款账号")
    @ApiModelProperty(value = "收款账号")
    private String collectionAccount;

    /** 账户号 */
    @Excel(name = "收款账号", needMerge = true)
    @ApiModelProperty(value = "账户号")
    private String otherAccount;

    /** 开户行 */
    @Excel(name = "开户行", needMerge = true)
    @ApiModelProperty(value = "开户行")
    private String otherBank;

    /**
     * 发起人名称
     */
    @Excel(name = "归属岗位", needMerge = true)
    @TableField(exist = false)
    private String userName;



    /** 结算单编号 */
    //@Excel(name = "结算单编号")
    @ApiModelProperty(value = "结算单编号")
    private String settlementId;

    /** 本期付款比例 */
    //@Excel(name = "本期付款比例")
    @ApiModelProperty(value = "本期付款比例")
    private String currentPaymentRatio;

    /** 本期应付金额(元) */
    //@Excel(name = "本期应付金额(元)")
    @ApiModelProperty(value = "本期应付金额(元)")
    private BigDecimal currentPaymentAmount;

    /** 实付金额(元) */
    //@Excel(name = "实付金额(元)")
    @ApiModelProperty(value = "实付金额(元)")
    private BigDecimal actualPaymentAmount;


    /** 申请付款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "申请付款日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请付款日期")
    private Date applyPaymentDate;

    /** 我方付款账户 */
    //@Excel(name = "我方付款账户")
    @ApiModelProperty(value = "我方付款账户")
    private String ourPaymentAccount;

    /** 账户类型 */
    //@Excel(name = "账户类型")
    @ApiModelProperty(value = "账户类型")
    private String accountType;


    /** 开户名称 */
    //@Excel(name = "开户名称")
    @ApiModelProperty(value = "开户名称")
    private String otherAccountName;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 合同编号 */
    //@Excel(name = "合同编号")
    @ApiModelProperty(value = "合同编号")
    private String contractNum;

    /** 合同ID */
    //@Excel(name = "合同ID")
    @ApiModelProperty(value = "合同ID")
    private String contractId;


    /** 采购合同/补充协议 */
    //@Excel(name = "采购合同/补充协议")
    @ApiModelProperty(value = "采购合同/补充协议")
    private String annex;

    /** 销售合同/补充协议 */
    //@Excel(name = "销售合同/补充协议")
    @ApiModelProperty(value = "销售合同/补充协议")
    private String salesAnnex;

    /** 物流凭证（汽运磅单/铁路大票等） */
    //@Excel(name = "物流凭证（汽运磅单/铁路大票等）")
    @ApiModelProperty(value = "物流凭证（汽运磅单/铁路大票等）")
    private String logisticsVoucher;

    /** 货权转移证明 */
    //@Excel(name = "货权转移证明")
    @ApiModelProperty(value = "货权转移证明")
    private String transferRights;

    /** 上游结算单 */
    //@Excel(name = "上游结算单")
    @ApiModelProperty(value = "上游结算单")
    private String upstreamSettlement;

    /** 下游结算单 */
    //@Excel(name = "下游结算单")
    @ApiModelProperty(value = "下游结算单")
    private String downstreamSettlement;

    /** 增值税发票 */
    //@Excel(name = "增值税发票")
    @ApiModelProperty(value = "增值税发票")
    private String vatInvoice;

    /** 其他附件 */
    //@Excel(name = "其他附件")
    @ApiModelProperty(value = "其他附件")
    private String otherAnnex;

    /** 付款方式 */
    //@Excel(name = "付款方式")
    @ApiModelProperty(value = "付款方式")
    private String paymentType;

    /** 付款单位 */
    //@Excel(name = "付款单位")
    @ApiModelProperty(value = "付款单位")
    private String paymentCompany;

    /** 付款账号 */
    //@Excel(name = "付款账号")
    @ApiModelProperty(value = "付款账号")
    private String paymentAccount;

    /** 收款单位 */
    //@Excel(name = "收款单位")
    @ApiModelProperty(value = "收款单位")
    private String collectionCompany;

    /** 付款凭证 */
    //@Excel(name = "付款凭证")
    @ApiModelProperty(value = "付款凭证")
    private String paymentVoucher;

    /** 凭证备注 */
    //@Excel(name = "凭证备注")
    @ApiModelProperty(value = "凭证备注")
    private String voucherRemark;

    /** 审批流程id */
    //@Excel(name = "审批流程id")
    @ApiModelProperty(value = "审批流程id")
    private String flowId;

    @TableField(exist = false)
    private BaSettlement baSettlement;

    /** 开户信息 */
    @TableField(exist = false)
    private List<BaBank> bankList;

    /** 开户信息 */
    @TableField(exist = false)
    private BaBank baBank;

    /** 业务状态 */
    //@Excel(name = "业务状态")
    @ApiModelProperty(value = "业务状态")
    private String paymentState;

    /** 运输编号 */
    //@Excel(name = "运输编号")
    @ApiModelProperty(value = "运输编号")
    private String transportId;

    /** 运输西信息 */
    @TableField(exist = false)
    private List<BaTransport> baTransport;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 发起人
     */
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @TableField(exist = false)
    private String deptName;

    /** 账户id **/
    @ApiModelProperty(value = "账户ID")
    private String bankId;

    /** 发起人公司名称 */
    @TableField(exist = false)
    private String comName;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /**
     * 订单对象
     */
    @TableField(exist = false)
    private List<BaOrderDTO> baOrderDTOs;

    /** 合同启动ID */
    //@Excel(name = "合同启动ID")
    @ApiModelProperty(value = "合同启动ID")
    private String startId;


    @ApiModelProperty(value = "合同启动对象")
    @TableField(exist = false)
    private BaContractStart baContractStart;

    /** 付款事由 */
    //@Excel(name = "付款事由")
    @ApiModelProperty(value = "付款事由")
    private String paymentReason;

    /** 收款单位名称 */
    @ApiModelProperty(value = "收款单位名称")
    @TableField(exist = false)
    private String companyName;

    /** 货转凭证 **/
    @Excel(name = "付款凭证")
    @ApiModelProperty(value = "货转凭证")
    @TableField(exist = false)
    private List<BaPaymentVoucher> baPaymentVouchers;

    /** 税号 **/
    @ApiModelProperty(value = "确认付款金额")
    @TableField(exist = false)
    private String dutyParagraph;

    @TableField(exist = false)
    @ApiModelProperty(value = "移动端查询的临时字段-付款")
    private String keyWords;

    /** 工作流状态 **/
    @ApiModelProperty(value = "工作流状态")
    private String workState;


    /** 仓储立项编号 **/
    //@Excel(name = "仓储立项编号")
    @ApiModelProperty(value = "仓储立项编号")
    private String projectId;

    /** 仓储立项名 **/
    @ApiModelProperty(value = "仓储立项名")
    @TableField(exist = false)
    private String projectName;

    /** 采购计划编号 **/
    //@Excel(name = "采购计划编号")
    @ApiModelProperty(value = "采购计划编号")
    private String planId;

    /** 采购计划名 **/
    @ApiModelProperty(value = "采购计划名")
    @TableField(exist = false)
    private String planName;

    /** 租户ID **/
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 多条件 **/
    //@Excel(name = "多条件")
    @ApiModelProperty(value = "多条件")
    @TableField(exist = false)
    private String manyCondition;

    /** 审批通过时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "审批通过时间")
    private Date passTime;

    /** 仅查询通过 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "仅查询通过")
    private String onlyPass;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;


    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    /**
     * 授权租户ID
     */
    @ApiModelProperty(value = "授权租户ID")
    private String authorizedTenantId;
}
