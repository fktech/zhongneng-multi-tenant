package com.business.system.domain.vo;

import com.business.system.domain.dto.json.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: js
 * @Description: 审批任务对象
 * @date: 2022/12/6 17:32
 */
@Data
@ApiModel(value="ProcessTaskVOVO对象",description="审批任务对象")
public class ProcessTaskVO implements Serializable {

    @ApiModelProperty("任务ID")
    private String id;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("审核类型")
    private String approveType;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("原因")
    private String reason;

    @ApiModelProperty("审核结果")
    private String approveResult;

    @ApiModelProperty("业务数据")
    private String businessData;

    @ApiModelProperty("业务key")
    private String businessKey;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("发起时间")
    private LocalDateTime startTime;

    @ApiModelProperty("流程节点")
    private String taskDefinitionKey;

    @ApiModelProperty("流程节点名称")
    private String taskDefinitionName;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("用户信息")
    private UserInfo userInfo;

    @ApiModelProperty("节点办理人")
    private String assignee;

    @ApiModelProperty("节点办理人姓名")
    private String assigneeName;

    @ApiModelProperty("发起人姓名-岗位")
    private String startNickName;

    @ApiModelProperty("任务处理人姓名")
    private String assigneeNickName;

    @ApiModelProperty("办理编辑状态")
    private String approveEditType;

    @ApiModelProperty("公司类型")
    private String companyType;

    @ApiModelProperty("发起人姓名")
    private String startUserName;

    @ApiModelProperty("时间差")
    private String timeDifference;

    @ApiModelProperty("任务办理时间")
    private String endTime;

    @ApiModelProperty("业务名称")
    private String busiName;
}
