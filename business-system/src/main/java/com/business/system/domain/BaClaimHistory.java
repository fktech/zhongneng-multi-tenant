package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 认领历史对象 ba_claim_history
 *
 * @author ljb
 * @date 2023-01-31
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "认领历史对象",description = "")
@TableName("ba_claim_history")
public class BaClaimHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 认领编号 */
    //@Excel(name = "认领编号")
    @ApiModelProperty(value = "认领编号")
    private String claimId;

    /** 订单编号 */
    //@Excel(name = "订单编号")
    @ApiModelProperty(value = "订单编号")
    private String orderId;

    /** 合同启动名称 */
    //@Excel(name = "合同启动名称")
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String startName;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer serialNumber;

    /**
     * 全局编号
     */
    @Excel(name = "全局编号")
    @ApiModelProperty(value = "全局编号")
    private String globalNumber;

    /**
     * 合同启动全局编号
     */
    @ApiModelProperty(value = "合同启动全局编号")
    @TableField(exist = false)
    private String startGlobalNumber;

    @Excel(name = "付款单位名称")
    @TableField(exist = false)
    @ApiModelProperty(value = "付款单位名称")
    private String paymentCompanyName;

    /**
     * 收款类别
     */
    @Excel(name = "收款类型", readConverterExp = "1=下游回款,2=上游退款,3=运费退款,4=其他收款,5=保证金退款,6=保证金收款")
    @ApiModelProperty(value = "收款类别")
    @TableField(exist = false)
    private String collectionCategory;

    /** 申请收款日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "收款日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请收款日期")
    @TableField(exist = false)
    private Date applyCollectionDate;

    /** 申请收款金额(元) */
    @Excel(name = "收款金额(元)")
    @ApiModelProperty(value = "申请收款金额(元)")
    @TableField(exist = false)
    private BigDecimal applyCollectionAmount;

    /**
     * 已认领金额
     */
    @Excel(name = "已认领金额(元)")
    @ApiModelProperty(value = "已认领金额(元)")
    @TableField(exist = false)
    private BigDecimal claimed;



    /** 本期回款 */
    @Excel(name = "本合同认领金额")
    @ApiModelProperty(value = "本期回款")
    private BigDecimal currentCollection;

    /** 待认领金额 */
    @Excel(name = "待认领金额")
    @ApiModelProperty(value = "待认领金额")
    @TableField(exist = false)
    private BigDecimal stayClaim;

    /** 认领状态 */
    @Excel(name = "认领状态",readConverterExp = "1=待认领,2=已认领")
    @ApiModelProperty(value = "认领状态")
    private String claimState;

    /**
     * 认领人
     */
    @Excel(name = "认领人")
    @ApiModelProperty(value = "认领人")
    @TableField(exist = false)
    private String userName;

    /**
     * 认领时间
     */
    @Excel(name = "认领时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "认领时间")
    @TableField(exist = false)
    private Date claimTime;

}
