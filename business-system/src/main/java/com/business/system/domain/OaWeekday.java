package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 工作日对象 oa_weekday
 *
 * @author single
 * @date 2023-11-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "工作日对象",description = "")
@TableName("oa_weekday")
public class OaWeekday extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 工作日名称 */
    @Excel(name = "工作日名称")
    @ApiModelProperty(value = "工作日名称")
    private String name;

    /** 工作日设置 */
    @Excel(name = "工作日设置")
    @ApiModelProperty(value = "工作日设置")
    private String weekdaySet;

    /** 考勤开始时间 */
    @Excel(name = "考勤开始时间")
    @ApiModelProperty(value = "考勤开始时间")
    private String attendanceStart;

    /** 工作时长 */
    @Excel(name = "工作时长")
    @ApiModelProperty(value = "工作时长")
    private Long workHours;

    /** 班次ID */
    @Excel(name = "班次ID")
    @ApiModelProperty(value = "班次ID")
    private String workTimeId;

    /** 班次对象 */
    @ApiModelProperty(value = "班次对象")
    @TableField(exist = false)
    private OaWorkTimes workTime;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 考勤组ID */
    @Excel(name = "考勤组ID")
    @ApiModelProperty(value = "考勤组ID")
    private String relationId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 日期 */
    @ApiModelProperty(value = "日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String date;



}
