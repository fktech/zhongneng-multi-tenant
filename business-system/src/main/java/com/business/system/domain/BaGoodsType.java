package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 商品分类对象 ba_goods_type
 *
 * @author ljb
 * @date 2022-11-30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "商品分类对象",description = "")
@TableName("ba_goods_type")
public class BaGoodsType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 等级 */
    @Excel(name = "等级")
    @ApiModelProperty(value = "等级")
    private String level;

    /** 上级id */
    @Excel(name = "上级id")
    @ApiModelProperty(value = "上级id")
    private String parentId;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    @ApiModelProperty(value = "显示顺序")
    private Integer sort;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    @TableField(exist = false)
    private List<BaGoodsType> childList;

    /** 计价单位 */
    @Excel(name = "计价单位")
    @ApiModelProperty(value = "计价单位")
    private String pricingInit;

    /** 计量单位名称 */
    @Excel(name = "计量单位名称")
    @TableField(exist = false)
    private String pricingInitName;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
