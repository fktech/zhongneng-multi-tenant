package com.business.system.domain;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 配煤方案对象 ba_blending_programme
 *
 * @author ljb
 * @date 2024-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "配煤方案对象",description = "")
@TableName("ba_blending_programme")
public class BaBlendingProgramme extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 创建人 */
    @Excel(name = "创建人")
    @ApiModelProperty(value = "创建人")
    private String founder;

    /** Ad */
    @Excel(name = "Ad")
    @ApiModelProperty(value = "Ad")
    private BigDecimal indexAd;

    /** Vdaf */
    @Excel(name = "Vdaf")
    @ApiModelProperty(value = "Vdaf")
    private BigDecimal indexVdaf;

    /** St,d */
    @Excel(name = "St,d")
    @ApiModelProperty(value = "St,d")
    private BigDecimal indexSt;

    /** G */
    @Excel(name = "G")
    @ApiModelProperty(value = "G")
    private BigDecimal indexG;

    /** Y */
    @Excel(name = "Y")
    @ApiModelProperty(value = "Y")
    private BigDecimal indexY;

    /** X */
    @Excel(name = "X")
    @ApiModelProperty(value = "X")
    private BigDecimal indexX;

    /** Ro */
    @Excel(name = "Ro")
    @ApiModelProperty(value = "Ro")
    private BigDecimal indexRo;

    /** 标准差 */
    @Excel(name = "标准差")
    @ApiModelProperty(value = "标准差")
    private BigDecimal standardDeviation;

    /** 价格 */
    @Excel(name = "价格")
    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private Long userName;

    /** 用户对象 */
    @Excel(name = "用户对象")
    @ApiModelProperty(value = "用户对象")
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 方案名称 **/
    @Excel(name = "方案名称")
    @ApiModelProperty(value = "方案名称")
    private String name;

    /** 配煤配比 **/
    @Excel(name = "配煤配比")
    @ApiModelProperty(value = "配煤配比")
    @TableField(exist = false)
    private List<BaBlendingProportioning> baBlendingProportioningList;

    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** Mt */
    @Excel(name = "Mt")
    @ApiModelProperty(value = "Mt")
    private BigDecimal indexMt;



}
