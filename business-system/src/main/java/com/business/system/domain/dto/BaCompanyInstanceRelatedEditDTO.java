package com.business.system.domain.dto;

import com.business.system.domain.BaCompany;
import com.business.system.domain.BaSupplier;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 公司业务与流程实例关联关系审批编辑对象
 * @date: 2023/1/10
 */
@Data
@ApiModel(value = "BaCompanyInstanceRelatedEditDTO对象", description = "公司业务与流程实例关联关系审批编辑对象")
public class BaCompanyInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private BaCompany baCompany;
}
