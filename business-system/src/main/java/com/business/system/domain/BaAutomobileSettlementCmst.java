package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 汽运结算对象 ba_automobile_settlement_cmst
 *
 * @author single
 * @date 2023-08-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "汽运结算对象",description = "")
@TableName("ba_automobile_settlement_cmst")
public class BaAutomobileSettlementCmst extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单号 */
    @Excel(name = "订单号")
    @ApiModelProperty(value = "订单号")
    private String orderId;

    /** 用户id */
    @Excel(name = "用户id")
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 部门id */
    @Excel(name = "部门id")
    @ApiModelProperty(value = "部门id")
    private Long deptId;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 吨位 */
    @Excel(name = "吨位")
    @ApiModelProperty(value = "吨位")
    private BigDecimal tonnage;

    /** 结算金额 */
    @Excel(name = "结算金额")
    @ApiModelProperty(value = "结算金额")
    private BigDecimal settleMoney;

    /** 自定义编号 */
    @Excel(name = "自定义编号")
    @ApiModelProperty(value = "自定义编号")
    private String selfComment;

    /** 母单号 ,批量货有 */
    @Excel(name = "母单号 ,批量货有")
    @ApiModelProperty(value = "母单号 ,批量货有")
    private String yardId;

    /** 货主结算价格 */
    @Excel(name = "货主结算价格")
    @ApiModelProperty(value = "货主结算价格")
    private BigDecimal settlementAmount;

    /** 结算时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结算时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结算时间")
    private Date settlementTime;

    /** 结算状态 0:货主结算  1:承运方结算 */
    @Excel(name = "结算状态 0:货主结算  1:承运方结算")
    @ApiModelProperty(value = "结算状态 0:货主结算  1:承运方结算")
    private String settlementType;


    @TableField(exist = false)
    @ApiModelProperty(value = "运单数")
    private String baShippingOrderCmstNumber;
    /** 货主名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "货主名称")
    private String consignorUserName;
    /** 货主名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "结算状态")
    private String checkFlag;
    /** 货主名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "运单对象")
    private List<BaShippingOrderCmst> baShippingOrderCmst;

}
