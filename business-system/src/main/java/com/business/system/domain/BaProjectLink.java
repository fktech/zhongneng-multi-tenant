package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 立项授权对象 ba_project_link
 *
 * @author ljb
 * @date 2023-06-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "立项授权对象",description = "")
@TableName("ba_project_link")
public class BaProjectLink extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 立项主键 */
    @Excel(name = "立项主键")
    @ApiModelProperty(value = "立项主键")
    private String projectId;

    /** 部门编号 */
    @Excel(name = "部门编号")
    @ApiModelProperty(value = "部门编号")
    private String deptId;

    /**
     * 项目类型
     */
    @Excel(name = "项目类型")
    @ApiModelProperty(value = "项目类型")
    private String projectType;

}
