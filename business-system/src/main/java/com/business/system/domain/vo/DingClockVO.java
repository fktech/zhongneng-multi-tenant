package com.business.system.domain.vo;

import com.business.system.domain.OaAttendanceAddress;
import com.business.system.domain.OaWorkTimes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="打卡规则对象",description="打卡规则对象")
public class DingClockVO {

    /** 数据来源  ATM：考勤机打卡（指纹/人脸打卡）
     BEACON：IBeacon
     DING_ATM：钉钉考勤机（考勤机蓝牙打卡）
     USER：用户打卡
     BOSS：老板改签
     APPROVE：审批系统
     SYSTEM：考勤系统
     AUTO_CHECK：自动打卡 */
    @ApiModelProperty(value = "是否休假")
    private String sourceType;

    /** 计算迟到和早退，基准时间 */
    @ApiModelProperty(value = "计算迟到和早退，基准时间")
    private Date baseCheckTime;

    /** 实际打卡时间, 用户打卡时间的毫秒数 */
    @ApiModelProperty(value = "实际打卡时间, 用户打卡时间的毫秒数")
    private Date userCheckTime;

    /** 关联的审批实例ID，当该字段非空时，表示打卡记录与请假、加班等审批有关*/
    @ApiModelProperty(value = "关联的审批实例ID，当该字段非空时，表示打卡记录与请假、加班等审批有关")
    private String procInstId;

    /** 是关联的审批ID，当该字段非空时，表示打卡记录与请假、加班等审批有关*/
    @ApiModelProperty(value = "是否允许外勤")
    private Number approveId;

    /** 位置结果：
     Normal：范围内
     Outside：范围外
     NotSigned：未打卡
    */
    @ApiModelProperty(value = "位置结果")
    private String locationResult;

    /** 打卡结果：
     Normal：正常
     Early：早退
     Late：迟到
     SeriousLate：严重迟到
     Absenteeism：旷工迟到
     NotSigned：未打卡
     */
    @ApiModelProperty(value = "打卡结果")
    private String timeResult;

    /** 考勤类型：
     OnDuty：上班
     OffDuty：下班
     */
    @ApiModelProperty(value = "考勤类型")
    private String checkType;

    /**
     * 打卡人的userId
     */
    @ApiModelProperty(value = "打卡人的userId")
    private String userId;

    /**
     * 工作日
     */
    @ApiModelProperty(value = "工作日")
    private Date workDate;

    /**
     * 打卡记录ID
     */
    @ApiModelProperty(value = "打卡记录ID")
    private Number recordId;

    /**
     * 考勤组ID
     */
    @ApiModelProperty(value = "考勤组ID")
    private Number planId;

    /**
     * 唯一标识ID
     */
    @ApiModelProperty(value = "唯一标识ID")
    private Number id;
}
