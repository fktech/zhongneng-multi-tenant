package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 装货站对象 ba_loading_station
 *
 * @author ljb
 * @date 2023-01-16
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "装货站对象",description = "")
@TableName("ba_loading_station")
public class BaLoadingStation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 装货站名称 */
    @Excel(name = "装货站名称")
    @ApiModelProperty(value = "装货站名称")
    private String name;

    /** 站台编号 */
    @Excel(name = "站台编号")
    @ApiModelProperty(value = "站台编号")
    private String platformId;

    /** 站台名称 */
    @Excel(name = "站台名称")
    @ApiModelProperty(value = "站台名称")
    private String platformName;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String phone;

    /** 附件 */
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 省 **/
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 **/
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 **/
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 铁路局地址 */
    @Excel(name = "铁路局地址")
    @ApiModelProperty(value = "铁路局地址")
    private String address;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;



}
