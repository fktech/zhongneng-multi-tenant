package com.business.system.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 流程实例审批链路对象
 * @date: 2022/12/9 17:9
 */
@Data
public class ProcessInstanceApproveLinkDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = -7633191519954911201L;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 发起人ID
     */
    private String startUserId;

    /**
     * 业务ID
     */
    private String businessId;
}
