package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="打卡结果对象",description="打卡结果对象")
public class DingClockResultVO {

    /**
     * 错误码
     */
    private String errcode;

    /**
     * 错误说明
     */
    private String errmsg;

    /**
     * 分页返回参数，表示是否还有更多数据。
     *
     * true：有
     * false：没有
     */
    private boolean hasMore;

    /**
     * 钉钉打卡结果
     */
    private List<DingDingVO> recordresult;
}
