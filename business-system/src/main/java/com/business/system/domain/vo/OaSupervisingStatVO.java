package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 督办统计VO
 * @date: 2023/12/18 16:01
 */
@Data
@ApiModel(value="OaSupervisingStatVO对象",description="督办统计VO")
public class OaSupervisingStatVO implements Serializable {

    private static final long serialVersionUID = 8255975567543872592L;

    /** 督办发起 */
    @ApiModelProperty(value = "督办发起")
    private Long startCount;

    /** 已经回告 */
    @ApiModelProperty(value = "已经回告")
    private Long replyCount;

    /** 已经办结 */
    @ApiModelProperty(value = "已经办结")
    private Long endCount;

    /** 已读待办 */
    @ApiModelProperty(value = "已读待办")
    private Long readCount;
}
