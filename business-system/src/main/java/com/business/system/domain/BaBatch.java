package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 批次对象 ba_batch
 *
 * @author single
 * @date 2023-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "批次对象",description = "")
@TableName("ba_batch")
public class BaBatch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 批次号 */
    @Excel(name = "批次号")
    @ApiModelProperty(value = "批次号")
    private String batch;

    /** 初始库存 */
    @Excel(name = "初始库存")
    @ApiModelProperty(value = "初始库存")
    private BigDecimal initialStock;

    /** 当前库存 */
    @Excel(name = "当前库存")
    @ApiModelProperty(value = "当前库存")
    private Long currentStock;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 供应商id */
    @Excel(name = "供应商id")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;

    /** 供应商名称 */
    @TableField(exist = false)
    private String supplierName;

    /** 订单id */
    @Excel(name = "订单id")
    @ApiModelProperty(value = "订单id")
    private String orderId;

    /** 订单id */
    @TableField(exist = false)
    @ApiModelProperty(value = "订单名称")
    private String orderName;

    @TableField(exist = false)
    @ApiModelProperty(value = "入库列表")
    private List<BaDepotHead> intoBaDepotHeads;

    @TableField(exist = false)
    @ApiModelProperty(value = "出库列表")
    private List<BaDepotHead> outputBaDepotHeads;

    @TableField(exist = false)
    @ApiModelProperty(value = "库存列表")
    private List<BaMaterialStock> baMaterialStocks;
}
