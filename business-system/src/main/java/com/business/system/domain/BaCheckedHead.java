package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 盘点对象 ba_checked_head
 *
 * @author single
 * @date 2023-01-05
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "盘点详情对象",description = "")
@TableName("ba_checked_head")
public class BaCheckedHead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 库存ID */
    @Excel(name = "库存ID")
    @ApiModelProperty(value = "库存ID")
    private String materialId;

    /** 盘点量 */
    @Excel(name = "盘点量")
    @ApiModelProperty(value = "盘点量")
    private BigDecimal checkedCount;

    /** 盘点结果 */
    @Excel(name = "盘点结果")
    @ApiModelProperty(value = "盘点结果")
    private String resultType;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 盘点状态（1:未盘点 2:已盘点） */
    @Excel(name = "盘点状态", readConverterExp = "1=:未盘点,2=:已盘点")
    @ApiModelProperty(value = "盘点状态")
    private String checkedState;

    /** 盘点ID */
    @Excel(name = "盘点ID")
    @ApiModelProperty(value = "盘点ID")
    private String checkedId;

    /** 盘点均价 */
    @Excel(name = "盘点均价")
    @ApiModelProperty(value = "盘点均价")
    private BigDecimal checkedAverage;

    /** 库存量 */
    @Excel(name = "库存量")
    @ApiModelProperty(value = "库存量")
    private BigDecimal inventoryValue;

    /** 库存均价 */
    @Excel(name = "库存均价")
    @ApiModelProperty(value = "库存均价")
    private BigDecimal inventoryAverage;

    /** 仓库id */
    @TableField(exist = false)
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    /** 仓库名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depName;

    /** 当前库存 */
    @TableField(exist = false)
    @ApiModelProperty(value = "当前库存")
    private Long currentNumber;

    /** 初始库存 */
    @TableField(exist = false)
    @ApiModelProperty(value = "初始库存")
    private Long startNumber;

    /** 供应商名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /** 供应商编号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "供应商编号")
    private String supplierId;

    /** 批次 */
    @TableField(exist = false)
    @ApiModelProperty(value = "批次")
    private String  batch;

    /** 批次号 */
    @TableField(exist = false)
    @ApiModelProperty(value = "批次号")
    private String  batchNum;

    /** 产品id */
    @TableField(exist = false)
    @ApiModelProperty(value = "产品id")
    private String goodsId;

    /** 产品名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "产品名称")
    private String goodsName;

    /** 产品编码 */
    @TableField(exist = false)
    @ApiModelProperty(value = "产品编码")
    private String goodsCode;

    /** 项目名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    /** 项目id */
    @TableField(exist = false)
    @ApiModelProperty(value = "项目id")
    private String projectId;

    /** 库存增减 */
    @Excel(name = "库存增减")
    @ApiModelProperty(value = "库存增减")
    private BigDecimal difference;

    /** 租户Id **/
    @Excel(name = "租户Id")
    @ApiModelProperty(value = "租户Id")
    private String tenantId;

}
