package com.business.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value="OrderCordinate",description="在途轨迹")
public class OrderCordinate {

    //订单号
    @ApiModelProperty(value = "订单号")
    private String orderId;

    //开始时间
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createdStartTime;

    //结束时间
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date createdEndTime;
}
