package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 验收对象 ba_check
 *
 * @author ljb
 * @date 2022-12-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "验收对象",description = "")
@TableName("ba_check")
public class BaCheck extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 关联主键 */
    @Excel(name = "关联主键")
    @ApiModelProperty(value = "关联主键")
    private String relationId;

    /** 到货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "到货日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到货日期")
    private Date arrivalTime;

    /** 到货结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "到货结束日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到货结束日期")
    private Date arrivalEndTime;

    /** 验收日期/发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "验收日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "验收日期")
    private Date checkTime;

    /** 发货结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货结束日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货结束日期")
    private Date checkEndTime;

    /** 车数 */
    @Excel(name = "车数")
    @ApiModelProperty(value = "车数")
    private Integer carsNum;

    /** 验收煤量/矿发煤量 */
    @Excel(name = "验收煤量")
    @ApiModelProperty(value = "验收煤量")
    private BigDecimal checkCoalNum;

    /** 来货重量 */
    @Excel(name = "来货重量")
    @ApiModelProperty(value = "来货重量")
    private BigDecimal incomingWeight;

    /** 耗损量 */
    @Excel(name = "耗损量")
    @ApiModelProperty(value = "耗损量")
    private BigDecimal consumption;

    /** 低位发热值 */
    @Excel(name = "低位发热值")
    @ApiModelProperty(value = "低位发热值")
    private BigDecimal lowerCalorificValue;

    /** 灰分 */
    @Excel(name = "灰分")
    @ApiModelProperty(value = "灰分")
    private BigDecimal ashContent;

    /** 挥发分 */
    @Excel(name = "挥发分")
    @ApiModelProperty(value = "挥发分")
    private BigDecimal volatileMatter;

    /** 硫分 */
    @Excel(name = "硫分")
    @ApiModelProperty(value = "硫分")
    private BigDecimal sulphur;

    /** 全水 */
    @Excel(name = "全水")
    @ApiModelProperty(value = "全水")
    private BigDecimal totalWater;

    /** 内水 */
    @Excel(name = "内水")
    @ApiModelProperty(value = "内水")
    private BigDecimal internalWater;

    /** 灰熔点 */
    @Excel(name = "灰熔点")
    @ApiModelProperty(value = "灰熔点")
    private BigDecimal ashMeltingPoint;

    /** 矿方热值 */
    @Excel(name = "矿方热值")
    @ApiModelProperty(value = "矿方热值")
    private BigDecimal mineCalorificValue;

    /** 固定碳 */
    @Excel(name = "固定碳")
    @ApiModelProperty(value = "固定碳")
    private BigDecimal fixedCarbon;

    /** 可磨系数 */
    @Excel(name = "可磨系数")
    @ApiModelProperty(value = "可磨系数")
    private BigDecimal grindable;

    /** G值 */
    @Excel(name = "G值")
    @ApiModelProperty(value = "G值")
    private BigDecimal gValue;

    /** Y值 */
    @Excel(name = "Y值")
    @ApiModelProperty(value = "Y值")
    private BigDecimal yValue;

    /** 焦渣 */
    @Excel(name = "焦渣")
    @ApiModelProperty(value = "焦渣")
    private BigDecimal cokeResidue;

    /** 其他指标 */
    @Excel(name = "其他指标")
    @ApiModelProperty(value = "其他指标")
    private String otherIndicators;

    /** 附件   化验附件/验收附件/汽车磅单*/
    @Excel(name = "附件   化验附件/验收附件/汽车磅单")
    @ApiModelProperty(value = "附件   化验附件/验收附件/汽车磅单")
    private String enclosure;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 1:发车验收信息  2:收货验收信息 */
    @Excel(name = "1:发车验收信息  2:收货验收信息")
    @ApiModelProperty(value = "1:发车验收信息  2:收货验收信息")
    private Integer type;

    /**
     * 收货附件
     */
    @ApiModelProperty(value = "收货附件")
    private String file;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;


    /**
     * 化验信息
     */
    @ApiModelProperty(value = "化验信息")
    private String testInformation;

    /**
     * 随车化验单
     */
    @ApiModelProperty(value = "随车化验单")
    private String laboratoryTest;

    /**
     * 铁路大票
     */
    @ApiModelProperty(value = "铁路大票")
    private String railwayTicket;

    /**
     * 轨道衡
     */
    @ApiModelProperty(value = "轨道衡")
    private String trackScale;

    /**
     * 随车附表
     */
    @ApiModelProperty(value = "随车附表")
    private String accompanyingSchedule;

    /** 货权转移证明 */
    @Excel(name = "货权转移证明")
    @ApiModelProperty(value = "货权转移证明")
    private String transferRights;

    /** 货转标记 0 第一次 1 已填充货转信息 */
    @Excel(name = "货转标记 0 第一次 1 已填充货转信息")
    @ApiModelProperty(value = "货转标记 0 第一次 1 已填充货转信息")
    private String transferFlag;
}
