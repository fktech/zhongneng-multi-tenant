package com.business.system.domain.vo;

import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 今日入库量、出库量统计
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaDepotHeadChartVO {

    /** 商品 */
    @ApiModelProperty(value = "商品")
    private String goodsName;

    /** 占比 */
    @ApiModelProperty(value = "商品")
    private BigDecimal proportion;
}
