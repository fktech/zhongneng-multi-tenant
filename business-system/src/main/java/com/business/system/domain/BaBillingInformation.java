package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 开票信息对象 ba_billing_information
 *
 * @author single
 * @date 2023-02-28
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "开票信息对象",description = "")
@TableName("ba_billing_information")
public class BaBillingInformation
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 税号 */
    @Excel(name = "税号")
    @ApiModelProperty(value = "税号")
    private String dutyParagraph;

    /** 单位地址 */
    @Excel(name = "单位地址")
    @ApiModelProperty(value = "单位地址")
    private String workAddress;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 银行账号 */
    @Excel(name = "银行账号")
    @ApiModelProperty(value = "银行账号")
    private String account;

    /** 电话 */
    @Excel(name = "电话")
    @ApiModelProperty(value = "电话")
    private String phone;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;



}
