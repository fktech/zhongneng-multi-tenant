package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 事前申请对象
 *
 * @author single
 * @date 2023-11-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "事前申请对象",description = "")
public class OaBeforehandStat extends BaseEntity
{

    private static final long serialVersionUID = -3053315985921437722L;

    /** 员工ID */
    @TableField(exist = false)
    private Long employeeId;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;
}
