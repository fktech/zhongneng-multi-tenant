package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 收费标准对象 ba_charging_standards
 *
 * @author js
 * @date 2023-06-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "收费标准对象",description = "")
@TableName("ba_charging_standards")
public class BaChargingStandards extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 储备单吨 */
    @Excel(name = "储备单吨")
    @ApiModelProperty(value = "储备单吨")
    private String reserveTon;

    /** 储备年化 */
    @Excel(name = "储备年化")
    @ApiModelProperty(value = "储备年化")
    private String reserveAnnualized;

    /** 到厂单吨 */
    @Excel(name = "到厂单吨")
    @ApiModelProperty(value = "到厂单吨")
    private String arriveTon;

    /** 到厂年化 */
    @Excel(name = "到厂年化")
    @ApiModelProperty(value = "到厂年化")
    private String arriveAnnualized;

    /** 车板单吨 */
    @Excel(name = "车板单吨")
    @ApiModelProperty(value = "车板单吨")
    private String vehicleTon;

    /** 车板年化 */
    @Excel(name = "车板年化")
    @ApiModelProperty(value = "车板年化")
    private String vehicleAnnualized;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 到厂运费单吨 */
    @Excel(name = "到厂运费单吨")
    @ApiModelProperty(value = "到厂运费单吨")
    private String arriveFreightTon;

    /** 到厂运费年化 */
    @Excel(name = "到厂运费年化")
    @ApiModelProperty(value = "到厂运费年化")
    private String arriveFreightAnnualized;

    /** 车板运费单吨 */
    @Excel(name = "车板运费单吨")
    @ApiModelProperty(value = "车板运费单吨")
    private String vehicleFreightTon;

    /** 车板运费年化 */
    @Excel(name = "车板运费年化")
    @ApiModelProperty(value = "车板运费年化")
    private String vehicleFreightAnnualized;

    /** 代采单吨 */
    @Excel(name = "代采单吨")
    @ApiModelProperty(value = "代采单吨")
    private String replacePurchaseTon;

    /** 代采年化 */
    @Excel(name = "代采年化")
    @ApiModelProperty(value = "代采年化")
    private String replacePurchaseAnnualized;

    /** 预付款单吨 */
    @Excel(name = "预付款单吨")
    @ApiModelProperty(value = "预付款单吨")
    private String advancePaymentTon;

    /** 预付款年化 */
    @Excel(name = "预付款年化")
    @ApiModelProperty(value = "预付款年化")
    private String advancePaymentAnnualized;

}
